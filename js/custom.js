jQuery(document).ready(function() {
	$('.table-responsive').on('show.bs.dropdown', function () {
     $('.table-responsive').css( "overflow", "inherit" );
	});
	$('.table-responsive').on('hide.bs.dropdown', function () {
	     $('.table-responsive').css( "overflow", "auto" );
	})
	$('[data-toggle="tooltip"]').tooltip();
	$(document).on("pjax:success", function() {
	  $('[data-toggle="tooltip"]').tooltip();
	});
	$(document).delegate('.btn-view', 'click', function() {
		if($(".btn-view").length){
			showDetail($(this).parents('tr').find('.btn-view'));
		}
  });
	$(document).delegate(".custom-file-input", "change", function() {
  	readImageURL(this,$(this));
  });
	$(document).delegate('.browse', 'click', function(){
	  var file = $(this).parents(".form-group").find('.file');
	  file.trigger('click');
	});
	$(document).delegate('.load-modal', 'click', function(){
		modalId="general-modal";
		if($(this).filter('[data-mid]').length!==0) {
			modalId=$(this).data("mid");
		}

		url=$(this).data("url");
		heading=$(this).data("heading");
		$.ajax({
			url: url,
			dataType: "html",
			success: function(data) {
				$("#"+modalId).find("h5.modal-title").html(heading);
				$("#"+modalId).find(".modalContent").html(data);
				$("#"+modalId).modal();
			},
			error: bbAlert
		});
	});
  $(document).delegate(".input-file", "change", function() {
		readURLNew(this,$(this));
	});
});
function bbAlert(xhr, ajaxOptions, thrownError){
	swal(thrownError,xhr.statusText + "\n" + xhr.responseText);
	$(".blockUI").remove();
}
window.closeModal = function()
{
    $('.modal').modal('hide');
};
function readURLNew(input,el) {
	console.log(el);
	console.log(input);
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			//$(el).parents(".avatar-upload").find(".imagePreview").css("background-image", "url("+e.target.result +")");
			//$(el).parents(".avatar-upload").find(".imagePreview").hide();
			//$(el).parents(".avatar-upload").find(".imagePreview").fadeIn(650);
			//$(el).parents(".avatar-upload").addClass("img-added");
		}
		reader.readAsDataURL(input.files[0]);
		$(el).parents(".form-group").find(".input-group input").val(input.files[0].name);
	}
}
function readImageURL(input,el) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$(el).parents(".input-group").find(".input-group-text img").attr("src", e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

function blink(selector){
	$(selector).fadeOut('slow', function(){
		$(this).fadeIn('slow', function(){
			blink(this);
		});
	});
}
function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
