<?php
use yii\helpers\Html;
use yii\helpers\Url;

$canBook=Yii::$app->user->identity->canBookBoat;
if($canBook==false){
$balance=Yii::$app->user->identity->profileInfo->credit_balance;
$this->registerJs('
$("body").on("click", ".low-balance", function () {
	swal({title: "Information", html: "Dear Captain, Your current balance is '.$balance.' AED. Please deposit funds by clicking deposit funds. Enjoy Boating With Relief", type: "info", timer: 5000});
});
');
}
?>
<script>
function loadFreeBoats(marina,date,time,contnr,fld,showBlk){
	if(marina!='' && marina!='null' && date!='' && date!='null' && time!='' && time!='null'){
		App.blockUI({
			message: "<?= Yii::t('app','Please wait...');?>",
			target: contnr,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "<?= Url::to(['suggestion/free-boats','s'=>'p'])?>&marina="+marina+"&date="+date+"&time="+time+"&showbulk="+showBlk,
			dataType: "html",
			success: function(html) {
				App.unblockUI($(contnr));
				$("#"+fld).html(html);
			},
			error: bbAlert
		});
	}
}
function openBoatSelection(marina_id,date,boatName,fld)
{
	$.ajax({
		url: "<?= Url::to(['suggestion/sub-boat-selection','marina_id'=>''])?>"+marina_id+"&date="+date+"&fld="+fld,
		dataType: "html",
		success: function(data) {
			$("#general-modal-secondary").find("h5.modal-title").html("Please select boat you would like for "+boatName);
			$("#general-modal-secondary").find(".modalContent").html(data);
			$("#general-modal-secondary").modal();
		},
		error: bbAlert
	});
}
function saveSubBoatSelection(fld)
{
	$("#"+fld+"-selected_boat").val($("#spSubBSel").val());
}
</script>
