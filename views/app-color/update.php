<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppColor */

$this->title = Yii::t('app', 'Update Color: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Colors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="color-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
