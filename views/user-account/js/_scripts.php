<?php
use yii\helpers\Url;

$this->registerJs('
	$(document).delegate(".btn-add-credit", "click", function() {
		id=$(this).data("userid");
		username=$(this).data("username");
		$.ajax({
			url: "'.Url::to(['user-account/add-credit','id'=>'']).'"+id,
			dataType: "html",
			type: "POST",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html("Add Credits to "+username);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
			},
			error: bbAlert
		});
	});
	$("body").on("beforeSubmit", "form#user-add-credit-form", function () {
		_targetContainer=$("#user-add-credit-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Credits saved successfully').'", type: "success"});
					  window.closeModal();
						$.pjax.reload({container: "#grid-container", timeout: 2000});
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
	$(document).delegate(".btn-charge-credit", "click", function() {
		id=$(this).data("userid");
		username=$(this).data("username");
		$.ajax({
			url: "'.Url::to(['user-account/charge-credit','id'=>'']).'"+id,
			dataType: "html",
			type: "POST",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html("Charge Credits to "+username);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
			},
			error: bbAlert
		});
	});
	$("body").on("beforeSubmit", "form#user-charge-form", function () {
		_targetContainer=$("#user-charge-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Amount charged successfully').'", type: "success"});
					  window.closeModal();
						$.pjax.reload({container: "#grid-container", timeout: 2000});
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
');
?>
