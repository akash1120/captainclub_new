<?php
use yii\helpers\Url;

$this->registerJs('
	$("body").on("beforeSubmit", "form#user-add-security-deposit-form", function () {
		_targetContainer=$("#user-add-security-deposit-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Security Deposit saved successfully').'", type: "success"});
					  window.closeModal();
						$.pjax.reload({container: "#grid-container", timeout: 2000});
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
	$("body").on("beforeSubmit", "form#user-deduct-security-deposit-form", function () {
		_targetContainer=$("#user-deduct-security-deposit-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Amount deducted from Security Deposit successfully').'", type: "success"});
					  window.closeModal();
						$.pjax.reload({container: "#grid-container", timeout: 2000});
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
');
?>
