<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id'=>'user-add-credit-form']); ?>
<?= $form->field($model, 'credits')->textInput() ?>
<?= $form->field($model, 'descp')->textInput() ?>
<div class="form-group">
	<?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
	<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
</div>
<?php ActiveForm::end(); ?>
