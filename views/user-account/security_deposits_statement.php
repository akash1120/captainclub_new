<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\widgets\UsersTabStart;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Member:').' '.$model->fullname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.filters{display:none;}
</style>
<div class="user-index">
  <?= UsersTabStart::header($model,$searchModel)?>
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomTabbedGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'options'=>['class'=>'grid-view'],
      'columns' => [
        ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']],
        'created_at:datetime',
        'descp',
        ['label'=>'Debit','attribute'=>'amount','value'=>function($model){
          return $model['trans_type']=='dr' ? 'AED '.$model['amount'] : '';
        }],
        ['label'=>'Credit','attribute'=>'amount','value'=>function($model){
          return $model['trans_type']=='cr' ? 'AED '.$model['amount'] : '';
        }],
        ['format'=>'raw','label'=>'Balance','value'=>function($model) use ($searchModel){
          return $searchModel->getRowBalanceAmount($model['id']);
        }],
      ],
    ]);?>
    <?php CustomPjax::end(); ?>
  <?= UsersTabStart::footer()?>
</div>
<?= $this->render('/user-account/js/_scripts')?>
<?= $this->render('/user-account/js/security_deposit_scripts')?>
