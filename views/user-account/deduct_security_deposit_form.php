<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id'=>'user-deduct-security-deposit-form']); ?>
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<?= $form->field($model, 'amount')->textInput() ?>
	</div>
	<div class="col-xs-12 col-sm-6">
		<?= $form->field($model, 'deposit_type')->dropDownList(['Cash'=>'Cash','Cheque'=>'Cheque'],['class'=>'custom-select']); ?>
	</div>
</div>
<?= $form->field($model, 'descp')->textArea(['rows'=>5]) ?>
<div class="form-group">
	<?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
	<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
</div>
</div>
<?php ActiveForm::end(); ?>
