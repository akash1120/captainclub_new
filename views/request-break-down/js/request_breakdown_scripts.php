<?php
$this->registerJs('
  $("body").on("beforeSubmit", "form#reqBreakDownForm", function () {
    _targetContainer=$("#reqBreakDownForm")
    App.blockUI({
      message: "'.Yii::t('app','Please wait...').'",
      target: _targetContainer,
      overlayColor: "none",
      cenrerY: true,
      boxed: true
    });

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find(".has-error").length) {
        return false;
     }
     // submit form
     $.ajax({
        url: form.attr("action"),
        type: "post",
        data: form.serialize(),
        success: function (response) {
          if(response=="success"){
            swal({title: "'.Yii::t('app','Sent').'", html: "'.Yii::t('app','Request Sent. Thank you.').'", type: "success"});
            form.trigger("reset");
            window.closeModal();
            if($("#grid-container").length){
              $.pjax.reload({container: "#grid-container", timeout: 2000});
            }
          }else{
            swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
          }
          App.unblockUI($(_targetContainer));
        }
     });
     return false;
  });
');
?>
