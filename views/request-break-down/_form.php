<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RequestBreakDown */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="request-break-down-form">
    <?php $form = ActiveForm::begin(['id'=>'reqBreakDownForm']); ?>
    <?= $form->field($model, 'boat_id')->dropDownList(Yii::$app->appHelperFunctions->getBoatsListArr(),['prompt'=>Yii::t('app','Select'),'tabindex'=>'1']) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'break_down_level')->radioList(Yii::$app->operationHelperFunctions->breakDownLevel) ?>
    <?= $form->field($model, 'reason')->radioList(Yii::$app->operationHelperFunctions->breakDownReason) ?>
    <?= $form->field($model, 'user_remarks')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->id!=null ? Yii::t('app', 'Update') : Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>
