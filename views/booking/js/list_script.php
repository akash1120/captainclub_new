<?php
use yii\helpers\Html;
use yii\helpers\Url;

$marinaEqupments=Yii::$app->jsFunctions->marinaEquipments;

$this->registerJs($marinaEqupments.'
	initScripts();
	$(document).on("pjax:success", function() {
	  initScripts();
	});
	$(document).delegate(".delbkng", "click", function() {
		_this=$(this);
	});
	$(document).delegate(".request-captain-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeCaptainMsg.'", type: "info"});
	});
	$(document).delegate(".request-equip-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeEquipmentMsg.'", type: "info"});
	});
	$(document).delegate(".request-bbq-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeBBQMsg.'", type: "info"});
	});
	$(document).delegate(".request-wake-boarding-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeWakeBoardingMsg.'", type: "info"});
	});
	$(document).delegate(".request-wake-surfing-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeWakeSurfingMsg.'", type: "info"});
	});
	$(document).delegate(".request-ice-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeIceMsg.'", type: "info"});
	});
	$(document).delegate(".request-kids-life-jacket-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeKidsLifeJacketMsg.'", type: "info"});
	});
	$(document).delegate(".request-early-departure-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeEarlyDepartureMsg.'", type: "info"});
	});
	$(document).delegate(".request-overnight-camping-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeOvernightCampingMsg.'", type: "info"});
	});
	$(document).delegate(".request-late-arrival-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeLateArrivalMsg.'", type: "info"});
	});
	$(document).delegate(".request-other-msg", "click", function() {
		swal({title: "Dear Captain", html: "'.Yii::$app->helperFunctions->shortNoticeOtherMsg.'", type: "info"});
	});
	$(document).delegate(".nd-captain", "click", function() {
		swal({title: "Dear Captain", html: "if you want an In-house Captain for this trip, kindly delete your booking and book a night cruise. Best Regards", type: "info"});
	});
	$(document).delegate(".request-captain", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");

		var _targetContainer=".bca-modal";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to request a captain for this booking').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-captain','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else if(response["selection"]){
								swal({
									title: response["selection"]["heading"],
									html: response["selection"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Confirm').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/buy-captain','id'=>'']).'"+recId,
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-captain','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
								reloadGrids();
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-buy-captain", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".bca-modal";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to request a captain for this booking').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','No thank you').'",
		}).then((result) => {
			if (result.value) {
				if(recId!="" && recId!=undefined){
					App.blockUI({
						message: "'.Yii::t('app','Please wait...').'",
						target: _targetContainer,
						overlayColor: "none",
						cenrerY: true,
						boxed: true
					});
					$.ajax({
						url: "'.Url::to(['request/buy-captain','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-captain','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
							}
							App.unblockUI($(_targetContainer));
							reloadGrids();
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".waitlist-captain", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		reason=_this.data("reason");
		var _targetContainer=".bca-modal";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['request/waiting-list-captain','id'=>'']).'"+recId+"&reason="+reason,
			type: "POST",
			dataType: "json",
			success: function(response) {
				if(response["success"]){
					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
				}else{
					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
				}
				App.unblockUI($(_targetContainer));
				reloadGrids();
				reloadGrids();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".cancel-captain", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel captain').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-captain','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-captain-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-captain-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-equip", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		cityId=_this.parents(".item-row").data("city_id");
		marinaId=_this.parents(".item-row").data("marina_id");
		var _targetContainer=".bca-modal";
		swal({
			title: "Sports Equipment",
			html: "'.Yii::t('app','Please select the equipment required.').'",
			type: "info",
			input: "select",
			inputOptions: eval("equips_"+cityId+"_"+marinaId),
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				equipId=result.value;
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/sports-equipment','id'=>'']).'"+recId+"&equip_id="+equipId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-sports-equipment','id'=>'']).'"+recId+"&equip_id="+equipId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-equipment", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel Equipment').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-equipment','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-equipment-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-sports-equipment-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-bbq", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".bca-modal";
		swal({
			title: "'.Yii::t('app','Dear Captain').'",
			html: "'.Yii::$app->appHelperFunctions->bBQMsg.'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-bbq','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: "Confirmed", html: "BBQ is confirmed", type: "success", timer: 5000});
								reloadGrids();
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-bbq','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
								reloadGrids();
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".waitlist-bbq", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		reason=_this.data("reason");
		var _targetContainer=".bca-modal";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['request/waiting-list-bbq','id'=>'']).'"+recId+"&reason="+reason,
			type: "POST",
			dataType: "json",
			success: function(response) {
				if(response["success"]){
					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
				}else{
					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
				}
				App.unblockUI($(_targetContainer));
				reloadGrids();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".cancel-bbq", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel BBQ Set').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-bbq','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-bbq-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-bbq-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-wake-boarding", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".bca-modal";
		swal({
			title: "'.Yii::t('app','Dear Captain').'",
			html: "'.Yii::t('app','Are you sure you want to request a wake boarding for this booking').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-wake-boarding','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: "Confirmed", html: "Wake Boarding is confirmed", type: "success", timer: 5000});
								reloadGrids();
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-wake-boarding','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
								reloadGrids();
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".waitlist-wake-boarding", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		reason=_this.data("reason");
		var _targetContainer=".bca-modal";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['request/waiting-list-wake-boarding','id'=>'']).'"+recId+"&reason="+reason,
			type: "POST",
			dataType: "json",
			success: function(response) {
				if(response["success"]){
					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
				}else{
					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
				}
				App.unblockUI($(_targetContainer));
				reloadGrids();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".cancel-wake-boarding", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel Wake Boarding').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-wake-boarding','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-wake-boarding-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-wake-boarding-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-wake-surfing", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".bca-modal";
		swal({
			title: "'.Yii::t('app','Dear Captain').'",
			html: "'.Yii::t('app','Are you sure you want to request a wake surfing for this booking').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-wake-surfing','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: "Confirmed", html: "Wake Surfing is confirmed", type: "success", timer: 5000});
								reloadGrids();
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-wake-surfing','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
								reloadGrids();
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".waitlist-wake-surfing", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		reason=_this.data("reason");
		var _targetContainer=".bca-modal";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['request/waiting-list-wake-surfing','id'=>'']).'"+recId+"&reason="+reason,
			type: "POST",
			dataType: "json",
			success: function(response) {
				if(response["success"]){
					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
				}else{
					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
				}
				App.unblockUI($(_targetContainer));
				reloadGrids();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".cancel-wake-surfing", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel Wake Surfing').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-wake-surfing','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-wake-surfing-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-wake-surfing-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-ice", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		date=_this.data("booking_date");
		var _targetContainer=".bca-modal";
		swal({
			title: "'.Yii::t('app','Dear Captain').'",
			html: "Kindly note that ice is provided by the marina, so this is subject to avaialbility of the marina on "+date,
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Noted').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-ice','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: "Noted", html: "Your request for ice is noted.", type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-early-departure", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".bca-modal";
		swal({
			title: "'.Yii::t('app','Dear Captain').'",
			html: "Are you sure you want to request Ealry Departure for this booking?",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-early-departure','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: "Dear Captain", html: "The Captain\'s club team will not physically hand over the boat. Guidelines will be sent to you one day before the booking.<br /><br />The Captain\'s Club reserves the right to cancel this request in case of any last minute emergencies.", type: "success", confirmButtonText: "'.Yii::t('app','Noted').'"});
								reloadGrids();
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-early-departure','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
								reloadGrids();
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".waitlist-early-departure", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		reason=_this.data("reason");
		var _targetContainer=".bca-modal";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['request/waiting-list-early-departure','id'=>'']).'"+recId+"&reason="+reason,
			type: "POST",
			dataType: "json",
			success: function(response) {
				if(response["success"]){
					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
				}else{
					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
				}
				App.unblockUI($(_targetContainer));
				reloadGrids();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".cancel-early-departure", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel Early Departure').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-early-departure','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-early-departure-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-early-departure-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-overnight-camping", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".bca-modal";
		swal({
			title: "'.Yii::t('app','Dear Captain').'",
			html: "Are you sure you want to request Overnight Camping for this booking?",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-overnight-camping','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: "Confirmed", html: "Overnight Camping is confirmed", type: "success", timer: 5000});
								reloadGrids();
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-overnight-camping','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else if(response["requestIt"]){
								swal({
									title: response["requestIt"]["heading"],
									html: response["requestIt"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Request').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-overnight-camping','id'=>'']).'"+recId+"&reason="+response["requestIt"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
								reloadGrids();
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".waitlist-overnight-camping", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		reason=_this.data("reason");
		var _targetContainer=".bca-modal";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['request/waiting-list-overnight-camping','id'=>'']).'"+recId+"&reason="+reason,
			type: "POST",
			dataType: "json",
			success: function(response) {
				if(response["success"]){
					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
				}else{
					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
				}
				App.unblockUI($(_targetContainer));
				reloadGrids();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".cancel-overnight-camping", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel Late Arrival').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-overnight-camping','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-overnight-camping-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-overnight-camping-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".request-late-arrival", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".bca-modal";
		swal({
			title: "'.Yii::t('app','Dear Captain').'",
			html: "Are you sure you want to request late arrival for this booking?",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/book-late-arrival','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: "Confirmed", html: "Late Arrival is confirmed", type: "success", timer: 5000});
								reloadGrids();
							}else if(response["waitinglist"]){
								swal({
									title: response["waitinglist"]["heading"],
									html: response["waitinglist"]["msg"],
									type: "info",
									showCancelButton: true,
									confirmButtonColor: "#47a447",
									confirmButtonText: "'.Yii::t('app','Wait List').'",
									cancelButtonText: "'.Yii::t('app','No thank you').'",
								}).then((result) => {
									if (result.value) {
										App.blockUI({
											message: "'.Yii::t('app','Please wait...').'",
											target: _targetContainer,
											overlayColor: "none",
											cenrerY: true,
											boxed: true
										});
										$.ajax({
											url: "'.Url::to(['request/waiting-list-late-arrival','id'=>'']).'"+recId+"&reason="+response["waitinglist"]["reason"],
											type: "POST",
											dataType: "json",
											success: function(response) {
												if(response["success"]){
													swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
												}else{
													swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
												}
												App.unblockUI($(_targetContainer));
												reloadGrids();
											},
											error: bbAlert
										});
									}
								});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
								reloadGrids();
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".waitlist-late-arrival", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		reason=_this.data("reason");
		var _targetContainer=".bca-modal";
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['request/waiting-list-late-arrival','id'=>'']).'"+recId+"&reason="+reason,
			type: "POST",
			dataType: "json",
			success: function(response) {
				if(response["success"]){
					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
				}else{
					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
				}
				App.unblockUI($(_targetContainer));
				reloadGrids();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".cancel-late-arrival", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel Late Arrival').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-late-arrival','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$(document).delegate(".cancel-late-arrival-waiting", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel it?').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-late-arrival-waiting','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$("body").on("beforeSubmit", "form#reqKidsLifeJacketsForm", function () {
		_targetContainer=$("#reqKidsLifeJacketsForm")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
				dataType: "json",
			  success: function (response) {
					if(response["success"]){
					  swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
						window.closeModal();
						reloadGrids();
				  }else{
					  swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
				  }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
	$(document).delegate(".cancel-kids-life-jacket", "click", function() {
		_this=$(this);
		recId=_this.parents(".item-row").data("key");
		var _targetContainer=".body-content";
		swal({
			title: "Confirmation",
			html: "'.Yii::t('app','Are you sure you want to cancel Kids Life Jackets').'",
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Confirm').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
			if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(recId!="" && recId!=undefined){
					$.ajax({
						url: "'.Url::to(['request/cancel-kids-life-jacket','id'=>'']).'"+recId,
						type: "POST",
						dataType: "json",
						success: function(response) {
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
								reloadGrids();
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
						},
						error: bbAlert
					});
				}
			}
		});
	});
');
?>
<script>
function reloadGrids()
{
	$("body").removeClass("modal-open");
	$(".modal-backdrop").remove();
	$('[data-toggle="tooltip"]').tooltip();
	if($("#db-user-booking-container").length){
		$.pjax.reload({container: "#db-user-booking-container", async:false, timeout: 2000});
	}
	if($("#mem-booking-container").length){
		$.pjax.reload({container: "#mem-booking-container", async:false, timeout: 2000});
	}
	if($("#db-user-request-container").length){
		$.pjax.reload({container: "#db-user-request-container", async:false, timeout: 2000});
	}
	if($("#grid-container").length){
		$.pjax.reload({container: "#grid-container", async:false, timeout: 2000});
	}
	if($("#pjax-booking").length){
		$.pjax.reload({container: "#pjax-booking", async:false, timeout: 2000});
	}
	$(".blockUI").remove();
}
function initScripts(){
	$('[data-toggle="popover"]').popover({
		trigger: "focus",
		html: true,
	});

	if($(".dtpicker").length>0){
	  $(".dtpicker").datepicker({
	  	format: "yyyy-mm-dd",
	  	todayHighlight: true,
	  	startDate: "today",
	  }).on("changeDate", function(e){
	  	$(this).datepicker("hide");
	  });
	}
	if($(".dtrpicker").length>0){
		$(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
		$(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
			$(this).trigger("change");
		});
		$(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
	}
}
function deleteBooking(recId)
{
	var _targetContainer=".body-content";
	if($("#fb-list-view").length>0){
		var _targetContainer="#fb-list-view";
	}
	swal({
		title: "Confirmation",
		html: "<?= Yii::t('app','Are you sure you want to delete this?')?>",
		type: "info",
		showCancelButton: true,
		confirmButtonColor: "#47a447",
		confirmButtonText: "<?= Yii::t('app','Confirm')?>",
		cancelButtonText: "<?= Yii::t('app','Cancel')?>",
	}).then((result) => {
		if (result.value) {
			App.blockUI({
				message: "<?= Yii::t('app','Please wait...')?>",
				target: _targetContainer,
				overlayColor: "none",
				cenrerY: true,
				boxed: true
			});
			if(recId!="" && recId!=undefined){
				$.ajax({
					url: "<?= Url::to(['booking/delete','id'=>''])?>"+recId,
					type: "POST",
					dataType: "json",
					success: function(response) {
						App.unblockUI($(_targetContainer));
						if(response["success"]){
							swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
							reloadGrids();
						}else{
							swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
						}
					},
					error: bbAlert
				});
			}
		}
	});
}
</script>
