<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\BookingAlert;
use app\models\BookingAlertTiming;

$txtAdminPopJs='';
if(Yii::$app->user->identity->user_type==1){
	$txtAdminPopJs.='	booking_type=$("#booking_booking_type").val("");'."\n";
	$txtAdminPopJs.='	booking_comments=$("#booking_booking_comments").val("");'."\n";
	$txtAdminPopJs.='	$("#booking-comment-modal").modal();'."\n";
}

$this->registerJs('
	initScripts();
	$(document).on("pjax:success", function() {
	  initScripts();
	});
	$(document).delegate(".btn.bookIt", "click", function() {
		_this=$(this);
		if(typeof _this.data("warning_alert") !== "undefined"){
			warningDiv = _this.data("warning_alert");
			html = $("#"+warningDiv).html();
			swal({
				title: "Dear Captain",
				html: html,
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#47a447",
				confirmButtonText: "Noted, proceed with booking",
				cancelButtonText: "Cancel",
			}).then((result) => {
				if (result.value) {
					BookBoat(_this);
				}
			});
		}else{
			BookBoat(_this);
		}
	});
	$(document).delegate(".btn.spBookIt", "click", function() {
		_this=$(this);
		if(typeof _this.data("warning_alert") !== "undefined"){
			warningDiv = _this.data("warning_alert");
			html = $("#"+warningDiv).html();
			swal({
				title: "Dear Captain",
				html: html,
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#47a447",
				confirmButtonText: "Noted, proceed with booking",
				cancelButtonText: "Cancel",
			}).then((result) => {
				if (result.value) {
					bookSpecialBoatMain(_this);
				}
			});
		}else{
			bookSpecialBoatMain(_this);
		}
	});
  $(document).delegate(".cbbt", "change", function() {
  	$(".radio-custom").removeClass("active");
  	$("input[name=\"boat_time\"]:checked").parent(".radio-custom").addClass("active");
  });
');
?>
<script>
function bookSpecialBoatMain(_this){
	msgDiv = _this.data("cnfm");
	bSelection=_this.data("boat_sel");
	boatName=_this.data("boat_name");
	date=_this.data("date");
	marina_id=_this.data("marina_id");
	boat_id=_this.data("boat_id");
	html=$("#"+msgDiv);
	swal({
		title: "Dear Captain",
		html: html,
		type: "info",
		showCancelButton: true,
		confirmButtonColor: "#47a447",
		confirmButtonText: "Yes, Book now",
		cancelButtonText: "Cancel",
	}).then((result) => {
		if (result.value) {
			if(bSelection==1){
				$.ajax({
					url: "<?= Url::to(['booking/night-drive-boat-selection','source'=>'main','marina_id'=>''])?>"+marina_id+"&boat_id="+boat_id+"&date="+date,
					dataType: "html",
					success: function(data) {
						$("#general-modal").find("h5.modal-title").html("Please select boat you would like for "+boatName);
						$("#general-modal").find(".modalContent").html(data);
						$("#general-modal").modal();
					},
					error: bbAlert
				});
			}else{
				sBookBoat(_this);
			}
		}
	});
}
function initScripts(){
  $('[data-toggle="popover"]').popover();
  $(".dtpicker").datepicker({
  	format: "yyyy-mm-dd",
  	todayHighlight: true,
  	startDate: "today",
  	<?= $loggedInUser->maxPackageEndDate!=null && $loggedInUser->maxPackageEndDate!='' ? 'endDate: "'.$loggedInUser->maxPackageEndDate.'",' : ''?>
  }).on("changeDate", function(e){
  	$(this).datepicker("hide");
  });
	jQuery.ias().destroy();
  var ias = $.ias({
    container:  "#boats-list-view",
    item:       ".item",
    pagination: ".pagination",
    next:       ".next a",
    delay:      1200,
    negativeMargin:0
  });
  ias.extension(new IASSpinnerExtension({
  	html: '<div class="col-xs-12 col-sm-12 text-center"><div class="loading-message loading-message-boxed"><img src="images/loading.gif" height="25"><span>&nbsp;&nbsp;<?= Yii::t('app','Loading')?></span></div></div>',
  }));
}
function BookBoat(el)
{
	city_id=el.data("city_id");
	marina_id=el.data("marina_id");
	boat_id=el.data("boat_id");
	date=el.data("date");
	time_id=el.data("time_id");

	if(el.data('selected_boat')){
		selected_boat=el.data("selected_boat");
	}else{
		selected_boat='';
	}

	booking_type=<?php if(Yii::$app->user->identity->user_type!=0){?>$("#bookingform-booking_type").val()<?php }else{echo 0;}?>;
	booking_comments=$("#bookingform-booking_comments").val();


	myKeyVals = {"Booking[city_id]" : city_id, "Booking[port_id]" : marina_id, "Booking[boat_id]" : boat_id, "Booking[selected_boat]" : selected_boat, "Booking[booking_date]" : date, "Booking[booking_time_slot]" : time_id, "Booking[booking_type]" : booking_type, "Booking[booking_comments]" : booking_comments};
	_targetContainer=$("#grid-container")
	App.blockUI({
		message: "<?= Yii::t('app','Please wait...')?>",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});
	$.ajax({
		url: "<?= Url::to(['booking/create'])?>",
		data: myKeyVals,
		type: "post",
		dataType: "json",
		success: function(response) {
			console.log(response);
			if(response["success"]){
					window.location.href="<?= Url::to(['site/index'])?>";
			}else{
				swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error"});
			}
			App.unblockUI($(_targetContainer));
		},
		error: bbAlert
	});

}
</script>
