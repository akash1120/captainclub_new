<?php
use yii\helpers\Html;
use yii\helpers\Url;

$marinaEqupments=Yii::$app->jsFunctions->marinaEquipments;

$subBoatSelectionBoatIds = json_encode(Yii::$app->appHelperFunctions->subBoatSelectionBoatIds);

$this->registerJs($marinaEqupments.'
	initScripts();
	$(document).on("pjax:success", function() {
	  initScripts();
	});
	$(document).delegate(".btn-change-status", "click", function() {
		id=$(this).data("id");
		status=$(this).data("status");
		title=$(this).data("title");
		msg="Are you sure you want to mark this booking as "+title;
		swal({
      title: "Dear Captain",
      html: msg,
      type: "info",
      showCancelButton: true,
      confirmButtonColor: "#47a447",
      confirmButtonText: "Confirm",
      cancelButtonText: "Cancel",
		}).then((result) => {
			if (result.value) {
				_targetContainer=$("#grid-container")
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				$.ajax({
					url: "'.Url::to(['booking/activity-status','id'=>'']).'"+id+"&status="+status,
					dataType: "html",
					success: function(data) {
						if(data=="done"){
							swal({title: "Confirmed", html: "Booking marked "+title+" successfully", type: "success"});
							if($("#grid-container").length){
								$.pjax.reload({container: "#grid-container", timeout: 2000});
							}
						}
						App.unblockUI($(_targetContainer));
					},
					error: bbAlert
				});
			}
      return false;
    });
	});
	$(document).delegate("#newuserbooking-time_id", "change", function() {
    marina=$("#newuserbooking-marina_id").val();
    date=$("#newuserbooking-date").val();
    time=$("#newuserbooking-time_id").val();
  	$("#newuserbooking-selected_boat").val("0");
    loadFreeBoats(marina,date,time,".modal-content","newuserbooking-boat_id","");
	});
	$(document).delegate("#newuserbooking-boat_id", "change", function() {
  	boatId=$(this).val();
  	subBoatSelectionBoatIds='.$subBoatSelectionBoatIds.';
  	if($.inArray(boatId,subBoatSelectionBoatIds) !== -1){
  		marina_id=$("#newuserbooking-marina_id").val();
  		date=$("#newuserbooking-date").val();
  		boatName=$("#newuserbooking-boat_id option:selected" ).text();
  		fld="newuserbooking";
  		openBoatSelection(marina_id,date,boatName,fld)
  	}else{
  		$("#newuserbooking-selected_boat").val("0");
  	}
  });

	$("body").on("beforeSubmit", "form#operations-booking-form", function () {
		_targetContainer=$("#new-user-booking-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Booking saved successfully').'", type: "success"});
					  window.closeModal();
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));

          if($("#grid-container").length){
            $.pjax.reload({container: "#grid-container", timeout: 2000});
          }
			  }
		 });
		 return false;
	});
');
?>
<script>
function initScripts(){
	$('[data-toggle="popover"]').popover({
		trigger: "focus",
		html: true,
	});

	if($(".dtpicker").length>0){
	  $(".dtpicker").datepicker({
	  	format: "yyyy-mm-dd",
	  	todayHighlight: true,
	  }).on("changeDate", function(e){
	  	$(this).datepicker("hide");
	  });
	}
}
</script>
