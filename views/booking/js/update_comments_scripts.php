<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
$(document).delegate(".update-comments", "click", function() {
  id=$(this).parents("tr").data("key");
  $.ajax({
    url: "'.Url::to(['booking/update-comments','id'=>'']).'"+id,
    dataType: "html",
    success: function(data) {
      $("#general-modal").find(".modal-title").html("Update Booking Comments");
      $("#general-modal").find(".modalContent").html(data);
      $("#general-modal").modal();
    },
    error: bbAlert
  });
});
$("body").on("beforeSubmit", "form#updateCommentForm", function () {
  _targetContainer=$("#updateCommentForm")
  App.blockUI({
    message: "'.Yii::t('app','Please wait...').'",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });

   var form = $(this);
   // return false if form still have some validation errors
   if (form.find(".has-error").length) {
      return false;
   }
   // submit form
   $.ajax({
      url: form.attr("action"),
      type: "post",
      data: form.serialize(),
      success: function (response) {
        if(response=="success"){
          swal({title: "'.Yii::t('app','Updated').'", html: "'.Yii::t('app','Comments updated successfully').'", type: "success"});
          window.closeModal();
        }else{
          swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
        }
        App.unblockUI($(_targetContainer));
        reloadGrids();
      }
   });
   return false;
});
');
?>
