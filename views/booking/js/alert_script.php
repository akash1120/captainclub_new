<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
$(document).delegate(".btn-new-service-2", "click", function() {
		htmlText = "<ul style=\"text-align:left\"><li>A complimentary captain will be provided to enjoy wake boarding/ wake surfing.</li>";
		htmlText+= "<li>It is a 2 hours sessions ONLY</li>";
		htmlText+= "<li>Maximum 4 People are allowed on the boat.</li>";
		htmlText+= "<li>In case the trip was cancelled due to technical problem the boat will be replaced with another boat not necessary a wake surfing boat.</li>";
		htmlText+= "<li>The replacement boat will not be with a captain</li>";
		htmlText+= "<li>If there was no replacement available, 2 weeks will be added to your membership as per the Contract.</li></ul>";
		swal({title: "Dear Captain", html: htmlText, type: "info"});
	});

$(document).delegate(".not-today", "click", function() {
  $(this).find("input[name=\"boat_time\"]").prop("checked","");
  boatType=$(this).data("boattype");
  boatName=$(this).data("boat_name");
  msg="Sorry for the inconvenience. It is a short notice to book "+boatName+" for today.";
  $(this).removeClass("active");
  swal({
    title: "Dear Captain",
    html: msg,
    type: "info",
    confirmButtonColor: "#47a447",
    confirmButtonText: "Ok",
  });
});
$(document).delegate(".too-late", "click", function() {
  $(this).find("input[name=\"boat_time\"]").prop("checked","");
  $(this).removeClass("active");
  swal({
    title: "Dear Captain",
    html: "'.Yii::t('app','Sorry for the inconvenience. It is a short notice to book this session.').'",
    type: "info",
    confirmButtonColor: "#47a447",
    confirmButtonText: "Ok",
  });
});
$(document).delegate(".already-passed", "click", function() {
  $(this).find("input[name=\"boat_time\"]").prop("checked","");
  $(this).removeClass("active");
  swal({
    title: "Dear Captain",
    html: "'.Yii::t('app','This session is already expired. Please try another session.<br />Best Regards').'",
    type: "info",
    confirmButtonColor: "#47a447",
    confirmButtonText: "Ok",
  });
});
$(document).delegate(".latemorningbooking", "click", function() {
  $(this).find("input[name=\"boat_time\"]").prop("checked","");
  $(this).removeClass("active");
  swal({
    title: "Dear Captain",
    html: "'.Yii::t('app','Sorry for the inconvenience. It is a short notice to book this session.').'",
    type: "info",
    confirmButtonColor: "#47a447",
    confirmButtonText: "Ok",
  });
});
$(document).delegate(".no-nd-permit", "click", function() {
  $(this).find("input[name=\"boat_time\"]").prop("checked","");
  $(this).removeClass("active");
  swal({
    title: "Dear Captain",
    html: "1 hour orientation required prior to boating at night for your safety and as per the terms and conditions. Please click on the button Below to proceed.",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "Proceed",
    cancelButtonText: "Cancel",
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: "'.Url::to(['request/night-drive-training']).'",
        dataType: "html",
        success: function(data) {
          $("#general-modal").find("h5.modal-title").html("'.Yii::t('app','Request Night Drive Training').'");
          $("#general-modal").find(".modalContent").html(data);
          $("#general-modal").modal();
        },
        error: bbAlert
      });
    }
    return false;
  });
});
');
?>
