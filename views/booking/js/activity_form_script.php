<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('

$(document).delegate("#bookingactivity-fuel_cost", "blur", function() {
	_t=$(this);
	val=parseInt(_t.val());
	if(val<'.$minPetrolConsumption.'){
		swal({title: "Alert", html: "Minimum Petrol consumption should not be below AED'.$minPetrolConsumption.'", type: "info"});
		_t.val('.$minPetrolConsumption.');
	}
});
$(document).delegate(".texp", "click", function() {
	_t=$(this);
	if(_t.val()==1){
		$("#tripExpCom input").val("");
		$("#tripExpCom").hide();
	}else{
		$("#tripExpCom").show();
	}
	$("#bookingactivity-hide_other").val(0);
});
$("body").on("change", "#bookingactivity-member_exp", function () {
	if($(this).is(":checked")) {
		$("#lateArrInfo").show();
	}else{
		$("#lateArrInfo").hide();
	}
	$("#bookingactivity-hide_other").val(0);
});
$("body").on("change", "#bookingactivity-dui", function () {
	if($(this).is(":checked")) {
		$("#duiInfo").show();
	}else{
		$("#duiInfo").hide();
	}
	$("#bookingactivity-hide_other").val(0);
});
'.($modelBooking->booking_exp==1 ? '
$(document).delegate("#bookingactivity-boat_provided", "change", function() {
	_t=$(this);
	if(_t.val()!='.$modelBooking->boat_id.'){
		swal({title: "Information", html: "Please select the Reason of chaning the boat, Is it Request or Boat Not Available", type: "info", timer: 5000});
		$("input:radio[name=\"BookingActivity[booking_exp]\"]").attr("checked", false);
		$("input:radio[name=\"BookingActivity[booking_exp]\"]").attr("disabled", false);
		$("input:radio#bExp1").attr("disabled", true);
	}else{
		$("input:radio[name=\"BookingActivity[booking_exp]\"]").attr("checked", false);
		$("input:radio[name=\"BookingActivity[booking_exp]\"]").attr("disabled", true);
		$("input:radio[name=\"BookingActivity[booking_exp]\"][value=\"'.$modelBooking->booking_exp.'\"]").attr("checked", true).attr("disabled", false);
	}
});
' : '').'
$(document).delegate("#bookingactivity-fuel_chargeable", "change", function() {
	_t=$(this);
	if(_t.val()==1){
		$("#fuel-cost-row").show();
		$("#no-charge-comments").hide();
	}else{
		$("#fuel-cost-row").hide();
		$("#bookingactivity-fuel_cost").val("0");
		$("#no-charge-comments").show();
	}
});
$(document).delegate("#bookingactivity-dep_engine_hours", "blur", function() {
	checkEngineHoursOnBlur();
});
$(document).delegate("#bookingactivity-arr_engine_hours", "blur", function() {
	checkEngineHoursOnBlur();
});

$("body").on("click", ".reset-sign", function () {
	$("#bookingactivity-"+$(this).data("iid")+"_sign").val("");
	$("#"+$(this).data("sdid")+"_sign").jSignature("reset");
	$("#bookingactivity-"+$(this).data("sdid")+"_sign_base30").val("");
});
$("body").on("beforeSubmit", "form#inoutForm", function () {
	 var form = $(this);
	 // return false if form still have some validation errors
	 if (form.find(".has-error").length) {
		  return false;
	 }
	 // submit form

	 '.($model->dep_crew_sign=='' ? 'var isSignatureProvided1=$("#depCrew_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	 if(isSignatureProvided1){
		 var deptStaffSign = $("#depCrew_sign").jSignature("getData","svgbase64");
		 $("#bookingactivity-dep_crew_sign").val("data:"+deptStaffSign[0]+","+deptStaffSign[1]);
	 }' : '').'
	 '.($model->dep_mem_sign=='' ? 'var isSignatureProvided2=$("#depMem_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	 if(isSignatureProvided2){
		 var deptMemSign = $("#depMem_sign").jSignature("getData","svgbase64");
		 $("#bookingactivity-dep_mem_sign").val("data:"+deptMemSign[0]+","+deptMemSign[1]);
	 }' : '').'
	 '.($model->arr_crew_sign=='' ? 'var isSignatureProvided3=$("#arrCrew_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	 if(isSignatureProvided3){
		 var ArrStaffSign = $("#arrCrew_sign").jSignature("getData","svgbase64");
		 $("#bookingactivity-arr_crew_sign").val("data:"+ArrStaffSign[0]+","+ArrStaffSign[1]);
	 }' : '').'
	 '.($model->arr_mem_sign=='' ? 'var isSignatureProvided4=$("#arrMem_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	 if(isSignatureProvided4){
		 var ArrMemSign = $("#arrMem_sign").jSignature("getData","svgbase64");
		 $("#bookingactivity-arr_mem_sign").val("data:"+ArrMemSign[0]+","+ArrMemSign[1]);
	 }' : '').'

	 return true;
});
$(document).delegate("#bookingactivity-hide_other", "change", function() {
if($(this).val()==1){
	'.($model->dep_crew_sign=='' ? 'var isSignatureProvided=$("#depCrew_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	if(isSignatureProvided){
		var deptStaffSign = $("#depCrew_sign").jSignature("getData","svgbase64");
		$("#bookingactivity-dep_crew_sign").val("data:"+deptStaffSign[0]+","+deptStaffSign[1]);
	}' : '').'
	'.($model->dep_mem_sign=='' ? 'var isSignatureProvided=$("#depMem_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	if(isSignatureProvided){
		var deptMemSign = $("#depMem_sign").jSignature("getData","svgbase64");
		$("#bookingactivity-dep_mem_sign").val("data:"+deptMemSign[0]+","+deptMemSign[1]);
	}' : '').'
	'.($model->arr_crew_sign=='' ? 'var isSignatureProvided=$("#arrCrew_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	if(isSignatureProvided){
		var ArrStaffSign = $("#arrCrew_sign").jSignature("getData","svgbase64");
		$("#bookingactivity-arr_crew_sign").val("data:"+ArrStaffSign[0]+","+ArrStaffSign[1]);
	}' : '').'
	'.($model->arr_mem_sign=='' ? 'var isSignatureProvided=$("#arrMem_sign").jSignature("getData","base30")[1].length>1 ? true : false;
	if(isSignatureProvided){
		var ArrMemSign = $("#arrMem_sign").jSignature("getData","svgbase64");
		$("#bookingactivity-arr_mem_sign").val("data:"+ArrMemSign[0]+","+ArrMemSign[1]);
	}' : '').'

	missingMsg=checkFld("dep_time","Please enter Departure Time");

	if(missingMsg=="")missingMsg=checkFld("arr_time","Please enter Arrival Time");
	if(missingMsg=="")missingMsg=checkFld("dep_engine_hours","Please enter Departure Engine Hours");
	if(missingMsg=="")missingMsg=checkFld("arr_engine_hours","Please enter Arrival Engine Hours");
	if(missingMsg=="")missingMsg=checkEngineHours();
	if(missingMsg=="")missingMsg=checkFld("dep_ppl","Please enter Departure No. of People");
	if(missingMsg=="")missingMsg=checkFld("arr_ppl","Please enter Arrival No. of People");
	if(missingMsg=="")missingMsg=checkFld("dep_fuel_level","Please enter Departure Fuel Level");
	if(missingMsg=="")missingMsg=checkFld("arr_fuel_level","Please enter Arrival Fuel Level");

	'.(in_array($modelBooking->boat_id,$marinaStayInIds) ? '' : '
		if($("#bookingactivity-fuel_chargeable").val()==1){
			if(missingMsg=="")missingMsg=checkFld("fuel_cost","Please enter Fuel Cost");
		}else{
			if(missingMsg=="")missingMsg=checkFld("no_charge_comment","Please specify reason why fuel is not chargeable");
		}
	').'
	'.($model->booking->port_id==Yii::$app->params['emp_id'] ? '
	if($("#bookingactivity-fuel_chargeable").val()==1){
		if(missingMsg=="")missingMsg=checkFileFld("bill_image","Please upload bill");
	}
	' : '').'


	if(missingMsg=="")missingMsg=checkFld("dep_crew_sign","Please enter Departure Crew Signature");
	if(missingMsg=="")missingMsg=checkFld("dep_mem_sign","Please enter Departure Member Signature");
	if(missingMsg=="")missingMsg=checkFld("arr_crew_sign","Please enter Arrival Crew Signature");
	if(missingMsg=="")missingMsg=checkFld("arr_mem_sign","Please enter Arrival Member Signature");

	if(missingMsg=="")missingMsg=checkRadioSelection("BookingActivity[booking_exp]","Please select Booking Status");
	if(missingMsg=="")missingMsg=checkRadioSelection("BookingActivity[trip_exp]","Please select Trip Status");

	if(missingMsg=="")missingMsg=checkTipExperience();
	if(missingMsg=="")missingMsg=checkMemberExperience();

	if(missingMsg!=""){
		swal({title: "Information", html: missingMsg, type: "info", timer: 5000});
		$(this).val(0);
	}else{
		swal({title: "Information", html: "Are you sure, all the data filled is final, as you wont be able to modify it after saving this.", type: "info"});
	}
}
});
');
?>
<script>
function checkFld(fld,msg)
{
	if($("#bookingactivity-"+fld).val()==""){
		$(".field-bookingactivity-"+fld).addClass("has-error").removeClass("has-success");
		$(".field-bookingactivity-"+fld).find(".help-block").html(msg);
		return msg;
	}
	return "";
}
function checkFileFld(fld,msg)
{
	//Check if image is already uploaded
	if($("#bill-img-container").length){

	}else{
		if($("#bookingactivity-"+fld).val()==""){
			$(".field-bookingactivity-"+fld).addClass("has-error").removeClass("has-success");
			$(".field-bookingactivity-"+fld).find(".help-block").html(msg);
			return msg;
		}
	}
	return "";
}
function checkRadioSelection(fld,msg)
{
	if($("input[name=\""+fld+"\"]:checked").val()=="" || $("input[name=\""+fld+"\"]:checked").val()==undefined){
		$("input[name=\""+fld+"\"]:checked").parent("td").addClass("has-error").removeClass("has-success");
		return msg;
	}
	return "";
}
function checkTipExperience()
{
	if($("input[name=\"BookingActivity[trip_exp]\"]:checked").val()!=1){
		$("#tripExpCom").show();
		if($("#bookingactivity-trip_exp_comments").val()==""){
			$(".field-bookingactivity-trip_exp_comments").addClass("has-error").removeClass("has-success");
			return "Please specify trip experience details";
		}
	}
	return "";
}
function checkMemberExperience()
{
	if($("input[name=\"BookingActivity[member_exp]\"]:checked").val()==1){
		$("#lateArrInfo").show();

		if($("#bookingactivity-member_exp_late_hr").val()==""){
			$(".field-bookingactivity-member_exp_late_hr").addClass("has-error").removeClass("has-success");
			return "Please specify late hour(s)";
		}
		if($("#bookingactivity-member_exp_late_min").val()==""){
			$(".field-bookingactivity-member_exp_late_min").addClass("has-error").removeClass("has-success");
			return "Please specify late minute(s)";
		}

		if($("#bookingactivity-member_exp_comments").val()==""){
			$(".field-bookingactivity-member_exp_comments").addClass("has-error").removeClass("has-success");
			return "Please specify late arrival reason";
		}
	}
	if($("input[name=\"BookingActivity[dui]\"]:checked").val()==1){
		if($("#bookingactivity-dui_comments").val()==""){
			$(".field-bookingactivity-dui_comments").addClass("has-error").removeClass("has-success");
			return "Please enter DUI remarks";
		}
	}
	return "";
}
function checkEngineHours()
{
	depVal=parseFloat($("#bookingactivity-dep_engine_hours").val());
	arrVal=parseFloat($("#bookingactivity-arr_engine_hours").val());
	if(arrVal<depVal){
		msg="Arrival Engine hours can not be less then departure hours";
		$("#bookingactivity-arr_engine_hours").parent(".form-group").addClass("has-error").removeClass("has-success");
		$("#bookingactivity-arr_engine_hours").parent(".form-group").find(".help-block").html(msg);
		//return msg;
	}
	return "";
}
</script>
