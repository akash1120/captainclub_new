<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="card bg-primary mb-3">
  <div class="card-header black-background text-center"><span class="card-header-custom yellow-font"> <?= Yii::$app->formatter->asDate($model->booking_date)?></span>
  </div>
  <div class="card-body card-body-custom">
    <div class="row p5">
      <div class="col-8 col-sm-8 p0">
        <div class="col-sm-12 p0 pb20"><i class="fas fa-map-marker yellow-font"></i> <?= $model->marina->short_name?></div>
        <div class="col-sm-12 p0 pb10"><i class="fas fa-clock yellow-font"></i> <?= $model->timeZone->dashboard_name?> <span class="small-font">(<?= Yii::$app->helperFunctions->getShortAmPmTime($model->timeZone->start_time).' - '.Yii::$app->helperFunctions->getShortAmPmTime($model->timeZone->end_time)?>)</span></div>
      </div>
      <div class="col-4 col-sm-4 p0">
        <?= $model->addonsPopupButton?>
        <?php
        $showDeleteBtn=true;
        if($model->user_id==Yii::$app->user->identity->id){
          if($model->booking_date==date("Y-m-d") && strtotime(date("H:i:s"))>=strtotime($model->timeZone->start_time)){
            $showDeleteBtn=false;
          }
        }else{
          $showDeleteBtn=false;
        }
        if($model->status!=1){
          $showDeleteBtn=false;
        }
        if($showDeleteBtn==true){
        ?>
        <div class="col-sm-12 p0">
          <div class="btn-containers">
            <button onclick="javascript:deleteBooking(<?= $model->id?>);" class="p-1 bg-red-600 text-white rounded shadow-lg btn-containers hover:shadow-none hover:bg-red-400 text-xs">
              <i class="fas fa-times-circle text-white"></i> <?= Yii::t('app','Delete');?>
            </button>
          </div>
        </div>
        <?php }?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 p0 pb10"><i class="fas fa-anchor yellow-font"></i>
        <?= $model->boatName.$model->boat->purpose?>
        <a href="javascript:;" class="load-modal" data-url="<?= Url::to(['suggestion/boat-info','id'=>$model->boat_id])?>" data-heading="Boat Info">
          <i class="fas fa-info-circle grey-font"></i>
        </a>
      </div>
    </div>
    <?= $model->addonsRequestedHtml?>
  </div>
</div>
