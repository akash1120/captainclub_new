<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->registerJs('
$(".sdtpicker").datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
  startDate: "today",
}).on("changeDate", function(e){
  $(this).datepicker("hide");
});
$(".lookup-member").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/member-search']).'",
  noCache: true,
    onSelect: function(suggestion) {
      if($("#newuserbooking-user_id").val()!=suggestion.data){
        $("#newuserbooking-user_id").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
        $("#newuserbooking-user_id").val("");
    }
});
')
?>
<?php $form = ActiveForm::begin(['id'=>'new-user-booking-form']); ?>
<div class="hidden">
  <?= $form->field($model, 'selected_boat')->textInput() ?>
  <?= $form->field($model, 'user_id')->textInput() ?>
</div>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'class' => 'form-control lookup-member', 'placeholder'=>$model->getAttributeLabel('username')]) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'date')->textInput(['maxlength' => true, 'class' => 'form-control sdtpicker', 'autocomplete'=>'off', 'placeholder'=>$model->getAttributeLabel('date')]) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->activeCityListArr,['prompt'=>Yii::t('app','Select')]) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'marina_id')->dropDownList([]) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'time_id')->dropDownList(Yii::$app->appHelperFunctions->timeSlotListDashboardArr) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'boat_id')->dropDownList([]) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'booking_type')->dropDownList(Yii::$app->helperFunctions->adminBookingTypes) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'comments')->textInput(['class'=>'form-control']) ?>
    </div>
  </div>

  <div class="form-group">
    <?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
  </div>
<?php ActiveForm::end(); ?>
