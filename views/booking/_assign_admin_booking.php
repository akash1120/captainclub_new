<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id'=>'assign-booking-form']); ?>
<div style="display:none;">
	<?= $form->field($model, 'to_user_id')->textInput() ?>
</div>
<div class="row">
	<div class="col-sm-6"><strong>Admin:</strong> <?= $bookingModel->member->fullname?></div>
	<div class="col-sm-6"><strong>City:</strong> <?= $bookingModel->city->name?></div>
</div>
<div class="row">
	<div class="col-sm-6"><strong>Marina:</strong> <?= $bookingModel->marina->name?></div>
	<div class="col-sm-6"><strong>Boat:</strong> <?= $bookingModel->boat->name?></div>
</div>
<div class="row">
	<div class="col-sm-6"><strong>Date:</strong> <?= $bookingModel->booking_date?></div>
	<div class="col-sm-6"><strong>Time:</strong> <?= $bookingModel->timeZone->name?></div>
</div>
<div class="row">
	<div class="col-sm-12"><strong>Comments:</strong> <?= Yii::$app->helperFunctions->adminBookingTypes[$bookingModel->booking_type]?> - <?= $bookingModel->booking_comments?></div>
</div>
<hr />
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'member_name')->textInput(['maxlength' => true, 'class' => 'form-control lookup-assign-member', 'placeholder'=>$model->getAttributeLabel('member_name')]) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'comments')->textInput() ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
