<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use app\components\widgets\CustomPjax;
use app\models\UserNightPermit;
use app\models\Booking;
use app\models\BookingAlert;
use app\models\BookingAlertTiming;
use app\assets\BookingCreateAssets;
BookingCreateAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Booking */

$loggedInUser=Yii::$app->user->identity;
$nightDrivePermit=false;
$nightDrivePermission=UserNightPermit::find()->where(['user_id'=>$loggedInUser->id,'port_id'=>$searchModel->port_id])->asArray()->one();
if($nightDrivePermission!=null){
  $nightDrivePermit=true;
}


$this->title = Yii::t('app', 'New Booking');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
$("body").on("click", ".btn.view-type", function () {
  $(".btn.view-type").removeClass("active");
  $(this).addClass("active");
  act=$(this).data("type");
  if(act==1){
    $(".item.isFullBooked").removeClass("showIt");
  }else{
    $(".item.isFullBooked").addClass("showIt");
  }
});
$("body").on("change", "#bookingform-city_id", function () {
  searchBoats();
});
$("body").on("change", "#bookingform-date", function () {
  searchBoats();
});
');

$fldCols=6;
$btnCol=6;
if(Yii::$app->user->identity->user_type!=0){
  $fldCols=3;
  $btnCol=3;
}

if(Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->canBookBoat==false){
  $userCredits=Yii::$app->user->identity->profileInfo->credit_balance;
?>
<section class="card">
  <header class="card-header">
		<h2 class="card-title">Dear Captain,</h2>
	</header>
  <div class="card-body">
    Your current balance is <span class="badge badge-danger"><?= $userCredits?></span> AED. Please deposit funds by clicking <a href="javascript:;" class="load-modal" data-dismiss="modal" data-url="<?= Url::to(['order/create'])?>" data-heading="Deposit Funds">here</a>.<br /><br />
    Enjoy Boating With Relief
  </div>
  <div class="card-footer clearfix">
		<a class="btn btn-primary pull-right load-modal" href="javascript:;" data-dismiss="modal" data-url="<?= Url::to(['request/boat-booking'])?>" data-heading="Boat Booking">Submit Booking Request</a>
	</div>
</section>
<?= $this->render('/order/js/scripts')?>
<?php
}else{
?>
<script>
function searchBoats()
{
  if($("#bookingform-city_id").val()!='' && $("#bookingform-date").val()!=''){
    $("#btnSearch").trigger('click');
    //document.getElementById("createBookingForm").submit();
  }
}
</script>
<style>
@media only screen and (max-width: 767px) {
.col-xs-3.img-col {
width: 33.33333333%;
padding-right: 5px;
}
.col-xs-9.detail-col {
width: 66.66666667%;
padding-left: 5px;
}
#boats-list-view .h3 {
font-size: 15px;
font-weight:bold;
}
}
.sticky {
  position: -webkit-sticky;
  position: sticky;
  top: 0;
}
.sticky:before,
.sticky:after {
  content: '';
  display: table;
}
</style>
<div class="booking-create">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>

  <?php
  $holdAlerts=BookingAlertTiming::find()
  ->select([
  	BookingAlertTiming::tableName().'.time_slot_id',
  	BookingAlert::tableName().'.message',
  ])
  ->innerJoin(BookingAlert::tableName(),BookingAlert::tableName().".id=".BookingAlertTiming::tableName().".alert_id")
  ->where([
  	'alert_type'=>2,
  	'alert_date'=>$searchModel->date,
  	'port_id'=>$searchModel->port_id,
  	'trashed'=>0,
  ])
  ->asArray()->all();
  if($holdAlerts!=null){
  	foreach($holdAlerts as $holdAlert){
  		$this->registerJs('
  		$(document).delegate(".hold-alert_'.$holdAlert['time_slot_id'].'", "click", function() {
  		   $(".radio-custom").removeClass("active");
    		 $(this).find("input[name=\"boat_time\"]").prop("checked","");
  			swal({
  					title: "Dear Captain",
  					html: "'.preg_replace("/\r|\n/","",nl2br($holdAlert['message'])).'",
  					type: "info",
  					showCancelButton: false,
  					confirmButtonColor: "#47a447",
  					confirmButtonText: "Noted",
  			});
  		});
  		');
  	}
  }
  ?>
  <div style="display:none;">
  <?php
  $warningAlerts=BookingAlertTiming::find()
  ->select([
  	BookingAlertTiming::tableName().'.time_slot_id',
  	BookingAlert::tableName().'.message',
  ])
  ->innerJoin(BookingAlert::tableName(),BookingAlert::tableName().".id=".BookingAlertTiming::tableName().".alert_id")
  ->where([
  	'alert_type'=>1,
  	'alert_date'=>$searchModel->date,
  	'port_id'=>$searchModel->port_id,
  	'trashed'=>0,
  ])
  ->asArray()->all();
  if($warningAlerts!=null){
  	foreach($warningAlerts as $warningAlert){
      echo '<div id="warning_'.$warningAlert['time_slot_id'].'">
        '.preg_replace("/\r|\n/","",nl2br($warningAlert['message'])).'
      </div>';
  	}

  }
  foreach(Yii::$app->helperFunctions->specialBoatTypeMessage as $key=>$val){
    echo '<div id="sp_'.$key.'">'.$val.'</div>';
  }
  ?>
  </div>
  <section class="card mb-1">
    <div class="card-body filter-padding">
      <?php $form = ActiveForm::begin(['id'=>'createBookingForm','action'=>Url::to(['booking/create']),'method'=>'get','options'=>['data-pjax' => '']]); ?>
      <div class="row top-filter-row">
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr,['prompt'=>Yii::t('app','Select')])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'date')->textInput(['maxlength' => true, 'class'=>'form-control dtpicker', 'readonly'=>'readonly', 'autocomplete'=>'off', 'placeholder'=>$searchModel->getAttributeLabel('date')])->label(false)?>
        </div>
        <?php if(Yii::$app->user->identity->user_type!=0){?>
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'booking_type')->dropDownList(Yii::$app->helperFunctions->adminBookingTypes,['prompt'=>Yii::t('app','Select')])->label(false)?>
        </div>
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'booking_comments')->textInput(['maxlength' => true, 'autocomplete'=>'off', 'placeholder'=>$searchModel->getAttributeLabel('booking_comments')])->label(false)?>
        </div>
        <?php }?>
        <div class="col-sm-6" style="display:none;">
          <?= Html::submitButton('<i class="fas fa-search"></i> '.Yii::t('app', 'Search'), ['id'=>'btnSearch', 'class' => 'btn btn-success btn-block']) ?>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </section>
    <?php if($searchModel->city_id!=null && $searchModel->date!=null && $searchModel->date>=date("Y-m-d")){?>
    <div class="tabs">
      <div class="sticky">
        <ul class="nav nav-tabs nav-justified">
          <?= $searchModel->tabItems?>
        </ul>
      </div>
      <?php if(1==1){?>
      <div class="tab-content">
        <div class="tab-pane active" style="padding-bottom:15px;">
          <div class="row">
            <?php
            $timeSlots=$searchModel->timeSlotsArray;
            if($timeSlots!=null){
              foreach($timeSlots as $timeSlot){
                $alertCls='';
                $availableBoats=$timeSlot['availableBoats'];
                if($availableBoats!=null && count($availableBoats)>0){
                  if($timeSlot['id']==1){
                    if($searchModel->date==date("Y-m-d",strtotime("+1 day",strtotime(date("Y-m-d"))))){
                      if(date("H")>date("H",mktime(Yii::$app->helperFunctions->lateMroningCheckTill,0,0,date("m"),date("d"),date("Y")))){
                        $checkMorningBookingExists=Booking::find()
                        ->where([
                          'and',
                          [
                            'city_id'=>$searchModel->city_id,
                            'port_id'=>$searchModel->port_id,
                            'DATE(booking_date)'=>$searchModel->date,
                            'booking_time_slot'=>$timeSlot['id'],
                            'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,
                            'is_bulk'=>0,
                            'trashed'=>0,
                          ],
                          ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
                        ]);
                        if(!$checkMorningBookingExists->exists()){
                          $alertCls=' latemorningbooking';
                        }
                      }
                    }elseif($searchModel->date==date("Y-m-d")){
                      if($timeSlot['available_till']!=null && date("H:i")<=$timeSlot['available_till']){
                        $checkMorningBookingExists=Booking::find()
                        ->where([
                          'and',
                          [
                            'city_id'=>$searchModel->city_id,
                            'port_id'=>$searchModel->port_id,
                            'DATE(booking_date)'=>$searchModel->date,
                            'booking_time_slot'=>$timeSlot['id'],
                            'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,
                            'is_bulk'=>0,
                            'trashed'=>0,
                          ],
                          ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
                        ]);
                        if(!$checkMorningBookingExists->exists()){
                          $alertCls=' latemorningbooking';
                        }
                      }
                    }
                  }

                  $holdAlert=BookingAlert::find()
                  ->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
                  ->where([
                    'alert_type'=>2,
                    'alert_date'=>$searchModel->date,
                    'port_id'=>$searchModel->port_id,
                    BookingAlertTiming::tableName().'.time_slot_id'=>$timeSlot['id'],
                  	'trashed'=>0,
                  ])
                  ->asArray()->one();
                  if($holdAlert!=null){
                    $addiotinalOptions['data-hold_alert']='hold_'.$timeSlot['id'];
                    $alertCls=' hold-alert_'.$timeSlot['id'];
                  }

                  $warninglert=BookingAlert::find()
                  ->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
                  ->where([
                    'alert_type'=>1,
                    'alert_date'=>$searchModel->date,
                    'port_id'=>$searchModel->port_id,
                    BookingAlertTiming::tableName().'.time_slot_id'=>$timeSlot['id'],
                  	'trashed'=>0,
                  ])
                  ->asArray()->one();
                  if($warninglert!=null){
                    $addiotinalOptions['data-warning_alert']='warning_'.$timeSlot['id'];
                  }

                  if($searchModel->date==date("Y-m-d") && $timeSlot['available_till']!=null && date("H:i")>=$timeSlot['available_till']){
                    $alertCls=' too-late';
                  }
                  if($searchModel->date==date("Y-m-d") && $timeSlot['expiry']!=null && date("H:i")>=$timeSlot['expiry']){
                    $alertCls=' already-passed';
                  }
            ?>
            <div class="col-sm-12">
              <section class="card card-featured card-featured-warning mb-1 mt-1">
                <div class="card-header">
                  <h2 class="card-title"><?= $timeSlot['title']?></h2>
                  <div class="card-actions">
    								<a href="javascript:;" class="card-action card-action-toggle" data-card-toggle=""></a>
    							</div>
                </div>
                <div class="card-body">
                  <?php
                  foreach($availableBoats as $availableBoat){

                    if($timeSlot['permit_required']==1){
                      if($nightDrivePermit==false){
                        $alertCls=' no-nd-permit';
                      }
                    }

                    $bookingCls=' bookIt';
                    if($availableBoat['special_boat']==1){
                      $bookingCls=' spBookIt';
                      $addiotinalOptions['id']='spboat_'.$availableBoat['boat_id'];
                      $addiotinalOptions['data-cnfm']='sp_'.$availableBoat['special_boat_type'];
                      $addiotinalOptions['data-boat_sel']=$availableBoat['boat_selection'];
                      $addiotinalOptions['data-boat_name']=$availableBoat['name'];
                      $addiotinalOptions['data-boat_id']=$availableBoat['boat_id'];
                    }

                    $addiotinalOptions['class']='btn btn-xs btn-success '.($alertCls=='' ? $bookingCls : '').' time_'.$timeSlot['id'];
                    $addiotinalOptions['data-city_id']=$searchModel->city_id;
                    $addiotinalOptions['data-marina_id']=$searchModel->port_id;
                    $addiotinalOptions['data-date']=$searchModel->date;
                    $addiotinalOptions['data-time_id']=$timeSlot['id'];
                    $addiotinalOptions['data-boat_id']=$availableBoat['boat_id'];
                    $btn=Html::a(
                      Yii::t('app','Book').($availableBoat['special_boat']==1 ? ' '.$availableBoat['name'] : ''),
                      'javascript:;',
                      $addiotinalOptions
                    );
                    $boatPurpose=Yii::$app->appHelperFunctions->getBoatPurposeById($availableBoat['boat_purpose_id']);
                  ?>
                  <?php if($timeSlot['is_special']==1){?>
                    <div class="sp-boat <?= $alertCls?>" data-boattype="sp" data-boat_name="<?= $availableBoat['name']?>">
                      <?= $btn?> (<?= Yii::$app->helperFunctions->getShortAmPmTime($availableBoat['start_time']).' - '.Yii::$app->helperFunctions->getShortAmPmTime($availableBoat['end_time'])?>)
                    </div>
                  <?php }else{?>


                          <style>
                              @media only screen and (max-device-width: 480px) {

                                  .b_title, .b_title_speed {
                                      clear: both;
                                  }

                                  .radio-custom label:before {
                                        top:20%
                                  }
                                  .radio-custom input[type="radio"]:checked + label:after {
                                      top: 20%;

                                  }
                              }

                          </style>


                  <div class="radio-custom clearfix radio-success<?= $alertCls?>" data-boattype="nrml" data-boat_name="<?= $availableBoat['name']?>">
                    <input type="radio" name="boat_time" class="cbbt" id="boat_time_<?= $availableBoat['boat_id'].'_'.$availableBoat['time_slot_id']?>" />
                    <label class="float-left i-item" for="boat_time_<?= $availableBoat['boat_id'].'_'.$availableBoat['time_slot_id']?>">
                      <span class="b_title"><?= $availableBoat['name'].'</span> <span class="b_title_speed">'.($boatPurpose!=null ? ' - '.$boatPurpose['title'] : '').' ('.Yii::$app->helperFunctions->getShortAmPmTime($availableBoat['start_time']).' - '.Yii::$app->helperFunctions->getShortAmPmTime($availableBoat['end_time']).')</span>'?>
                    </label>
                    <div class="float-left i-item">
                    <a href="javascript:;" class="load-modal" data-url="<?= Url::to(['suggestion/boat-info','id'=>$availableBoat['boat_id']])?>" data-heading="Boat Info">
                      <i class="fas fa-info-circle grey-font"></i>
                    </a>
                    </div>
                    <?php if($availableBoat['special_boat']==2){?>
                      <div class="float-left i-item">
                       <span class="badge badge-success btn-new-service-2 cursor-pointer">New Service</span>
                      </div>
                    <?php }?>
                    <div class="float-left i-item bookbtn"><?= $btn?></div>
                  </div>
                  <?php }}?>
                </div>
              </section>
            </div>
            <?php }}}//?>
          </div>
        </div>
        <center>
          <?= Html::button('<i class="fas fa-mail-bulk"></i> '.Yii::t('app', 'Request'), ['class' => 'btn btn-info', 'data-toggle'=>'modal','data-target'=>'#request-modal']) ?>
          <?= Html::a('<i class="fas fa-chevron-left"></i> '.Yii::t('app', 'Cancel'), ['site/index'], ['class' => 'btn btn-default', 'data-pjax'=>'0']) ?>
        </center>
      </div>
      <?php }?>
    </div>
    <?php }?>
  <?php CustomPjax::end(); ?>
</div>
<?php
}
Modal::begin([
'headerOptions' => ['class' => 'modal-header'],
'id' => 'booking-comment-modal',
'size' => 'modal-md',
'header' => '<h5 class="modal-title">Reason</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
'closeButton' => false,
]);
$adminBookingTypes[0]=Yii::t('app','Select');
$abtArr=Yii::$app->helperFunctions->adminBookingTypes;
if($abtArr!=null){
  foreach ($abtArr as $key=>$val){
    $adminBookingTypes[$key]=$val;
  }
}

echo '<div class="modalContent">';
echo '  <div class="form-group required">';
echo '    <label class="control-label" for="booking_booking_type">Type</label>';
echo '    '.Html::dropDownList('booking_type', 0, $adminBookingTypes, ['id'=>'booking_booking_type','class'=>'form-control']);
echo '    <div class="help-block"></div>';
echo '  </div>';
echo '  <div class="form-group required">';
echo '    <label class="control-label" for="booking_booking_comments">Comments</label>';
echo '    '.Html::textInput('booking_comments', "", ['id'=>'booking_booking_comments','class'=>'form-control']);
echo '    <div class="help-block"></div>';
echo '  </div>';
echo '  <div class="row">';
echo '    <div class="col-md-12">';
echo '      <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>';
echo '    </div>';
echo '  </div>';
echo "</div>";
Modal::end();
?>
<?= $this->render('/request/selection',['user'=>Yii::$app->user->identity])?>
<?= $this->render('/request/js/selection_scripts')?>
<?= $this->render('/booking/js/alert_script')?>
<?= $this->render('/booking/js/booking_script',['loggedInUser'=>$loggedInUser])?>
<?= $this->render('/booking/js/alert_script')?>
<?= $this->render('/request/js/freeze_scripts')?>
<?= $this->render('/request/js/other_request_scripts')?>
<?= $this->render('/request/js/night_drive_training_request_scripts')?>
<?= $this->render('/request/js/package_city_restriction_script')?>
<?= $this->render('/request/js/boat_booking_scripts')?>
