<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id'=>'updateCommentForm']); ?>
<div class="row">
	<div class="col-sm-6"><strong>Member:</strong> <?= $model->booking->member->username?></div>
	<div class="col-sm-6"><strong>City:</strong> <?= $model->booking->city->name?></div>
</div>
<div class="row">
	<div class="col-sm-6"><strong>Marina:</strong> <?= $model->booking->marina->name?></div>
	<div class="col-sm-6"><strong>Boat:</strong> <?= $model->booking->boat->name?></div>
</div>
<div class="row">
	<div class="col-sm-6"><strong>Date:</strong> <?= $model->booking->booking_date?></div>
	<div class="col-sm-6"><strong>Time:</strong> <?= $model->booking->timeZone->name?></div>
</div>
<div class="row">
	<div class="col-sm-12"><strong>Old Comments:</strong> <?= (isset(Yii::$app->helperFunctions->adminBookingTypes[$model->booking->booking_type]) ? Yii::$app->helperFunctions->adminBookingTypes[$model->booking->booking_type].' - ' : '').$model->booking->booking_comments?></div>
</div>
<hr />
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'new_booking_type')->dropDownList(Yii::$app->helperFunctions->adminBookingTypes) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'new_booking_comments')->textInput() ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
