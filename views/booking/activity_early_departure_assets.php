<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$photo=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$model['photo'],'tiny');
$batterySwitchPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$model['battery_switch'],'tiny');
$registrationCardLocPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$model['registration_card_location'],'tiny');
$keysOfBoatPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$model['keys_of_the_boat'],'tiny');
$petroGuagePhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$model['petro_guage'],'tiny');
?>
<?php $form = ActiveForm::begin(['id'=>'updateEarlyDepartureAssets','options'=>['enctype'=>'multipart/form-data']]); ?>
<div class="row">
	<div class="col-sm-6"><strong>Member:</strong> <?= $model->booking->member->fullname?></div>
	<div class="col-sm-6"><strong>City:</strong> <?= $model->booking->city->name?></div>
</div>
<div class="row">
	<div class="col-sm-6"><strong>Marina:</strong> <?= $model->booking->marina->name?></div>
	<div class="col-sm-6"><strong>Boat:</strong> <?= $model->booking->boat->name?></div>
</div>
<div class="row">
	<div class="col-sm-6"><strong>Date:</strong> <?= $model->booking->booking_date?></div>
	<div class="col-sm-6"><strong>Time:</strong> <?= $model->booking->timeZone->name?></div>
</div>
<hr />
<div class="alert alert-info">
	<strong>Note:</strong> An email will be sent immediately after you submit this info, Please make sure you submit this form when everything is ready.
	<br /><br />This information can not be modified.
</div>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'photo',['template'=>'
		{label}
		<div class="input-group file-input">
		<div class="input-group-prepend">
		<span class="input-group-text"><img src="'.$photo.'" /></span>
		</div>
		<div class="custom-file">
		{input}
		<label class="custom-file-label" for="fldImgP">Choose file</label>
		</div>
		</div>
		{error}{hint}
		'])->fileInput(['id'=>'fldImgP','class'=>'custom-file-input','accept'=>'image/*']) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'berth_no')->textInput(['maxlength' => true])?>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'battery_switch',['template'=>'
		{label}
		<div class="input-group file-input">
		<div class="input-group-prepend">
		<span class="input-group-text"><img src="'.$batterySwitchPhoto.'" /></span>
		</div>
		<div class="custom-file">
		{input}
		<label class="custom-file-label" for="fldImgBs">Choose file</label>
		</div>
		</div>
		{error}{hint}
		'])->fileInput(['id'=>'fldImgBs','class'=>'custom-file-input','accept'=>'image/*']) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'registration_card_location',['template'=>'
		{label}
		<div class="input-group file-input">
		<div class="input-group-prepend">
		<span class="input-group-text"><img src="'.$registrationCardLocPhoto.'" /></span>
		</div>
		<div class="custom-file">
		{input}
		<label class="custom-file-label" for="fldImgRcL">Choose file</label>
		</div>
		</div>
		{error}{hint}
		'])->fileInput(['id'=>'fldImgRcL','class'=>'custom-file-input','accept'=>'image/*']) ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'keys_of_the_boat',['template'=>'
		{label}
		<div class="input-group file-input">
		<div class="input-group-prepend">
		<span class="input-group-text"><img src="'.$keysOfBoatPhoto.'" /></span>
		</div>
		<div class="custom-file">
		{input}
		<label class="custom-file-label" for="fldImgBk">Choose file</label>
		</div>
		</div>
		{error}{hint}
		'])->fileInput(['id'=>'fldImgBk','class'=>'custom-file-input','accept'=>'image/*']) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'petro_guage',['template'=>'
		{label}
		<div class="input-group file-input">
		<div class="input-group-prepend">
		<span class="input-group-text"><img src="'.$petroGuagePhoto.'" /></span>
		</div>
		<div class="custom-file">
		{input}
		<label class="custom-file-label" for="fldImgPg">Choose file</label>
		</div>
		</div>
		{error}{hint}
		'])->fileInput(['id'=>'fldImgPg','class'=>'custom-file-input','accept'=>'image/*']) ?>
	</div>
</div>
<?= $form->field($model, 'comments')->textArea(['rows'=>'4','maxlength' => true])?>
<?= $form->field($model, 'staff_comments')->textArea(['rows'=>'4','maxlength' => true])?>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
