<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\assets\AllBookingListAssets;
AllBookingListAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if($searchModel->listType=='future'){
  $this->title = Yii::t('app', 'Active Bookings');
}elseif($searchModel->listType=='archived'){
  $this->title = Yii::t('app', 'Archived Bokings');
}elseif($searchModel->listType=='history'){
  $this->title = Yii::t('app', 'Booking History');
}elseif($searchModel->listType=='all'){
  $this->title = Yii::t('app', 'All Bookings');
}elseif($searchModel->listType=='deleted'){
  $this->title = Yii::t('app', 'Deleted Bookings');
}

$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}

if($searchModel->city_id!=null){
  $marinaList=Yii::$app->appHelperFunctions->getActiveCityMarinaListArr($searchModel->city_id);
}else{
  $marinaList=Yii::$app->appHelperFunctions->activeMarinaListArr;
}

$boatsList=Yii::$app->appHelperFunctions->getActiveBoatsListArr($searchModel->city_id,$searchModel->port_id);

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value' => function ($model){
  return ($model->member!=null ? $model->member->firstLastName : 'No user found!').' <span class="badge badge-info">'.Yii::$app->formatter->asDateTime($model['created_at']).'</span>'.($model['trashed']==1 ? ' <span class="badge badge-danger">'.Yii::$app->formatter->asDateTime($model['trashed_at']).'</span>' : '');
}];
$columns[]=['format'=>'html','attribute'=>'city_id','label'=>'City','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');},'filter'=>Yii::$app->appHelperFunctions->activeCityListArr];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');},'filter'=>$marinaList];
$columns[]=['format'=>'raw','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return $model->boatName;},'filter'=>$boatsList];
$columns[]=['format'=>'date','attribute'=>'booking_date','filterInputOptions'=>['class'=>'form-control dtrpicker','autocomplete'=>'off']];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>Yii::$app->appHelperFunctions->timeSlotListArr];
if(Yii::$app->user->identity->user_type!=0){
  $columns[]=['format'=>'raw','attribute'=>'booking_type','label'=>'Remarks','value'=>function($model){return $model->remarks;},'filter'=>Yii::$app->helperFunctions->adminBookingTypes];
}
//$columns[] = ['format'=>'html','label'=>'Status','attribute'=>'status','value'=>function($model){return '<span class="badge badge-'.($model->status==1 ? 'success' : 'dark').'">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>';},'filter'=>Yii::$app->helperFunctions->bookingStatus];

$columns[] = ['format'=>'raw','label'=>'Status','attribute'=>'status','value'=>function($model){
  $statusTitle=Yii::$app->helperFunctions->bookingStatus[$model->status];
  if($model->status==1){
    $menuHtml ='<div class="btn-group flex-wrap">';
    $menuHtml.='  <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">';
    $menuHtml.='    '.$statusTitle.' <span class="caret"></span>';
    $menuHtml.='  </button>';
    $menuHtml.='  <div class="dropdown-menu" role="menu">';
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> No Show', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>3,
        'data-title' => Yii::t('app', 'No Show'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Same Day Cancellation', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>7,
        'data-title' => Yii::t('app', 'Same Day Cancellation'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Cancelled due to Weather Warning', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>8,
        'data-title' => Yii::t('app', 'Cancelled due to Weather Warning'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> TCC Weather Cancelled', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>4,
        'data-title' => Yii::t('app', 'TCC Weather Cancelled'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> TCC Cancelled - Boat Not Available', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>5,
        'data-title' => Yii::t('app', 'TCC Cancelled - Boat Not Available'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->member!=null && $model->member->user_type!=0){
      if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
        $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Training', 'javascript:;', [
          'data-id'=>$model->id,
          'data-status'=>9,
          'data-title' => Yii::t('app', 'Training'),
          'class'=>'dropdown-item text-1 btn-change-status',
          'data-pjax'=>"0",
        ]);
      }
      if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
        $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Emergency', 'javascript:;', [
          'data-id'=>$model->id,
          'data-status'=>10,
          'data-title' => Yii::t('app', 'Emergency'),
          'class'=>'dropdown-item text-1 btn-change-status',
          'data-pjax'=>"0",
        ]);
      }
      if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
        $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Out of Service', 'javascript:;', [
          'data-id'=>$model->id,
          'data-status'=>11,
          'data-title' => Yii::t('app', 'Out of Service'),
          'class'=>'dropdown-item text-1 btn-change-status',
          'data-pjax'=>"0",
        ]);
      }
      if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
        $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Stay In', 'javascript:;', [
          'data-id'=>$model->id,
          'data-status'=>2,
          'data-title' => Yii::t('app', 'Stay In'),
          'class'=>'dropdown-item text-1 btn-change-status',
          'data-pjax'=>"0",
        ]);
      }
    }
    $menuHtml.='  </div>';
    $menuHtml.='</div>';
    $txtStatus=$menuHtml;
  }else{
    $txtStatus='<span class="badge badge-dark">'.$statusTitle.'</span>';
  }
  return $txtStatus;
},'contentOptions'=>['class'=>'nosd'],'filter'=>false];
$columns[]=['format'=>'raw','label'=>'Addons','value' => function ($model){return $model->addonsMenu;},'contentOptions'=>['class'=>'nosd']];
$actionBtn='';
if($searchModel->listType=='future'){
  $actionBtn='{delete} ';
}
if($searchModel->listType=='history'){
//if(Yii::$app->menuHelperFunction->checkActionAllowed('activity-detail','booking')){
  $actionBtn.='{booking-activity}';
//}
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('bookings','user')){
  $actionBtn.='{member-bookings}';
}
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'headerOptions'=>['class'=>'noprint','style'=>'width:15px;'],
  'contentOptions'=>['class'=>'noprint'],
  'template' =>'
    <div class="btn-group flex-wrap">
      <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
      </button>
      <div class="dropdown-menu dropdown-menu-right" role="menu">
        '.$actionBtn.'
      </div>
    </div>',
  'buttons' => [
    'delete' => function ($url, $model) {
      return Html::a('<i class="fa fa-trash"></i> Delete', 'javascript:;', [
        'title' => Yii::t('app', 'Delete'),
        'class'=>'dropdown-item text-1',
        'onclick'=>'javascript:deleteBooking('.$model->id.');',
        'data-id'=>$model->id,
        'data-method'=>"post",
      ]);
    },
    'booking-activity' => function ($url, $model) {
      return Html::a('<i class="fa fa-list"></i> Activity Detail', 'javascript:;', [
        'title' => Yii::t('app', 'Activity Detail'),
        'class'=>'dropdown-item text-1 load-modal',
        'data-heading'=>'Booking Activity',
        'data-url'=>Url::to(['booking/activity-detail','id'=>$model->id]),
      ]);
    },
    'member-bookings' => function ($url, $model) {
      return Html::a('<i class="fa fa-anchor"></i> Member Detail', 'javascript:;', [
        'title' => Yii::t('app', 'Member Detail'),
        'class'=>'dropdown-item text-1',
        'onclick'=>"window.open('".Url::to(['user/bookings','id'=>$model->user_id])."')",
        'target'=>'_blank',
      ]);
    },
  ],
];

$additionalHtml='<label><input type="checkbox" name="BookingSearch[search_source]" value="mem" id="cbmem" /> Members</label> <label><input type="checkbox" name="BookingSearch[search_source]" value="admin" id="cbadm" /> Admin</label> <label><input type="checkbox" name="BookingSearch[search_source]" value="bulk" id="cbbb" /> Bulk Booking</label>';
$additionalHtml='
<form>
<input type="text" name="BookingSearch[member_name]" value="'.$searchModel->member_name.'" />
<input type="text" name="BookingSearch[city_id]" value="'.$searchModel->city_id.'" />
<input type="text" name="BookingSearch[port_id]" value="'.$searchModel->port_id.'" />
<input type="text" name="BookingSearch[boat_id]" value="'.$searchModel->boat_id.'" />
<input type="text" name="BookingSearch[booking_date]" value="'.$searchModel->booking_date.'" />
<input type="text" name="BookingSearch[booking_time_slot]" value="'.$searchModel->booking_time_slot.'" />
<input type="text" name="BookingSearch[booking_type]" value="'.$searchModel->booking_type.'" />
<select name="BookingSearch[search_source][]" id="bsCusFilter" multiple="multiple">
  <option value="">All</option>
  <option value="mem">Members</option>
  <option value="admin">Admin</option>
  <option value="bulk">Bulk Booking</option>
</select>
<button type="submit">
  Go
<button>
</form>
';
?>
<style>
.grid-additional-block{margin-bottom: 10px;}
.grid-additional-block label{padding-right: 20px;}
</style>
<div class="city-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomTabbedGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'additionalHtml' => $additionalHtml,
    'filterSelector' => '#bsCusFilter',
    'rowOptions' => function ($model, $index, $widget, $grid){
      return ['class'=>'item-row','data-city_id'=>$model->city_id, 'data-marina_id'=>$model->port_id];
    },
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<?= $this->render('/booking/js/list_script')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
<?= '';//$this->render('/booking/js/activity_list_script')?>
