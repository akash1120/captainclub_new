<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\EmailTemplate;
use app\models\ArrivalTimings;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Booking Confirmation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Booking'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
$(document).delegate(".btn-del", "click", function(e) {
	e.preventDefault();
	var _targetContainer=".grid-view";
	t=$(this);
	sweetAlert({
		title: "Confirmation",
		text: "Are you sure you want to delete this booking?",
		type: "info",
		showCancelButton: true,
		confirmButtonColor: "#47a447",
		confirmButtonText: "'.Yii::t('app','Confirm').'",
		cancelButtonText: "'.Yii::t('app','Cancel').'",
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: "'.Url::to(['booking/delete','id'=>$model->id]).'",
				type: "POST",
				dataType: "json",
				success: function(response) {
					window.location.href="'.Url::to(['site/index','dm'=>1]).'";
				},
				error: bbAlert
			});
		}
	});
});
');

$comingTimeOptions=ArrayHelper::map(ArrivalTimings::find()->where(['time_slot_id'=>$model->booking_time_slot])->asArray()->all(),"title","title");
?>
<section class="card card-featured-top card-featured-warning">
  <header class="card-header">
    <h2 class="card-title"><?= $this->title?></h2>
  </header>
  <div class="card-body">
		Dear captain <?= $model->member->fullname?>,<br /><br />
		This is to confirm your booking for tomorrow <?= Yii::$app->formatter->asDate($model->booking_date)?>, if you will not show up, please <?= Html::a('delete','javascript:;',['class'=>'btn-del'])?> your booking.<br />
		City: <?=$model->city->name?><br />
		Marina: <?= $model->marina->name?><br />
		Boat: <?= $model->boat->name?><br />
		Time: <?= $model->timeZone->name?><br />
	</div>
	<footer class="card-footer">
		<?= Html::a('Confirm','javascript:;',['class'=>'btn btn-md btn-success','data-toggle'=>'modal','data-target'=>'#confirmation_form'])?>
		<?= Html::a('Delete','javascript:;',['class'=>'btn btn-md btn-danger btn-del'])?>
	</footer>
</section>


<style>
#bookingexpectedarrivaltime-arrival_time label{width:100%;line-height: 25px;}
input[type="radio"], input[type="checkbox"]{position: relative !important;left:0 !important;}
</style>

<div id="confirmation_form" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<?php $form = ActiveForm::begin(['id'=>'boat-swap-form']); ?>
		<div class="modal-content">
			<div class="modal-header" style="display: block;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				Please let us know, what time you will be arriving?
				<?= $form->field($modelArriving, 'arrival_time')->radioList($comingTimeOptions)->label(false); ?>
			</div>
			<div class="modal-footer">
			  <?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
			  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>
