<?php
use yii\helpers\Html;
use yii\helpers\Url;

$bookingActivity=$model->bookingActivity;
?>
<style>
.activity-detail{line-height: 30px;}
.activity-detail strong{font-weight: bold !important;font-size: 15px;}
.activity-detail .badge-info .badge-success{ display: none;}
</style>
<div class="activity-detail">
  <div class="row">
    <div class="col-sm-5">
      <strong>City:</strong> <?= $model->city->name?>
    </div>
    <div class="col-sm-7">
      <strong>Marina:</strong> <?= $model->marina->name?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <strong>Boat:</strong> <?= $bookingActivity!=null ? $bookingActivity->boatProvided->name : $model->boat->name?>
    </div>
    <div class="col-sm-7">
      <strong>Date:</strong> <?= Yii::$app->formatter->asDate($model->booking_date).' - '.$model->timeZone->name?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <strong>Booking Status:</strong> <?= '<span class="badge badge-'.($model->status==1 ? 'success' : 'dark').'">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>'?>
    </div>
    <div class="col-sm-7">
      <strong>Booking Experience:</strong> <?= Yii::$app->helperFunctions->bookingExperience[$model->booking_exp]?>
    </div>
  </div>
  <?php if($model->tripExperienceComments!=''){?>
  <div class="row">
    <div class="col-sm-12">
      <strong>Trip Status:</strong> <?= $model->tripExperienceComments?>
    </div>
  </div>
  <?php }?>
  <?php if($bookingActivity!=null && $bookingActivity->member_exp==1){?>
  <div class="row">
    <div class="col-sm-12">
      <strong>Behaviour:</strong> <?= $bookingActivity->memberExperienceComments;?>
    </div>
  </div>
  <?php }?>
  <?php if($model->bookingExtras!=null){?>
  <div class="row">
    <div class="col-sm-12">
      <strong>Extras:</strong> <?= $model->bookingExtras;?>
    </div>
  </div>
  <?php }?>
  <?php if($model->oprRemarks!=null){?>
  <div class="row">
    <div class="col-sm-12">
      <strong>Remarks:</strong> <?= $model->oprRemarks;?>
    </div>
  </div>
  <?php }?>
  <?php if($model->bookingDamages!=null){?>
  <div class="row">
    <div class="col-sm-12">
      <strong>Damages:</strong> <?= $model->bookingDamages;?>
    </div>
  </div>
  <?php }?>

  <?php if($bookingActivity!=null){?>
  <div class="row">
    <div class="col-sm-5">
      <strong>Time In:</strong> <?= $bookingActivity->dep_time?>
    </div>
    <div class="col-sm-7">
      <strong>Time Out:</strong> <?= $bookingActivity->arr_time?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <strong>Fuel In:</strong> <?= $bookingActivity->dep_fuel_level?>
    </div>
    <div class="col-sm-7">
      <strong>Fuel Out:</strong> <?= $bookingActivity->arr_fuel_level?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <strong>Engine In:</strong> <?= $bookingActivity->dep_engine_hours?>
    </div>
    <div class="col-sm-7">
      <strong>Engine Out:</strong> <?= $bookingActivity->arr_engine_hours?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <strong># of Ppl In:</strong> <?= $bookingActivity->dep_ppl?>
    </div>
    <div class="col-sm-7">
      <strong># of Ppl Out:</strong> <?= $bookingActivity->arr_ppl?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-5">
      <strong>Cost of Fuel:</strong> <?= 'AED '.$bookingActivity->fuel_cost.($bookingActivity->bill_image!=null && file_exists(Yii::$app->params['fuelbill_abs_path'].$bookingActivity->bill_image) ? ' <a href="'.Yii::$app->fileHelperFunctions->getImagePath('fuelbill',$bookingActivity->bill_image,'full').'" class="btn btn-info btn-xs" target="_blank">View bill</a>' : '')?>
    </div>
    <div class="col-sm-5">
      <strong>Opr Staff:</strong> <?= $bookingActivity->createdBy?>
    </div>
  </div>
  <?php }?>
  <?php if($model->addonsSpan!=''){?>
  <div class="row">
    <div class="col-sm-12">
      <strong>Addons:</strong><br /><?= $model->addonsSpan;?>
    </div>
  </div>
  <?php }?>
</div>
