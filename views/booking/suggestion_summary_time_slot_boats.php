<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Booking;
use app\models\BulkBooking;
use app\models\Boat;
use app\models\BoatToTimeSlot;
use app\models\BoatAvailableDays;
use app\models\BookingSuggestionForm;
use app\models\TimeSlot;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

list($sy,$sm,$sd)=explode("-",$date);
$dayOfWeek=date("w",mktime(0,0,0,$sm,$sd,$sy));

$model = new BookingSuggestionForm();
$model->city_id=$city_id;
$model->date=$date;
$model->user_id=$user->id;

$timeSlotsArr=Yii::$app->appHelperFunctions->timeSlotListDashboard;
?>
<?php $form = ActiveForm::begin(['id'=>'bookTimeSlotPopForm','action'=>Url::to(['booking/create-from-suggestion'])]); ?>
<div class="hidden">
	<?= $form->field($model, 'user_id')->textInput() ?>
	<?= $form->field($model, 'city_id')->textInput() ?>
	<?= $form->field($model, 'date')->textInput(['id'=>'sug-date']) ?>
</div>
<strong>The Following Boats are available on <?= Yii::$app->formatter->asDate($date,"php:l j M, Y").' - '.$slotTitle?> in <?= $marinaName?></strong><br />
<?php
$showSubmitBtn=false;
$sqAvailability=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);
$sqBulkedBooked=BulkBooking::find()->select(['boat_id'])->where(['and',['trashed'=>0],['<=','start_date',$date],['>=','end_date',$date]]);
$boatResults=BoatToTimeSlot::find()
->select([
	BoatToTimeSlot::tableName().'.id',
	'boat_id'=>Boat::tableName().'.id',
	Boat::tableName().'.city_id',
	Boat::tableName().'.port_id',
	BoatToTimeSlot::tableName().'.time_slot_id',
	Boat::tableName().'.name',
	TimeSlot::tableName().'.timing',
])
->innerJoin(Boat::tableName(),Boat::tableName().".id=".BoatToTimeSlot::tableName().".boat_id")
->innerJoin(TimeSlot::tableName(),TimeSlot::tableName().".id=".BoatToTimeSlot::tableName().".time_slot_id")
->where([
	'and',
	[
		Boat::tableName().'.id'=>$sqAvailability,
		Boat::tableName().'.city_id'=>$city_id,
		Boat::tableName().'.port_id'=>$marina_id,
		Boat::tableName().'.special_boat'=>[0,2],
		BoatToTimeSlot::tableName().'.time_slot_id'=>explode(",",$time_id),
		Boat::tableName().'.trashed'=>0,
		Boat::tableName().'.status'=>1,
	],
	['not in',Boat::tableName().'.id',$sqBulkedBooked],
])
->asArray()
->all();
if($boatResults!=null){
	echo '<div style="height:255px; padding:0 1px; overflow-y: auto; margin-bottom:10px;">';
	foreach($boatResults as $boatResult){
		$booking=Booking::find()->where(['city_id'=>$boatResult['city_id'],'port_id'=>$boatResult['port_id'],'boat_id'=>$boatResult['boat_id'],'booking_date'=>$date,'booking_time_slot'=>$boatResult['time_slot_id'],'status'=>1,'trashed'=>0]);
		if(!$booking->exists()){
			$showSubmitBtn=true;
			?>
			<div style="margin:10px 0;">
				<label>
					<input style="margin-l1eft:-13px;" type="radio" class="cb2" name="BookingSuggestionForm[port_boat_time_id]" value="<?= $boatResult['port_id'].'_'.$boatResult['boat_id'].'_'.$boatResult['time_slot_id']?>" />
					<?= $boatResult['name'].' ('.$boatResult['timing'].')'?>
					<a href="javascript:;" class="btn btn-xs btn-info load-modal" data-url="<?= Url::to(['suggestion/boat-info','id'=>$boatResult['boat_id']])?>" data-mid="general-modal-secondary" data-heading="Boat Info" style="padding: 0px 5px;">Info</a>
				</label>
			</div>
			<?php
		}
	}
	echo '</div>';
if($showSubmitBtn==true){
?>
<div>
	<button type="submit" class="btn btn-success">Book</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
</div>
<?php
}
}
?>
<?php ActiveForm::end(); ?>
