<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use app\components\widgets\CustomPjax;
use app\models\BookingAlert;
use app\models\BookingAlertTiming;
use app\assets\BookingCreateAssets;
BookingCreateAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Booking */

$loggedInUser=Yii::$app->user->identity;

$this->title = Yii::t('app', 'New Booking');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
$("body").on("click", ".btn.view-type", function () {
  $(".btn.view-type").removeClass("active");
  $(this).addClass("active");
  act=$(this).data("type");
  if(act==1){
    $(".item.isFullBooked").removeClass("showIt");
  }else{
    $(".item.isFullBooked").addClass("showIt");
  }
});
$("body").on("change", "#bookingform-city_id", function () {
  searchBoats();
});
$("body").on("change", "#bookingform-date", function () {
  searchBoats();
});
');

$fldCols=6;
$btnCol=6;
if(Yii::$app->user->identity->user_type!=0){
  $fldCols=3;
  $btnCol=3;
}

if(Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->canBookBoat==false){
  $userCredits=Yii::$app->user->identity->profileInfo->credit_balance;
?>
<section class="card">
  <header class="card-header">
		<h2 class="card-title">Dear Captain,</h2>
	</header>
  <div class="card-body">
    Your current balance is <span class="badge badge-danger"><?= $userCredits?></span> AED. Please deposit funds by clicking <a href="javascript:;" class="load-modal" data-dismiss="modal" data-url="<?= Url::to(['order/create'])?>" data-heading="Deposit Funds">here</a>.<br /><br />
    Enjoy Boating With Relief
  </div>
  <div class="card-footer clearfix">
		<a class="btn btn-primary pull-right load-modal" href="javascript:;" data-dismiss="modal" data-url="<?= Url::to(['request/boat-booking'])?>" data-heading="Boat Booking">Submit Booking Request</a>
	</div>
</section>
<?= $this->render('/order/js/scripts')?>
<?php
}else{
?>
<script>
function searchBoats()
{
  if($("#bookingform-city_id").val()!='' && $("#bookingform-date").val()!=''){
    $("#btnSearch").trigger('click');
    //document.getElementById("createBookingForm").submit();
  }
}
</script>
<style>
@media only screen and (max-width: 767px) {
.col-xs-3.img-col {
width: 33.33333333%;
padding-right: 5px;
}
.col-xs-9.detail-col {
width: 66.66666667%;
padding-left: 5px;
}
#boats-list-view .h3 {
font-size: 15px;
font-weight:bold;
}
}
</style>
<div class="booking-create">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <section class="card mb-1">
    <div class="card-body filter-padding">
      <?php $form = ActiveForm::begin(['id'=>'createBookingForm','action'=>Url::to(['booking/create']),'method'=>'get','options'=>['data-pjax' => '']]); ?>
      <div class="row top-filter-row">
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr,['prompt'=>Yii::t('app','Select')])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'date')->textInput(['maxlength' => true, 'class'=>'form-control dtpicker', 'readonly'=>'readonly', 'autocomplete'=>'off', 'placeholder'=>$searchModel->getAttributeLabel('date')])->label(false)?>
        </div>
        <?php if(Yii::$app->user->identity->user_type!=0){?>
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'booking_type')->dropDownList(Yii::$app->helperFunctions->adminBookingTypes,['prompt'=>Yii::t('app','Select')])->label(false)?>
        </div>
        <div class="col-xs-12 col-sm-<?= $fldCols?>">
          <?= $form->field($searchModel, 'booking_comments')->textInput(['maxlength' => true, 'autocomplete'=>'off', 'placeholder'=>$searchModel->getAttributeLabel('booking_comments')])->label(false)?>
        </div>
        <?php }?>
        <div class="col-sm-6" style="display:none;">
          <?= Html::submitButton('<i class="fas fa-search"></i> '.Yii::t('app', 'Search'), ['id'=>'btnSearch', 'class' => 'btn btn-success btn-block']) ?>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </section>


<?php

$holdAlerts=BookingAlertTiming::find()
->select([
	BookingAlertTiming::tableName().'.time_slot_id',
	BookingAlert::tableName().'.message',
])
->innerJoin(BookingAlert::tableName(),BookingAlert::tableName().".id=".BookingAlertTiming::tableName().".alert_id")
->where([
	'alert_type'=>2,
	'alert_date'=>$searchModel->date,
	'port_id'=>$searchModel->port_id,
	'trashed'=>0,
])
->asArray()->all();
if($holdAlerts!=null){
	foreach($holdAlerts as $holdAlert){
		$this->registerJs('
		$(document).delegate(".hold-alert_'.$holdAlert['time_slot_id'].'", "click", function() {
		   $(".radio-custom").removeClass("active");
  		 $(this).find("input[name=\"boat_time\"]").prop("checked","");
			swal({
					title: "Dear Captain",
					html: "'.preg_replace("/\r|\n/","",nl2br($holdAlert['message'])).'",
					type: "info",
					showCancelButton: false,
					confirmButtonColor: "#47a447",
					confirmButtonText: "Noted",
			});
		});
		');
	}
}
?>
<div style="display:none;">
<?php
$warningAlerts=BookingAlertTiming::find()
->select([
	BookingAlertTiming::tableName().'.time_slot_id',
	BookingAlert::tableName().'.message',
])
->innerJoin(BookingAlert::tableName(),BookingAlert::tableName().".id=".BookingAlertTiming::tableName().".alert_id")
->where([
	'alert_type'=>1,
	'alert_date'=>$searchModel->date,
	'port_id'=>$searchModel->port_id,
	'trashed'=>0,
])
->asArray()->all();
if($warningAlerts!=null){
	foreach($warningAlerts as $warningAlert){
    echo '<div id="warning_'.$warningAlert['time_slot_id'].'">
      '.preg_replace("/\r|\n/","",nl2br($warningAlert['message'])).'
    </div>';
	}

}
foreach(Yii::$app->helperFunctions->specialBoatTypeMessage as $key=>$val){
  echo '<div id="sp_'.$key.'">'.$val.'</div>';
}
?>
</div>
<style>
.sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 0;
}
.sticky:before,
.sticky:after {
    content: '';
    display: table;
}
</style>
    <div class="tabs">
      <div class="sticky">
        <ul class="nav nav-tabs nav-justified">
          <?= $searchModel->tabItems?>
        </ul>
      </div>
      <?php if($dataProvider->getTotalCount()>0){?>
      <div class="tab-content">
        <div class="tab-pane active">
          <div class="row btnopts">
          	<div class="col-sm-12">
              <a href="javascript:;" class="btn btn-md btn-default view-type active" data-type="1">View Available <?= $searchModel->availableSessions?> Boats</a>
              <a href="javascript:;" class="btn btn-md btn-default view-type" data-type="0">View All <?= $searchModel->totalSessions?> Boats</a>
            </div>
          </div>
          <?= ListView::widget( [
          	'dataProvider' => $dataProvider,
          	'id' => 'boats-list-view',
            'options'=>['tag'=>'ul','class'=>'list-unstyled searc1h-results-list'],
          	'itemOptions' => ['tag'=>'li','class' => 'item'],
          	'itemView' => '_booking_item',
						'viewParams'=>['searchModel'=>$searchModel],
          	'layout'=>"
          		{items}
          		{pager}
          	",
          ] );
          ?>
        </div>
        <center>
          <?= Html::button('<i class="fas fa-mail-bulk"></i> '.Yii::t('app', 'Request'), ['class' => 'btn btn-info', 'data-toggle'=>'modal','data-target'=>'#request-modal']) ?>
          <?= Html::a('<i class="fas fa-chevron-left"></i> '.Yii::t('app', 'Cancel'), ['site/index'], ['class' => 'btn btn-default', 'data-pjax'=>'0']) ?>
        </center>
      </div>
      <?php }?>
    </div>
  <?php CustomPjax::end(); ?>
</div>
<?php
}
Modal::begin([
'headerOptions' => ['class' => 'modal-header'],
'id' => 'booking-comment-modal',
'size' => 'modal-md',
'header' => '<h5 class="modal-title">Reason</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
'closeButton' => false,
]);
$adminBookingTypes[0]=Yii::t('app','Select');
$abtArr=Yii::$app->helperFunctions->adminBookingTypes;
if($abtArr!=null){
  foreach ($abtArr as $key=>$val){
    $adminBookingTypes[$key]=$val;
  }
}

echo '<div class="modalContent">';
echo '  <div class="form-group required">';
echo '    <label class="control-label" for="booking_booking_type">Type</label>';
echo '    '.Html::dropDownList('booking_type', 0, $adminBookingTypes, ['id'=>'booking_booking_type','class'=>'form-control']);
echo '    <div class="help-block"></div>';
echo '  </div>';
echo '  <div class="form-group required">';
echo '    <label class="control-label" for="booking_booking_comments">Comments</label>';
echo '    '.Html::textInput('booking_comments', "", ['id'=>'booking_booking_comments','class'=>'form-control']);
echo '    <div class="help-block"></div>';
echo '  </div>';
echo '  <div class="row">';
echo '    <div class="col-md-12">';
echo '      <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>';
echo '    </div>';
echo '  </div>';
echo "</div>";
Modal::end();
?>
<?= $this->render('/request/selection',['user'=>Yii::$app->user->identity])?>
<?= $this->render('/request/js/selection_scripts')?>
<?= $this->render('/booking/js/alert_script')?>
<?= $this->render('/booking/js/booking_script',['loggedInUser'=>$loggedInUser])?>
<?= $this->render('/booking/js/alert_script')?>
<?= $this->render('/request/js/freeze_scripts')?>
<?= $this->render('/request/js/other_request_scripts')?>
<?= $this->render('/request/js/night_drive_training_request_scripts')?>
<?= $this->render('/request/js/package_city_restriction_script')?>
<?= $this->render('/request/js/boat_booking_scripts')?>
