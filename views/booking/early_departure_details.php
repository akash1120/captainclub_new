<?php
use app\assets\AppEarlyDepartureAsset;
AppEarlyDepartureAsset::register($this);

$this->title = Yii::t('app','Early Departure Details');

$assets=$model->earlyDepartureAssets;

if(filter_var($assets['photo'], FILTER_VALIDATE_URL)){
	$boatPhoto=$assets['photo'];
	$boatBigPhoto=$assets['photo'];
}else{
	$boatPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['photo'],'large');
	$boatBigPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['photo'],'xlarge');
}

if(filter_var($assets['battery_switch'], FILTER_VALIDATE_URL)){
	$batterySwitchPhoto=$assets['battery_switch'];
	$batterySwitchBigPhoto=$assets['battery_switch'];
}else{
	$batterySwitchPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['battery_switch'],'large');
	$batterySwitchBigPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['battery_switch'],'xlarge');
}

if(filter_var($assets['registration_card_location'], FILTER_VALIDATE_URL)){
	$regCardLocPhoto=$assets['registration_card_location'];
	$regCardLocBigPhoto=$assets['registration_card_location'];
}else{
	$regCardLocPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['registration_card_location'],'large');
	$regCardLocBigPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['registration_card_location'],'xlarge');
}

if(filter_var($assets['keys_of_the_boat'], FILTER_VALIDATE_URL)){
	$keysOfTheBoatPhoto=$assets['keys_of_the_boat'];
	$keysOfTheBoatBigPhoto=$assets['keys_of_the_boat'];
}else{
	$keysOfTheBoatPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['keys_of_the_boat'],'large');
	$keysOfTheBoatBigPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['keys_of_the_boat'],'xlarge');
}

if(filter_var($assets['petro_guage'], FILTER_VALIDATE_URL)){
	$petroGuagePhoto=$assets['petro_guage'];
	$petroGuageBigPhoto=$assets['petro_guage'];
}else{
	$petroGuagePhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['petro_guage'],'large');
	$petroGuageBigPhoto=Yii::$app->fileHelperFunctions->getImagePath('early-departure',$assets['petro_guage'],'xlarge');
}

$this->registerJs('
$(document).ready(function() {
	$(".fancybox").fancybox({
		openEffect	: "none",
		closeEffect	: "none"
	});
});
');
?>
Dear Captain <?= $model->member->fullname?>,<br /><br />

Your Early Departure request is Confirmed.<br /><br />

Since The Captains Club team will not hand over the boat Phisically in the marina:<br />

The boat considered under your reponsibilty upon recieving this information<br />

The Captain's Club reserves the right to cancel this request in case of any last minute emergencies.<br /><br />
<style>
.body-sign .card-sign .card-body{border-top: 0px;padding: 10px;}
.edinfo .card-body{border-top: 0px; text-align: center;}
@media only screen and (max-width: 768px) {
  /* For mobile phones: */
  .body-sign{width:95%; max-width:95%;height:auto;}
  .edinfo .card-body img{width: 100%;}
}
.text-box{
  font-size: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 220px;
}
.card-body img{width: 100%;}
</style>
<div class="row edinfo">
  <div class="col-xs-12 col-sm-6">
    <section class="card card-featured card-featured-warning mb-4">
      <header class="card-header">
        <h2 class="card-title">Boat Photo</h2>
      </header>
      <div class="card-body">
        <a href="<?= $boatBigPhoto?>" data-fancybox="gallery" data-caption="Boat Photo"><img src="<?= $boatPhoto?>" alt="Boat Photo" /></a>
      </div>
    </section>
  </div>
  <div class="col-xs-12 col-sm-6">
    <section class="card card-featured card-featured-warning mb-4">
      <header class="card-header">
        <h2 class="card-title">Berth No</h2>
      </header>
      <div class="card-body text-box">
        <strong><?= $assets['berth_no']?></strong>
      </div>
    </section>
  </div>
</div>
<div class="row edinfo">
  <div class="col-xs-12 col-sm-6">
    <section class="card card-featured card-featured-warning mb-4">
      <header class="card-header">
        <h2 class="card-title">Battery Switch</h2>
      </header>
      <div class="card-body">
        <a href="<?= $batterySwitchBigPhoto?>" data-fancybox="gallery" data-caption="Battery Switch"><img src="<?= $batterySwitchPhoto?>" alt="Battery Switch" /></a>
      </div>
    </section>
  </div>
  <div class="col-xs-12 col-sm-6">
    <section class="card card-featured card-featured-warning mb-4">
      <header class="card-header">
        <h2 class="card-title">Registration Card Location</h2>
      </header>
      <div class="card-body">
        <a href="<?= $regCardLocBigPhoto?>" data-fancybox="gallery" data-caption="Registration Card Location"><img src="<?= $regCardLocPhoto?>" alt="Registration Card Location" /></a>
      </div>
    </section>
  </div>
</div>
<div class="row edinfo">
  <div class="col-xs-12 col-sm-6">
    <section class="card card-featured card-featured-warning mb-4">
      <header class="card-header">
        <h2 class="card-title">Keys Of The Boat</h2>
      </header>
      <div class="card-body">
        <a href="<?= $keysOfTheBoatBigPhoto?>" data-fancybox="gallery" data-caption="Keys Of The Boat"><img src="<?= $keysOfTheBoatPhoto?>" alt="Keys Of The Boat" /></a>
      </div>
    </section>
  </div>
  <div class="col-xs-12 col-sm-6">
    <section class="card card-featured card-featured-warning mb-4">
      <header class="card-header">
        <h2 class="card-title">Petro Guage</h2>
      </header>
      <div class="card-body">
        <a href="<?= $petroGuageBigPhoto?>" data-fancybox="gallery" data-caption="Petro Guage"><img src="<?= $petroGuagePhoto?>" alt="Petro Guage" /></a>
      </div>
    </section>
  </div>
</div>
<?php if($assets['comments']!=''){?>
<section class="card card-featured card-featured-warning mb-4">
  <header class="card-header">
    <h2 class="card-title">Comments</h2>
  </header>
  <div class="card-body">
    <?= nl2br($assets['comments'])?>
  </div>
</section>
<?php }?>
