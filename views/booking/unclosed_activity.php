<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\OperationUserCityMarina;
use app\models\City;
use app\models\Marina;
use app\models\Boat;
use app\models\BookingEarlyDepartureAssets;
use app\assets\BookingActivityAssets;
BookingActivityAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('operation-create')){
  $createBtn=true;
}

$modelOpr=OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();

$this->title = Yii::t('app', 'Unclosed Activity');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operations'), 'url' => ['operations/index']];
$this->params['breadcrumbs'][] = $this->title;

if($searchModel->city_id!=null){
  $marinaList=Yii::$app->appHelperFunctions->getActiveCityMarinaListArr($searchModel->city_id);
}else{
  $marinaList=Yii::$app->appHelperFunctions->activeMarinaListArr;
}



$cities=ArrayHelper::map(City::find()
  ->select([
    'id',
    'name',
  ])
  ->where(['id'=>$modelOpr->city_id,'trashed'=>0])
  ->orderBy(['name'=>SORT_ASC])
  ->asArray()
  ->all(),"id","name");
$marinaList=ArrayHelper::map(Marina::find()
  ->select([
    'id',
    'name',
  ])
  ->where(['id'=>$modelOpr->marina_id,'trashed'=>0])
  ->orderBy(['name'=>SORT_ASC])
  ->asArray()
  ->all(),"id","name");

$marinaBoats=ArrayHelper::map(Boat::find()
  ->select(['id','name'])
  ->where(['city_id'=>$modelOpr->city_id,'port_id'=>$modelOpr->marina_id,'status'=>1,'trashed'=>0])
  ->orderBy(['rank'=>SORT_ASC])
  ->asArray()
  ->all(),"id","name");
$txtJScript=Yii::$app->jsFunctions->getCityMarinaArr($cities);

$this->registerJs($txtJScript.'
$(document).delegate("#bookingsearch-city_id", "change", function() {
	$("#bookingsearch-port_id").html("");
	array_list=cities[$(this).val()];
	$(array_list).each(function (i) {
		$("#bookingsearch-port_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
	});
});
$("body").on("beforeSubmit", "form#updateEarlyDepartureAssets", function () {
	_targetContainer=$("#updateEarlyDepartureAssets")
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});

	 var form = $(this);
	 // return false if form still have some validation errors
	 if (form.find(".has-error").length) {
			return false;
	 }
	 var formData = new FormData(form[0]);
	 // submit form
	 $.ajax({
			url: form.attr("action"),
			type: "post",
			data: formData,
      dataType: "json",
			success: function (response) {
        if(response["success"]){
          swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 10000});
          window.closeModal();
          $.pjax.reload({container: "#grid-container", timeout: 2000});

          $("#tb1").find(".nav-link").removeClass("active");
          $("#tb2").find(".nav-link").addClass("active");
          $("#t1").removeClass("active");
          $("#t2").addClass("active");
        }else{
          swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 10000});
        }
				App.unblockUI($(_targetContainer));
			},
	    cache: false,
	    contentType: false,
	    processData: false
	 });
	 return false;
});
');

$boatsList=Yii::$app->appHelperFunctions->getActiveBoatsListArr($searchModel->city_id,$searchModel->port_id);

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'raw','value'=>function($model){
  return Html::img(Yii::$app->fileHelperFunctions->getImagePath('user',$model->member!=null ? $model->member->image : '','tiny'),['width'=>50,'height'=>50]);
}];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value' => function ($model){
  $html = $model->member!=null ? $model->member->fullname : '';

  if($model->member!=null){
    $credit_balance=Yii::$app->helperFunctions->getCreditHtml($model->member->profileInfo->credit_balance);
    $html.='<br /><small>'.$credit_balance.'</small>';
    $html.='<br />License: '.Yii::$app->helperFunctions->arrYesNoIcon[$model->member->profileInfo->is_licensed];
  }
  return $html;
},'filter'=>false];
$columns[]=['format'=>'raw','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return $model->boatName;},'filter'=>false];
$columns[]=['format'=>'date','attribute'=>'booking_date','filter'=>false];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>false];
$columns[]=['format'=>'raw','attribute'=>'booking_type','label'=>'Remarks','value'=>function($model){return $model->oprRemarks;},'filter'=>false];
$columns[]=['format'=>'raw','label'=>'Addons','value' => function ($model){return $model->oprAddonsTable;},'contentOptions'=>['class'=>'nosd']];
$columns[]=['format'=>'raw','label'=>'Petrol','value' => function ($model){
  $txtPetrol='';
  $activity=$model->bookingActivity;
  if($activity!=null && $activity->hide_other==1){
    if($activity->fuel_chargeable==1){
      $txtPetrol='AED '.$activity->fuel_cost;
    }else{
      $txtPetrol='<small><span class="badge grid-badge badge-info">Not Charged: '.$activity->no_charge_comment.'</span></small>';
    }
  }
  return $txtPetrol;
},'contentOptions'=>['class'=>'nosd']];
$columns[] = ['format'=>'raw','label'=>'Status','attribute'=>'status','value'=>function($model){
  $statusTitle=Yii::$app->helperFunctions->bookingStatus[$model->status];
  if($model->status==1 && $model->booking_date<=date("Y-m-d")){
    $menuHtml ='<div class="btn-group flex-wrap">';
    $menuHtml.='  <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown">';
    $menuHtml.='    '.$statusTitle.' <span class="caret"></span>';
    $menuHtml.='  </button>';
    $menuHtml.='  <div class="dropdown-menu" role="menu">';
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> No Show', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>3,
        'data-title' => Yii::t('app', 'No Show'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Same Day Cancellation', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>7,
        'data-title' => Yii::t('app', 'Same Day Cancellation'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Cancelled due to Weather Warning', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>8,
        'data-title' => Yii::t('app', 'Cancelled due to Weather Warning'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> TCC Weather Cancelled', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>4,
        'data-title' => Yii::t('app', 'TCC Weather Cancelled'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> TCC Cancelled - Boat Not Available', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>5,
        'data-title' => Yii::t('app', 'TCC Cancelled - Boat Not Available'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
      $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Stay In', 'javascript:;', [
        'data-id'=>$model->id,
        'data-status'=>2,
        'data-title' => Yii::t('app', 'Stay In'),
        'class'=>'dropdown-item text-1 btn-change-status',
        'data-pjax'=>"0",
      ]);
    }
    if($model->member!=null && $model->member->user_type!=0){
      if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
        $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Training', 'javascript:;', [
          'data-id'=>$model->id,
          'data-status'=>9,
          'data-title' => Yii::t('app', 'Training'),
          'class'=>'dropdown-item text-1 btn-change-status',
          'data-pjax'=>"0",
        ]);
      }
      if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
        $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Emergency', 'javascript:;', [
          'data-id'=>$model->id,
          'data-status'=>10,
          'data-title' => Yii::t('app', 'Emergency'),
          'class'=>'dropdown-item text-1 btn-change-status',
          'data-pjax'=>"0",
        ]);
      }
      if($model->status==1 && ($model->bookingActivity==null || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==0))){
        $menuHtml.=Html::a('<i class="fas fa-chevron-right"></i> Out of Service', 'javascript:;', [
          'data-id'=>$model->id,
          'data-status'=>11,
          'data-title' => Yii::t('app', 'Out of Service'),
          'class'=>'dropdown-item text-1 btn-change-status',
          'data-pjax'=>"0",
        ]);
      }
    }
    $menuHtml.='  </div>';
    $menuHtml.='</div>';
    $txtStatus=$menuHtml;
  }else{
    $txtStatus='<span class="badge badge-dark">'.$statusTitle.'</span>';
  }
  return $txtStatus;
},'contentOptions'=>['class'=>'nosd'],'filter'=>false];
//if($searchModel->booking_date<=date("Y-m-d")){
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
  'contentOptions'=>['class'=>'noprint'],
  'template' => '{in-out}',
  'buttons' => [
    'in-out' => function ($url, $model) {
      if($model->booking_date<=date("Y-m-d")){
        $url=Url::to(['in-out','id'=>$model->id,'rb'=>'uca']);
        if($model->status==1 || ($model->bookingActivity!=null && $model->bookingActivity->hide_other==1)){
          $color='btn-danger';
          if($model->bookingActivity!=null){
            if($model->bookingActivity->hide_other==1){
              $color='btn-success';
              $url=Url::to(['view-in-out','id'=>$model->id,'rb'=>'uca']);
            }else{
              $color='btn-warning';
            }
          }
          return Html::a('<i class="fas fa-edit"></i> In - Out', $url, [
            'data-id'=>$model->id,
            //'data-status'=>11,
            'data-title' => Yii::t('app', 'In - Out'),
            'class'=>'btn btn-xs '.$color,
            'data-pjax'=>"0",
          ]);
        }
      }
    },
  ],
];
//}
?>
<style>
.filters{display: none;}
</style>
<div class="booking-activity">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <section class="card mb-1">
    <div class="card-body filter-padding">
      <?php $form = ActiveForm::begin(['action'=>Url::to(['booking/unclosed-activity']),'method'=>'get','options'=>['data-pjax' => '']]); ?>
      <div class="row top-filter-row">
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'booking_date')->textInput(['class'=>'form-control dtpicker','maxlength'=>true,'autocomplete'=>'off'])?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'city_id')->dropDownList($cities) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'port_id')->dropDownList($marinaList) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'booking_time_slot')->dropDownList(Yii::$app->appHelperFunctions->timeSlotListDashboardArr,['prompt'=>'Select']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'boat_id')->dropDownList($marinaBoats,['prompt'=>'Select']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <div class="row">
            <div class="col-xs-12 col-sm-<?= $createBtn==true ? '8' : '12'?>">
              <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <?= Html::submitButton('<i class="fas fa-search"></i> '.Yii::t('app', 'Search'), ['class' => 'btn btn-info btn-block']) ?>
              </div>
            </div>
            <?php if($createBtn==true){?>
            <div class="col-xs-12 col-sm-4">
              <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <a href="javascript:;" class="btn btn-success btn-block load-modal" data-heading="New Booking" data-url="<?= Url::to(['booking/operation-create'])?>"><i class="fa fa-plus"></i></a>
              </div>
            </div>
            <?php }?>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </section>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => false,
    'cardHeader'=>true,
    'rowOptions' => function ($model, $index, $widget, $grid){
      return ['data-city_id'=>$model->city_id, 'data-marina_id'=>$model->port_id];
    },
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<?= $this->render('/booking/js/activity_list_script')?>
