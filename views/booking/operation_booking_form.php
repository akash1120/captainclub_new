<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\OperationUserCityMarina;
use yii\helpers\ArrayHelper;
use app\models\City;
use app\models\Marina;

$this->registerJs('
$(".sdtpicker").datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
  startDate: "today",
}).on("changeDate", function(e){
  $(this).datepicker("hide");
});
$(".lookup-member").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/member-search']).'",
  noCache: true,
    onSelect: function(suggestion) {
      if($("#newuserbooking-user_id").val()!=suggestion.data){
        $("#newuserbooking-user_id").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
        $("#newuserbooking-user_id").val("");
    }
});
');

$modelOpr=OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();

$cities=ArrayHelper::map(City::find()
  ->select([
    'id',
    'name',
  ])
  ->where(['id'=>$modelOpr->city_id,'trashed'=>0])
  ->orderBy(['name'=>SORT_ASC])
  ->asArray()
  ->all(),"id","name");
$marinaList=ArrayHelper::map(Marina::find()
  ->select([
    'id',
    'name',
  ])
  ->where(['id'=>$modelOpr->marina_id,'trashed'=>0])
  ->orderBy(['name'=>SORT_ASC])
  ->asArray()
  ->all(),"id","name");
?>
<?php $form = ActiveForm::begin(['id'=>'operations-booking-form']); ?>
<div class="hidden">
  <?= $form->field($model, 'selected_boat')->textInput() ?>
  <?= $form->field($model, 'date')->textInput() ?>
  <?= $form->field($model, 'user_id')->textInput() ?>
</div>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'class' => 'form-control lookup-member', 'placeholder'=>$model->getAttributeLabel('username')]) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'city_id')->dropDownList($cities) ?>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'marina_id')->dropDownList($marinaList) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'time_id')->dropDownList(Yii::$app->appHelperFunctions->timeSlotListDashboardArr,['prompt'=>Yii::t('app','Select')]) ?>
    </div>
  </div>
  <?= $form->field($model, 'boat_id')->dropDownList([]) ?>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'booking_type')->dropDownList(Yii::$app->helperFunctions->adminBookingTypes) ?>
    </div>
    <div class="col-xs-12 col-sm-6">
      <?= $form->field($model, 'comments')->textInput(['class'=>'form-control']) ?>
    </div>
  </div>

  <div class="form-group">
    <?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
  </div>
<?php ActiveForm::end(); ?>
