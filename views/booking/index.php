<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\assets\BookingListAssets;
BookingListAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if($searchModel->listType=='future'){
  $this->title = Yii::t('app', 'Active Bookings');
}elseif($searchModel->listType=='history'){
  $this->title = Yii::t('app', 'Booking History');
}elseif($searchModel->listType=='deleted'){
  $this->title = Yii::t('app', 'Deleted Bookings');
}

$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}

if($searchModel->city_id!=null){
  $marinaList=Yii::$app->appHelperFunctions->getActiveCityMarinaListArr($searchModel->city_id);
}else{
  $marinaList=Yii::$app->appHelperFunctions->activeMarinaListArr;
}

$boatsList=Yii::$app->appHelperFunctions->getActiveBoatsListArr($searchModel->city_id,$searchModel->port_id);

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value' => function ($model){
  return $model->member->fullname.' <span class="badge badge-info">'.Yii::$app->formatter->asDateTime($model['created_at']).'</span>';
}];
$columns[]=['format'=>'html','attribute'=>'city_id','label'=>'City','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');},'filter'=>Yii::$app->appHelperFunctions->activeCityListArr];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');},'filter'=>$marinaList];
$columns[]=['format'=>'raw','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return $model->boatName;},'filter'=>$boatsList];
$columns[]=['format'=>'date','attribute'=>'booking_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>Yii::$app->appHelperFunctions->timeSlotListArr];
if(Yii::$app->user->identity->user_type!=0){
  $columns[]=['format'=>'raw','attribute'=>'booking_type','label'=>'Remarks','value'=>function($model){return $model->remarks;},'filter'=>Yii::$app->helperFunctions->adminBookingTypes];
}
$columns[] = ['format'=>'html','label'=>'Status','attribute'=>'status','value'=>function($model){return '<span class="badge badge-'.($model->status==1 ? 'success' : 'dark').'">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>';}];
$columns[]=['format'=>'raw','label'=>'Addons','value' => function ($model){return $model->addonsPopupButton.$model->addonsRequestedHtml;},'contentOptions'=>['class'=>'nosd']];
if($searchModel->listType=='future'){
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'headerOptions'=>['class'=>'noprint','style'=>'width:75px;'],
  'contentOptions'=>['class'=>'noprint'],
  'template' => '{delete}',
  'buttons' => [
    'delete' => function ($url, $model) {
      $showDeleteBtn=true;
      if($model->user_id==Yii::$app->user->identity->id){
        if($model->booking_date==date("Y-m-d") && strtotime(date("H:i:s"))>=strtotime($model->timeZone->start_time)){
          $showDeleteBtn=false;
        }
      }else{
        $showDeleteBtn=false;
      }
      if($model->status!=1){
        $showDeleteBtn=false;
      }
      if($showDeleteBtn==true){
        return Html::a('<i class="fa fa-trash"></i> Delete Booking', 'javascript:;', [
          'title' => Yii::t('app', 'Delete Booking'),
          'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-danger',
          'onclick'=>'javascript:deleteBooking('.$model->id.');',
          'data-id'=>$model->id,
          'data-method'=>"post",
        ]);
      }
    },
  ],
];
}
?>
<style>
    @media (max-width: 480px) {
        .nav-tabs{
            display: block !important;
            padding-top: 40px;
        }
        ul.nav-tabs > li:last-child {
            top:0px;
        }
    }
</style>
<div class="booking-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomTabbedGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'rowOptions' => function ($model, $index, $widget, $grid){
      return ['class'=>'item-row','data-city_id'=>$model->city_id, 'data-marina_id'=>$model->port_id];
    },
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<?= $this->render('/booking/js/list_script')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
<?= $this->render('/request/js/night_drive_training_request_scripts')?>
