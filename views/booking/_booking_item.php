<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Booking;
use app\models\UserNightPermit;
use app\models\BoatAvailableDays;
use app\models\BookingAlert;
use app\models\BookingAlertTiming;
use app\models\BookingSpecialBoatSelection;

$timings=$model->timings;
$ndAllowedLimitInMarina=$model->marina->night_drive_limit;

//isAvailable
$boatIsAvailableOnDay=Yii::$app->appHelperFunctions->checkBoatAvailability($model['id'],$searchModel->date);
$bulkBooked=Yii::$app->appHelperFunctions->checkBoatAvailabilityByBulkBooking($model['id'],$searchModel->date);
if($bulkBooked==true){
  echo "Bulk";
  $boatIsAvailableOnDay=false;
}
?>
<div class="row">
  <div class="col-xs-3 img-col col-sm-3">
    <?= Html::img(Yii::$app->fileHelperFunctions->getImagePath('boat',$model['image'],'large'),['alt'=>$model['name']]);?>
  </div>
  <div class="col-xs-9 detail-col col-sm-9">
    <p class="h3 title text-dark">
      <?= $model['name']?>
      <?php if($model['special_boat']==0){?>
      <a href="javascript:;" class="badge badge-info load-modal" data-url="<?= Url::to(['suggestion/boat-info','id'=>$model['id']])?>" data-heading="Boat Info">Info</a>
      <?php }?>
      <?php if($model['special_boat']==2){?>
         <span class="badge badge-success">New Service</span>
         <a href="javascript:;" class="btn btn-xs btn-info btn-new-service-2" style="padding: 0px 5px;">(I)</a>
      <?php }?>
    </p>
    <?php if($model['special_boat']==1){?>
      <?php if($model['name']=='Drop Off'){?>
        <div>8 Persons Maximum. (<a href="javascript:;" data-toggle="popover" data-placement="right" data-html="true" data-trigger="hover" title="Drop Off" data-content="This service can be used for:<br />• Transportation for your guests, when the boat's capacity you booked is not enough.<br />• If you want to go the Islands for more than the boating sessions.<br />• Can be used daily even if you have a boat booked on the same day.">?</a>)</div>
      <?php }?>
    <?php }?>
    <?php
    if($timings!=null){
      $fullyBooked=true;
      foreach($timings as $timing){
        if($boatIsAvailableOnDay==false){
          //If Boat is not available on the day selected.
          if($model['special_boat']==1){
            echo Html::a(Yii::t('app','Already Booked'),'javascript:;',['class'=>'btn btn-danger']);
          }else{
            echo '<div class="radio-custom disabled">';
            echo '  <input type="radio" name="boat_time" disabled="disabled" /> ';
            echo '  <label>';
            echo '    '.$timing['name'];
            echo '    </label>';
            echo ' </div>';
          }
        }else{
          //Check Already Booked
          $alreadyBooking=Booking::find()
          ->where([
            'booking_date'=>$searchModel->date,
            'city_id'=>$searchModel->city_id,
            'port_id'=>$searchModel->port_id,
            'boat_id'=>$model['id'],
            'booking_time_slot'=>$timing['id'],
            'is_archive'=>0,
            'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,
            'trashed'=>0,
          ]);
          if($alreadyBooking->exists()){
            if($model['special_boat']==1){
              //If its special boat
              echo '('.$timing['timing'].')<br />';
              echo Html::a(Yii::t('app','Already Booked'),'javascript:;',['class'=>'btn btn-danger']);
            }else{
              //If its normal boat
              echo '<div class="radio-custom disabled">';
              echo '  <input type="radio" name="boat_time" disabled="disabled" /> ';
              echo '  <label>';
              echo '    '.$timing['name'];
              echo '    </label>';
              echo ' </div>';
            }
          }else{
            $fullyBooked=false;
            $timeSlot=Yii::$app->appHelperFunctions->getTimeSlotById($timing['id']);
            $alertCls='';
            //Late Morning Booking
            if($timing['id']==1){
              if($searchModel->date==date("Y-m-d",strtotime("+1 day",strtotime(date("Y-m-d"))))){
                if(date("H")>date("H",mktime(Yii::$app->helperFunctions->lateMroningCheckTill,0,0,date("m"),date("d"),date("Y")))){
                  $checkMorningBookingExists=Booking::find()
                  ->where([
                    'and',
                    [
                      'city_id'=>$searchModel->city_id,
                      'port_id'=>$searchModel->port_id,
                      'DATE(booking_date)'=>$searchModel->date,
                      'booking_time_slot'=>$timing['id'],
        							'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,
                      'is_bulk'=>0,
                      'trashed'=>0,
                    ],
                    ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
                  ]);
                  if(!$checkMorningBookingExists->exists()){
                    $alertCls=' latemorningbooking';
                  }
                }
              }elseif($searchModel->date==date("Y-m-d")){
                $timeSlot=Yii::$app->appHelperFunctions->getTimeSlotById($timing['id']);
                if($timeSlot['available_till']!=null && date("H:i")<=$timeSlot['available_till']){
                  $checkMorningBookingExists=Booking::find()
                  ->where([
                    'and',
                    [
                      'city_id'=>$searchModel->city_id,
                      'port_id'=>$searchModel->port_id,
                      'DATE(booking_date)'=>$searchModel->date,
                      'booking_time_slot'=>$timing['id'],
                      'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,
                      'is_bulk'=>0,
                      'trashed'=>0,
                    ],
                    ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
                  ]);
                  if(!$checkMorningBookingExists->exists()){
                    $alertCls=' latemorningbooking';
                  }
                }
              }
            }

            if($searchModel->date==date("Y-m-d")){
              //Check if date is today & time is already ended

    					if($timeSlot['available_till']!=null && date("H:i")>=$timeSlot['available_till']){
                $alertCls=' too-late';
              }

              if($timeSlot['expiry']!=null && date("H:i")>=$timeSlot['expiry']){
                $alertCls=' already-passed';
              }

              if(in_array($model['id'],Yii::$app->appHelperFunctions->restrictedTodayBookingBoats)){
                $alertCls=' not-today';
              }

            }

            if($timeSlot['permit_required']==1){
              $nightDrivePermit=true;
              $nightDrivePermission=UserNightPermit::find()->where(['user_id'=>Yii::$app->user->identity->id,'port_id'=>$searchModel->port_id])->asArray()->one();
              if($nightDrivePermission==null){
                $alertCls=' no-nd-permit';
              }
            }

            $holdAlert=BookingAlert::find()
            ->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
            ->where([
              'alert_type'=>2,
              'alert_date'=>$searchModel->date,
              'port_id'=>$searchModel->port_id,
              BookingAlertTiming::tableName().'.time_slot_id'=>$timing['id'],
            	'trashed'=>0,
            ])
            ->asArray()->one();
            if($holdAlert!=null){
              $alertCls=' hold-alert_'.$timing['id'];
            }

            $warninglert=BookingAlert::find()
            ->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
            ->where([
              'alert_type'=>1,
              'alert_date'=>$searchModel->date,
              'port_id'=>$searchModel->port_id,
              BookingAlertTiming::tableName().'.time_slot_id'=>$timing['id'],
            	'trashed'=>0,
            ])
            ->asArray()->one();
            if($warninglert!=null){
              $addiotinalOptions['data-warning_alert']='warning_'.$timing['id'];
            }
            $bookingCls=' bookIt';
            if($model['special_boat']==1){
              $bookingCls=' spBookIt';
              $addiotinalOptions['id']='spboat_'.$model['id'];
              $addiotinalOptions['data-cnfm']='sp_'.$model['special_boat_type'];
              $addiotinalOptions['data-boat_sel']=$model['boat_selection'];
              $addiotinalOptions['data-boat_name']=$model['name'];
              $addiotinalOptions['data-boat_id']=$model['id'];
            }

            $addiotinalOptions['class']='btn btn-success btn-book'.($alertCls=='' ? $bookingCls : '').' time_'.$timing['id'];
            $addiotinalOptions['data-city_id']=$searchModel->city_id;
            $addiotinalOptions['data-marina_id']=$searchModel->port_id;
            $addiotinalOptions['data-date']=$searchModel->date;
            $addiotinalOptions['data-time_id']=$timing['id'];
            $addiotinalOptions['data-boat_id']=$model['id'];
            $btn=Html::a(
              Yii::t('app','Book').($model['special_boat']==1 ? ' '.$model['name'] : ''),
              'javascript:;',
              $addiotinalOptions
            );
            //No Booking found.
            if($model['special_boat']==1){
              //special boat
              echo '<div class="sp-boat '.$alertCls.'" data-boattype="sp" data-boat_name="'.$model['name'].'">';
              echo '  <div>('.$timing['timing'].')</div>';
              echo '  '.$btn;
              echo '</div>';
            }else{
              //Normal boat
              $showOption=true;

              if($timing['id']==Yii::$app->params['nightDriveTimeSlot']){
                //Today check if there is any night drive.
                if($searchModel->date==date("Y-m-d")){
                  $alreadyHaveNightDrive=Booking::find()->where(['port_id'=>$searchModel->port_id,'booking_date'=>$searchModel->date,'booking_time_slot'=>Yii::$app->params['nightDriveTimeSlot'],'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'trashed'=>0,'is_bulk'=>0]);
                  if(!$alreadyHaveNightDrive->exists()){
                    $showOption=false;
                  }
                }

                //If Night Drive and limit already reached
                $bookedCount=Booking::find()->where(['port_id'=>$searchModel->port_id,'booking_date'=>$searchModel->date,'booking_time_slot'=>Yii::$app->params['nightDriveTimeSlot'],'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'is_bulk'=>0,'trashed'=>0])->count('id');
                if($bookedCount>=$ndAllowedLimitInMarina){
                  $showOption=false;
                }

                //Check if this boat is already selected by special boat
                $subQueryActiveBooking=Booking::find()->select(['id'])->where(['port_id'=>$searchModel->port_id,'booking_date'=>$searchModel->date,'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'trashed'=>0]);
                $checkBooked=BookingSpecialBoatSelection::find()->where(['booking_id'=>$subQueryActiveBooking,'boat_id'=>$model['id']])->asArray()->one();
                if($checkBooked!=null){
                  $showOption=false;
                }
              }

              if($showOption==true){
                echo '<div class="radio-custom radio-success'.$alertCls.'" data-boattype="nrml" data-boat_name="'.$model['name'].'">';
                echo '  <input type="radio" name="boat_time" class="cbbt" id="boat_time_'.$model['id'].'_'.$timing['id'].'" /> ';
                echo '  <label for="boat_time_'.$model['id'].'_'.$timing['id'].'">';
                echo '    '.$timing['name'];
                echo '    '.$btn;
                echo '  </label>';
                echo '</div>';
              }else{
                echo '<div class="radio-custom disabled">';
                echo '  <input type="radio" name="boat_time" disabled="disabled" /> ';
                echo '  <label>';
                echo '    '.$timing['name'];
                echo '    </label>';
                echo ' </div>';
              }
            }
          }
        }
      }
      if($fullyBooked==true){
        $this->registerJs('
        $("li[data-key=\"'.$model['id'].'\"]").addClass("isFullBooked");
        ');
      }
    }
    ?>
  </div>
</div>
