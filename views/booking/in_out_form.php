<?php
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\BookingActivityAddons;
use app\assets\BookingActivityInOutAssets;
BookingActivityInOutAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingActivity */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'In Out Form');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operations'), 'url' => ['operations/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activity'), 'url' => ['activity']];
$this->params['breadcrumbs'][] = $this->title;

$minPetrolConsumption=Yii::$app->appHelperFunctions->getMinPetrolConsumption($modelBooking->port_id);

$marinaStayInIds=Yii::$app->appHelperFunctions->marinaStayInIds;

if($model->dep_crew_sign=='')$this->registerJs('$("#depCrew_sign").jSignature({height:100,width:220});');
if($model->dep_mem_sign=='')$this->registerJs('$("#depMem_sign").jSignature({height:100,width:220});');
if($model->arr_crew_sign=='')$this->registerJs('$("#arrCrew_sign").jSignature({height:100,width:220});');
if($model->arr_mem_sign=='')$this->registerJs('$("#arrMem_sign").jSignature({height:100,width:220});');
?>
<style>
.signatures-tbl .img-block{width:150px;height:150px;border:1px solid #ccc;padding:10px; text-align:center;}
.signatures-tbl img{height:100%; max-width: 100%;}
.experience-info .help-block{display: none;}
.experience-info .form-group{margin-bottom: 0px;}
</style>
<section class="card card-featured-top card-featured-warning card-collapsed">
  <header class="card-header">
    <div class="card-actions">
      <a href="javascript:;" class="card-action card-action-toggle" data-card-toggle=""></a>
    </div>
    <h2 class="card-title">Member Info</h2>
  </header>
  <div class="card-body" style="display:none;">
    <div class="widget-summary widget-summary-xlg">
      <div class="widget-summary-col widget-summary-col-icon">
        <div class="summary-icon">
          <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('user',$modelBooking->member->image,'tiny')?>" alt="" />
        </div>
      </div>
      <div class="widget-summary-col">
        <div class="summary">
          <strong>Username:</strong> <?= $modelBooking->member->username?><br />
          <strong>Name:</strong> <?= $modelBooking->member->firstname.' '.$modelBooking->member->lastname?><br />
          <?= $modelBooking->boatName.' '.Yii::$app->formatter->asDate($modelBooking->booking_date).' - '.$modelBooking->timeZone->name?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="card card-featured-top card-featured-warning" style="margin-top:10px;">
  <header class="card-header">
    <h2 class="card-title">In Out Form</h2>
  </header>
  <?php $form = ActiveForm::begin(['id'=>'inoutForm','options'=>['enctype'=>'multipart/form-data']]); ?>
  <div class="card-body table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th></th>
          <th><?= Yii::t('app','Departure')?></th>
          <th><?= Yii::t('app','Arrival')?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?= $model->getAttributeLabel('dep_time')?></td>
          <td><?= $form->field($model, 'dep_time')->textInput(['maxlength' => true])->label(false)?></td>
          <td><?= $form->field($model, 'arr_time')->textInput(['maxlength' => true])->label(false)?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('dep_engine_hours')?></td>
          <td><?= $form->field($model, 'dep_engine_hours')->textInput(['maxlength' => true])->label(false)?></td>
          <td><?= $form->field($model, 'arr_engine_hours')->textInput(['maxlength' => true])->label(false)?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('dep_ppl')?></td>
          <td><?= $form->field($model, 'dep_ppl')->textInput(['maxlength' => true])->label(false)?></td>
          <td><?= $form->field($model, 'arr_ppl')->textInput(['maxlength' => true])->label(false)?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('dep_fuel_level')?></td>
          <td><?= $form->field($model, 'dep_fuel_level')->textInput(['maxlength' => true])->label(false)?></td>
          <td><?= $form->field($model, 'arr_fuel_level')->textInput(['maxlength' => true])->label(false)?></td>
        </tr>
        <?php if(!in_array($modelBooking->boat_id,$marinaStayInIds)){?>
          <tr>
            <td><?= $model->getAttributeLabel('fuel_chargeable')?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'fuel_chargeable')->dropDownList(['1'=>'Yes','0'=>'No'])->label(false)?></td>
          </tr>
          <tr id="no-charge-comments"<?= $model->fuel_chargeable==1 ? ' style="display:none;"' : ''?>>
            <td><?= $model->getAttributeLabel('no_charge_comment')?></td>
            <td>&nbsp;</td>
            <td><?= $form->field($model, 'no_charge_comment')->textInput(['maxlength' => true])->label(false)?></td>
          </tr>
        <?php }?>
        <tr id="fuel-cost-row" style="<?= in_array($modelBooking->boat_id,$marinaStayInIds) || $model->fuel_chargeable==0 ? 'display:none;' : ''?>">
          <td><?= $model->getAttributeLabel('fuel_cost')?></td>
          <td>&nbsp;</td>
          <td>
            <?php if($model->id!=null && $model->fuel_cost>0 && $model->hide_other==1){?>
              <div class="hidden">
                <?= $form->field($model, 'fuel_cost',['template'=>'
                {label}
                <div class="input-group">
                <span class="input-group-prepend">
                <span class="input-group-text">
                AED
                </span>
                </span>
                {input}
                </div>
                {hint}{error}
                '])->textInput(['maxlength' => true])->label(false)?>
              </div>
              <div class="form-group">
                <div class="input-group">
                <span class="input-group-prepend">
                <span class="input-group-text">
                AED
                </span>
                </span>
                  <input type="text" class="form-control" aria-invalid="false" readonly="readonly" value="<?= $model->fuel_cost?>" />
                </div>
              </div>
            <?php }else{?>
              <?= $form->field($model, 'fuel_cost',['template'=>'
              {label}
              <div class="input-group">
              <span class="input-group-prepend">
              <span class="input-group-text">
              AED
              </span>
              </span>
              {input}
              </div>
              {hint}{error}
              '])->textInput(['maxlength' => true])->label(false)?>
            <?php }?>
          </td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('charge_bbq')?></td>
          <td>&nbsp;</td>
          <td><?= $form->field($model, 'charge_bbq')->dropDownList(['0'=>'No','1'=>'Yes'])->label(false)?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('charge_captain')?></td>
          <td>&nbsp;</td>
          <td><?= $form->field($model, 'charge_captain')->dropDownList(['0'=>'No','1'=>'Yes'])->label(false)?></td>
        </tr>
        <tr<?= $model->booking->port_id!=Yii::$app->params['emp_id'] ? ' style="display:none;"' : ''?>>
        <td>Upload Fuel Bill</td>
        <td>&nbsp;</td>
        <td>
          <?php if($model->bill_image!=null){
          $this->registerJs('
          $(document).delegate(".btn-remove-bill", "click", function() {
            var _targetContainer=".request-service-form";
            swal({
              title: "Confirmation",
              html: "'.Yii::t('app','Are you sure you want to delete the bill').'",
              type: "info",
              showCancelButton: true,
              confirmButtonColor: "#47a447",
              confirmButtonText: "'.Yii::t('app','Yes Delete').'",
              cancelButtonText: "'.Yii::t('app','Cancel').'",
            }).then(function (result) {
  						if (result.value) {
                App.blockUI({
                  message: "'.Yii::t('app','Please wait...').'",
                  target: _targetContainer,
                  overlayColor: "none",
                  cenrerY: true,
                  boxed: true
                });
                $.ajax({
                  url: "'.Url::to(['booking/delete-bill','id'=>$model->id]).'",
                  type: "POST",
                  success: function(response) {
                    response = jQuery.parseJSON(response);
                    App.unblockUI($(_targetContainer));
                    if(response["success"]){
                      swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
                      $("#bill-img-container").remove();
                      $("#upload-fld").show();
                    }else{
                      swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
                    }
                  },
                  error: bbAlert
                });
              }
            });
          });
          ');
          if(filter_var($model->bill_image, FILTER_VALIDATE_URL)){
            echo '<span id="bill-img-container">'.Html::a('View Uploaded Bill',$model->bill_image,['class'=>'btn btn-xs btn-success','target'=>'_blank']).' <a href="javascript:;" class="btn btn-xs btn-danger btn-remove-bill"><i class="fa fa-times"></i></a></span><span id="upload-fld" style="display:none;">'.$form->field($model, 'bill_image')->fileInput(['accept'=>'image/*'])->label(false).'</span>';
          }elseif(file_exists(Yii::$app->params['fuelbill_abs_path'].$model->bill_image)){
            echo '<span id="bill-img-container">'.Html::a('View Uploaded Bill',Yii::$app->fileHelperFunctions->getImagePath('fuelbill',$model->bill_image,'full'),['class'=>'btn btn-xs btn-success','target'=>'_blank']).' <a href="javascript:;" class="btn btn-xs btn-danger btn-remove-bill"><i class="fa fa-times"></i></a></span><span id="upload-fld" style="display:none;">'.$form->field($model, 'bill_image')->fileInput(['accept'=>'image/*'])->label(false).'</span>';
          }
        }else{?><?= $form->field($model, 'bill_image')->fileInput(['accept'=>'image/*'])->label(false)?><?php }?>
        </td>
      </tr>
      <tr>
        <td><?= $model->getAttributeLabel('damage_comments')?></td>
        <td>&nbsp;</td>
        <td>
          <a href="javascript:;" data-toggle="modal" data-target="#itemsList"><?= Yii::t('app','Click here to add from the list')?></a>
          <?= $form->field($model, 'damage_comments')->textInput(['maxlength' => true])->label(false)?>
        </td>
      </tr>
    </tbody>

    <tfoot>
      <tr>
        <td><?= $model->getAttributeLabel('dep_crew_sign')?></td>
        <td>
          <table class="table signatures-tbl">
            <tr>
              <td  style="vertical-align:top;">
                <label>Crew Sign <?php if($model->dep_crew_sign==''){?>(<a href="javascript:;" class="reset-sign" data-sdid="depCrew" data-iid="dep_crew_sign">reset</a>)<?php }?></label>
                <?php if($model->dep_crew_sign!=''){?>
                  <div class="img-block"><img src="<?= $model->dep_crew_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->dep_crew_sign,'full')?>" /></div>
                <?php }else{?>
                  <div id="depCrew_sign" class="sign-container"></div>
                <?php }?>
                <div style="display:none"><?= $form->field($model, 'dep_crew_sign')->textarea(['rows' => 4])->label(false) ?></div>
              </td>
              <td  style="vertical-align:top;">
                <label>Member Sign <?php if($model->dep_mem_sign==''){?>(<a href="javascript:;" class="reset-sign" data-sdid="depMem" data-iid="dep_mem_sign">reset</a>)<?php }?></label>
                <?php if($model->dep_mem_sign!=''){?>
                  <div class="img-block"><img src="<?= $model->dep_mem_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->dep_mem_sign,'full')?>" /></div>
                <?php }else{?>
                  <div id="depMem_sign" class="sign-container"></div>
                <?php }?>
                <div style="display:none"><?= $form->field($model, 'dep_mem_sign')->textarea(['rows' => 4])->label(false) ?></div>
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table class="table signatures-tbl">
            <tr>
              <td  style="vertical-align:top;">
                <label>Crew Sign <?php if($model->arr_crew_sign==''){?>(<a href="javascript:;" class="reset-sign" data-sdid="arrCrew" data-iid="arr_crew_sign">reset</a>)<?php }?></label>
                <?php if($model->arr_crew_sign!=''){?>
                  <div class="img-block"><img src="<?= $model->arr_crew_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->arr_crew_sign,'full')?>" /></div>
                <?php }else{?>
                  <div id="arrCrew_sign" class="sign-container"></div>
                <?php }?>
                <div style="display:none"><?= $form->field($model, 'arr_crew_sign')->textarea(['rows' => 4])->label(false) ?></div>
              </td>
              <td  style="vertical-align:top;">
                <label>Member Sign <?php if($model->arr_mem_sign==''){?>(<a href="javascript:;" class="reset-sign" data-sdid="arrMem" data-iid="arr_mem_sign">reset</a>)<?php }?></label>
                  <?php if($model->arr_mem_sign!=''){?>
                    <div class="img-block"><img src="<?= $model->arr_mem_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->arr_mem_sign,'full')?>" /></div>
                  <?php }else{?>
                    <div id="arrMem_sign" class="sign-container"></div>
                  <?php }?>
                  <div style="display:none"><?= $form->field($model, 'arr_mem_sign')->textarea(['rows' => 4])->label(false) ?></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </tfoot>
  </table>
  <?= $form->field($model, 'boat_provided')->dropDownList(Yii::$app->appHelperFunctions->getActiveBoatsListArr($modelBooking->city_id,$modelBooking->port_id),['onchange'=>'ChangeReason()','tabindex'=>'1']) ?>
<style>
.experience-info label{width: 100%;}
.experience-info td{vertical-align: top;}
</style>
  <table class="table table-bordered experience-info">
    <tr>
      <th width="20%">Booking</th>
      <th width="30%">Trip</th>
      <th width="30%">Member</th>
      <th width="20%">Extras</th>
    </tr>
    <tr>
      <td>
        <?php
        foreach(Yii::$app->helperFunctions->bookingExperience as $key=>$val){
          if($model->booking_exp==$key){
            $checked=' checked="checked"';
            $disabled='';
          }else{
            $disabled=' disabled="disabled"';
            $checked='';
          }
        ?>
          <label>
            <input type="radio" id="bExp<?= $key?>" name="BookingActivity[booking_exp]" class="bexp" value="<?= $key?>"<?=  $checked.$disabled?> />
            <?= $val?>
          </label>
        <?php }?>
      </td>
      <td>
        <?php foreach(Yii::$app->helperFunctions->tripExperience as $key=>$val){?>
          <label>
            <input type="radio" name="BookingActivity[trip_exp]" class="texp" value="<?= $key?>"<?= $model->trip_exp==$key ? ' checked="checked"' : ''?> />
            <?= $val?>
          </label>
        <?php }?>
        <div id="tripExpCom"<?= $model->trip_exp==1 || $model->trip_exp==null ? ' style="display:none;"' : ''?> class="remarks">
          <?= $form->field($model, 'trip_exp_comments')->textInput(['maxlength' => true])->label('Please specify details')?>
        </div>
      </td>
      <td>
        <?php foreach(Yii::$app->helperFunctions->memberExperience as $key=>$val){?>
          <?php if($key==1){?>
            <?= $form->field($model, 'member_exp')->checkbox(array('label'=>$val)); ?>
          <div id="lateArrInfo"<?= $model->member_exp==1 ? '' : ' style="display:none;"'?> class="remarks">
            <div class="row">
              <div class="col-sm-6">
                <?= $form->field($model, 'member_exp_late_hr',['template'=>'
                {label}
                <div class="input-group">
                {input}
                <span class="input-group-append">
    							<span class="input-group-text">
    								Hr(s)
    							</span>
    						</span>
                </div>
                {hint}{error}
                '])->textInput()->label(false) ?>
              </div>
              <div class="col-sm-6">
                <?= $form->field($model, 'member_exp_late_min',['template'=>'
                {label}
                <div class="input-group">
                {input}
                <span class="input-group-append">
    							<span class="input-group-text">
    								Min(s)
    							</span>
    						</span>
                </div>
                {hint}{error}
                '])->textInput()->label(false) ?>
              </div>
            </div>
            <?= $form->field($model, 'member_exp_comments')->textInput(['maxlength' => true])->label('Late Arrival Remarks')?>
          </div>
          <?php }?>
          <?php if($key==2){?>
            <?= $form->field($model, 'dui')->checkbox(array('label'=>$val)); ?>
            <div id="duiInfo"<?= $model->dui==1 ? '' : ' style="display:none;"'?> class="remarks">
              <?= $form->field($model, 'dui_comments')->textInput(['maxlength' => true])->label('DUI Remarks')?>
            </div>
          <?php }?>
        <?php }?>
      </td>
      <td>
        <?php
        foreach(Yii::$app->helperFunctions->bookingExtras as $key=>$val){
          $checked='';
          $beRow=BookingActivityAddons::find()->where(['booking_id'=>$model->booking_id,'addon_id'=>$key]);
          if($beRow->exists()){
            $checked=' checked="checked"';
          }
        ?>
          <label>
            <input type="checkbox" name="BookingActivity[booking_extra][]" class="bextra" value="<?= $key?>"<?= $checked?> />
            <?= $val?>
          </label>
        <?php }?>
      </td>
    </tr>
  </table>
  <?= $form->field($model, 'overall_remarks')->textarea(['rows' => 4]) ?>
  <?php if($model->id==null || $model->hide_other==0){?>
    <?= $form->field($model, 'hide_other')->dropDownList(['0'=>'No','1'=>'Yes']) ?>
  <?php }?>
</div>
<?php
Modal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'itemsList',
  'size' => 'modal-lg',
 	'header' => '<h5 class="modal-title">Select Damaged Items from the list</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton' => false,
  'footer' => '
  <button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  ',
]);
echo '<div class="modalContent">';
echo $this->render('/damage-check-item/_in_out_form_list', ['model' => $model,]);
echo '</div>';
Modal::end();
?>
<div class="card-footer">
  <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
  <?= Html::a('Cancel',(Yii::$app->session->get('bookingReturnUrl')!=null ? Yii::$app->session->get('bookingReturnUrl') : ['index']),['class'=>'btn btn-default pull-right'])?>
</div>
<?php ActiveForm::end(); ?>
</section>
<?= $this->render('/booking/js/activity_form_script',['minPetrolConsumption'=>$minPetrolConsumption,'model'=>$model,'modelBooking'=>$modelBooking,'marinaStayInIds'=>$marinaStayInIds])?>
