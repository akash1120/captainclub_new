<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Booking;
use app\models\UserNightPermit;
use app\models\BoatAvailableDays;

$timings=$model->timings;

//isAvailable
$boatIsAvailableOnDay=Yii::$app->appHelperFunctions->checkBoatAvailability($model['id'],$searchModel->date)
?>
<div class="row<?= $boatIsAvailableOnDay==false ? ' hide-boat' : ''?>">
  <div class="col-xs-12 col-sm-3">
    <?= Html::img(Yii::$app->fileHelperFunctions->getImagePath('boat',$model['image'],'large'),['alt'=>$model['name']]);?>
  </div>
  <div class="col-xs-12 col-sm-9">
    <p class="h3 title text-dark">
      <?= $model['name']?>
      <?php if($model['special_boat']==0){?>
      <a href="javascript:;" class="badge badge-info load-modal" data-url="<?= Url::to(['suggestion/boat-info','id'=>$model['id']])?>" data-heading="Boat Info">Info</a>
    <?php }?>
    </p>
    <?php if($model['special_boat']==1){?>
      <?php if($model['name']=='Drop Off'){?>
        <div>8 Persons Maximum. (<a href="javascript:;" data-toggle="popover" data-placement="right" data-html="true" data-trigger="hover" title="Drop Off" data-content="This service can be used for:<br />• Transportation for your guests, when the boat's capacity you booked is not enough.<br />• If you want to go the Islands for more than the boating sessions.<br />• Can be used daily even if you have a boat booked on the same day.">?</a>)</div>
      <?php }?>
    <?php }?>
    <?php
    if($timings!=null){
      foreach($timings as $timing){
        //If Its not entirly Available
        if($boatIsAvailableOnDay==false){
          if($model['special_boat']==1){
            echo Html::a(Yii::t('app','Already Booked'),'javascript:;',['class'=>'btn btn-danger']);
          }else{
            echo '<div class="radio-custom disabled">';
            echo '  <input type="radio" name="boat_time" disabled="disabled" /> ';
            echo '  <label>';
            echo '    '.$timing['name'];
            echo '    </label>';
            echo ' </div>';
          }
        }else{
          //Check Already Booked
          $alreadyBooking=Booking::find()
          ->where([
            'booking_date'=>$searchModel->date,
            'city_id'=>$searchModel->city_id,
            'port_id'=>$searchModel->port_id,
            'boat_id'=>$model['id'],
            'booking_time_slot'=>$timing['id'],
            'is_archive'=>0,
            'status'=>1,
            'trashed'=>0,
          ]);

          //Check Time is late
          if($model['special_boat']==1){
            echo '('.$timing['timing'].')<br />';
            if($alreadyBooking->exists()){
              echo Html::a(Yii::t('app','Already Booked'),'javascript:;',['class'=>'btn btn-danger']);
            }else{
              echo Html::a(Yii::t('app','Book').' '.$model['name'],'javascript:;',['class'=>'btn btn-success time_'.$timing['id']]);
            }
          }else{
            if($alreadyBooking->exists()){
              echo '<div class="radio-custom disabled">';
              echo '  <input type="radio" name="boat_time" disabled="disabled" /> ';
              echo '  <label>';
              echo '    '.$timing['name'];
              echo '    </label>';
              echo ' </div>';
            }else{
              $alertCls='';
              //Late Morning Booking
              if($timing['id']==1){
                if($searchModel->date==date("Y-m-d",mktime(0,0,0,date("m"),(date("d")+1),date("Y")))){
                  if(date("H")>date("H",mktime(20,0,0,date("m"),date("d"),date("Y")))){
                    $checkMorningBookingExists=Booking::find()
                    ->where([
                      'and',
                      [
                        'city_id'=>$searchModel->city_id,
                        'port_id'=>$searchModel->port_id,
                        'DATE(booking_date)'=>$searchModel->date,
                        'booking_time_slot'=>$timing['id'],
                        'is_bulk'=>0,
                        'trashed'=>0,
                      ],
                      ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
                    ]);
                    if(!$checkMorningBookingExists->exists()){
                      $alertCls=' latemorningbooking';
                    }
                  }
                }elseif($searchModel->date==date("Y-m-d")){
                  if(date("H:i")<=Yii::$app->helperFunctions->sameDayTimeCheck[$timing['id']]){
                    $checkMorningBookingExists=Booking::find()
                    ->where([
                      'and',
                      [
                        'city_id'=>$searchModel->city_id,
                        'port_id'=>$searchModel->port_id,
                        'DATE(booking_date)'=>$searchModel->date,
                        'booking_time_slot'=>$timing['id'],
                        'is_bulk'=>0,
                        'trashed'=>0,
                      ],
                      ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
                    ]);
                    if(!$checkMorningBookingExists->exists()){
                      $alertCls=' latemorningbooking';
                    }
                  }
                }
              }

              if($searchModel->date==date("Y-m-d")){
                //Check if date is today & time is already ended
                if(Yii::$app->helperFunctions->isTimeAlreadyPassed(Yii::$app->helperFunctions->sameDayTimeCheck[$timing['id']],date("H:i"))){
                  echo date("H:i").'>='.Yii::$app->helperFunctions->sameDayTimeCheck[$timing['id']].'<hr />';
                  $alertCls=' already-passed';
                }
              }

              if($timing['id']==Yii::$app->params['nightDriveTimeSlot']){
                $nightDrivePermit=true;
          			$nightDrivePermission=UserNightPermit::find()->where(['user_id'=>Yii::$app->user->identity->id,'port_id'=>$searchModel->port_id])->asArray()->one();
          			if($nightDrivePermission==null){
          				$alertCls=' no-nd-permit';
          			}
              }




              echo '<div class="radio-custom radio-success'.$alertCls.'">';
              echo '  <input type="radio" name="boat_time" class="cbbt" id="boat_time_'.$model['id'].'_'.$timing['id'].'" /> ';
              echo '  <label for="boat_time_'.$model['id'].'_'.$timing['id'].'">';
              echo '    '.$timing['name'];
              echo '    <a href="javascript:;" class="btn btn-success btn-sm btn-book bookIt" data-city_id="'.$searchModel->city_id.'" data-marina_id="'.$searchModel->port_id.'" data-date="'.$searchModel->date.'" data-time_id="'.$timing['id'].'" data-boat_id="'.$model['id'].'">Book</a>';
              echo '  </label>';
              echo '</div>';
            }
          }
        }
      }
    }
    ?>
  </div>
</div>
