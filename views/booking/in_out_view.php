<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\BookingActivityAddons;
use app\assets\BookingActivityInOutAssets;
BookingActivityInOutAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingActivity */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('app', 'In Out Form');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operations'), 'url' => ['operations/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activity'), 'url' => ['activity']];
$this->params['breadcrumbs'][] = $this->title;

$minPetrolConsumption=Yii::$app->appHelperFunctions->getMinPetrolConsumption($modelBooking->port_id);

$marinaStayInIds=Yii::$app->appHelperFunctions->marinaStayInIds;
?>
<style>
.signatures-tbl .img-block{width:150px;height:150px;border:1px solid #ccc;padding:10px; text-align:center;}
.signatures-tbl img{height:100%; max-width: 100%;}
</style>
<section class="card card-featured-top card-featured-warning card-collapsed">
  <header class="card-header">
    <div class="card-actions">
      <a href="javascript:;" class="card-action card-action-toggle" data-card-toggle=""></a>
    </div>
    <h2 class="card-title">Member Info</h2>
  </header>
  <div class="card-body" style="display:none;">
    <div class="widget-summary widget-summary-xlg">
      <div class="widget-summary-col widget-summary-col-icon">
        <div class="summary-icon">
          <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('user',$modelBooking->member->image,'tiny')?>" alt="" />
        </div>
      </div>
      <div class="widget-summary-col">
        <div class="summary">
          <strong>Username:</strong> <?= $modelBooking->member->username?><br />
          <strong>Name:</strong> <?= $modelBooking->member->firstname.' '.$modelBooking->member->lastname?><br />
          <?= ($modelBooking->boat!=null ? $modelBooking->boat->name : 'Boat Not found!').' '.Yii::$app->formatter->asDate($modelBooking->booking_date).' - '.$modelBooking->timeZone->name?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="card card-featured-top card-featured-warning" style="margin-top:10px;">
  <header class="card-header">
    <h2 class="card-title">In Out Form</h2>
  </header>
  <div class="card-body">
    <table class="table">
      <thead>
        <tr>
          <th width="50%">&nbsp;</th>
          <th width="25%"><?= Yii::t('app','Departure')?></th>
          <th width="25%"><?= Yii::t('app','Arrival')?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td width="50%"><?= $model->getAttributeLabel('dep_time')?></td>
          <td width="25%"><?= $model->dep_time?></td>
          <td width="25%"><?= $model->arr_time?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('dep_engine_hours')?></td>
          <td><?= $model->dep_engine_hours?></td>
          <td><?= $model->arr_engine_hours?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('dep_ppl')?></td>
          <td><?= $model->dep_ppl?></td>
          <td><?= $model->arr_ppl?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('dep_fuel_level')?></td>
          <td><?= $model->dep_fuel_level?></td>
          <td><?= $model->arr_fuel_level?></td>
        </tr>
        <?php if(!in_array($modelBooking->boat_id,$marinaStayInIds)){?>
          <tr>
            <td><?= $model->getAttributeLabel('fuel_chargeable')?></td>
            <td>&nbsp;</td>
            <td><?= Yii::$app->helperFunctions->arrYesNo[$model->fuel_chargeable]?></td>
          </tr>
          <tr id="no-charge-comments"<?= $model->fuel_chargeable==1 ? ' style="display:none;"' : ''?>>
            <td><?= $model->getAttributeLabel('no_charge_comment')?></td>
            <td>&nbsp;</td>
            <td><?= $model->no_charge_comment?></td>
          </tr>
        <?php }?>
        <tr id="fuel-cost-row" style="<?= in_array($modelBooking->boat_id,$marinaStayInIds) || $model->fuel_chargeable==0 ? 'display:none;' : ''?>">
          <td><?= $model->getAttributeLabel('fuel_cost')?></td>
          <td>&nbsp;</td>
          <td>AED <?= $model->fuel_cost?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('charge_bbq')?></td>
          <td>&nbsp;</td>
          <td><?= Yii::$app->helperFunctions->arrYesNo[$model->charge_bbq]?></td>
        </tr>
        <tr>
          <td><?= $model->getAttributeLabel('charge_captain')?></td>
          <td>&nbsp;</td>
          <td><?= Yii::$app->helperFunctions->arrYesNo[$model->charge_captain]?></td>
        </tr>
        <tr<?= $model->booking->port_id!=Yii::$app->params['emp_id'] ? ' style="display:none;"' : ''?>>
        <td>Upload Fuel Bill</td>
        <td>&nbsp;</td>
        <td>
          <?php if(filter_var($model->bill_image, FILTER_VALIDATE_URL)){
          echo '<span id="bill-img-container">'.Html::a('View Uploaded Bill',$model->bill_image,['class'=>'btn btn-xs btn-success','target'=>'_blank']);
        }elseif($model->bill_image!=null && file_exists(Yii::$app->params['fuelbill_abs_path'].$model->bill_image)){
          echo '<span id="bill-img-container">'.Html::a('View Uploaded Bill',Yii::$app->fileHelperFunctions->getImagePath('fuelbill',$model->bill_image,'full'),['class'=>'btn btn-xs btn-success','target'=>'_blank']);
        }?>
        </td>
      </tr>
      <tr>
        <td><?= $model->getAttributeLabel('damage_comments')?></td>
        <td>&nbsp;</td>
        <td>
          <?= $model->damage_comments?>
        </td>
      </tr>
    </tbody>

    <tfoot>
      <tr>
        <td><?= $model->getAttributeLabel('dep_crew_sign')?></td>
        <td>
          <table class="table signatures-tbl">
            <tr>
              <td style="vertical-align:top">
                <label>Crew Sign <?php if($model->dep_crew_sign==''){?>(<a href="javascript:;" class="reset-sign" data-id="dep_crew">reset</a>)<?php }?></label>
                <?php if($model->dep_crew_sign!=''){?>
                  <div class="img-block"><img src="<?= $model->dep_crew_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->dep_crew_sign,'full')?>" /></div>
                <?php }?>
              </td>
              <td style="vertical-align:top">
                <label>Member Sign <?php if($model->dep_mem_sign==''){?>(<a href="javascript:;" class="reset-sign" data-id="dep_mem">reset</a>)<?php }?></label>
                <?php if($model->dep_mem_sign!=''){?>
                  <div class="img-block"><img src="<?= $model->dep_mem_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->dep_mem_sign,'full')?>" /></div>
                <?php }?>
              </td>
            </tr>
          </table>
        </td>
        <td>
          <table class="table signatures-tbl">
            <tr>
              <td style="vertical-align:top">
                <label>Crew Sign <?php if($model->arr_crew_sign==''){?>(<a href="javascript:;" class="reset-sign" data-id="arr_crew">reset</a>)<?php }?></label>
                <?php if($model->arr_crew_sign!=''){?>
                  <div class="img-block"><img src="<?= $model->arr_crew_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->arr_crew_sign,'full')?>" /></div>
                <?php }?>
              </td>
              <td style="vertical-align:top">
                <label>Member Sign <?php if($model->arr_mem_sign==''){?>(<a href="javascript:;" class="reset-sign" data-id="arr_mem">reset</a>)<?php }?></label>
                  <?php if($model->arr_mem_sign!=''){?>
                    <div class="img-block"><img src="<?= $model->arr_mem_sign;//Yii::$app->fileHelperFunctions->getImagePath('signature',$model->arr_mem_sign,'full')?>" /></div>
                  <?php }?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </tfoot>
  </table>
  <strong>Boat Provided:</strong> <?= $model->boatProvided->name?><br /><br />
<style>
.experience-info label{width: 100%;}
.experience-info td{vertical-align: top;}
</style>
  <table class="table table-bordered experience-info">
    <tr>
      <th width="25%">Booking</th>
      <th width="25%">Trip</th>
      <th width="25%">Member</th>
      <th width="25%">Extras</th>
    </tr>
    <tr>
      <td>
        <?php
        foreach(Yii::$app->helperFunctions->bookingExperience as $key=>$val){
          if($model->booking_exp==$key){
            echo $val;
          }
        }
        ?>
      </td>
      <td>
        <?php
        foreach(Yii::$app->helperFunctions->tripExperience as $key=>$val){
          if($model->trip_exp==$key){
            echo $val.($model->trip_exp_comments!='' ? ' - '.$model->trip_exp_comments : '');
          }
        }
        ?>
      </td>
      <td>
        <?php
        foreach(Yii::$app->helperFunctions->memberExperience as $key=>$val){
          if($key==1){
            if($model->member_exp==1){
              echo '<div>'.$model->memberExperienceComments.'</div>';
            }
          }
          if($key==2){
            if($model->dui==1){
              echo '<div>'.$val.($model->dui_comments!='' ? ' - '.$model->dui_comments : '').'</div>';
            }
          }
        }
        ?>
      </td>
      <td>
        <?php
        foreach(Yii::$app->helperFunctions->bookingExtras as $key=>$val){
          $checked='';
          $beRow=BookingActivityAddons::find()->where(['booking_id'=>$model->booking_id,'addon_id'=>$key]);
          if($beRow->exists()){
            echo '<div>'.$val.'</div>';
          }
        }
        ?>
      </td>
    </tr>
  </table>
  <strong>Overall Remarks:</strong><br /><?= nl2br($model->overall_remarks)?>
</div>
</section>
<?= $this->render('/booking/js/activity_form_script',['minPetrolConsumption'=>$minPetrolConsumption,'model'=>$model,'modelBooking'=>$modelBooking,'marinaStayInIds'=>$marinaStayInIds])?>
