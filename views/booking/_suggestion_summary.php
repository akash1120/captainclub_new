<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Boat;
use app\models\Booking;
use app\models\BoatToTimeSlot;
use app\models\BookingSuggestionForm;
use app\models\BookingAlert;
use app\models\BookingAlertTiming;
use app\models\UserNightPermit;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

list($sy,$sm,$sd)=explode("-",$date);
$dayOfWeek=date("w",mktime(0,0,0,$sm,$sd,$sy));

$timeSlotsArr=Yii::$app->appHelperFunctions->timeSlotListDashboard;
$marinas=Yii::$app->appHelperFunctions->getActiveCityMarinaList($city_id);

if($marinas!=null){
$model = new BookingSuggestionForm();
$model->city_id=$city_id;
$model->date=$date;
?>
<style>
label.btn {
    position: relative;
    overflow: hidden;
}
label.btn input {
    position: absolute;
    left: -20px;
}
</style>
<?php $form = ActiveForm::begin(['id'=>'bookSuggestionPopForm','action'=>Url::to(['booking/create-from-suggestion'])]); ?>
<div style="display:none;">
	<?= $form->field($model, 'city_id')->textInput() ?>
	<?= $form->field($model, 'date')->textInput(['id'=>'sug-date']) ?>
</div>
<h3>The Following Boats are available on the day you choose.</h3>
Click on the number to view the boats.
<div class=" tbl-container">
	<table class="table table-bordered table-stripped table-responsive-md">
		<tr>
			<th>Marina</th>
			<?php foreach($timeSlotsArr as $timeSlot){?>
			<th style="text-align:center;"><?= $timeSlot['dashboard_name']?></th>
			<?php }?>
		</tr>
		<?php
		foreach($marinas as $marina){
		?>
		<tr>
			<th><?= $marina['name']?></th>
			<?php
			foreach($timeSlotsArr as $timeSlot){
				$freeAvailable=Yii::$app->statsFunctions->getMarinaDateTimeFreeBoats($marina['id'],$model->date,$timeSlot['id'],$dayOfWeek);
			?>
			<td style="text-align:center;">
				<?php
				$html = '';
				$alertCls='';
				if($timeSlot['id']==1){
					if($model->date==date("Y-m-d",strtotime("+1 day",strtotime(date("Y-m-d"))))){
						if(date("H")>date("H",mktime(Yii::$app->helperFunctions->lateMroningCheckTill,0,0,date("m"),date("d"),date("Y")))){
							$checkMorningBookingExists=Booking::find()
							->where([
								'and',
								[
									'city_id'=>$model->city_id,
									'port_id'=>$marina['id'],
									'DATE(booking_date)'=>$model->date,
									'booking_time_slot'=>$timeSlot['id'],
									'status'=>1,
									'is_bulk'=>0,
									'trashed'=>0,
								],
								['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
							]);
							if(!$checkMorningBookingExists->exists()){
								$alertCls=' latemorningbooking';
							}
						}
					}elseif($model->date==date("Y-m-d")){
						$timeSlot=Yii::$app->appHelperFunctions->getTimeSlotById($timeSlot['id']);
						if($timeSlot['available_till']!=null && date("H:i")<=$timeSlot['available_till']){
							$checkMorningBookingExists=Booking::find()
							->where([
								'and',
								[
									'city_id'=>$model->city_id,
									'port_id'=>$marina['id'],
									'DATE(booking_date)'=>$model->date,
									'booking_time_slot'=>$timeSlot['id'],
									'status'=>1,
									'is_bulk'=>0,
									'trashed'=>0,
								],
								['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
							]);
							if(!$checkMorningBookingExists->exists()){
								$alertCls=' latemorningbooking';
							}
						}
					}
				}
				if($model->date==date("Y-m-d")){
					//Check if date is today & time is already ended
					if($timeSlot['available_till']!=null && date("H:i")>=$timeSlot['available_till']){
						$alertCls=' too-late';
					}

					if($timeSlot['expiry']!=null && date("H:i")>=$timeSlot['expiry']){
						$alertCls=' already-passed';
					}
				}

				if($timeSlot['permit_required']==1){
					$nightDrivePermit=true;
					$nightDrivePermission=UserNightPermit::find()->where(['user_id'=>Yii::$app->user->identity->id,'port_id'=>$marina['id']])->asArray()->one();
					if($nightDrivePermission==null){
						$alertCls=' no-nd-permit';
					}
				}

				$holdAlert=BookingAlert::find()
				->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
				->where([
					'alert_type'=>2,
					'alert_date'=>$model->date,
					'port_id'=>$marina['id'],
					BookingAlertTiming::tableName().'.time_slot_id'=>$timeSlot['id'],
					'trashed'=>0,
				])
				->asArray()->one();
				if($holdAlert!=null){
					$alertCls=' shold-alert_'.$timeSlot['id'];
					$this->registerJs('
					$(document).delegate(".shold-alert_'.$timeSlot['id'].'", "click", function() {
						swal({
								title: "Dear Captain",
								html: "'.preg_replace("/\r|\n/","",nl2br($holdAlert['message'])).'",
								type: "info",
								showCancelButton: false,
								confirmButtonColor: "#47a447",
								confirmButtonText: "Noted",
						});
					});
					');
				}
				$spBoatType='';
				$spBoatId='';
				$saddiotinalOptions='';
				if($timeSlot['is_special']==1){
					if($freeAvailable>0){
						$sqBoatTime=BoatToTimeSlot::find()->select(['boat_id'])->where(['time_slot_id'=>$timeSlot['id']]);
						$boat=Boat::find()->where(['id'=>$sqBoatTime,'port_id'=>$marina['id']])->asArray()->one();
						if($boat!=null){
							$spBoatId=$boat['id'];
							$spBoatType=$boat['special_boat_type'];
							$warninglert=BookingAlert::find()
	            ->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
	            ->where([
	              'alert_type'=>1,
	              'alert_date'=>$model->date,
	              'port_id'=>$marina['id'],
	              BookingAlertTiming::tableName().'.time_slot_id'=>$timeSlot['id'],
	            	'trashed'=>0,
	            ])
	            ->asArray()->one();
	            if($warninglert!=null){
	              $saddiotinalOptions='data-swarning_alert="swarning_'.$timeSlot['id'].'"';
	            }

							if($model->date==date("Y-m-d") && in_array($boat['id'],Yii::$app->appHelperFunctions->restrictedTodayBookingBoats)){
								$alertCls=' not-today';
							}
						}
					}
				}
				if($timeSlot['is_special']==1 && $freeAvailable<=0){
					$html = '<div class="btn btn-xs btn-danger">'.Yii::t('app','Already Booked').'</div>';
				}
				if($alertCls!=''){
					if($timeSlot['is_special']==1){
						$html='<div class="btn btn-xs btn-success'.$alertCls.'">Book</div>';
					}else{
						$html='<div class="'.$alertCls.'">'.$freeAvailable.'</div>';
					}
				}else{
					if($timeSlot['is_special']==1){
						$html='<a class="btn btn-xs btn-success sspBookIt"'.$saddiotinalOptions.' data-cnfm="sp_'.$spBoatType.'" data-city_id="'.$model->city_id.'" data-marina_id="'.$marina['id'].'" data-date="'.$model->date.'" data-time_id="'.$timeSlot['id'].'" data-boat_id="'.$spBoatId.'">Book</a>';
					}else{
						$toBookCls='load-sel-boats" data-url="'.Url::to(['booking/suggestion-time-boats','city_id'=>$model->city_id,'marina_id'=>$marina['id'],'date'=>$model->date,'time_id'=>$timeSlot['id']]).'" data-heading="Available Boats - '.$timeSlot['dashboard_name'].'"';
						$warninglert=BookingAlert::find()
						->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
						->where([
							'alert_type'=>1,
							'alert_date'=>$model->date,
							'port_id'=>$marina['id'],
							BookingAlertTiming::tableName().'.time_slot_id'=>$timeSlot['id'],
							'trashed'=>0,
						])
						->asArray()->one();
						if($warninglert!=null){
							$toBookCls.=' data-swarning_alert="swarning_'.$timeSlot['id'].'"';
						}
						$html='<a href="javascript:;" class="'.$toBookCls.'">'.$freeAvailable.'</a>';;
					}
				}
				?>
				<?= $html?>
			</td>
			<?php }?>
		</tr>
		<?php }?>
	</table>
</div>
<?php ActiveForm::end(); ?>

<div style="display:none;">
<?php
foreach($marinas as $marina){
	$warningAlerts=BookingAlertTiming::find()
	->select([
		BookingAlertTiming::tableName().'.time_slot_id',
		BookingAlert::tableName().'.message',
	])
	->innerJoin(BookingAlert::tableName(),BookingAlert::tableName().".id=".BookingAlertTiming::tableName().".alert_id")
	->where([
		'alert_type'=>1,
		'alert_date'=>$model->date,
		'port_id'=>$marina['id'],
		'trashed'=>0,
	])
	->asArray()->all();
	if($warningAlerts!=null){
		foreach($warningAlerts as $warningAlert){
	    echo '<div id="swarning_'.$warningAlert['time_slot_id'].'">
	      '.preg_replace("/\r|\n/","",nl2br($warningAlert['message'])).'
	    </div>';
		}

	}
}
foreach(Yii::$app->helperFunctions->specialBoatTypeMessage as $key=>$val){
  echo '<div id="sp_'.$key.'">'.$val.'</div>';
}
?>
</div>

<?php
}
?>
