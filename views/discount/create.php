<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Discount */

$this->title = Yii::t('app', 'New Discount');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
