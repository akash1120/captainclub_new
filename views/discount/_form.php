<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Discount */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="discount-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
      </div>
    </div>
    <?= $form->field($model, 'descp')->textArea(['rows' => 5])?>
    <?php if($model->id!=null){?>
      <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('discount',$model['image'],'medium')?>" width="75" height="75" />
    <?php }?>
    <?= $form->field($model, 'image',[
    'template' => '
    {label}
    {input}
    <div class="input-group">
      <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-file"></i></span></div>
      <input type="text" class="form-control" disabled placeholder="'.Yii::t('app','Upload Image').'">
      <span class="input-group-append">
        <button class="browse btn btn-success input-md" type="button"><i class="glyphicon glyphicon-folder-open"></i> '.Yii::t('app','Browse').'</button>
      </span>
    </div>
    {error}'
    ])->fileInput(['class'=>'file input-file', 'data-img'=>'mcsc-photo', 'accept' => 'image/*']);?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
