<section class="card card-featured card-featured-warning mb-4">
  <div class="card-body text-center">
    <a href="javascript:;" onclick="javascript:;openModal(<?= $model['id']?>,'<?= $model['title']?>')">
      <div><img src="<?= Yii::$app->fileHelperFunctions->getImagePath('discount',$model['image'],'medium')?>" width="150" height="150" alt="<?= $model['title']?>" /></div>
      <h4><?= $model['title']?></h4>
    </a>
    <div id="descp<?= $model['id']?>" style="display:none;">
      <div class="text-center discount-image"><img src="<?= Yii::$app->fileHelperFunctions->getImagePath('discount',$model['image'],'medium')?>" width="150 " height="150 " alt="<?= $model['title']?>" /></div>
      <?= nl2br($model['descp'])?>
    </div>
  </div>
</section>
