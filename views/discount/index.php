<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Discounts');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
$actionBtns.='{view}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
?>
<div class="discount-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      ['format' => ['image',['width'=>'75','height'=>'75']],'attribute'=>'image','value'=>function($model){
        return Yii::$app->fileHelperFunctions->getImagePath('discount',$model['image'],'small');
      },'headerOptions'=>['class'=>'noprint','style'=>'width:75px;']],
      'title',
      'descp',
      ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){return Yii::$app->helperFunctions->arrPublishingIcon[$model['status']];},'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrPublishing],
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
        'contentOptions'=>['class'=>'noprint actions'],
        'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
        'buttons' => [
            'view' => function ($url, $model) {
              return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), 'javascript:;', [
                'title' => Yii::t('app', 'View'),
                'class'=>'dropdown-item text-1 btn-view',
                'data-pjax'=>"0",
                'data-title'=>$model['title'],
                'data-descp'=>nl2br($model['descp']),
                'data-image'=>Yii::$app->fileHelperFunctions->getImagePath('discount',$model['image'],'medium'),
                'data-status'=>Yii::$app->helperFunctions->arrUserStatusIcon[$model['status']],
              ]);
            },
            'update' => function ($url, $model) {
              return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                'title' => Yii::t('app', 'Edit'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'status' => function ($url, $model) {
              if($model['status']==1){
                return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                  'title' => Yii::t('app', 'Disable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                  'data-method'=>"post",
                ]);
              }else{
                return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                  'title' => Yii::t('app', 'Enable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                  'data-method'=>"post",
                ]);
              }
            },
            'delete' => function ($url, $model) {
              return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                'title' => Yii::t('app', 'Delete'),
                'class'=>'dropdown-item text-1',
                'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                'data-method'=>"post",
                'data-pjax'=>"0",
              ]);
            },
        ],
      ],
    ],
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function showDetail(_t){
  html = '';
  html+= '<div class="row">';
  html+= '  <div class="col-xs-12 col-sm-12">';
  html+= '<table class="view-detail table table-striped table-bordered">';
  if(_t.data("image")!='' && _t.data("image")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Image:')?></th>';
  html+= '    <td><img src="'+_t.data("image")+'" width="150" height="150" alt="'+_t.data("title")+'" /></td>';
  html+= '  </tr>';
  }
  html+= '  <tr>';
  html+= '    <th width="150"><?= Yii::t('app','Title:')?></th>';
  html+= '    <td>'+_t.data("title")+'</td>';
  html+= '  </tr>';
  if(_t.data("descp")!='' && _t.data("descp")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Description:')?></th>';
  html+= '    <td>'+_t.data("descp")+'</td>';
  html+= '  </tr>';
  }
  if(_t.data("status")!='' && _t.data("status")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Status:')?></th>';
  html+= '    <td>'+_t.data("status")+'</td>';
  html+= '  </tr>';
  }
  html+= '</table>';

  html+= '  </div>';
  html+= '</div>';
  viewPopUpModal("<?= Yii::t('app','Discount Detail')?>",html)
}
</script>
