<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BoatInfoField */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="discount-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'title')->textInput() ?>
      </div>
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'input_type')->dropDownList(Yii::$app->params['inputTypes']) ?>
      </div>
    <div class="row">
    </div>
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'sort_order')->textInput() ?>
      </div>
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'Disable']) ?>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
