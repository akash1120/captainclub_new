<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoatInfoField */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Boat Info Input',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Boat Info Inputs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="boat-info-field-update">
  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>
</div>
