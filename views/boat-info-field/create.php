<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoatInfoField */

$this->title = Yii::t('app', 'New  {modelClass}', [
    'modelClass' => 'Boat Info Input',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Boat Info Inputs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boat-info-field-create">
  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>
</div>
