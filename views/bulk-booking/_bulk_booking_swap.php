<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BoatSwap */
/* @var $form yii\widgets\ActiveForm */

$model->new_port_id = $model->booking->port_id;
$model->new_time = $model->booking->booking_time_slot;
$this->registerJs('$("#bulkbookingswap-new_port_id").trigger("change");');
?>
<div class="bulkboat-swap-form">
	<?php $form = ActiveForm::begin(['id'=>'bulkboat-swap-form']); ?>
	<div id="old-Info">
		<strong>Member:</strong> <?= $model->booking->member->fullname?><br />
		<strong>City:</strong> <?= $model->booking->city->name?><br />
		<strong>Marina:</strong> <?= $model->booking->marina->name?><br />
		<strong>Boat:</strong> <?= $model->booking->boat->name?><br />
		<strong>Date:</strong> <?= Yii::$app->formatter->asDate($model->booking->booking_date)?><br />
		<strong>Time:</strong> <?= $model->booking->timeZone->name?><br />
	</div>
	<h4><strong>Swap With</strong></h4>
	<div class="row">
		<div class="col-xs-12 col-sm-4">
			<?= $form->field($model, 'new_port_id')->dropDownList(Yii::$app->appHelperFunctions->getCityMarinaListArr($model->booking->city_id),['data-date'=>$model->booking->booking_date]) ?>
		</div>
		<div class="col-xs-12 col-sm-4">
			<?= $form->field($model, 'new_time')->dropDownList(Yii::$app->appHelperFunctions->timeSlotListArr,['data-date'=>$model->booking->booking_date]) ?>
		</div>
		<div class="col-xs-12 col-sm-4">
			<?= $form->field($model, 'new_boat')->dropDownList([]) ?>
		</div>
	</div>
	<div class="form-group">
	  <?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
	  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
