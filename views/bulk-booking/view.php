<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;

/* @var $this yii\web\View */
$this->title = 'Bulk Booking';


$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value' => function ($model){return $model->member->fullname.'<br /><span class="badge badge-info">'.Yii::$app->formatter->asDate($model->created_at).'</span>';},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'city_id','label'=>'City','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return ($model->boat!=null ? $model->boat->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'date','attribute'=>'booking_date','filter'=>false];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>false];
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'',
  'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],
  'contentOptions'=>['class'=>'noprint'],
  'template' => '{replace}',
  'buttons' => [
    'replace' => function ($url, $model) {
      return Html::a('<i class="fa fa-edit"></i> Replace', 'javascript:;', [
        'title' => Yii::t('app', 'Replace'),
        'class'=>'btn btn-sm btn-primary load-modal',
				'data-url'=>Url::to(['bulk-booking/swap','id'=>$model['id']]),
				'data-heading' => 'Replace Boat',
        'data-pjax'=>"0",
      ]);
    },
  ],
]
?>
<style>
.filters{display: none;}
</style>
<section class="bulk-booking-form card card-featured card-featured-warning">
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<strong class="control-label">Boat:</strong> <?= $model->boat->name?>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<strong class="control-label">Start Date:</strong> <?= Yii::$app->formatter->asDate($model->start_date)?>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<strong class="control-label">End Date:</strong> <?= Yii::$app->formatter->asDate($model->end_date)?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<strong class="control-label">Type:</strong> <?= Yii::$app->helperFunctions->adminBookingTypes[$model->booking_type]?>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<strong class="control-label">Comments:</strong> <?= $model->booking_comments?>
				</div>
			</div>
		</div>
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
		<h3>Following bookings are made in the selected date</h3>
		<?= CustomGridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'createBtn' => false,
			'cardHeader'=>false,
			'columns' => $columns,
		]);?>
    <?php CustomPjax::end(); ?>
	</div>
</section>
<?= $this->render('js/bulk_booking_scripts')?>
