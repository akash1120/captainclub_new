<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;
use app\assets\BulkBookingAssets;
BulkBookingAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\BulkBooking */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$(".dtpicker").datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
  startDate: "today",
}).on("changeDate", function(e) {
  $(this).datepicker("hide");
});
');
?>
<style>
.filters{display: none;}
</style>
<section class="bulk-booking-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'boat_id')->dropDownList(Yii::$app->appHelperFunctions->getActiveBoatsListArr(),['prompt'=>'Select Boat']) ?>
      </div>
      <div class="col-sm-4">
        <?php if($model->id!=null){?>
        <?= $form->field($model, 'start_date')->textInput(['class'=>'form-control','autocomplete'=>'off','readonly'=>'readonly']) ?>
        <?php }else{?>
        <?= $form->field($model, 'start_date')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off']) ?>
        <?php }?>
      </div>
      <div class="col-sm-4">
        <?php if($model->id!=null){?>
          <?= $form->field($model, 'end_date')->textInput(['class'=>'form-control','autocomplete'=>'off','readonly'=>'readonly']) ?>
          <?php }else{?>
        <?= $form->field($model, 'end_date')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off']) ?>
        <?php }?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'booking_type')->dropDownList(Yii::$app->helperFunctions->adminBookingTypes) ?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'booking_comments')->textInput() ?>
      </div>
    </div>
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?php if($model->id!=null){?>
    <?php
    if($model->done==1){
$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value' => function ($model){return $model->member!=null ? $model->member->fullname : '';},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'city_id','label'=>'City','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return ($model->boat!=null ? $model->boat->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'date','attribute'=>'booking_date','filter'=>false];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>false];
    ?>
      <h3>Following bookings are made in the selected date</h3>
      <?= CustomGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'createBtn' => false,
        'cardHeader'=>false,
        'columns' => $columns,
      ]);?>
      <?php
      }else{
        $this->registerJs('

        //var _targetContainer=".bulk-booking-form";

        /*App.blockUIProgress({
          message: "'.Yii::t('app','Please wait...').'<br>'.Yii::t('app','System is booking the boats. Please do not close this window or click anywhere as it may cancel the process.').'",
          target: _targetContainer,
          overlayColor: "none",
          cenrerY: true,
          boxed: true
        });*/

        //startBookings("'.Url::to(['bulk-booking/book-for-bulk-booking','bulk_booking_id'=>$model->id,'date'=>$model->start_date]).'",_targetContainer);
        ');
      ?>
    <?php }?>
    <?php }?>
    <?php CustomPjax::end(); ?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>

<script>
function startBookings(url,_targetContainer)
{
$.ajax({
  url: url,
  type: "POST",
  success: function(response) {
    response = jQuery.parseJSON(response);
    if(response["error"]!=null && response["error"]!=''){
      App.unblockUI($(_targetContainer));
      swal({title: response["error"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
    }
    if(response["success"]!=null && response["success"]!=''){
      App.unblockUI($(_targetContainer));
      $.pjax.reload({container: "#grid-container", timeout: 2000});
      //swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
    }
    if(response["progress"]!=null && response["progress"]!=""){
      move(response["progress"]["percentage"]);
      startBookings(response["progress"]["url"],_targetContainer);
    }
  },
  error: bbAlert
});
}
</script>
