<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BulkBooking */

$this->title = Yii::t('app', 'New Bulk Booking');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Bulk Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bulk-booking-create">
    <?= $this->render('_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>
</div>
