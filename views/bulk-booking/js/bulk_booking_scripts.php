<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
  $(document).delegate("#bulkbookingswap-new_port_id", "change", function() {
  	marina=$(this).val();
  	date=$(this).data("date");
  	time=$("#bulkbookingswap-new_time").val();
  	loadFreeBoats(marina,date,time,".modal-content","bulkbookingswap-new_boat","");
  });
  $(document).delegate("#bulkbookingswap-new_time", "change", function() {
    marina=$("#bulkbookingswap-new_port_id").val();
  	date=$(this).data("date");
  	time=$(this).val();
  	loadFreeBoats(marina,date,time,".modal-content","bulkbookingswap-new_boat","");
  });
  $("body").on("beforeSubmit", "form#bulkboat-swap-form", function () {
    _targetContainer=$("#bulkboat-swap-form")
    App.blockUI({
      message: "'.Yii::t('app','Please wait...').'",
      target: _targetContainer,
      overlayColor: "none",
      cenrerY: true,
      boxed: true
    });

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find(".has-error").length) {
        return false;
     }
     // submit form
     $.ajax({
        url: form.attr("action"),
        type: "post",
        data: form.serialize(),
        success: function (response) {
          if(response=="success"){
            swal({title: "'.Yii::t('app','Swapped').'", html: "'.Yii::t('app','Boat swapped successfully').'", type: "success"});
            window.closeModal();
          }else{
          	marina=$("#bulkbookingswap-new_port_id").val();
          	date=$("#bulkbookingswap-new_port_id").data("date");
          	time=$("#bulkbookingswap-new_time").val();
          	loadFreeBoats(marina,date,time,".modal-content","bulkbookingswap-new_boat","");
            swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
          }
          if($("#grid-container").length){
            $.pjax.reload({container: "#grid-container", timeout: 2000});
          }
          App.unblockUI($(_targetContainer));
        }
     });
     return false;
  });
')
?>
