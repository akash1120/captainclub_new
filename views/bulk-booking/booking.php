<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bulk Booked Boat Bookings');
$this->params['breadcrumbs'][] = $this->title;

$columns[] = ['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value' => function ($model){return $model->member!=null ? $model->member->fullname : 'No user found!';}];
$columns[] = ['format'=>'html','label'=>'City','attribute'=>'city_id','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');}];
$columns[] = ['format'=>'html','label'=>'Marina','attribute'=>'port_id','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');}];
$columns[] = ['format'=>'html','label'=>'Boat','attribute'=>'boat_id','value' => function ($model){return ($model->boat!=null ? $model->boat->name : 'Not set');}];
$columns[] = ['format'=>'date','label'=>'Date','attribute'=>'booking_date'];
$columns[] = ['format'=>'html','label'=>'Time','attribute'=>'booking_time_slot','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');}];
$columns[] = ['format'=>'html','label'=>'Status','attribute'=>'status','value'=>function($model){return ($model->booking_date>=date("Y-m-d") ? "Active" : "Old");}];
$columns[] = ['format'=>'raw','attribute'=>'booking_type','label'=>'Remarks','value'=>function($model){return $model->remarks;},'filter'=>Yii::$app->helperFunctions->adminBookingTypes];
$columns[] = ['format'=>'html','attribute'=>'is_licensed', 'label'=>'License', 'value'=>function($model){return ($model->member!=null && $model->member->is_licensed==1 ? '<span class="label label-success">Yes</span>' : '<span class="label label-warning">No</span>');}];
$columns[]=['format'=>'raw','label'=>'Addons','value' => function ($model){return $model->addonsMenu;},'contentOptions'=>['class'=>'nosd']];
$columns[] = [
		'class' => 'yii\grid\ActionColumn',
		'header'=>'Actions',
		'headerOptions' => ['style'=>'width:100px;'],
		'template' => '{replace}',
		'buttons' => [
			'replace' => function ($url, $model) {
				if(Yii::$app->menuHelperFunction->checkActionAllowed('dashboard','site')){
					return Html::a('<i class="glyphicon glyphicon-refresh"></i> Replace', 'javascript:;', [
						'title' => Yii::t('app', 'Replace'),
		        'class'=>'btn btn-sm btn-primary load-modal',
						'data-url'=>Url::to(['bulk-booking/swap','id'=>$model['id']]),
						'data-heading' => 'Replace Boat',
		        'data-pjax'=>"0",
					]);
				}
			},
		],
	];
?>
<style>
.filters{display: none;}
.btn-group.dropright{display: none !important;}
.requested-addons-list a.btn-warning{display: none;}
</style>
<div class="booking-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => false,
    'rowOptions' => function ($model, $index, $widget, $grid){
      return ['data-city_id'=>$model->city_id, 'data-marina_id'=>$model->port_id];
    },
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<?= $this->render('/booking/js/list_script')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
<?= $this->render('js/bulk_booking_scripts')?>
