<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\assets\BulkBookingAssets;
BulkBookingAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BulkBookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
  initScripts();
});
$("body").on("click", ".del-bulk-booking", function () {

  _this=$(this);
  url=_this.data("url");

  var _targetContainer=".grid-view";

  swal({
    title: "Confirmation",
    html: "'.Yii::t('app','Are you sure you want to delete it?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Confirm').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {

      App.blockUIProgress({
        message: "'.Yii::t('app','Please wait...').'<br>'.Yii::t('app','System is deleting booking, Please do not close this window or click anywhere as it may cancel the process').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });

      deleteBookings(url,_targetContainer);

    }
  });
});
');

if($searchModel->listType=='active'){
  $this->title = Yii::t('app', 'Active Bulk Bookings');
}elseif($searchModel->listType=='history'){
  $this->title = Yii::t('app', 'Bulk Booking History');
}elseif($searchModel->listType=='deleted'){
  $this->title = Yii::t('app', 'Deleted Bulk Bookings');
}

$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
$createBtnLink=[];
if(Yii::$app->menuHelperFunction->checkActionAllowed('add')){
  $createBtn=true;
  $createBtnLink=['add'];
  $actionBtns.='{add}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
  $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
  $actionBtns.='{delete}';
}
$boatsList=Yii::$app->appHelperFunctions->getActiveBoatsListArr();


$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'boat_id','value'=>function($model){return $model['boat_name'];},'label'=>'Boat','filter'=>$boatsList];
$columns[] = 'start_date';
$columns[] = 'end_date';
$columns[] = ['attribute'=>'booking_type','value'=>function($model){return Yii::$app->helperFunctions->adminBookingTypes[$model['booking_type']];},'filter'=>Yii::$app->helperFunctions->adminBookingTypes];
$columns[] = 'booking_comments';
$columns[] = ['format'=>'datetime','attribute'=>'created_at','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[] = ['attribute'=>'created_by','label'=>'By','value'=>function($model){return $model['created_by_user'];},'filter'=>Yii::$app->appHelperFunctions->staffMembersArr];
if($searchModel->listType=='active'){
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'',
  'headerOptions'=>['class'=>'noprint','style'=>'width:15px;'],
  'contentOptions'=>['class'=>'noprint'],
  'template' => '
    <div class="btn-group flex-wrap">
      <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
      </button>
      <div class="dropdown-menu dropdown-menu-right" role="menu">
        '.$actionBtns.'
      </div>
    </div>',
  'buttons' => [
    'add' => function ($url, $model) {
      return Html::a('<i class="fa fa-edit"></i> Update', $url, [
        'title' => Yii::t('app', 'Update'),
        'class'=>'dropdown-item text-1',
        'data-pjax'=>"0",
      ]);
    },
    'view' => function ($url, $model) {
      return Html::a('<i class="fa fa-table"></i> View', $url, [
        'title' => Yii::t('app', 'View'),
        'class'=>'dropdown-item text-1',
        'data-pjax'=>"0",
      ]);
    },
    'delete' => function ($url, $model) {
      if($model!=null && $model['end_date']>=date("Y-m-d")){
        //If date is in future
        if($model['start_date']>=date("Y-m-d")){
          $date=$model['start_date'];
        }else{
          //If its already started
          $date=date("Y-m-d");
        }

        return Html::a('<i class="fa fa-trash"></i> Delete', 'javascript:;', [
          'title' => Yii::t('app', 'Delete'),
          'class'=>'dropdown-item text-1 del-bulk-booking',
          'data-url'=>Url::to(['delete','id'=>$model['id'],'start_date'=>$date,'current_date'=>$date]),
          'data-pjax'=>"0",
        ]);
      }else{

      }
    },
  ],
];
}
?>
<div class="bulk-booking-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomTabbedGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'createBtnLink' => $createBtnLink,
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function initScripts()
{
  $(".dtpicker").datepicker({
    format: "yyyy-mm-dd",
  }).on("changeDate", function(e){
    $(this).datepicker("hide");
  });
  $(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
	$(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
		$(this).trigger("change");
	});
	$(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});
}
function deleteBookings(url,_targetContainer){
  $.ajax({
    url: url,
    type: "POST",
    success: function(response) {
      response = jQuery.parseJSON(response);
      if(response["error"]!=null && response["error"]!=''){
        App.unblockUI($(_targetContainer));
        swal({title: response["error"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
      }
      if(response["success"]!=null && response["success"]!=''){
        App.unblockUI($(_targetContainer));
        swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
        $.pjax.reload({container: "#grid-container", timeout: 2000});
      }
      if(response["progress"]!=null && response["progress"]!=""){
        move(response["progress"]["percentage"]);
        deleteBookings(response["progress"]["url"],_targetContainer);
      }
    },
    error: bbAlert
  });
}
</script>
