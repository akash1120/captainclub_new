<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Boat;
use app\models\BoatToTimeSlot;

$this->title='Assign Addons & Night Drive Access';

$marinas = Yii::$app->appHelperFunctions->activeMarinaList;

$boats=Boat::find()->where(['port_id'=>$model->marina_id,'status'=>1,'trashed'=>0])->asArray()->all();

$timeSlots=Yii::$app->appHelperFunctions->timeSlotListArr;
?>
<div class="tabs">
	<ul class="nav nav-tabs">
		<?php
		if($marinas!=null){
			$n=1;
			foreach ($marinas as $marina) {
				?>
				<li class="nav-item">
					<a class="nav-link<?= $model->marina_id==$marina['id'] ? ' active' : ''?>" href="<?= Url::to(['boat-options','marina_id'=>$marina['id']])?>"><?= $marina['name']?></a>
				</li>
				<?php
				$n++;
			}
		}
		?>
	</ul>
	<div class="tab-content">
    <div class="tab-pane active">
      <?php $form = ActiveForm::begin(); ?>
			<div class="row">
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr) ?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_name')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_short_name')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_fuel_profit')->textInput(['maxlength' => true])?>
	      </div>
	    </div>
	    <div class="row">
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_captains')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_bbq')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_no_of_early_departures')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_no_of_late_arrivals')->textInput(['maxlength' => true])?>
	      </div>
	    </div>
	    <div class="row">
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_no_of_overnight_camps')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_night_drive_limit')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_wake_boarding_limit')->textInput(['maxlength' => true])?>
	      </div>
	      <div class="col-xs-12 col-sm-3">
	        <?= $form->field($model, 'marina_wake_surfing_limit')->textInput(['maxlength' => true])?>
	      </div>
	    </div>
      <?php if($boats!=null){?>
				<div class="alert alert-info">
					Please update only night drive session for Timing column. That is which boats can have night drive sessions. Do not select night drive/marina stay in/drop off for <strong>Normal</strong> boats. as their working is different than normal boats, its handled in coding.
				</div>
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th width="10">#</th>
            <th>Boat</th>
            <th width="400">Timings</th>
            <th width="400">Addons</th>
          </tr>
        </thead>
        <tbody>
          <?php $n=1;foreach($boats as $boat){?>
          <tr>
            <td><?= $n?></td>
            <td><?= $boat['name']?></td>
            <td>
              <?php
              if($timeSlots!=null){
                foreach($timeSlots as $time_id=>$time_name){
                  $checked='';
                  $checkIfHasTimeSLot=BoatToTimeSlot::find()->where(['boat_id'=>$boat['id'],'time_slot_id'=>$time_id]);
                  if($checkIfHasTimeSLot->exists()){
                    $checked=' checked="checked"';
                  }
              ?>
              <div>
                <label>
									<input type="checkbox" name="BoatOptionsForm[boat_timings][<?= $boat['id']?>][]" value="<?= $time_id?>"<?= $checked?> />
                	<?= $time_name?>
								</label>
              </div>
              <?php
                }
              }
              ?>
            </td>
            <td>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[watersports][<?= $boat['id']?>]" value="1" <?= $boat['watersports']==1 ? ' checked="checked"' : ''?> /> Water Sports (Donut)</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[bbq][<?= $boat['id']?>]" value="1" <?= $boat['bbq']==1 ? ' checked="checked"' : ''?> /> BBQ</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[early_departure][<?= $boat['id']?>]" value="1" <?= $boat['early_departure']==1 ? ' checked="checked"' : ''?> /> Early Departure</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[ice][<?= $boat['id']?>]" value="1" <?= $boat['ice']==1 ? ' checked="checked"' : ''?> /> Ice</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[kids_life_jacket][<?= $boat['id']?>]" value="1" <?= $boat['kids_life_jacket']==1 ? ' checked="checked"' : ''?> /> Kids Life Jacket</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[overnight_camp][<?= $boat['id']?>]" value="1" <?= $boat['overnight_camp']==1 ? ' checked="checked"' : ''?> /> Overnight Camping</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[late_arrival][<?= $boat['id']?>]" value="1" <?= $boat['late_arrival']==1 ? ' checked="checked"' : ''?> /> Late Arrival</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[wake_boarding][<?= $boat['id']?>]" value="1" <?= $boat['wake_boarding']==1 ? ' checked="checked"' : ''?> /> Wake Boarding</label>
              </div>
              <div>
                <label><input type="checkbox" name="BoatOptionsForm[wake_surfing][<?= $boat['id']?>]" value="1" <?= $boat['wake_surfing']==1 ? ' checked="checked"' : ''?> /> Wake Surfing</label>
              </div>
            </td>
          </tr>
          <?php $n++;}?>
        </tbody>
      </table>
      <?php }?>

      <div>
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    	</div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
