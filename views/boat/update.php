<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Boats */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Boat',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Boats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="boat-update">
  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>
</div>
