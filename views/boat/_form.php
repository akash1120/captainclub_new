<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Cities;
use app\models\Timeslots;
use app\models\FuelConsumptionType;
use app\models\BoatFuelConsumptionRate;
use app\models\BoatInfoField;
use app\models\BoatInfoValue;
use app\models\BoatToTags;
use app\models\BoatAvailableDays;
use app\models\BoatToTimeSlot;

/* @var $this yii\web\View */
/* @var $model app\models\Boats */
/* @var $form yii\widgets\ActiveForm */

$infoFields=BoatInfoField::find()->where(['status'=>1,'trashed'=>0])->orderBy(['sort_order'=>SORT_ASC])->asArray()->all();
$boatTags=Yii::$app->appHelperFunctions->boatTagsListArr;
$model->tag_id=ArrayHelper::map(BoatToTags::find()->select(['tag_id'])->where(['boat_id'=>$model->id])->asArray()->all(),"tag_id","tag_id");

$weekDays=Yii::$app->helperFunctions->weekDayNames;
$model->available_days=ArrayHelper::map(BoatAvailableDays::find()->select(['available_day'])->where(['boat_id'=>$model->id])->asArray()->all(),"available_day","available_day");
$model->timeslots=ArrayHelper::map(BoatToTimeSlot::find()->select(['time_slot_id'])->where(['boat_id'=>$model->id])->asArray()->all(),"time_slot_id","time_slot_id");

$fuelConsumptionTypes=FuelConsumptionType::find()->asArray()->all();

$cities=Yii::$app->appHelperFunctions->cityListArr;
$marinas=Yii::$app->appHelperFunctions->getCityMarinaListArr($model->city_id);

$txtJScript=Yii::$app->jsFunctions->getCityMarinaArr($cities);



$this->registerJs($txtJScript.'
	$(document).delegate("#boat-city_id", "change", function() {
		array_list=cities[$(this).val()];
		$("#boat-port_id").html("<option value=\"\">Marina</option>");
		$(array_list).each(function (i) {
			$("#boat-port_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
		});
	});
	$(document).delegate("#boat-special_boat", "change", function() {
		if($(this).val()==1){
			$("#spc1").addClass("col-sm-6").removeClass("col-sm-12");
			$("#spc2").show();
		}else{
			$("#spc1").removeClass("col-sm-6").addClass("col-sm-12");
			$("#spc2").hide();
		}
	});
');

$activeBookingCount=Yii::$app->statsFunctions->getBoatFutureBookingCount($model->id);
?>
<style>
#boat-tag_id label{width: 14%;float: left;margin-top: 0px;}
#boat-available_days label{width: 14%;float: left;margin-top: 0px;}
#boat-timeslots label{width: 25%;float: left;margin-top: 0px;}
</style>

<section class="city-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'city_id')->dropDownList($cities,['prompt'=>$model->getAttributeLabel('city_id')]) ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'port_id')->dropDownList($marinas,['prompt'=>$model->getAttributeLabel('port_id')]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'name')->textInput() ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'boat_purpose_id')->dropDownList(Yii::$app->appHelperFunctions->boatPurposeListArr,['prompt'=>$model->getAttributeLabel('boat_purpose_id')]) ?>
			</div>
		</div>
		<?= $form->field($model, 'descp')->textInput() ?>
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'rank')->textInput() ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'repair')->textInput() ?>
			</div>
		</div>
		<?php if(Yii::$app->user->identity->id==1){?>
		<div class="row">
			<div class="col-sm-12">
				<?= $form->field($model, 'tag_id')->checkboxList($boatTags) ?>
			</div>
		</div>
		<hr />
		<?php }?>
		<?php if(Yii::$app->user->identity->id==1){?>
		<div class="row">
			<div class="col-sm-12">
				<?= $form->field($model, 'available_days')->checkboxList($weekDays) ?>
			</div>
		</div>
		<hr />
		<?php }?>
		<div class="row">
			<div class="col-sm-12">
				<?= $form->field($model, 'timeslots')->checkboxList(Yii::$app->appHelperFunctions->timeSlotListArr) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?php if($model->id!=null){?>
		      <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('boat',$model['image'],'medium')?>" width="75" height="75" />
		    <?php }?>
		    <?= $form->field($model, 'image',[
		    'template' => '
			    {label}
			    {input}
			    <div class="input-group">
			      <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-file"></i></span></div>
		        <input type="text" class="form-control" disabled placeholder="'.Yii::t('app','Upload Image').'">
		        <span class="input-group-append">
		          <button class="browse btn btn-success input-md" type="button"><i class="glyphicon glyphicon-folder-open"></i> '.Yii::t('app','Browse').'</button>
		        </span>
		      </div>
		    {error}'
		    ])->fileInput(['class'=>'file input-file', 'data-img'=>'mcsc-photo', 'accept' => 'image/*']);?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<?php
					$spCol=12;
					$spTypeStyle=' style="display:none;"';
					if($model->special_boat==1){
						$spCol=6;
						$spTypeStyle='';
					}
					?>
					<div id="spc1" class="col-sm-<?= $spCol?>">
						<?= $form->field($model, 'special_boat')->dropDownList(['0'=>'Normal','1'=>'Special']) ?>
					</div>
					<div id="spc2" class="col-sm-6"<?= $spTypeStyle?>>
						<div class="row">
							<div class="col-sm-6">
								<?= $form->field($model, 'special_boat_type')->dropDownList(Yii::$app->helperFunctions->specialBoatTypes) ?>
							</div>
							<div class="col-sm-6">
								<?= $form->field($model, 'boat_selection')->dropDownList(['0'=>'No','1'=>'Yes']) ?>
							</div>
						</div>

					</div>
				</div>

			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'Disable'],['prompt'=>$model->getAttributeLabel('status')])->label($model->getAttributeLabel('status').($activeBookingCount>0 ? ' ('.$activeBookingCount.' active bookings)' : '')) ?>
			</div>
		</div>

		<h3>Addons</h3>
		<div class="row">
			<div class="col-sm-4">
				<?= $form->field($model, 'watersports')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'bbq')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'early_departure')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'ice')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'kids_life_jacket')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'overnight_camp')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'late_arrival')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'wake_boarding')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'wake_surfing')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
			</div>
		</div>

		<?php if($infoFields!=null){?>
		<h3>Boat Information</h3>
		<div class="row">
			<?php
			foreach($infoFields as $infoField){
				$valRow=BoatInfoValue::find()->where(['boat_id'=>$model->id,'info_field_id'=>$infoField['id']])->asArray()->one();
				if($valRow!=null){
					$model->info_field[$infoField['id']]=$valRow['field_value'];
				}
			?>
				<div class="col-xs-12 col-sm-4">
					<?php if($infoField['input_type']==1){?>
					<?= $form->field($model, 'info_field['.$infoField['id'].']')->textInput()->label($infoField['title']) ?>
				<?php }elseif($infoField['input_type']==2){?>
						<?= $form->field($model, 'info_field['.$infoField['id'].']')->textArea(['rows'=>4])->label($infoField['title']) ?>
					<?php }?>
				</div>
			<?php }?>
		</div>
		<?php }?>
		<?php if($fuelConsumptionTypes!=null){?>
		<h3>Fuel Consumption Chart</h3>
		<table class="table table-bordered">
			<tr>
				<?php foreach($fuelConsumptionTypes as $fuelConsumptionType){?>
				<th><?= $fuelConsumptionType['title']?></th>
				<?php }?>
			</tr>
			<tr>
				<?php
				foreach($fuelConsumptionTypes as $fuelConsumptionType){
					$valRow=BoatFuelConsumptionRate::find()->where(['boat_id'=>$model->id,'type_id'=>$fuelConsumptionType['id']])->asArray()->one();
					if($valRow!=null){
						$model->fuel_consumption[$fuelConsumptionType['id']]=$valRow['rate_value'];
					}
				?>
				<th><?= $form->field($model, 'fuel_consumption['.$fuelConsumptionType['id'].']')->textInput()->label(false) ?></th>
				<?php }?>
			</tr>
		</table>
		<?php }?>
	</div>
	<div class="card-footer">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</section>
