<?php
use yii\helpers\Html;
use app\models\BoatInfoField;
use app\models\BoatInfoValue;
use app\models\FuelConsumptionType;
use app\models\BoatFuelConsumptionRate;

/* @var $this yii\web\View */
/* @var $model app\models\Boats */
$infoFields=BoatInfoValue::find()
  ->select([
    BoatInfoField::tableName().'.title',
    BoatInfoValue::tableName().'.field_value',
  ])
  ->innerJoin("boat_info_field",BoatInfoField::tableName().".id=".BoatInfoValue::tableName().".info_field_id")
  ->where([
    BoatInfoValue::tableName().'.boat_id'=>$model->id,
    BoatInfoField::tableName().'.status'=>1,
    BoatInfoField::tableName().'.trashed'=>0
  ])
  ->orderBy([BoatInfoField::tableName().'.sort_order'=>SORT_ASC])
  ->asArray()->all();
?>
<h3 style="margin:0 0 10px"><?= $model->name?></h3>
<img src="<?= Yii::$app->fileHelperFunctions->getImagePath('boat',$model['image'],'large')?>" />
<div class="table-responsive" style="border:0px;">
<?php if($infoFields!=null){?>
<table class="table table-bordered table-stripped" style="margin-top:10px;">
  <tr>
  <?php foreach($infoFields as $infoField){?>
    <th><?= $infoField['title']?>:</th>
  <?php }?>
  </tr>
  <tr>
  <?php foreach($infoFields as $infoField){?>
    <td><?= $infoField['field_value']?></td>
  <?php }?>
  </tr>
</table>
<?php }
$boatFuelConsumptionChart='';
if($model->port_id!=Yii::$app->params['emp_id']){
	$fuelConsumptionTypes=FuelConsumptionType::find()->asArray()->all();
	if($fuelConsumptionTypes!=null){
  ?>
  <strong>Fuel Consumption Chart</strong>
	<table class="table table-bordered table-stripped" style="margin-top:10px;">
		<tr>
    <?php foreach($fuelConsumptionTypes as $fuelConsumptionType){?>
  	   <th><?= $fuelConsumptionType['title']?></th>
    <?php }?>
  	</tr>
		<tr>
      <?php
				foreach($fuelConsumptionTypes as $fuelConsumptionType){
					$valRow=BoatFuelConsumptionRate::find()->where(['boat_id'=>$model->id,'type_id'=>$fuelConsumptionType['id']])->asArray()->one();
					if($valRow!=null){
      ?>
	       <th><?= $valRow['rate_value']?></th>
      <?php
					}
				}
      ?>
    </tr>
	</table>
	<?php }
}
echo $boatFuelConsumptionChart;
echo '</div>';
exit;?>
