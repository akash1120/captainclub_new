<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Package;
use app\models\UserStatusReason;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequests */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<strong>Member:</strong> <?= $model->_user->fullname?>
	</div>
	<div class="col-xs-12 col-sm-6">
		<strong>Package:</strong> <?= $model->_user->activeContract->package->name?>
	</div>
	<div class="col-xs-12 col-sm-6">
		<strong>Expiry:</strong> <?= Yii::$app->formatter->asDate($model->_user->activeContract->end_date)?>
	</div>
</div>
<hr />
<div class="membership-status-form">
	<?php $form = ActiveForm::begin(['id'=>'membership-status-form']); ?>
	<?= $form->field($model, 'date')->textInput(['class'=>'form-control dtpicker']) ?>
	<?= $form->field($model, 'reason')->textarea(['rows' => 6]) ?>
	<?= $form->field($model, 'delete_booking')->checkbox() ?>
	<?php
	$otherRequests=UserStatusReason::find()->where(['and',['user_id'=>$model->user_id,'status'=>$model->status],['>=','date',date("Y-m-d")]])->asArray()->all();
	if($otherRequests!=null){
		?>
		You already have following <?= $model->status==2 ? 'hold' : 'cancel'?> requests scheduled:
		<ol style="padding-left:15px;">
			<?php
			foreach($otherRequests as $otherRequest){
				echo '<li>'.Yii::$app->formatter->asDate($otherRequest['date']).' - '.nl2br($otherRequest['reason']).'</li>';
			}
			?>
		</ol>
		<?= $form->field($model, 'delete_other_req')->checkbox() ?>
		<?php
	}
	?>
	<div class="form-group">
		<?= Html::submitButton('Save '.($model->status==2 ? 'Hold' : 'Cancel'), ['class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
