<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Package;
use app\models\PackageService;
use app\models\User;
use app\models\ContractMember;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequests */
/* @var $form yii\widgets\ActiveForm */

$activeContract = $model->_user->activeContract;

$freeMembers=User::find()->select(['id','username'])->where(['and',['status'=>[1,2,3,20]],['!=','id',$model->_user->id]])->orderBy(['username'=>SORT_ASC])->asArray()->all();//,['in','id',$expiredUsers],['not in','id', $oldUsers]]
$secondUserArr=[];
$secondUserArr[0]='Select';
if($freeMembers!=null){
	foreach($freeMembers as $freeMember){
		$secondUserArr[$freeMember['id']]=$freeMember['username'];
	}
}
$stedOpts=['class'=>'form-control dtpicker'];
if($activeContract!=null){
	if($model->_user->status==1){
		//$stedOpts=['readonly'=>'readonly'];
	}

	$members=$activeContract->members;
	$model->member_idz[0]=$model->_user->id;
	if($members!=null){
		foreach($members as $member){
			if($member->user_id!=$model->_user->id){
				$model->member_idz[1]=$member->user_id;
			}
		}
	}
}
$showMemberOpt=false;
?>
<div class="renew-form">
	<?php $form = ActiveForm::begin(['id'=>'renew-form']); ?>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'package_id')->dropDownList(ArrayHelper::map(Package::find()->all(),'id','name'),['prompt'=>$model->getAttributeLabel('package_id')]) ?>
		</div>
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'package_name')->textInput() ?>
		</div>
	</div>
	<div id="multi-user" class="row"<?= $showMemberOpt==false ? ' style="display:none;"' : ''?>>
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'member_idz[0]')->dropDownList(ArrayHelper::map(User::find()->where(['id'=>$model->_user->id])->all(),'id','username'),['id'=>'first-user']) ?>
		</div>
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'member_idz[1]',['template'=>'
			{label}
			<div style="width:100%;">
			{input}
			</div>
			'])->dropDownList($secondUserArr,['id'=>'second-user']) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'start_date')->textInput($stedOpts) ?>
		</div>
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'end_date')->textInput($stedOpts) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'allowed_captains')->textInput() ?>
		</div>
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'allowed_freeze_days')->textInput() ?>
		</div>
	</div>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Renew'), ['class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
