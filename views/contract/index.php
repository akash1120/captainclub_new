<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\UserStatusReason;
use app\widgets\UsersTabStart;
use app\assets\ContractListAssets;
ContractListAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$memberName=$model->fullname;
$this->title = Yii::t('app', 'Member:').' '.$memberName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;

$gridBtnsArr='{freeze} ';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update-history')){
	$gridBtnsArr.='{history} ';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $gridBtnsArr.='{update}';
}

$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
	$(document).delegate(".chkValidMax", "blur", function() {
		max=$(this).data("max");
		if($(this).val()>max){
			swal({title: "'.Yii::t('app','Error').'", html: "You can reduce to Maximum: "+max, type: "error"});
			$(this).val(max)
		}
	});
	$(document).delegate(".btn-update-contract", "click", function() {
		id=$(this).data("key");
		heading=$(this).data("heading");
		$.ajax({
			url: "'.Url::to(['contract/update','id'=>'']).'"+id,
			dataType: "html",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html("Update contract<br />"+heading);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
			},
			error: bbAlert
		});
	});
	$("body").on("beforeSubmit", "form#update-contract-form", function () {
		_targetContainer=$("#update-contract-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
				return false;
		 }
		 // submit form
		 $.ajax({
				url: form.attr("action"),
				type: "post",
				data: form.serialize(),
				success: function (response) {
					if(response=="success"){
						swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Information saved successfully').'", type: "success"});
						window.closeModal();
						$.pjax.reload({container: "#grid-container", timeout: 2000});
					}else{
						swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
					}
					App.unblockUI($(_targetContainer));
				}
		 });
		 return false;
	});
');
?>
<style>
.filters{display:none;}
.redbox .control-label{color:red !important;}
.card-action-btns {
    right: 10px;
    position: absolute;
    top: 10px;
}
.grid-badge.status-badge{max-width: 140px;white-space: pre-line;line-height: 1.3}
</style>
<div class="user-index">
  <?= UsersTabStart::header($model,$searchModel)?>
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'cardHeader' => false,
      'options'=>['class'=>'grid-view'],
      'columns' => [
        ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
        'id',
        'package_name',
        'start_date:date',
        'end_date:date',
				['format'=>'html','label'=>'Captains','value'=>function($model){
					$captainStats=Yii::$app->statsFunctions->getCaptainsByContract($model['id']);
					$html ='Allowed: '.$captainStats['allowed'].'<br />';
					$html.='Free Used: '.$captainStats['freeused'].'<br />';
					$html.='Remaining: '.$captainStats['remaining'].'<br />';
					$html.='Paid: '.$captainStats['paidused'];
					return $html;
				}],
				['format'=>'html','label'=>'Freeze Days','value'=>function($model){
					$freezeDaysStats=Yii::$app->statsFunctions->getFreezeDaysByContract($model['id']);
					$html ='Allowed: '.$freezeDaysStats['allowed'].'<br />';
					$html.='Used: '.$freezeDaysStats['used'].'<br />';
					$html.='Remaining: '.$freezeDaysStats['remaining'];
					return $html;
				}],
        ['format'=>'html','attribute'=>'status','value'=>function($model) use ($searchModel){
          $userStatusArr=Yii::$app->helperFunctions->arrUserStatusIcon;
          if($model['status']==1){
            if($model['start_date']>date("Y-m-d")){
              return '<span class="badge grid-badge badge-renewed">Renewed</span>';
            }elseif($model['end_date']>date("Y-m-d")){
              return $userStatusArr[$model['status']];
            }else{
              return '<span class="badge grid-badge badge-warning">Expired</span>';
            }
          }else{
						if($model['status']==2 || $model['status']==3){
							$heading='';
							$reason='';
							$reasonRow = UserStatusReason::find()->where(['user_id'=>$searchModel->user_id,'status'=>$model['status']])->asArray()->orderBy(['id'=>SORT_DESC])->one();
							if($reasonRow!=null){
								$heading=' on '.Yii::$app->formatter->asDate($reasonRow['date']);
								$reason='<br />'.$reasonRow['reason'];
							}
							if($model['status']==2){
								$html='<span class="badge text-left status-badge grid-badge badge-primary">'.Yii::t('app','Hold').' '.$heading.' '.$reason.'</span>';
							}
							if($model['status']==3){
								$html='<span class="badge text-left status-badge grid-badge badge-info">'.Yii::t('app','Canceled').' '.$heading.' '.$reason.'</span>';
							}
							return $html;
						}else{
            	return $userStatusArr[$model['status']];
						}
          }
        }],
        [
          'class' => 'yii\grid\ActionColumn',
          'header'=>'',
          'headerOptions'=>['class'=>'noprint','style'=>'width:155px;'],
          'contentOptions'=>['class'=>'noprint'],
          'template' => $gridBtnsArr,
          'buttons' => [
            'freeze' => function ($url, $model) use ($searchModel, $memberName) {
							if($model['end_date']>date("Y-m-d")){
	              return Html::a('<i class="fa fa-times"></i> Freeze', 'javascript:;', [
	                'title' => Yii::t('app', 'Freeze'),
	                'class'=>'btn btn-xs btn-danger load-modal',
	                'data-heading'=>'Freeze '.$memberName.' Account',
	                'data-url'=>Url::to(['request/freeze','user_id'=>$searchModel->user_id]),
	                'data-pjax'=>'0'
	              ]);
							}
            },
            'history' => function ($url, $model) use ($searchModel) {
              return Html::a('<i class="fa fa-history"></i> History', ['update-history','user_id'=>$searchModel->user_id,'cid'=>$model['id']], [
                'title' => Yii::t('app', 'Contract Change History'),
                'class'=>'btn btn-xs btn-info btn-history',
                'data-pjax'=>'0'
              ]);
            },
            'update' => function ($url, $model) use ($searchModel) {
              if($model['end_date']>date("Y-m-d")){
                return Html::a('<i class="fa fa-edit"></i> Update', 'javascript:;', [
                  'title' => Yii::t('app', 'Update'),
                  'class'=>'btn btn-xs btn-success btn-update-contract',
		              'data-key'=>$model['id'],
		              'data-heading'=>$model['package_name'].' '.Yii::$app->formatter->asDate($model['start_date']).' <b>To</b> '.Yii::$app->formatter->asDate($model['end_date']),
                ]);
              }
            },
          ],
        ],
      ],
    ]);?>
    <?php CustomPjax::end(); ?>
<?= UsersTabStart::footer()?>
</div>
<script>
function showMoreInputs(divId,clsName){
	$("."+clsName).find("input").val("");
	$("."+clsName).hide();
	$("#"+divId).show();
}
</script>
<?= $this->render('/request/js/freeze_scripts')?>
