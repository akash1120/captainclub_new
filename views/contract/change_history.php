<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\widgets\UsersTabStart;
use app\assets\AppAutoScrollAsset;
AppAutoScrollAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel common\models\DiscussionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Member:').' '.$model->fullname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = ['label' => $model->fullname, 'url' => ['user/view','id'=>$model->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Contracts'), 'url' => ['contract/index','id'=>$model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update History');

$this->registerJs('
	var ias = $.ias({
	  container:  "#historyList",
	  item:       ".item",
	  pagination: ".pagination",
	  next:       ".next a",
	  delay:      1200,
	  negativeMargin:0
	});
	ias.extension(new IASSpinnerExtension({
		html: "<div class=\"col-xs-12 col-sm-12 text-center\"><div class=\"loading-message loading-message-boxed\"><img src=\"images/loading.gif\" height=\"25\"><span>&nbsp;&nbsp;'.Yii::t('app','Loading').'</span></div></div>",
	}));
');
?>
<style>
.panel-body.comments{padding:5px;}
.timeline .tm-body{ padding:1px 0;}
.timeline.timeline-simple .tm-body .tm-items > li{margin: 10px 0;}
.empty{padding-left: 40px;}
</style>
<div class="contract-history-index">
  <?= UsersTabStart::header($model,$searchModel)?>
  <section class="card">
    <header class="card-header">
      <h2 class="card-title"><?= $modelContract->package->name.' - '.Yii::$app->formatter->asDate($modelContract->start_date).' - '.Yii::$app->formatter->asDate($modelContract->end_date)?></h2>
      <div class="card-btn-actions">
        <a href="<?= Url::to(['contract/index','id'=>$model->id])?>" class="btn btn-xs btn-info" data-pjax="0"><i class="fa fa-backward"></i> Back to Contracts</a>
      </div>
    </header>
    <div class="card-body">
      <div class="timeline">
        <div class="tm-body">
          <?php
          echo ListView::widget( [
            'dataProvider' => $dataProvider,
            'id' => 'historyList',
            'options'=>['tag'=>'ol', 'class'=>'tm-items'],
            'itemOptions' => ['tag'=>'li','class' => 'item'],
            'itemView' => '_history_item',
            'layout'=>"
            {items}
            <center>{pager}</center>
            ",
          ] );
          ?>
        </div>
      </div>
    </div>
  </section>
  <?= UsersTabStart::footer()?>
</div>
