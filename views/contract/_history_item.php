<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequestsDiscussion */
?>
<div class="tm-info">
  <div class="tm-icon"><i class="fas fa-edit"></i></div>
  <time class="tm-datetime" datetime="<?= $model->updated_at?>">
    <div class="tm-datetime-date"><?= Yii::$app->formatter->asDate($model->updated_at)?></div>
    <div class="tm-datetime-time"><?= Yii::$app->formatter->asTime($model->updated_at)?></div>
  </time>
</div>
<div class="tm-box appear-animation animated fadeInRight appear-animation-visible" data-appear-animation="fadeInRight" data-appear-animation-delay="100">
  <p>
    <?= $model->old_package_id!=$model->new_package_id ? 'Package Changed from '.$model->oldPackage->name.' to '.$model->newPackage->name.'<br />' : ''?>
    <?= $model->old_package_name!=$model->new_package_name ? 'Package Name Changed from '.$model->old_package_name.' to '.$model->new_package_name.'<br />' : ''?>
    <?= $model->old_allowed_captains!=$model->new_allowed_captains ? 'Captains Changed from '.$model->old_allowed_captains.' to '.$model->new_allowed_captains.'<br />' : ''?>
    <?= $model->old_allowed_freeze_days!=$model->new_allowed_freeze_days ? 'Freeze Days Changed from '.$model->old_allowed_freeze_days.' to '.$model->new_allowed_freeze_days.'<br />' : ''?>
    <?= $model->days_extended>0 ? 'Days Extended '.$model->days_extended.'<br />' : ''?>
    <?= '<span class="badge badge-info">Comments</span> '.nl2br($model->note)?>
  </p>
  <div class="tm-meta">
    <span>
      <i class="fas fa-user"></i> By <?= $model->createdBy?>
    </span>
  </div>
</div>
