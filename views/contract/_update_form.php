<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Package;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequests */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="renew-form">
	<?php $form = ActiveForm::begin(['id'=>'update-contract-form']); ?>
	<div class="row">
		<div class="col-xs-12 col-sm-6"><label class="control-label"><?= '<strong>Start Date:</strong> '.Yii::$app->formatter->asDate($model->start_date)?></label></div>
		<div class="col-xs-12 col-sm-6"><label class="control-label"><?= '<strong>End Date:</strong> '.Yii::$app->formatter->asDate($model->end_date)?></label></div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'package_id')->dropDownList(ArrayHelper::map(Package::find()->all(),'id','name'),['prompt'=>$model->getAttributeLabel('package_id')]) ?>
		</div>
		<div class="col-xs-12 col-sm-6">
			<?= $form->field($model, 'package_name')->textInput() ?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6">
			<section class="card card-featured card-featured-primary mb-4">
				<header class="card-header">
					<div class="card-action-btns">
						<a href="javascript:;" class="card-acqtion btn btn-xs btn-success" onclick="javascript:showMoreInputs('addCaptains','captainOpts');"><i class="fa fa-plus"></i></a>
						<a href="javascript:;" class="card-acqtion btn btn-xs btn-danger" onclick="javascript:showMoreInputs('reduceCaptains','captainOpts');"><i class="fa fa-minus"></i></a>
					</div>
					<h2 class="card-title">Captains</h2>
				</header>
				<div class="card-body">
					<?= '<strong>Allowed:</strong> '.$model->allowed_captains?><br />
					<?= '<strong>Used:</strong> '.$model->used_captains?><br />
					<?= '<strong>Remaing:</strong> '.$model->remaining_captains?>
					<div id="addCaptains" class="captainOpts" style="display:none;">
						<?= $form->field($model, 'captains_to_add')->textInput() ?>
					</div>
					<div id="reduceCaptains" class="captainOpts redbox" style="display:none;">
						<?= $form->field($model, 'captains_to_reduce')->textInput(['class'=>'form-control chkValidMax','data-max'=>$model->remaining_captains]) ?>
					</div>
				</div>
			</section>
		</div>
		<div class="col-xs-12 col-sm-6">
			<section class="card card-featured card-featured-primary mb-4">
				<header class="card-header">
					<div class="card-action-btns">
						<a href="javascript:;" class="card-acqtion btn btn-xs btn-success" onclick="javascript:showMoreInputs('addFreezeDays','freezeOpts');"><i class="fa fa-plus"></i></a>
						<a href="javascript:;" class="card-acqtion btn btn-xs btn-danger" onclick="javascript:showMoreInputs('reduceFreezeDays','freezeOpts');"><i class="fa fa-minus"></i></a>
					</div>

					<h2 class="card-title">Freeze Days</h2>
				</header>
				<div class="card-body">
					<?= '<strong>Allowed:</strong> '.$model->allowed_freeze_days?><br />
					<?= '<strong>Used:</strong> '.$model->used_freeze_days?><br />
					<?= '<strong>Remaining:</strong> '.$model->remaining_freeze_days?>
					<div id="addFreezeDays" class="freezeOpts" style="display:none;">
						<?= $form->field($model, 'freeze_days_to_add')->textInput() ?>
					</div>
					<div id="reduceFreezeDays" class="freezeOpts redbox" style="display:none;">
						<?= $form->field($model, 'freeze_days_to_reduce')->textInput(['class'=>'form-control chkValidMax','data-max'=>$model->remaining_freeze_days]) ?>
					</div>
				</div>
			</section>
		</div>
		<div class="col-xs-12 col-sm-12">
			<section class="card card-featured card-featured-primary mb-4">
				<header class="card-header">
					<div class="card-action-btns">
						<a href="javascript:;" class="card-acqtion btn btn-xs btn-success" onclick="javascript:showMoreInputs('addContractDays','contractOpts');"><i class="fa fa-plus"></i></a>
						<a href="javascript:;" class="card-acqtion btn btn-xs btn-danger" onclick="javascript:showMoreInputs('reduceContractDays','contractOpts');"><i class="fa fa-minus"></i></a>
					</div>

					<h2 class="card-title">Contract Days</h2>
				</header>
				<div class="card-body">
					<?= '<strong>Total:</strong> '.$model->contract_days?><br />
					<?= '<strong>Passed:</strong> '.$model->passed_contract_days?><br />
					<?= '<strong>Remaining:</strong> '.$model->remaining_contract_days?>
					<div id="addContractDays" class="contractOpts" style="display:none;">
						<?= $form->field($model, 'days_to_add')->textInput() ?>
					</div>
					<div id="reduceContractDays" class="contractOpts redbox" style="display:none;">
						<?= $form->field($model, 'days_to_reduce')->textInput(['class'=>'form-control chkValidMax','data-max'=>$model->remaining_contract_days]) ?>
					</div>
				</div>
			</section>
		</div>
	</div>
	<?= $form->field($model, 'note')->textarea(['rows'=>4]) ?>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
