<?php
use yii\helpers\Url;
use app\models\PackageAllowedOptions;

$txtCondition='';
$showMemberOpt=false;
$service=Yii::$app->appHelperFunctions->getPackageOptionByKeyword('2users');

$results=PackageAllowedOptions::find()->where(['service_id'=>$service['id']])->asArray()->all();
if($results!=null){
	foreach($results as $result){
		if($txtCondition!='')$txtCondition.=' || ';
		$txtCondition.='selPackId=='.$result['package_id'];
	}
}

$this->registerJs('
	$(document).delegate(".btn-renew", "click", function() {
		id=$(this).data("key");
		heading=$(this).data("heading");
		$.ajax({
			url: "'.Url::to(['contract/renew','id'=>'']).'"+id,
			dataType: "html",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html(heading);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
        initRenewJs();
			},
			error: bbAlert
		});
	});
  $("body").on("beforeSubmit", "form#renew-form", function () {
		_targetContainer=$("#renew-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Renewd').'", html: "'.Yii::t('app','Member is renewed').'", type: "success"});
					  window.closeModal();
            $.pjax.reload({container: "#grid-container", timeout: 2000});
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});

')
?>
<script>
function initRenewJs(){
  $(".dtpicker").datepicker({
  	format: "yyyy-mm-dd",
  }).on("changeDate", function(e){
  	$(this).datepicker("hide");
  });
  <?php if($txtCondition!=''){?>
  $("#renewform-package_id").change(function() {
  	selPackId=$(this).val();
  	if(<?= $txtCondition?>){
  		$("#multi-user").show();
  	}else{
  		$("#second-user option:eq(0)").attr("selected","selected");
  		$("#multi-user").hide();
  	}
  });
  <?php }?>
  $("#second-user").select2({
  	placeholder: "Search User",
  	allowClear: true,
  	width: "100%",
  });
}
</script>
