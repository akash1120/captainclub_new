<?php
use yii\grid\GridView;
use app\components\widgets\CustomPjax;

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'city_id','label'=>'City','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return ($model->boat!=null ? $model->boat->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'date','attribute'=>'booking_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>false];
$columns[] = ['format'=>'html','label'=>'Status','attribute'=>'status','value'=>function($model){return '<span class="badge badge-'.($model->status==1 ? 'success' : 'dark').'">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>';}];
$columns[]=['format'=>'raw','label'=>'Addons','value' => function ($model){return $model->addonsMenu;},'contentOptions'=>['class'=>'nosd']];
?>
<style>
.request-booking-list .filters{display: none;}
</style>
<div class="request-booking-list">
  <?php CustomPjax::begin(['id'=>'mem-booking-container']); ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'rowOptions' => function ($model, $index, $widget, $grid){
      return ['class'=>'item-row','data-city_id'=>$model->city_id, 'data-marina_id'=>$model->port_id];
    },
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
