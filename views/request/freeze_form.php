<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

//$member=Yii::$app->user->identity;
$myContractFreezingStats=Yii::$app->statsFunctions->getFreezeDaysByContract($member->active_contract_id);
$remainingFreezeDays=$myContractFreezingStats['remaining'];

$freezeDays=[];
if($remainingFreezeDays>10){
	for($n=10;$n<=$remainingFreezeDays;$n++){
		$freezeDays[$n]=$n.' '.($n==1 ? 'Day' : 'Days');
	}
}else{
	$freezeDays[$remainingFreezeDays]=$remainingFreezeDays.' '.($remainingFreezeDays==1 ? 'Day' : 'Days');
}

$this->registerJs('
$(".dtpicker").datepicker({
	format: "yyyy-mm-dd",
	todayHighlight: true,
	startDate: "today",
	endDate: "'.$member->activeContract->end_date.'",
}).on("changeDate", function(e){
	$(this).datepicker("hide");
});
')
?>
<?php $form = ActiveForm::begin(['id'=>'reqFreezeForm','action'=>Url::to(['request/freeze','user_id'=>$model->user_id])]); ?>
<div class="alert alert-info"><strong>Freezing Days Remaining:</strong> <?= $remainingFreezeDays?></div>
<?= $form->field($model, 'noofdays')->dropDownList($freezeDays) ?>
<?= $form->field($model, 'start_date')->textInput(['class'=>'form-control dtpicker','readonly'=>'readonly','autocomplete'=>'off']) ?>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
