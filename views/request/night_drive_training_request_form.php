<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$bookingCount=Yii::$app->statsFunctions->getUserAMPMBookingsCount($model->user_id);
$bookingCount=7;
if($bookingCount<5){
?>
<div class="alert alert-warning">
  Dear Captain for safety purposes, to operate the boats at night you shall have at least 5 bookings am or pm prior to scheduling orientation.
</div>
<?php
}else{
?>
<?php $form = ActiveForm::begin(['id'=>'ndtRequestForm']); ?>
<?= $form->field($model, 'descp')->textArea(['rows'=>5]) ?>
<div class="row">
    <div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="resetModelWin()">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
<?php }?>
