<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->registerJs('
$(".bbrdtpicker").datepicker({
	format: "yyyy-mm-dd",
	todayHighlight: true,
	startDate: "today",
}).on("changeDate", function(e) {
	$(this).datepicker("hide");
});
');
?>
<?php $form = ActiveForm::begin(['id'=>'reqBookingForm','action'=>Url::to(['request/boat-booking','user_id'=>$model->user_id])]); ?>
<div>
	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr,['prompt'=>$model->getAttributeLabel('city_id')]) ?>
		</div>
		<div class="col-sm-6">
			<?= $form->field($model, 'marina')->dropDownList([],['prompt'=>$model->getAttributeLabel('marina')]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'requested_date')->textInput(['class'=>'form-control bbrdtpicker','readonly'=>'readonly']) ?>
		</div>
		<div class="col-sm-6">
			<?= $form->field($model, 'time_slot')->dropDownList($model->timeZones,['prompt'=>$model->getAttributeLabel('time_slot')]) ?>
		</div>
	</div>
</div>
<?= $form->field($model, 'descp')->textArea(['rows'=>5]) ?>
<div id="first-step" class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmitbbf','class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Back'), 'javascript:;', ['onclick'=>'javascript:goBackToRequest();','data-dismiss'=>'modal','class' => 'btn btn-primary']) ?>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal" onclick="resetModelWin()">Close</button>
	</div>
</div>
<div id="last-step" class="row" style="display:none;">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmitbb3','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
