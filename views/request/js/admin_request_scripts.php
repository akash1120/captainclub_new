<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
	$("body").on("beforeSubmit", "form#trainingdone-form", function () {
		_targetContainer=$("#trainingdone-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
				return false;
		 }
		 // submit form
		 $.ajax({
				url: form.attr("action"),
				type: "post",
				data: form.serialize(),
				success: function (response) {
					if(response=="success"){
						swal({title: "'.Yii::t('app','Updated').'", html: "'.Yii::t('app','Request Updated. Thank you.').'", type: "success"});
						window.closeModal();
					}else{
						swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
					}
					App.unblockUI($(_targetContainer));
					if($("#grid-container").length){
						$.pjax.reload({container: "#grid-container", timeout: 2000});
					}
					if($("#db-admin-container").length){
						$.pjax.reload({container: "#db-admin-container", timeout: 2000});
					}
				}
		 });
		 return false;
	});
	$(document).delegate(".ndtraining-met", "click", function() {
		id=$(this).parents("tr").data("key");
		$.ajax({
			url: "'.Url::to(['request-training-action','s'=>'1','id'=>'']).'"+id,
			dataType: "html",
			success: function(data) {
				$("#requestAction").find(".modal-body").html(data);
				$("#requestAction").modal();
			},
			error: bbAlert
		});
	});
	$("body").on("beforeSubmit", "form#popup-form", function () {
		_targetContainer=$("#popup-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
				return false;
		 }
		 // submit form
		 $.ajax({
				url: form.attr("action"),
				type: "post",
				data: form.serialize(),
				success: function (response) {
					if(response=="success"){
						swal({title: "'.Yii::t('app','Updated').'", html: "'.Yii::t('app','Request Updated. Thank you.').'", type: "success"});
						window.closeModal();
					}else{
						swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
					}
					App.unblockUI($(_targetContainer));
					if($("#grid-container").length){
						$.pjax.reload({container: "#grid-container", timeout: 2000});
					}
					if($("#db-admin-container").length){
						$.pjax.reload({container: "#db-admin-container", timeout: 2000});
					}
				}
		 });
		 return false;
	});
	$(document).delegate(".btn-delrequest", "click", function() {
		item_type=$(this).data("item_type");
		id=$(this).data("id");
		var _targetContainer="#grid-container";
		swal({
			title: "'.Yii::t('app','Confirmation').'",
			html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
			type: "warning",
			showCancelButton: true,
	    confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
	    if (result.value) {
	      App.blockUI({
	  			message: "'.Yii::t('app','Please wait...').'",
	  			target: _targetContainer,
	  			overlayColor: "none",
	  			cenrerY: true,
	  			boxed: true
	  		});
	  		$.ajax({
	  			url: "'.Url::to(['request/delete','id'=>'']).'"+id+"&item_type="+item_type,
	  			dataType: "html",
	  			type: "POST",
	  			success: function(html) {
	  				App.unblockUI($(_targetContainer));
	  				swal({title: "'.Yii::t('app','Deleted!').'", html: "'.Yii::t('app','Request deleted.').'", type: "success", timer: 3000});

						if($("#grid-container").length){
							$.pjax.reload({container: "#grid-container", timeout: 2000});
						}
						if($("#db-admin-container").length){
							$.pjax.reload({container: "#db-admin-container", timeout: 2000});
						}
	  			},
	  			error: bbAlert
	  		});
	    }
	  });
	});
');
?>
