<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
	$("body").on("beforeSubmit", "form#reqFreezeForm", function () {
		var _targetContainer="#reqFreezeForm";
		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Confirmed').'", html: "'.Yii::t('app','Your request is confirmed. Thank you.').'", type: "success", timer: 5000});
						window.closeModal();
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error", timer: 5000});
				  }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
');
?>
