<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
  $("body").on("change", "#requestbookingsuggestion-city_id", function () {
		loadSuggestedInfo();
	});
  $("body").on("beforeSubmit", "form#bookSuggestionPopForm", function () {
		var _targetContainer="#bookSuggestionPopForm";
		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
				dataType: "json",
			  success: function (response) {
					if(response["success"]){
						document.getElementById("reqBookingSuggestionForm").reset();
            $("#requestbookingsuggestion-date").val("").datepicker("update");
            $("#modal-boat-container").html("");
						swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
						closeModal();
					}else{
						swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
					}
					if($("#pjax-booking").length>0){
						$.pjax.reload({container: "#pjax-booking", timeout: 2000});
					}
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
	$("body").on("beforeSubmit", "form#bookTimeSlotPopForm", function () {
		var _targetContainer="#bookTimeSlotPopForm";
		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
				dataType: "json",
			  success: function (response) {
					if(response["success"]){
						form.trigger("reset");
            document.getElementById("reqBookingSuggestionForm").reset();
            $("#requestbookingsuggestion-date").val("").datepicker("update");
            $("#modal-boat-container").html("");
						swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
						closeModal();
					}else{
						swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
					}
					if($("#pjax-booking").length>0){
						$.pjax.reload({container: "#pjax-booking", timeout: 2000});
					}
					if($("#db-user-booking-container").length>0){
						$.pjax.reload({container: "#db-user-booking-container", timeout: 2000});
					}
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});

  $(document).delegate(".load-sel-boats", "click", function() {
    _this=$(this);
    if(typeof _this.data("swarning_alert") !== "undefined"){
      warningDiv = _this.data("swarning_alert");
      html = $("#"+warningDiv).html();
      swal({
        title: "Dear Captain",
        html: html,
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#47a447",
        confirmButtonText: "Noted, proceed with booking",
        cancelButtonText: "Cancel",
      }).then((result) => {
        if (result.value) {
          showTimeBoats(_this);
        }
      });
    }else{
      showTimeBoats(_this);
    }
  });
	$(document).delegate(".btn.sspBookIt", "click", function() {
		_this=$(this);
		if(typeof _this.data("swarning_alert") !== "undefined"){
			warningDiv = _this.data("swarning_alert");
			html = $("#"+warningDiv).html();
			swal({
				title: "Dear Captain",
				html: html,
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#47a447",
				confirmButtonText: "Noted, proceed with booking",
				cancelButtonText: "Cancel",
			}).then((result) => {
				if (result.value) {
					booksSpecialBoatRequest(_this);
				}
			});
		}else{
			booksSpecialBoatRequest(_this);
		}
	});
');
?>
<script>
function showTimeBoats(el)
{
  modalId="general-modal";
  var _targetContainer="#requestBoatBooking-modal";
   App.blockUI({
    message: "<?= Yii::t('app','Please wait...')?>",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });
  url=el.data("url");
  heading=el.data("heading");
  $.ajax({
    url: url,
    dataType: "html",
    success: function(data) {
      $("#"+modalId).find("h5.modal-title").html(heading);
      $("#"+modalId).find(".modalContent").html(data);
      $("#"+modalId).modal();
      App.unblockUI($(_targetContainer));
    },
    error: bbAlert
  });
}
function booksSpecialBoatRequest(_this)
{
	msgDiv = _this.data("cnfm");
	bSelection=_this.data("boat_sel");
	boatName=_this.data("boat_name");
	date=_this.data("date");
	marina_id=_this.data("marina_id");
	boat_id=_this.data("boat_id");
	html=$("#"+msgDiv);
	swal({
		title: "Dear Captain",
		html: html,
		type: "info",
		showCancelButton: true,
		confirmButtonColor: "#47a447",
		confirmButtonText: "Yes, Book now",
		cancelButtonText: "Cancel",
	}).then((result) => {
		if (result.value) {
      if(bSelection==1){
				$.ajax({
					url: "<?= Url::to(['booking/night-drive-boat-selection','source'=>'suggestion','marina_id'=>''])?>"+marina_id+"&boat_id="+boat_id+"&date="+date,
					dataType: "html",
					success: function(data) {
						$("#general-modal").find("h5.modal-title").html("Please select boat you would like for "+boatName);
						$("#general-modal").find(".modalContent").html(data);
						$("#general-modal").modal();
					},
					error: bbAlert
				});
			}else{
  			sBookBoat(_this);
      }
		}
	});
}
</script>
