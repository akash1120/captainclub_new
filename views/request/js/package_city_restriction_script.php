<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;

//Script for Restricted Cities
if(Yii::$app->user->identity->user_type==0){
$jScript='';
$allowedCities=Yii::$app->appHelperFunctions->allowedCities;
$jScript.='var allowedCities=[';
if($allowedCities!=null){
	$jScript.=implode(",",$allowedCities);
}
$jScript.=']';
$this->registerJs($jScript.'
$(document).delegate("#bookingform-city_id", "change", function() {
	cval=parseInt($(this).val());
	if($(this).val()!=""){
		if(jQuery.inArray(cval, allowedCities) !== -1){

		}else{
			closeModal();
			$("#cityRestrictionPopUp").modal();
			$(this).val("");
			return false;
		}
	}
});
$(document).delegate("#requestbookingsuggestion-city_id", "change", function() {
	cval=parseInt($(this).val());
	if($(this).val()!=""){
		if(jQuery.inArray(cval, allowedCities) !== -1){

		}else{
			closeModal();
			$("#cityRestrictionPopUp").modal();
			$(this).val("");
			return false;
		}
	}
});
$(document).delegate(".btn-upgradeCity", "click", function() {
	_targetContainer="#cityRestrictionPopUp";
	$.ajax({
		url: "'.Url::to(['request/upgrade-city']).'",
		type: "POST",
		success: function(response) {
			response = jQuery.parseJSON(response);
			App.unblockUI($(_targetContainer));
			if(response["success"]){
				swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
				closeModal();
			}else{
				swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
			}
		},
		error: bbAlert
	});
});
');

Modal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'cityRestrictionPopUp',
  'size' => 'modal-md',
  'header' => '<h5 class="modal-title">Dear Captain</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton' => false,
]);
?>
<div class='modalContent'>
  Your membership is for 1 city only,<br /><br />
  for upgrading your membership for 2 cities access please click Upgrade. <a href="javascript:;" class="btn btn-xs btn-success btn-upgradeCity">Upgrade</a><br /><br />
  for a complimentary trip, subject to availability as per your contract. please click the request button. <a href="javascript:;" class="btn btn-xs btn-success load-modal" data-dismiss="modal" title="Requests" data-heading="Boat Booking" data-url="<?= Url::to(['request/boat-booking','user_id'=>Yii::$app->user->identity->id])?>">Request</a><br /><br />
  Best Regards
</div>
<?php
Modal::end();
?>
<?php }?>
