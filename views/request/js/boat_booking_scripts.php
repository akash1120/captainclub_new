<?php
use yii\helpers\Html;
use yii\helpers\Url;

$cities=Yii::$app->appHelperFunctions->cityListArr;
$txtJScript=Yii::$app->jsFunctions->getCityMarinaNameArr($cities);

$this->registerJs($txtJScript.'
$(document).delegate("#userbookingrequest-city_id", "change", function() {
	$("#userbookingrequest-marina").html("");
	array_list=cities[$(this).val()];
	$(array_list).each(function (i) {
		$("#userbookingrequest-marina").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
	});
});

$("body").on("beforeSubmit", "form#reqBookingForm", function () {
	var _targetContainer="#reqBookingForm";
	var form = $(this);
	// return false if form still have some validation errors
	if (form.find(".has-error").length) {
		return false;
	}
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});
	// submit form
	$.ajax({
		url: form.attr("action"),
		type: "post",
		data: form.serialize(),
		success: function (response) {
			if(response=="success"){
				swal({title: "'.Yii::t('app','Sent').'", html: "'.Yii::t('app','Request Sent Captain, We will be in touch shortly. Thank you.').'", type: "success", timer: 5000});
				closeModal();
				form.trigger("reset");
			}else{
				swal({title: "'.Yii::t('app','Error').'", html: response, type: "error", timer: 5000});
			}
			App.unblockUI($(_targetContainer));
		}
	});
	return false;
});
');
?>
