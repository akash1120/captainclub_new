<?php
use yii\helpers\Html;
use yii\helpers\Url;

$subBoatSelectionBoatIds = json_encode(Yii::$app->appHelperFunctions->subBoatSelectionBoatIds);

$this->registerJs('
$(document).delegate("#loguserrequesttoboatassign-marina_id", "change", function() {
	marina=$(this).val();
	date=$(this).data("date");
	time=$("#loguserrequesttoboatassign-time_id").val();
	$("#loguserrequesttoboatassign-selected_boat").val("0");
	loadFreeBoats(marina,date,time,".modal-content","loguserrequesttoboatassign-boat_id","");
});
$(document).delegate("#loguserrequesttoboatassign-time_id", "change", function() {
	marina=$("#loguserrequesttoboatassign-marina_id").val();
	date=$(this).data("date");
	time=$(this).val();
	$("#loguserrequesttoboatassign-selected_boat").val("0");
	loadFreeBoats(marina,date,time,".modal-content","loguserrequesttoboatassign-boat_id","");
});
$(document).delegate("#loguserrequesttoboatassign-boat_id", "change", function() {
	boatId=$(this).val();
	subBoatSelectionBoatIds='.$subBoatSelectionBoatIds.';
	if($.inArray(boatId,subBoatSelectionBoatIds) !== -1){
		marina_id=$("#loguserrequesttoboatassign-marina_id").val();
		date=$("#loguserrequesttoboatassign-date").val();
		boatName=$("#loguserrequesttoboatassign-boat_id option:selected" ).text();
		fld="loguserrequesttoboatassign";
		openBoatSelection(marina_id,date,boatName,fld)
	}else{
		$("#loguserrequesttoboatassign-selected_boat").val("0");
	}
});
$("body").on("beforeSubmit", "form#request-boat-assign-form", function () {
	_targetContainer=$("#request-boat-assign-form")
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});

	 var form = $(this);
	 // return false if form still have some validation errors
	 if (form.find(".has-error").length) {
			return false;
	 }
	 // submit form
	 $.ajax({
			url: form.attr("action"),
			type: "post",
			data: form.serialize(),
			success: function (response) {
				if(response=="success"){
					swal({title: "'.Yii::t('app','Booked').'", html: "'.Yii::t('app','Boat booked successfully').'", type: "success"});
					window.closeModal();
				}else{

					swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				}
				'.(Yii::$app->controller->id=='user-requests' ? '$.pjax.reload({container: "#grid-container", timeout: 2000});' : '$.pjax.reload({container: "#dashboard-container", timeout: 2000});').'
				App.unblockUI($(_targetContainer));
			}
	 });
	 return false;
});
');
?>
