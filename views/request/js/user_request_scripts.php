<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
	$(document).delegate(".btn-del-request", "click", function() {
		item_type=$(this).data("item_type");
		id=$(this).data("id");
		var _targetContainer="#db-user-request-container";
		swal({
			title: "'.Yii::t('app','Confirmation').'",
			html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
			type: "warning",
			showCancelButton: true,
	    confirmButtonColor: "#47a447",
			confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
	    if (result.value) {
	      App.blockUI({
	  			message: "'.Yii::t('app','Please wait...').'",
	  			target: _targetContainer,
	  			overlayColor: "none",
	  			cenrerY: true,
	  			boxed: true
	  		});
	  		$.ajax({
	  			url: "'.Url::to(['request/delete','id'=>'']).'"+id+"&item_type="+item_type,
	  			dataType: "html",
	  			type: "POST",
	  			success: function(html) {
	  				swal({title: "'.Yii::t('app','Deleted!').'", html: "'.Yii::t('app','Request deleted.').'", type: "success", timer: 3000});
						$.pjax.reload({container: "#db-user-request-container", timeout: 10000});
						$.pjax.reload({container: "#db-user-infOpt-container", timeout: 10000});
		  			App.unblockUI($(_targetContainer));
	  			},
	  			error: bbAlert
	  		});
	    }
	  });
	});
');
?>
