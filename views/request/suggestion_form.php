<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id'=>'reqSuggestionForm']); ?>
Dear Captain<br />
This is directly linked to the managing director's email. Please feel free to express what you want.
<center><strong>"Your satisfaction is our success"</strong></center>
<?= $form->field($model, 'descp')->textArea(['rows'=>5]) ?>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
