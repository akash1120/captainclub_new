<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$limit=5;
$oneToThree=[];
for($n=1;$n<=$limit;$n++){
	$oneToThree[$n]=$n;
}
$fourToTwelve=[];
for($n=1;$n<=$limit;$n++){
	$fourToTwelve[$n]=$n;
}
?>
<?php $form = ActiveForm::begin(['id'=>'reqKidsLifeJacketsForm','action'=>Url::to(['request/book-kids-life-jacket','id'=>$model->booking_id])]); ?>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'no_of_one_to_three_jackets')->dropDownList($oneToThree,['prompt'=>'Select']) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'no_of_four_to_twelve_jackets')->dropDownList($fourToTwelve,['prompt'=>'Select']) ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
