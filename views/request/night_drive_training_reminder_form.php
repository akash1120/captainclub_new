<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id'=>'ndtRequestReminderForm']); ?>
<div class="alert alert-info">Dear Member your night drive request is already in the system. If you wish you can send a reminder. Please enter your comments and click submit.</div>
<?= $form->field($model, 'comments')->textArea(['rows'=>5]) ?>
<div class="row">
    <div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="resetModelWin()">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
