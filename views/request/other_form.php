<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		startDate: "today",
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
');
$url=[];
$url[]='request/other';
if($booking_id!=null){
	$url['user_id']=$user_id;
	$url['booking_id']=$booking_id;
}
?>
<?php $form = ActiveForm::begin(['id'=>'reqOtherForm','action'=>Url::to($url)]); ?>
<?php if($booking_id==null){?>
<?= $form->field($model, 'requested_date')->textInput(['class'=>'form-control dtpicker','readonly'=>'readonly','autocomplete'=>'off']) ?>
<?php }?>
<?= $form->field($model, 'descp')->textArea(['rows'=>5]) ?>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal" onclick="resetModelWin()">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
