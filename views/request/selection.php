<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use app\models\RequestBookingSuggestion;

$weekendOff=true;
$isAdmin=$user->user_type!=0 ? true : false;

$this->registerJs('
  $(".bsgdtpicker").datepicker({
    format: "yyyy-mm-dd",
    todayHighlight: true,
    startDate: "today",
    '.($isAdmin ? '' : 'endDate: "'.$user->maxPackageEndDate.'",').'
    //'.(!$isAdmin && $weekendOff==true ? 'daysOfWeekDisabled: [5,6],' : '').'
  }).on("changeDate", function(e){
    $(this).datepicker("hide");
    if($("#requestbookingsuggestion-city_id").val()==""){
      swal({title: "'.Yii::t('app','Attention').'", html: "'.Yii::t('app','Please select city').'", type: "success", timer: 5000});
      return false;
    }
    loadSuggestedInfo();
  });
');

Modal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'request-modal',
  'size' => 'modal-md',
  'header' => '<h5 class="modal-title">Request</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton' => false,
]);
?>
<div class='modalContent'>
  <div id="type-sel" class="reqs">
    <label for="reqType">Select Type</label>
    <select name="reqType" id="reqType">
      <option value="">Select</option>
      <option value="req_boat_suggestions">New Boat Booking</option>
      <option value="req_booking_addons">Related to an existing Booking</option>
      <option value="req_freeze">Freeze</option>
      <option value="req_night_drive_training">Night Drive Orientation</option>
      <!--option value="req_other">Other</option-->
    </select>
    <button class="btn btn-success" onclick="openModal()">Next</button>
  </div>
</div>
<?php
Modal::end();

Modal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'requestBoatBooking-modal',
  'size' => 'modal-lg',
  'header' => '<h5 class="modal-title">Booking Suggestions</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton' => false,
]);
if($user->user_type==0 && $user->canBookBoat==false){
  $userCredits=$user->profileInfo->credit_balance;
  echo 'Dear Captain,<br />
  Your current balance is '.$userCredits.' AED. Please deposit funds by clicking <a href="javascript:;" class="load-modal" data-dismiss="modal" data-url="'.Url::to(['order/create']).'" data-heading="Deposit Funds">here</a>.<br /><br />
  Enjoy Boating With Relief<br />';
  echo '<div class="clearfix">';
  echo '  <a class="btn btn-primary pull-right load-modal" href="javascript:;" data-dismiss="modal" data-url="'.Url::to(['request/boat-booking','user_id'=>$user->id]).'" data-heading="Boat Booking">Submit Booking Request</a>';
  echo '</div>';

}else{
$model = new RequestBookingSuggestion();
?>
<div class='modalContent'>
  <?php $form = ActiveForm::begin(['id'=>'reqBookingSuggestionForm','action'=>Url::to(['request/boat-booking-suggestion'])]); ?>
  <div id="bb-main">
      <div class="row">
          <div class="col-6 col-sm-6">
              <?= $form->field($model, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr,['class'=>'form-control selMenu2','prompt'=>$model->getAttributeLabel('city_id')]) ?>
          </div>
          <div class="col-6 col-sm-6">
              <?= $form->field($model, 'date')->textInput(['class'=>'form-control bsgdtpicker','readonly'=>'readonly']) ?>
          </div>
      </div>
  </div>
  <?php ActiveForm::end(); ?>
  <div id="modal-boat-container"></div>
  <div id="btn-row" class="row" style="display:none;">
      <div class="col-md-12">
  		<?= Html::a(Yii::t('app', 'Ignore Suggestion Submit Request'), 'javascript:;', ['data-dismiss'=>'modal', 'data-url'=>Url::to(['request/boat-booking','user_id'=>$user->id]), 'data-heading'=>Yii::t('app','Boat Booking'),'class' => 'btn btn-primary load-modal']) ?>
      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
  	</div>
  </div>
</div>
<?php
}
Modal::end();
?>
<script>
function goBackToRequest(){
  if($("#reqType").val()==""){

  }else{
    openModal();
  }
}
function openModal(){
	id=$("#reqType").val();
  url="";
  heading='';
	if(id!=""){
		window.closeModal();
		if(id=='req_other'){
      url="<?= Url::to(['request/other'])?>";
  		heading='Other';
		}else if(id=='req_freeze'){
      url="<?= Url::to(['request/freeze','user_id'=>$user->id])?>";
      heading='Freeze Account';
		}else if(id=='req_boat_suggestions'){
      $("#requestBoatBooking-modal").modal();
		}else if(id=='req_booking_addons'){
      url="<?= Url::to(['request/booking-list','user_id'=>$user->id])?>";
      heading='Request For Existing Booking';
		}else if(id=='req_night_drive_training'){
      url="<?= Url::to(['request/night-drive-training','user_id'=>$user->id])?>";
      heading='Request Night Drive Training';
		}
    if(url!="" && heading!=""){
		$.ajax({
			url: url,
			dataType: "html",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html(heading);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
			},
			error: bbAlert
		});
    }
	}else{
		swal({title: "<?= Yii::t('app','Attention')?>", html: "<?= Yii::t('app','Please select request type')?>", type: "warning"});
	}
}
function loadSuggestedInfo(){
  $("#btn-row").hide();
	if($("#requestbookingsuggestion-city_id").val()!="" && $("#requestbookingsuggestion-date").val()!=""){
		var _targetContainer="#reqBookingSuggestionForm";
		 App.blockUI({
			message: "<?= Yii::t('app','Please wait...')?>",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "<?= Url::to(['booking/suggestion-summary','user_id'=>$user->id,'city_id'=>''])?>"+$("#requestbookingsuggestion-city_id").val()+"&date="+$("#requestbookingsuggestion-date").val(),
			dataType: "html",
			success: function(html) {
				$("#modal-boat-container").html(html);
        if(html!=''){
          $("#btn-row").show();
        }
				App.unblockUI($(_targetContainer));
			},
			error: bbAlert
		});
	}
	$("#btnBookbtn").attr("disabled",false);
}
function saveSelectionToSpecialBoat(source,id)
{
  elId="#spboat_"+id;
  if(source=='suggestion'){
    elId="#sspboat_"+id;
  }
	$(elId).attr("data-selected_boat",$("#spBSel").val());
	window.closeModal();
	sBookBoat($(elId));
}
function sBookBoat(el)
{
	city_id=el.data("city_id");
	marina_id=el.data("marina_id");
	boat_id=el.data("boat_id");
	date=el.data("date");
	time_id=el.data("time_id");

	booking_type=0;
  if($("#bookingform-booking_type").length>0){
    booking_type=$("#bookingform-booking_type").val();
  }
	booking_comments='';
  if($("#bookingform-booking_comments").length>0){
    booking_comments=$("#bookingform-booking_comments").val();
  }

	if(el.data('selected_boat')){
		selected_boat=el.data("selected_boat");
	}else{
		selected_boat='';
	}


	myKeyVals = {"Booking[city_id]" : city_id, "Booking[port_id]" : marina_id, "Booking[boat_id]" : boat_id, "Booking[selected_boat]" : selected_boat, "Booking[booking_date]" : date, "Booking[booking_time_slot]" : time_id, "Booking[booking_type]" : booking_type, "Booking[booking_comments]" : booking_comments};
	_targetContainer=$("#grid-container")
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});
	$.ajax({
		url: "<?= Url::to(['booking/create'])?>",
		data: myKeyVals,
		type: "post",
		dataType: "json",
		success: function(response) {
			console.log(response);
			if(response["success"]){
					window.location.href="<?= Url::to(['site/index'])?>";
			}else{
				swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error"});
			}
			App.unblockUI($(_targetContainer));
		},
		error: bbAlert
	});

}
</script>
