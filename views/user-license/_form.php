<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$(".dtpicker").datepicker({
	format: "yyyy-mm-dd",
	todayHighlight: true,
}).on("changeDate", function(e){
	$(this).datepicker("hide");
});
');

$licenseImage=Yii::$app->fileHelperFunctions->getImagePath('user_license',$model['license_image'],'tiny');
?>
<?php $form = ActiveForm::begin(['id'=>'licenseForm','options'=>['enctype'=>'multipart/form-data']]); ?>
<?= $form->field($model, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr) ?>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'license_start')->textInput(['class'=>'form-control dtpicker','readonly'=>'readonly']) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'license_expiry')->textInput(['class'=>'form-control dtpicker','readonly'=>'readonly']) ?>
	</div>
</div>
<?= $form->field($model, 'license_image',['template'=>'
{label}
<div class="input-group file-input">
<div class="input-group-prepend">
<span class="input-group-text"><img src="'.$licenseImage.'" /></span>
</div>
<div class="custom-file">
{input}
<label class="custom-file-label" for="fldImg">Choose file</label>
</div>
</div>
{error}{hint}
'])->fileInput(['id'=>'fldImg','class'=>'custom-file-input','accept'=>'image/*']) ?>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Cancel', 'javascript:;', ['class' => 'btn btn-default', 'data-dismiss'=>'modal']) ?>
	</div>
</div>
<?php ActiveForm::end(); ?>
