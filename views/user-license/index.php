<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserLicenseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create','user-license')){
  $createBtn=true;
}
?>
<style>
.filters{display: none;}
</style>
<div class="user-license-index">
  <?php if($createBtn==true){?>
  <div class="row">
    <div class="col-sm-12 text-right">
      <a href="javascript:;" class="btn btn-success btn-xs load-modal" data-url="<?= Url::to(['user-license/create','user_id'=>$model->id])?>" data-heading=" <?= $model->fullname?> - Add License"><i class="fa fa-plus"></i></a>
    </div>
  </div>
  <?php }?>
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= $this->render('_gridview',['searchModel'=>$searchModel,'dataProvider'=>$dataProvider,'model'=>$model])?>
  <?php CustomPjax::end(); ?>
</div>

<script>
function showDetail(_t){
  html = '';
  html+= '<div class="row">';
  html+= '  <div class="col-xs-12 col-sm-12">';
  html+= '<table class="view-detail table table-striped table-bordered">';
  if(_t.data("photo")!='' && _t.data("photo")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Photo:')?></th>';
  html+= '    <td><img src="'+_t.data("photo")+'" alt="" /></td>';
  html+= '  </tr>';
  }
  html+= '  <tr>';
  html+= '    <th width="150"><?= Yii::t('app','City:')?></th>';
  html+= '    <td>'+_t.data("city")+'</td>';
  html+= '  </tr>';
  if(_t.data("start_date")!='' && _t.data("start_date")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Start:')?></th>';
  html+= '    <td>'+_t.data("start_date")+'</td>';
  html+= '  </tr>';
  }
  if(_t.data("end_date")!='' && _t.data("end_date")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Expiry:')?></th>';
  html+= '    <td>'+_t.data("end_date")+'</td>';
  html+= '  </tr>';
  }
  html+= '</table>';

  html+= '  </div>';
  html+= '</div>';
  viewPopUpModal("<?= $model->fullname.' - '.Yii::t('app','License Detail')?>",html)
}
</script>
<?= $this->render('/user-license/js/user_license_scritps')?>
