<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;

$createBtn=false;
$actionBtns="";
if(Yii::$app->controller->id=="user" && Yii::$app->controller->action->id=='update'){
	$actionBtns='{view}';
}

if(Yii::$app->menuHelperFunction->checkActionAllowed('update','user-license')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete','user-license')){
  $actionBtns.='{delete}';
}
?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'createBtn' => $createBtn,
  'cardHeader' => false,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
    ['format' => ['image',['width'=>'40','height'=>'40']],'label'=>'Photo','attribute'=>'license_image','value'=>function($model){
      return Yii::$app->fileHelperFunctions->getImagePath('user_license',$model['license_image'],'small');
    },'headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']],
    ['attribute'=>'city_name','label'=>'City'],
    ['format'=>'date','attribute'=>'license_start','label'=>'Start'],
    ['format'=>'date','attribute'=>'license_expiry','label'=>'Expiry'],
    [
      'class' => 'yii\grid\ActionColumn',
      'header'=>'',
      'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
      'contentOptions'=>['class'=>'noprint actions'],
      'template' => '
        <div class="btn-group flex-wrap">
          <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
          </button>
          <div class="dropdown-menu" role="menu">
            '.$actionBtns.'
          </div>
        </div>',
      'buttons' => [
          'view' => function ($url, $model) {
            return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), 'javascript:;', [
              'title' => Yii::t('app', 'View'),
              'class'=>'dropdown-item text-1 btn-view',
              'data-pjax'=>"0",
              'data-photo'=>Yii::$app->fileHelperFunctions->getImagePath('user_license',$model['license_image'],'medium'),
              'data-city'=>$model['city_name'],
              'data-start_date'=>Yii::$app->formatter->asDate($model['license_start']),
              'data-end_date'=>Yii::$app->formatter->asDate($model['license_expiry']),
            ]);
          },
          'update' => function ($url, $model) {

						if(Yii::$app->controller->id=="user" && Yii::$app->controller->action->id=='update'){
							$htmlModal="general-modal";
						}else{
							$htmlModal="general-modal-secondary";
						}
            $memberName=Yii::$app->helperFunctions->getMemberFullName($model['username'],$model['firstname'],$model['lastname']);
            return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), 'javascript:;', [
              'title' => Yii::t('app', 'Edit'),
              'class'=>'dropdown-item text-1 nosd load-modal',
              'data-url'=>Url::to(['user-license/update','id'=>$model['id']]),
              'data-heading'=>''.$memberName.' - Update License',
              'data-mid'=>$htmlModal,
              'data-pjax'=>"0",
            ]);
          },
          'delete' => function ($url, $model) {
            $memberName=Yii::$app->helperFunctions->getMemberFullName($model['username'],$model['firstname'],$model['lastname']);
            return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), 'javascript:;', [
              'title' => Yii::t('app', 'Delete'),
              'class'=>'dropdown-item text-1 nosd del-lic',
              'data-id'=>$model['id'],
              'data-username'=>''.$memberName.'',
              'data-pjax'=>"0",
            ]);
          },
      ],
    ],
  ],
]);?>
