<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
$("body").on("beforeSubmit", "form#licenseForm", function () {
	_targetContainer=$("#licenseForm")
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});

	 var form = $(this);
	 // return false if form still have some validation errors
	 if (form.find(".has-error").length) {
			return false;
	 }
	 var formData = new FormData(form[0]);
	 // submit form
	 $.ajax({
			url: form.attr("action"),
			type: "post",
			data: formData,
			success: function (response) {
				if(response=="success"){
					swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Information updated successfully').'", type: "success"});
          if($("#license-grid-container").length>0){

          }else{
            window.closeModal();
          }
				}else{
					swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				}
				App.unblockUI($(_targetContainer));
        if($("#license-grid-container").length>0){
          $.pjax.reload({container: "#license-grid-container", timeout: 40000});
        }else{
          $.pjax.reload({container: "#grid-container", timeout: 2000});
        }

			},
	    cache: false,
	    contentType: false,
	    processData: false
	 });
	 return false;
});
$(document).delegate(".del-lic", "click", function() {
  id=$(this).data("id");
  username=$(this).data("username");
  swal({
    title: "'.Yii::t('app','Confirm').'",
    html: "'.Yii::t('app','Are you sure you want to delete this?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Yes, Delete it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: "'.Url::to(['user-license/delete','id'=>'']).'"+id,
        dataType: "html",
        method: "POST",
        success: function(data) {
          swal({title: "'.Yii::t('app','Deleted').'", html: "'.Yii::t('app','License deleted successfully').'", type: "success"});
          if($("#license-grid-container").length>0){
            //$.pjax.reload({container: "#license-grid-container", timeout: 2000});
          }else{
            $.pjax.reload({container: "#grid-container", timeout: 2000});
          }
        },
        error: bbAlert
      });
    }
  });
});
');
?>
