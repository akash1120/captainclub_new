<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BlackListedEmail */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Import Bounced Emails');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bounced Emails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
  <div class="col-xs-12 col-sm-6">
    <section class="manual-import-form card card-featured card-featured-warning">
      <?php $form = ActiveForm::begin(); ?>
    	<header class="card-header">
    		<h2 class="card-title"><?= $this->title?> (Manual)</h2>
    	</header>
    	<div class="card-body">
        <?= $form->field($model2, 'emails')->textArea(['rows' => 10]) ?>
      </div>
      <div class="card-footer">
          <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
          <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
    	</div>
      <?php ActiveForm::end(); ?>
    </section>
  </div>
  <div class="col-xs-12 col-sm-6">
    <section class="mailgun-import-form card card-featured card-featured-warning">
      <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    	<header class="card-header">
    		<h2 class="card-title"><?= $this->title?> (Mailgun CSV)</h2>
    	</header>
    	<div class="card-body">
        <?= $form->field($model, 'mail_gun_importfile')->fileInput() ?>
      </div>
      <div class="card-footer">
          <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn btn-success']) ?>
          <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-default'])?>
    	</div>
      <?php ActiveForm::end(); ?>
    </section>
  </div>
</div>
