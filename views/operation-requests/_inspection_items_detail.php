<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RequestInspection */
?>
<div class="inspection-items-list">
	<div class="row">
		<div class="col-sm-12"><h4><?= $model->boat->name?></h4></div>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th width="10">#</th>
				<th>Items to check</th>
				<th>Conditions & Availability</th>
				<th>Remarks & Inquiries</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$n=1;
			$inspectionItems=$model->inspectionItems;
			if($inspectionItems!=null){
				foreach($inspectionItems as $item){
					?>
					<tr>
						<td><?= $n?></td>
						<td><?= $item->item->title?></td>
						<td><?= $item->item_condition?></td>
						<td><?= $item->item_comments?></td>
					</tr>
					<?php
					$n++;
				}
			}
			?>
		</tbody>
	</table>
</div>
