<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\BreakdownRequestAssets;
BreakdownRequestAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestBreakDownSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Breakdown');
$this->params['breadcrumbs'][] = Yii::t('app', 'Operation Reports');
$this->params['breadcrumbs'][] = $this->title;


$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['attribute'=>'boat_id','value'=>function($model){
	return $model['boat_name'];
},'filter'=>Yii::$app->appHelperFunctions->getBoatsListArr()];
$columns[]='description';
$columns[]=['label'=>Yii::t('app','Level'),'attribute'=>'break_down_level','value'=>function($model){
	return Yii::$app->operationHelperFunctions->breakDownLevel[$model['break_down_level']];
},'filter'=>Yii::$app->operationHelperFunctions->breakDownLevel];
$columns[]=['format'=>'html','label'=>'Reason','attribute'=>'reason','value'=>function($model){
	return Yii::$app->operationHelperFunctions->breakDownReason[$model['reason']];
},'filter'=>Yii::$app->operationHelperFunctions->breakDownReason];
$columns[]=['format'=>'html','label'=>'Member Remarks','attribute'=>'user_remarks','value'=>function($model){
	return $model['user_remarks'];
}];
$columns[]=['format'=>'date','label'=>Yii::t('app','Date'),'attribute'=>'req_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
if(Yii::$app->user->identity->user_type==1){
	$columns[]=['format'=>'html','label'=>Yii::t('app','Status'),'attribute'=>'status','value'=>function($model){
		return Yii::$app->operationHelperFunctions->requestStatusIcon[$model['status']];
	},'filter'=>Yii::$app->operationHelperFunctions->requestStatus];
	$columns[]='admin_remarks';
	$columns[]=['format'=>'date','label'=>Yii::t('app','Action Date'),'attribute'=>'admin_action_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
}

$actionBtns='';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update','request-break-down')){
	$actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('change-status','request-break-down')){
	$actionBtns.='{change-status}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('admin-remarks','request-break-down')){
	$actionBtns.='{admin-remarks}';
}
$columns[]=[
	'class' => 'yii\grid\ActionColumn',
	'header'=>'Actions',
	'headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],
	'contentOptions'=>['class'=>'noprint nsd'],
	'template' => '
	<div class="btn-group flex-wrap">
	<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
	<span class="caret"></span>
	</button>
	<div class="dropdown-menu" role="menu">
	'.$actionBtns.'
	</div>
	</div>',
	'buttons' => [
		'update' => function ($url, $model) {
			if($model['status']==0){
				return Html::a('<i class="fas fa-edit"></i> Update', 'javascript:;', [
					'class'=>'dropdown-item text-1 load-modal',
					'data-url'=>Url::to(['request-break-down/update','id'=>$model['id']]),
					'data-heading' => Yii::t('app', 'Update Request'),
					'data-pjax'=>"0",
				]);
			}
		},
		'change-status' => function ($url, $model) {
			if($model['status']==0){
				return Html::a('<i class="fas fa-check"></i> Done', 'javascript:;', [
					'class'=>'dropdown-item text-1 change-status',
					'data-id'=>$model['id'],
					'data-title'=>'Done',
					'data-status'=>2,
				]).
				Html::a('<i class="fas fa-times"></i> Cancelled', 'javascript:;', [
					'class'=>'dropdown-item text-1 change-status',
					'data-id'=>$model['id'],
					'data-title'=>'Cancelled',
					'data-status'=>1,
				]);
			}
		},
		'admin-remarks' => function ($url, $model) {
			return Html::a('<i class="fas fa-edit"></i> Remarks', 'javascript:;', [
				'class'=>'dropdown-item text-1 load-modal',
				'data-url'=>Url::to(['request-break-down/admin-remarks','id'=>$model['id']]),
				'data-heading' => Yii::t('app', 'Remarks'),
				'data-pjax'=>"0",
			]);
		},
	],
];
?>
<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => $columns,
  'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
]); ?>
<?php CustomPjax::end(); ?>
<?= $this->render('js/breakdown_scripts')?>
<?= $this->render('/request-break-down/js/request_breakdown_scripts')?>
