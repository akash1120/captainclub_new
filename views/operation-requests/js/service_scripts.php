<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
	initDatePicker();
	$(document).on("pjax:success", function() {
		initDatePicker();
	});
	$(document).delegate(".change-status", "click", function() {
		_targetContainer=$("#grid-container")
		id=$(this).data("id");
		status=$(this).data("status");
		title=$(this).data("title");
		swal({
			title: "Confirmation",
			html: "Are you sure you want to mark this request as "+title,
			type: "question",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "'.Yii::t('app','Yes').'",
			cancelButtonText: "'.Yii::t('app','Cancel').'",
		}).then((result) => {
	    if (result.value) {
				App.blockUI({
					message: "'.Yii::t('app','Please wait...').'",
					target: _targetContainer,
					overlayColor: "none",
					cenrerY: true,
					boxed: true
				});
				if(id!="" && id!=undefined){
					$.ajax({
						url: "'.Url::to(['request-service/change-status','id'=>'']).'"+id+"&status="+status,
						type: "POST",
						success: function(response) {
							response = jQuery.parseJSON(response);
							App.unblockUI($(_targetContainer));
							if(response["success"]){
								swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
							}else{
								swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
							}
							$.pjax.reload({container: "#grid-container", timeout: 2000});
						},
						error: bbAlert
					});
				}
			}
		});
	});
	$("body").on("beforeSubmit", "form#serviceActionForm", function () {
		_targetContainer=$("#serviceActionForm")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Updated').'", html: "'.Yii::t('app','Request Updated. Thank you.').'", type: "success"});
					  window.closeModal();
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));
				  $.pjax.reload({container: "#grid-container", timeout: 2000});
			  }
		 });
		 return false;
	});
');
?>
<script>
function initDatePicker()
{
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
}
</script>
