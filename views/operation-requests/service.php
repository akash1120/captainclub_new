<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\ServiceRequestAssets;
ServiceRequestAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Service Report');
$this->params['breadcrumbs'][] = Yii::t('app', 'Operation Reports');
$this->params['breadcrumbs'][] = $this->title;

$colums[]=['class' => 'yii\grid\SerialColumn'];
$colums[]=['attribute'=>'boat_id','value'=>function($model){
	return $model['boat_name'];
},'filter'=>Yii::$app->appHelperFunctions->getBoatsListArr()];
$colums[]='current_hours';
$colums[]='scheduled_hours';
$colums[]=['format'=>'date','label'=>Yii::t('app','Date'),'attribute'=>'req_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$colums[]='remarks';
if(Yii::$app->user->identity->user_type==1){
	$colums[]=['format'=>'html','label'=>Yii::t('app','Status'),'attribute'=>'status','value'=>function($model){
		return Yii::$app->operationHelperFunctions->requestStatusIcon[$model['status']];
	},'filter'=>Yii::$app->operationHelperFunctions->requestStatus];
	$colums[]='admin_remarks';
	$colums[]=['format'=>'date','label'=>Yii::t('app','Action Date'),'attribute'=>'admin_action_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
}

$actionBtns='';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update','request-service')){
	$actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('change-status','request-service')){
	$actionBtns.='{change-status}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('admin-remarks','request-service')){
	$actionBtns.='{admin-remarks}';
}
$colums[]=[
	'class' => 'yii\grid\ActionColumn',
	'header'=>'Actions',
	'headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],
	'contentOptions'=>['class'=>'noprint nsd'],
	'template' => '
	<div class="btn-group flex-wrap">
	<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
	<span class="caret"></span>
	</button>
	<div class="dropdown-menu" role="menu">
	'.$actionBtns.'
	</div>
	</div>',
	'buttons' => [
		'update' => function ($url, $model) {
			if($model['status']==0){
				return Html::a('<i class="fas fa-edit"></i> Update', 'javascript:;', [
					'class'=>'dropdown-item text-1 load-modal',
					'data-url'=>Url::to(['request-service/update','id'=>$model['id']]),
					'data-heading' => Yii::t('app', 'Update Request'),
					'data-pjax'=>"0",
				]);
			}
		},
		'change-status' => function ($url, $model) {
			if($model['status']==0){
				return Html::a('<i class="fas fa-check"></i> Done', 'javascript:;', [
					'class'=>'dropdown-item text-1 change-status',
					'data-id'=>$model['id'],
					'data-title'=>'Done',
					'data-status'=>2,
				]).
				Html::a('<i class="fas fa-times"></i> Cancelled', 'javascript:;', [
					'class'=>'dropdown-item text-1 change-status',
					'data-id'=>$model['id'],
					'data-title'=>'Cancelled',
					'data-status'=>1,
				]);
			}
		},
		'admin-remarks' => function ($url, $model) {
			return Html::a('<i class="fas fa-edit"></i> Remarks', 'javascript:;', [
				'class'=>'dropdown-item text-1 load-modal',
				'data-url'=>Url::to(['request-service/admin-remarks','id'=>$model['id']]),
				'data-heading' => Yii::t('app', 'Remarks'),
				'data-pjax'=>"0",
			]);
		},
	],
];
?>

<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<?= CustomGridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => $colums,
	'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
]); ?>
<?php CustomPjax::end(); ?>
<?= $this->render('js/service_scripts')?>
<?= $this->render('/request-service/js/request_service_scripts')?>
