<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\InspectionRequestAssets;
InspectionRequestAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestInspectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Inspections');
$this->params['breadcrumbs'][] = Yii::t('app', 'Operation Reports');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
	initDatePicker();
	$(document).on("pjax:success", function() {
		initDatePicker();
	});
');
$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['attribute'=>'boat_id','value'=>function($model){
	return $model['boat_name'];
},'filter'=>Yii::$app->appHelperFunctions->getBoatsListArr()];
$columns[]=['attribute'=>'inspection_date','format'=>'date','label'=>Yii::t('app','Date'),'value'=>function($model){
	return $model['inspection_date'];
},'filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]='remarks';
$columns[]=['format'=>'raw','label'=>Yii::t('app','Items'),'value'=>function($model){
	return Html::a('View Items','javascript:;',['class'=>'load-modal','data-heading'=>'Inspection Items','data-url'=>Url::to(['inspection-detail','id'=>$model['id']])]);
}];
?>
<?php CustomPjax::begin(); ?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => $columns,
  'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
]); ?>
<?php CustomPjax::end(); ?>
<script>
function initDatePicker()
{
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
}
</script>
