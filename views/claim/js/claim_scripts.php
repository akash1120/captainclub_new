<?php
use yii\helpers\Html;
use yii\helpers\Url;

$cities=Yii::$app->appHelperFunctions->cityListArr;
$txtJScript=Yii::$app->jsFunctions->getCityMarinaArr($cities);

$this->registerJs($txtJScript.'
$(document).delegate(".cb,.select-on-check-all", "click", function() {
  claculateSelection();
});
$(".dtpicker").datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
}).on("changeDate", function(e){
  $(this).datepicker("hide");
});
_targetContainer=$("#booking-container");

$("#bookingsearch-city_id").change(function() {
  val=$(this).val();
  $("#bookingsearch-port_id").html("");
  array_list=cities[$(this).val()];
  $(array_list).each(function (i) {
    $("#bookingsearch-port_id").append("<option value=\""+array_list[i].value+"\">"+array_list[i].display+"</option>");
  });
});


$(document).delegate(".btnClaimAmount", "click", function() {
  pid=$(this).data("pid");
  cid=$(this).data("cid");
  maxAmount=parseFloat($(this).data("mid"));
  amount=$("#amt"+pid).val();
  comment=$("#comment"+pid).val();
  if(amount<=0){
    swal({title: "Attention", html: "Please provide valid amount", type: "error", timer: 5000});
    return;
  }
  if(amount>maxAmount){
    swal({title: "Attention", html: "Please provide valid amount", type: "error", timer: 5000});
    return;
  }
  swal({
    title: amount+" will be sent for claiming",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "'.Yii::t('app','Proceed').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: "'.Url::to(['claim-amount']).'",
        data: {"ClaimAmountForm[city_id]":cid,"ClaimAmountForm[port_id]":pid,"ClaimAmountForm[amount]":amount,"ClaimAmountForm[comments]":comment},
        type: "POST",
        success: function(response) {
          response = jQuery.parseJSON(response);
          App.unblockUI($(_targetContainer));
          if(response["success"]){
            swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
          }else{
            swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
          }
          $.pjax.reload({container: "#marinaClaimingContainer"+pid, timeout: 2000});
        },
        error: bbAlert
      });
    }
  });
});

$("body").on("beforeSubmit", "form#updateCommentForm", function () {
  _targetContainer=$("#updateCommentForm")
  App.blockUI({
    message: "'.Yii::t('app','Please wait...').'",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });

   var form = $(this);
   // return false if form still have some validation errors
   if (form.find(".has-error").length) {
      return false;
   }
   // submit form
   $.ajax({
      url: form.attr("action"),
      type: "post",
      data: form.serialize(),
      success: function (response) {
        if(response=="success"){
          swal({title: "'.Yii::t('app','Updated').'", html: "'.Yii::t('app','Comments updated successfully').'", type: "success"});
          window.closeModal();
        }else{
          swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
        }
        App.unblockUI($(_targetContainer));
        $.pjax.reload({container: "#booking-container", timeout: 2000});
      }
   });
   return false;
});
');
?>

<script>
function claimBulk(){
	swal({
		title: "Confirmation",
		html: "<?= Yii::t('app','Selected booking will be claimed under your name.')?>",
		type: "question",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "<?= Yii::t('app','Proceed')?>",
		cancelButtonText: "<?= Yii::t('app','Cancel')?>",
  }).then((result) => {
    if (result.value) {
      _targetContainer="#booking-container";
  		App.blockUI({
  			message: "<?= Yii::t('app','Please wait...')?>",
  			target: _targetContainer,
  			overlayColor: "none",
  			cenrerY: true,
  			boxed: true
  		});
  		city_id=<?= $searchModel->city_id?>;
  		port_id=<?= $searchModel->port_id?>;
  		comments=$("#empCOComments").val();
  		var allVals = [];
  		$('.cb:checked').each(function() {
  		   allVals.push($(this).val());
  		 });
  		 $.ajax({
   			url: "<?= Url::to(['claim-bulk'])?>",
  			data: {selection:allVals,city_id:city_id,port_id:port_id,comments:comments},
   			type: "POST",
   			success: function(response) {
   				response = jQuery.parseJSON(response);
   				App.unblockUI($(_targetContainer));
   				if(response["success"]){
   					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
   				}else{
   					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
   				}
   				$.pjax.reload({container: "#booking-container", timeout: 2000});
   			},
   			error: bbAlert
   		});
    }
	});
}
function claculateSelection()
{
  totalamt=0;
  $(".cb").each(function () {
     if(this.checked){
       tAmt=parseFloat($(this).parents("tr").data("price"));
       totalamt+=tAmt;
     }
  });
  $("#totalamt").html("AED "+totalamt);
}
</script>
