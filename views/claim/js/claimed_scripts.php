<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
	$(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: "YYYY-MM-DD",cancelLabel: "Clear"},});
	$(".dtrpicker").on("apply.daterangepicker", function(ev, picker) {
		$(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
		$(this).trigger("change");
	});
	$(document).delegate(".cb,.select-on-check-all", "click", function() {
	  claculateSelection();
	});

	$("body").on("beforeSubmit", "form#updateCommentForm", function () {
		_targetContainer=$("#updateCommentForm")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Updated').'", html: "'.Yii::t('app','Comments updated successfully').'", type: "success"});
					  window.closeModal();
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));
				  $.pjax.reload({container: "#booking-container", timeout: 2000});
			  }
		 });
		 return false;
	});
');
?>
<script>
function claculateSelection()
{
	totalamt=0;
	$(".cb").each(function () {
		 if(this.checked){
			 tAmt=parseFloat($(this).parents("tr").data("price"));
			 totalamt+=tAmt;
		 }
	});
	$("#totalamt").html("AED "+totalamt);
}
<?php if($list == 'pending'){?>
function approveBulk(){
	swal({
		title: "Confirmation",
		html: "<?= Yii::t('app','Selected claim will be approved.')?>",
		type: "question",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "<?= Yii::t('app','Proceed')?>",
		cancelButtonText: "<?= Yii::t('app','Cancel')?>",
  }).then((result) => {
    if (result.value) {
			_targetContainer="#booking-container";
			App.blockUI({
				message: "<?= Yii::t('app','Please wait...')?>",
				target: _targetContainer,
				overlayColor: "none",
				cenrerY: true,
				boxed: true
			});
			var allVals = [];
			$('.cb:checked').each(function() {
			   allVals.push($(this).val());
			 });
			 $.ajax({
	 			url: "<?= Url::to(['approve-bulk'])?>",
				data: {selection:allVals},
	 			type: "POST",
	 			success: function(response) {
	 				response = jQuery.parseJSON(response);
	 				App.unblockUI($(_targetContainer));
	 				if(response["success"]){
	 					swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
	 				}else{
	 					swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
	 				}
	 				$.pjax.reload({container: "#booking-container", timeout: 2000});
	 			},
	 			error: bbAlert
	 		});
		}
	});
}
<?php }?>
</script>
