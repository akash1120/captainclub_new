<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;
use yii\widgets\ActiveForm;
use app\models\BookingActivity;
use app\models\User;
use app\models\ClaimOrderComments;
use app\models\ClaimOrderItem;
use app\models\Booking;
use app\assets\AppDateRangePickerAsset;
AppDateRangePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if($searchModel->list_type == 'approved'){
	$this->title = Yii::t('app', 'Approved Claims');
}else{
	$this->title = Yii::t('app', 'Pending Claims');
}

$this->params['breadcrumbs'][] = $this->title;

$columns[]=['class' => 'yii\grid\SerialColumn','contentOptions'=>['style'=>'width: 20px;text-align:center;']];
if($searchModel->list_type != 'approved'){
$columns[]=['class' => 'yii\grid\CheckboxColumn','checkboxOptions'=>function ($model, $key, $index, $column){return ['value' => $model->id, 'class'=>'cb'];},'contentOptions'=>['style'=>'width: 20px;text-align:center;']];
}
$columns[]='ref_no';
$columns[]='created_at:date';
$columns[]=['attribute'=>'user_id','label'=>Yii::t('app','Username'),'value'=>function($model){
	return $model->user!=null ? $model->user->fullname : '';
}];
$columns[]=['attribute'=>'total_amount','value'=>function($model){
	return 'AED '.$model->total_amount;
}];
$columns[]=['format'=>'raw','label'=>'Members','value'=>function($model){
	$html='';
	$results=ClaimOrderItem::find()
	->select(['username','bill_image'])
	->innerJoin(Booking::tableName(),Booking::tableName().".id=".ClaimOrderItem::tableName().".main_booking_id")
	->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
	->innerJoin("user",User::tableName().".id=".Booking::tableName().".user_id")
	->where(['order_id'=>$model->id])
	->asArray()->all();
	if($results!=null){
		foreach($results as $result){
			$billImage='';
			if($result['bill_image']!=null && file_exists(Yii::$app->params['fuelbill_abs_path'].$result['bill_image'])){
				$billImage=Yii::$app->fileHelperFunctions->getImagePath('fuelbill',$result['bill_image'],'full');
			}
			$html.=($html!='' ? '<br />' : '').$result['username'].($billImage!='' ? ' <a href="'.$billImage.'" target="_blank" data-pjax="0" class="label label-primary">View bill</a>' : '');
		}
	}
	if($model->bill_image!='' && file_exists(Yii::$app->params['claimbill_uploads_abs_path'].$model->bill_image)){
		$billImage=Yii::$app->fileHelperFunctions->getImagePath('claimfuelbill',$model->bill_image,'full');
		$html.='<a href="'.$billImage.'" target="_blank" data-pjax="0" class="label label-primary">View bill</a>';
	}
	return $html;
}];
$columns[]=['format'=>'html','label'=>'Remarks','value'=>function($model){
	$html='';
	$comments=ClaimOrderComments::find()
	->select([
		'id',
		'comments',
		'user_type',
		'created_by',
	])
	->where([
		'claim_order_id'=>$model['id'],
		'trashed'=>0,
	])
	->asArray()
	->orderBy(['id'=>SORT_DESC])->all();
	if($comments!=null){

		foreach($comments as $comment){
			$createdByName='';
			$user=User::find()->where(['id'=>$comment['created_by']])->one();
			if($user!=null){
				$createdByName='<span class="label label-info">'.$user->fullname.':</span> ';
			}

			$html.=$createdByName.$comment['comments'].'<br />';
		}
	}
	return $html;
}];

$actionBtns='{add-comment}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('view-claim','claim')){
	$actionBtns.=' {view-claim}';
}

$columns[]=[
	'class' => 'yii\grid\ActionColumn',
	'header'=>'Actions',
	'headerOptions'=>['style'=>'width:180px'],
	'contentOptions'=>['class'=>'nosd'],
	'template' => $actionBtns,
	'buttons' => [
		'add-comment' => function ($url, $model) {
			return Html::a('<i class="glyphicon glyphicon-pencil"></i> Add Comment', 'javascript:;', [
				'title' => Yii::t('app', 'Add Comment'),
				'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-warning load-modal',
				'data-url'=>Url::to(['claim/add-order-comments','id'=>$model['id']]),
				'data-heading'=>'Update Booking Comments',
				'data-pjax'=>0,
			]);
		},
		'view-claim' => function ($url, $model) {
			return Html::a('<i class="glyphicon glyphicon-pencil"></i> View', $url, [
				'title' => Yii::t('app', 'View'),
				'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-primary',
				'data-pjax'=>0,
			]);
		},
	],
];
?>
<style>
.filters{display: none;}
</style>
<div class="tabs">
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link<?= $list == 'approved' ? ' active' : ''?>" href="<?= Url::to(['claimed','list'=>'approved'])?>">Approved</a>
		</li>
		<li class="nav-item">
			<a class="nav-link<?= $list == 'pending' ? ' active' : ''?>" href="<?= Url::to(['claimed','list'=>'pending'])?>">Pending</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active">
			<?php $form = ActiveForm::begin(['method'=>'get']); ?>
			<div class="row">
				<div class="col-sm-3"><?= $form->field($searchModel, 'date_range')->textInput(['placeholder'=>'Start Date','class'=>'form-control dtrpicker','autocomplete'=>'off'])->label(false) ?></div>
				<div class="col-sm-3"><?= $form->field($searchModel, 'port_id')->dropDownList(Yii::$app->appHelperFunctions->activeMarinaListArr,['prompt'=>'Select Marina'])->label(false) ?></div>
				<div class="col-sm-3"><?= $form->field($searchModel, 'user_id')->dropDownList(Yii::$app->appHelperFunctions->staffMembersArr,['prompt'=>'Select Staff'])->label(false) ?></div>
				<div class="col-sm-3"><button class="btn btn-primary" type="submit">Search</button></div>
			</div>
			<?php ActiveForm::end(); ?>

			<div class="table-responsive">
				<?php CustomPjax::begin(['id'=>'booking-container']); ?>
				<?php if($list == 'pending'){?>
				<div class="row">
					<div class="col-sm-6">
						<a class="btn btn-success btn-sm" onclick="javascript:approveBulk();"><?= Yii::t('app','Approve Selected')?></a>
					</div>
					<div class="col-sm-6 text-right">
						<strong>Total Selected: <span id="totalamt">AED 0</span></strong>
					</div>
				</div>
				<?php }?>
				<?= CustomGridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
					'cardHeader'=>false,
					'rowOptions' => function($model){
						return ['data-price'=>$model->total_amount];
					},
					'columns' => $columns,
					'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
				]); ?><br />
				<?php CustomPjax::end(); ?>
			</div>
		</div>
	</div>
</div>

<?= $this->render('js/claimed_scripts',['list'=>$list])?>
