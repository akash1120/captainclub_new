<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use app\models\BookingOperationComments;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Claim Detail');
if($model->status==1){
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Approved'), 'url' => ['claimed','list'=>'approved']];
}else{
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Approve'), 'url' => ['claimed','list'=>'pending']];
}
$this->params['breadcrumbs'][] = $this->title;

$billImage="";
if($model->bill_image!=''){
	if(filter_var($model->bill_image, FILTER_VALIDATE_URL)){
		$billImage=$model->bill_image;
	}else{
		$billImage=Yii::$app->fileHelperFunctions->getImagePath('claimfuelbill',$model->bill_image,'full');
	}
}
?>
<section class="discount-form card card-featured card-featured-warning">
	<header class="card-header">
		<h2 class="card-title"><?= $this->title.' - Ref#: '.$model->ref_no.' (AED '.$model->total_amount.')'?></h2>
		<small><?= Yii::$app->formatter->asDate($model->created_at)?></small>
		<?php if($billImage!=''){?>
			<div class="card-actions">
				<a href="<?= $billImage?>" target="_blank" class="btn btn-sm btn-primary"><?= Yii::t('app','View Bill')?></a>
			</div>
		<?php }?>
	</header>
	<div class="card-body">
		<div class="table-respo1nsive">
			<?php Pjax::begin(['id'=>'booking-container']); ?>
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'columns' => [
					['class' => 'yii\grid\SerialColumn','contentOptions'=>['style'=>'width: 20px;text-align:center;']],
					['format'=>'html','label'=>'Member','value' => function ($model){return ($model->booking!=null ? $model->booking->member->fullname : 'Not set');}],
					['format'=>'html','label'=>'Marina','value' => function ($model){return ($model->booking->marina!=null ? $model->booking->marina->name : 'Not set');}],
					['format'=>'html','label'=>'Boat','value' => function ($model){return ($model->booking->boat!=null ? $model->booking->boat->name : 'Not set');}],
					['format'=>'date','label'=>'Date','value' => function ($model){return $model->booking->booking_date;}],
					['format'=>'html','label'=>'Time','value' => function ($model){return $model->booking->timeZone->name.($model->booking->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');}],
					['attribute'=>'amount','value'=>function($model){
						return 'AED '.$model->amount;
					}],
					['format'=>'html','label'=>'Remarks','value'=>function($model){
						$html='';
						$comments=BookingOperationComments::find()
						->where([
							'process_type'=>0,
							'booking_id'=>$model->booking->id,
							'trashed'=>0,
						])
						->asArray()
						->orderBy(['id'=>SORT_DESC])->all();
						if($comments!=null){
							foreach($comments as $comment){
								$html.=$comment['comments'];
							}
						}
						return $html;
					}],
				],
				'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
			]); ?><br />
			<?php Pjax::end(); ?>
		</div>
	</div>
</section>
