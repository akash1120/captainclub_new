<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;
use yii\widgets\ActiveForm;
use app\models\BookingOperationComments;
use app\models\ClaimAmountForm;
use app\models\OperationUserCityMarina;
use app\models\Marina;
use app\models\BookingSearch;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$modelOpr=OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();

$this->title = Yii::t('app', 'Claim');
$this->params['breadcrumbs'][] = $this->title;

$isAdmin=false;
if(Yii::$app->user->identity->id==1)$isAdmin=true;
if($isAdmin==true){
	$marinas = Yii::$app->appHelperFunctions->activeMarinaList;
}else{
	$marinas = Marina::find()
	  ->select([
	    'id',
	    'city_id',
	    'name',
	  ])
	  ->where(['id'=>$modelOpr->marina_id,'trashed'=>0])
	  ->orderBy(['name'=>SORT_ASC])
	  ->asArray()
	  ->all();
}
?>
<style>
.filters{display: none;}
</style>
<div class="tabs">
	<ul class="nav nav-tabs">
		<?php
		if($marinas!=null){
			$n=1;
			foreach ($marinas as $marina) {
				?>
				<li class="nav-item">
					<a class="nav-link<?= $n==1 ? ' active' : ''?>" href="#tab_<?= $marina['id']?>" data-toggle="tab"><?= $marina['name']?></a>
				</li>
				<?php
				$n++;
			}
		}
		?>
	</ul>
	<div class="tab-content">
		<?php
		if($marinas!=null){
			$n=1;
			foreach ($marinas as $marina) {
				?>
				<div id="tab_<?= $marina['id']?>" class="tab-pane<?= $n==1 ? ' active' : ''?>">
					<?php
					if($marina['id']==Yii::$app->params['emp_id']){

						$searchModel = new BookingSearch();
						$searchModel->city_id = $marina['city_id'];
						$searchModel->port_id = $marina['id'];
						$dataProvider = $searchModel->searchForClaim(Yii::$app->request->queryParams);
						$dataProvider->pagination->pageSize=100;


						$boats=[];
						if($searchModel->city_id!=null && $searchModel->port_id!=null){
							$boats=Yii::$app->appHelperFunctions->getActiveBoatsListArr($searchModel->city_id,$searchModel->port_id);
						}

						?>
						<?php $form = ActiveForm::begin(['method'=>'get']); ?>
						<div class="row">
							<div class="col-sm-3"><?= $form->field($searchModel, 'booking_date')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off']) ?></div>
							<div class="col-sm-3"><?= $form->field($searchModel, 'boat_id')->dropDownList($boats,['prompt'=>'Select'])->label(Yii::t('app','Boat')) ?></div>
							<div class="col-sm-3"><?= $form->field($searchModel, 'booking_time_slot')->dropDownList(Yii::$app->appHelperFunctions->timeSlotListDashboardArr,['prompt'=>'Select'])->label(Yii::t('app','Time')) ?></div>
							<div class="col-sm-3"><button class="btn btn-primary" type="submit" style="margin-top:26px;">Search</button></div>
						</div>
						<?php ActiveForm::end(); ?>
						<hr />
						<?php CustomPjax::begin(['id'=>'booking-container']); ?>
						<div class="row">
							<div class="col-sm-6">
								<div class="input-group">
									<span class="input-group-prepend">
										<span class="input-group-text">
											Comments
										</span>
									</span>
									<input id="empCOComments" type="text" class="form-control" />
									<span class="input-group-append">
										<button class="btn btn-success" type="button" onclick="javascript:claimBulk();">
											<?= Yii::t('app','Claim Selected')?>
										</button>
									</span>
								</div>
							</div>
							<div class="col-sm-6 text-right">
								<strong>Total Selected: <span id="totalamt">AED 0</span></strong>
							</div>
						</div>
						<?= CustomGridView::widget([
							'dataProvider' => $dataProvider,
							'filterModel' => $searchModel,
							'rowOptions' => function($model){
								return ['data-price'=>$model->bookingActivity->claimAmount];
							},
							'columns' => [
								['class' => 'yii\grid\SerialColumn'],
								['class' => 'yii\grid\CheckboxColumn','checkboxOptions'=>function ($model, $key, $index, $column){
									return ['value' => $model->id, 'class'=>'cb'];
								},'contentOptions'=>['style'=>'width: 20px;text-align:center;','class'=>'nosd']],
								['format'=>'html','label'=>'Member','value' => function ($model){return ($model->member!=null ? $model->member->fullname : 'Not set');}],
								['format'=>'html','label'=>'Boat','value' => function ($model){return ($model->boat!=null ? $model->boat->name : 'Not set');}],
								['format'=>'date','label'=>'Date','value' => function ($model){return $model->booking_date;}],
								['format'=>'html','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');}],
								['label'=>'Petrol Consumed','value'=>function($model){return 'AED '.$model->bookingActivity->claimAmount;}],
								['format'=>'raw','label'=>'Remarks','value'=>function($model){
									$html='';
									$comments=BookingOperationComments::find()
									->where([
										'process_type'=>0,
										'booking_id'=>$model['id'],
										'trashed'=>0,
									])
									->asArray()
									->orderBy(['id'=>SORT_DESC])->all();
									if($comments!=null){
										foreach($comments as $comment){
											$html.=$comment['comments'];
										}
									}else{
										$html.='<a href="javascript:;" class="load-modal btn btn-xs btn-warning" data-heading="Update Booking Comments" data-url='.Url::to(['claim/add-comments','id'=>$model['id'],'process'=>0]).'>';
										$html.='	<i class="fa fa-plus"></i>';
										$html.='</a>';
									}
									return $html;
								},'contentOptions'=>['class'=>'nosd']]
							],
							'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
						]);
						?><br />
						<?php CustomPjax::end(); ?>
						<?= $this->render('js/claim_scripts',['searchModel'=>$searchModel])?>
						<?php
					}else{
						$amountToClaim=Yii::$app->appHelperFunctions->getUnClaimedMarinaAmount($marina['id']);

						$modelCA = new ClaimAmountForm;
						$modelCA->city_id=$marina['city_id'];
						$modelCA->port_id=$marina['id'];
						$this->registerJs('
						$("body").on("beforeSubmit", "form#claimAmount'.$marina['id'].'", function () {
							_targetContainer=$("#marinaClaimingContainer'.$marina['id'].'")
							App.blockUI({
								message: "'.Yii::t('app','Please wait...').'",
								target: _targetContainer,
								overlayColor: "none",
								cenrerY: true,
								boxed: true
							});

							var form = $(this);
							// return false if form still have some validation errors
							if (form.find(".has-error").length) {
								return false;
							}
							var formData = new FormData(form[0]);
							// submit form
							$.ajax({
								url: form.attr("action"),
								type: "post",
								data: formData,
								success: function (response) {
									response = jQuery.parseJSON(response);
									App.unblockUI($(_targetContainer));
									if(response["success"]){
										swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
									}else{
										swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
									}
									$.pjax.reload({container: "#marinaClaimingContainer'.$marina['id'].'", timeout: 2000});
								},
								cache: false,
								contentType: false,
								processData: false
							});
							return false;
						});
						');
						?>
						<?php CustomPjax::begin(['id'=>'marinaClaimingContainer'.$marina['id']]); ?>
						<div class="row">
							<div class="col-sm-4">
								<section class="card card-featured">
									<div class="card-body">
										<h2 class="card-title">Total unclaimed: <?= $amountToClaim?></h2>
									</div>
								</section>
							</div>
							<div class="col-sm-8">
								<section class="card card-featured">
									<?php $form = ActiveForm::begin(['id'=>'claimAmount'.$marina['id'], 'action'=>Url::to(['claim-amount']),'options'=>['enctype'=>'multipart/form-data']]); ?>
									<div class="hidden">
										<?= $form->field($modelCA, 'city_id')->textInput(['maxlength' => true])?>
										<?= $form->field($modelCA, 'port_id')->textInput(['maxlength' => true])?>
									</div>
									<div class="card-body">
										<?= $form->field($modelCA, 'comments')->textInput(['maxlength' => true])?>
										<?= $form->field($modelCA, 'bill_image',['template'=>'
										{label}
										<div class="input-group file-input">
										<div class="input-group-prepend">
										<span class="input-group-text"><img src="'.Yii::$app->fileHelperFunctions->getImagePath('','','').'" /></span>
										</div>
										<div class="custom-file">
										{input}
										<label class="custom-file-label" for="billImg'.$marina['id'].'">Choose file</label>
										</div>
										</div>
										{error}{hint}
										'])->fileInput(['id'=>'billImg'.$marina['id'],'class'=>'custom-file-input','accept'=>'image/*'])?>
										<?= $form->field($modelCA, 'amount',['template'=>'
										{label}
										<div class="input-group">
		                <span class="input-group-prepend">
		                <span class="input-group-text">
		                Amount
		                </span>
		                </span>
										{input}
										<span class="input-group-btn">
										<button class="btn btn-primary" type="submit">
										Claim
										</button>
										</span>
										</div>
										{hint}{error}
										'])->textInput(['maxlength' => true])?>
									</div>
									<?php ActiveForm::end(); ?>
								</section>
							</div>
						</div>
						<?php CustomPjax::end(); ?>
						<?php
					}
					?>
				</div>
				<?php
				$n++;
			}
		}
		?>
	</div>
</div>
