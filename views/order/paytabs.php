<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\PayTabs;
use app\models\UserSavedCc;
//use app\assets\PayTabsAsset;
//PayTabsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\PaymentForm */

$this->title = Yii::t('app', 'Confirm & Pay');
$this->params['breadcrumbs'][] = $this->title;

$finalAmout=Yii::$app->paytabsHelperFunctions->calculateFinalAmount($model->qty);

$this->registerJs('
  $("body").addClass("scroll-wrapper");
')
?>
<style>
.scroll-wrapper {
    -webkit-overflow-scrolling: touch;
      overflow-y: scroll;
      /* important:  dimensions or positioning here! */
}
</style>
<div class="row">
  <div class="col-xs-12 col-sm-3"></div>
  <div class="col-xs-12 col-sm-6">
    <section class="order-error card card-featured card-featured-warning">
    	<div class="card-body">
        <center><img src="images/email_logo.png" /></center>
        <table class="table table-striped">
          <tr>
            <th>Detail</th>
            <th width="100">Price</th>
          </tr>
          <tr>
            <td><?= $model->qty?> Credits</td>
            <td>AED <?= $model->qty?></td>
          </tr>
          <tr>
            <td>Service Charges</td>
            <td>AED <?= $finalAmout-$model->qty?></td>
          </tr>
          <tr>
            <td><strong>Total</strong></td>
            <td>AED <?= $finalAmout?></td>
          </tr>
          <?php if($model->use_saved_cc_id>0){?>
          <tr>
            <td></td>
            <td>
              <a class="btn btn-success" onclick="$(this).hide();$('.btn-processing').show()" href="<?= Url::to(['process-payment','id'=>$model->order_id])?>">Checkout</a>
              <a class="btn btn-success btn-processing" href="javascript:;" style="display:none;">Processing</a>
            </td>
          </tr>
          <?php }else{
            $paytabsOptions=Yii::$app->paytabsHelperFunctions->paytabOptions;
          ?>
          <tr>
            <td></td>
            <td><a href="javascript:;" onclick="Paytabs.openPaymentPage();" class="btn btn-success">Checkout</a>
            <!-- Button Code for PayTabs Express Checkout -->
            <script src="https://www.paytabs.com/express/v4/paytabs-express-checkout.js"
               id="paytabs-express-checkout"
               data-secret-key="<?= $paytabsOptions['secretKey']?>"
               data-merchant-id="<?= $paytabsOptions['merchentId']?>"
               data-url-redirect="<?= $paytabsOptions['returnUrl']?>"
               data-amount="<?= $model->amount_payable?>"
               data-currency="<?= $model->currency?>"
               data-title="<?= $model->member->fullname.'-'.$model->member->email ?>"
               data-product-names="<?= $model->detail?>"
               data-order-id="<?= $model->order_id?>"
               data-customer-phone-number="<?= $model->member->profileInfo->mobile?>"
               data-customer-email-address="<?= $paytabsOptions['testMode']==1 ? 'abc@accept.com' : $model->member->email?>"
               data-customer-country-code="971"

               data-billing-full-address="<?= $model->member->profileInfo->city->name.', United Arab Emirates'?>"
               data-billing-state="<?= $model->member->profileInfo->city->city_code?>"
               data-billing-city="<?= $model->member->profileInfo->city->name?>"
               data-billing-country="ARE"
               data-billing-postal-code="00000"
               data-ui-show-billing-address="false"
               data-language="en"
               data-is-tokenization="true"
               data-is-popup="true"
            /></td>
          </tr>
          <?php }?>
        </table>
      </div>
    </section>
  </div>
</div>
<div id="demo" style="height:520px;"></div>
