<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentForm */

$serviceFees=Yii::$app->appHelperFunctions->getSetting('paytab_servicefees_u');
$bankDetails=Yii::$app->appHelperFunctions->getSetting('bank_details');
?>
<div id="selection" class="selopts">
  <div class="row">
    <div class="col-xs-12 col-sm-12 text-center">
      <h3 style="margin-bottom:30px;">Please choose payment method</h3>
      <a class="btn btn-success" href="javascript:;" style="display:inline-block;margin-bottom:20px;" onclick="showPaymentOption('ccOption')">Credit Card ( 3% service charge applicable)</a><br />
      <a class="btn btn-success" href="javascript:;" style="display:inline-block;margin-bottom:20px;" onclick="showPaymentOption('bankOption')">Bank Transfer ( free of charge)</a>
    </div>
  </div>
</div>

<div id="ccOption" class="selopts" style="display:none;">
  <?php $form = ActiveForm::begin(['id'=>'depositeFundsForm']); ?>
  <?= $form->field($model, 'credits',['template'=>'
  {label}
  <div class="input-group">
    <span class="input-group-prepend">
      <span class="input-group-text">AED</span>
    </span>
    {input}
  </div>
  {hint}
  {error}
  '])->textInput()->hint('Minimum AED 100 can be deposited<br/>Kindly note '.$serviceFees.'% service charge is applicable') ?>
  <?php if(Yii::$app->user->identity->profileInfo->save_cc==0){?>
  <?= $form->field($model, 'save_info')->checkbox(); ?>
<?php }else{$alreadyCards=Yii::$app->user->identity->savedCards;?>
    <?php if($alreadyCards==null){?>
      <?= $form->field($model, 'save_info')->checkbox(); ?>
    <?php }else{
    //$model->use_already=0;
    $i=1;
      foreach($alreadyCards as $key=>$val){
        $pMethodOpts[$key]=$val;
        if($i==1){$model->use_already=$key;}
        $i++;
      }
      $pMethodOpts[0]=Yii::t('app','Use a new Card');
    ?>
    <?= $form->field($model, 'use_already')->radioList($pMethodOpts,[
      'item' => function($index, $label, $name, $checked, $value) {

          $return = '<label class="modal-radio" style="width:100%;">';
          $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.($checked==1 ? ' checked="checked"' : '').' tabindex="3">&nbsp;';
          $return .= '<span>' . ucwords($label) . '</span>';
          $return .= '</label>';

          return $return;
      }
    ])->label(Yii::t('app','Please choose one of options below')); ?>
    <?= '';//$form->field($model, 'use_already')->dropDownList(,['prompt'=>'Select Already saved card'])->hint("To use new card, you can ignore this selection") ?>
    <?php }?>
  <?php }?>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <?= Html::submitButton(Yii::t('app', 'Deposit'), ['class' => 'btn btn-success']) ?>
      <?= Html::a('Back', 'javascript:;', ['class' => 'btn btn-primary pull-right', 'onclick'=>"showPaymentOption('selection')"]) ?>
      <?= Html::a('Cancel', 'javascript:;', ['data-dismiss'=>'modal','class' => 'btn btn-default']) ?>
    </div>
  </div>
  <?php ActiveForm::end(); ?>

</div>

<div id="bankOption" class="selopts" style="display:none;">
  <?= $bankDetails?>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <?= Html::a('Back', 'javascript:;', ['class' => 'btn btn-primary pull-right', 'onclick'=>"showPaymentOption('selection')"]) ?>
      <?= Html::a('Noted', 'javascript:;', ['data-dismiss'=>'modal','class' => 'btn btn-success']) ?>
    </div>
  </div>
</div>
