<?php
use yii\helpers\Html;
//use app\assets\PayTabsAsset;
//PayTabsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\PaymentForm */

$this->title = Yii::t('app', 'Checkout Success');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="order-error card card-featured card-featured-warning">
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="alert alert-success text-center">
      Payment completed successfully
    </div>
  </div>
</section>
