<?php
$minCredit=Yii::$app->helperFunctions->minimumCredits;
$this->registerJs('
  $(document).delegate("#paymentform-credits", "blur", function() {
    if(parseInt($(this).val())<'.$minCredit.'){
      $(this).val('.$minCredit.');
    }
  });
  $("body").on("beforeSubmit", "form#depositeFundsForm", function () {
    _targetContainer=$("#depositFundsModal")
    App.blockUI({
      message: "'.Yii::t('app','Please wait...').'",
      target: _targetContainer,
      overlayColor: "none",
      cenrerY: true,
      boxed: true
    });

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find(".has-error").length) {
        return false;
     }
     // submit form
     $.ajax({
        url: form.attr("action"),
        type: "post",
        data: form.serialize(),
        success: function (response) {
          response = jQuery.parseJSON(response);
          if(response["success"]){
              window.location.href=response["success"]["url"];
          }else{
            swal({title: "'.Yii::t('app','Error').'", html: response["error"]["msg"], type: "error"});
          }
          App.unblockUI($(_targetContainer));
        }
     });
     return false;
  });
');
?>
<script>
function showPaymentOption(id)
{
  $(".selopts").hide();
  $("#"+id).show();
}
</script>
