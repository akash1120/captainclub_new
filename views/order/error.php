<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = Yii::t('app', 'Checkout Error');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="order-error card card-featured card-featured-warning">
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="alert alert-danger text-center">
      There was some error processing your payment, Please check with the managment.
    </div>
  </div>
</section>
