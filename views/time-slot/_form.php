<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TimeSlot */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="time-slot-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'timing')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'dashboard_rank')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
      </div>
    </div>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
