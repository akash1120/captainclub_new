<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DamageCheckItems */

$this->title = Yii::t('app', 'Update Damage Check Item: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Damage Check Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="damage-check-item-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
