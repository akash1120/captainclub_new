<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\DamageCheckItems;
use app\models\BookingActivityDamages;

/* @var $this yii\web\View */
/* @var $model app\models\Items */
/* @var $form yii\widgets\ActiveForm */
$items=Yii::$app->appHelperFunctions->damageCheckItems;

$this->registerJs('
$("body").on("click", ".cbItm", function () {
	if($(this).is(":checked")){
		$(this).parents("tr").find("input[type=\"text\"]").attr("disabled",false);
	}else{
		$(this).parents("tr").find("input[type=\"text\"]").val("").attr("disabled",true);
	}
});
');
?>
<div class="items-select-form">
	<table class="table">
		<tr>
			<th width="10"></th>
			<th>Items to check</th>
			<th>Departure</th>
			<th>Arrival</th>
		</tr>
		<?php
		if($items!=null){
			foreach($items as $item){
				$checked="";
				$item_condition="";
				$item_comments="";
				$io_damageRow=BookingActivityDamages::find()->where(['booking_id'=>$model->booking_id,'item_id'=>$item['id']])->asArray()->one();
				if($io_damageRow!=null){
					$checked=" checked=\"checked\"";
					$item_condition=$io_damageRow['item_condition'];
					$item_comments=$io_damageRow['item_comments'];
				}
				?>
				<tr>
					<td><input type="checkbox" id="dciCb<?= $item['id']?>" class="cbItm" name="BookingActivity[item_id][]" value="<?= $item['id']?>"<?= $checked?>></td>
					<td><label for="dciCb<?= $item['id']?>"><?= $item['title']?></label></td>
					<td><input type="text" id="com1_<?= $item['id']?>" name="BookingActivity[item_condition][]" class="form-control" value="<?= $item_condition?>"<?= $checked!="" ? '' : ' disabled="disabled"'?> /></td>
					<td><input type="text" id="com1_<?= $item['id']?>" name="BookingActivity[item_comments][]" class="form-control" value="<?= $item_comments?>"<?= $checked!="" ? '' : ' disabled="disabled"'?> /></td>
				</tr>
				<?php
			}
		}
		?>
	</table>
</div>
