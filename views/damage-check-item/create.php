<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DamageCheckItems */

$this->title = Yii::t('app', 'New Damage Check Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Damage Check Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="damage-check-item-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
