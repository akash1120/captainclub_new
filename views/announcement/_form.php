<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		startDate: "today",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
');
?>
<section class="announcement-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'heading')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'date')->textInput(['class' => 'form-control dtpicker', 'maxlength' => true])?>
      </div>
    </div>
    <?= $form->field($model, 'descp')->textArea(['rows' => 5])?>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'start_date')->textInput(['class' => 'form-control dtpicker', 'maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'end_date')->textInput(['class' => 'form-control dtpicker', 'maxlength' => true])?>
      </div>
    </div>
    <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
