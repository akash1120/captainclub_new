<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Announcement */

$this->title = Yii::t('app', 'New Announcement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Announcements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="announcement-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
