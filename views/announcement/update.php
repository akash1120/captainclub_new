<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */

$this->title = Yii::t('app', 'Update Announcement: {nameAttribute}', [
    'nameAttribute' => $model->heading,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Announcements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="announcement-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
