<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnnouncementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Announcements');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
$actionBtns.='{view}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
$this->registerJs('
  initDtpicker();
  $(document).on("pjax:success", function() {
		initDtpicker();
	});
');
?>
<div class="announcement-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      'heading',
      'date:date',
      'start_date:date',
      'end_date:date',
      ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){return Yii::$app->helperFunctions->arrPublishingIcon[$model['status']];},'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrPublishing],
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
        'contentOptions'=>['class'=>'noprint actions'],
        'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
        'buttons' => [
            'view' => function ($url, $model) {
              return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), 'javascript:;', [
                'title' => Yii::t('app', 'View'),
                'class'=>'dropdown-item text-1 btn-view',
                'data-pjax'=>"0",
                'data-heading'=>$model['heading'],
                'data-date'=>Yii::$app->formatter->asDate($model['date']),
                'data-descp'=>$model['descp'],
                'data-start_date'=>Yii::$app->formatter->asDate($model['start_date']),
                'data-end_date'=>Yii::$app->formatter->asDate($model['end_date']),
                'data-status'=>Yii::$app->helperFunctions->arrUserStatusIcon[$model['status']],
              ]);
            },
            'update' => function ($url, $model) {
              return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                'title' => Yii::t('app', 'Edit'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'status' => function ($url, $model) {
              if($model['status']==1){
                return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                  'title' => Yii::t('app', 'Disable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                  'data-method'=>"post",
                ]);
              }else{
                return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                  'title' => Yii::t('app', 'Enable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                  'data-method'=>"post",
                ]);
              }
            },
            'delete' => function ($url, $model) {
              return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                'title' => Yii::t('app', 'Delete'),
                'class'=>'dropdown-item text-1',
                'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                'data-method'=>"post",
                'data-pjax'=>"0",
              ]);
            },
        ],
      ],
    ],
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function showDetail(_t){
  html = '';
  html+= '<div class="row">';
  html+= '  <div class="col-xs-12 col-sm-12">';
  html+= '<table class="view-detail table table-striped table-bordered">';
  html+= '  <tr>';
  html+= '    <th width="150"><?= Yii::t('app','Heading:')?></th>';
  html+= '    <td>'+_t.data("heading")+'</td>';
  html+= '  </tr>';
  if(_t.data("date")!='' && _t.data("date")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Date:')?></th>';
  html+= '    <td>'+_t.data("date")+'</td>';
  html+= '  </tr>';
  }
  if(_t.data("descp")!='' && _t.data("descp")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Description:')?></th>';
  html+= '    <td>'+_t.data("descp")+'</td>';
  html+= '  </tr>';
  }
  if(_t.data("start_date")!='' && _t.data("start_date")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Start Date:')?></th>';
  html+= '    <td>'+_t.data("start_date")+'</td>';
  html+= '  </tr>';
  }
  if(_t.data("end_date")!='' && _t.data("end_date")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','End Date:')?></th>';
  html+= '    <td>'+_t.data("end_date")+'</td>';
  html+= '  </tr>';
  }
  if(_t.data("status")!='' && _t.data("status")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Status:')?></th>';
  html+= '    <td>'+_t.data("status")+'</td>';
  html+= '  </tr>';
  }
  html+= '</table>';

  html+= '  </div>';
  html+= '</div>';
  viewPopUpModal("<?= Yii::t('app','Announcement Detail')?>",html)
}
function initDtpicker()
{
	$("input[name=\"AnnouncementSearch[date]\"],input[name=\"AnnouncementSearch[start_date]\"],input[name=\"AnnouncementSearch[end_date]\"]").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		startDate: "today",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
}
</script>
