<?php
use yii\helpers\Html;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;

/* @var $this yii\web\View */
/* @var $model app\models\Newsletter */

$this->title = Yii::t('app', 'Newsletter Detail') . ': ' . $model->subject;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Newsletter'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Detail');
?>
<style>
.filters{display:none;}
</style>
<section class="card card-featured card-featured-warning">
  <header class="card-header">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <h2 class="card-title"><?= $model->subject?></h2>
      </div>
    </div>
  </header>
  <div class="card-body">
    <?= $model->email_from!='' ? '<strong>From:</strong> '.$model->email_from.'<br />' : ''?>
    <?= $model->ccAdminEmails!='' ? '<strong>Cc:</strong> '.$model->ccAdminEmails.'<br />' : ''?>
    <div><strong>Message:</strong><br /><?= $model->message?></div>
  </div>
  <div class="card-footer">
    <?php $attachmentInfo=$model->attachmentInfo;
    if($attachmentInfo!=null){?>
      <table class="table table-striped table-bordered">
        <tr>
          <th>Attachments</th>
        </tr>
        <?php foreach($attachmentInfo as $afile){?>
          <tr>
            <td width="100" align="left">
              <?= Html::a($afile['linkText'],$afile['link'],['target'=>'_blank']);?>
            </td>
          </tr>
        <?php }?>
      </table>
    <?php }?>
  </div>
</section>
<?php CustomPjax::begin(['id' => 'grid-container']) ?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],
    'username',
    'email:html',
    'opens',
    'clicks',
    'softBouncedStatus:raw',
    'hardBouncedStatus:raw',
  ],
  'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
]); ?>
<?php CustomPjax::end() ?>
