<?php
use yii\helpers\Html;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsletterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Newsletter');
$this->params['breadcrumbs'][] = $this->title;
$gridBtnsArr='';

//if(Yii::$app->controller->checkActionAllowed('view')){
$gridBtnsArr.=' {view}';
//}
?>
<style>
.filters{display:none;}
</style>
<div class="newsletter-index">
	<?php CustomPjax::begin(['id' => 'grid-container']) ?>
	<?= CustomGridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'subject',
			'sentStatus:html',
			'UniqueOpens',
			'opens',
			'uniqueClicks',
			'clicks',
			'softBounces',
			'hardBounces',
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Actions',
				'headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],
				'contentOptions'=>['class'=>'noprint'],
				'template' => $gridBtnsArr,
				'buttons' => [
					'view' => function ($url, $model) {
						return Html::a('<i class="fa fa-table"></i>', $url, [
							'title' => Yii::t('app', 'View'),
							'class'=>'btn btn-xs btn-info',
							'data-pjax'=>"0",
						]);
					},
				],
			],
		],
		'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
	]); ?>
	<?php CustomPjax::end() ?>
</div>
