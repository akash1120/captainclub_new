<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\Contract;
use app\models\ContractMember;
use app\assets\UserListAssets;
UserListAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Members');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.filters{display:none;}
.grid-view .card-header .btn{margin:0 5px;}
.mark-dead{display: none;}
</style>
<div class="user-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => false,
    'mailBtn' => false,
    'showPerPage' => true,
    'options'=>['class'=>'grid-view fixed-header1'],
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']],
      'id',
      ['format'=>'raw','attribute'=>'username','value'=>function($model){
        $html='';
        $html.='<div><a href="'.Url::to(['contract/index','id'=>$model['id']]).'" data-pjax="0" target="_blank">'.$model['email'].'</a></div>';
        $html.=$model['username']!='' ? '<div><span class="badge grid-badge badge-info">Old Username: '.$model['username'].'</span></div>' : '';
        $html.='<div>City: '.$model['city_name'].'</div>';
        $html.='<div>Package: '.$model['package_name'].'</div>';
        if($model['end_date']!='0000-00-00'){
          $html.='<div>End Date: '.$model['end_date'].'</div>';
        }
        $html.='<div>Status: '.Yii::$app->helperFunctions->getUserStatus($model['id'],$model['status'],$model).'</div>';
        return $html;
      },'headerOptions'=>['class'=>'noprint fxdBig'],'contentOptions'=>['class'=>'fxdBig']],
      ['format'=>'raw','value'=>function($model){
        $userModel=User::findOne($model['id']);
        $contractCount=ContractMember::find()->where(['user_id'=>$model['id']])->count('id');
        $html = 'Total Contract: '.$contractCount.'<br />';
        $html.= 'Active Contract ID: '.$userModel['active_contract_id'].'<br />';
        $subQueryMemberContracts=ContractMember::find()->select(['contract_id'])->where(['user_id'=>$model['id']]);
        $contracts=Contract::find()->where(['id'=>$subQueryMemberContracts])->asArray()->orderBy([Contract::tableName().'.id'=>SORT_ASC])->all();
        if($contracts!=null){
          $html.= '<table class="table table-bordered">';
          $html.= ' <tr>';
          $html.= '   <td>Contract ID</td>';
          $html.= '   <td>Start</td>';
          $html.= '   <td>End</td>';
          $html.= '   <td>Activated</td>';
          $html.= '   <td>Renewed Contract ID</td>';
          $html.= ' </tr>';
          foreach($contracts as $contract){
            $html.= '<tr>';
            $html.= ' <td>'.$contract['id'].'</td>';
            $html.= ' <td>'.$contract['start_date'].'</td>';
            $html.= ' <td>'.$contract['end_date'].'</td>';
            $html.= ' <td>'.$contract['is_activated'].'</td>';
            $html.= ' <td>'.$contract['renewed_contract_id'].'</td>';
            $html.= '</tr>';
          }
          $html.= '</table>';
        }
        return $html;
      }],
  ],
]);?>
<?php CustomPjax::end(); ?>
</div>
