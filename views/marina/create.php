<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marina */

$this->title = Yii::t('app', 'New Marina');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Marinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marina-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
