<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Marina */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Marinas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="marina-view card card-featured card-featured-warning mb-4">
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <?= DetailView::widget([
      'model' => $model,
      'attributes' => [
        'id',
        'name',
        'short_name',
        ['format'=>'raw','attribute'=>'status','value'=>Yii::$app->helperFunctions->arrPublishingIcon[$model['status']]],
        ['format'=>'raw','attribute'=>'created_at','value'=>Yii::$app->formatter->asDateTime($model['created_at']).($model->createdby!=null ? ' - '.$model->createdby : '')],
        ['format'=>'raw','attribute'=>'updated_at','value'=>Yii::$app->formatter->asDateTime($model['updated_at']).($model->updatedby!=null ? ' - '.$model->createdby : '')],
      ],
    ]) ?>
	</div>
  <div class="card-footer">
    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
          'method' => 'post',
        ],
    ]) ?>
	</div>
</section>
