<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Marina */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="marina-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <?= $form->field($model, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr) ?>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'short_name')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'captains')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'bbq')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'no_of_early_departures')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'no_of_late_arrivals')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'no_of_overnight_camps')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'night_drive_limit')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'wake_boarding_limit')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'wake_surfing_limit')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'fuel_profit')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
      </div>
    </div>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
