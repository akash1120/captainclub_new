<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\OperationMainAssets;
OperationMainAssets::register($this);

/* @var $this yii\web\View */

$this->title = 'Operations Dashboard';
?>

<div class="row db-mbtns">
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['operation-requests/service'])?>" title="Service">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-ship"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Service</h3>
        </div>
      </section>
    </a>
  </div>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['operation-requests/break-down'])?>" title="Break Down">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-file-alt"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Break Down</h3>
        </div>
      </section>
    </a>
  </div>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['operation-requests/inspepction'])?>" title="Inspection">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-file-invoice-dollar"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Inspection</h3>
        </div>
      </section>
    </a>
  </div>
  <?php if(Yii::$app->user->identity->user_type==1){?>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['report/booking'])?>" title="Booking">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-file-invoice-dollar"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Booking</h3>
        </div>
      </section>
    </a>
  </div>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['report/claim'])?>" title="Claim">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-file-invoice-dollar"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Claim</h3>
        </div>
      </section>
    </a>
  </div>
  <?php }?>
</div>
