<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\OperationMainAssets;
OperationMainAssets::register($this);

/* @var $this yii\web\View */

$this->title = 'Operations Dashboard';


$cities=Yii::$app->appHelperFunctions->cityListArr;
$marinas=Yii::$app->appHelperFunctions->getCityMarinaListArr($model->city_id);

$txtJScript=Yii::$app->jsFunctions->getCityMarinaArr($cities);

$this->registerJs($txtJScript.'
	$(document).delegate("#operationusercitymarina-city_id", "change", function() {
		array_list=cities[$(this).val()];
		$("#operationusercitymarina-marina_id").html("<option value=\"\">Marina</option>");
		$(array_list).each(function (i) {
			$("#operationusercitymarina-marina_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
		});
	});
');
?>
<?php if(Yii::$app->user->identity->id==1){?>
<section class="marina-form card card-featured card-featured-warning mb-2">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title">Select your City & Marina</h2>
	</header>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-4">
				<?= $form->field($model, 'city_id')->dropDownList($cities,['prompt'=>$model->getAttributeLabel('city_id')])->label(false) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($model, 'marina_id')->dropDownList($marinas,['prompt'=>$model->getAttributeLabel('marina_id')])->label(false) ?>
			</div>
			<div class="col-sm-4">
				<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-block btn-success']) ?>
			</div>
		</div>
  </div>
  <?php ActiveForm::end(); ?>
</section>
<?php }?>
<?php if($model!=null && $model->city_id!=null && $model->marina_id!=null){?>
<div class="row db-mbtns">
	<?php if(Yii::$app->menuHelperFunction->checkActionAllowed('activity','booking')){?>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['booking/activity'])?>" title="Activity">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-ship"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Activity</h3>
        </div>
      </section>
    </a>
  </div>
	<?php }?>
	<?php if(Yii::$app->menuHelperFunction->checkActionAllowed('unclosed-activity','booking')){?>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['booking/unclosed-activity'])?>" title="<?= Yii::t('app','Unclosed Activity')?>">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-ship"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center"><?= Yii::t('app','Unclosed Activity')?></h3>
        </div>
      </section>
    </a>
  </div>
	<?php }?>
  <div class="col-6 col-sm-4">
    <section class="card mb-4">
      <header class="card-header bg-white">
        <div class="card-header-icon bg-primary">
          <i class="fa fa-edit"></i>
        </div>
      </header>
      <div class="card-body">
        <h3 class="mt-0 font-weight-bold mb-0 text-center"><a href="javascript:;" class="load-modal" data-url="<?= Url::to(['request-service/create'])?>" data-heading="Request Service">Request Service</a></h3>
        <h3 class="mt-0 font-weight-bold mb-0 text-center"><a href="javascript:;" class="load-modal" data-url="<?= Url::to(['request-break-down/new'])?>" data-heading="Request Break Down">Request Break Down</a></h3>
        <h3 class="mt-0 font-weight-bold mb-0 text-center"><a href="javascript:;" class="load-modal" data-url="<?= Url::to(['request-inspection/new'])?>" data-heading="Request Inspection">Request Inspection</a></h3>
      </div>
    </section>
  </div>
	<?php if(Yii::$app->menuHelperFunction->checkActionAllowed('index','claim')){?>
	<div class="col-6 col-sm-4">
    <a href="<?= Url::to(['claim/index'])?>" title="Claim">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-file-invoice-dollar"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Claim</h3>
        </div>
      </section>
    </a>
  </div>
	<?php }?>
	<?php if(Yii::$app->menuHelperFunction->checkActionAllowed('claimed','claim')){?>
	<div class="col-6 col-sm-4">
    <a href="<?= Url::to(['claim/claimed'])?>" title="Claim">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-file-invoice-dollar"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Approve Claims</h3>
        </div>
      </section>
    </a>
  </div>
	<?php }?>
</div>
<?php }?>
<?= $this->render('/request-service/js/request_service_scripts')?>
<?= $this->render('/request-break-down/js/request_breakdown_scripts')?>
<?= $this->render('/request-inspection/js/request_inspection_scripts')?>
