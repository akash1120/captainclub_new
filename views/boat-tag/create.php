<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BoatTag */

$this->title = Yii::t('app', 'New BoatTag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Boat Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boat-tag-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
