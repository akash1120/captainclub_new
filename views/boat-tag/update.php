<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoatTag */

$this->title = Yii::t('app', 'Update Boat Tag: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Boat Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="boat-tag-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
