<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\InspectionItems;

/* @var $this yii\web\View */
/* @var $model app\models\RequestService */
/* @var $form yii\widgets\ActiveForm */

$items=Yii::$app->appHelperFunctions->inspectionItems;

$this->registerJs('
$(".dtpicker").datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
}).on("changeDate", function(e){
  $(this).datepicker("hide");
});
')
?>
<div class="request-service-form">
    <?php $form = ActiveForm::begin(['id'=>'reqInspectionForm']); ?>
    <div class="row">
    	<div class="col-sm-6">
			<?= $form->field($model, 'boat_id')->dropDownList(Yii::$app->appHelperFunctions->getBoatsListArr(),['prompt'=>Yii::t('app','Select'),'tabindex'=>'1']) ?>
        </div>
    	<div class="col-sm-6">
        	<?= $form->field($model, 'inspection_date')->textInput(['maxlength' => true, 'class' => 'form-control dtpicker']) ?>
        </div>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="10"></th>
                <th>Items to check</th>
                <th>Conditions & Availability</th>
                <th>Remarks & Inquiries</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($items!=null){
                foreach($items as $item){
            ?>
            <tr>
                <td><input type="checkbox" id="iiCb<?= $item['id']?>" class="cbItm" name="RequestInspection[item_id][]" value="<?= $item['id']?>"></td>
                <td><label for="iiCb<?= $item['id']?>"><?= $item['title']?></label></td>
                <td><input type="text" id="com1_<?= $item['id']?>" name="RequestInspection[item_condition][]" class="form-control" value="" disabled="disabled" /></td>
                <td><input type="text" id="com1_<?= $item['id']?>" name="RequestInspection[item_comments][]" class="form-control" value="" disabled="disabled" /></td>
            </tr>
            <?php
                }
            }
            ?>
        </tbody>
    </table>
	<?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>
