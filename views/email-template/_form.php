<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JqueryAsset;

/* @var $this yii\web\View */
/* @var $model app\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */

//$this->registerCssFile('@web/'.'plugins/wysihtml5/bootstrap3-wysihtml5.min.css', ['depends' => [JqueryAsset::className()]]);
//$this->registerJsFile('@web/'.'plugins/wysihtml5/bootstrap3-wysihtml5.all.min.js', ['depends' => [JqueryAsset::className()]]);
$this->registerJsFile('@web/'.'plugins/tinymce/tinymce.min.js', ['depends' => [JqueryAsset::className()]]);

$this->registerJs('
tinymce.init({
	selector:".textarea",
	menubar:false,
	paste_as_text: true,
	plugins: [
		"advlist autolink lists link image charmap print preview anchor textcolor",
		"searchreplace visualblocks code fullscreen",
		"insertdatetime media table paste"
	],
	toolbar: "insertfile undo redo | paste | bold italic | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
});
/*$(".textarea").wysihtml5({
	//"stylesheets": ["plugins/wysihtml5/wysiwyg-color.css"],
	//"color": true,
	"font-styles": true,
	"html": true,
});*/
');
?>
<section class="email-template-form card card-featured card-featured-warning">
	<?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-12">
				<?= $form->field($model, 'title')->textInput() ?>
				<div class="alert alert-info">
					<strong>You can use following Shortcodes</strong><br />
					Please note {password} is available only in new user creation. Password is stored in encrypted form and can not be decrypted.<br />
					<?php
					$n=1;
					foreach(Yii::$app->helperFunctions->templateShortCodes as $key=>$val)
					{
						if($n>1){
							echo ', ';
						}
						echo $val;
						$n++;
					}
					?>
				</div>
				<?= $form->field($model, 'template_html')->textarea(['class'=>'form-control textarea', 'rows'=>8]) ?>
				<?= $form->field($model, 'template_text')->textarea(['rows'=>8]) ?>
			</div>
		</div>
	</div>
	<div class="card-footer">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
		<?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</section>
