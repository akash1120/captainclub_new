<?php
use yii\helpers\Html;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Email Templates');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
$actionBtns.='{view}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
?>
<div class="email-teplate-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      'title',
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
        'contentOptions'=>['class'=>'noprint actions'],
        'template' => '
        <div class="btn-group flex-wrap">
        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        </button>
        <div class="dropdown-menu" role="menu">
        '.$actionBtns.'
        </div>
        </div>',
        'buttons' => [
          'view' => function ($url, $model) {
            return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), 'javascript:;', [
              'title' => Yii::t('app', 'View'),
              'class'=>'dropdown-item text-1 btn-view',
              'data-pjax'=>"0",
              'data-title'=>$model['title'],
              'data-template_html'=>nl2br($model['template_html']),
              'data-template_text'=>nl2br($model['template_text']),
            ]);
          },
          'update' => function ($url, $model) {
            return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
              'title' => Yii::t('app', 'Edit'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
            ]);
          },
        ],
      ],
    ],
  ]); ?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function showDetail(_t){
  html = '';
  html+= '<div class="row">';
  html+= '  <div class="col-xs-12 col-sm-12">';
  html+= '<table class="view-detail table table-striped table-bordered">';
  html+= '  <tr>';
  html+= '    <th width="150"><?= Yii::t('app','Title:')?></th>';
  html+= '    <td>'+_t.data("title")+'</td>';
  html+= '  </tr>';
  if(_t.data("template_html")!='' && _t.data("template_html")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Html:')?></th>';
  html+= '    <td>'+_t.data("template_html")+'</td>';
  html+= '  </tr>';
  }
  if(_t.data("template_text")!='' && _t.data("template_text")!=undefined){
  html+= '  <tr>';
  html+= '    <th><?= Yii::t('app','Text:')?></th>';
  html+= '    <td>'+_t.data("template_text")+'</td>';
  html+= '  </tr>';
  }
  html+= '</table>';

  html+= '  </div>';
  html+= '</div>';
  viewPopUpModal("<?= Yii::t('app','Email Teplate Detail')?>",html)
}
</script>
