<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;

$this->title = Yii::t('app','Members Behaviour');

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['label'=>'Member','value'=>function($model){return $model->member->fullName;}];
$columns[]=['label'=>'DOJ','value'=>function($model){return $model->member->profileInfo->joining_date;},'format'=>'date'];
$columns[]=['label'=>'History total','value'=>function($modelRow) use ($model){
	$html = '<a href="javascript:;" class="load-modal" data-heading="Booking History - '.$modelRow->member->fullName.'" data-url="'.Url::to(['suggestion/user-booking-history','user_id'=>$modelRow->user_id,'status_type'=>$model->status_type,'status'=>$model->status]).'">';
	$html.= Yii::$app->statsFunctions->getUserAllOldBookingCount($modelRow->user_id);
	$html.= '</a>';
	return $html;
},'format'=>'raw'];
if($model->status_type=='be'){
	$columns[]=['label'=>'Original Bt','value'=>function($model){
		if($model->swapInfo!=null){
			return $model->swapInfo->oldBoat->name;
		}else{
			if($model->bulkBookingSwapInfo!=null){
				return $model->bulkBookingSwapInfo->adminBooking->boat->name;
			}else{
				return $model->boat->name;
			}
		}
	}];
	$columns[]=['label'=>'Replacement BT','value'=>function($model){
		$boatProvided='';
		if($model->swapInfo!=null){
			$boatProvided=$model->swapInfo->newBoat->name;
		}else{
			if($model->bulkBookingSwapInfo!=null){
				$boatProvided=$model->boat->name;
			}else{
				if($model->bookingActivity!=null){
					if($model->bookingActivity->boatProvided!=null){
						$boatProvided=$model->bookingActivity->boatProvided->name;
					}else{
						$boatProvided='No Boat Info found';
					}
				}else{
					$boatProvided='No In/Out Info Found';
				}
			}
		}
		return $boatProvided;
	}];
}else{
	$columns[]=['label'=>'Boat','value'=>function($model){return $model->boat->name;}];
}
$columns[]=['label'=>'Frequency','value'=>function($modelRow) use ($model){return Yii::$app->statsFunctions->memberHistoryTotal($modelRow,$model);}];
if($model->status_type=='be'){
	$columns[]=['label'=>'Date of Booking','value'=>function($model){return $model->booking_date;},'format'=>'date'];
	$columns[]=['label'=>'Replacmenet Date','value'=>function($model){
		if($model->swapInfo!=null){
			return $model->swapInfo->created_at;
		}else{
			if($model->bulkBookingSwapInfo!=null){
				return $model->bulkBookingSwapInfo->created_at;
			}else{
				return $model->booking_date;
			}
		}
	},'format'=>'date'];
	$columns[]=['label'=>'Remarks','value'=>function($model){
		return $model->bookingActivity!=null ? $model->bookingActivity->overall_remarks : '';
	}];
}else{
	//Status type remarks...
	$columns[]=['label'=>'Remarks','value'=>function($modelRow) use ($model){
		$html = '';
		if($modelRow->bookingActivity!=null){
			if($model->status_type=='te'){
				$html.= '<div>'.$modelRow->bookingActivity->trip_exp_comments.'</div>';
			}
			if($model->status_type=='mb'){
				if($model->status==1){
					$html.= '<div>'.$modelRow->bookingActivity->memberExperienceComments.'</div>';
				}
				if($model->status==2){
					$html.= '<div>'.$modelRow->bookingActivity->dui_comments.'</div>';
				}

			}
			$html.='<div>'.$modelRow->bookingActivity->overall_remarks.'</div>';
		}
		return $html;
	},'format'=>'raw'];
}
?>
<?= $this->render('header',['model'=>$model,'date'=>$model->date])?>
<style>
.filters{display: none;}
</style>
<div class="tabs">
	<ul class="nav nav-tabs">
    <li class="nav-item<?= $model->status_type=='be' && $model->status==2 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'be','status'=>2])?>" data-pjax="0"><?= Yii::t('app','Original NA')?> (<?= $model->getTotalCount('be',2)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='te' && $model->status==2 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'te','status'=>2])?>" data-pjax="0"><?= Yii::t('app','Technical')?> (<?= $model->getTotalCount('te',2)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='te' && $model->status==6 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'te','status'=>6])?>" data-pjax="0"><?= Yii::t('app','Incident')?> (<?= $model->getTotalCount('te',6)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='te' && $model->status==5 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'te','status'=>5])?>" data-pjax="0"><?= Yii::t('app','Others')?> (<?= $model->getTotalCount('te',5)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='te' && $model->status==4 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'te','status'=>4])?>" data-pjax="0"><?= Yii::t('app','Accident')?> (<?= $model->getTotalCount('te',4)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='te' && $model->status==3 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'te','status'=>3])?>" data-pjax="0"><?= Yii::t('app','Stuck')?> (<?= $model->getTotalCount('te',3)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='bs' && $model->status==3 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'bs','status'=>3])?>" data-pjax="0"><?= Yii::t('app','No Show')?> (<?= $model->getTotalCount('bs',3)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='bs' && $model->status==7 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'bs','status'=>7])?>" data-pjax="0"><?= Yii::t('app','Same Day Cancel')?> (<?= $model->getTotalCount('bs',7)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='mb' && $model->status==1 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'mb','status'=>1])?>" data-pjax="0"><?= Yii::t('app','Late Arrival')?> (<?= $model->getTotalCount('mb',1)?>)</a>
    </li>
    <li class="nav-item<?= $model->status_type=='mb' && $model->status==2 ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/member-behaviour','date'=>$model->date,'status_type'=>'mb','status'=>2])?>" data-pjax="0"><?= Yii::t('app','DUI')?> (<?= $model->getTotalCount('mb',2)?>)</a>
    </li>
  </ul>
	<div class="tab-content">
    <div class="tab-pane active">
			<?= CustomGridView::widget([
			  'dataProvider' => $dataProvider,
			  'filterModel' => $model,
			  'cardHeader' => false,
			  'columns' => $columns,
			]); ?>
		</div>
	</div>
</div>
<?= $this->render('footer')?>
