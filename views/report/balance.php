<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
//use app\models\Ports;
use app\models\UserAccounts;
use app\models\Order;
use app\assets\ReportAsset;
ReportAsset::register($this);

$this->title = 'Balance Report';

$columns[]=['label'=>'Date','value'=>function($model){return $model->created_at;},'format'=>'datetime'];
$columns[]=['format'=>'html','label'=>'Member','value'=>function($model){
	$html = '';
	$html.=$model->member!=null ? $model->member->username : '';
	if($model->account_id==0){
		if($model->order_id>0){
			$order=Order::find()->select(['transaction_id'])->where(['id'=>$model->order_id])->asArray()->one();
			if($order!=null){
				$html.='<br /><span><small>Paytabs: '.$order['transaction_id'].'</small></span>';
			}
		}else{
			$html.= '<br /><span><small>Manual</small></span>';
		}
	}
	return $html;
}];
$columns[]=['label'=>'Balance','value'=>function($model){
	$value='';
	if($model->account_id==0){
		$value = $model->amount;
	}else{
		$value = $model->amount;
		if($model->trans_type=='dr'){
			$value = '-'.$value;
		}
	}
	return $value;
	$sumOfCr=UserAccounts::find()
	->where(['and',['trans_type'=>'cr'],['<=',UserAccounts::tableName().'.id',$model->id]])
	->sum('amount');
	$sumOfCr=$sumOfCr>0 ? $sumOfCr : 0;
	$sumOfDr=UserAccounts::find()
	->where(['and',['trans_type'=>'dr'],['<=',UserAccounts::tableName().'.id',$model->id]])
	->sum('amount');
	$sumOfDr=$sumOfDr>0 ? $sumOfDr : 0;
	return $sumOfCr-$sumOfDr;
},'header'=>'Balance'.$searchModel->unConsumedOB,'footer'=>$searchModel->unConsumedTotal];
$columns[]=['label'=>'Charges','value'=>function($model){
	$value='';
	if($model->account_id==0 && $model->order_id>0){
		$value = $model->order->amount_payable-$model->amount-$model->order->profit;
	}
	return $value;
},'header'=>'Charges'.$searchModel->chargesOB,'footer'=>$searchModel->chargesTotal];
$columns[]=['label'=>'Income','value'=>function($model){
	$value='';
	if($model->account_id==0 && $model->order_id>0){
		$value = $model->profit;
	}
	return $value;
},'header'=>'Income'.$searchModel->incomeOB,'footer'=>$searchModel->incomeTotal];

$ports=Yii::$app->appHelperFunctions->marinaList;//Ports::find()->select(['id','name','short_name'])->where(['status'=>1,'trashed'=>0])->asArray()->all();
if($ports!=null){
	foreach($ports as $port){
		$columns[]=['label'=>$port['short_name'].' Fuel','value'=>function($model) use ($port){
			$value='';
			if($model->account_id==1 && $model->booking_id>0){
				if($model->booking!=null && $model->booking->port_id==$port['id']){
					$value=$model->amount-$model->profit;
				}
			}
			return $value;
		},'header'=>$port['short_name'].' Fuel'.$searchModel->marinaFuelOB($port['id']),'footer'=>$searchModel->marinaFuelTotal($port['id'])];
	}
}

$columns[]=['label'=>'Fuel Profit','value'=>function($model){
	$value='';
	if($model->account_id==1 && $model->booking_id>0){
		$value = $model->profit;
	}
	return $value;
},'header'=>'Fuel Profit'.$searchModel->fuelProfitOB,'footer'=>$searchModel->fuelProfitTotal];
$columns[]=['label'=>'Captain','value'=>function($model){
	$value='';
	if($model->account_id==2 && $model->booking_id>0){
		$value = $model->amount;
	}
	return $value;
},'header'=>'Captain'.$searchModel->captainOB,'footer'=>$searchModel->captainTotal];
$columns[]=['label'=>'BBQ','value'=>function($model){
	$value='';
	if($model->account_id==3 && $model->booking_id>0){
		$value = $model->amount;
	}
	return $value;
},'header'=>'BBQ'.$searchModel->bbqOB,'footer'=>$searchModel->bbqTotal];
$columns[]=['label'=>'Other','value'=>function($model){
	$value='';
	if($model->account_id==10){
		$value = $model->amount;
	}
	return $value;
},'header'=>'Other'.$searchModel->otherOB,'footer'=>$searchModel->otherTotal];
$columns[]=['header'=>'Paytabs','value'=>function($model){
	$value='';

	if($model->account_id==11){
		$value=-$model->amount;
	}else{
		if($model->order_id>0){
			$value=$model->amount+$model->profit;
		}
	}
	return $value;
},'header'=>'Paytabs '.(Yii::$app->menuHelperFunction->checkActionAllowed('paytabs-withdraw','report') ? '<a href="javascript:;" data-url="'.Url::to(['report/paytabs-withdraw']).'" data-heading="Paytabs Withdrawal" class="btn btn-xs btn-danger load-modal"><i class="fa fa-minus"></i></a>' : '').$searchModel->paytabsOB,'footer'=>$searchModel->paytabsTotal];
?>
<div class="report-balance">
	<?php CustomPjax::begin(['id'=>'grid-container']); ?>
	<div class="row stats-row">
		<div class="col-xs-12 col-sm-4">
			<section class="card card-featured-left card-featured-primary mb-2">
				<div class="card-body">
					<div class="widget-summary widget-summary-sm">
						<div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon bg-primary">
								<i class="fas fa-money-bill-alt"></i>
							</div>
						</div>
						<div class="widget-summary-col">
							<div class="summary">
								<h4 class="title">Deposits</h4>
								<div class="info">
									<strong class="amount"><?= $searchModel->totalDeposits?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-xs-12 col-sm-4">
			<section class="card card-featured-left card-featured-primary mb-2">
				<div class="card-body">
					<div class="widget-summary widget-summary-sm">
						<div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon bg-primary">
								<i class="fas fa-hand-holding-usd"></i>
							</div>
						</div>
						<div class="widget-summary-col">
							<div class="summary">
								<h4 class="title">Manual</h4>
								<div class="info">
									<strong class="amount"><?= $searchModel->totalManual?></strong>
									<?php if(Yii::$app->menuHelperFunction->checkActionAllowed('manual-deposits')){?>
										<a href="<?= Url::to(['manual-deposits'])?>" class="text-primary" data-pjax="0">(View report)</a>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-xs-12 col-sm-4">
			<section class="card card-featured-left card-featured-primary mb-2">
				<div class="card-body">
					<div class="widget-summary widget-summary-sm">
						<div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon bg-primary">
								<i class="fas fa-balance-scale"></i>
							</div>
						</div>
						<div class="widget-summary-col">
							<div class="summary">
								<h4 class="title">Opening Balance</h4>
								<div class="info">
									<strong class="amount"><?= $searchModel->openingBalance?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<section class="card mb-1">
		<div class="card-body filter-padding">
			<?php $form = ActiveForm::begin(['action'=>Url::to(['report/balance']),'method'=>'get','options'=>['data-pjax' => '']]); ?>
			<div class="row top-filter-row">
				<div class="col-xs-12 col-sm-3">
					<?= $form->field($searchModel, 'date_range')->textInput(['autocomplete'=>'off','placeholder'=>'Date Range'])->label(false) ?>
				</div>
				<div class="col-xs-12 col-sm-3">
					<?= $form->field($searchModel, 'paytab_code')->textInput(['autocomplete'=>'off','placeholder'=>'Paytabs Code'])->label(false) ?>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="hidden">
						<?= $form->field($searchModel, 'user_id')->textInput()->label(false) ?>
					</div>
					<?= $form->field($searchModel, 'username')->textInput(['autocomplete'=>'off','placeholder'=>'Username'])->label(false) ?>
				</div>
				<div class="col-xs-12 col-sm-3">
					<div class="form-group">
						<?= Html::submitButton('<i class="fas fa-search"></i> '.Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
					</div>
				</div>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</section>
	<?= CustomGridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'tableOptions' => ['id'=>'fixTable', 'class'=>'table table-striped table-bordered'],
		'showFooter' => true,
    'showPerPage' => true,
		'columns' => $columns,
		'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
	]); ?>
	<?php CustomPjax::end(); ?>

	<section class="card card-featured" style="margin-top:20px;">
		<header class="card-header">
			<h2 class="card-title">Grand Total</h2>
		</header>
		<div class="card-body">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Un Consumed</th>
						<th>Charges</th>
						<th>Income</th>
						<?php
						if($ports!=null){
							foreach($ports as $port){
								echo '<th>'.$port['short_name'].' Fuel</th>';
							}
						}
						?>
						<th>Fuel Profit</th>
						<th>Captain</th>
						<th>BBQ</th>
						<th>Other</th>
						<th>Paytabs</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?= Yii::$app->statsFunctions->totalUnConsumed?></td>
						<td><?= Yii::$app->statsFunctions->totalCharges?></td>
						<td><?= Yii::$app->statsFunctions->totalIncome?></td>
						<?php
						if($ports!=null){
							foreach($ports as $port){
								echo '<td>'.Yii::$app->statsFunctions->marinaFuel($port['id']).'</td>';
							}
						}
						?>
						<td><?= Yii::$app->statsFunctions->totalFuelProfit?></td>
						<td><?= Yii::$app->statsFunctions->totalCaptin?></td>
						<td><?= Yii::$app->statsFunctions->totalBBQ?></td>
						<td><?= Yii::$app->statsFunctions->totalOther?></td>
						<td><?= Yii::$app->statsFunctions->totalPaytabs?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>
</div>
<?= $this->render('js/scripts')?>
<?= $this->render('js/withdraw_scripts')?>
