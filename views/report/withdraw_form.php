<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(['id'=>'withdraw-form']); ?>
<?= $form->field($model, 'credits')->textInput() ?>
<?= $form->field($model, 'descp')->textInput() ?>
<div class="form-group">
	<?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
	<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
</div>
<?php ActiveForm::end(); ?>
