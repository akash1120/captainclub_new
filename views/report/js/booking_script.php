<?php
use yii\helpers\Html;
use yii\helpers\Url;

$cities=Yii::$app->appHelperFunctions->cityListArr;
$txtJScript=Yii::$app->jsFunctions->getCityMarinaArr($cities);

$this->registerJs($txtJScript.'
	initScripts();
	$(document).on("pjax:success", function() {
	  initScripts();
	});
	$(document).delegate("#bookingsearch-city_id", "change", function() {
		$("#bookingsearch-port_id").html("<option value=\"\">Marina</option>");
		array_list=cities[$(this).val()];
		$(array_list).each(function (i) {
			$("#bookingsearch-port_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
		});
	});
')
?>
<script>
function initScripts(){
	$(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
	$(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
		$(this).trigger("change");
	});
	$(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});
}
</script>
