<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
	initScripts();
	$(document).on("pjax:success", function() {
	  initScripts();
	});
')
?>
<script>
function initScripts(){
	$("#reportsearch-date_range").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
	$("#reportsearch-date_range").on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
		$(this).trigger("change");
	});
	$("#reportsearch-date_range").on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});
	$("#reportsearch-username").autocomplete({
    serviceUrl: "<?= Url::to(['suggestion/member-search'])?>",
		noCache: true,
    onSelect: function(suggestion) {
			$("#reportsearch-user_id").val(suggestion.data);
			$("#reportsearch-username").val(suggestion.value);
    },
    onInvalidateSelection: function(suggestion) {
			$("#reportsearch-user_id").val("");
    },
  });
}
</script>
