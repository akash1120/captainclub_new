<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\ReportFormAssets;
ReportFormAssets::register($this);

/* @var $this yii\web\View */
$controller=Yii::$app->controller->id;
$action=Yii::$app->controller->action->id;

$this->registerJs('
initDtrpicker();
$(document).on("pjax:success", function() {
  initDtrpicker();
});
');
?>
<script>
function initDtrpicker(){
	$(".dtrpicker").daterangepicker({
    maxDate: new Date(),
    autoUpdateInput: false,
    locale: {
      format: 'YYYY-MM-DD',
      cancelLabel: '<?= Yii::t('app','Clear')?>'
    },
  });
	$(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
		$(this).trigger("change");
	});
	$(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('<?= $model->date?>');
	});
}
</script>
<div class="tabs">
	<ul class="nav nav-tabs nav-jus1tified">
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('booking-stats','report')){?>
    <li class="nav-item<?= $action=='booking-stats' ? ' active' : ''?>">
      <a class="nav-link" href="<?= Url::to(['report/booking-stats'])?>" data-pjax="0"><?= Yii::t('app','Booking Stats')?></a>
    </li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('member-behaviour','report')){?>
		<li class="nav-item<?= $action=='member-behaviour' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['report/member-behaviour'])?>" data-pjax="0"><?= Yii::t('app','Members Behaviour')?></a>
		</li>
    <?php }?>
  </ul>
	<div class="tab-content">
    <div class="tab-pane active">
      <div class="row">
        <div class="col-sm-4">
          <?= Html::beginForm([$controller.'/'.$action], 'get', ['data-pjax' => '', 'id'=>'reportForm', 'class' => 'form-inline']); ?>
          <div class="input-group mb-3" style="width:100%;">
          	<span class="input-group-prepend">
          		<span class="input-group-text">Date Range</span>
          	</span>
          	<?= Html::input('text', 'date', $model->date, ['class' => 'form-control dtrpicker','autocomplete'=>'off', 'onchange'=>'document.getElementById(\'btnsrch\').click();']) ?>
          </div>
          <div class="hidden">
            <button id="btnsrch" type="submit">Go</button>
          </div>
          <?= Html::endForm() ?>
        </div>
        <div class="col-sm-8 text-right">
          <?= Yii::$app->formatter->asDate($model->start_date).' to '.Yii::$app->formatter->asDate($model->end_date)?>
        </div>
      </div>
