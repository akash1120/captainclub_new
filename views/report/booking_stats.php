<?php
$this->title = Yii::t('app','Booking Stats');

$marinaList=Yii::$app->appHelperFunctions->marinaList;
$marinaCount=count($marinaList);
?>
<?= $this->render('header',['model'=>$model,'date'=>$model->date])?>
<style>
.rdivider{background-color:#cecece;}
</style>
<table class="table table-bordered table-hover table-striped">
  <tr>
    <th></th>
    <?php foreach($marinaList as $marina){?>
    <th><?= $marina['short_name']?></th>
    <?php }?>
  </tr>
  <tr>
    <th class="rdivider" colspan="<?= $marinaCount+1?>">Bookings</th>
  </tr>
  <tr>
    <th>Total</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaTotalBookings($marina['id'],$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Smooth</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaTotalBookingsByBookingExp($marina['id'],1,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Request</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaTotalBookingsByBookingExp($marina['id'],3,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Orignal NA</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaTotalBookingsByBookingExp($marina['id'],2,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Unclosed</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaUnClosedBookings($marina['id'],$model->date)?></td>
    <?php }?>
  </tr>


  <tr>
    <th class="rdivider" colspan="<?= $marinaCount+1?>">Trip</th>
  </tr>
  <tr>
    <th>Smooth</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingTripStats($marina['id'],1,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Technical</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingTripStats($marina['id'],2,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Stuck</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingTripStats($marina['id'],3,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Accident</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingTripStats($marina['id'],4,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Incident</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingTripStats($marina['id'],6,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Others</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingTripStats($marina['id'],5,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>No show</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingStatusStats($marina['id'],3,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Same day cancel</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingStatusStats($marina['id'],7,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>TCC canceled weather</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingStatusStats($marina['id'],4,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>TCC canceled NA</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingStatusStats($marina['id'],5,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Weather warning cancel</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingStatusStats($marina['id'],8,$model->date)?></td>
    <?php }?>
  </tr>
  <tr>
    <th>Stay In</th>
    <?php foreach($marinaList as $marina){?>
    <td><?= Yii::$app->statsFunctions->getMarinaBookingStatusStats($marina['id'],2,$model->date)?></td>
    <?php }?>
  </tr>
</table>
<?= $this->render('footer')?>
