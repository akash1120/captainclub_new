<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;
use yii\widgets\ActiveForm;
use app\models\Timeslots;
use app\models\Boats;
use app\models\BookingOperationComments;
use app\models\ClaimOrderComments;
use app\models\BookingInOut;
use app\models\User;
use app\models\RemoteUser;
use app\assets\AppDateRangePickerAsset;
AppDateRangePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Claimed Report');
$this->params['breadcrumbs'][] = $this->title;


$boats=[];
if($searchModel->port_id!=null){
	$marina=Yii::$app->appHelperFunctions->getMarinaById($searchModel->port_id);
	$boats=Yii::$app->appHelperFunctions->getActiveBoatsListArr($marina['city_id'],$searchModel->port_id);
}
$marinas = Yii::$app->appHelperFunctions->activeMarinaList;

$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
');
?>

<div class="tabs">
	<ul class="nav nav-tabs">
		<?php
		if($marinas!=null){
			$n=1;
			foreach ($marinas as $port) {
if($port['id']!=Yii::$app->params['emp_id']){
		?>
		<li class="nav-item">
			<a class="nav-link<?= $port_id==$port['id'] ? ' active' : ''?>" href="<?= Url::to(['report/claims','port_id'=>$port['id']])?>"><?= $port['name']?></a>
		</li>
		<?php $n++;}}}?>
	</ul>
	<div class="tab-content">
		<div id="tab_1" class="tab-pane active">
			<section class="panel">
				<?php CustomPjax::begin(['id'=>'list-container']); ?>
				<div class="panel-body">
					<?php if($port_id==Yii::$app->params['emp_id']){?>
					<?= CustomGridView::widget([
						'dataProvider' => $dataProvider,
						'filterModel'=>$searchModel,
						'cardHeader'=>false,
						'columns' => [
							['class' => 'yii\grid\SerialColumn'],
							['format'=>'html','label'=>'Member','value' => function ($model){return ($model->booking->member!=null ? $model->booking->member->fullname : 'Not set');}],
							['format'=>'html','label'=>'Boat','value' => function ($model){return ($model->booking->boat!=null ? $model->booking->boat->name : 'Not set');}],
							['format'=>'date','label'=>'Booking Date','value' => function ($model){return $model->booking->booking_date;}],
							['format'=>'date','label'=>'Claimed Date','value' => function ($model){return $model->claimOrder->created_at;}],
							['format'=>'html','label'=>'Time','value' => function ($model){return $model->booking->timeZone->name.($model->booking->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');}],
							['label'=>'Petrol Consumed','value'=>function($model){return 'AED '.$model->booking->bookingActivity->claimAmount;}],
							['format'=>'html','label'=>'Remarks','value'=>function($model){
								$html='';
								$comments=BookingOperationComments::find()
								->where([
									'process_type'=>0,
									'booking_id'=>$model->booking->id,
									'trashed'=>0,
								])
								->asArray()
								->orderBy(['id'=>SORT_DESC])->all();
								if($comments!=null){
									foreach($comments as $comment){
										$html.=$comment['comments'];
									}
								}
								return $html;
							}]
						],
						'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
					]); ?><br />
					<?php }else{?>
						<?= CustomGridView::widget([
							'dataProvider' => $dataProvider,
							'filterModel'=>$searchModel,
							'cardHeader'=>false,
							'showFooter' => true,
							'columns' => [
								['class' => 'yii\grid\SerialColumn'],
								['format'=>'date','label'=>'Date','value'=>function($model){
									return $model['created_at'];
								}],
								['format'=>'html','label'=>'Description','value'=>function($model){
									$html='';
									if($model['rec_type']=='order'){
										$staffName='';
										$user=User::find()->where(['id'=>$model['user_id']])->one();
										$html = 'Claimed by '.$user->fullname;
									}
									if($model['rec_type']=='booking'){
										$memberName='';
										$user=User::find()->where(['id'=>$model['user_id']])->one();
										$html = 'Booking by '.$user->fullname.' ';
									}
									return $html;
								}],
								['format'=>'html','label'=>'Amount','value'=>function($model){
									$amt='';
									if($model['rec_type']=='order'){
										$amt='AED -'.$model['amount'];
									}
									if($model['rec_type']=='booking'){
										$amt='AED +'.$model['amount'];
									}
									return $amt;
								},'footer'=>'AED '.Yii::$app->appHelperFunctions->getUnClaimedMarinaAmount($searchModel->port_id)],
								['format'=>'html','label'=>'Remarks','value'=>function($model){
									$html='';
									if($model['rec_type']=='order'){
										$comments=ClaimOrderComments::find()
										->where([
											'process_type'=>0,
											'claim_order_id'=>$model['id'],
											'trashed'=>0,
										])
										->asArray()
										->orderBy(['id'=>SORT_DESC])->all();
										if($comments!=null){
											foreach($comments as $comment){
												$html.=$comment['comments'];
											}
										}
									}
									return $html;
								}]
							],
							'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
						]); ?><br />
					<?php }?>
				</div>
			<?php CustomPjax::end(); ?>
			</section>
		</div>
	</div>
</div>
