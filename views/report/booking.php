<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use app\assets\BookingReportAsset;
BookingReportAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Booking Report');
$this->params['breadcrumbs'][] = Yii::T('app','Reports');
$this->params['breadcrumbs'][] = $this->title;


$columns[]=['class'=>'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value'=>function($model){
	$html =$model->member->fullname.'<br />';
	$html.='Licensed: '.Yii::$app->helperFunctions->arrYesNoSpan[$model->member->profileInfo->is_licensed].'<br />';
	$html.='Status: '.'<span class="badge badge-'.($model->status==1 ? 'success' : 'dark').'">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>';
	return $html;
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3'],'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value'=>function($model){
	return ($model->marina!=null ? $model->marina->name : 'Not set');
},'headerOptions'=>['class'=>'fxd4'],'contentOptions'=>['class'=>'fxd4'],'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'boat_id','label'=>'Boat','value'=>function($model){
	return ($model->boat!=null ? $model->boat->name : 'Not set');
},'headerOptions'=>['class'=>'fxd4'],'contentOptions'=>['class'=>'fxd4'],'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'boat_provided','label'=>'Boat Provided','value'=>function($model){
	return ($model->bookingActivity!=null && $model->bookingActivity->boatProvided!=null ? $model->bookingActivity->boatProvided->name : '');
},'headerOptions'=>['class'=>'fxd4'],'contentOptions'=>['class'=>'fxd4'],'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Session','value'=>function($model){
	return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');
},'headerOptions'=>['class'=>'fxd4'],'contentOptions'=>['class'=>'fxd4'],'filter'=>false];
$columns[]=['format'=>'date','attribute'=>'booking_date','label'=>'Date','headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3'],'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'booking_exp','label'=>'Booking Status','value'=>function($model){
	return Yii::$app->helperFunctions->bookingExperience[$model->booking_exp];
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3'],'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'trip_exp','label'=>'Trip Status','value'=>function($model){
	return $model->tripExperienceComments;
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3'],'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'member_exp','label'=>'Behaviour','value'=>function($model){
	$activity=$model->bookingActivity;
	if($activity!=null && $activity->member_exp==1){
		return $activity->memberExperienceComments;
	}
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3'],'filter'=>false];
$columns[]=['format'=>'raw','attribute'=>'booking_extra','label'=>'Extras','value'=>function($model){
	return $model->bookingExtras;
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3'],'filter'=>false];
$columns[]=['format'=>'raw','attribute'=>'booking_comments','label'=>'Remarks','value'=>function($model){
	return $model->oprRemarks;
},'headerOptions'=>['class'=>'fxd4'],'contentOptions'=>['class'=>'fxd4'],'filter'=>false];
$columns[]=['format'=>'raw','attribute'=>'booking_damages','label'=>'Damages','value'=>function($model){
	return $model->bookingDamages;
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3'],'filter'=>false];
$columns[]=['format'=>'raw','label'=>'Addons','value'=>function($model){
	return $model->addonsSpan;
},'headerOptions'=>['class'=>'fxd5'],'contentOptions'=>['class'=>'fxd5']];
$columns[]=['label'=>'Time In','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->dep_time : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];
$columns[]=['label'=>'Time Out','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->arr_time : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];
$columns[]=['label'=>'Fuel In','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->dep_fuel_level : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];
$columns[]=['label'=>'Fuel Out','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->arr_fuel_level : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];
$columns[]=['label'=>'Cost of Fuel','value'=>function($model){
	return $model->bookingActivity!=null ? 'AED '.$model->bookingActivity->fuel_cost : '';
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3']];
$columns[]=['label'=>'Engine In','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->dep_engine_hours : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];
$columns[]=['label'=>'Engine Out','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->arr_engine_hours : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];
$columns[]=['label'=>'# of Ppl In','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->dep_ppl : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];
$columns[]=['label'=>'# of Ppl Out','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->arr_ppl : '';
},'headerOptions'=>['class'=>'fxd3'],'contentOptions'=>['class'=>'fxd3']];
$columns[]=['label'=>'Opr Staff','value'=>function($model){
	return $model->bookingActivity!=null ? $model->bookingActivity->createdBy : '';
},'headerOptions'=>['class'=>'fxd2'],'contentOptions'=>['class'=>'fxd2']];











if($searchModel->city_id!=null){
  $marinaList=Yii::$app->appHelperFunctions->getActiveCityMarinaListArr($searchModel->city_id);
}else{
  $marinaList=Yii::$app->appHelperFunctions->activeMarinaListArr;
}

$boatsList=Yii::$app->appHelperFunctions->getActiveBoatsListArr($searchModel->city_id,$searchModel->port_id);

?>
<style>
.filters{display: none;}
.top-filter-row .form-group{margin-bottom: 5px;}
</style>
<div class="report-booking">
	<?php CustomPjax::begin(['id'=>'grid-container']); ?>
	<section class="card mb-1">
    <div class="card-body filter-padding">
      <?php $form = ActiveForm::begin(['action'=>Url::to(['report/booking']),'method'=>'get','options'=>['data-pjax' => '']]); ?>
      <div class="row top-filter-row">
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'booking_date')->textInput(['class'=>'form-control dtrpicker','autocomplete'=>'off','placeholder'=>'Date Range'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'booking_time_slot')->dropDownList(Yii::$app->appHelperFunctions->timeSlotListDashboardArr,['prompt'=>'Session'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr,['prompt'=>$searchModel->getAttributeLabel('city_id')])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'port_id')->dropDownList($marinaList,['prompt'=>$searchModel->getAttributeLabel('port_id')])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'boat_id')->dropDownList($boatsList,['prompt'=>$searchModel->getAttributeLabel('boat_id')])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'boat_provided')->dropDownList($boatsList,['prompt'=>'Boat Provided'])->label(false) ?>
        </div>
				<div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'member_name')->textInput(['autocomplete'=>'off','placeholder'=>'Member'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'is_licensed')->dropDownList(Yii::$app->helperFunctions->arrYesNo,['prompt'=>'Licensed'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'status')->dropDownList(Yii::$app->helperFunctions->reportBookingStatus,['prompt'=>'Booking Experience'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'booking_exp')->dropDownList(Yii::$app->helperFunctions->bookingExperience,['prompt'=>'Booking Status'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'report_trip_exp')->dropDownList(Yii::$app->helperFunctions->reportTripExperience,['prompt'=>'Trip Experience'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'member_exp')->dropDownList(Yii::$app->helperFunctions->memberExperience,['prompt'=>'Behaviour'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'booking_extra')->dropDownList(Yii::$app->helperFunctions->bookingExtras,['prompt'=>'Extra'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
					<?= $form->field($searchModel, 'booking_comments')->textInput(['autocomplete'=>'off','placeholder'=>'Remarks'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <div class="form-group">
            <?= Html::submitButton('<i class="fas fa-search"></i> '.Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </section>
	<?= CustomGridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'showFooter' => false,
		'showPerPage' => true,
    'options'=>['class'=>'grid-view fixed-cols1'],
		'columns' => $columns,
		'layout'=>"<div class=\"table-responsive\">{items}</div><div>{summary}</div><div>{pager}</div>",
	]); ?>
	<?php CustomPjax::end(); ?>
</div>
<?= $this->render('js/booking_script')?>
