<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use yii\widgets\ActiveForm;
use app\assets\ReportAsset;
ReportAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manual Deposits');
$this->params['breadcrumbs'][] = Yii::t('app','Report');
$this->params['breadcrumbs'][] = $this->title;
$name='';
?>
<div class="report-manual-deposit">
	<?php CustomPjax::begin(['id'=>'grid-container']); ?>
	<section class="card mb-1">
    <div class="card-body filter-padding">
      <?php $form = ActiveForm::begin(['action'=>Url::to(['report/manual-deposits']),'method'=>'get','options'=>['data-pjax' => '']]); ?>
      <div class="row top-filter-row">
        <div class="col-xs-12 col-sm-4">
          <?= $form->field($searchModel, 'date_range')->textInput(['autocomplete'=>'off','placeholder'=>'Date Range'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-4">
					<div class="hidden">
						<?= $form->field($searchModel, 'user_id')->textInput()->label(false) ?>
					</div>
					<?= $form->field($searchModel, 'username')->textInput(['autocomplete'=>'off','placeholder'=>'Username'])->label(false) ?>
        </div>
        <div class="col-xs-12 col-sm-4">
          <div class="form-group">
            <?= Html::submitButton('<i class="fas fa-search"></i> '.Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </section>
	<?= CustomGridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'showFooter' => true,
		'showPerPage' => true,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			['format'=>'datetime','label'=>'Date','attribute'=>'created_at','enableSorting' => false],
			['attribute'=>'descp','enableSorting' => false],
			['label'=>'Member','value'=>function($model){return $model->member!=null ? $model->member->username : '';},'enableSorting' => false],
			['attribute'=>'amount', 'footer'=>$searchModel->totalManual,'enableSorting' => false],
		],
		'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
	]); ?>
	<?php CustomPjax::end(); ?>
</div>
<?= $this->render('js/scripts')?>
