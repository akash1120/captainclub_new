<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */

$this->title = Yii::t('app', 'Update Permission Group: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Permission Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="admin-group-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
