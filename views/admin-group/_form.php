<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$("body").on("click", ".cbl1", function () {
  thisid=$(this).attr("value");
  $(".cbl1_"+thisid).prop("checked",$(this).prop("checked"));
});
');

$menuLinks=Yii::$app->menuHelperFunction->adminMenuList;
?>
<section class="admin-group-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $this->title?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
      </div>
    </div>
    <div class="row">
      <?php
      $n=1;
      foreach($menuLinks as $menuLink){
        $boxSelected=Yii::$app->menuHelperFunction->isChecked($model->id,$menuLink['id']);
        ?>
        <div class="col-xs-12 col-sm-6">
          <div class="card card-featured">
            <header class="card-header">
							<h2 class="card-title">
                <label>
                  <input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl1" name="AdminGroup[actions][]" value="<?= $menuLink['id']?>" />
                  <?= $menuLink['title']?>
                </label>
              </h2>
						</header>
            <div class="card-body">
              <?php
              $menuSubOptions=Yii::$app->menuHelperFunction->getSubOptions($menuLink['id']);
              if($menuSubOptions!=null){
                foreach($menuSubOptions as $subOption){
                  $boxSelected=Yii::$app->menuHelperFunction->isChecked($model->id,$subOption['id']);'';
                  ?>
                  <label><input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl2 cbl1_<?= $menuLink['id']?>" name="AdminGroup[actions][]" value="<?= $subOption['id']?>" /> <?= $subOption['title']?></label>&nbsp;&nbsp;
                  <?php
                }
              }
              ?>
            </div>
          </div>
        </div>
        <?php
        $n++;
        if($n>=3){echo '</div><div class="row">';$n=1;}
      }
      ?>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
