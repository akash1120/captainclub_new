<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */

$this->title = Yii::t('app', 'New Permission Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Permission Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
