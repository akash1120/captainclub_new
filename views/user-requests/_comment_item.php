<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequestsDiscussion */
?>
<div class="tm-box">
<div class="mt-comment-body">
    <div class="mt-comment-info">
        <span class="mt-comment-author"><?= $model->createdBy?>:</span>
        <span class="mt-comment-date"><i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDateTime($model->updated_at)?></span>
    </div>
    <div class="mt-comment-text"><?= nl2br($model->comments)?></div>
</div>

</div>
