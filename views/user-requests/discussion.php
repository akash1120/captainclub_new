<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DiscussionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Discussion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Discussion');

?>
<section class="card mb-4">
	<header class="card-header">
		<div class="card-actions">
      <strong>Posted:</strong> <?= $model->request->created_at?>
			<a href="ui-elements-cards.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
		</div>
		<h2 class="card-title"><?= Yii::$app->helperFunctions->requestTypes[$model->request_type].' request by '.$model->request->member->fullname?></h2>
	</header>
	<div class="card-body">
		<?= Yii::$app->appHelperFunctions->getRequestDetail($model['request_type'],$model['request_id'])?>
	</div>
</section>
<section class="card mb-4">
	<header class="card-header">
    <div class="discussion-form">
      <?php $form = ActiveForm::begin(); ?>
      <div class="row">
        <div class="col-sm-12">
          <?= $form->field($model, 'comments',['template'=>'
          <div class="input-group">
          {input}
          <span class="input-group-btn">
          '.Html::submitButton(Yii::t('app', 'Post'), ['class' => 'btn btn-success']).'
          </span>
          </div>
          {error}
          '])->textInput(['maxlength' => true]) ?>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
	</header>
	<div class="card-body comments">
    <div class="timeline timeline-simple changelog">
      <div class="tm-body">
        <?php
        echo ListView::widget( [
          'dataProvider' => $dataProvider,
          'id' => 'my-listview-id',
          'options'=>['tag'=>'ol', 'class'=>'tm-items'],
          'itemOptions' => ['tag'=>'li','class' => 'tm-items'],
          'itemView' => '_comment_item',
          'layout'=>"
          {items}
          <center>{pager}</center>
          ",
          ] );
          ?>
        </div>
      </div>
	</div>
</section>
<style>
.panel-body.comments{padding:5px;}
.timeline .tm-body{ padding:1px 0;}
.timeline.timeline-simple .tm-body .tm-items > li{margin: 10px 0;}
.empty{padding-left: 40px;}
</style>
