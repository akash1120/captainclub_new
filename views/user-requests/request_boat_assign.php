<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequestsBoatAssign */
/* @var $form yii\widgets\ActiveForm */

if($model->city_id>0){
	$marinaList = Yii::$app->appHelperFunctions->getCityMarinaListArr($model->city_id);
}else{
	$marinaList = Yii::$app->appHelperFunctions->marinaListArr;
}

$timeZones = Yii::$app->appHelperFunctions->timeSlotListArr;

$memberName=$requestModel->member->fullname;
?>
<?= '<strong>Member:</strong> '.$memberName?><br />
<?= Yii::$app->appHelperFunctions->getRequestDetail($item_type,$requestModel['id'])?>
<hr />
<div class="request-boat-assign-form">
	<?php $form = ActiveForm::begin(['id'=>'request-boat-assign-form']); ?>
	<div style="display:none;">
		<?= $form->field($model, 'selected_boat')->textInput() ?>
		<?= $form->field($model, 'date')->textInput() ?>
	</div>
  <div class="row">
		<div class="col-sm-6">
				<?= $form->field($model, 'marina_id')->dropDownList($marinaList,['class'=>'form-control','prompt'=>$model->getAttributeLabel('marina_id'),'data-date'=>$requestModel['requested_date']]) ?>
		</div>
    <div class="col-sm-6">
        <?= $form->field($model, 'time_id')->dropDownList($timeZones,['class'=>'form-control','prompt'=>$model->getAttributeLabel('time_id'),'data-date'=>$requestModel['requested_date']]) ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'boat_id')->dropDownList([]) ?>
    </div>
  </div>
	<div class="form-group">
	  <?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
	  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
