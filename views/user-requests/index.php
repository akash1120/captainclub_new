<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\TimeSlotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-request-index">
  <?= $this->render('_gridview',[
    'searchModel'=>$searchModel,
    'dataProvider'=>$dataProvider,
  ])?>
</div>
