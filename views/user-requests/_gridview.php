<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\LogUserRequestToBoatAssign;
use app\models\Booking;
use app\assets\UserRequestAssets;
UserRequestAssets::register($this);



$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
  initScripts();
});
');

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'raw','attribute'=>'membername','value'=>function($model){
  $isPending=true;
  if($model['status']!=0){
    $isPending=false;
  }else{
    if($model['requested_date']<date("Y-m-d")){
      $isPending=false;
    }else{
      $isAssigned = LogUserRequestToBoatAssign::find()->where(['request_type'=>$model['item_type'],'request_id'=>$model['id']]);
      if($isAssigned->exists()){
        $isPending=false;
      }
    }
  }
  $memberModel=User::findOne($model['user_id']);
  if($isPending==false){
    $memberName=$memberModel->firstLastName;
  }else{
    $memberName='<a href="javascript:;" class="load-modal" data-url="'.Url::to(['user-requests/boat-assign','item_type'=>$model['item_type'],'id'=>$model['id']]).'" data-heading="Booking Request">'.$memberModel->firstLastName.'</a>';
  }
  return $memberName.'<br />'.$memberModel->packageWithExpiry;
}];

if(Yii::$app->controller->action->id=='night-drive-training'){
  $requestTypes['nightdrivetraining']='Night Drive Training';
}else{
  $requestTypes=Yii::$app->helperFunctions->requestTypes;
}
$columns[]=['format'=>'html','attribute'=>'item_type','label'=>Yii::t('app','Detail'),'value'=>function($model){
  $html='';
  if($model['item_type']=='nightdrivetraining' && in_array($model['status'],[1,-1])){
    $html=Yii::$app->appHelperFunctions->getNDTrainingRemarks($model['id']);
  }
  return Yii::$app->appHelperFunctions->getRequestDetail($model['item_type'],$model['id']).$html;
},'filter'=>$requestTypes];
$columns[]=['format'=>'date','attribute'=>'requested_date','label'=>Yii::t('app','Requested'),'filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]=['format'=>'date','attribute'=>'created_at','label'=>Yii::t('app','Posted'),'value'=>function($model){
  return Yii::$app->formatter->asDate(date("Y-m-d",strtotime($model['created_at'])));
},'filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]=['format'=>'html','attribute'=>'status','label'=>'Status','value'=>function($model){
  if($model['item_type']=='bookingice' || $model['item_type']=='bookingkidsjackets'){
    $html='<span class="badge grid-badge badge-info">Noted</span>';
    if($model['item_type']=='bookingkidsjackets'){
      $booking=Booking::findOne($model['id']);
      $html.='<br />';
      $html.=($booking->no_of_one_to_three_jackets>0 ? '<span class="badge grid-badge badge-info">Age (1-3 yrs) = '.$booking->no_of_one_to_three_jackets.'</span>' : '').($booking->no_of_four_to_twelve_jackets>0 ? '<span class="badge grid-badge badge-info">Age (4-12 yrs) = '.$booking->no_of_four_to_twelve_jackets.'</span>' : '');
    }
  }else{
    $html=Yii::$app->helperFunctions->requestStatusIcon[$model['status']];
    if(
      $model['item_type']=='waitingcaptain'
      || $model['item_type']=='waitingequipment'
      || $model['item_type']=='waitingbbq'
      || $model['item_type']=='waitingwakeboarding'
      || $model['item_type']=='waitingwakesurfing'
      || $model['item_type']=='waitingearlydeparture'
      || $model['item_type']=='waitingovernightcamping'
      || $model['item_type']=='waitinglatearrival'
    ){
      if($model['reason']!='' && $model['reason']!=null){
        $html.='<br /><span class="badge badge-info">Reason: '.$model['reason'].'</span>';
      }
    }
  }
  return $html;
},'filter'=>['0'=>'Under Process','1'=>'Met','2'=>'Not Met','3'=>'Cancelled']];
$columns[]=['attribute'=>'expected_reply','label'=>Yii::t('app','Estimated Time Of Reply'),'value'=>function($model){
  $date='';
  if(
    $model['item_type']=='waitingcaptain'
    || $model['item_type']=='waitingequipment'
    || $model['item_type']=='waitingbbq'
    || $model['item_type']=='waitingwakeboarding'
    || $model['item_type']=='waitingwakesurfing'
    || $model['item_type']=='waitingearlydeparture'
    || $model['item_type']=='waitingovernightcamping'
    || $model['item_type']=='waitinglatearrival'
  ){
    if($model['expected_reply']!='' && $model['expected_reply']!=null){
      $date=Yii::$app->formatter->asDate($model['expected_reply']);
    }
  }
  return $date;
},'filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]='remarks';
if(Yii::$app->menuHelperFunction->checkActionAllowed('discussion')){
  $columns[]=[
    'class' => 'yii\grid\ActionColumn',
    'header'=>'Discussion',
    'contentOptions'=>['class'=>'nosd'],
    'template' => '{discussion}',
    'buttons' => [
      'discussion' => function ($url, $model) {
        return Yii::$app->appHelperFunctions->getRequestDiscussionLastComment($model['item_type'],$model['id']).Html::a('<i class="fas fa-comments"></i> Discussion', 'javascript:;', [
          'title' => Yii::t('app', 'Discussion'),
          'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-primary',
          'onclick'=>"window.open('".Url::to(['user-requests/discussion','item_type'=>$model['item_type'],'id'=>$model['id']])."')",
          'target'=>'_blank',
        ]);
      },
    ],
  ];
}
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'contentOptions'=>['class'=>'nosd'],
  'template' => '
  <div class="btn-group flex-wrap">
  <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
  <span class="caret"></span>
  </button>
  <div class="dropdown-menu" role="menu">
  {met} {notmet} {cancelled} {delete} {active-booking}
  </div>
  </div>',
  'buttons' => [
    'met' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action')){
        if(in_array($model['item_type'],['boatbooking','other','upgradecity','waitingcaptain','waitingequipment','waitingbbq','waitingwakeboarding','waitingwakesurfing','waitingovernightcamping','waitingearlydeparture','waitinglatearrival'])){
          if($model['status']==0){
            return Html::a('<i class="fas fa-check"></i> Met', 'javascript:;', [
              'title' => Yii::t('app', 'Met'),
              'class'=>'dropdown-item text-1 load-modal',
              'data-url'=>Url::to(['request-action','s'=>1,'item_type'=>$model['item_type'],'id'=>$model['id']]),
              'data-heading'=>Yii::t('app','Request Action'),
            ]);
          }
        }elseif($model['item_type']=='nightdrivetraining' && $model['status']==0){
          return Html::a('<i class="fas fa-check"></i> Mark Done', 'javascript:;', [
            'title' => Yii::t('app', 'Mark Done'),
            'data-id'=>$model['id'],
            'data-item_type'=>$model['item_type'],
            'class'=>'dropdown-item text-1 load-modal',
            'data-url'=>Url::to(['training-done','item_type'=>$model['item_type'],'id'=>$model['id']]),
            'data-heading'=>Yii::t('app','Request Action'),
          ]);
        }elseif($model['item_type']=='nightdrivetraining' && $model['status']==-1){
          return Html::a('<i class="fas fa-check"></i> Met', 'javascript:;', [
            'title' => Yii::t('app', 'Met'),
            'data-id'=>$model['id'],
            'data-item_type'=>$model['item_type'],
            'class'=>'dropdown-item text-1 load-modal',
            'data-url'=>Url::to(['request-action','s'=>1,'item_type'=>$model['item_type'],'id'=>$model['id']]),
            'data-heading'=>Yii::t('app','Request Action'),
          ]);
        }
      }
    },
    'notmet' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action')){
        if(in_array($model['item_type'],['boatbooking','other','upgradecity','nightdrivetraining'])){
        //if(in_array($model['item_type'],['boatbooking','other','upgradecity','nightdrivetraining','waitingcaptain','waitingovernightcamping','waitingearlydeparture','waitinglatearrival'])){
          if($model['status']==0){
            return Html::a('<i class="fas fa-times"></i> Not Met', 'javascript:;', [
              'title' => Yii::t('app', 'Not Met'),
              'data-id'=>$model['id'],
              'data-item_type'=>$model['item_type'],
              'class'=>'dropdown-item text-1 load-modal',
              'data-url'=>Url::to(['request-action','s'=>2,'item_type'=>$model['item_type'],'id'=>$model['id']]),
              'data-heading'=>Yii::t('app','Request Action'),
            ]);
          }
        }
      }
    },
    'cancelled' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action')){
        if(in_array($model['item_type'],['boatbooking','other','upgradecity','nightdrivetraining'])){
          if($model['status']==0){
            return Html::a('<i class="fas fa-exclamation"></i> Cancelled', 'javascript:;', [
              'title' => Yii::t('app', 'Cancelled'),
              'data-id'=>$model['id'],
              'data-item_type'=>$model['item_type'],
              'class'=>'dropdown-item text-1 load-modal',
              'data-url'=>Url::to(['request-action','s'=>3,'item_type'=>$model['item_type'],'id'=>$model['id']]),
              'data-heading'=>Yii::t('app','Request Action'),
            ]);
          }
        }
      }
    },
    'delete' => function ($url, $model) {
      if(($model['status']==0 || ($model['status']==1 && $model['item_type']=='freeze')) && Yii::$app->menuHelperFunction->checkActionAllowed('delete','user-requests')){
        return Html::a('<i class="fas fa-trash"></i> Delete', 'javascript:;', [
          'title' => Yii::t('app', 'Delete'),
          'class'=>'dropdown-item text-1 btn-delrequest',
          'data-id'=>$model['id'],
          'data-item_type'=>$model['item_type'],
        ]);
      }
    },
    'active-booking' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('bookings','user')){
        return Html::a('<i class="fas fa-anchor"></i> Active Bookings', 'javascript:;', [
          'title' => Yii::t('app', 'Active Bookings'),
          'class'=>'dropdown-item text-1',
          'onclick'=>"window.open('".Url::to(['user/bookings','id'=>$model['user_id'],'listType'=>'future'])."')",
          'target'=>'_blank',
        ]);
      }
    },
  ],
];
?>
<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<?= CustomTabbedGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => $columns,
]);?>
<?php CustomPjax::end(); ?>
<script>
function initScripts()
{
  $(".dtpicker").datepicker({
    format: "yyyy-mm-dd",
  }).on("changeDate", function(e){
    $(this).datepicker("hide");
  });
}
</script>
<?= $this->render('/request/js/admin_request_scripts')?>
<?= $this->render('/request/js/boat_assign_scripts')?>
