<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequests */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
');
?>
<div class="request-training-form">
    <?php $form = ActiveForm::begin(['id'=>'trainingdone-form']); ?>
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<?= $form->field($model, 'training_date')->textInput(['class'=>'form-control dtpicker','readonly'=>'readonly']) ?>
			</div>
			<div class="col-xs-12 col-sm-6">
				<?= $form->field($model, 'marina_id')->dropDownList(YII::$app->appHelperFunctions->marinaListArr,['prompt'=>'Select']) ?>
			</div>
		</div>
    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>
