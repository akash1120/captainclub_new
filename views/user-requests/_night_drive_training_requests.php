<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\UserRequestsBoatAssign;
use app\assets\UserRequestAssets;
UserRequestAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserRequestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Night Drive Training Requests');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
  initScripts();
});
');

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'raw','attribute'=>'membername','value'=>function($model){
  $isPending=true;
  if($model['status']!=0){
    $isPending=false;
  }else{
    $isAssigned = UserRequestsBoatAssign::find()->where(['request_id'=>$model['id']]);
    if($isAssigned->exists()){
      $isPending=false;
    }
  }
  $memberModel=User::findOne($model['user_id']);
  if($isPending==false){
    $memberName=$memberModel->username;
  }else{
    $memberName='<a href="javascript:;" class="load-modal" data-url="'.Url::to(['user-requests/boat-assign',''=>$model['item_type'],'id'=>$model['id']]).'" data-heading="Boking Request">'.$memberModel->username.'</a>';
  }
  return $memberName.'<br />'.$memberModel->packageWithExpiry;
}];
$columns[]=['format'=>'html','attribute'=>'item_type','label'=>Yii::t('app','Detail'),'value'=>function($model){
  return Yii::$app->appHelperFunctions->getRequestDetail($model['item_type'],$model['id']);
},'filter'=>Yii::$app->helperFunctions->requestTypes];
$columns[]=['format'=>'date','attribute'=>'requested_date','label'=>Yii::t('app','Requested')];
$columns[]=['format'=>'date','attribute'=>'created_at','label'=>Yii::t('app','Posted')];
$columns[]=['format'=>'html','attribute'=>'status','label'=>'Status','value'=>function($model){
  return Yii::$app->helperFunctions->requestStatusIcon[$model['status']];
},'filter'=>['0'=>'Under Process','1'=>'Met','2'=>'Not Met']];
$columns[]='remarks';
if(Yii::$app->menuHelperFunction->checkActionAllowed('discussion')){
  $columns[]=[
    'class' => 'yii\grid\ActionColumn',
    'header'=>'Discussion',
    'contentOptions'=>['class'=>'nosd'],
    'template' => '{discussion}',
    'buttons' => [
      'discussion' => function ($url, $model) {
        return Yii::$app->appHelperFunctions->getRequestDiscussionLastComment($model['item_type'],$model['id']).Html::a('<i class="fas fa-comments"></i> Discussion', 'javascript:;', [
          'title' => Yii::t('app', 'Discussion'),
          'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-primary',
          'onclick'=>"window.open('".Url::to(['user-requests/discussion','item_type'=>$model['item_type'],'id'=>$model['id']])."')",
          'target'=>'_blank',
        ]);
      },
    ],
  ];
}
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'contentOptions'=>['class'=>'nosd'],
  'template' => '
  <div class="btn-group flex-wrap">
  <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
  <span class="caret"></span>
  </button>
  <div class="dropdown-menu" role="menu">
  {met} {notmet} {cancelled} {delete} {active-booking}
  </div>
  </div>',
  'buttons' => [
    'met' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action')){
        echo "Here?";
        if(in_array($model['item_type'],['boatbooking','other','upgradecity'])){
          if($model['status']==0){
            return Html::a('<i class="fas fa-check"></i> Met', 'javascript:;', [
              'title' => Yii::t('app', 'Met'),
              'class'=>'dropdown-item text-1 load-modal',
              'data-url'=>Url::to(['request-action','s'=>1,'item_type'=>$model['item_type'],'id'=>$model['id']]),
              'data-heading'=>Yii::t('app','Request Action'),
            ]);
          }
        }elseif($model['item_type']=='nightdrivetraining' && $model['status']==0){
          return Html::a('<i class="fas fa-check"></i> Mark Done', 'javascript:;', [
            'title' => Yii::t('app', 'Mark Done'),
            'data-id'=>$model['id'],
            'data-item_type'=>$model['item_type'],
            'class'=>'dropdown-item text-1 load-modal',
            'data-url'=>Url::to(['training-done','item_type'=>$model['item_type'],'id'=>$model['id']]),
            'data-heading'=>Yii::t('app','Request Action'),
          ]);
        }elseif($model['item_type']=='nightdrivetraining' && $model['status']==-1){
          return Html::a('<i class="fas fa-check"></i> Met', 'javascript:;', [
            'title' => Yii::t('app', 'Met'),
            'data-id'=>$model['id'],
            'data-item_type'=>$model['item_type'],
            'class'=>'dropdown-item text-1 load-modal',
            'data-url'=>Url::to(['request-training-action','item_type'=>$model['item_type'],'id'=>$model['id']]),
            'data-heading'=>Yii::t('app','Request Action'),
          ]);
        }
      }
    },
    'notmet' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action')){
        if(in_array($model['item_type'],['boatbooking','other','upgradecity','nightdrivetraining'])){
          if($model['status']==0){
            return Html::a('<i class="fas fa-times"></i> Not Met', 'javascript:;', [
              'title' => Yii::t('app', 'Not Met'),
              'data-id'=>$model['id'],
              'data-item_type'=>$model['item_type'],
              'class'=>'dropdown-item text-1 load-modal',
              'data-url'=>Url::to(['request-action','s'=>2,'item_type'=>$model['item_type'],'id'=>$model['id']]),
              'data-heading'=>Yii::t('app','Request Action'),
            ]);
          }
        }
      }
    },
    'cancelled' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action')){
        if($model['status']==0){
          return Html::a('<i class="fas fa-exclamation"></i> Cancelled', 'javascript:;', [
            'title' => Yii::t('app', 'Cancelled'),
            'data-id'=>$model['id'],
            'data-item_type'=>$model['item_type'],
            'class'=>'dropdown-item text-1 load-modal',
            'data-url'=>Url::to(['request-action','s'=>3,'item_type'=>$model['item_type'],'id'=>$model['id']]),
            'data-heading'=>Yii::t('app','Request Action'),
          ]);
        }
      }
    },
    'delete' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
        return Html::a('<i class="fas fa-trash"></i> Delete', 'javascript:;', [
          'title' => Yii::t('app', 'Delete'),
          'class'=>'dropdown-item text-1 btn-delrequest',
          'data-id'=>$model['id'],
          'data-item_type'=>$model['item_type'],
        ]);
      }
    },
    'active-booking' => function ($url, $model) {
      if(Yii::$app->menuHelperFunction->checkActionAllowed('bookings','user')){
        return Html::a('<i class="fas fa-anchor"></i> Active Bookings', 'javascript:;', [
          'title' => Yii::t('app', 'Active Bookings'),
          'class'=>'dropdown-item text-1 btn-delrequest',
          'onclick'=>"window.open('".Url::to(['user/bookings','id'=>$model['user_id'],'listType'=>'future'])."')",
          'target'=>'_blank',
        ]);
      }
    },
  ],
];
?>
<div class="color-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function initScripts()
{
  $(".dtpicker").datepicker({
    format: "yyyy-mm-dd",
  }).on("changeDate", function(e){
    $(this).datepicker("hide");
  });
}
</script>
<?= $this->render('/request/js/admin_request_scripts')?>
