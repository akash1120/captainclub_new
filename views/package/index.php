<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Packages');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
?>
<div class="package-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      'name',
      //'members',
      ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){return isset(Yii::$app->helperFunctions->arrPublishingIcon[$model['status']]) ? Yii::$app->helperFunctions->arrPublishingIcon[$model['status']] : '';},'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrPublishing],
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
        'contentOptions'=>['class'=>'noprint actions'],
        'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
        'buttons' => [
            'update' => function ($url, $model) {
              return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                'title' => Yii::t('app', 'Edit'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'status' => function ($url, $model) {
              if($model['status']==1){
                return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                  'title' => Yii::t('app', 'Disable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                  'data-method'=>"post",
                ]);
              }else{
                return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                  'title' => Yii::t('app', 'Enable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                  'data-method'=>"post",
                ]);
              }
            },
            'delete' => function ($url, $model) {
              return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                'title' => Yii::t('app', 'Delete'),
                'class'=>'dropdown-item text-1',
                'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                'data-method'=>"post",
                'data-pjax'=>"0",
              ]);
            },
        ],
      ],
    ],
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
