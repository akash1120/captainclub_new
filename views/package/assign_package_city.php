<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PackageCity;

/* @var $this yii\web\View */

$this->title = 'Assign Cities to Packages';


$packages=Yii::$app->appHelperFunctions->packageList;
$cities=Yii::$app->appHelperFunctions->cityList;
?>
<section class="package-assign-city-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <?php if($packages!=null){?>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Package</th>
          <th width="300">Allowed Cities</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($packages as $package){?>
        <tr>
          <td><?= $package['name']?></td>
          <td>
            <?php
            if($cities!=null){
              foreach($cities as $city){
                $checked="";
                $cityAllowed=PackageCity::find()->where(['package_id'=>$package['id'],'city_id'=>$city['id']]);
                if($cityAllowed->exists()){
                  $checked=' checked="checked"';
                }
            ?>
            <label>
              <input type="checkbox"<?= $checked?> name="PackageCityAssignForm[package_city][<?= $package['id']?>][]" value="<?= $city['id']?>" /> <?= $city['name']?>
            </label>&nbsp;&nbsp;
            <?php
              }
            }
            ?>
          </td>
        </tr>
        <?php }?>
      </tbody>
    </table>
    <?php }?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
