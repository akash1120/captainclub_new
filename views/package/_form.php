<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\PackageAllowedOptions;
use app\models\PackageCity;
use app\models\PackageAllowedPurpose;

/* @var $this yii\web\View */
/* @var $model app\models\Package */
/* @var $form yii\widgets\ActiveForm */
if($model->id!=null){
  $model->allowed_options=ArrayHelper::map(PackageAllowedOptions::find()->select(['service_id'])->where(['package_id'=>$model->id])->asArray()->all(),"service_id","service_id");
  $model->allowed_cities=ArrayHelper::map(PackageCity::find()->select(['city_id'])->where(['package_id'=>$model->id])->asArray()->all(),"city_id","city_id");
  $model->allowed_purposes=ArrayHelper::map(PackageAllowedPurpose::find()->select(['purpose_id'])->where(['package_id'=>$model->id])->asArray()->all(),"purpose_id","purpose_id");
}
?>
<style>
.field-package-allowed_options label, .field-package-allowed_cities label, .field-package-allowed_purposes label{ width: 30%;}
</style>
<section class="package-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
      </div>
    </div>
    <?= $form->field($model, 'allowed_options')->checkboxList(Yii::$app->appHelperFunctions->packageOptionsListArr) ?>
    <?= $form->field($model, 'allowed_cities')->checkboxList(Yii::$app->appHelperFunctions->cityListArr) ?>
    <?= $form->field($model, 'allowed_purposes')->checkboxList(Yii::$app->appHelperFunctions->boatPurposeListArr) ?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
