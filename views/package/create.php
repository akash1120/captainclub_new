<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Package */

$this->title = Yii::t('app', 'New Package');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
