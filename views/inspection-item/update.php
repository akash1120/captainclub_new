<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InspectionItems */

$this->title = Yii::t('app', 'Update Inspection Item: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Inspection Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="inspection-item-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
