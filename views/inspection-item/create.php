<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\InspectionItems */

$this->title = Yii::t('app', 'New Inspection Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Inspection Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inspection-item-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
