<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Booking;
use app\models\Boat;
use app\models\BoatToTimeSlot;
use app\models\BoatAvailableDays;

$dayOfWeek=Yii::$app->helperFunctions->WeekendDay($date);
$available=true;
$subQueryAvailability=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);
$subQueryTimeSlot=BoatToTimeSlot::find()->select(['boat_id'])->where(['time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
$subQueryAlreadyBookedNightrives=Booking::find()->select(['boat_id'])->where(['port_id'=>$marina_id,'booking_date'=>$date,'booking_time_slot'=>Yii::$app->params['nightDriveTimeSlot'],'status'=>1,'trashed'=>0]);
$boats=Boat::find()
->select(['id','name'])
->where([
  'and',
  [
    'id'=>$subQueryAvailability,
    'id'=>$subQueryTimeSlot,
    'port_id'=>$marina_id,
    'status'=>1,
    'trashed'=>0,
  ],
  ['not in', 'id', $subQueryAlreadyBookedNightrives],
])
->asArray()->all();
$boatsArr=ArrayHelper::map($boats,"id","name");
?>
<div class="form-group">
  <label class="control-label">Select Boat</label>
  <?= Html::dropDownList('special_boat_id',null,$boatsArr,['id'=>'spSubBSel','class'=>'form-control']) ?>
</div>
<div>
	<button type="submit" class="btn btn-success" onclick="saveSubBoatSelection('<?= $fld?>')" data-dismiss="modal">Save</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
</div>
