<?php
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'city_id','label'=>'City','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');},'filter'=>false];
$columns[]=['format'=>'raw','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return $model->boatName;},'filter'=>false];
$columns[]=['format'=>'date','attribute'=>'booking_date','filterInputOptions'=>['class'=>'form-control dtrpicker','autocomplete'=>'off']];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>false];
$columns[] = ['format'=>'raw','label'=>'Status','attribute'=>'status','value'=>function($model){
  return '<span class="badge badge-primary">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>';;
}];
$columns[] = ['format'=>'raw','label'=>'Booking','value'=>function($model){
  return $model->bookingActivity!=null && $model->bookingActivity->booking_exp!=null ? '<span class="badge badge-primary">'.Yii::$app->helperFunctions->bookingExperience[$model->bookingActivity->booking_exp].'</span>' : '<span class="badge badge-primary">'.Yii::t('app','Smooth').'</span>';
}];
$columns[] = ['format'=>'raw','label'=>'Trip','value'=>function($model){
  return $model->bookingActivity!=null ? '<span class="badge badge-primary">'.Yii::$app->helperFunctions->tripExperience[$model->bookingActivity->trip_exp].'</span>' : '';
}];
?>
<?php CustomPjax::begin(['id'=>'grid-container','enablePushState' => false]); ?>
<?= CustomGridView::widget([
  'cardHeader'=>false,
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'createBtn' => false,
  'columns' => $columns,
]);?>
<?php CustomPjax::end(); ?>
