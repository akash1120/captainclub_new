<?php
use yii\widgets\ListView;
?>
<style>
.timeline .tm-body{padding: 0px;}
.timeline.timeline-simple .tm-body:after{left: -10px;}
.timeline.timeline-simple .tm-body .tm-items > li:before{left: -10px;}
.timeline.timeline-simple .tm-body .tm-items > li{padding: 0 0 0 10px;}
.timeline .tm-items > li{margin: 25px 0;}
.timeline .tm-items > li:first-child{margin: 0 0 25px;}
</style>
<section class="staff-note card card-featured card-featured-warning">
  <header class="card-header">
    <h2 class="card-title">Notes</h2>
  </header>
  <div class="card-body">
  <?= ListView::widget( [
      'dataProvider' => $dataProvider,
      'id' => 'my-listview-id',
      'options'=>['class'=>'timeline timeline-simple changelog'],
      'itemOptions' => ['tag'=>'li','class' => 'tm-items'],
      'itemView' => '/user-note/_item',
      'layout'=>"
          <div class=\"tm-body\"><ol class=\"tm-items\">{items}</ol></div>
          <center>{pager}</center>
      ",
      'pager'=>[
        'maxButtonCount' => 5,
  			'linkContainerOptions' => ['class'=>'paginate_button page-item'],
  			'linkOptions' => ['class'=>'page-link'],
  			'prevPageCssClass' => 'previous',
  			'disabledPageCssClass' => 'disabled',
  			'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
      ]
  ]);
  ?>
  </div>
</section>
