<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequestsDiscussion */
?>
<div class="timeline-item">
<div class="mt-comment-body">
    <div class="mt-comment-info">
        <span class="mt-comment-author"><?= $model->createdBy->username?>:</span>
        <span class="mt-comment-date"><small><i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDateTime($model->updated_at)?></small></span>
    </div>
    <div class="mt-comment-text">
      <?= nl2br($model->comments)?>
      <?php if($model->reminder!=null){?>
      <br /><?php if($model->reminder>=date("Y-m-d") && $model->remarks==null){?><span class="badge badge-warning" style="width:20px; min-width:15px; display:inline-block"><i class="fa fa-exclamation"></i></span> <?php }?><span class="mt-comment-date"><small>Scheduled: <?= Yii::$app->formatter->asDateTime($model->reminder)?></small></span>
      <?php }?>
    </div>
    <?php if($model->remarks!=null){?>
    <div class="mt-comment-text"><span class="badge badge-success" style="width:20px; min-width:15px; display:inline-block"><i class="fa fa-check"></i></span> <?= nl2br($model->remarks)?></div>
    <?php
    }else{
      if(Yii::$app->controller->action->id!='update'){
    ?>
    <a href="javascript:;" class="btn btn-xs btn-primary btn-task" data-key="<?= $model->id?>" data-title="<?= (isset(Yii::$app->params['reminderTypeHelper'][$model->reminder_type]) ? Yii::$app->params['reminderTypeHelper'][$model->reminder_type].' ' : '').$model->forUser->username?>" data-descp="<?= $model->comments?>">Update</a>
  <?php }}?>
</div>

</div>
