<?php
use yii\helpers\Html;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\widgets\UsersTabStart;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserNoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Member:').' '.$model->fullname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
.filters{display:none;}
</style>
<div class="user-note-index">
  <?= UsersTabStart::header($model,$searchModel)?>
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomTabbedGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'showPerPage' => true,
      'options'=>['class'=>'grid-view'],
      'columns' => [
        ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']],
        'comments:html',
        'remarks',
        ['format'=>'raw','attribute'=>'reminder','value'=>function($model){
          $html='';
          if($model['reminder']>=date("Y-m-d") && $model['remarks']==null){
            $html.='<span class="badge grid-badge badge-warning" style="width:20px; min-width:15px; display:inline-block"><i class="fa fa-exclamation"></i></span> ';
          }
          $html.='<span class="mt-comment-date"><small>Scheduled: '.Yii::$app->formatter->asDateTime($model->reminder).'</small></span>';
          return $html;
        }],
        ['attribute'=>'created_by','value'=>function($model){
          return $model->createdBy->name;
        }],
        'updated_at:datetime',
        [
          'class' => 'yii\grid\ActionColumn',
          'header'=>'',
          'headerOptions'=>['class'=>'noprint','style'=>'width:80px;'],
          'contentOptions'=>['class'=>'noprint'],
          'template' => '{update}',
          'buttons' => [
            'update' => function ($url, $model) {
              if($model['remarks']==null || $model['remarks']==''){
                $reminderTypes=Yii::$app->helperFunctions->reminderTypes;
                return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Update'), 'javascript:;', [
                  'title' => Yii::t('app', 'Update'),
                  'class'=>'btn btn-xs btn-success btn-task',
                  'data-key'=>$model['id'],
                  'data-descp'=>$model['comments'],
                  'data-title'=>(isset($reminderTypes[$model->reminder_type]) ? $reminderTypes[$model->reminder_type].' ' : '').$model->forUser->username,
                  'data-pjax'=>"0",
                ]);
              }
            },
          ],
        ],
      ],
    ]);?>
    <?php CustomPjax::end(); ?>
  <?= UsersTabStart::footer()?>
</div>
<?= $this->render('/user-note/js/_scripts')?>
