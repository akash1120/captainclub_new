<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserRequests */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="reminder-form">
	<?php $form = ActiveForm::begin(['id'=>'reminder-form']); ?>
	<?= $form->field($model, 'remarks')->textInput() ?>
	<?= $form->field($model, 'ad_reminder')->checkbox(); ?>
	<section id="reminder-info" class="panel" style="display:none;">
		<header class="panel-heading">
			<h2 class="panel-title">Set Reminder</h2>
		</header>
		<div class="panel-body">
			<?= $form->field($model, 'comments')->textInput() ?>
			<div class="row">
				<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'reminder_type')->dropDownList(Yii::$app->helperFunctions->reminderTypes) ?></div>
				<div class="col-xs-12 col-sm-6"><?= $form->field($model, 'reminder')->textInput(['class'=>'form-control datetimepicker','autocomplete'=>'off']) ?></div>
			</div>
			<?= $form->field($model, 'staffIdz')->dropDownList(ArrayHelper::map(Yii::$app->appHelperFunctions->staffMembersList,'id','username'),['multiple'=>'multiple','class'=>'form-control userSelect2']) ?>
		</div>
	</section>
	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
