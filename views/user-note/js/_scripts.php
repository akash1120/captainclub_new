<?php
use yii\helpers\Url;
use app\assets\ReminderAssets;
ReminderAssets::register($this);

$this->registerJs('
	$(document).delegate("#usernoteupdateform-ad_reminder", "click", function() {
		if($("#usernoteupdateform-ad_reminder").prop("checked") == true){
			$("#reminder-info").show();
		}else{
			$("#reminder-info").hide();
		}
	});
	$(document).delegate(".btn-new-task", "click", function() {
		id=$(this).data("key");
		username=$(this).data("username");
		$.ajax({
			url: "'.Url::to(['user-task/create','id'=>'']).'"+id,
			dataType: "html",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html("Add Task/Reminder for "+username);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
				initTaskJs();
			},
			error: bbAlert
		});
	});
	$(document).delegate(".btn-task", "click", function() {
		id=$(this).data("key");
		noteTitle = $(this).data("title");
		noteDescp = $(this).data("descp");
		$.ajax({
			url: "'.Url::to(['user-task/update','id'=>'']).'"+id,
			dataType: "html",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html("Update "+noteTitle+"<br /><small>"+noteDescp+"</small>");
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
				initTaskJs();
			},
			error: bbAlert
		});
	});
	$("body").on("beforeSubmit", "form#reminder-form", function () {
		_targetContainer=$("#reminder-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
				return false;
		 }
		 // submit form
		 $.ajax({
				url: form.attr("action"),
				type: "post",
				data: form.serialize(),
				success: function (response) {
					if(response=="success"){
						swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Note saved successfully').'", type: "success"});
						window.closeModal();
					}else{
						swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
					}
					App.unblockUI($(_targetContainer));
					'.((Yii::$app->controller->id=='user-task' && Yii::$app->controller->action->id=='index') || (Yii::$app->controller->id=='user' && Yii::$app->controller->action->id=='index') ? '
					$.pjax.reload({container: "#grid-container", timeout: 2000});
					' : '
					calendar.refetchEvents();
					').'

				}
		 });
		 return false;
	});
');
?>
<script>
function initTaskJs()
{
	$(".datetimepicker").datetimepicker({
		format: "Y-m-d H:i",
	});
	$(".userSelect2").select2({
	  placeholder: "Search User",
	  allowClear: true,
	   width: "100%",
	});
}
</script>
