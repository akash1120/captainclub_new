<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$avatar=Yii::$app->fileHelperFunctions->getImagePath('user',$model['image'],'tiny');
//$licenseImage=Yii::$app->fileHelperFunctions->getImagePath('user_license',$model['license_image'],'tiny');
?>
<?php $form = ActiveForm::begin(['id'=>'basicInfoFrm','options'=>['enctype'=>'multipart/form-data']]); ?>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'firstname')->textInput() ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'lastname')->textInput() ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'membership_number')->textInput() ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'joining_date')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off']) ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<?= $form->field($model, 'image',['template'=>'
		{label}
		<div class="input-group file-input">
		<div class="input-group-prepend">
		<span class="input-group-text"><img src="'.$avatar.'" /></span>
		</div>
		<div class="custom-file">
		{input}
		<label class="custom-file-label" for="fldImg">Choose file</label>
		</div>
		</div>
		{error}{hint}
		'])->fileInput(['id'=>'fldImg','class'=>'custom-file-input','accept'=>'image/*']) ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Cancel', 'javascript:;', ['class' => 'btn btn-default', 'data-dismiss'=>'modal']) ?>
	</div>
</div>
<?php ActiveForm::end(); ?>
<?php if(Yii::$app->menuHelperFunction->checkActionAllowed('create','user-license')){?>
<hr />
<div class="row">
	<div class="col-sm-6">
		<h3>License Info</h3>
	</div>
  <div class="col-sm-6 text-right">
    <a href="javascript:;" class="btn btn-success btn-xs load-modal" data-mid="general-modal-secondary" data-url="<?= Url::to(['user-license/create','user_id'=>$model->id])?>" data-heading=" <?= $model->fullname?> - Add License"><i class="fa fa-plus"></i></a>
  </div>
</div>
<style>
#license-grid-container .filters{display: none;}
h3{margin: 0px;}
</style>
<?php CustomPjax::begin(['id'=>'license-grid-container']); ?>
<?= $this->render('/user-license/_gridview',[
	'searchModel'=>$licenseSearchModel,
	'dataProvider'=>$licenseDataProvider,
	'model'=>$model
])?>
<?php CustomPjax::end(); ?>
<?php }?>
