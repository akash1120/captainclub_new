<?php
use yii\helpers\Url;
//Script for Edit Booking Comments
$this->registerJs('
	$(document).delegate(".btn-deposit", "click", function() {
		id=$(this).data("userid");
		username=$(this).data("username");
		$.ajax({
			url: "'.Url::to(['user/security-deposit','id'=>'']).'"+id,
			dataType: "html",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html("Update deposit for "+username);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
			},
			error: bbAlert
		});
	});
	$("body").on("beforeSubmit", "form#securityDepositForm", function () {
		_targetContainer=$("#securityDepositForm")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
				return false;
		 }
		 // submit form
		 $.ajax({
				url: form.attr("action"),
				type: "post",
				data: form.serialize(),
				success: function (response) {
					if(response=="success"){
						swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Deposit saved successfully').'", type: "success"});
						window.closeModal();
						$.pjax.reload({container: "#grid-container", timeout: 2000});
					}else{
						swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
					}
					App.unblockUI($(_targetContainer));
				}
		 });
		 return false;
	});
');
?>
