<?php
use yii\helpers\Url;
$this->registerJs('
$(document).delegate(".btn-del-hold", "click", function() {
  _this=$(this);
  recId=_this.data("id");
  userId=_this.data("user_id");
  var _targetContainer=".grid-view";
  swal({
    title: "Confirmation",
    html: "'.Yii::t('app','Are you sure you want to delete it?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Confirm').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      if(recId!="" && recId!=undefined){
        $.ajax({
          url: "'.Url::to(['contract/del-hold','user_id'=>'']).'"+userId+"&id="+recId,
          type: "POST",
          dataType: "json",
          success: function(response) {
            App.unblockUI($(_targetContainer));
            if(response["success"]){
              swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
              $.pjax.reload({container: "#grid-container", timeout: 2000});
            }else{
              swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
            }
          },
          error: bbAlert
        });
      }
    }
  });
});
$(document).delegate(".btn-del-cancel", "click", function() {
  _this=$(this);
  recId=_this.data("id");
  userId=_this.data("user_id");
  var _targetContainer=".grid-view";
  swal({
    title: "Confirmation",
    html: "'.Yii::t('app','Are you sure you want to delete it?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Confirm').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      if(recId!="" && recId!=undefined){
        $.ajax({
          url: "'.Url::to(['contract/del-cancel','user_id'=>'']).'"+userId+"&id="+recId,
          type: "POST",
          dataType: "json",
          success: function(response) {
            App.unblockUI($(_targetContainer));
            if(response["success"]){
              swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success", timer: 5000});
              $.pjax.reload({container: "#grid-container", timeout: 2000});
            }else{
              swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
            }
          },
          error: bbAlert
        });
      }
    }
  });
});
');
?>
