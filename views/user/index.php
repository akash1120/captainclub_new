<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\assets\UserListAssets;
UserListAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$mailModel->email_from=Yii::$app->params['supportEmail'];

$this->title = Yii::t('app', 'Members');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
//$actionBtns.='{view}';
if(Yii::$app->menuHelperFunction->checkActionAllowed('member-bookings','booking')){
  $actionBtns.='{bookings}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('renew','contract')){
  $actionBtns.='{renew}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('index','contract')){
  $actionBtns.='{contract-history}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('hold','contract')){
  $actionBtns.='{hold}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('resume','contract')){
  $actionBtns.='{resume}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('cancel','contract')){
  $actionBtns.='{cancel}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('create','user-task')){
  $actionBtns.='{task}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('credit-log','user-account')){
  $actionBtns.='{credit-log}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('account-statement','user-account')){
  $actionBtns.='{account-statement}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('bookings','user')){
  $actionBtns.='{user-bookings}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('requests','user')){
  $actionBtns.='{user-requests}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('index','user-feedback')){
  $actionBtns.='{user-feedbacks}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('create','user-feedback')){
  $actionBtns.='{feedback}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('security-deposit')){
  //$actionBtns.='{security-deposit}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('request')){
  $actionBtns.='{request}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('reset-user-password')){
  $actionBtns.='{reset-user-password}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}

$mailBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('mail')){
  $mailBtn=true;
}

$this->registerJs('
$(document).delegate(".mark-dead", "click", function() {
  _this=$(this);
  recId=_this.parents("tr").data("key");
  var memberName = _this.data("username");
  var _targetContainer=".swal2-modal";
  sweetAlert({
    title: "'.Yii::t('app','Are you sure you want to mark \""+memberName+"\" as dead?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Confirm').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      if(recId!="" && recId!=undefined){
        $.ajax({
          url: "'.Url::to(['mark-dead','id'=>'']).'"+recId,
          type: "POST",
          success: function(response) {
            App.unblockUI($(_targetContainer));
            if("success"){
              swal({title: "Marked", text: "Member marked as dead", type: "success", timer: 5000});
            }else{
              swal({title: "Error", text: reponse, type: "error", timer: 5000});
            }
            $.pjax.reload({container: "#grid-container", timeout: 2000});
          },
          error: bbAlert
        });
      }
    }
  });
});
$(document).delegate(".btn-cancel", "click", function() {
  id=$(this).data("key");
  username=$(this).data("username");
  swal({
    title: "'.Yii::t('app','Confirm').'",
    html: "'.Yii::t('app','Are you sure you want to cancel this?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Yes, Cancel it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: "'.Url::to(['contract/cancel','id'=>'']).'"+id,
        dataType: "html",
        success: function(data) {
          $("#general-modal").find("h5.modal-title").html("Cancel "+username);
          $("#general-modal").find(".modalContent").html(data);
          $("#general-modal").modal();
          $(".dtpicker").datepicker({
            format: "yyyy-mm-dd",
          }).on("changeDate", function(e){
            $(this).datepicker("hide");
          });
          App.unblockUI($(_targetContainer));
        },
        error: bbAlert
      });
    }
  });
});
$(document).delegate(".btn-hold", "click", function() {
  id=$(this).data("key");
  username=$(this).data("username");
  swal({
    title: "'.Yii::t('app','Confirm').'",
    html: "'.Yii::t('app','Are you sure you want to Hold this?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Yes, Hold it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: "'.Url::to(['contract/hold','id'=>'']).'"+id,
        dataType: "html",
        success: function(data) {
          $("#general-modal").find("h5.modal-title").html("Hold "+username);
          $("#general-modal").find(".modalContent").html(data);
          $("#general-modal").modal();
          $(".dtpicker").datepicker({
            format: "yyyy-mm-dd",
          }).on("changeDate", function(e){
            $(this).datepicker("hide");
          });
          App.unblockUI($(_targetContainer));
        },
        error: bbAlert
      });
    }
  });
});
$(document).delegate(".btn-resume", "click", function() {
  id=$(this).data("key");
  username=$(this).data("username");
  swal({
    title: "'.Yii::t('app','Confirm').'",
    html: "'.Yii::t('app','Are you sure you want to Resume this?').'",
    type: "info",
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Yes, Resume it!').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.value) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: _targetContainer,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: "'.Url::to(['contract/resume','id'=>'']).'"+id,
        dataType: "html",
        success: function(data) {
          if(data=="resumed"){
            swal({title: "'.Yii::t('app','Resumed').'", html: "'.Yii::t('app','Membership resumed successfully').'", type: "success"});
          }
          $.pjax.reload({container: "#grid-container", timeout: 2000});
          App.unblockUI($(_targetContainer));
        },
        error: bbAlert
      });
    }
  });
});
$(document).delegate(".request-for-member", "click", function() {
  id=$(this).data("userid");
  name=$(this).data("name");
  $.ajax({
    url: "'.Url::to(['user/request','id'=>'']).'"+id,
    dataType: "html",
    success: function(data) {
      $("#reqHtml").html(data);
      $("#request-modal h5.modal-title").html("Request on behalf of "+name);
      $("#request-modal").modal();

    },
    error: bbAlert
  });
});
$("body").on("beforeSubmit", "form#membership-status-form", function () {
  _targetContainer=$("#membership-status-form")
  App.blockUI({
    message: "'.Yii::t('app','Please wait...').'",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });

  var form = $(this);
  // return false if form still have some validation errors
  if (form.find(".has-error").length) {
    return false;
  }
  // submit form
  $.ajax({
    url: form.attr("action"),
    type: "post",
    data: form.serialize(),
    success: function (response) {
      if(response=="holded"){
        swal({title: "'.Yii::t('app','Success').'", html: "'.Yii::t('app','Member\'s hold period saved successfully').'", type: "success"});
        window.closeModal();
      }else if(response=="canceled"){
        swal({title: "'.Yii::t('app','Success').'", html: "'.Yii::t('app','Member\'s cancel period saved successfully').'", type: "success"});
        window.closeModal();
      }else{
        swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
      }
      App.unblockUI($(_targetContainer));
      $.pjax.reload({container: "#grid-container", timeout: 2000});
    }
  });
  return false;
});
$("body").on("beforeSubmit", "form#userResetPassForm", function () {
  _targetContainer=$("#userResetPassForm")
  App.blockUI({
    message: "'.Yii::t('app','Please wait...').'",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });

  var form = $(this);
  // return false if form still have some validation errors
  if (form.find(".has-error").length) {
    return false;
  }
  // submit form
  $.ajax({
    url: form.attr("action"),
    type: "post",
    data: form.serialize(),
    success: function (response) {
      response = jQuery.parseJSON(response);
      if(response["success"]){
        swal({title: response["success"]["heading"], html: response["success"]["msg"], type: "success"});
        window.closeModal();
      }else{
        swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error"});
      }
      App.unblockUI($(_targetContainer));
    }
  });
  return false;
});

$(document).delegate("#btn-mailing", "click", function() {
  $(this).hide();
  $("#mail-form-area").slideDown();
  tinymce.init({
  	selector:".textarea",
  	menubar:false,
  	paste_as_text: true,
  	plugins: [
  		"advlist autolink lists link image charmap print preview anchor textcolor",
  		"searchreplace visualblocks code fullscreen",
  		"insertdatetime media table paste"
  	],
  	toolbar: "insertfile undo redo | paste | bold italic | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
  });
  initDropZone();
  $("#newsletter-ccadminid").select2({
    placeholder: "'.$mailModel->getAttributeLabel('ccAdminId').'",
    allowClear: true
  });
});
$(document).delegate(".btn-cancel-mail", "click", function() {
  $("#mail-form-area").hide();
  $("#btn-mailing").show();
});
initScripts();
$(document).on("pjax:success", function() {
  initScripts();
});
')
?>
<style>
.filters{display:none;}
.grid-view .card-header .btn{margin:0 5px;}
.table.table-responsive-lg{min-height: 400px;}
.grid-badge.status-badge{max-width: 140px;white-space: pre-line;line-height: 1.3}
</style>
<div id="reqHtml">
</div>
<div class="user-index">
  <section class="card mb-1">
    <div class="card-body filter-padding">
      <?php $form = ActiveForm::begin(['action'=>Url::to(['user/index']),'method'=>'get','options'=>['data-pjax' => '']]); ?>
      <div class="row top-filter-row">
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'keyword')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'credit_balance')->dropDownList(['+'=>'+','-'=>'-'],['prompt'=>'Select']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'is_licensed')->dropDownList(['1'=>'Yes','0'=>'No'],['prompt'=>'Select']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'package_name')->dropDownList(Yii::$app->appHelperFunctions->packageListArr,['class'=>'form-control selectpicker','multiple'=>'multiple']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'city_name')->dropDownList(Yii::$app->appHelperFunctions->cityListArr,['class'=>'form-control selectpicker','multiple'=>'multiple']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'status')->dropDownList(Yii::$app->helperFunctions->arrUserStatus,['prompt'=>'Select']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'tasks')->dropDownList(['1'=>'No Task','2'=>'Pending','3'=>'Future'],['prompt'=>'Select']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <?= $form->field($searchModel, 'expiry_month')->dropDownList(Yii::$app->helperFunctions->expiryFilters,['prompt'=>'Select']) ?>
        </div>
        <div class="col-xs-12 col-sm-2">
          <div class="form-group">
            <label class="control-label">&nbsp;</label>
            <?= Html::submitButton('<i class="fas fa-search"></i> '.Yii::t('app', 'Search'), ['class' => 'btn btn-success btn-block']) ?>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </section>
  <div id="mail-form-area"<?= $mailModel->hasErrors() ? '' : ' style="display:none"'?>>
    <section class="card card-featured card-featured-info mb-4 mt-4">
      <header class="card-header">
        <h2 class="card-title">Send Mail</h2>
      </header>
      <div class="card-body bg-i1nfo">
        <?php $form = ActiveForm::begin(); ?>
        <div class="hidden">
          <?= $form->field($mailModel, 'username')->textInput() ?>
          <?= $form->field($mailModel, 'keyword')->textInput() ?>
          <?= $form->field($mailModel, 'credit_balance')->textInput() ?>
          <?= $form->field($mailModel, 'is_licensed')->textInput() ?>
          <?= $form->field($mailModel, 'package_idz')->textInput() ?>
          <?= $form->field($mailModel, 'city_idz')->textInput() ?>
          <?= $form->field($mailModel, 'status')->textInput() ?>
          <?= $form->field($mailModel, 'tasks')->textInput() ?>
          <?= $form->field($mailModel, 'expiry_month')->textInput() ?>
        </div>
        <?= $form->field($mailModel, 'email_from')->textInput() ?>
        <?= $form->field($mailModel, 'ccAdminId')->dropDownList(ArrayHelper::map(Yii::$app->appHelperFunctions->staffMembersList,'id','username'),['multiple'=>'multiple']) ?>
        <?= $form->field($mailModel, 'subject')->textInput() ?>
        <?= $form->field($mailModel, 'message')->textarea(['class'=>'form-control textarea', 'rows'=>12]) ?>
        <div id="hidden-files" class="hidden"></div>

        <div id="dZU1pload" class="dropzone_my">
          <div class="hidden">
            <div id="template" class="dz-preview dz-file-preview dz-processing dz-success dz-complete filesbox" style="width:100%; line-height:none; margin:0px;">
              <div style="width:94%; margin:0px 3%;">
                <table width="100%" class="table table-striped table-bordered table-hover" style="margin-bottom:0px;">
                  <td width="48%">
                    <div class="dz-file-caption-container">
                      <input type="text" class="form-control" placeholder="<?= Yii::t('app','Caption')?>" name="caption" onkeyup="javascript:copyTitle(this);" onblur="javascript:copyTitle(this);" />
                      <div class="dz-filename hidden"><span data-dz-name></span></div>
                    </div>
                  </td>
                  <td width="20%" style="max-width:20%;">
                    <div class="dz-filename"><span data-dz-name></span></div>
                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                    <div class="dz-error-message"><span data-dz-errormessage></span></div>
                    <div class="dz-success-mark"></div>
                    <div class="dz-error-mark"></div>
                  </td>
                  <td width="18%" align="center"> <div class="dz-size"><span data-dz-size></span></div></td>
                  <td width="12%" align="center">
                    <a class="btn btn-xs btn-orange dz-remove" href="javascript:undefined;" data-dz-remove="" style="font-size:12px;">
                      <?= Yii::t('app','Remove')?>
                    </a>
                  </td>
                </table>
              </div>
            </div>
          </div>
          <div class="dz-default dz-message">
            <?= Yii::t('app','Drop attachments here or click to upload.')?><br/>
            <small>(png,jpg,jpeg,gif,pdf,doc,docx,xls,xlsx)</small>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-success btn-default']) ?>
            <?= Html::resetButton('Cancel', ['class' => 'btn btn-default btn-cancel-mail']) ?>
          </div>
        </div>
        <?php ActiveForm::end(); ?>
      </div>
    </section>
  </div>
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'mailBtn' => $mailBtn,
    'showPerPage' => true,
    'options'=>['class'=>'grid-view fixed-header1'],
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']],
      ['format' => ['image',['width'=>'40','height'=>'40']],'attribute'=>'image','value'=>function($model){
        return Yii::$app->fileHelperFunctions->getImagePath('user',$model['image'],'small');
      },'headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']],
      ['format'=>'raw','attribute'=>'username','value'=>function($model){
        $html='';
        $html.=$model['email'].(trim($model['name'])!='' ? '<div><span class="badge grid-badge badge-info">Name: '.$model['name'].'</span></div>' : '');
        $html.=$model['username']!='' ? '<div><span class="badge grid-badge badge-info">Old Username: '.$model['username'].'</span></div>' : '';
        return $html;
      },'headerOptions'=>['class'=>'noprint fxdBig'],'contentOptions'=>['class'=>'fxdBig']],
      ['attribute'=>'security_deposit','value'=>function($model){
        return $model['security_deposit'];
      },'headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2']],
      ['format'=>'raw','attribute'=>'credit_balance','value'=>function($model){
        $credit_balance = $model['credit_balance'];
        return '<small><span class="badge grid-badge badge-'.($credit_balance<0 ? 'danger' : 'success').'">Credit: AED '.$credit_balance.'</span></small>';
      },'filter'=>['+'=>'+','-'=>'-']],

      ['attribute'=>'is_licensed','value'=>function($model){
        return Yii::$app->helperFunctions->arrYesNo[$model['is_licensed']];
      },'headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2'],'filter'=>Yii::$app->helperFunctions->arrYesNo],
      //['attribute'=>'email','headerOptions'=>['class'=>'noprint fxdBig'],'contentOptions'=>['class'=>'fxdBig']],
      'mobile',
      ['attribute'=>'city_name','headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2'],'filter'=>Yii::$app->appHelperFunctions->cityListArr],
      ['attribute'=>'package_name','filter'=>Yii::$app->appHelperFunctions->packageListArr],
      ['format'=>'date','attribute'=>'end_date','value'=>function($model){
        $user=User::findOne($model['id']);
        return $user->maxPackageEndDate;
      }],
      ['format'=>'raw','label'=>'Tasks','attribute'=>'tasks','value'=>function($model){
        $html = '';
        $taskCount=Yii::$app->statsFunctions->getUserTaskCount($model['id']);
        $futureCount=Yii::$app->statsFunctions->getUserFutureTaskCount($model['id']);
        $pendingCount=Yii::$app->statsFunctions->getUserPendingTaskCount($model['id']);
        if($taskCount==0){
          $html = '<a href="'.Url::to(['user-task/index','id'=>$model['id'],'listType'=>'all']).'" target="_blank" data-pjax="0" class="btn btn-xs btn-default">Tasks</a>';
        }
        if($pendingCount>0){
          $html = '<a href="'.Url::to(['user-task/index','id'=>$model['id'],'listType'=>'pending']).'" target="_blank" data-pjax="0" class="btn btn-xs btn-danger">Tasks</a>';
        }

        if($futureCount>0){
          $html = '<a href="'.Url::to(['user-task/index','id'=>$model['id'],'listType'=>'future']).'" target="_blank" data-pjax="0" class="btn btn-xs btn-success">Tasks</a>';
        }

        if($futureCount>0 && $pendingCount>0){
          $html = '
          <a href="'.Url::to(['user-task/index','id'=>$model['id'],'listType'=>'all']).'" target="_blank" data-pjax="0" class="btn btn-xs btn-info">
          <span class="badge grid-badge badge-danger">'.$pendingCount.'</span>
          <span class="badge grid-badge badge-success">'.$futureCount.'</span><br />
          Tasks
          </a>';
        }
        return $html;
      },'headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2'],'filter'=>['1'=>'No Task','2'=>'Pending','3'=>'Future']],
      ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
        return Yii::$app->helperFunctions->getUserStatus($model['id'],$model['status'],$model);
      },'headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2'],'filter'=>Yii::$app->helperFunctions->arrUserStatus],
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint fxd1'],
        'contentOptions'=>['class'=>'noprint fxd1 actions'],
        'template' => '
        <div class="btn-group flex-wrap">
        <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
        </button>
        <div class="dropdown-menu" role="menu">
        '.$actionBtns.'
        </div>
        </div>',
        'buttons' => [
          'view' => function ($url, $model) {
            return Html::a('<i class="fas fa-table"></i> '.$model['username'].' '.$model['name'], 'javascript:;', [
              'title' => Yii::t('app', 'View'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
            ]);
          },
          'bookings' => function ($url, $model) {
            return Html::a('<i class="fas fa-anchor"></i> '.Yii::t('app', 'Bookings'), $url, [
              'title' => Yii::t('app', 'Bookings'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
              'target'=>'_blank',
            ]);
          },
          'renew' => function ($url, $model) {
            if($model['status']!=2){
              $contractType='Create New Contract';
              $userContractCount=Yii::$app->statsFunctions->getUserContractCount($model['id']);
              if($userContractCount>0){
                $contractType='Renew';
              }
              return Html::a('<i class="fas fa-folder-plus"></i> '.$contractType, 'javascript:;', [
                'title' => $contractType,
                'class'=>'dropdown-item text-1 btn-renew',
                'data-key'=>$model['id'],
                'data-heading'=>$contractType.' - '.$model['username'].($model['name']!='' ? ' - '.$model['name'] : ''),
                'data-pjax'=>"0",
              ]);
            }
          },
          'hold' => function ($url, $model) {
            if($model['status']==1){
              return Html::a('<i class="fas fa-pause-circle"></i> '.Yii::t('app', 'Hold'), 'javascript:;', [
                'title' => Yii::t('app', 'Hold'),
                'class'=>'dropdown-item text-1 btn-hold',
                'data-key'=>$model['id'],
                'data-username'=>$model['username'].($model['name']!='' ? ' - '.$model['name'] : ''),
              ]);
            }
          },
          'resume' => function ($url, $model) {
            if($model['status']==2){
              return Html::a('<i class="fas fa-play-circle"></i> '.Yii::t('app', 'Resume'), 'javascript:;', [
                'title' => Yii::t('app', 'Resume'),
                'class'=>'dropdown-item text-1 btn-resume',
                'data-key'=>$model['id'],
                'data-username'=>$model['username'].($model['name']!='' ? ' - '.$model['name'] : ''),
              ]);
            }
          },
          'cancel' => function ($url, $model) {
            if($model['status']==1 || $model['status']==2 || $model['status']==20){
              return Html::a('<i class="fas fa-ban"></i> '.Yii::t('app', 'Cancel'), 'javascript:;', [
                'title' => Yii::t('app', 'Cancel'),
                'class'=>'dropdown-item text-1 btn-cancel',
                'data-key'=>$model['id'],
                'data-username'=>$model['username'].($model['name']!='' ? ' - '.$model['name'] : ''),
              ]);
            }
          },
          'contract-history' => function ($url, $model) {
            return Html::a('<i class="fas fa-history"></i> '.Yii::t('app', 'Contract History'), ['contract/index','id'=>$model['id']], [
              'title' => Yii::t('app', 'Contract History'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
              'target'=>'_blank',
            ]);
          },
          'task' => function ($url, $model) {
            return Html::a('<i class="fas fa-tasks"></i> '.Yii::t('app', 'Add Task'), 'javascript:;', [
              'title' => Yii::t('app', 'Add Task'),
              'class'=>'dropdown-item text-1 btn-new-task',
              'data-key'=>$model['id'],
              'data-username'=>$model['username'].($model['name']!='' ? ' - '.$model['name'] : ''),
              'data-pjax'=>"0",
            ]);
          },
          'credit-log' => function ($url, $model) {
            return Html::a('<i class="fas fa-clipboard-list"></i> '.Yii::t('app', 'Old Credit Log'), ['user-account/credit-log','id'=>$model['id']], [
              'title' => Yii::t('app', 'Old Credit Log'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
              'target'=>'_blank',
            ]);
          },
          'account-statement' => function ($url, $model) {
            return Html::a('<i class="fas fa-file-invoice"></i> '.Yii::t('app', 'Account Statement'), ['user-account/account-statement','id'=>$model['id']], [
              'title' => Yii::t('app', 'Account Statement'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
              'target'=>'_blank',
            ]);
          },
          'user-bookings' => function ($url, $model) {
            return Html::a('<i class="fas fa-anchor"></i> '.Yii::t('app', 'Member Bookings'), ['user/bookings','id'=>$model['id']], [
              'title' => Yii::t('app', 'Bookings'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
              'target'=>'_blank',
            ]);
          },
          'user-requests' => function ($url, $model) {
            return Html::a('<i class="fas fa-envelope"></i> '.Yii::t('app', 'Member Requests'), ['user/requests','id'=>$model['id']], [
              'title' => Yii::t('app', 'Member Requests'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
              'target'=>'_blank',
            ]);
          },
          'user-feedbacks' => function ($url, $model) {
            return Html::a('<i class="fas fa-comments"></i> '.Yii::t('app', 'Member Feedbacks'), ['user-feedback/index','id'=>$model['id']], [
              'title' => Yii::t('app', 'Member Feedbacks'),
              'class'=>'dropdown-item text-1',
              'data-pjax'=>"0",
              'target'=>'_blank',
            ]);
          },
          'feedback' => function ($url, $model) {
            return Html::a('<i class="fas fa-comment-dots"></i> Add Feedback', 'javascript:;', [
              'title' => Yii::t('app', 'Add Feedback'),
              'data-userid'=>$model['id'],
              'data-username'=>$model['username'].($model['name']!='' ? ' - '.$model['name'] : ''),
              'class'=>'dropdown-item text-1 btn-feedback',
              'data-pjax'=>"0",
            ]);
          },
          'security-deposit' => function ($url, $model) {
            return ;
            /*return Html::a('<i class="fas fa-money-bill-wave"></i> Security Deposit', 'javascript:;', [
            'title' => Yii::t('app', 'Security Deposit'),
            'data-userid'=>$model['id'],
            'data-username'=>$model['username'].' '.$model['name'],
            'class'=>'dropdown-item text-1 btn-deposit',
            'data-pjax'=>"0",
          ]);*/
        },
        'update' => function ($url, $model) {
          return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Update'), $url, [
            'title' => Yii::t('app', 'Update'),
            'class'=>'dropdown-item text-1',
            'data-pjax'=>"0",
          ]);
        },
        'reset-user-password' => function ($url, $model) {
          $name=$model['name'];
          if($name=='')$name=$model['username'];
          return Html::a('<i class="fas fa-lock"></i> '.Yii::t('app', 'Reset Password'), 'javascript:;', [
            'title' => Yii::t('app', 'Reset Password'),
            'class'=>'dropdown-item text-1 load-modal',
            'data-heading'=>'Reset Password for User - '.$name,
            'data-url'=>Url::to(['reset-user-password','id'=>$model['id']]),
            'data-pjax'=>"0",
          ]);
        },
        'request' => function ($url, $model) {
          return Html::a('<i class="fas fa-mail-bulk"></i> '.Yii::t('app', 'Request'), 'javascript:;', [
            'title' => Yii::t('app', 'Request'),
            'data-userid'=>$model['id'],
            'data-name'=>$model['username'].($model['name']!='' ? ' - '.$model['name'] : ''),
            'class'=>'dropdown-item text-1 request-for-member',
            'data-pjax'=>"0",
          ]);
        },
        'status' => function ($url, $model) {
          if($model['status']==1){
            return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
              'title' => Yii::t('app', 'Disable'),
              'class'=>'dropdown-item text-1',
              'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
              'data-method'=>"post",
            ]);
          }else{
            return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
              'title' => Yii::t('app', 'Enable'),
              'class'=>'dropdown-item text-1',
              'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
              'data-method'=>"post",
            ]);
          }
        },
        'delete' => function ($url, $model) {
          return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
            'title' => Yii::t('app', 'Delete'),
            'class'=>'dropdown-item text-1',
            'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
            'data-method'=>"post",
            'data-pjax'=>"0",
          ]);
        },
      ],
    ],
  ],
]);?>
<?php CustomPjax::end(); ?>
</div>
<script>
function showDetail(){}
function initScripts(){
  $('[data-toggle=\"popover\"]').popover();
  $(".selectpicker").selectpicker();
  initDropZone();
}
function copyTitle(el)
{
  el=$(el);
  key=el.parent(".dz-file-caption-container").find(".dz-filename > span").text();

  $("input[data-file-key=\""+key+"\"]").val(el.val());
}
function initDropZone()
{
  var previewNode = document.querySelector("#template");
  previewNode.id = "";
  var previewTemplate = previewNode.parentNode.innerHTML;

  var mydropzone;
  Dropzone.autoDiscover = false;
  $("#dZU1pload").dropzone({
    url: "<?= Url::to(['site/drop-upload'])?>",
    previewTemplate: previewTemplate,
    addRemoveLinks: false,
    acceptedFiles: ".png,.jpg,.jpeg,.gif,.pdf,.doc,.docx,.xls,.xlsx",
    init: function() {
      mydropzone=this;
    },
    success: function (file, response) {
      var fileName = response;
      file.previewElement.classList.add("dz-success");
      $("#hidden-files").append("<input type=\"hidden\" name=\"Newsletter[attachmentFile][]\" class=\"form-control\" value=\""+fileName+"\" />");
      $("#hidden-files").append("<input type=\"hidden\" name=\"Newsletter[attachmentFileCaption][]\" class=\"form-control\" data-file-key=\""+fileName+"\" value=\"\" />");

      var removeButton = Dropzone.createElement("<i class=\"glyphicon glyphicon-trash text-danger\"></i>");
      removeButton.addEventListener("click", function(e) {
        e.preventDefault();
        e.stopPropagation();
        _this.removeFile(file);
        // If you want to the delete the file on the server as well, you can do the AJAX request here.
      });
    },
    removedfile: function(file){
      $("input[value=\""+file.name+"\"]").remove();
      $("input[data-file-key=\""+file.name+"\"]").remove();
      $(document).find(file.previewElement).remove();
    },
    error: function (file, response) {
      file.previewElement.querySelector("[data-dz-errormessage]").textContent = response;
      file.previewElement.classList.add("dz-error");
    }
  });
}
</script>
<?= $this->render('/user-feedback/js/feedback_scripts')?>
<?= $this->render('/booking/js/list_script')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
<?= $this->render('/user/js/list_script')?>
<?= $this->render('/request/js/night_drive_training_request_scripts')?>
<?= $this->render('/request/js/selection_scripts')?>
<?= $this->render('/booking/js/alert_script')?>
<?= '';//$this->render('/user/js/deposit_scripts')?>
<?= $this->render('/user-note/js/_scripts')?>
<?= $this->render('/contract/js/_renew_scripts')?>
<?= $this->render('/request/js/freeze_scripts')?>
<?= $this->render('/request/js/boat_booking_scripts')?>
