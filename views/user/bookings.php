<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\widgets\UsersTabStart;
use app\assets\MemberBookingListAssets;
MemberBookingListAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title =$model->fullname.' - ';

if($searchModel->listType=='future'){
  $this->title .= Yii::t('app', 'Active Bookings');
}elseif($searchModel->listType=='history'){
  $this->title .= Yii::t('app', 'Booking History');
}elseif($searchModel->listType=='deleted'){
  $this->title .= Yii::t('app', 'Deleted Bookings');
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtnLink=[];
$createBtn=false;
if($searchModel->listType=='future' && Yii::$app->menuHelperFunction->checkActionAllowed('new-user-booking','booking')){
  $createBtn=true;
  $createBtnLink=['booking/new-user-booking','user_id'=>$model->id];
}

if($searchModel->city_id!=null){
  $marinaList=Yii::$app->appHelperFunctions->getActiveCityMarinaListArr($searchModel->city_id);
}else{
  $marinaList=Yii::$app->appHelperFunctions->activeMarinaListArr;
}

$boatsList=Yii::$app->appHelperFunctions->getActiveBoatsListArr($searchModel->city_id,$searchModel->port_id);

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'html','attribute'=>'member_name','label'=>'Member','value' => function ($model){
  return $model->member->fullname.'<br /><span class="badge badge-info">'.Yii::$app->formatter->asDateTime($model['created_at']).'</span>'.($model['trashed']==1 ? '<br /><span class="badge badge-danger">'.Yii::$app->formatter->asDateTime($model['trashed_at']).'</span>' : '');
}];
$columns[]=['format'=>'html','attribute'=>'city_id','label'=>'City','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');},'filter'=>Yii::$app->appHelperFunctions->activeCityListArr];
$columns[]=['format'=>'html','attribute'=>'port_id','label'=>'Marina','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');},'filter'=>$marinaList];
$columns[]=['format'=>'html','attribute'=>'boat_id','label'=>'Boat','value' => function ($model){return ($model->boat!=null ? $model->boat->name : 'Not set');},'filter'=>$boatsList];
$columns[]=['format'=>'date','attribute'=>'booking_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]=['format'=>'html','attribute'=>'booking_time_slot','label'=>'Time','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');},'filter'=>Yii::$app->appHelperFunctions->timeSlotListArr];
if(Yii::$app->user->identity->user_type!=0){
  $columns[]=['format'=>'raw','attribute'=>'booking_type','label'=>'Remarks','value'=>function($model){return $model->remarks;},'filter'=>Yii::$app->helperFunctions->adminBookingTypes];
}
$columns[]=['format'=>'raw','label'=>'Addons','value' => function ($model){return $model->addonsMenu;},'contentOptions'=>['class'=>'nosd']];
if($searchModel->listType!='deleted'){
$columns[] = ['format'=>'html','label'=>'Status','attribute'=>'status','value'=>function($model){return '<span class="badge badge-'.($model->status==1 ? 'success' : 'dark').'">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>';},'filter'=>false];
}
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'headerOptions'=>['class'=>'noprint','style'=>'width:75px;'],
  'contentOptions'=>['class'=>'noprint'],
  'template' => '{booking-activity} {delete}',
  'buttons' => [
    'booking-activity' => function ($url, $model) use ($searchModel){
      if($searchModel->listType=='history'){
      //if(Yii::$app->menuHelperFunction->checkActionAllowed('activity-detail','booking')){
        return Html::a('<i class="fa fa-list"></i> View Activity', 'javascript:;', [
          'title' => Yii::t('app', 'View Activity'),
          'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-info load-modal',
          'data-heading'=>'Booking Activity',
          'data-url'=>Url::to(['booking/activity-detail','id'=>$model->id]),
        ]);
      //}
      }
    },
    'delete' => function ($url, $model) use ($searchModel){
      if($searchModel->listType=='future'){
        if(date("Y-m-d",strtotime($model->booking_date))>=date("Y-m-d")){
          return Html::a('<i class="fa fa-trash"></i> Delete', 'javascript:;', [
            'title' => Yii::t('app', 'Delete'),
            'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-danger',
            'onclick'=>'javascript:deleteBooking('.$model->id.');',
            'data-id'=>$model->id,
            'data-method'=>"post",
          ]);
        }
      }
    },
  ],
];
?>
<style>
.dropdown-item.text-1.highlightIt{background-color:#e6e6e6;color:#000;font-weight:bold;}
</style>
<div class="city-index">
  <?= UsersTabStart::header($model,$searchModel)?>
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <div class="btn-group flex-wrap">
		<button type="button" class="mb-1 mt-1 mr-1 btn btn-default dropdown-toggle" data-toggle="dropdown">
      <?= $contract->package->name?> (<small><strong>From:</strong> <?= Yii::$app->formatter->asDate($contract->start_date);?> | <strong>To:</strong> <?= Yii::$app->formatter->asDate($contract->end_date);?></small>)
      <span class="caret"></span>
    </button>
		<div class="dropdown-menu" role="menu">
      <?php
      $memberContracts=$model->lastFiveContracts;
      if($memberContracts!=null){
        foreach($memberContracts as $memberContract){
          $contractTitle=$memberContract['package_name'].' (<small><strong>From:</strong> '.Yii::$app->formatter->asDate($memberContract['start_date']).' | <strong>To:</strong> '.Yii::$app->formatter->asDate($memberContract['end_date']).'</small>)';
          echo Html::a($contractTitle,['user/bookings','id'=>$model->id,'contract_id'=>$memberContract['id'],'listType'=>$searchModel->listType],['class'=>'dropdown-item text-1'.($searchModel->contract_id==$memberContract['id'] ? ' highlightIt' : '')]);
        }
      }
      ?>
		</div>
	</div>
  <?php if($searchModel->listType=='bookingstats'){?>
    <?= $this->render('booking_stats',['user'=>$model,'searchModel'=>$searchModel,'contract'=>$contract])?>
  <?php }else{?>
  <?= CustomTabbedGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'createBtnLink' => $createBtnLink,
    'createInPopUp' => true,
    'popUpHeading' => 'New Member Booking',
    'rowOptions' => function ($model, $index, $widget, $grid){
      return ['class'=>'item-row','data-city_id'=>$model->city_id, 'data-marina_id'=>$model->port_id];
    },
    'columns' => $columns,
  ]);?>
  <?php }?>
  <?php CustomPjax::end(); ?>
  <?= UsersTabStart::footer()?>
</div>
<?= $this->render('/booking/js/list_script')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
<?= $this->render('/site/admin/js/scripts')?>
