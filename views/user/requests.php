<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\widgets\UsersTabStart;
use app\models\User;
use app\models\LogUserRequestToBoatAssign;
use app\assets\UserRequestAssets;
UserRequestAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title =$model->fullname.' - ';

if($searchModel->listType=='future'){
  $this->title .= Yii::t('app', 'Active Requests');
}elseif($searchModel->listType=='history'){
  $this->title .= Yii::t('app', 'Requests History');
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-requests-index">
  <?= UsersTabStart::header($model,$searchModel)?>
  <?= $this->render('/user-requests/_gridview',[
    'searchModel'=>$searchModel,
    'dataProvider'=>$dataProvider,
  ])?>
  <?= UsersTabStart::footer()?>
</div>
<?= $this->render('/request/js/boat_booking_scripts')?>
