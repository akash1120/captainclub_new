<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\UserNightPermit;
use app\models\UserLicense;
use app\assets\UserFormAssets;
UserFormAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */

if($model->profileInfo!=null){
  $model->mobile=$model->profileInfo->mobile;
  $model->is_licensed=$model->profileInfo->is_licensed;
  $model->city_id=$model->profileInfo->city_id;
  $model->marina_permits=ArrayHelper::map(UserNightPermit::find()->Select(['port_id'])->where(['user_id'=>$model->id])->asArray()->all(),"port_id","port_id");

  $model->membership_number=$model->profileInfo->membership_number;
  $model->joining_date=$model->profileInfo->joining_date;
}
$this->registerJs('
$(".dtpicker").datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
}).on("changeDate", function(e){
  $(this).datepicker("hide");
});
')
?>
<style>
#staff-marina_permits label{width: 30%;}
</style>
<div class="tabs">
  <?php if($model->id!=null){?>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#mainForm">Member Information</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#tabLicense">License</a>
    </li>
    <?php if($model->id!=null){?>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#tabMemCard">Membership Card</a>
    </li>
    <?php }?>
  </ul>
  <?php }?>
  <div class="tab-content">
    <div id="mainForm" class="tab-pane active">
      <?php $form = ActiveForm::begin(); ?>
      <?php if($model->id!=null){?>
        <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('user',$model['image'],'medium')?>" width="75" height="75" />
      <?php }?>
      <?= $form->field($model, 'image',[
      'template' => '
      {label}
      {input}
      <div class="input-group">
        <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-file"></i></span></div>
        <input type="text" class="form-control" disabled placeholder="'.Yii::t('app','Upload Image').'">
        <span class="input-group-append">
          <button class="browse btn btn-success input-md" type="button"><i class="glyphicon glyphicon-folder-open"></i> '.Yii::t('app','Browse').'</button>
        </span>
      </div>
      {error}'
      ])->fileInput(['class'=>'file input-file', 'data-img'=>'mcsc-photo', 'accept' => 'image/*']);?>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr) ?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'membership_number')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'joining_date')->textInput(['class'=>'form-control dtpicker', 'maxlength' => true, 'autocomplete'=>'off'])?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
        </div>
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'mobile',['template'=>'
          {label}
          <div class="input-group">
            <span class="input-group-prepend">
              <span class="input-group-text">
                +971
              </span>
            </span>
            {input}
          </div>
          {hint}{error}
          '])->textInput() ?>
        </div>
        <?php if($model->user_type==0){?>
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'is_licensed')->dropDownList(Yii::$app->helperFunctions->arrYesNo) ?>
        </div>
        <?php }else{?>
        <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'permission_group_id')->dropDownList(Yii::$app->menuHelperFunction->adminGroupListArr) ?>
        </div>
        <?php }?>
      </div>
      <?php if($model->id!=null){?>
      <?= $form->field($model, 'marina_permits')->checkboxList(Yii::$app->appHelperFunctions->marinaListArr) ?>
      <?= $form->field($model, 'update_note')->textInput(['maxlength' => true])?>
      <?php
      }else{
        $cities=Yii::$app->appHelperFunctions->activeCityList;
      ?>
        <h3>License Info</h3>
        <div class="row">
          <?php
          foreach($cities as $city){
            $licenseImage=Yii::$app->fileHelperFunctions->getDefaultPhoto('user_license');
          ?>
          <div class="col-xs-12 col-sm-6">
            <section class="card card-featured-top card-featured-left card-featured-right card-featured-bottom card-featured-quaternary mb-4">
              <header class="card-header">
                <h2 class="card-title"><?= $city['name']?></h2>
              </header>
        			<div class="card-body">
                <?= $form->field($model, 'city_license_image['.$city['id'].']',['template'=>'
            		{label}
            		<div class="input-group file-input">
            		<div class="input-group-prepend">
            		<span class="input-group-text"><img src="'.$licenseImage.'" /></span>
            		</div>
            		<div class="custom-file">
            		{input}
            		<label class="custom-file-label" for="fldImg_'.$city['id'].'">Choose file</label>
            		</div>
            		</div>
            		{error}{hint}
            		'])->fileInput(['id'=>'fldImg_'.$city['id'].'','class'=>'custom-file-input','accept'=>'image/*']) ?>
                <div class="row">
                  <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'city_license_start['.$city['id'].']')->textInput(['class'=>'form-control dtpicker', 'maxlength' => true, 'autocomplete'=>'off'])?>
                  </div>
                  <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'city_license_expiry['.$city['id'].']')->textInput(['class'=>'form-control dtpicker', 'maxlength' => true, 'autocomplete'=>'off'])?>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <?php }?>
        </div>
      <?php }?>
      <div>
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    	</div>
      <?php ActiveForm::end(); ?>
    </div>
    <?php if($model->id!=null){?>
    <div id="tabLicense" class="tab-pane">
      <?php if($model->id==null){?>
        <div class="alert alert-info">
          Please save member details first
        </div>
      <?php }else{?>
      <?= $this->render('/user-license/index',['searchModel'=>$licenseSearchModel,'dataProvider'=>$licenseDataProvider,'model'=>$model])?>
      <?php }?>
    </div>
    <div id="tabMemCard" class="tab-pane">
      <?= $this->render('/user/membership_card',['model'=>$model])?>
    </div>
    <?php }?>
  </div>
</div>
