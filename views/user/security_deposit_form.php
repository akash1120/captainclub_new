<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
if($model->deposit_type==null){
  $model->deposit_type='Cash';
}
?>
<?php $form = ActiveForm::begin(['id'=>'securityDepositForm']); ?>
<?= $form->field($model, 'amount')->textInput([]) ?>
<?= $form->field($model, 'deposit_type')->radioList(array('Cash'=>'Cash','Cheque'=>'Cheque')); ?>
<div class="row">
    <div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="resetModelWin()">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
