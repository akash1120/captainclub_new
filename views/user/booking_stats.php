<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\UsersTabStart;
use app\models\Contract;
/*use app\models\OperationsBooking;
use app\models\BookingInOutBookingExp;
use app\models\BookingCancelled;
use app\models\BookingInOutTripExp;
use app\models\BookingInOutMemberExp;
use app\models\BookingNoShow;*/

$cities=Yii::$app->appHelperFunctions->cityList;

$contract_start_date='';
$contract_end_date='';
$contract_id='';
if($searchModel->contract_id!=null){
  $contract=Contract::find()->where(['id'=>$searchModel->contract_id])->one();
  if($contract!=null){
    $contract_id=$contract['id'];
    $contract_start_date=$contract['start_date'];
    $contract_end_date=$contract['end_date'];
  }
}
?>
<div id="w0" class="grid-view">
  <div class="tabs">
    <ul class="nav nav-tabs" style="position:relative;">
      <li class="nav-item">
        <a class="nav-link" href="<?= Url::to(['user/bookings','id'=>$user->id,'contract_id'=>$contract_id,'listType'=>'future'])?>" data-pjax="0">Active Bookings (<?= Yii::$app->statsFunctions->getUserActiveBookingCount($user->id,$contract_start_date,$contract_end_date)?>)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= Url::to(['user/bookings','id'=>$user->id,'contract_id'=>$contract_id,'listType'=>'history'])?>" data-pjax="0">Booking History (<?= Yii::$app->statsFunctions->getUserOldBookingCount($user->id,$contract_start_date,$contract_end_date)?>)</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= Url::to(['user/bookings','id'=>$user->id,'contract_id'=>$contract_id,'listType'=>'deleted'])?>" data-pjax="0">Deleted Bookings (<?= Yii::$app->statsFunctions->getUserDeletedBookingCount($user->id,$contract_start_date,$contract_end_date)?>)</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="javascript:;" data-pjax="0">Booking Stats</a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active">
        <section class="card">
          <div class="card-body">
            <div class="row">
              <?php $n=1;foreach($cities as $city){?>
                <div class="col-xs-12 col-sm-6">
                  <section class="panel">
                    <header class="panel-heading">
                      <h2 class="panel-title"><?= $city['name']?></h2>
                    </header>
                    <div class="panel-body">
                      <?php $ports=Yii::$app->appHelperFunctions->getCityMarinaList($city['id']);?>
                      <div class="tabs">
                        <ul class="nav nav-tabs">
                          <li class="nav-item"><a class="nav-link active" href="#bscpall<?= $city['id']?>" data-toggle="tab">All</a></li>
                          <?php foreach($ports as $port){?>
                            <li class="nav-item"><a class="nav-link" href="#bscp<?= $port['id']?>" data-toggle="tab"><?= $port['name']?></a></li>
                          <?php }?>
                        </ul>
                        <div class="tab-content">
                          <?php
                          $bsSmoothBookingsAll=0;
                          $bsNABookingsAll=0;
                          $bsRequestBookingsAll=0;

                          $tsCanceledWeatherBookingsAll=0;
                          $tsCanceledNABookingsAll=0;
                          $tsTechnicalBookingsAll=0;
                          $tsStuckBookingsAll=0;
                          $tsAccidentBookingsAll=0;
                          $tsIncidentBookingsAll=0;
                          $tsNoShowBookingsAll=0;
                          $tsSameDayCancelBookingsAll=0;
                          $tsWeatherWarningCancelBookingsAll=0;
                          $tsOtherBookingsAll=0;
                          $tsStayInBookingsAll=0;
                          $tsSmoothBookingsAll=0;

                          $behLateArrivalBookingsAll=0;

                          $extraOvernightCampBookingsAll=0;
                          $extraCaptainBookingsAll=0;
                          foreach($ports as $port){
                            //Booking Status
                            $bsNABookings=Yii::$app->bookingHelperFunctions->getBookingExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,2);
                            $bsNABookingsAll+=$bsNABookings;
                            $bsRequestBookings=Yii::$app->bookingHelperFunctions->getBookingExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,3);
                            $bsRequestBookingsAll+=$bsRequestBookings;
                            $bsSmoothBookings=Yii::$app->bookingHelperFunctions->getBookingExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,1);
                            //$bsSmoothBookings=($bsSmoothBookings-$bsNABookings-$bsRequestBookings);
                            $bsSmoothBookingsAll+=$bsSmoothBookings;

                            //Trip Status
                            $tsCanceledWeatherBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,4,'bs');
                            $tsCanceledWeatherBookingsAll+=$tsCanceledWeatherBookings;

                            $tsCanceledNABookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,5,'bs');
                            $tsCanceledNABookingsAll+=$tsCanceledNABookings;

                            $tsTechnicalBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,2);
                            $tsTechnicalBookingsAll+=$tsTechnicalBookings;

                            $tsStuckBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,3);
                            $tsStuckBookingsAll+=$tsStuckBookings;

                            $tsAccidentBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,4);
                            $tsAccidentBookingsAll+=$tsAccidentBookings;

                            $tsIncidentBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,6);
                            $tsIncidentBookingsAll+=$tsIncidentBookings;

                            $tsNoShowBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,3,'bs');
                            $tsNoShowBookingsAll+=$tsNoShowBookings;

                            $tsSameDayCancelBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,7,'bs');
                            $tsSameDayCancelBookingsAll+=$tsSameDayCancelBookings;

                            $tsWeatherWarningCancelBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,8,'bs');
                            $tsWeatherWarningCancelBookingsAll+=$tsWeatherWarningCancelBookings;

                            $tsStayInBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,2,'bs');
                            $tsStayInBookingsAll+=$tsStayInBookings;

                            $tsOtherBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,5);
                            $tsOtherBookingsAll+=$tsOtherBookings;


                            $tsSmoothBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat($user->id,$port['id'],$contract->start_date,$contract->end_date,1);
                            //$tsSmoothBookings=($tsSmoothBookings-$tsCanceledWeatherBookings-$tsCanceledNABookings-$tsTechnicalBookings-$tsStuckBookings-$tsAccidentBookings-$tsNoShowBookings-$tsSameDayCancelBookings-$tsWeatherWarningCancelBookings-$tsStayInBookings-$tsOtherBookings);//////
                            $tsSmoothBookingsAll+=$tsSmoothBookings;

                            //Behaviour
                            $behLateArrivalBookings=Yii::$app->bookingHelperFunctions->getBookingBehaviorStat($user->id,$port['id'],$contract->start_date,$contract->end_date,1);
                            $behLateArrivalBookingsAll+=$behLateArrivalBookings;

                            //Extra
                            $extraOvernightCampBookings=Yii::$app->bookingHelperFunctions->getBookingExtraStat($user->id,$port['id'],$contract->start_date,$contract->end_date,1);
                            $extraOvernightCampBookingsAll+=$extraOvernightCampBookings;

                            $extraCaptainBookings=Yii::$app->bookingHelperFunctions->getBookingExtraStat($user->id,$port['id'],$contract->start_date,$contract->end_date,2);
                            $extraCaptainBookingsAll+=$extraCaptainBookings;
                            ?>
                            <div id="bscp<?= $port['id']?>" class="tab-pane">
                              <table class="table table-striped table-bordered">
                                <tr>
                                  <th>Booking Status</th>
                                  <th>Trip Status</th>
                                  <th>Behavior</th>
                                  <th>Extra</th>
                                </tr>
                                <tr>
                                  <td><?= $bsSmoothBookings?> Smooth</td>
                                  <td><?= $tsSmoothBookings?> Smooth</td>
                                  <td><?= $behLateArrivalBookings?> Late arrival</td>
                                  <td><?= $extraOvernightCampBookings?> Overnight Camp</td>
                                </tr>
                                <tr>
                                  <td><?= $bsNABookings?> Original NA</td>
                                  <td><?= $tsStuckBookings?> Stuck</td>
                                  <td></td>
                                  <td><?= $extraCaptainBookings?> Captain</td>
                                </tr>
                                <tr>
                                  <td><?= $bsRequestBookings?> Request</td>
                                  <td><?= $tsTechnicalBookings?> Technical</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsAccidentBookings?> Accident</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsIncidentBookings?> Incident</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsNoShowBookings?> No Show</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsCanceledWeatherBookings?> Canceled Due To Bad Weather</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsCanceledNABookings?> Canceled Original N/A</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsSameDayCancelBookings?> Same day Canceled</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsWeatherWarningCancelBookings?> Canceled due to weather warning</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsStayInBookings?> Stayin</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><?= $tsOtherBookings?> Other</td>
                                  <td></td>
                                  <td></td>
                                </tr>
                              </table>
                            </div>
                          <?php }?>
                          <div id="bscpall<?= $city['id']?>" class="tab-pane active">
                            <table class="table table-striped table-bordered">
                              <tr>
                                <th>Booking Status</th>
                                <th>Trip Status</th>
                                <th>Behavior</th>
                                <th>Extra</th>
                              </tr>
                              <tr>
                                <td><?= $bsSmoothBookingsAll?> Smooth</td>
                                <td><?= $tsSmoothBookingsAll?> Smooth</td>
                                <td><?= $behLateArrivalBookingsAll?> Late arrival</td>
                                <td><?= $extraOvernightCampBookingsAll?> Overnight Camp</td>
                              </tr>
                              <tr>
                                <td><?= $bsNABookingsAll?> Original NA</td>
                                <td><?= $tsStuckBookingsAll?> Stuck</td>
                                <td></td>
                                <td><?= $extraCaptainBookingsAll?> Captain</td>
                              </tr>
                              <tr>
                                <td><?= $bsRequestBookingsAll?> Request</td>
                                <td><?= $tsTechnicalBookingsAll?> Technical</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsAccidentBookingsAll?> Accident</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsAccidentBookingsAll?> Incident</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsNoShowBookingsAll?> No Show</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsCanceledWeatherBookingsAll?> Canceled Due To Bad Weather</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsCanceledNABookingsAll?> Canceled Original N/A</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsSameDayCancelBookingsAll?> Same day Canceled</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsWeatherWarningCancelBookingsAll?> Canceled due to weather warning</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsStayInBookingsAll?> Stayin</td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td></td>
                                <td><?= $tsOtherBookingsAll?> Other</td>
                                <td></td>
                                <td></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
                <?php $n++;}?>
              </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
