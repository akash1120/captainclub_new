<?php
$avatarImg=Yii::$app->fileHelperFunctions->getImagePath('user',$model->image,'medium');
?>
<section class="body-sign membership-card" style="padding: 0 5px;">
  <div class="panel card-sign">
    <div class="card-title-sign mt-3 text-center">
      <h2 class="title text-uppercase font-weight-bold m-0">
        <img src="images/logo.png" alt="The Captains Club" height="80">
      </h2>
    </div>
    <div class="card-body">
      <div class="bg-memcard clearfix">
        <img src="<?= $avatarImg?>" class="photo pull-left" alt="<?= $model->firstname?>" />
        <div class="mem-info pull-left">
          <strong>Name: </strong> <?= $model->firstname.' '.$model->lastname?><br />
          <strong>Membership#: </strong> <?= $model->profileInfo->membership_number;?><br />
          <strong>Category: </strong> <?= $model->activeContract->package_name?><br />
          <strong>Joining Date: </strong> <?= $model->profileInfo->joining_date!=null ? Yii::$app->formatter->asDate($model->profileInfo->joining_date,"php: M d, Y") : ''?><br />
        </div>
    </div>
  </div>
</section>
