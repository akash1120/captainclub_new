<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;
use app\models\User;
use app\models\Package;
use app\models\Cities;
use yii\widgets\ActiveForm;
use app\assets\FrezedMembersAsset;
FrezedMembersAsset::register($this);

$this->title = Yii::t('app', 'Members on Freeze');
$this->params['breadcrumbs'][] = $this->title;


$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
	initScripts();
});
');
?>
<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<section class="card mb-2">
	<div class="card-body filter-padding">
		<?php $form = ActiveForm::begin(['method'=>'get']); ?>
		<div class="row">
			<div class="col-xs-12 col-sm-3 pull-left">
				<?= $form->field($searchModel, 'freeze_date_range')->textInput(['autocomplete'=>'off','class'=>'form-control dtrPicker','placeholder'=>'Date Range'])->label(false) ?>
			</div>
			<div class="col-xs-12 col-sm-2 pull-left text-right">
				<?= Html::submitButton(Yii::t('app', 'Search'), ['class'=>'btn btn-success btn-block']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</section>
<?= CustomGridView::widget([
'dataProvider' => $dataProvider,
'filterModel' => $searchModel,
'columns' => [
	['class' => 'yii\grid\SerialColumn'],
	['format' => ['image',['width'=>'40','height'=>'40']],'attribute'=>'image','value'=>function($model){
		return Yii::$app->fileHelperFunctions->getImagePath('user',$model['image'],'small');
	},'headerOptions'=>['class'=>'noprint fxd1'],'contentOptions'=>['class'=>'fxd1']],
	['format'=>'raw','attribute'=>'username','value'=>function($model){
		$html='';
		$html.=$model['email'].(trim($model['name'])!='' ? '<div><span class="badge grid-badge badge-info">Name: '.$model['name'].'</span></div>' : '');
		$html.=$model['username']!='' ? '<div><span class="badge grid-badge badge-info">Old Username: '.$model['username'].'</span></div>' : '';
		return $html;
	},'headerOptions'=>['class'=>'noprint fxdBig'],'contentOptions'=>['class'=>'fxdBig']],
	['attribute'=>'package_name','filter'=>Yii::$app->appHelperFunctions->packageListArr],
	['attribute'=>'city_name','headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2'],'filter'=>Yii::$app->appHelperFunctions->cityListArr],
	['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
		return Yii::$app->helperFunctions->getUserStatus($model['id'],$model['status'],$model);
	},'headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2'],'filter'=>Yii::$app->helperFunctions->arrUserStatus],
	['format'=>'date','attribute'=>'freeze_start','label'=>'Freeze Started','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']],
	['format'=>'date','attribute'=>'freeze_end','label'=>'Freeze Ending','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']],
],
'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
]); ?>
<?php CustomPjax::end(); ?>
<script>
function initScripts()
{
	$(".dtrPicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
	$(".dtrPicker").on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
		$(this).trigger("change");
	});
	$(".dtrPicker").on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
}
</script>
