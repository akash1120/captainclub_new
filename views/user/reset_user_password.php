<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserPasswordResetForm */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id'=>'userResetPassForm']); ?>
<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'new_password')->textInput() ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'force_change_password')->dropDownList(['1'=>'No','0'=>'Yes']) ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Cancel', 'javascript:;', ['class' => 'btn btn-default', 'data-dismiss'=>'modal']) ?>
	</div>
</div>
<?php ActiveForm::end(); ?>
