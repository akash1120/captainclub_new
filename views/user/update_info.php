<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\User;
use app\models\UserLicense;
use app\models\Package;
use app\models\Cities;
use yii\widgets\ActiveForm;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

$this->title = Yii::t('app', 'Members');
$this->params['breadcrumbs'][] = $this->title;

$cities=Yii::$app->appHelperFunctions->activeCityList;

$gridBtnsArr='';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update-info')){
	$gridBtnsArr.=' {update-info}';
}

$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
	initScripts();
});
$(document).delegate(".btn-load-form", "click", function() {
	id=$(this).data("id");
	username=$(this).data("username");
	$.ajax({
		url: "'.Url::to(['user/update-member-info','id'=>'']).'"+id,
		dataType: "html",
		success: function(data) {
			$("#general-modal").find("h5.modal-title").html("Update Info - "+username);
			$("#general-modal").find(".modalContent").html(data);
			$("#general-modal").modal();
			$(".dtpicker").datepicker({
				format: "yyyy-mm-dd",
				todayHighlight: true,
			}).on("changeDate", function(e){
				$(this).datepicker("hide");
			});
		},
		error: bbAlert
	});
});
$("body").on("beforeSubmit", "form#basicInfoFrm", function () {
	_targetContainer=$("#basicInfoFrm")
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});

	 var form = $(this);
	 // return false if form still have some validation errors
	 if (form.find(".has-error").length) {
			return false;
	 }
	 var formData = new FormData(form[0]);
	 // submit form
	 $.ajax({
			url: form.attr("action"),
			type: "post",
			data: formData,
			success: function (response) {
				if(response=="success"){
					swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Information updated successfully').'", type: "success"});
					window.closeModal();
				}else{
					swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				}
				App.unblockUI($(_targetContainer));
				$.pjax.reload({container: "#grid-container", timeout: 2000});
			},
	    cache: false,
	    contentType: false,
	    processData: false
	 });
	 return false;
});
');
?>
<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<?= CustomGridView::widget([
'dataProvider' => $dataProvider,
'filterModel' => $searchModel,
'columns' => [
	['class' => 'yii\grid\SerialColumn'],
	['format'=>'html','attribute'=>'image','value'=>function($model){
		return Html::img(Yii::$app->fileHelperFunctions->getImagePath('user',$model['image'],'small'),['width'=>100,'height'=>100]);
	}],
	'username',
	'firstname',
	'lastname',
	['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
		return Yii::$app->helperFunctions->getUserStatus($model['id'],$model['status'],$model);
	},'headerOptions'=>['class'=>'noprint fxd2'],'contentOptions'=>['class'=>'fxd2'],'filter'=>Yii::$app->helperFunctions->arrUserStatus],
	['format'=>'raw','label'=>'License','value'=>function($model) use ($cities){
		$html='';
		foreach($cities as $city){
			$licenseRow=UserLicense::find()->where(['user_id'=>$model['id'],'city_id'=>$city['id']])->asArray()->orderBy(['license_expiry'=>SORT_DESC])->one();
			if($licenseRow!=null){
				$licImage=Yii::$app->fileHelperFunctions->getImagePath('user_license',$licenseRow['license_image'],'small');
				if($licImage=="images/default/default.png"){
					$imgCode='<img src="'.$licImage.'" width="20" height="20" />';
				}else{
					$imgCode='<a><img src="'.$licImage.'" width="20" height="20" /></a>';
				}

				$html.='<table class="table table-responsive-lg table-sm table-striped table-hover mb-0">';
				$html.='	<tr>';
				$html.='		<td>'.$imgCode.'</td>';
				$html.='		<td>'.$city['name'].'</td>';
				$html.='		<td>'.Yii::$app->formatter->asDate($licenseRow['license_start']).' to '.Yii::$app->formatter->asDate($licenseRow['license_expiry']).'</td>';
				$html.='	</tr>';
				$html.='</table>';
			}
		}
		return $html;
	}],
	[
		'class' => 'yii\grid\ActionColumn',
		'header'=>'Actions',
		'template' => $gridBtnsArr,
		'buttons' => [
			'update-info' => function ($url, $model) {
				return Html::a('<i class="glyphicon glyphicon-pencil"></i> Update', 'javascript:;', [
					'title' => Yii::t('app', 'Update Member'),
					'data-id'=>$model['id'],
					'data-username'=>$model['username'],
					'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-success btn-load-form',
				]);
			},
			'license-info' => function ($url, $model) {
				return Html::a('<i class="glyphicon glyphicon-pencil"></i> License', 'javascript:;', [
					'title' => Yii::t('app', 'Add / Update License'),
					'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-info btn-load-form',
				]);
			},
		],
	],
],
'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
]); ?>
<?php CustomPjax::end(); ?>
<script>
function initScripts()
{
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
}
</script>
<?= $this->render('/user-license/js/user_license_scritps')?>
