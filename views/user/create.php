<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'New Member');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <?= $this->render('_form', [
        'model' => $model,
        'licenseSearchModel' => null,
        'licenseDataProvider' => null,
    ]) ?>
</div>
