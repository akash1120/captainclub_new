<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use app\models\UserNightPermit;
use app\assets\AppAutoCompleteAsset;
AppAutoCompleteAsset::register($this);

$this->title = 'Search Member';

$this->registerJs('
	$("#lookup_member").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/member-search']).'",
		noCache: true,
      onSelect: function(suggestion) {
				window.location.href="'.Url::to(['search','id'=>'']).'"+suggestion.data;
      },
  });
');
$name='';
?>
<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<section class="staff-form card card-featured card-featured-warning">
  <header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
		<div class="input-group">
			<span class="input-group-prepend">
				<span class="input-group-text">Search</span>
			</span>
			<input id="lookup_member" type="text" value="<?= $name?>" class="form-control" />
		</div>
		<?php if($model!=null && $model->id!=null){?>
		<br />
		<table class="table table-stripped table-bordered">
			<tr>
				<th>Photo</th>
				<th>Username</th>
				<th>Full Name</th>
				<th>Licensed</th>
				<th>Package</th>
				<th>Email</th>
				<th>City</th>
				<th>Mobile</th>
				<th>Night Drive Permits</th>
				<th>Captain</th>
				<th>Freeze Days</th>
			</tr>
			<tr>
				<td><img src="<?= Yii::$app->fileHelperFunctions->getImagePath('user',$model->image,'tiny')?>" width="75" height="75" alt="<?= $model->username?>" /></td>
				<td><?= $model->username?></td>
				<td><?= $model->fullname?></td>
				<td><?= Yii::$app->helperFunctions->arrYesNo[$model->profileInfo->is_licensed]?></td>
				<td><?= $model->activeContract->package->name?></td>
				<td><?= $model->email?></td>
				<td><?= ($model->city!=null ? $model->city->name : 'No city assigned!');?></td>
				<td><?= $model->mobile?></td>
				<td><?= $model->allowedMarinaPermits?></td>
				<td>
					<?php
					$captainStats=Yii::$app->statsFunctions->getCaptainsByContract($model->active_contract_id);
					$allowedCaptains=$captainStats['allowed'];
					$usedCaptains=$captainStats['freeused'];
					$remainingCaptains=$captainStats['remaining'];
					$paidCaptains=$captainStats['paidused'];
					?>
					<strong>Allowed:</strong> <?= $allowedCaptains?><br />
					<strong>Used:</strong> <?= $usedCaptains?><br />
					<strong>Remaining:</strong> <?= $remainingCaptains?><br />
					<strong>Paid:</strong> <?= $paidCaptains?>
				</td>
				<td>
					<?php
					$freezeDaysStats=Yii::$app->statsFunctions->getFreezeDaysByContract($model->active_contract_id);
					$freezeTotal=$freezeDaysStats['allowed'];
					$freezeUsed=$freezeDaysStats['used'];
					$freezeRemaining=$freezeDaysStats['remaining'];
					?>
					<strong>Allowed:</strong> <?= $freezeTotal?><br />
					<strong>Used:</strong> <?= $freezeUsed?><br />
					<strong>Remaining:</strong> <?= $freezeRemaining?>
				</td>
			</tr>
		</table>
		<?php }?>
	</div>
</section>
<?php CustomPjax::end(); ?>
