<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserLicense;
use app\models\City;

/* @var $this yii\web\View */

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = $this->title;

$expiryAlert=Yii::$app->helperFunctions->expiryAlertDays;
$loggedInUser=Yii::$app->user->identity;
?>
<div class="tabs">
	<ul class="nav nav-tabs nav-jus1tified">
    <li class="nav-item">
      <a class="nav-link active" href="#tab1" data-toggle="tab"><?= $this->title?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#tab2" data-toggle="tab">Membership Card</a>
    </li>
	</ul>
	<div class="tab-content">
    <div id="tab1" class="tab-pane active">
      <?php $form = ActiveForm::begin(); ?>
          <?php
          $results=UserLicense::find()
          ->select([
            UserLicense::tableName().'.license_expiry',
            City::tableName().'.name',
          ])
          ->innerJoin(City::tableName(),City::tableName().".id=".UserLicense::tableName().".city_id")
          ->where([
            'and',
            ['user_id'=>$loggedInUser->id],
          ])
          ->groupBy('city_id')
          ->orderBy(['license_expiry'=>SORT_DESC])
          ->asArray()->all();
          if($results!=null){
          ?>
          <div class="row">
            <?php
            foreach($results as $result){
              $licExpiryRemainingDays=Yii::$app->helperFunctions->getNumberOfDaysBetween(date("Y-m-d"),$result['license_expiry']);
            ?>
            <div class="col-xs-12 col-sm-6">
              <div class="alert alert-<?= $licExpiryRemainingDays<=$expiryAlert ? 'danger' : 'info'?>">
                <div<?= $licExpiryRemainingDays<=$expiryAlert ? ' class="blink"' : ''?>>
                  <strong><?= $result['name'].' License Expiry'?>:
                  </strong> <?= Yii::$app->formatter->asDate($result['license_expiry'])?>
                  <a href="javascript:;" data-toggle="popover" data-html="true" data-trigger="hover" data-content="Please renew your license before <strong><?= Yii::$app->formatter->asDate($result['license_expiry'])?></strong> to continue boating smoothly.<br /><br />1 month reminder and guidance will be sent by the concerned department.<br /><br />Thank you.<br /><br />Best regards,">(I)</a>
                </div>
              </div>
            </div>
            <?php }?>
          </div>
          <?php }?>
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label class="control-label">Username</label>
              <input type="text" class="form-control" disabled="disabled" value="<?= $loggedInUser->username?>" />
              <div class="help-block"></div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label class="control-label">City</label>
              <input type="text" class="form-control" disabled="disabled" value="<?= $loggedInUser->profileInfo->city!=null ? $loggedInUser->profileInfo->city->name : 'N/A'?>" />
              <div class="help-block"></div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
              <?= $form->field($model, 'firstname')->textInput() ?>
          </div>
          <div class="col-xs-12 col-sm-6">
              <?= $form->field($model, 'lastname')->textInput() ?>
          </div>
          <div class="col-xs-12 col-sm-6">
              <?= $form->field($model, 'email')->textInput() ?>
          </div>
          <div class="col-xs-12 col-sm-6">
              <?= $form->field($model, 'mobile',['template'=>'
              {label}
              <div class="input-group">
                <span class="input-group-prepend">
                	<span class="input-group-text">
                		+971
                	</span>
                </span>
                {input}
              </div>
              {hint}{error}
              '])->textInput() ?>
          </div>
          <?php if(Yii::$app->user->identity->user_type==0){?>
          <div class="col-xs-12 col-sm-12">
            <?= $form->field($model, 'pref_marina_id')->dropDownList(Yii::$app->appHelperFunctions->marinaListArr) ?>
          </div>
          <?php }?>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-sm btn-success']) ?>
            <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-sm btn-default']) ?>
          </div>
        </div>
      <?php ActiveForm::end(); ?>
    </div>
    <div id="tab2" class="tab-pane">
      <?= $this->render('/user/membership_card',['model'=>$loggedInUser])?>
    </div>
  </div>
</div>
