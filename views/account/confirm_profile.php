<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserLicense;
use app\models\City;

/* @var $this yii\web\View */

$this->title = 'Confirm Profile';
$this->params['breadcrumbs'][] = $this->title;

$loggedInUser=Yii::$app->user->identity;
if($loggedInUser->profileInfo->profile_updated==0){
?>
<section class="admin-group-form card card-featured card-featured-warning">
  <header class="card-header">
    <h2 class="card-title">Profile Information</h2>
  </header>
  <div class="card-body">
    <?php $form = ActiveForm::begin(); ?>
      <div class="form-group">
        <label class="control-label">City</label>
        <input type="text" class="form-control" disabled="disabled" value="<?= $loggedInUser->profileInfo->city!=null ? $loggedInUser->profileInfo->city->name : 'N/A'?>" />
        <div class="help-block"></div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($modelProfile, 'firstname')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($modelProfile, 'lastname')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($modelProfile, 'email')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($modelProfile, 'mobile',['template'=>'
            {label}
            <div class="input-group">
              <span class="input-group-prepend">
                <span class="input-group-text">
                  +971
                </span>
              </span>
              {input}
            </div>
            {hint}{error}
            '])->textInput() ?>
        </div>
        <?php if(Yii::$app->user->identity->user_type==0){?>
        <div class="col-xs-12 col-sm-12">
          <?= $form->field($modelProfile, 'pref_marina_id')->dropDownList(Yii::$app->appHelperFunctions->marinaListArr) ?>
        </div>
        <?php }?>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-success']) ?>
        </div>
      </div>
    <?php ActiveForm::end(); ?>
  </div>
</section>
<?php
}

if($loggedInUser->profileInfo->changed_pass==0){
?>
<section class="admin-group-form card card-featured card-featured-warning">
  <header class="card-header">
    <h2 class="card-title">Set new Password</h2>
  </header>
  <div class="card-body">
    <?php $form = ActiveForm::begin(); ?>
      <div class="row">
        <div class="col-sm-12">
          <?= $form->field($modelPassword, 'old_password')->passwordInput(['autofocus' => true]) ?>
        </div>
        <div class="col-sm-12">
          <?= $form->field($modelPassword, 'new_password')->passwordInput() ?>
        </div>
        <div class="col-sm-12">
          <?= $form->field($modelPassword, 'confirm_new_password')->passwordInput() ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-sm btn-success']) ?>
        </div>
      </div>
    <?php ActiveForm::end(); ?>
  </div>
</section>
<?php
}
?>
