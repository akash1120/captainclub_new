<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ChangePasswordForm */

$this->title = Yii::t('app','Change Password');
?>
<div class="change-password">
  <section class="admin-group-form card card-featured card-featured-warning">
    <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
    <header class="card-header">
      <h2 class="card-title"><?= $this->title?></h2>
    </header>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <?= $form->field($model, 'old_password')->passwordInput(['autofocus' => true]) ?>
        </div>
        <div class="col-sm-12">
          <?= $form->field($model, 'new_password')->passwordInput() ?>
        </div>
        <div class="col-sm-12">
          <?= $form->field($model, 'confirm_new_password')->passwordInput() ?>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
      <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
  </section>
</div>
