<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\AppGuestAsset;

AppGuestAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="fixed js flexbox flexboxlegacy no-touch csstransforms csstransforms3d no-overflowscrolling webkit chrome mac js no-mobile-device custom-scroll">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?= $this->render('blocks/favicon')?>
  <?php $this->head() ?>
  <base href="/">
</head>
<body>
  <?php $this->beginBody() ?>
  <section class="body-sign">
    <div class="center-sign">
      <div class="panel card-sign">
        <div class="card-title-sign mt-3 text-center">
          <h2 class="title text-uppercase font-weight-bold m-0">
            <img src="images/logo.png" height="80" alt="<?= Yii::$app->params['siteName']?>" />
          </h2>
        </div>
        <div class="card-body">
          <?= Alert::widget() ?>
          <?= $content ?>
        </div>
      </div>
      <p class="text-center text-muted mt-3 mb-3">&copy; Copyright <?php echo date('Y');?>. <?php echo Yii::$app->params['siteName'];?></p>
    </div>
  </section>

  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
