<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\components\widgets\ToasterAlert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\widgets\CustomBreadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$avatarImg=Yii::$app->fileHelperFunctions->getImagePath('user',Yii::$app->user->identity->image,'tiny');
$loggedInUser = Yii::$app->user->identity;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="fixed sidebar-left-collapsed">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title.' - '.Yii::$app->params['siteName']) ?></title>
  <?= $this->render('blocks/favicon')?>
  <?php $this->head() ?>
  <base href="/">
    <style>
        @media (max-width: 768px) {
            .modal-body {
                max-height: calc(120vh - 210px) !important;
                overflow-y: auto !important;
            }

            #modal-boat-container {
                font-size: 10px;
            }

            .userbox:after {
                content: none;
            }
        }
    </style>
</head>
<body>
  <?php $this->beginBody() ?>
  <section class="body">
    <header class="header">
      <div class="logo-container text-center" style="background: black">
        <div id="userbox" class="userbox text-left d-block d-md-none" style="width: 0px;">
                        <a href="javascript:;" data-toggle="modal" data-target="#memebershipcard">
                            <figure class="profile-picture">
                                <img src="<?= $avatarImg ?>" alt="<?= Yii::$app->user->identity->name ?>" class="rounded-circle"/>
                            </figure>
                            <div class="profile-info">
                                <span class="name" style="color: #ACACAC;"><?= Yii::$app->user->identity->name ?></span>
                                <span class="role"><?= Yii::$app->user->identity->user_type == 0 ? 'Member   <i class="fas fa-credit-card" style="color: #c0ca33; font-size: 14px;" ></i>' : 'Admin' ?></span>

                            </div>
                            <!-- <i class="fa custom-caret"></i>-->

                        </a>


                        <!--  <div class="dropdown-menu">
                        <ul class="list-unstyled mb-2">
                          <li class="divider"></li>
                          <li>
                            <a role="menuitem" tabindex="-1" href="<? /*= Url::to(['account/update-profile'])*/ ?>"><i class="fas fa-user"></i> My Profile</a>
                          </li>
                          <li>
                            <a role="menuitem" tabindex="-1" href="<? /*= Url::to(['account/change-password'])*/ ?>"><i class="fas fa-lock"></i> Change Password</a>
                          </li>
                          <li>
                            <a role="menuitem" tabindex="-1" href="<? /*= Url::to(['site/logout'])*/ ?>" data-method="post"><i class="fas fa-power-off"></i> Logout</a>
                          </li>
                        </ul>
                      </div>-->
                    </div>
        <div class="d-block d-md-none" style="margin-left: -15px">
          <a href="<?= Url::to(['site/index']) ?>" class="logo"><img src="images/email_logo.png" width="65" height="50" alt="<?= Yii::$app->params['siteName']?>" /></a>
        </div>
        <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
          <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
        <a href="<?= Url::to(['site/index']) ?>" class="logo d-none d-md-block"><img src="images/email_logo1.png" width="65" height="50" alt="<?= Yii::$app->params['siteName'] ?>"/></a>
      </div>
      <div class="header-right d-none d-md-block">
        <span class="separator"></span>
        <div id="userbox" class="userbox">
          <a href="javascript:;" data-toggle="modal" data-target="#memebershipcard">
            <figure class="profile-picture">
              <img src="<?= $avatarImg?>" alt="<?= Yii::$app->user->identity->name?>" class="rounded-circle" />
            </figure>
            <div class="profile-info">
              <span class="name"><?= Yii::$app->user->identity->name?></span>
              <span class="role"><?= Yii::$app->user->identity->user_type==0 ? 'Member <i class="fas fa-credit-card" style="color: #c0ca33; font-size: 14px;" ></i>' : 'Admin'?></span>
            </div>
            <!-- <i class="fa custom-caret"></i>-->
          </a>
          <!--  <div class="dropdown-menu">
            <ul class="list-unstyled mb-2">
              <li class="divider"></li>
              <li>
                <a role="menuitem" tabindex="-1" href="<? /*= Url::to(['account/update-profile'])*/ ?>"><i class="fas fa-user"></i> My Profile</a>
              </li>
              <li>
                <a role="menuitem" tabindex="-1" href="<? /*= Url::to(['account/change-password'])*/ ?>"><i class="fas fa-lock"></i> Change Password</a>
              </li>
              <li>
                <a role="menuitem" tabindex="-1" href="<?= Url::to(['site/logout']) ?>" data-method="post"><i class="fas fa-power-off"></i> Logout</a>
              </li>
            </ul>
          </div>-->
        </div>
      </div>
    </header>
    <div class="inner-wrapper">
      <aside id="sidebar-left" class="sidebar-left">
        <div class="sidebar-header">
          <div class="sidebar-title" style="color: #abb4be">
            Navigation
            <div class="d-md-none toggle-sidebar-left" style="float: right" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
                <b>Back</b>
            </div>
          </div>
          <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
          </div>
        </div>
        <div class="nano has-scrollbar">
          <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
              <ul class="nav nav-main">
                <li<?= Yii::$app->controller->id=='site' && Yii::$app->controller->action->id=='index' ? ' class="nav-active"' : ''?>>
                  <a class="nav-link" href="<?= Url::to(['site/index'])?>">
                    <i class="fas fa-home" aria-hidden="true"></i> <span><?= Yii::t('app','Dashboard')?></span>
                  </a>
                </li>
                <?php if (Yii::$app->user->identity->user_type == 0) { ?>
                <li class="nav-parent"><a class="nav-link" href="#"> <i class="fas fa-edit"></i> <span>Manage Bookings</span>
                    </a>
                    <ul class="nav nav-children">
                        <li>
                            <a class="nav-link" href="<?= Url::to(['booking/create']) ?>"> <i
                                        class="fas fa-angle-right"></i> <span>Make a booking</span> </a>
                        </li>
                        <li><a class="nav-link"
                               href="<?= Url::to(['booking/index?listType=future']) ?>"> <i
                                        class="fas fa-angle-right"></i> <span>Future Bookings</span>
                            </a>
                        </li>
                        <li><a class="nav-link"
                               href="<?= Url::to(['booking/index?listType=history']) ?>"> <i
                                        class="fas fa-angle-right"></i> <span>Booking History</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-parent"><a class="nav-link" href="#"> <i class="fas fa-question"></i>
                        <span>Manage Requests</span>
                    </a>
                    <ul class="nav nav-children">
                        <li>
                            <a href="javascript:;" data-toggle="modal" data-target="#request-modal">
                                <i class="fas fa-angle-right"></i> <span>Submit Request</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link"
                               href="<?= Url::to(['site/my-requests?listType=active']) ?>">
                                <i class="fas fa-angle-right"></i> <span>Active Requests</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link"
                               href="<?= Url::to(['site/my-requests?listType=history']) ?>">
                                <i class="fas fa-angle-right"></i> <span>Request History</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-parent"><a class="nav-link" href="#"> <i class="fa fa-user"></i> <span>Manage Profile</span>
                    </a>
                    <ul class="nav nav-children">
                        <li>
                            <a class="nav-link" href="<?= Url::to(['account/update-profile']) ?>">
                                <i class="fas fa-angle-right"></i> <span>My Profile</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="javascript:;" data-toggle="modal"
                               data-target="#memebershipcard">
                                <i class="fas fa-angle-right"></i> <span>Membership Card</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#">
                                <i class="fas fa-angle-right"></i> <span>Boating License</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?= Url::to(['account/change-password']) ?>">
                                <i class="fas fa-angle-right"></i> <span>Change Password</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="nav-link load-modal" href="javascript:;" data-url="<?= Url::to(['request/suggestion']) ?>" data-heading="Suggestions / Complaints">
                        <i class="fas fa-envelope" aria-hidden="true"></i>
                        <span>Submit Feedback</span>
                    </a>
                </li>
                <li<?= Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'discount' ? ' class="nav-active"' : '' ?>>
                    <a class="nav-link" href="<?= Url::to(['site/discount']) ?>">
                        <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                        <span>My Discounts</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="<?= Url::to(['site/logout']) ?>" data-method="post">
                        <i class="fas fa-power-off" aria-hidden="true"></i>
                        <span>Logout</span>
                    </a>
                </li>
                <?php } else { ?>
                <?= Yii::$app->menuHelperFunction->generateStaffMenu?>
                <?php } ?>
              </ul>
            </nav>
          </div>
        </div>
      </aside>
      <section role="main" class="content-body">
        <header class="page-header">
          <h2><?= $this->title?></h2>
          <div class="right-wrapper text-right">
            <?= CustomBreadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
          </div>
        </header>
        <div class="body-content">
          <noscript>
            <div class="alert alert-danger"><strong>Javascript is disabled.</strong><br />Please enable JavaScript in your browser for better use of the website.</div>
          </noscript>
          <?= ToasterAlert::widget() ?>
          <?= $content ?>
        </div>
      </section>
    </div>
  </section>
<?php
if($loggedInUser->user_type==0){
Modal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'memebershipcard',
  'size' => 'modal-lg',
 	'header' => '<h5 class="modal-title">Membership Card</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton' => false,
]);
echo $this->render('/user/membership_card',['model'=>$loggedInUser]);
Modal::end();
}
Modal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'general-modal',
  'size' => 'modal-lg',
 	'header' => '<h5 class="modal-title"></h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton' => false,
]);
echo "<div class='modalContent'></div>";
Modal::end();
Modal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'general-modal-secondary',
  'size' => 'modal-lg',
 	'header' => '<h5 class="modal-title"></h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton' => false,
]);
echo "<div class='modalContent'></div>";
Modal::end();
?>
<?= $this->render('/shared/js/scripts')?>
<?= $this->registerJs('
$(\'.table_div a\').on(\'click\', function () {
    $(\'.sm_book_model\').modal(\'hide\');
});
   '); ?>
  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
