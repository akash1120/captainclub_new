<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

$this->title = Yii::t('app', 'Update Staff Member: {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Staff Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="staff-update">
  <div class="row">
    <div class="col-xs-12 col-sm-8">
      <?= $this->render('/user/_form', [
          'model' => $model,
          'licenseSearchModel' => $licenseSearchModel,
          'licenseDataProvider' => $licenseDataProvider,
      ]) ?>
    </div>
    <div class="col-xs-12 col-sm-4">
      <?= $this->render('/user-note/list-view', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]) ?>
    </div>
  </div>
</div>
