<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SupportTicket */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ticket',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tickets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ticket-reply">
  <?= $this->render('_form', [
      'model' => $model,
      'parentModel' => $parentModel,
  ]) ?>
</div>
