<?php
use yii\helpers\Url;
?>
<div class="panel-body">
  <div class="row">
    <div class="col-xs-12 col-sm-9">
      <strong><?= $model->createdByUser->username?></strong>
    </div>
    <div class="col-xs-12 col-sm-3 text-right">
      <i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDate($model->created_at);?>
    </div>
  </div>
  <div class="row">
    <?php
    $decpCol=12;
  	$attachmentFiles=$model->attachmentInfo;
  	if($attachmentFiles!=null){
      $decpCol=9;
    }
  	?>
    <div class="col-xs-12 col-sm-<?= $decpCol?>">
      <?= $model->descp?>
    </div>
    <?php if($attachmentFiles!=null){?>
    <div class="col-xs-12 col-sm-3 text-right">
      <strong>Attachments:</strong>
      <ul class="att-container">
        <?php foreach($attachmentFiles as $attachmentFile){?>
          <li class="att-Item">
            <a href="<?= $attachmentFile['link']?>" data-pjax="0" target="_blank">
              <img src="<?= $attachmentFile['iconPath']?>" class="img-responsive" alt="" />
            </a>
          </li>
        <?php }?>
      </ul>
    </div>
    <?php }?>
  </div>
  <?php if(Yii::$app->user->identity->id==$model->created_by){?>
  <a href="<?= Url::to(['support-ticket-reply/update','parent_id'=>$model->parent->rssid,'id'=>$model->rssid])?>" data-pjax="0" class="btn btn-xs btn-info">Update</a>
  <a href="javascript:;" class="btn btn-xs btn-danger btn-del-reply" data-id="<?= $model->rssid?>">Delete</a>
  <?php }?>
</div>
