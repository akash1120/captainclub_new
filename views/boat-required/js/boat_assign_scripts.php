<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAutoCompleteAsset;
AppAutoCompleteAsset::register($this);

$this->registerJs('
  $(document).delegate(".assign-booking", "click", function() {
    id=$(this).data("booking_id");
    $.ajax({
      url: "'.Url::to(['booking/assign','id'=>'']).'"+id,
      dataType: "html",
      success: function(data) {
        $("#general-modal").find("h5.modal-title").html("Assign Boat");
        $("#general-modal").find(".modalContent").html(data);
        $("#general-modal").modal();
      	$(".lookup-assign-member").autocomplete({
          serviceUrl: "'.Url::to(['suggestion/member-search']).'",
      		noCache: true,
            onSelect: function(suggestion) {
      				if($("#adminbookingassign-to_user_id").val()!=suggestion.data){
      					$("#adminbookingassign-to_user_id").val(suggestion.data);
      				}
            },
            onInvalidateSelection: function() {
                $("#adminbookingassign-to_user_id").val("");
            }
        });
      },
      error: bbAlert
    });
  });
  $("body").on("beforeSubmit", "form#assign-booking-form", function () {
    _targetContainer=$("#assign-booking-form")
    App.blockUI({
      message: "'.Yii::t('app','Please wait...').'",
      target: _targetContainer,
      overlayColor: "none",
      cenrerY: true,
      boxed: true
    });

     var form = $(this);
     // return false if form still have some validation errors
     if (form.find(".has-error").length) {
        return false;
     }
     // submit form
     $.ajax({
        url: form.attr("action"),
        type: "post",
        data: form.serialize(),
        success: function (response) {
          if(response=="success"){
            swal({title: "'.Yii::t('app','Assigned').'", html: "'.Yii::t('app','Boat assigned successfully').'", type: "success"});
            window.closeModal();
          }else{
            swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
          }
          $.pjax.reload({container: "#grid-container", timeout: 2000});
          App.unblockUI($(_targetContainer));
        }
     });
     return false;
  });
');
?>
