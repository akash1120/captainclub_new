<?php
$this->registerJs('
$("body").on("beforeSubmit", "form#boat-required-form", function () {
  _targetContainer=$("#boat-required-form")
  App.blockUI({
    message: "'.Yii::t('app','Please wait...').'",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });

   var form = $(this);
   // return false if form still have some validation errors
   if (form.find(".has-error").length) {
      return false;
   }
   // submit form
   $.ajax({
      url: form.attr("action"),
      type: "post",
      data: form.serialize(),
      success: function (response) {
        if(response=="success"){
          swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Request saved successfully').'", type: "success"});
          window.closeModal();
        }else{
          swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
        }
        App.unblockUI($(_targetContainer));
      }
   });
   return false;
});
');
?>
