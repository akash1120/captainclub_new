<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BoatRequiredForm */
/* @var $form yii\widgets\ActiveForm */
$timeZones = Yii::$app->appHelperFunctions->timeSlotListArr;

$cities=Yii::$app->appHelperFunctions->cityListArr;
$txtJScript=Yii::$app->jsFunctions->getCityMarinaArr($cities);

$this->registerJs($txtJScript.'
$("#boatrequired-city_id").change(function() {
	$("#boatrequired-marina_id").html("");
	array_list=cities[$(this).val()];
	$("#boatrequired-marina_id").html("<option value=\"\">Select</option>");
	$(array_list).each(function (i) {
		$("#boatrequired-marina_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
	});
});
$("#boatrequired-marina_id").change(function() {
	$("#boatrequired-boat_id").html("");
	$.ajax({
		url: "'.Url::to(['suggestion/boats-list','marina_id'=>'']).'"+$(this).val(),
		dataType: "html",
		success: function(html) {
			$("#boatrequired-boat_id").html(html);
			$("#boatrequired-boat_id").select2({
		  	placeholder: "Search Boat",
		  	allowClear: true,
		  	width: "100%",
		  });
		},
		error: bbAlert
	});
});
$("#boatrequired-boat_type").change(function() {
	tval=$(this).val();
	if(tval=="all"){
		$("#rowByBoat").hide();
		$("#rowByBoat input").val("");
		$("#rowByTag").hide();
		$("#rowByTag input").val("");
	}
	if(tval=="byboat"){
		$("#rowByBoat").show();
		$("#boatrequired-boat_id").select2({
	  	placeholder: "Search Boat",
	  	allowClear: true,
	  	width: "100%",
	  });

		$("#rowByTag").hide();
		$("#rowByTag input").val("");
	}
	if(tval=="bytag"){
		$("#rowByTag").show();
		$("#boatrequired-tag_id").select2({
	  	placeholder: "Search Tags",
	  	allowClear: true,
	  	width: "100%",
	  });

		$("#rowByBoat").hide();
		$("#rowByBoat input").val("");
	}
});
$(".brdtpicker").datepicker({
	format: "yyyy-mm-dd",
	todayHighlight: true,
	startDate: "'.date("Y-m-d").'",
}).on("changeDate", function(e){
	$(this).datepicker("hide");
});
');
?>
<div class="boat-required-form">
	<?php $form = ActiveForm::begin(['id'=>'boat-required-form']); ?>
	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'city_id')->dropDownList($cities,['prompt'=>$model->getAttributeLabel('city_id')]) ?>
		</div>
		<div class="col-sm-6">
			<?= $form->field($model, 'marina_id')->dropDownList([],['prompt'=>$model->getAttributeLabel('marina_id')]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'date')->textInput(['class'=>'form-control brdtpicker','readonly'=>'readonly']) ?>
		</div>
		<div class="col-sm-6">
			<?= $form->field($model, 'time_id')->dropDownList($timeZones,['prompt'=>$model->getAttributeLabel('time_id')]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<?= $form->field($model, 'no_of_boats')->textInput() ?>
		</div>
		<div class="col-sm-6">
			<?= $form->field($model, 'boat_type')->dropDownList(Yii::$app->helperFunctions->boatRequiredOptions) ?>
		</div>
	</div>
	<div id="rowByBoat" style="display:none;">
		<?= $form->field($model, 'boat_id')->dropDownList([],['multiple'=>'multiple']) ?>
	</div>
	<div id="rowByTag" style="display:none;">
		<?= $form->field($model, 'tag_id')->dropDownList(Yii::$app->appHelperFunctions->boatTagsListArr,['multiple'=>'multiple']) ?>
	</div>
	<?= $form->field($model, 'comments')->textArea(['rows'=>4]) ?>
	<div class="form-group">
		<?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
