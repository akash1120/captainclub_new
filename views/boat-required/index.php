<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\assets\AppDateRangePickerAsset;
AppDateRangePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BoatRequiredSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if($searchModel->listType=='future'){
	$this->title = Yii::t('app', 'Active Requests');
}elseif($searchModel->listType=='history'){
	$this->title = Yii::t('app', 'Requests History');
}elseif($searchModel->listType=='deleted'){
	$this->title = Yii::t('app', 'Deleted Requests');
}

$this->params['breadcrumbs'][] = $this->title;
$gridBtnsArr='';

if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
	$gridBtnsArr.=' {delete}';
}

$this->registerJs('
initDtpicker();
$(document).on("pjax:success", function() {
	initDtpicker();
});
');

if($searchModel->city_id!=null){
  $marinaList=Yii::$app->appHelperFunctions->getActiveCityMarinaListArr($searchModel->city_id);
}else{
  $marinaList=Yii::$app->appHelperFunctions->activeMarinaListArr;
}

$columns[] = ['class' => 'yii\grid\SerialColumn'];
$columns[] = ['label'=>'City','attribute'=>'city_id','value' => function ($model){return $model->city->name;},'filter'=>Yii::$app->appHelperFunctions->cityListArr];
$columns[] = ['label'=>'Marina','attribute'=>'marina_id','value' => function ($model){return $model->port->name;},'filter'=>$marinaList];
$columns[] = ['format'=>'date','attribute'=>'date','filterInputOptions'=>['class'=>'form-control dtrpicker','autocomplete'=>'off']];
$columns[] = ['label'=>'Time','attribute'=>'time_id','value' => function ($model){return $model->timeZone->name;},'filter'=>Yii::$app->appHelperFunctions->timeSlotListArr];
$columns[] = 'no_of_boats';
$columns[] = ['format'=>'html','attribute'=>'boat_type','value'=>function($model){
	$typeHtml=Yii::$app->helperFunctions->boatRequiredOptions[$model['boat_type']];
	if($model->boat_type=='byboat'){
		$typeHtml.='<br />'.$model->boatsByIdz;
	}
	if($model->boat_type=='bytag'){
		$typeHtml.='<br />'.$model->boatsByTagz;
	}
	return $typeHtml;
},'filter'=>Yii::$app->helperFunctions->boatRequiredOptions];
$columns[] = 'comments:html';
$columns[] = ['format'=>'raw','attribute'=>'bookingsDone','contentOptions'=>['class'=>'nosd']];
if($searchModel->listType=='future'){
	$columns[] = [
		'class' => 'yii\grid\ActionColumn',
		'header'=>'Actions',
		'headerOptions'=>['class'=>'noprint','style'=>'width:20px;'],
		'contentOptions'=>['class'=>'noprint'],
		'template' => $gridBtnsArr,
		'buttons' => [
			'delete' => function ($url, $model) {
				return Html::a('<i class="fa fa-trash"></i>', $url, [
					'title' => Yii::t('app', 'Delete'),
					'class'=>'btn btn-xs btn-danger',
					'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
					'data-method'=>"post",
					'data-pjax'=>"0",
				]);
			},
		],
	];
}
?>
<div class="boat-required-index">
	<?php CustomPjax::begin(['id' => 'grid-container']) ?>
	<?= CustomTabbedGridView::widget([
		'import' => true,
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => $columns,
		'layout'=>"{items}<div>{summary}</div><div>{pager}</div>",
	]); ?>
	<?php CustomPjax::end() ?>
</div>
<script>
function initDtpicker(){
	$(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
	$(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
		$(this).trigger("change");
	});
	$(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('');
	});
}
</script>
<?= $this->render('/boat-required/js/boat_assign_scripts')?>
