<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app','Permission Denied!');
?>
<div class="alert alert-danger">
	<?= Yii::t('app','Sorry, you dont have permission to access this area!')?>
</div>