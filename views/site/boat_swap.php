<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BoatSwap */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
	$(".lookup-member").autocomplete({
    serviceUrl: "'.Url::to(['suggestion/member-search']).'",
		noCache: true,
      onSelect: function(suggestion) {
				if($("#boatswap-user_id").val()!=suggestion.data){
					$("#boatswap-user_id").val(suggestion.data);
					$("#lnkC2").html("Select booking of "+suggestion.value);
					$("#lnkC2").trigger("click");
					var _targetContainer="#collapsed2";
					App.blockUI({
						message: "'.Yii::t('app','Please wait...').'",
						target: _targetContainer,
						overlayColor: "none",
						cenrerY: true,
						boxed: true
					});
					$.ajax({
						url: "'.Url::to(['suggestion/member-bookings','id'=>'']).'"+suggestion.data,
						dataType: "html",
						success: function(html) {
							App.unblockUI($(_targetContainer));
							$("#member-bookings").html(html);
							accordionInit();
							$("#s1").removeClass("active").addClass("done");
							$("#s1 .toggle-content").hide();
							$("#s2").addClass("active");
							$("#s2 .toggle-content").show();
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
      },
      onInvalidateSelection: function() {
          $("#boatswap-user_id").val("");
      }
  });
	accordionInit();
');
?>
<style>
#member-bookings{max-height: 200px; overflow-y: auto; overflow-x: hidden; padding: 0 3px;}
</style>
<div class="boat-swap-form">
	<?php $form = ActiveForm::begin(['id'=>'boat-swap-form']); ?>
	<div class="hidden">
		<?= $form->field($model, 'selected_boat')->textInput() ?>
		<?= $form->field($model, 'user_id')->textInput() ?>
		<input type="text" id="boatswap-tmp_date" value="" />
	</div>
	<div class="toggle toggle-quaternary" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">
		<div id="s1" class="toggle active">
			<label>
				Select Member
			</label>
			<div class="toggle-content" style="">
				<?= $form->field($model, 'username')->textInput(['maxlength' => true, 'class' => 'form-control lookup-member', 'placeholder'=>$model->getAttributeLabel('user_id')])->label(false) ?>
			</div>
		</div>
		<div id="s2" class="toggle">
			<label>
				Select Bookings
			</label>
			<div class="toggle-content">
				<div id="member-bookings"></div>
			</div>
		</div>
		<div id="s3" class="toggle">
			<label>
				Select Boat & Reason
			</label>
			<div class="toggle-content">
				<div id="old-boat"></div>
				<?= $form->field($model, 'new_port_id')->dropDownList([]) ?>
				<?= $form->field($model, 'new_time')->dropDownList(Yii::$app->appHelperFunctions->timeSlotListDashboardArr) ?>
				<?= $form->field($model, 'new_boat')->dropDownList([]) ?>
				<?= $form->field($model, 'reason')->radioList(Yii::$app->helperFunctions->boatSwapReason); ?>
			</div>
		</div>
	</div>
	<div class="form-group">
	  <?= Html::submitButton('Save ', ['class' => 'btn btn-success']) ?>
	  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
	</div>
	<?php ActiveForm::end(); ?>
</div>
