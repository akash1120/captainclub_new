<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\PasswordResetRequestForm */

$this->title = Yii::t('app','Forgot Password?');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['id' => 'forget-form', 'options' => ['class'=>'forget-form1']]); ?>
    <h3><?= $this->title?></h3>
    <p>
         <?= Yii::t('app','Enter your registered email address to reset your password.')?>
    </p>
    <?= $form->field($model, 'email')->textInput(['class'=>'form-control form-control-solid placeholder-no-fix','autocomplete'=>'off','placeholder'=>$model->getAttributeLabel('email')])->label(false) ?>
    <div class="form-actions">
        <?= Html::a(Yii::t('app','Back'), ['site/login'], ['class' => 'btn btn-default', 'name' => 'back-btn', 'id' => 'back-btn']) ?>
        <?= Html::submitButton(Yii::t('app','Submit'), ['class' => 'btn btn-success uppercase pull-right', 'name' => 'fp-submit-button']) ?>
    </div>
<?php ActiveForm::end(); ?>
