<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use app\assets\AppAutoScrollAsset;
AppAutoScrollAsset::register($this);

/* @var $this yii\web\View */

$this->title = 'My Discounts';

$this->registerJs('
	var ias = $.ias({
	  container:  "#discount-list-view",
	  item:       ".item",
	  pagination: ".pagination",
	  next:       ".next a",
	  delay:      1200,
	  negativeMargin:0
	});
	ias.extension(new IASSpinnerExtension({
		html: "<div class=\"col-xs-12 col-sm-12 text-center\"><div class=\"loading-message loading-message-boxed\"><img src=\"images/loading.gif\" height=\"25\"><span>&nbsp;&nbsp;'.Yii::t('app','Loading').'</span></div></div>",
	}));
')
?>
<h3 class="text-center">Captain Enjoy the Discounts on the below outlets and Marinas.</h3><br />
<?= ListView::widget( [
	'dataProvider' => $dataProvider,
	'id' => 'discount-list-view',
	'itemOptions' => ['class' => 'item col-xs-12 col-sm-4'],
	'itemView' => '/discount/_item',
	'layout'=>"
		<div class=\"row\">{items}</div>
		{pager}
	",
] );
?>
<script>
function openModal(id,title){
  html = $("#descp"+id).html();
  viewPopUpModal(title,html);
}
</script>
