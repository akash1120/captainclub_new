<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\models\UserFreezeRequest;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

/* @var $this yii\web\View */

$this->registerJs('
	initScripts();
	$(document).on("pjax:success", function() {
	  initScripts();
	});
');

if($searchModel->listType=='future'){
  $this->title = Yii::t('app', 'Active Requests');
}elseif($searchModel->listType=='history'){
  $this->title = Yii::t('app', 'Requests History');
}

$this->params['breadcrumbs'][] = $this->title;

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'raw','attribute'=>'item_type','label'=>Yii::t('app','Detail'),'value'=>function($model){
  return Yii::$app->appHelperFunctions->getRequestDetail($model['item_type'],$model['id']);
}];
$columns[]=['format'=>'date','attribute'=>'created_at','label'=>Yii::t('app','Date Posted'),'filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off'],'headerOptions'=>['style'=>'width:150px;']];
$columns[]=['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
  return Yii::$app->helperFunctions->requestStatusIcon[$model['status']];
}];
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'headerOptions'=>['style'=>'width:100px;'],
  'template' => '{delete}',
  'buttons' => [
    'delete' => function ($url, $model) {
      if($model['user_id']==Yii::$app->user->identity->id){
        $showBtn=true;
        if(($model['item_type']=='other' || $model['item_type']=='boat-booking') && ($model['requested_date']!=null && date("Y-m-d",strtotime($model['requested_date']))<=date("Y-m-d")))$showBtn=false;

        if($model['status']==1 || $model['status']==-1)$showBtn=false;
        if($model['item_type']=='freeze' && $model['status']==1){
          $freezeReqModel=UserFreezeRequest::findOne(['id'=>$model['id']]);
          if($freezeReqModel['start_date']>date("Y-m-d")){
            $showBtn=true;
          }
        }
        if($showBtn==true){
          return Html::a('<i class="fa fa-trash"></i> Delete', 'javascript:;', [
            'title' => Yii::t('app', 'Delete'),
            'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-danger btn-del-request',
            'data-id'=>$model['id'],
            'data-item_type'=>$model['item_type'],
          ]);
        }
      }
    },
  ],
];
?>
<style>
.filters{display: none;}
@media (max-width: 480px) {
    .nav-tabs{
        display: block !important;
        padding-top: 40px;
    }
}
</style>
<div class="requests-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomTabbedGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => false,
    'columns' => $columns,
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<?= $this->render('/request/js/user_request_scripts')?>

<script>
function initScripts(){
	if($(".dtpicker").length>0){
	  $(".dtpicker").datepicker({
	  	format: "yyyy-mm-dd",
	  	todayHighlight: true,
	  }).on("changeDate", function(e){
	  	$(this).datepicker("hide");
	  });
	}
}
</script>
