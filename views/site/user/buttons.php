<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

$canBook=Yii::$app->user->identity->canBookBoat;
?>
<div class="row db-mbtns">
  <div class="col-12 col-sm-4">
    <a href="<?= $canBook==false ? 'javascript:;" class="low-balance"' : Url::to(['booking/create'])?>" title="Make a booking">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-edit"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Make a booking</h3>
        </div>
      </section>
    </a>
  </div>
  <div class="col-6 col-sm-4">
    <a href="javascript:;" title="Requests" data-toggle="modal" data-target="#request-modal">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-question"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Requests</h3>
        </div>
      </section>
    </a>
  </div>
  <div class="col-6 col-sm-4">
    <a href="javascript:;" title="Feedback" class="load-modal" data-url="<?= Url::to(['request/suggestion'])?>" data-heading="Suggestions / Complaints">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-envelope"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Feedback</h3>
        </div>
      </section>
    </a>
  </div>
  <!--div class="col-6 col-sm-4">
    <a href="<?= Url::to(['site/discount'])?>" title="My Discounts">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-cart-arrow-down"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">My Discounts</h3>
        </div>
      </section>
    </a>
  </div>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['account/update-profile'])?>" title="My Profile">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-user"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">My Profile / Membership Card</h3>
        </div>
      </section>
    </a>
  </div>
  <div class="col-6 col-sm-4">
    <a href="<?= Url::to(['booking/index','listType'=>'history'])?>" title="Booking & Request History">
      <section class="card mb-4">
        <header class="card-header bg-white">
          <div class="card-header-icon bg-primary">
            <i class="fa fa-archive"></i>
          </div>
        </header>
        <div class="card-body">
          <h3 class="mt-0 font-weight-bold mb-0 text-center">Booking & Request History</h3>
        </div>
      </section>
    </a>
  </div-->
</div>

<?= $this->render('/request/selection',['user'=>Yii::$app->user->identity])?>
<?= $this->render('/request/js/selection_scripts')?>
<?= $this->render('/booking/js/alert_script')?>
<?= $this->render('/request/js/freeze_scripts')?>
<?= $this->render('/request/js/other_request_scripts')?>
<?= $this->render('/request/js/night_drive_training_request_scripts')?>
<?= $this->render('/request/js/suggestion_scripts')?>
<?= $this->render('/request/js/package_city_restriction_script')?>
<?= $this->render('/request/js/boat_booking_scripts')?>
<script>
</script>
