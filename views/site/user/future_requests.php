<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use app\components\widgets\CustomGridView;
use app\models\RequestsSearch;
use app\models\UserFreezeRequest;

$searchModel=new RequestsSearch;
$searchModel->listType='future';
$searchModel->user_id=Yii::$app->user->identity->id;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageParam = 'fr-page';

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'raw','attribute'=>'item_type','label'=>Yii::t('app','Detail'),'value'=>function($model){
  return Yii::$app->appHelperFunctions->getRequestDetail($model['item_type'],$model['id']);
}];
$columns[]=['format'=>'date','attribute'=>'created_at','label'=>Yii::t('app','Date Posted'),'headerOptions'=>['style'=>'width:150px;']];
$columns[]=['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
  if($model['item_type']=='bookingice' || $model['item_type']=='bookingkidsjackets'){
    return '<span class="badge grid-badge badge-info">Noted</span>';
  }else{
    return Yii::$app->helperFunctions->userRequestStatusIcon[$model['status']];
  }
}];
$columns[]=['format'=>'raw','label'=>Yii::t('app','Estimated Time Of Reply'),'value'=>function($model){
  return Yii::$app->appHelperFunctions->getRequestEstimatedTimeOfReply($model['item_type'],$model['id']);
}];
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>'Actions',
  'headerOptions'=>['style'=>'width:100px;'],
  'contentOptions'=>['class'=>'nosd'],
  'template' => '{delete}',
  'buttons' => [
    'delete' => function ($url, $model) {
      if($model['user_id']==Yii::$app->user->identity->id){
        if(
          $model['item_type']=='waitingcaptain'
          || $model['item_type']=='waitingequipment'
          || $model['item_type']=='waitingbbq'
          || $model['item_type']=='waitingwakeboarding'
          || $model['item_type']=='waitingwakesurfing'
          || $model['item_type']=='waitingearlydeparture'
          || $model['item_type']=='waitingovernightcamping'
          || $model['item_type']=='waitinglatearrival'
        ){
          $showBtn=true;
          if(in_array($model['status'],[1,-1,2,3]))$showBtn=false;
          if($showBtn==true){
            $clsName='';
            if($model['item_type']=='waitingcaptain')$clsName='cancel-captain-waiting';
            if($model['item_type']=='waitingequipment')$clsName='cancel-equipment-waiting';
            if($model['item_type']=='waitingbbq')$clsName='cancel-bbq-waiting';
            if($model['item_type']=='waitingwakeboarding')$clsName='cancel-wake-boarding-waiting';
            if($model['item_type']=='waitingwakesurfing')$clsName='cancel-wake-surfing-waiting';
            if($model['item_type']=='waitingearlydeparture')$clsName='cancel-early-departure-waiting';
            if($model['item_type']=='waitingovernightcamping')$clsName='cancel-overnight-camping-waiting';
            if($model['item_type']=='waitinglatearrival')$clsName='cancel-late-arrival-waiting';
            $html = '<span class="item-row" data-key="'.$model['booking_id'].'">';
            $html.= Html::a('<i class="fa fa-trash"></i> Delete', 'javascript:;', [
              'title' => Yii::t('app', 'Delete'),
              'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-danger '.$clsName,
              'data-id'=>$model['id'],
              'data-item_type'=>$model['item_type'],
            ]);
            $html.= '</span>';
            return $html;
          }
        }else{
          $showBtn=true;
          if(($model['item_type']=='other' || $model['item_type']=='boat-booking') && ($model['requested_date']!=null && date("Y-m-d",strtotime($model['requested_date']))<=date("Y-m-d")))$showBtn=false;

          if(in_array($model['status'],[1,-1,2,3]))$showBtn=false;
          if($model['item_type']=='freeze' && $model['status']==1){
            $freezeReqModel=UserFreezeRequest::findOne(['id'=>$model['id']]);
            if($freezeReqModel['start_date']>date("Y-m-d")){
              $showBtn=true;
            }
          }
          if($showBtn==true){
            return Html::a('<i class="fa fa-trash"></i> Delete', 'javascript:;', [
              'title' => Yii::t('app', 'Delete'),
              'class'=>'mb-xs mt-xs mr-xs btn btn-xs btn-danger btn-del-request',
              'data-id'=>$model['id'],
              'data-item_type'=>$model['item_type'],
            ]);
          }
        }
      }
    },
  ],
];
?>
<?php CustomPjax::begin(['id'=>'db-user-request-container']); ?>
<section class="card card-featured card-featured-warning mb-4">
  <header class="card-header">
    <h2 class="card-title">Future Requests</h2>
  </header>
  <div class="card-body">
    <?= CustomGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'cardHeader' => false,
      'columns' => $columns,
    ]); ?>
  </div>
</section>
<?php CustomPjax::end(); ?>
<?= $this->render('/request/js/user_request_scripts')?>
