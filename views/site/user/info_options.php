<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use app\models\UserLicense;
use app\models\City;
use app\assets\MemberDashboardAssets;
MemberDashboardAssets::register($this);

$maxPackageEndDate=Yii::$app->user->identity->maxPackageEndDate;
$expiryAlert=Yii::$app->helperFunctions->expiryAlertDays;

$remainingDays=Yii::$app->helperFunctions->getNumberOfDaysBetween(date("Y-m-d"),$maxPackageEndDate);

$myContractFreezingStats=Yii::$app->statsFunctions->getFreezeDaysByContract(Yii::$app->user->identity->active_contract_id);
$remainingFreezeDays=$myContractFreezingStats['remaining'];

$myContractCaptainsStats=Yii::$app->statsFunctions->getCaptainsByContract(Yii::$app->user->identity->active_contract_id);
$remainingCaptains=$myContractCaptainsStats['remaining'];

$creditBalance=Yii::$app->user->identity->profileInfo->credit_balance;

$this->registerJs('
if($(".blink").length){
  blink(".blink");
}
');
?>
<?php CustomPjax::begin(['id'=>'db-user-infOpt-container']); ?>
<div class="row">
  <?php if ($remainingDays <= 60) { ?>
  <div class="col-xs-12 col-sm-3">
    <div class="alert alert-<?= $remainingDays<=$expiryAlert ? 'danger' : 'info'?>">
      <div<?= $remainingDays<=$expiryAlert ? ' class="blink"' : ''?>>
        <strong>Renewal Date:</strong> <?= Yii::$app->formatter->asDate($maxPackageEndDate)?>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php $freezePeriod=Yii::$app->user->identity->activeFreezePeriod;
  if($freezePeriod!=null){?>
  <div class="col-xs-12 col-sm-3">
    <div class="alert alert-warning">
      <strong>Your Account is on the freezing mode,</strong>
      from <strong><?= Yii::$app->formatter->asDate($freezePeriod['start_date'])?></strong> to
      <strong><?= Yii::$app->formatter->asDate($freezePeriod['end_date'])?></strong>
      <?php if(date("Y-m-d")<$freezePeriod['end_date']){?>
      (<a href="javascript:;" class="btn-del-request" data-id="<?= $freezePeriod['id']?>" data-item_type="<?= 'freeze'?>">Unfreeze</a>)
      <?php }?>
    </div>
  </div>
  <?php }else{?>
  <div class="col-xs-12 col-sm-3">
    <div class="alert alert-info">
      <strong>Freezing Days Remaining:</strong> <?= $remainingFreezeDays?>
      <?php if($remainingFreezeDays>0){?>
      (<a href="javascript:;" class="load-modal" data-url="<?= Url::to(['request/freeze'])?>" data-heading="Freeze Account">Freeze</a>)
      <?php }?>
    </div>
  </div>
  <?php }?>
  <div class="col-xs-12 col-sm-3">
    <div class="alert alert-info">
      <strong>Free Captains Remaining:</strong> <?= $remainingCaptains?>
    </div>
  </div>
  <div class="col-xs-12 col-sm-3">
    <div class="alert alert-<?= $creditBalance<0 ? 'danger' : 'success'?>">
      <strong>Fuel Credit:</strong> <?= $creditBalance?>
      (<a href="javascript:;" class="load-modal" data-url="<?= Url::to(['order/create'])?>" data-heading="Deposit Funds">Deposit Funds</a>)
    </div>
  </div>
</div>
<?php CustomPjax::end(); ?>
<?= $this->render('/order/js/scripts')?>
