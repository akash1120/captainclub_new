<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
use yii\widgets\ListView;

use app\components\widgets\CustomGridView;
use app\models\BookingSearch;

$searchModel=new BookingSearch;
$searchModel->listType='future';
$searchModel->user_id=Yii::$app->user->identity->id;
$searchModel->status=1;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageParam = 'fb-page';
?>
<?php CustomPjax::begin(['id'=>'db-user-booking-container']); ?>
<h2 class="nc-heading">Future Bookings</h2>
<?= ListView::widget( [
  'dataProvider' => $dataProvider,
  'id' => 'fb-list-view',
  'itemOptions' => function ($model, $index, $widget, $grid){
    return ['class' => 'fb-item item-row col-sm-6 col-md-4','data-city_id'=>$model->city_id, 'data-marina_id'=>$model->port_id];
  },
  'itemView' => '/booking/booking_card_item',
  'layout'=>"
    <div class=\"row\">
      {items}
    </div>
    {pager}
  ",
  ] );
?>
<?php CustomPjax::end(); ?>
<?= $this->render('/booking/js/list_script')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
