<?php

/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<?= Yii::$app->user->identity->user_type!=0 ? $this->render('admin_options',['tab'=>$tab,'city'=>$city,'date'=>$date,'modelOpr'=>$modelOpr]) : ''?>
<?= Yii::$app->user->identity->user_type==0 ? $this->render('user/info_options') : ''?>
<?= Yii::$app->user->identity->user_type==0 ? $this->render('user/buttons') : ''?>
<?= Yii::$app->user->identity->user_type==0 ? $this->render('user/future_bookings') : ''?>
<?= Yii::$app->user->identity->user_type==0 ? $this->render('user/future_requests') : ''?>
