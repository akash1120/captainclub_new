<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\DashboardPlan;
use app\assets\DashboardPlanAssets;
DashboardPlanAssets::register($this);

$model = new DashboardPlan;
$model->load(Yii::$app->request->queryParams);

$citiesList=Yii::$app->appHelperFunctions->activeCityList;
?>
<div class="row">
  <div class="col-sm-4">
    <?php $form = ActiveForm::begin(['id'=>'dashboardPlan-form']); ?>
    <div class="hidden">
      <?= $form->field($model, 'forCity')->textInput() ?>
    </div>
    <?= $form->field($model, 'forDate',['template'=>'
    <div class="input-group">
    <span class="input-group-prepend"><span class="input-group-text">Date</span></span>
    {input}
    </div>
    {error}'])->textInput(['class'=>'form-control ddtpicker','readonly'=>'readonly']) ?>
    <?php ActiveForm::end(); ?>
  </div>
  <div class="col-sm-8 text-right">
    <a href="<?= Url::to(['booking/all','BookingSearch[booking_date]'=>$model->forDate.' - '.$model->forDate])?>" target="_blank" data-pjax="0" class="btn btn-success">All Bookings</a>
    <a href="javascript:;" class="btn btn-success load-modal" data-url="<?= Url::To(['site/boat-swap'])?>" data-heading="Swap Boat">Swap</a>
    <a href="javascript:;" class="btn btn-success load-modal" data-url="<?= Url::To(['boat-required/create'])?>" data-heading="Boat Required">Boat Required</a>
    <a href="javascript:;" class="btn btn-success load-modal" data-url="<?= Url::To(['booking/new-user-booking'])?>" data-heading="New Booking">Assign Request</a>
  </div>
</div>
<style>
.dbpr td{vertical-align: top;}
</style>
<div class="tabs">
  <ul class="nav nav-tabs">
    <?php foreach($citiesList as $city){?>
      <li class="nav-item<?= $model->forCity==$city['id'] ? ' active' : ''?>">
        <a class="nav-link" href="<?= Url::to(['site/index','DashboardPlan[forCity]'=>$city['id'],'DashboardPlan[forDate]'=>$model->forDate])?>" data-id="<?= $city['id']?>" class="dbcity"><?= $city['name']?></a>
      </li>
    <?php }?>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active">
      <?php
      $bulkBookedBoats=$model->marinaBulkBookedBoats;
      if($bulkBookedBoats!=null){
        echo '<table class="table table-bordered" style="margin:5px 0;">';
        foreach($bulkBookedBoats as $marina){
          echo '<tr>';
          echo '<td style="line-height:25px;">&nbsp;<strong>'.$marina['name'].':</strong>';
          foreach($marina['boats'] as $boats){
            echo '&nbsp;'.Html::a($boats['name'],['bulk-booking/member-bookings','date'=>$model->forDate,'boat_id'=>$boats['id']],['class'=>'btn btn-xs btn-info', 'data-pjax'=>0,'target'=>'_blank']);
          }
          echo '</td>';
          echo '</tr>';
        }
        echo '</table>';
      }
      ?>
      <table class="table table-bordered t1able-striped">
        <tr>
          <th width="100"><?= Yii::$app->formatter->asDate($model->forDate)?></th>
          <th width="300">
            <table class="table">
              <tr>
                <td width="140">Admin</td>
                <td width="140">Remarks</td>
                <td width="10">Del</td>
              </tr>
            </table>
          </th>
          <th width="165">Free</th>
          <th width="165">Bulk Booking</th>
          <th>
            <table class="table">
              <tr>
                <td>Name</td>
                <td width="140">Status</td>
                <td width="10">Action</td>
              </tr>
            </table>
          </th>
        </tr>
        <?php
        $timeSlots=$model->timeSlots;
        if($timeSlots!=null){
          $n=1;
          foreach($timeSlots as $timeSlot){
            if($n>1)echo '<tr><td colspan="12" style="background:#000;"></td></tr>';
        ?>
        <tr class="dbpr"<?= $timeSlot['bg']!='' ? ' style="background-color:'.$timeSlot['bg'].'"' : ''?>>
          <td><?= $timeSlot['title'].(isset($timeSlot['boat_required_request']) ? '<br />'.$timeSlot['boat_required_request'] : '')?></td>
          <td>
            <?php
            //Admin Bookings
            if(isset($timeSlot['admin_bookings']) && $timeSlot['admin_bookings']!=null){
              echo '<table class="table table-bordered col-table-container">';
              $n=0;
              foreach($timeSlot['admin_bookings'] as $adminBooking){
              ?>
              <tr<?= ($n>1 ? ' class="vmore" style="display:none;"' : '')?> data-key="<?= $adminBooking['id']?>">
                <td width="140"><?= $adminBooking['boat'].($adminBooking['is_bulk']==1 ? '<span class="badge badge-info">Bulk Booking</span>' : '')?></td>
                <td width="140"><?php
                echo $adminBooking['remarks'];
                if(Yii::$app->user->identity->user_type!=0 && Yii::$app->menuHelperFunction->checkActionAllowed('update-comments','booking')){
          				echo '<br /><a href="javascript:;" class="update-comments"><i class="fa fa-edit"></i></a>';
          			}
                ?></td>
                <td width="10"><a href="<?= Url::to(['booking/delete','id'=>$adminBooking['id'],'p'=>'d'])?>" data-method="post" onclick="javascript:if(confirm('Are you sure you want to delete this?')){return true;}else{return false;}">Del</a></td>
              </tr>
              <?php
                $n++;
              }
              if($n>2){
                echo '<tr class="vmore" style="display:none;"><td colspan="3"><a href="javascript:;" class="db-view-less">view less</a></td></tr>';
                echo '<tr class="vall"><td colspan="3"><a href="javascript:;" class="db-view-all">view all('.$n.')</a></td></tr>';
              }
              echo '</table>';
            }
            ?>
          </td>
          <td>
            <?php
            //Free Boats
            if(isset($timeSlot['free_boats']) && $timeSlot['free_boats']!=null){
              echo '<table class="table col-table-container">';
              $n=0;
              foreach($timeSlot['free_boats'] as $freeBoat){
                echo '<tr'.($n>1 ? ' class="vmore" style="display:none;"' : '').'><td>'.$freeBoat['boat'].'</td></tr>';
                $n++;
              }
              if($n>2){
                echo '<tr class="vmore" style="display:none;"><td><a href="javascript:;" class="db-view-less">view less</a></td></tr>';
                echo '<tr class="vall"><td><a href="javascript:;" class="db-view-all">view all('.$n.')</a></td></tr>';
              }
              echo '</table>';
            }
            ?>
          </td>
          <td>
            <?php
            //Bulk Booking replacements
            if(isset($timeSlot['bulk_bookings']) && $timeSlot['bulk_bookings']!=null){
              echo '<table class="table col-table-container">';
              $n=0;
              foreach($timeSlot['bulk_bookings'] as $bulkBooking){
                $reason='';
                if($bulkBooking['booking_type']!='' && $bulkBooking['booking_type']>0){
                  $reason=' / <span class="badge badge-info">'.($bulkBooking['booking_type']>0 ? Yii::$app->helperFunctions->adminBookingTypes[$bulkBooking['booking_type']].': '.$bulkBooking['booking_comments'] : '').'</span>';
                }
                echo '<tr'.($n>1 ? ' class="vmore" style="display:none;"' : '').'>';
                echo '  <td>';
                echo '    <a href="javascript:;" class="load-modal" data-heading="Replace Boat" data-url="'.Url::to(['bulk-booking/swap','id'=>$bulkBooking['id']]).'">';
                echo '      '.$bulkBooking['fullname'].' > '.$bulkBooking['boat'].$reason.'</a>';
                echo '    </a>';
                echo '  </td>';
                echo '</tr>';
                $n++;
              }
              if($n>2){
                echo '<tr class="vmore" style="display:none;"><td><a href="javascript:;" class="db-view-less">view less</a></td></tr>';
                echo '<tr class="vall"><td><a href="javascript:;" class="db-view-all">view all('.$n.')</a></td></tr>';
              }
              echo '</table>';
            }
            ?>
          </td>
          <td>
            <?php
            //User Requests
            if(isset($timeSlot['user_requests']) && $timeSlot['user_requests']!=null){
              echo '<table class="table">';
              foreach($timeSlot['user_requests'] as $userRequest){
                $usersName=$userRequest['firstname'].($userRequest['lastname']!='' ? ' '.$userRequest['lastname'] : '');
                if($usersName==''){
                  $usersName=$userRequest['username'];
                }

                if($userRequest['isPending']==true){
                  $userRequestUser='<a href="javascript:;" class="load-modal" data-url="'.Url::to(['user-requests/boat-assign','item_type'=>'boatbooking','id'=>$userRequest['id']]).'" data-heading="Booking Request" data-request_id="'.$userRequest['id'].'">'.$usersName.'</a>';
                }else{
                  $userRequestUser=''.$usersName.'';
                }
              ?>
              <tr data-key="<?= $userRequest['id']?>">
                <td><?= $userRequestUser?></td>
                <td width="140"><?= $userRequest['txtStatus']?></td>
                <td width="10">
                  <div class="btn-group flex-wrap">
                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <?php
                      if(Yii::$app->menuHelperFunction->checkActionAllowed('discussion','user-requests')){
                        if($userRequest['status']==0){
                          echo Html::a('<i class="fa fa-comments"></i> Discussion', 'javascript:;', [
                            'title' => Yii::t('app', 'Discussion'),
                            'class'=>'dropdown-item text-1',
                            'onclick'=>"window.open('".Url::to(['user-requests/discussion','item_type'=>'boatbooking','id'=>$userRequest['id']])."')",
                            'data-heading'=>Yii::t('app','Request Action'),
                          ]);
                        }
                      }
                      ?>
                      <?php
                      if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action','user-requests')){
                        if($userRequest['status']==0){
                          echo Html::a('<i class="fa fa-check"></i> Met', 'javascript:;', [
                            'title' => Yii::t('app', 'Met'),
                            'class'=>'dropdown-item text-1 load-modal',
                            'data-url'=>Url::to(['user-requests/request-action','s'=>1,'item_type'=>'boatbooking','id'=>$userRequest['id']]),
                            'data-heading'=>Yii::t('app','Request Action'),
                          ]);
                        }
                      }
                      ?>
                      <?php
              				if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action','user-requests')){
              					if($userRequest['status']==0){
              						echo Html::a('<i class="fa fa-times"></i> Not Met', 'javascript:;', [
              							'title' => Yii::t('app', 'Not Met'),
                            'class'=>'dropdown-item text-1 load-modal',
                            'data-url'=>Url::to(['user-requests/request-action','s'=>2,'item_type'=>'boatbooking','id'=>$userRequest['id']]),
                            'data-heading'=>Yii::t('app','Request Action'),
              						]);
              					}
              				}
                      ?>
                      <?php
              				if(Yii::$app->menuHelperFunction->checkActionAllowed('request-action','user-requests')){
              					if($userRequest['status']==0){
              						echo Html::a('<i class="fa fa-exclamation"></i> Cancelled', 'javascript:;', [
              							'title' => Yii::t('app', 'Cancelled'),
                            'class'=>'dropdown-item text-1 load-modal',
                            'data-url'=>Url::to(['user-requests/request-action','s'=>3,'item_type'=>'boatbooking','id'=>$userRequest['id']]),
                            'data-heading'=>Yii::t('app','Request Action'),
              						]);
              					}
              				}
                      ?>
                      <?php
              				if($userRequest['status']==0 && Yii::$app->menuHelperFunction->checkActionAllowed('delete','user-requests')){
              					echo Html::a('<i class="fa fa-trash"></i> Delete', 'javascript:;', [
              						'title' => Yii::t('app', 'Delete'),
              						'class'=>'dropdown-item text-1 btn-delrequest',
                          'data-id'=>$userRequest['id'],
                          'data-item_type'=>'boatbooking',
              					]);
              				}
                      ?>
                      <?php
                			if(Yii::$app->menuHelperFunction->checkActionAllowed('bookings','user')){
                				echo Html::a('<i class="fa fa-anchor"></i> Active Bookings', 'javascript:;', [
                					'title' => Yii::t('app', 'Active Bookings'),
                          'class'=>'dropdown-item text-1',
                					'onclick'=>"window.open('".Url::to(['user/bookings','id'=>$userRequest['user_id'],'listType'=>'future'])."')",
                					'target'=>'_blank',
                				]);
                			}
                      ?>
                    </div>
                  </div>
                </td>
              </tr>
              <?php
              }
              echo '</table>';
            }
            ?>
          </td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
    </div>
  </div>
</div>
