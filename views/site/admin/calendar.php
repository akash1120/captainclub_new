<?php
use yii\helpers\Url;
use app\assets\AppFullCalendarAsset;
AppFullCalendarAsset::register($this);

$this->registerJs('
  var calendarEl = document.getElementById("calendar");
  var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: ["interaction", "dayGrid", "timeGrid", "list" ],
      header: {
        left: "prev,next today",
        center: "title",
        right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
      },
      defaultDate: "'.date("Y-m-d").'",
      editable: true,
      navLinks: true, // can click day/week names to navigate views
      eventLimit: true, // allow "more" link when too many events
      events: {
        url: "'.Url::to(['user-task/calendar-json']).'",
        failure: function() {
          document.getElementById("script-warning").style.display = "block"
        }
      },
      eventRender: function(info) {
        var tooltip = new Tooltip(info.el, {
          title: info.event.extendedProps.description,
          html: true,
          placement: "top",
          trigger: "hover",
          container: "body"
        });
      },
      eventClick:  function(info) {
        $("#general-modal h5").html(info.event.title);
        colSize=6;
        if(info.event.extendedProps.isfeedback==1){
          colSize=4;
        }
        html = info.event.extendedProps.description+"<br /><br />";
        html+= "<div class=\"row\">";
        html+= "  <div class=\"col-sm-"+colSize+" text-left\">";
        html+= "    <a class=\"btn btn-sm btn-info\" href=\"'.Url::to(['user-task/index','id'=>'']).'"+info.event.extendedProps.user_id+"\" target=\"_blank\">All Tasks</a>";
        html+= "  </div>";
        if(info.event.extendedProps.isfeedback==1){
        html+= "  <div class=\"col-sm-4 text-center\">";
        html+= "    <button class=\"btn btn-sm btn-primary btn-feedback\" data-userid=\""+info.event.extendedProps.user_id+"\" data-username=\""+info.event.extendedProps.username+"\">Feedback</button>";
        html+= "  </div>";
        }
        html+= "  <div class=\"col-sm-"+colSize+" text-right\">";
        html+= "    <button class=\"btn btn-sm btn-success btn-task\" data-key=\""+info.event.id+"\" data-title=\""+info.event.title+"\" data-descp=\""+info.event.extendedProps.description+"\">Update</button>";
        html+= "  </div>";
        html+= "</div>";
        $("#general-modal .modalContent").html(html);
        $("#general-modal").modal();
      },
    });
    calendar.render();
');
?>
<style>

  .popper,
  .tooltip {
    position: absolute;
    z-index: 9999;
    background: #FFC107;
    color: black;
    width: 150px;
    border-radius: 3px;
    box-shadow: 0 0 2px rgba(0,0,0,0.5);
    padding: 0px;
    text-align: center;
    opacity: inherit;
  }
  .style5 .tooltip {
    background: #1E252B;
    color: #FFFFFF;
    max-width: 200px;
    width: auto;
    font-size: .8rem;
    padding: .5em 1em;
  }
  .popper .popper__arrow,
  .tooltip .tooltip-arrow {
    width: 0;
    height: 0;
    border-style: solid;
    position: absolute;
    margin: 5px;
  }

  .tooltip .tooltip-arrow,
  .popper .popper__arrow {
    border-color: #FFC107;
  }
  .style5 .tooltip .tooltip-arrow {
    border-color: #1E252B;
  }
  .popper[x-placement^="top"],
  .tooltip[x-placement^="top"] {
    margin-bottom: 5px;
  }
  .popper[x-placement^="top"] .popper__arrow,
  .tooltip[x-placement^="top"] .tooltip-arrow {
    border-width: 5px 5px 0 5px;
    border-left-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    bottom: -5px;
    left: calc(50% - 5px);
    margin-top: 0;
    margin-bottom: 0;
  }
  .popper[x-placement^="bottom"],
  .tooltip[x-placement^="bottom"] {
    margin-top: 5px;
  }
  .tooltip[x-placement^="bottom"] .tooltip-arrow,
  .popper[x-placement^="bottom"] .popper__arrow {
    border-width: 0 5px 5px 5px;
    border-left-color: transparent;
    border-right-color: transparent;
    border-top-color: transparent;
    top: -5px;
    left: calc(50% - 5px);
    margin-top: 0;
    margin-bottom: 0;
  }
  .tooltip[x-placement^="right"],
  .popper[x-placement^="right"] {
    margin-left: 5px;
  }
  .popper[x-placement^="right"] .popper__arrow,
  .tooltip[x-placement^="right"] .tooltip-arrow {
    border-width: 5px 5px 5px 0;
    border-left-color: transparent;
    border-top-color: transparent;
    border-bottom-color: transparent;
    left: -5px;
    top: calc(50% - 5px);
    margin-left: 0;
    margin-right: 0;
  }
  .popper[x-placement^="left"],
  .tooltip[x-placement^="left"] {
    margin-right: 5px;
  }
  .popper[x-placement^="left"] .popper__arrow,
  .tooltip[x-placement^="left"] .tooltip-arrow {
    border-width: 5px 0 5px 5px;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    right: -5px;
    top: calc(50% - 5px);
    margin-left: 0;
    margin-right: 0;
  }
  .tooltip-inner hr{margin: 0px; background: #ccc;}
</style>
<div id="calendar"></div>

<?= $this->render('/user-feedback/js/feedback_scripts')?>
<?= $this->render('/user-note/js/_scripts')?>
