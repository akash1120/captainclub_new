<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\models\BookingAlertSearch;

$searchModel=new BookingAlertSearch;
$searchModel->alert_type=$alert_type;
$searchModel->listType='active';
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageParam = 'ba-page';

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'date','attribute'=>'alert_date','headerOptions'=>['style'=>'width:110px']];
$columns[]=['attribute'=>'port_id','value' => function ($model){return $model->marina->name;},'headerOptions'=>['style'=>'width:100px'],'filter'=>Yii::$app->appHelperFunctions->marinaListArr];
$columns[]='message:text';
$columns[]=['format'=>'raw','attribute'=>'timingHtml','headerOptions'=>['style'=>'width:210px'],'filter'=>Yii::$app->appHelperFunctions->timeSlotListArr];

?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'cardHeader' => false,
  'columns' => $columns,
]); ?>
