<?php
use yii\helpers\Url;
$cities=Yii::$app->appHelperFunctions->cityListArr;
$txtJScript=Yii::$app->jsFunctions->getCityMarinaArr($cities);

$subBoatSelectionBoatIds = json_encode(Yii::$app->appHelperFunctions->subBoatSelectionBoatIds);

$this->registerJs($txtJScript.'
  initScripts();
  $(document).on("pjax:success", function() {
    initScripts();
  });
  $(document).delegate(".db-view-all","click",function(){
    $(this).parents("table.col-table-container").find(".vmore").show();
    $(this).parents("table.col-table-container").find(".vall").hide();
  });
  $(document).delegate(".db-view-less","click",function(){
    $(this).parents("table.col-table-container").find(".vmore").hide();
    $(this).parents("table.col-table-container").find(".vall").show();
  });
	$(document).delegate("#newuserbooking-city_id", "change", function() {
		cval=$(this).val();
		$("#newuserbooking-marina_id").html("<option>'.Yii::t('app','Select').'</option>");
    array_list=cities[$(this).val()];
  	$(array_list).each(function (i) {
  		$("#newuserbooking-marina_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
  	});
  	$("#newuserbooking-selected_boat").val("0");
	});
	$(document).delegate("#newuserbooking-marina_id", "change", function() {
    marina=$("#newuserbooking-marina_id").val();
    date=$("#newuserbooking-date").val();
    time=$("#newuserbooking-time_id").val();
  	$("#newuserbooking-selected_boat").val("0");
    loadFreeBoats(marina,date,time,".modal-content","newuserbooking-boat_id","");
	});
	$(document).delegate("#newuserbooking-time_id", "change", function() {
    marina=$("#newuserbooking-marina_id").val();
    date=$("#newuserbooking-date").val();
    time=$("#newuserbooking-time_id").val();
  	$("#newuserbooking-selected_boat").val("0");
    loadFreeBoats(marina,date,time,".modal-content","newuserbooking-boat_id","");
	});
	$(document).delegate("#newuserbooking-date", "change", function() {
    marina=$("#newuserbooking-marina_id").val();
    date=$("#newuserbooking-date").val();
    time=$("#newuserbooking-time_id").val();
  	$("#newuserbooking-selected_boat").val("0");
    loadFreeBoats(marina,date,time,".modal-content","newuserbooking-boat_id","");
	});
  $(document).delegate("#newuserbooking-boat_id", "change", function() {
  	boatId=$(this).val();
  	subBoatSelectionBoatIds='.$subBoatSelectionBoatIds.';
  	if($.inArray(boatId,subBoatSelectionBoatIds) !== -1){
  		marina_id=$("#newuserbooking-marina_id").val();
  		date=$("#newuserbooking-date").val();
  		boatName=$("#newuserbooking-boat_id option:selected" ).text();
  		fld="newuserbooking";
  		openBoatSelection(marina_id,date,boatName,fld)
  	}else{
  		$("#newuserbooking-selected_boat").val("0");
  	}
  });

	$("body").on("beforeSubmit", "form#new-user-booking-form", function () {
		_targetContainer=$("#new-user-booking-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Booking saved successfully').'", type: "success"});
					  window.closeModal();
				  }else{
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }
				  App.unblockUI($(_targetContainer));

          if($("#grid-container").length){
            $.pjax.reload({container: "#grid-container", timeout: 2000});
          }
			  }
		 });
		 return false;
	});
');
?>

<script>
function initScripts(){
	$('[data-toggle="popover"]').popover({
		trigger: "focus",
		html: true,
	});
	if($(".ddtpicker").length>0){
    $(".ddtpicker").datepicker({
      format: "yyyy-mm-dd",
      todayHighlight: true,
    }).on("changeDate", function(e){
      $(this).datepicker("hide");
      window.location.href="<?= Url::to(['site/index','tab'=>'dashboard'])?>&DashboardPlan[forCity]="+$("#dashboardplan-forcity").val()+"&DashboardPlan[forDate]="+$("#dashboardplan-fordate").val();
    });
  }
	if($(".dtpicker").length>0){
	  $(".dtpicker").datepicker({
	  	format: "yyyy-mm-dd",
	  	todayHighlight: true,
	  	startDate: "today",
	  }).on("changeDate", function(e){
	  	$(this).datepicker("hide");
	  });
	}
	if($(".dtrpicker").length>0){
		$(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
		$(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
			$(this).trigger("change");
		});
		$(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
	}
}
</script>
