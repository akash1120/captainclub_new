<?php
use yii\helpers\Url;

$subBoatSelectionBoatIds = json_encode(Yii::$app->appHelperFunctions->subBoatSelectionBoatIds);

$this->registerJs('
  $(document).delegate("#boatswap-new_port_id", "change", function() {
    marina=$("#boatswap-new_port_id").val();
    date=$("#boatswap-tmp_date").val();
    time=$("#boatswap-new_time").val();
  	$("#boatswap-selected_boat").val("0");
    loadFreeBoats(marina,date,time,".modal-content","boatswap-new_boat","");
  });
  $(document).delegate("#boatswap-new_time", "change", function() {
    marina=$("#boatswap-new_port_id").val();
    date=$("#boatswap-tmp_date").val();
    time=$("#boatswap-new_time").val();
  	$("#boatswap-selected_boat").val("0");
    loadFreeBoats(marina,date,time,".modal-content","boatswap-new_boat","");
  });
	$(document).delegate(".b_rb", "change", function() {
		id=$(this).data("booking_id");
		city=$(this).data("city_id");
		marina=$(this).data("marina_id");
		time=$(this).data("time_id");
    date=$(this).data("booking_date");
    $("#boatswap-tmp_date").val(date);
  	$("#boatswap-selected_boat").val("0");

		$("#boatswap-new_time").find("option[value=\""+time+"\"]").attr("selected",true);
		$("#boatswap-new_port_id").html("");

    array_list=cities[city];
  	$(array_list).each(function (i) {
  		$("#boatswap-new_port_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
  	});
    $("#boatswap-new_port_id").find("option[value=\""+marina+"\"]").attr("selected",true);

		boatDetail=$(this).data("booking");
    $("#old-boat").html("<strong>Booking Detail:</strong><br />"+boatDetail+"<hr />");

    $("#s2").removeClass("active").addClass("done");
    $("#s2 .toggle-content").hide();
    $("#s3").addClass("active");
    $("#s3 .toggle-content").show();


    loadFreeBoats(marina,date,time,".modal-content","boatswap-new_boat","");
	});
  $(document).delegate("#boatswap-new_boat", "change", function() {
  	boatId=$(this).val();
  	subBoatSelectionBoatIds='.$subBoatSelectionBoatIds.';
  	if($.inArray(boatId,subBoatSelectionBoatIds) !== -1){
  		marina_id=$("#boatswap-new_port_id").val();
  		date=$("#boatswap-tmp_date").val();
  		boatName=$("#boatswap-new_boat option:selected" ).text();
  		fld="boatswap";
  		openBoatSelection(marina_id,date,boatName,fld)
  	}else{
  		$("#boatswap-selected_boat").val("0");
  	}
  });
  $("body").on("beforeSubmit", "form#boat-swap-form", function () {
		_targetContainer=$("#boat-swap-form")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
			  return false;
		 }
		 // submit form
		 $.ajax({
			  url: form.attr("action"),
			  type: "post",
			  data: form.serialize(),
			  success: function (response) {
				  if(response=="success"){
					  swal({title: "'.Yii::t('app','Swapped').'", html: "'.Yii::t('app','Boat swapped successfully').'", type: "success"});
					  window.closeModal();
				  }else{
        		loadSwapFreeBoats();
					  swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
				  }

          if($("#db-admin-container").length){
            $.pjax.reload({container: "#db-admin-container", timeout: 2000});
          }
				  App.unblockUI($(_targetContainer));
			  }
		 });
		 return false;
	});
');
?>
<script>
function accordionInit(){
	$("[data-plugin-toggle]").each(function() {
		var $this = $( this ),
			opts = {};

		var pluginOptions = $this.data("plugin-options");
		if (pluginOptions)
			opts = pluginOptions;

		$this.themePluginToggle(opts);
	});
}
</script>
