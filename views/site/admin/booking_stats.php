<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppDateRangePickerAsset;
AppDateRangePickerAsset::register($this);

$lastSevenDays=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")-7),date("Y"))).' - '.date("Y-m-d");
$booking_stats_range=$lastSevenDays;

if(isset($_GET['booking_stats_range']) && $_GET['booking_stats_range']!='')$booking_stats_range=$_GET['booking_stats_range'];

list($start_date,$end_date)=explode(" - ",$booking_stats_range);

$cities=Yii::$app->appHelperFunctions->activeCityList;

$this->registerJs('
initDtrpicker();
$(document).on("pjax:success", function() {
  initDtrpicker();
});
');
?>
<div class="row">
  <div class="col-sm-6">
    <?= Yii::$app->formatter->asDate($start_date)?> <strong>-</strong> <?= Yii::$app->formatter->asDate($end_date)?>
  </div>
  <div class="col-sm-6">
    <?= Html::beginForm(['site/index','tab'=>'booking-stats'], 'get', ['data-pjax' => '', 'id'=>'bookingStatsForm', 'class' => 'form-inline']); ?>
    <div class="input-group mb-3" style="width:100%;">
    	<span class="input-group-prepend">
    		<span class="input-group-text">Date Range</span>
    	</span>
    	<?= Html::input('text', 'booking_stats_range', $booking_stats_range, ['class' => 'form-control dtrpicker','autocomplete'=>'off', 'onchange'=>'document.getElementById(\'btnsrch\').click();']) ?>
    </div>
    <div class="hidden">
      <button id="btnsrch" type="submit">Go</button>
    </div>
    <?= Html::endForm() ?>
  </div>
</div>
<?php if($cities!=null){?>
<div class="row">
  <?php foreach($cities as $city){?>
  <div class="col-sm-6">
    <section class="card card-featured-top card-featured-left card-featured-right card-featured-bottom card-featured-quaternary mb-4">
      <header class="card-header">
        <h2 class="card-title"><?= $city['name']?></h2>
      </header>
			<div class="card-body">
        <?php $marinas=Yii::$app->appHelperFunctions->getActiveCityMarinaList($city['id']);?>
        <div class="tabs">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link active" href="#bscpall<?= $city['id']?>" data-toggle="tab">All</a>
            </li>
            <?php foreach($marinas as $marina){?>
            <li class="nav-item">
              <a class="nav-link" href="#bscp<?= $marina['id']?>" data-toggle="tab"><?= $marina['name']?></a>
            </li>
            <?php }?>
          </ul>
          <div class="tab-content">
            <?php
            $bsSmoothBookingsAll=0;
            $bsNABookingsAll=0;
            $bsRequestBookingsAll=0;

            $bsTrainingAll=0;
            $bsEmergencyAll=0;
            $bsOutOfServiceAll=0;

            $tsSmoothBookingsAll=0;
            $tsStuckBookingsAll=0;
            $tsTechnicalBookingsAll=0;
            $tsAccidentBookingsAll=0;
            $tsIncidentBookingsAll=0;
            $tsOtherBookingsAll=0;

            $tsNoShowBookingsAll=0;
            $tsCanceledWeatherBookingsAll=0;
            $tsCanceledNABookingsAll=0;
            $tsSameDayCancelBookingsAll=0;
            $tsWeatherWarningCancelBookingsAll=0;
            $tsStayInBookingsAll=0;

            $behLateArrivalBookingsAll=0;

            $extraOvernightCampBookingsAll=0;
            $extraCaptainBookingsAll=0;
            foreach($marinas as $marina){
              //Booking Status
              $bsSmoothBookings=Yii::$app->bookingHelperFunctions->getBookingExperienceStat(null,$marina['id'],$start_date,$end_date,1);
              $bsSmoothBookingsAll+=$bsSmoothBookings;

              $bsNABookings=Yii::$app->bookingHelperFunctions->getBookingExperienceStat(null,$marina['id'],$start_date,$end_date,2);
              $bsNABookingsAll+=$bsNABookings;

              $bsRequestBookings=Yii::$app->bookingHelperFunctions->getBookingExperienceStat(null,$marina['id'],$start_date,$end_date,3);
              $bsRequestBookingsAll+=$bsRequestBookings;

              $bsTraining=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,9);//Training
              $bsTrainingAll+=$bsTraining;

              $bsEmergency=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,10);//Emergency
              $bsEmergencyAll+=$bsEmergency;

              $bsOutOfService=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,11);//Out of Service
              $bsOutOfServiceAll+=$bsOutOfService;

              //Trip Status
              $tsSmoothBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat(null,$marina['id'],$start_date,$end_date,1);
              $tsSmoothBookingsAll+=$tsSmoothBookings;

              $tsTechnicalBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat(null,$marina['id'],$start_date,$end_date,2);
              $tsTechnicalBookingsAll+=$tsTechnicalBookings;

              $tsStuckBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat(null,$marina['id'],$start_date,$end_date,3);
              $tsStuckBookingsAll+=$tsStuckBookings;

              $tsAccidentBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat(null,$marina['id'],$start_date,$end_date,4);
              $tsAccidentBookingsAll+=$tsAccidentBookings;

              $tsIncidentBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat(null,$marina['id'],$start_date,$end_date,6);
              $tsIncidentBookingsAll+=$tsIncidentBookings;

              $tsOtherBookings=Yii::$app->bookingHelperFunctions->getTripExperienceStat(null,$marina['id'],$start_date,$end_date,5);
              $tsOtherBookingsAll+=$tsOtherBookings;

              $tsCanceledWeatherBookings=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,4);
              $tsCanceledWeatherBookingsAll+=$tsCanceledWeatherBookings;

              $tsCanceledNABookings=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,5);
              $tsCanceledNABookingsAll+=$tsCanceledNABookings;

              $tsNoShowBookings=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,3);
              $tsNoShowBookingsAll+=$tsNoShowBookings;

              $tsSameDayCancelBookings=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,7);
              $tsSameDayCancelBookingsAll+=$tsSameDayCancelBookings;

              $tsWeatherWarningCancelBookings=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,8);
              $tsWeatherWarningCancelBookingsAll+=$tsWeatherWarningCancelBookings;

              $tsStayInBookings=Yii::$app->bookingHelperFunctions->getBookingStatusStat(null,$marina['id'],$start_date,$end_date,2);
              $tsStayInBookingsAll+=$tsStayInBookings;


              //Behaviour
              $behLateArrivalBookings=Yii::$app->bookingHelperFunctions->getBookingBehaviorStat(null,$marina['id'],$start_date,$end_date,1);
              $behLateArrivalBookingsAll+=$behLateArrivalBookings;

              //Extra
              $extraOvernightCampBookings=Yii::$app->bookingHelperFunctions->getBookingExtraStat(null,$marina['id'],$start_date,$end_date,1);
              $extraOvernightCampBookingsAll+=$extraOvernightCampBookings;

              $extraCaptainBookings=Yii::$app->bookingHelperFunctions->getBookingExtraStat(null,$marina['id'],$start_date,$end_date,2);
              $extraCaptainBookingsAll+=$extraCaptainBookings;
            ?>
            <div id="bscp<?= $marina['id']?>" class="tab-pane">
              <table class="table table-striped table-bordered">
                <tr>
                  <th>Booking Status</th>
                  <th>Trip Status</th>
                  <th>Behavior</th>
                  <th>Extra</th>
                </tr>
                <tr>
                  <td><?= $bsSmoothBookings?> Smooth</td>
                  <td><?= $tsSmoothBookings?> Smooth</td>
                  <td><?= $behLateArrivalBookings?> Late arrival</td>
                  <td><?= $extraOvernightCampBookings?> Overnight Camp</td>
                </tr>
                <tr>
                  <td><?= $bsNABookings?> Original NA</td>
                  <td><?= $tsStuckBookings?> Stuck</td>
                  <td></td>
                  <td><?= $extraCaptainBookings?> Captain</td>
                </tr>
                <tr>
                  <td><?= $bsRequestBookings?> Request</td>
                  <td><?= $tsTechnicalBookings?> Technical</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><?= $bsTraining?> Training</td>
                  <td><?= $tsAccidentBookings?> Accident</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><?= $bsEmergency?> Emergency</td>
                  <td><?= $tsIncidentBookings?> Incident</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><?= $bsOutOfService?> Out of Service</td>
                  <td><?= $tsNoShowBookings?> No Show</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsCanceledWeatherBookings?> Canceled Due To Bad Weather</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsCanceledNABookings?> Canceled Original N/A</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsSameDayCancelBookings?> Same day Canceled</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsWeatherWarningCancelBookings?> Canceled due to weather warning</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsStayInBookings?> Stayin</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsOtherBookings?> Other</td>
                  <td></td>
                  <td></td>
                </tr>
              </table>
            </div>
            <?php
            }
            ?>
            <div id="bscpall<?= $city['id']?>" class="tab-pane active">
              <table class="table table-striped table-bordered">
                <tr>
                  <th>Booking Status</th>
                  <th>Trip Status</th>
                  <th>Behavior</th>
                  <th>Extra</th>
                </tr>
                <tr>
                  <td><?= $bsSmoothBookingsAll?> Smooth</td>
                  <td><?= $tsSmoothBookingsAll?> Smooth</td>
                  <td><?= $behLateArrivalBookingsAll?> Late arrival</td>
                  <td><?= $extraOvernightCampBookingsAll?> Overnight Camp</td>
                </tr>
                <tr>
                  <td><?= $bsNABookingsAll?> Original NA</td>
                  <td><?= $tsStuckBookingsAll?> Stuck</td>
                  <td></td>
                  <td><?= $extraCaptainBookingsAll?> Captain</td>
                </tr>
                <tr>
                  <td><?= $bsRequestBookingsAll?> Request</td>
                  <td><?= $tsTechnicalBookingsAll?> Technical</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><?= $bsTrainingAll?> Training</td>
                  <td><?= $tsAccidentBookingsAll?> Accident</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><?= $bsEmergencyAll?> Emergency</td>
                  <td><?= $tsIncidentBookingsAll?> Incident</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td><?= $bsOutOfServiceAll?> Out of Service</td>
                  <td><?= $tsNoShowBookingsAll?> No Show</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsCanceledWeatherBookingsAll?> Canceled Due To Bad Weather</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsCanceledNABookingsAll?> Canceled Original N/A</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsSameDayCancelBookingsAll?> Same day Canceled</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsWeatherWarningCancelBookingsAll?> Canceled due to weather warning</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsStayInBookingsAll?> Stayin</td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                  <td><?= $tsOtherBookingsAll?> Other</td>
                  <td></td>
                  <td></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
			</div>
		</section>
  </div>
  <?php }?>
</div>
<?php }?>

<script>
function initDtrpicker(){
	$(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
	$(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
		$(this).trigger("change");
	});
	$(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
		$(this).val('<?= $lastSevenDays?>');
	});
}
</script>
