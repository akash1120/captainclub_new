<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\models\RequestBreakDownSearch;
use app\assets\DashboardBreadDownAssets;
DashboardBreadDownAssets::register($this);

$searchModel=new RequestBreakDownSearch;
$searchModel->status=0;
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageParam = 'pbd-page';

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['label'=>Yii::t('app','Boat'),'attribute'=>'boat_id','value'=>function($model){return $model['boat_name'];},'filter'=>Yii::$app->appHelperFunctions->boatsListArr];
$columns[]=['label'=>Yii::t('app','Description'),'attribute'=>'description','value'=>function($model){return $model['description'];}];
$columns[]=['label'=>Yii::t('app','Level'),'attribute'=>'break_down_level','value'=>function($model){return $model['break_down_level']=='maj' ? Yii::t('app','Major') : Yii::t('app','Minor');},'filter'=>Yii::$app->operationHelperFunctions->breakDownLevel];
$columns[]=['format'=>'html','label'=>'Reason','attribute'=>'reason','value'=>function($model){return $model['reason']=='mec' ? Yii::t('app','Mechanical') : Yii::t('app','Member');},'filter'=>Yii::$app->operationHelperFunctions->breakDownReason];
$columns[]=['format'=>'html','label'=>'Member Remarks','attribute'=>'user_remarks','value'=>function($model){return $model['user_remarks'];}];
$columns[]=['format'=>'date','label'=>Yii::t('app','Date Posted'),'attribute'=>'created_at','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]=['format'=>'date','label'=>Yii::t('app','Requested Date'),'attribute'=>'req_date','filterInputOptions'=>['class'=>'form-control dtpicker','autocomplete'=>'off']];
$columns[]=['format'=>'html','label'=>Yii::t('app','Status'),'attribute'=>'status','value'=>function($model){return Yii::$app->operationHelperFunctions->requestStatusIcon[$model['status']];},'filter'=>false];
?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'cardHeader' => false,
  'columns' => $columns,
  'rowOptions' => function($model){
    $thirtyDays=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")-30),date("Y")));
    if($model['req_date']<$thirtyDays){
      return ['style'=>'background-color:red;color:#fff;'];
    }
  },
]); ?>
<style>
#db-admin-container .filters{display:table-row;}
</style>
