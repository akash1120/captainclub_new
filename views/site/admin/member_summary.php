<?php
use app\models\PackageGroup;
use app\models\PackageGroupToPackage;
use app\models\Package;
use app\models\User;
use app\models\Contract;
use app\models\ContractMember;

$subQueryRenewedUsers = Contract::find()
  ->select([ContractMember::tableName().".user_id"])
  ->innerJoin("contract_member","contract_member.contract_id=contract.id")
  ->where([
    'and',
    [ContractMember::tableName().'.status'=>1],
    [
      'or',
      ['>=','start_date',date("Y-m-d")],
      ['>=','end_date',date("Y-m-d")],
    ]

  ])
  ->groupBy(ContractMember::tableName().".user_id");

$subQueryRenewedNext = Contract::find()
  ->select([ContractMember::tableName().".user_id"])
  ->innerJoin("contract_member","contract_member.contract_id=contract.id")
  ->where([
    'and',
    [ContractMember::tableName().'.status'=>1],
    ['>=','start_date',date("Y-m-d")],
  ])
  ->groupBy(ContractMember::tableName().".user_id");

$packageGroups=PackageGroup::find()->select(['id','title'])->where(['status'=>1])->orderBy(['sort_order'=>SORT_ASC])->asArray()->all();
if($packageGroups!=null){
  foreach($packageGroups as $packageGroup){
    $subQueryGroupPackages = PackageGroupToPackage::find()
      ->select(["package_id"])
      ->where(['package_group_id'=>$packageGroup['id']]);
$packages=Package::find()->where(['id'=>$subQueryGroupPackages])->asArray()->all();
if($packages!=null){
  $nextSixMonths=[];
  for($n=0;$n<=7;$n++){
    $dueTime=mktime(0,0,0,(date("m")+$n),1,date("Y"));
    list($y,$m)=explode("-",date("Y-m",$dueTime));
    $nextSixMonths[]=['title'=>date("M, Y",$dueTime),'year'=>$y,'month'=>$m];
  }
?>
<section class="card card-featured card-featured-dark mb-2">
  <header class="card-header">
    <h2 class="card-title"><?= $packageGroup['title']?></h2>
  </header>
  <div class="card-body">
    <table class="table table-striped table-bordered">
      <tr>
        <th>Package</th>
        <th>Active</th>
        <th>Canceled</th>
        <th>Hold</th>
        <th>Expired</th>
        <th>Dead Expired</th>
        <?php if($nextSixMonths!=null){foreach($nextSixMonths as $key=>$val){?>
        <th><?= $val['title']?></th>
        <?php }}?>
        <th>Total</th>
      </tr>
      <?php
      $totalActive=0;
      $totalCancelled=0;
      $totalHold=0;
      $totalExpired=0;
      $totalDeadExpired=0;
      $montWiseTotal=[];

      foreach($packages as $package){
        $activeContracts=User::find()
          ->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
          ->where([
            'and',
            [Contract::tableName().'.package_id'=>$package['id'],User::tableName().'.status'=>1],
            ['>=',Contract::tableName().'.end_date',date("Y-m-d")]
          ])
          ->orderBy([Contract::tableName().'.end_date'=>SORT_DESC])
          ->count(User::tableName().'.id');
        $totalActive+=$activeContracts;

        $expiredAlready=User::find()
          ->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
          ->where([
            'and',
            [Contract::tableName().'.package_id'=>$package['id'],User::tableName().'.status'=>1],
            ['<',Contract::tableName().'.end_date',date("Y-m-d")],
            ['not in',User::tableName().'.id',$subQueryRenewedUsers]
          ])
          ->orderBy([Contract::tableName().'.end_date'=>SORT_DESC])
          ->count(User::tableName().'.id');
        $totalExpired+=$expiredAlready;

        $deadExpiredAlready=User::find()
          ->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
          ->where([
            'and',
            [Contract::tableName().'.package_id'=>$package['id'],User::tableName().'.status'=>5],
            ['<',Contract::tableName().'.end_date',date("Y-m-d")],
            ['not in',User::tableName().'.id',$subQueryRenewedUsers]
          ])
          ->count(User::tableName().'.id');
        $totalDeadExpired+=$deadExpiredAlready;

        $onHold=User::find()
          ->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
          ->where([
            'and',
            [Contract::tableName().'.package_id'=>$package['id'],User::tableName().'.status'=>2],
            ['not in',User::tableName().'.id',$subQueryRenewedUsers]
          ])
          ->count(User::tableName().'.id');
        $totalHold+=$onHold;

        $canceled=User::find()
          ->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
          ->where([
            'and',
            [Contract::tableName().'.package_id'=>$package['id'],User::tableName().'.status'=>3],
            ['not in',User::tableName().'.id',$subQueryRenewedUsers]
          ])
          ->count(User::tableName().'.id');
        $totalCancelled+=$canceled;
      ?>
      <tr>
        <th><?= $package['name']?></th>
        <td><?= $activeContracts?></td>
        <td><?= $canceled?></td>
        <td><?= $onHold?></td>
        <td><?= $expiredAlready?></td>
        <td><?= $deadExpiredAlready?></td>
        <?php
        $packageTotal[$package['id']]=0;
        if($nextSixMonths!=null){
          foreach($nextSixMonths as $key=>$val){
            $expiringContracts=User::find()
          ->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
              ->where([
                'and',
                [
                  Contract::tableName().'.package_id'=>$package['id'],
                  'MONTH('.Contract::tableName().'.end_date)'=>$val['month'],
                  'YEAR('.Contract::tableName().'.end_date)'=>$val['year'],
                  User::tableName().'.status'=>1,
                ],
                ['>=',Contract::tableName().'.end_date',date("Y-m-d",mktime(0,0,0,date("m"),1,date("Y")))],
                ['not in',User::tableName().'.id',$subQueryRenewedNext]
              ])
              ->groupBy(User::tableName().'.id')
              ->count('id');

            $packageTotal[$package['id']]+=$expiringContracts;
            $oldTotal=isset($montWiseTotal[$packageGroup['id']][$val['month'].$val['year']]) ? $montWiseTotal[$packageGroup['id']][$val['month'].$val['year']] : 0;
            $montWiseTotal[$packageGroup['id']][$val['month'].$val['year']]=$oldTotal+$expiringContracts;
            //echo '<pre>';print_r($montWiseTotal);echo '</pre>';
        ?>
        <td><?= $expiringContracts?></td>
        <?php
          }
        }
        ?>
        <th><?= $packageTotal[$package['id']]?></th>
      </tr>
      <?php
      }
      ?>
      <tfoot>
        <tr>
          <th>Total</th>
          <th><?= $totalActive?></th>
          <th><?= $totalCancelled?></th>
          <th><?= $totalHold?></th>
          <th><?= $totalExpired?></th>
          <th><?= $totalDeadExpired?></th>
          <?php if($nextSixMonths!=null){foreach($nextSixMonths as $key=>$val){?>
          <th><?= $montWiseTotal[$packageGroup['id']][$val['month'].$val['year']]?></th>
          <?php }}?>
          <th></th>
        </tr>
      </tfoot>
    </table>
  </div>
</section>
<?php }}}else{
echo '<div class="alert alert-info">No Group Found!, Please add group and assign packages.</div>';
}?>
