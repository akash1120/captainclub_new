<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\models\BookingSearch;
use app\models\BookingExpectedArrivalTime;

$searchModel = new BookingSearch();
$searchModel->booking_date=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")+1),date("Y")));
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$dataProvider->pagination->pageParam = 'feed-page';


$columns[] = ['class' => 'yii\grid\SerialColumn'];
$columns[] = ['format'=>'html','attribute'=>'member_name','value'=>function($model){
	$text=$model->member->fullname;
	$text.='<br /><span class="badge badge-info">'.Yii::$app->formatter->asDateTime($model->created_at).'</span>';
	$arrivalTime=BookingExpectedArrivalTime::find()->select(['arrival_time'])->where(['booking_id'=>$model->id])->asArray()->one();
	if($arrivalTime!=null){
		$text.='<br /><span class="badge badge-success">Expected Arrival: '.$arrivalTime['arrival_time'].'</span>';
	}else{
		$text.='<br /><span class="badge badge-warning">Not yet confirmed</span>';
	}
	return $text;
}];
$columns[] = ['format'=>'html','label'=>'City','attribute'=>'city_id','value' => function ($model){return ($model->city!=null ? $model->city->name : 'Not set');}];
$columns[] = ['format'=>'html','label'=>'Marina','attribute'=>'port_id','value' => function ($model){return ($model->marina!=null ? $model->marina->name : 'Not set');}];
$columns[] = ['format'=>'html','label'=>'Boat','attribute'=>'boat_id','value' => function ($model){return ($model->boat!=null ? $model->boat->name : 'Not set');}];
$columns[] = ['format'=>['date','php:D, M d, Y'],'label'=>'Date','attribute'=>'booking_date','value' => function ($model){return $model->booking_date;}];
$columns[] = ['format'=>'html','label'=>'Time','attribute'=>'booking_time_slot','value' => function ($model){return $model->timeZone->name.($model->booking_time_slot==3 ? ' <span style="color:red;">Marina Stay in</span>' : '');}];
$columns[] = ['format'=>'html','label'=>'Status','attribute'=>'status','value'=>function($model){return '<span class="badge badge-'.($model->status==1 ? 'success' : 'dark').'">'.Yii::$app->helperFunctions->bookingStatus[$model->status].'</span>';}];
$columns[] = ['format'=>'raw','attribute'=>'booking_type','label'=>'Remarks','value'=>function($model){return $model->remarks;}];
$columns[] = ['format'=>'html','attribute'=>'is_licensed', 'label'=>'License', 'value'=>function($model){return ($model->member!=null && $model->member->is_licensed==1 ? '<span class="label label-success">Yes</span>' : '<span class="label label-warning">No</span>');}];
$columns[] = ['format'=>'html','label'=>'Addons','value' => function ($model){return $model->addonsMenu;}];

?>
<style>
.btn-group.dropright{display: none !important;}
.requested-addons-list a.btn-warning{display: none;}
</style>
<div class="row">
  <div class="col-sm-4">
    <?php $form = ActiveForm::begin(['id'=>'filterform','method'=>'get']); ?>
    <?= $form->field($searchModel, 'port_id')->dropDownList(Yii::$app->appHelperFunctions->marinaListArr,['onchange'=>"document.getElementById('filterform').submit()",'prompt'=>$searchModel->getAttributeLabel('port_id')])->label(false) ?>
    <?php ActiveForm::end(); ?>
  </div>
</div>
<?php CustomPjax::begin(['id'=>'grid-container']); ?>
<?= CustomGridView::widget([
  'dataProvider' => $dataProvider,
  'filterModel' => $searchModel,
  'columns' => $columns,
  'cardHeader'=>false,
]); ?>
<?php CustomPjax::end(); ?>
<?= $this->render('/booking/js/list_script')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
