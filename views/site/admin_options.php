<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\DashboardPlan;
use app\components\widgets\CustomPjax;

$modelPlan = new DashboardPlan;

$tomorrowDate=$modelPlan->forDate;
if(isset($_GET['DashboardPlan']['forDate'])){
	$tomorrowDate=$_GET['DashboardPlan']['forDate'];
}
$planAllowed=false;
?>
<style>
#db-admin-container .filters{display: none;}
</style>
<?php CustomPjax::begin(['id'=>'db-admin-container']); ?>
<div class="tabs">
	<ul class="nav nav-tabs nav-jus1tified">
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('dashboard','site')){$planAllowed=true;?>
		<li class="nav-item<?= $tab=='dashboard' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'dashboard'])?>">Plan for <?= Yii::$app->formatter->asDate($tomorrowDate)?></a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('calendar-json','user-task')){?>
		<li class="nav-item<?= $tab=='calendar' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'calendar'])?>" data-pjax="0">Calendar</a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('booking-stats','site')){?>
		<li class="nav-item<?= $tab=='booking-stats' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'booking-stats'])?>">Booking Stats</a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('member-summary','site')){?>
		<li class="nav-item<?= $tab=='member-summary' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'member-summary'])?>">Member Summary</a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('future-warning','site')){?>
		<li class="nav-item<?= $tab=='future-warning' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'future-warning'])?>">Warnings</a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('future-hold','site')){?>
		<li class="nav-item<?= $tab=='future-hold' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'future-hold'])?>">Holds</a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('pending-breakdowns','site')){?>
		<li class="nav-item<?= $tab=='pending-breakdowns' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'pending-breakdowns'])?>">Breakdowns</a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('tomorrows-feed','site')){?>
		<li class="nav-item<?= $tab=='tomorrows-feed' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'tomorrows-feed'])?>">Tomorrow's Feed</a>
		</li>
    <?php }?>
    <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('index','operations')){?>
		<li class="nav-item<?= ($planAllowed==false && $tab=='dashboard') || $tab=='operations' ? ' active' : ''?>">
			<a class="nav-link" href="<?= Url::to(['site/index','tab'=>'operations'])?>">Operations</a>
		</li>
    <?php }?>
	</ul>
	<div class="tab-content">
    <div class="tab-pane active">
      <?php if($tab=='dashboard' && Yii::$app->menuHelperFunction->checkActionAllowed('dashboard','site')){?>
      <?= $this->render('admin/plan_by_date',['city'=>$city,'date'=>$date]);?>
      <?php }?>
      <?php if($tab=='calendar' && Yii::$app->menuHelperFunction->checkActionAllowed('calendar-json','user-task')){?>
  		<?= $this->render('admin/calendar');?>
      <?php }?>
      <?php if($tab=='booking-stats' && Yii::$app->menuHelperFunction->checkActionAllowed('booking-stats','site')){?>
  		<?= $this->render('admin/booking_stats');?>
      <?php }?>
      <?php if($tab=='member-summary' && Yii::$app->menuHelperFunction->checkActionAllowed('member-summary','site')){?>
  		<?= $this->render('admin/member_summary');?>
      <?php }?>
      <?php if($tab=='future-warning' && Yii::$app->menuHelperFunction->checkActionAllowed('future-warning','site')){?>
  		<?= $this->render('admin/booking_alerts',['alert_type'=>1]);?>
      <?php }?>
      <?php if($tab=='future-hold' && Yii::$app->menuHelperFunction->checkActionAllowed('future-hold','site')){?>
  		<?= $this->render('admin/booking_alerts',['alert_type'=>2]);?>
      <?php }?>
      <?php if($tab=='pending-breakdowns' && Yii::$app->menuHelperFunction->checkActionAllowed('pending-breakdowns','site')){?>
  		<?= $this->render('admin/pending_breakdowns');?>
      <?php }?>
      <?php if($tab=='tomorrows-feed' && Yii::$app->menuHelperFunction->checkActionAllowed('tomorrows-feed','site')){?>
  		<?= $this->render('admin/tomorrows_feed');?>
      <?php }?>
      <?php if(($planAllowed==false && $tab=='dashboard' && Yii::$app->menuHelperFunction->checkActionAllowed('index','operations')) || ($tab=='operations' && Yii::$app->menuHelperFunction->checkActionAllowed('index','operations'))){?>
  		<?= $this->render('/operations/index',['model'=>$modelOpr]);?>
      <?php }?>
		</div>
	</div>
</div>
<?php CustomPjax::end(); ?>
<?= $this->render('admin/js/scripts')?>
<?= $this->render('/booking/js/update_comments_scripts')?>
<?= $this->render('/boat-required/js/scripts')?>
<?= $this->render('/bulk-booking/js/bulk_booking_scripts')?>
<?= $this->render('/request/js/boat_assign_scripts')?>
<?= $this->render('/request/js/admin_request_scripts')?>
<?= $this->render('admin/js/swap_boat_scripts')?>
