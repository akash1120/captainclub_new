<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'username',['template'=>'
{label}
<div class="input-group">
{input}
<span class="input-group-append">
  <span class="input-group-text">
    <i class="fas fa-lock"></i>
  </span>
</span>
</div>
{error}
','options'=>['class'=>'form-group mb-3'],'errorOptions' => ['encode' => false]])->textInput(['class'=>'form-control form-control-lg','autofocus' => true]) ?>

<?= $form->field($model, 'password',['template'=>'
<div class="clearfix">
  {label}
</div>
<div class="input-group">
{input}
<span class="input-group-append">
  <span class="input-group-text">
    <i class="fas fa-lock"></i>
  </span>
</span>
</div>
{error}
','options'=>['class'=>'form-group mb-3']])->passwordInput(['class'=>'form-control form-control-lg']) ?>
<div class="row">
  <div class="col-sm-8">
    <div class="checkbox-custom checkbox-default">
      <input id="RememberMe" name="rememberme" type="checkbox"/>
      <label for="RememberMe">Remember Me</label>
    </div>
  </div>
  <div class="col-sm-4 text-right">
    <button type="submit" class="btn btn-primary mt-2">Sign In</button>
  </div>
</div>
<div  class="text-center">
<a href="<?= Url::to(['site/forget-password'])?>">Forgot Password?</a>
</div>
<?php ActiveForm::end(); ?>
