<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BookingAlert */

$this->title = Yii::t('app', 'New  {modelClass}', [
    'modelClass' => $model->alert_type==1 ? 'Warning Alert' : 'Hold Alert',
]);
$this->params['breadcrumbs'][] = ['label' => $model->alert_type==1 ? Yii::t('app', 'Warning Alerts') : Yii::t('app', 'Hold Alerts'), 'url' => $model->alert_type==1 ? ['warning'] : ['hold']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-alert-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
