<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BookingAlert */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => $model->alert_type==1 ? 'Warning Alert' : 'Hold Alert',
]);
$this->params['breadcrumbs'][] = ['label' => $model->alert_type==1 ? Yii::t('app', 'Warning Alerts') : Yii::t('app', 'Hold Alerts'), 'url' => $model->alert_type==1 ? ['warning'] : ['hold']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="booking-alert-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
