<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
use app\models\BookingAlertTiming;
use app\assets\BookingAlertFormAssets;
BookingAlertFormAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\BookingAlert */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$(".dtpicker").datepicker({
	format: "yyyy-mm-dd",
	todayHighlight: true,
	startDate: "today",
}).on("changeDate", function(e) {
	$(this).datepicker("hide");
});
');
if($model->id!=null){
$model->time_slot_id=ArrayHelper::map(BookingAlertTiming::find()->select(['time_slot_id'])->where(['alert_id'=>$model->id])->asArray()->all(),"time_slot_id","time_slot_id");
}
?>
<style>
#bookingalert-time_slot_id label{width: 30%;}
</style>
<section class="city-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'alert_date')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off']) ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'port_id')->dropDownList(Yii::$app->appHelperFunctions->marinaListArr,['prompt'=>$model->getAttributeLabel('port_id')]) ?>
			</div>
		</div>
		<?= $form->field($model, 'time_slot_id')->checkboxList(Yii::$app->appHelperFunctions->timeSlotListArr) ?>
		<?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
		<?= $form->field($model, 'email_message')->textarea(['rows' => 6]) ?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
		<?= Html::a('Cancel', $model->alert_type==1 ? ['warning'] : ['hold'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
