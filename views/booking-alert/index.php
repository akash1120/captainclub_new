<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomTabbedGridView;
use app\components\widgets\CustomPjax;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingAlertSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $searchModel->alert_type==1 ? Yii::t('app', 'Warning Alerts') : Yii::t('app', 'Hold Alerts');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
	initScripts();
');
$actionBtns='';
$createBtn=false;
$createBtnLink=[];
if(Yii::$app->controller->action->id=='warning' && Yii::$app->menuHelperFunction->checkActionAllowed('create-warning')){
  $createBtn=true;
  $createBtnLink=['create-warning'];
}
if(Yii::$app->controller->action->id=='hold' && Yii::$app->menuHelperFunction->checkActionAllowed('create-hold')){
  $createBtn=true;
  $createBtnLink=['create-hold'];
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
  $actionBtns.='{delete}';
}

$columns[]=['class' => 'yii\grid\SerialColumn'];
$columns[]=['format'=>'date','attribute'=>'alert_date','headerOptions'=>['style'=>'width:110px']];
$columns[]=['attribute'=>'port_id','value' => function ($model){return $model->marina->name;},'headerOptions'=>['style'=>'width:100px'],'filter'=>Yii::$app->appHelperFunctions->marinaListArr];
$columns[]='message:text';
$columns[]=['format'=>'raw','attribute'=>'timingHtml','headerOptions'=>['style'=>'width:210px'],'filter'=>Yii::$app->appHelperFunctions->timeSlotListArr];
//$columns[]=['format'=>'raw','attribute'=>'is_sent','value'=>function($model){return Yii::$app->helperFunctions->arrYesNoIcon[$model['is_sent']];},'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],'filter'=>Yii::$app->helperFunctions->arrYesNo];
if($searchModel->listType=='active'){
$columns[]=[
	'class' => 'yii\grid\ActionColumn',
	'header'=>'',
	'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
	'contentOptions'=>['class'=>'noprint actions'],
	'template' => '
	<div class="btn-group flex-wrap">
	<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
	<span class="caret"></span>
	</button>
	<div class="dropdown-menu" role="menu">
	'.$actionBtns.'
	</div>
	</div>',
	'buttons' => [
		'update' => function ($url, $model) {
			if($model['alert_date']>=date("Y-m-d")){
				return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
					'title' => Yii::t('app', 'Edit'),
					'class'=>'dropdown-item text-1',
					'data-pjax'=>"0",
				]);
			}
		},
		'delete' => function ($url, $model) {
			if($model['alert_date']>=date("Y-m-d")){
				return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
					'title' => Yii::t('app', 'Delete'),
					'class'=>'dropdown-item text-1',
					'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
					'data-method'=>"post",
					'data-pjax'=>"0",
				]);
			}
		},
	],
];
}
?>
<div class="boat-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
	<?= CustomTabbedGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => $createBtn,
    'createBtnLink'=>$createBtnLink,
    'showPerPage'=>true,
    'columns' => $columns,
  ]); ?>
  <?php CustomPjax::end(); ?>
</div>
<script>
function initScripts()
{
	$('input[name="BookingAlertSearch[alert_date]"]').datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		startDate: "today",
	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});

	$(document).on("pjax:success", function() {
		$('input[name="BookingAlertSearch[alert_date]"]').datepicker({
			format: "yyyy-mm-dd",
		}).on("changeDate", function(e){
			$(this).datepicker("hide");
		});
	});
}
</script>
