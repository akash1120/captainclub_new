<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
use app\models\EmailTemplate;
use app\assets\SettingsAssets;
SettingsAssets::register($this);

$emailTemplates = EmailTemplate::find()->all();

/* @var $this yii\web\View */

$this->title = 'Settings';

$this->registerJs('
$("#settingform-feedback_staff").select2({
	placeholder: "Search Staff",
	allowClear: true,
	 width: "100%",
});
tinymce.init({
	selector:".txtEditor",
	menubar:false,
	paste_as_text: true,
	plugins: [
		"advlist autolink lists link image charmap print preview anchor textcolor",
		"searchreplace visualblocks code fullscreen",
		"insertdatetime media table paste"
	],
	toolbar: "insertfile undo redo | paste | bold italic | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | code",
});
');
$model->feedback_staff=explode(",",$model->feedback_staff);
?>
<div class="system-setting">
	<section class="city-form card card-featured card-featured-warning">
		<header class="card-header">
			<h2 class="card-title"><?= $this->title?></h2>
		</header>
		<?php $form = ActiveForm::begin(); ?>
		<div class="card-body">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_useremailchanged_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_newuser_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_renewuser_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'auto_freeze_response')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_freezeuser_a')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_newbooking_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_newbooking_a')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_cancelbooking_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_cancelbooking_a')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_tomorrowreminder_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_alert_a')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_alert_days')->textInput(['placeholde'=>$model->getAttributeLabel('e_alert_days')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'freeze_finish_alert_days')->textInput(['placeholde'=>$model->getAttributeLabel('freeze_finish_alert_days')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_freezeopenuser_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_freezeopenuser_a')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'adminEmail')->textInput(['placeholde'=>$model->getAttributeLabel('adminEmail')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'requestEmail')->textInput(['placeholde'=>$model->getAttributeLabel('requestEmail')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'metEmail')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'notmetEmail')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'requestCancelledEmail')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_expiry_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_expiry_a')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'opLateArrival')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'opNoShow')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'opStuck')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'opTechnical')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_newrequiredbooking_a')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_boatswaped_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_requestboatalloc_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'e_bulkbookboatreplaced_u')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'opPetrolConsumtion')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'ndtrainingmet')->dropDownList(ArrayHelper::map($emailTemplates,'id','title'),['prompt'=>'Select']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'paytab_servicefees_u',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">%</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput(['placeholde'=>$model->getAttributeLabel('paytab_servicefees_u')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'paytab_servicefees_pt',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">%</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput(['placeholde'=>$model->getAttributeLabel('paytab_servicefees_pt')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'paytab_addiotional_pt')->textInput(['placeholde'=>$model->getAttributeLabel('paytab_addiotional_pt')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'feedback_staff')->dropDownList(Yii::$app->appHelperFunctions->staffMembersArr,['multiple'=>'multiple']) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'expiry_alert_days',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">Days</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput(['placeholde'=>$model->getAttributeLabel('expiry_alert_days')]) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'passenger_info_id')->dropDownList(Yii::$app->appHelperFunctions->boatInfoListArr) ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'behaviour_report_counter')->textInput(['placeholde'=>$model->getAttributeLabel('behaviour_report_counter')]) ?>
				</div>
				<div class="col-xs-12 col-sm-12">
					<?= $form->field($model, 'bank_details')->textArea(['rows'=>5,'class'=>'form-control txtEditor','placeholde'=>$model->getAttributeLabel('bank_details')]) ?>
				</div>
			</div>
			<h3>Short Notice</h3>
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_captain',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_donut',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_bbq',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_ice',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_kids_life_jacket',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_early_departure',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_late_arrival_wo_nd',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_late_arrival_w_nd',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">same day</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_overnight_camp',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">hours before trip</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_wakeboarding',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_wakesurfing',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
				<div class="col-xs-12 col-sm-4">
					<?= $form->field($model, 'sn_bvlgari_access',['template'=>'
					{label}
					<div class="input-group">
					{input}
					<span class="input-group-append">
						<span class="input-group-text">1 day before</span>
					</span>
					</div>
					{hint}{error}
					'])->textInput() ?>
				</div>
			</div>
		</div>
	  <div class="card-footer">
			<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Cancel', ['site/index'], ['class' => 'btn btn-default']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</section>
</div>
