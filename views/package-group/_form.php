<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\PackageGroupToPackage;

/* @var $this yii\web\View */
/* @var $model app\models\PackageGroup */
/* @var $form yii\widgets\ActiveForm */
if($model->id!=null){
  $model->package_id=ArrayHelper::map(PackageGroupToPackage::find()->select(['package_id'])->where(['package_group_id'=>$model->id])->asArray()->all(),"package_id","package_id");
}
?>
<style>
.field-packagegroup-package_id label{ width: 30%;}
</style>
<section class="package-group-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
      </div>
    </div>
    <?= $form->field($model, 'package_id')->checkboxList(Yii::$app->appHelperFunctions->packageListArr) ?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
