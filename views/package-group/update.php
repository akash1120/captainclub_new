<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PackageGroup */

$this->title = Yii::t('app', 'Update Package Group: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Package Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="package-group-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
