<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PackageGroup */

$this->title = Yii::t('app', 'New Package Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Package Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
