<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RequestService */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('
	$(".dtpicker").datepicker({
		format: "yyyy-mm-dd",
	}).on("changeDate", function(e){
		$(this).datepicker("hide");
	});
');
?>
<div class="request-service-form">
    <?php $form = ActiveForm::begin(['id'=>'serviceActionForm']); ?>
    <?= $form->field($model, 'admin_action_date')->textInput(['maxlength' => true, 'class' => 'form-control dtpicker']) ?>
    <?= $form->field($model, 'admin_remarks')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>
