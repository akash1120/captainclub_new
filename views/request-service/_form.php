<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RequestService */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="request-service-form">
    <?php $form = ActiveForm::begin(['id'=>'reqServiceForm']); ?>
    <?= $form->field($model, 'boat_id')->dropDownList(Yii::$app->appHelperFunctions->getBoatsListArr(),['prompt'=>Yii::t('app','Select'),'tabindex'=>'1']) ?>
    <div class="row">
    	<div class="col-sm-6">
			<?= $form->field($model, 'current_hours')->textInput(['maxlength' => true]) ?>
        </div>
    	<div class="col-sm-6">
            <?= $form->field($model, 'scheduled_hours')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->id!=null ? Yii::t('app', 'Update') : Yii::t('app', 'Send'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
    </div>
    <?php ActiveForm::end(); ?>
</div>
