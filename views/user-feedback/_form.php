<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$rtnList=[
	'1'=>'1 star',
	'2'=>'2 star',
	'3'=>'3 star',
	'4'=>'4 star',
	'5'=>'5 star'
];
$model->rating=5;
?>
<?php $form = ActiveForm::begin(['id'=>'feedbackForm']); ?>
<?= $form->field($model, 'rating')->radioList($rtnList,[
	'item' => function($index, $label, $name, $checked, $value) {
		$return = '<label><input type="radio" name="'.$name.'" value="'.$value.'"'.($checked ? ' checked="checked"' : '').'> '.$label.'&nbsp;&nbsp;</label>';
		$return .= '<i></i>';
		return $return;
	}
,'class'=>'star-rating']); ?>
<?= $form->field($model, 'descp')->textArea(['rows'=>5])->label('Feedback comments from user') ?>
<div class="row">
	<div class="col-md-12">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btnSubmit','class' => 'btn btn-success']) ?>
		<button type="button" class="btn btn-default" data-dismiss="modal" onclick="resetModelWin()">Close</button>
	</div>
</div>
<?php ActiveForm::end(); ?>
