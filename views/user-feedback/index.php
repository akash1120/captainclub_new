<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\widgets\UsersTabStart;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Member:').' '.$model->fullname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Members'), 'url' => ['user/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.filters{display:none;}
</style>
<div class="user-index">
  <?= UsersTabStart::header($model,$searchModel)?>
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'cardHeader' => false,
      'options'=>['class'=>'grid-view'],
      'columns' => [
        ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
        ['format'=>'datetime','attribute'=>'created_at','headerOptions'=>['style'=>'width:200px;']],
        ['format'=>'html','attribute'=>'descp','label'=>Yii::t('app','Comments'),'value'=>function($model){
          return ($model['rating']>0 ? '<strong>Rating: '.$model['rating'].' star'.($model['rating']>1 ? 's' : '').'</strong><br />' : '').$model['descp'];
        }],
      ],
    ]);?>
    <?php CustomPjax::end(); ?>
<?= UsersTabStart::footer()?>
</div>
