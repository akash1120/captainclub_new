<?php
use yii\helpers\Url;
//Script for Edit Booking Comments
$this->registerJs('
	$(document).delegate(".btn-feedback", "click", function() {
		id=$(this).data("userid");
		username=$(this).data("username");
		$.ajax({
			url: "'.Url::to(['user-feedback/create','id'=>'']).'"+id,
			dataType: "html",
			success: function(data) {
				$("#general-modal").find("h5.modal-title").html("Feedback from "+username);
				$("#general-modal").find(".modalContent").html(data);
				$("#general-modal").modal();
			},
			error: bbAlert
		});
	});
	$("body").on("beforeSubmit", "form#feedbackForm", function () {
		_targetContainer=$("#feedbackForm")
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});

		 var form = $(this);
		 // return false if form still have some validation errors
		 if (form.find(".has-error").length) {
				return false;
		 }
		 // submit form
		 $.ajax({
				url: form.attr("action"),
				type: "post",
				data: form.serialize(),
				success: function (response) {
					if(response=="success"){
						swal({title: "'.Yii::t('app','Saved').'", html: "'.Yii::t('app','Feedback saved successfully').'", type: "success"});
						window.closeModal();
					}else{
						swal({title: "'.Yii::t('app','Error').'", html: response, type: "error"});
					}
					App.unblockUI($(_targetContainer));
				}
		 });
		 return false;
	});
');
?>
