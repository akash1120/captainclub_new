<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\AppDatePickerAsset;
AppDatePickerAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\SupportTicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tickets');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
	$actionBtns.=' {view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
	$actionBtns.=' {delete}';
}

$ticketStatus=Yii::$app->helperFunctions->ticketStatus;

$this->registerJs('
	initDtpicker();
	$(document).on("pjax:success", function() {
		initDtpicker();
	});
');
?>
  <div class="ticket-index">
    <?php CustomPjax::begin(['id' => 'grid-container']) ?>
    <?= CustomGridView::widget([
      'import' => true,
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
	    'createBtn' => $createBtn,
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'ref_no',
        ['attribute'=>'username','value'=>function($model){
					return $model->member!=null ? $model->member->username : '';
				}],
        ['attribute'=>'created_by','value'=>function($model){
					return $model->createdByUser->username;
				},'filter'=>Yii::$app->appHelperFunctions->staffMembersArr],
        'title',
        ['format'=>'html','attribute'=>'priority_id','value'=>function($model){
					return $model->priority->codeColor;
				},'filter'=>Yii::$app->appHelperFunctions->prioritiesListArr],
        ['attribute'=>'due_date','value'=>function($model){
					if($model->due_date!=''){
						return Yii::$app->formatter->asDate($model->due_date);
					}else{
						return "";
					}
				}],
        ['attribute'=>'status','value'=>function($model) use ($ticketStatus){
					return $ticketStatus[$model->status];
				},'filter'=>$ticketStatus],
        [
          'class' => 'yii\grid\ActionColumn',
	        'header'=>'',
	        'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
	        'contentOptions'=>['class'=>'noprint actions'],
	        'template' => '
	          <div class="btn-group flex-wrap">
	  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
	              <span class="caret"></span>
	            </button>
	  					<div class="dropdown-menu" role="menu">
	              '.$actionBtns.'
	  					</div>
	  				</div>',
          'buttons' => [
						'view' => function ($url, $model) {
							return Html::a('<i class="fas fa-edit"></i> Update', ['view','id'=>$model->rssid], [
								'title' => Yii::t('app', 'Update'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
							]);
						},
            'delete' => function ($url, $model) {
              return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                'title' => Yii::t('app', 'Delete'),
                'class'=>'dropdown-item text-1',
                'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                'data-method'=>"post",
                'data-pjax'=>"0",
              ]);
            },
          ],
        ],
      ],
    ]); ?>
    <?php CustomPjax::end() ?>
  </div>
<script>
function initDtpicker(){
	$("input[name=\"SupportTicketSearch[due_date]\"]").attr("autocomplete","off");
	$("input[name=\"SupportTicketSearch[due_date]\"]").datepicker({
		format: "yyyy-mm-dd",
		todayHighlight: true,
		startDate: "today",

	}).on("changeDate", function(e) {
		$(this).datepicker("hide");
	});
}
</script>
