<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\SupportTicketAssets;
SupportTicketAssets::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\SupportTicket */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$(".dtpicker").datepicker({
  format: "yyyy-mm-dd",
  todayHighlight: true,
  startDate: "today",

}).on("changeDate", function(e) {
  $(this).datepicker("hide");
});
$("body").on("click", ".remove_file", function () {
	_this=$(this);
	id=_this.data("id");
	var _targetContainer=".panel-body";
	swal({
		title: "'.Yii::t('app','Confirmation').'",
		html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
		type: "warning",
		showCancelButton: true,
    confirmButtonColor: "#47a447",
		confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
		cancelButtonText: "'.Yii::t('app','Cancel').'",
	}).then((result) => {
    if (result.value) {
      App.blockUI({
  			message: "'.Yii::t('app','Please wait...').'",
  			target: _targetContainer,
  			overlayColor: "none",
  			cenrerY: true,
  			boxed: true
  		});
  		$.ajax({
  			url: "'.Url::to(['delete-file','tid'=>$model->rssid,'id'=>'']).'"+id,
  			dataType: "html",
  			type: "POST",
  			success: function(html) {
  				_this.parents("#attchment"+id).remove();
  				App.unblockUI($(_targetContainer));
  				swal({title: "'.Yii::t('app','Deleted!').'", html: "'.Yii::t('app','Image deleted.').'", type: "success", timer: 3000});
  			},
  			error: bbAlert
  		});
    }
  });
});
$("#supportticket-username").autocomplete({
  serviceUrl: "'.Url::to(['site/member-search']).'",
	noCache: true,
    onSelect: function(suggestion) {
      if($("#supportticket-user_id").val()!=suggestion.data){
        $("#supportticket-user_id").val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
        $("#supportticket-user_id").val("");
    }
});

initDropZone();
');
?>
<style>
.att-container{padding: 0; margin: 0;}
.att-container li{display: inline-block;width: 100px; height: 100px; margin-right: 20px;}
.att-container li img{width: 100px; height: 100px;}
.att-container .att-Item{position:relative;}
.att-container .remove_file{position: absolute; right: 0px; top: 0px;}
</style>

<section class="time-slot-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="hidden">
      <?= $form->field($model, 'user_id')->textInput() ?>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'username')->textInput() ?>
      </div>
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'title')->textInput() ?>
      </div>
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'priority_id')->dropDownList(Yii::$app->appHelperFunctions->prioritiesListArr) ?>
      </div>
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'due_date')->textInput(['class'=>'form-control dtpicker','autocomplete'=>'off']) ?>
      </div>
    </div>
    <?= $form->field($model, 'descp')->textarea(['rows'=>5]) ?>
    <strong>Attachments:</strong>
    <?php
  	$attachmentFiles=$model->attachmentInfo;
  	if($attachmentFiles!=null){
  	?>
    <ul class="att-container">
      <?php foreach($attachmentFiles as $attachmentFile){?>
        <li id="attchment<?= $attachmentFile['id']?>" class="att-Item">
          <a href="javascript:;" class="btn btn-xs btn-danger remove_file" data-id="<?= $attachmentFile['id']?>"><i class="fa fa-times"></i></a>
          <a href="<?= $attachmentFile['link']?>" target="_blank">
            <img src="<?= $attachmentFile['iconPath']?>" class="img-responsive" alt="" />
          </a>
        </li>
      <?php }?>
    </ul>
  	<?php
  	}
  	?>
    <div id="hidden-files" class="hidden"></div>
  	<div id="dZU1pload" class="dropzone_my">
  		<div class="hidden">
  			<div id="template" class="dz-preview dz-file-preview dz-processing dz-success dz-complete filesbox" style="width:100%; line-height:none; margin:0px;">
          <div style="width:94%; margin:0px 3%;">
            <table width="100%" class="table table-striped table-bordered table-hover" style="margin-bottom:0px;">
              <tr class="upimg-item">
                <td width="10%">
                  <a href="javascript:;" target="_blank">
      							<img src="" class="prvimg img-responsive" width="160" height="100" alt="" />
      						</a>
                </td>
                <td style="max-width:20%;">
                  <div class="dz-filename"><span data-dz-name></span></div>
                  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                  <div class="dz-error-message"><span data-dz-errormessage></span></div>
                  <div class="dz-success-mark"></div>
                  <div class="dz-error-mark"></div>
                </td>
                <td width="18%" align="center"> <div class="dz-size"><span data-dz-size></span></div></td>
                <td width="12%" align="center">
                  <a class="btn btn-xs btn-orange dz-remove" href="javascript:undefined;" data-dz-remove="" style="font-size:12px;">
                    <?= Yii::t('app','Remove')?>
                  </a>
                </td>
              </tr>
            </table>
          </div>
  			</div>
  		</div>
  		<div class="dz-default dz-message">
  			<?= Yii::t('app','Drop attachments here or click to upload.')?><br/>
  			<small>(png,jpg,jpeg,gif,doc,docx,xls,xlsx,pdf)</small>
  		</div>
  	</div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
<script>

function initDropZone()
{
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;

		var mydropzone;
		Dropzone.autoDiscover = false;
		$("#dZU1pload").dropzone({
		  url: "<?= Url::to(['site/drop-upload'])?>",
		  previewTemplate: previewTemplate,
		  addRemoveLinks: false,
			acceptedFiles: '.png,.jpg,.jpeg,.gif,.pdf,.doc,.docx,.xls,.xlsx',
		  init: function() {
		    mydropzone=this;
		  },
		  success: function (file, response) {
		    var fileName = response;
		    file.previewElement.classList.add("dz-success");
		    $("#hidden-files").append("<input type=\"hidden\" name=\"SupportTicket[attachmentFile][]\" class=\"form-control\" value=\""+fileName+"\" />");

				updateImagesSrc();

		    var removeButton = Dropzone.createElement("<i class=\"glyphicon glyphicon-trash text-danger\"></i>");
		    removeButton.addEventListener("click", function(e) {
		      e.preventDefault();
		      e.stopPropagation();
		      _this.removeFile(file);
		      // If you want to the delete the file on the server as well, you can do the AJAX request here.
		    });
		  },
		  removedfile: function(file){
		    $("input[value=\""+file.name+"\"]").remove();
		    $(document).find(file.previewElement).remove();
		  },
		  error: function (file, response) {
		    file.previewElement.querySelector("[data-dz-errormessage]").textContent = response;
		    file.previewElement.classList.add("dz-error");
		  }
		});
}
var otherFileTypes=['hello','doc','docx','xls','xlsx','pdf'];
function updateImagesSrc()
{
	$(".upimg-item").each(function( index ) {
		_t=$(this);
		imgName=_t.find(".dz-filename > span").text();
    var extension = imgName.substr( (imgName.lastIndexOf('.') +1) );
    if(jQuery.inArray(extension,otherFileTypes)>=0){
      _t.find(".prvimg").attr("src","<?= Yii::$app->params['images_uploads_rel_path']?>icons/"+extension+".jpg");
    }else{
      _t.find(".prvimg").attr("src","<?= Yii::$app->params['temp_rel_path']?>"+imgName);
    }
	});
}
</script>
