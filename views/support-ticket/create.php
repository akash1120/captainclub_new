<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SupportTicket */

$this->title = Yii::t('app', 'New  {modelClass}', [
    'modelClass' => 'Ticket',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tickets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-create">
  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>
</div>
