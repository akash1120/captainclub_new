<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\components\widgets\CustomPjax;
use app\models\SupportTicket;

/* @var $this yii\web\View */
/* @var $model app\models\SupportTicket */

$this->title = Yii::t('app', 'View {modelClass}', [
  'modelClass' => 'Ticket',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tickets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
$(document).delegate(".change-status-ticket", "click", function() {
	_this=$(this);
	type=_this.data("type");
	var _targetContainer="#grid-container-info";
	App.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});
	$.ajax({
		url: "'.Url::to(['change-status','id'=>$model->rssid,'type'=>'']).'"+type,
		dataType: "html",
		type: "POST",
		success: function(html) {
      toastr.success("Ticket updated successfully");
      $.pjax.reload({container: "#grid-container-info", timeout: 2000});
		},
		error: bbAlert
	});
});
$(document).delegate(".btnAddReply", "click", function() {
    $("html, body").animate({
        scrollTop: $("#addReplyArea").offset().top
    }, 500);
});
$(document).delegate(".btn-del-reply", "click", function() {
	_this=$(this);
	id=_this.data("id");
	var _targetContainer=".panel-body";
	swal({
		title: "'.Yii::t('app','Confirmation').'",
		html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
		type: "warning",
		showCancelButton: true,
    confirmButtonColor: "#47a447",
		confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
		cancelButtonText: "'.Yii::t('app','Cancel').'",
	}).then(function () {
		App.blockUI({
			message: "'.Yii::t('app','Please wait...').'",
			target: _targetContainer,
			overlayColor: "none",
			cenrerY: true,
			boxed: true
		});
		$.ajax({
			url: "'.Url::to(['support-ticket-reply/delete','id'=>'']).'"+id,
			dataType: "html",
			type: "POST",
			success: function(html) {
				_this.parents(".reply-item").remove();
				App.unblockUI($(_targetContainer));
        toastr.success("Reply deleted successfully");
        $.pjax.reload({container: "#grid-container-replies", timeout: 2000});
			},
			error: bbAlert
		});
	});
});
');
?>
<style>
h4{margin: 0px; margin-bottom: 10px;}
.att-container{padding: 0; margin: 0;}
.att-container li{display: inline-block;width: 50px; height: 50px; margin-right: 20px;}
.att-container li img{width: 50px; height: 50px;}
.att-container .att-Item{position:relative;}
.att-container .remove_file{position: absolute; right: 0px; top: 0px;}
</style>
<section class="city-form card card-featured card-featured-warning">
  <?php CustomPjax::begin(['id' => 'grid-container-info']) ?>
	<header class="card-header">
    <div class="row">
      <div class="col-sm-12 col-md-9">
        <h2 class="card-title"><?= $model->title?></h2>
      </div>
      <div class="col-sm-12 col-md-3 text-right">
        <strong>Created: <i class="fa fa-clock-o"></i></strong> <?= Yii::$app->formatter->asDate($model->created_at)?>
      </div>
    </div>
    <?php if($model->member!=null){?>
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <strong>Member:</strong> <?= $model->member->username?>
      </div>
    </div>
    <?php }?>
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <strong>Priority:</strong> <?= $model->priority->codeColor?>
      </div>
      <?php if($model->due_date!=''){?>
        <div class="col-sm-12 col-md-6 text-right">
          <strong>Due Date:</strong> <?= Yii::$app->formatter->asDate($model->due_date)?>
        </div>
      <?php }?>
    </div>
	</header>
	<div class="card-body">
    <div class="row">
      <?php
      $decpCol=12;
    	$attachmentFiles=$model->attachmentInfo;
    	if($attachmentFiles!=null){
        $decpCol=9;
    	?>
      <div class="col-sm-12 col-md-3" style="border-right:1px solid #ccc">
        <strong>Attachments:</strong>
        <ul class="att-container">
          <?php foreach($attachmentFiles as $attachmentFile){?>
            <li class="att-Item">
              <a href="<?= $attachmentFile['link']?>" data-pjax="0" target="_blank">
                <img src="<?= $attachmentFile['iconPath']?>" class="img-responsive" alt="" />
              </a>
            </li>
          <?php }?>
        </ul>
      </div>
    	<?php
    	}
    	?>
      <div class="col-sm-12 col-md-<?= $decpCol?>">
        <strong>Detail:</strong><br />
        <?= $model->descp?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3"></div>
      <div class="col-sm-9 pull-right">
        <?php if($model->status==1){?>
        <a href="javascript:;" class="btn btn-sm btn-success change-status-ticket" data-type="2">Close Ticket</a>
        <?php }?>
        <?php if($model->status==2){?>
        <a href="javascript:;" class="btn btn-sm btn-warning change-status-ticket" data-type="1">Open Ticket</a>
        <?php }?>
      </div>
    </div>
  </div>
  <?php CustomPjax::end() ?>
</section>
<section class="card card-featured card-featured-warning">
  <header class="card-header">
    <div class="row">
      <div class="col-sm-12 col-md-6">
        <h2 class="card-title">Replies</h2>
      </div>
      <div class="col-sm-12 col-md-6 text-right">
        <a href="javascript:;" data-pjax="0" class="btn btn-sm btn-info btnAddReply">Add Reply</a>
      </div>
    </div>
  </header>
  <div class="card-body">
    <?php CustomPjax::begin(['id' => 'grid-container-replies']) ?>
    <?= ListView::widget( [
      'dataProvider' => $dataProvider,
      'id' => 'my-listview-id',
      'options'=>['class'=>'timeline timeline-simple changelog'],
      'itemOptions' => ['class' => 'panel reply-item'],
      'itemView' => '/support-ticket-reply/_item',
      'layout'=>"
          {items}
          <center>{pager}</center>
      ",
    ] );
    ?>
    <?php CustomPjax::end() ?>
  </div>
</section>
<section id="addReplyArea" class="card card-featured card-featured-warning">
  <header class="card-header">
    <h2 class="card-title">Add Reply</h2>
  </header>
  <div class="card-body">
<?php
$this->registerJs('
$("body").on("beforeSubmit", "form#ticketReplyForm", function () {
  var _targetContainer="#ticketReplyForm";
   var form = $(this);
   // return false if form still have some validation errors
   if (form.find(".has-error").length) {
      return false;
   }
   App.blockUI({
    message: "'.Yii::t('app','Please wait...').'",
    target: _targetContainer,
    overlayColor: "none",
    cenrerY: true,
    boxed: true
  });
   // submit form
   $.ajax({
      url: form.attr("action"),
      type: "post",
      data: form.serialize(),
      dataType: "json",
      success: function (response) {
        if(response["success"]){
          toastr.success(response["success"]["msg"]);
          $.pjax.reload({container: "#grid-container-replies", timeout: 2000});
          document.getElementById("ticketReplyForm").reset();
          $("#hidden-files").html("");
          var myDropzone = Dropzone.forElement("#dZU1pload");
          myDropzone.removeAllFiles(true);
        }else{
          swal({title: response["error"]["heading"], html: response["error"]["msg"], type: "error", timer: 5000});
        }
        App.unblockUI($(_targetContainer));
      }
   });
   return false;
});
');
$replyModel=new SupportTicket;
echo $this->render('/support-ticket-reply/_form',['model'=>$replyModel,'parentModel'=>$model])
?>
  </div>
</section>
