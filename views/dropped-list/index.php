<?php
use yii\helpers\Html;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DroppedEmailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Soft Bounced Emails');
$this->params['breadcrumbs'][] = $this->title;
$gridBtnsArr='';

if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
	$gridBtnsArr.=' {delete}';
}
?>
<div class="dropped-list-index">
	<?php CustomPjax::begin(['id' => 'grid-container']) ?>
	<?= CustomGridView::widget([
		'import' => true,
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],],
			'email:email',
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Actions',
				'headerOptions'=>['class'=>'noprint','style'=>'width:10px;'],
				'contentOptions'=>['class'=>'noprint'],
				'template' => $gridBtnsArr,
				'buttons' => [
					'delete' => function ($url, $model) {
						if($model['trashed_count']<=3){
							return Html::a('<i class="fa fa-trash"></i>', $url, [
								'title' => Yii::t('app', 'Delete'),
								'class'=>'btn btn-xs btn-danger',
								'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
								'data-method'=>"post",
								'data-pjax'=>"0",
							]);
						}
					},
				],
			],
		],
	]); ?>
	<?php CustomPjax::end() ?>
</div>
