<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WaterSportEquipment */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Water Sport Equipment',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Water Sport Equipments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="water-sport-equipment-update">
  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>
</div>
