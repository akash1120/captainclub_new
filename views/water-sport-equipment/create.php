<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WaterSportEquipment */

$this->title = Yii::t('app', 'Create Water Sport Equipment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Water Sport Equipments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="water-sport-equipment-create">
  <?= $this->render('_form', [
      'model' => $model,
  ]) ?>
</div>
