<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cities;
use app\models\Ports;

$defPorts=[];
$jScript="var cities = []\n";
$cities=Yii::$app->appHelperFunctions->cityList;
if($cities!=null){
	foreach($cities as $city){
		$jScript.="var city_".$city['id']." = [\n";
		$marinas=Yii::$app->appHelperFunctions->getCityMarinaList($city['id']);
		if($marinas!=null){
			foreach($marinas as $marina){
				$jScript.='{display: "'.$marina['name'].'", value: "'.$marina['id'].'", sel: "'.(($model['id']!=null && $model->port_id==$marina['id']) ? 'selected=\"selected\"' : '').'" },'."\n";
			}
		}
		$jScript.="];";
	}
}
if($model->city_id){
	$defPorts=Yii::$app->appHelperFunctions->getCityMarinaListArr($model->city_id);
}
$this->registerJs($jScript.'
$("#watersportequipment-city_id").change(function() {
	cval=$(this).val();
	$("#watersportequipment-port_id").html("");
	array_list=eval("city_"+cval)
	$(array_list).each(function (i) {
		$("#watersportequipment-port_id").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
	});
});
');

/* @var $this yii\web\View */
/* @var $model app\models\WaterSportEquipment */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="discount-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'city_id')->dropDownList(Yii::$app->appHelperFunctions->cityListArr,['class'=>'form-control selMenu','prompt'=>$model->getAttributeLabel('city_id')]) ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'port_id')->dropDownList($defPorts,['prompt'=>$model->getAttributeLabel('port_id')]) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-sm-6">
				<?= $form->field($model, 'qty')->textInput(['maxlength' => true]) ?>
			</div>
		</div>

		<?php if($model->id!=null){?>
			<img src="<?= Yii::$app->fileHelperFunctions->getImagePath('watersportequipment',$model['image'],'medium')?>" width="75" height="75" />
		<?php }?>
		<?= $form->field($model, 'image',[
		'template' => '
		{label}
		{input}
		<div class="input-group">
			<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-file"></i></span></div>
			<input type="text" class="form-control" disabled placeholder="'.Yii::t('app','Upload Image').'">
			<span class="input-group-append">
				<button class="browse btn btn-success input-md" type="button"><i class="glyphicon glyphicon-folder-open"></i> '.Yii::t('app','Browse').'</button>
			</span>
		</div>
		{error}'
		])->fileInput(['class'=>'file input-file', 'data-img'=>'mcsc-photo', 'accept' => 'image/*']);?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
