<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BoatPurpose */

$this->title = Yii::t('app', 'Update Boat Purpose: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Boat Purposes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="boat-purpose-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
