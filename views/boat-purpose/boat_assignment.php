<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Boat;


/* @var $this yii\web\View */
/* @var $model app\models\BoatPurpose */

$this->title = Yii::t('app', 'Assign Boat Purposes');
$this->params['breadcrumbs'][] = $this->title;

$marinas = Yii::$app->appHelperFunctions->activeMarinaList;

$boats=Boat::find()->where(['port_id'=>$model->marina_id,'status'=>1,'trashed'=>0])->asArray()->all();
?>
<div class="tabs">
	<ul class="nav nav-tabs">
		<?php
		if($marinas!=null){
			$n=1;
			foreach ($marinas as $marina) {
				?>
				<li class="nav-item">
					<a class="nav-link<?= $model->marina_id==$marina['id'] ? ' active' : ''?>" href="<?= Url::to(['boat-assignment','marina_id'=>$marina['id']])?>"><?= $marina['name']?></a>
				</li>
				<?php
				$n++;
			}
		}
		?>
	</ul>
	<div class="tab-content">
    <div class="tab-pane active">
      <?php $form = ActiveForm::begin(); ?>
      <?php
      if($boats!=null){
      ?>
      <table class="table table-striped table-bordered table-hover">
        <tr>
          <th>#</th>
          <th>Boat Name</th>
          <th>Purpose</th>
        </tr>
        <?php
          $n=1;
          foreach($boats as $boat){
            $model->boat_name[$boat['id']]=$boat['name'];
            $model->boat_purpose_id[$boat['id']]=$boat['boat_purpose_id'];
        ?>
        <tr>
          <td width="10"><?= $n?></td>
          <td><?= $form->field($model, 'boat_name['.$boat['id'].']')->textInput()->label(false) ?></td>
          <td><?= $form->field($model, 'boat_purpose_id['.$boat['id'].']')->dropDownList(Yii::$app->appHelperFunctions->boatPurposeListArr,['prompt'=>$model->getAttributeLabel('boat_purpose_id')])->label(false) ?></td>
        </tr>
        <?php $n++;}?>
      </table>
      <?php }?>
      <div>
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    	</div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
