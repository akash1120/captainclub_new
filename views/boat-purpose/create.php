<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BoatPurpose */

$this->title = Yii::t('app', 'New Boat Purpose');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Boat Purposes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boat-purpose-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
