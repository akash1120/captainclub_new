<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppPriority */

$this->title = Yii::t('app', 'Update Priority: {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Priorities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="priority-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
