<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppPriority */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
.field-appcolor-code label, .field-appcolor-text_color label{width: 100%;}
</style>
<section class="city-form card card-featured card-featured-warning">
  <?php $form = ActiveForm::begin(); ?>
	<header class="card-header">
		<h2 class="card-title"><?= $this->title?></h2>
	</header>
	<div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
          <?= $form->field($model, 'color_id')->dropDownList(Yii::$app->appHelperFunctions->colorListArr) ?>
      </div>
    </div>
    <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrPublishing) ?>
	</div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
	</div>
  <?php ActiveForm::end(); ?>
</section>
