<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppSelect2Asset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/select2/select2.css',
  ];
  public $js = [
    'plugins/select2/select2.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
