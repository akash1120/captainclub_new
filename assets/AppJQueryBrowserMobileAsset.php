<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppJQueryBrowserMobileAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/jquery-browser-mobile/jquery.browser.mobile.js',
  ];
  public $depends = [
  ];
}
