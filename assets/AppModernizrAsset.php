<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppModernizrAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/modernizr/modernizr.js',
  ];
  public $depends = [
    
  ];
}
