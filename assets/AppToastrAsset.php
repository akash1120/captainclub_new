<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppToastrAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/toastr/toastr.min.css',
  ];
  public $js = [
    'plugins/toastr/toastr.min.js',
  ];
  public $depends = [
  ];
}
