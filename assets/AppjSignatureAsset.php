<?php
namespace app\assets;

use Yii;
use yii\web\AssetBundle;

class AppjSignatureAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/jsignature/flashcanvas.js',
    'plugins/jsignature/jsignature.min.js',
  ];
  public $depends = [
  ];
}
