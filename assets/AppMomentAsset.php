<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppMomentAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/moment/moment.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
