<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppEarlyDepartureAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'yii\web\YiiAsset',
    'app\assets\AppGuestAsset',
    'app\assets\AppFancyBoxAsset',
  ];
}
