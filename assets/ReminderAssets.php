<?php
namespace app\assets;

use yii\web\AssetBundle;

class ReminderAssets extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppJQueryDateTimePickerAsset',
    'app\assets\AppSelect2Asset',
  ];
}
