<?php
namespace app\assets;

use yii\web\AssetBundle;

class DataTablesButtonsAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/DataTables/Buttons-1.5.1/css/buttons.dataTables.min.css',
  ];
  public $js = [
    'plugins/DataTables/Buttons-1.5.1/js/dataTables.buttons.min.js',
  ];
  public $depends = [
    'app\assets\DataTablesAsset',
  ];
}
