<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppSelectPickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/bootstrap-select-1.13.7/dist/css/bootstrap-select.css',
  ];
  public $js = [
    'plugins/bootstrap-select-1.13.7/dist/js/bootstrap-select.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
