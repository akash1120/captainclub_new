<?php
namespace app\assets;

use yii\web\AssetBundle;

class SupportTicketAssets extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppDatePickerAsset',
    'app\assets\AppDropZoneAsset',
    'app\assets\AppAutoCompleteAsset',
  ];
}
