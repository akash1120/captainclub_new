<?php
namespace app\assets;

use yii\web\AssetBundle;

class BookingReportAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppDateRangePickerAsset',
  ];
}
