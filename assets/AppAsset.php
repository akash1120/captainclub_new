<?php
/**
* @link http://www.yiiframework.com/
* @copyright Copyright (c) 2008 Yii Software LLC
* @license http://www.yiiframework.com/license/
*/

namespace app\assets;

use yii\web\AssetBundle;

/**
* Main application asset bundle.
*
* @author Qiang Xue <qiang.xue@gmail.com>
* @since 2.0
*/
class AppAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'https://fonts.googleapis.com/css?family=Open+Sans%7CShadows+Into+Light',

    'plugins/animate/animate.css',
    'plugins/font-awesome/css/all.min.css',
    'css/theme.css',
    'css/custom.css',
  ];
  public $js = [
    'plugins/jquery.blockui.min.js',
    'plugins/nanoscroller/nanoscroller.js',
    'js/theme.js',
    'js/custom.js',
    'js/app-init.js?v=1.0.0.0.3',
  ];
  public $depends = [
    'app\assets\AppModernizrAsset',
    'yii\web\YiiAsset',
    'app\assets\AppJQueryBrowserMobileAsset',
    'app\assets\AppPopperAsset',
    'yii\bootstrap\BootstrapAsset',
    'yii\bootstrap\BootstrapPluginAsset',
    //'app\assets\AppBootstrapAsset',
    'app\assets\AppToastrAsset',
    'app\assets\AppSweetAlertAsset',
  ];
}
