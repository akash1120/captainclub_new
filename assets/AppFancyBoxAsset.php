<?php
namespace app\assets;

use Yii;
use yii\web\AssetBundle;

class AppFancyBoxAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/fancybox/dist/jquery.fancybox.min.css',
  ];
  public $js = [
    'plugins/fancybox/dist/jquery.fancybox.min.js',
  ];
  public $depends = [
  ];
}
