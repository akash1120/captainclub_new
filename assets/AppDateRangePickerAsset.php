<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppDateRangePickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/daterangepicker/daterangepicker.css',
  ];
  public $js = [
    'plugins/daterangepicker/moment.min.js',
    'plugins/daterangepicker/daterangepicker.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
