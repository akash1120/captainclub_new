<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppJQueryDateTimePickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/jquery-datetimepicker/build/jquery.datetimepicker.min.css',
  ];
  public $js = [
    'plugins/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
