<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppBootstrapAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/bootstrap/css/bootstrap.css',
  ];
  public $js = [
    'plugins/bootstrap/js/bootstrap.js',
  ];
  public $depends = [
  ];
}
