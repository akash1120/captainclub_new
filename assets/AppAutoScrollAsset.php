<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAutoScrollAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/autoscroll/jquery-ias.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
