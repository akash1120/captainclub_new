<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppColorPickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/spectrum-colorpicker/spectrum.css',
  ];
  public $js = [
    'plugins/spectrum-colorpicker/spectrum.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
