<?php
namespace app\assets;

use Yii;
use yii\web\AssetBundle;

class AppSweetAlertAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/sweetalert2/sweetalert2.all.js',
  ];
  public $depends = [
  ];
}
