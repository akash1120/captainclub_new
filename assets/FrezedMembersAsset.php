<?php
namespace app\assets;

use yii\web\AssetBundle;

class FrezedMembersAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppDateRangePickerAsset',
    'app\assets\AppDatePickerAsset',
  ];
}
