<?php
namespace app\assets;

use Yii;
use yii\web\AssetBundle;

class AppFullCalendarAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/fullcalendar-4.1.0/packages/core/main.css',
    'plugins/fullcalendar-4.1.0/packages/bootstrap/main.css',
    'plugins/fullcalendar-4.1.0/packages/timegrid/main.css',
    'plugins/fullcalendar-4.1.0/packages/daygrid/main.css',
    'plugins/fullcalendar-4.1.0/packages/list/main.css',
  ];
  public $js = [
    'plugins/fullcalendar-4.1.0/packages/core/main.js',
    'plugins/fullcalendar-4.1.0/packages/interaction/main.js',
    'plugins/fullcalendar-4.1.0/packages/bootstrap/main.js',
    'plugins/fullcalendar-4.1.0/packages/daygrid/main.js',
    'plugins/fullcalendar-4.1.0/packages/timegrid/main.js',
    'plugins/fullcalendar-4.1.0/packages/list/main.js',
    'plugins/popper/umd/tooltip.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppMomentAsset',
  ];
}
