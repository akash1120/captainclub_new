<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppWysiHtmlAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/wysihtml5/bootstrap3-wysihtml5.min.css',
  ];
  public $js = [
    'plugins/wysihtml5/bootstrap3-wysihtml5.all.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
