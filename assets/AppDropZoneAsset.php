<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppDropZoneAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/dropzone/dropzone_my.css',
  ];
  public $js = [
    'plugins/dropzone/dropzone.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
