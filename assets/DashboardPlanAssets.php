<?php
namespace app\assets;

use yii\web\AssetBundle;

class DashboardPlanAssets extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppDatePickerAsset',
    'app\assets\AppSelect2Asset',
    'app\assets\AppAutoCompleteAsset',
  ];
}
