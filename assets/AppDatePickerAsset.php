<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppDatePickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/datepicker/datepicker3.css',
  ];
  public $js = [
    'plugins/datepicker/bootstrap-datepicker.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
