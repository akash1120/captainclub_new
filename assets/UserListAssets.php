<?php
namespace app\assets;

use yii\web\AssetBundle;

class UserListAssets extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppDatePickerAsset',
    'app\assets\AppSelect2Asset',
    'app\assets\AppSelectPickerAsset',
    'app\assets\AppDropZoneAsset',
    'app\assets\AppTinyMceAsset',
  ];
}
