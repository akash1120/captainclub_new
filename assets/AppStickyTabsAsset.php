<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppStickyTabsAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/stickyfill/dist/stickyfill.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
