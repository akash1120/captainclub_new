<?php
namespace app\assets;

use yii\web\AssetBundle;

class UserFormAssets extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AppDatePickerAsset',
  ];
}
