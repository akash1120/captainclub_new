<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAutoCompleteAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/jquery-autocomplete/src/jquery.autocomplete.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
