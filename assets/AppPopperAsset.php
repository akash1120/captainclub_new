<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppPopperAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/popper/umd/popper.min.js',
  ];
  public $depends = [
  ];
}
