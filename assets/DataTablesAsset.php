<?php
namespace app\assets;

use yii\web\AssetBundle;

class DataTablesAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/DataTables/DataTables-1.10.16/css/jquery.dataTables.min.css',
  ];
  public $js = [
    'plugins/DataTables/DataTables-1.10.16/js/jquery.dataTables.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
