<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppGuestAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'https://fonts.googleapis.com/css?family=Open+Sans%7CShadows+Into+Light',
    'plugins/bootstrap/css/bootstrap.css',
    'plugins/animate/animate.css',
    'plugins/font-awesome/css/all.min.css',
    'css/theme.css',
    'css/custom.css',
  ];
  public $js = [
    'plugins/jquery.blockui.min.js',
  ];
  public $depends = [
    'yii\web\YiiAsset',
    //'yii\bootstrap\BootstrapAsset',
  ];
}
