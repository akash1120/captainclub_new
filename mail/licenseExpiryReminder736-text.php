<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Dear Captain <?= $model->member->fullname?>,

Hope everything is going well with you.

We would like to inform you that your DMCA License will expire on <?= Yii::$app->formatter->asDate($model->license_expiry)?>.

For your reference, providing to you below the requirements for the renewal of DMCA license application which can be done online (http://dmca.ae/en/) if you have an existing DMCA account.

Please prepare <?= Yii::$app->helperFunctions->dubaiLicenseFee?> for the payment of the license valid for five (5) years.

1. Expired DMCA license
2. Passport copy
3. Visa Copy
4. Emirates ID copy
5. Picture ( to be use in the license)
6. Medical Certificate - MED3 attached - (just fill up the form and no need to go for the Doctor)

Once you successfully done with the application, kindly send us the copy of your Renewed DMCA License for our records.

If you need further assistance concerning the application, please don't hesitate to contact me.

Thank you.

Best regards,
