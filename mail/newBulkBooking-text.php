<?php

/* @var $this yii\web\View */
/* @var $model app\models\BulkBooking */
?>
Dear team

Kindly note the operation team placed the following bulk booking

Marina: <?= $model->boat->marina->name?>
Boat: <?= $model->boat->name?>
Starting Date: <?= Yii::$app->formatter->asDate($model->start_date)?>
Finish Date: <?= Yii::$app->formatter->asDate($model->end_date)?>
Remarks: <?= ($model->booking_type!=null ? Yii::$app->helperFunctions->adminBookingTypes[$model->booking_type].' - ' : '').$model->booking_comments?>
