<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\FeedbackForm */
?>
<p>Hello Admin,</p>

<p>A new feedback has been posted, following is the information:</p>

<p>
<strong>Name:</strong> <?= $model->member->fullname?><br />
<strong>Email:</strong> <?= $model->member->email?><br />
<strong>Date:</strong> <?= Yii::$app->formatter->asDate(date("Y-m-d"))?><br />
<strong>Rating:</strong> <?= $model->rating.' star'?><br />
<strong>Comments:</strong> <?= nl2br($model->descp)?>
</p>
