<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<div class="new-request">
<p>Hello Admin,</p>

<p>A new request type Suggestion/Complaints has been posted, following is the information:</p>

<p>
<strong>Name:</strong> <?= $model->member->fullname?><br />
<strong>Email:</strong> <?= $model->member->email?><br />
<strong>Date:</strong> <?= Yii::$app->formatter->asDate(date("Y-m-d"))?><br />
<strong>Comments:</strong> <?= nl2br($model->descp)?>
</p>
</div>
