<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<div class="new-request">
<p>Hello Admin,</p>

<p>A new request type Other has been posted, following is the information:</p>

<p>
<strong>Posted Date:</strong> <?= Yii::$app->formatter->asDate(date("Y-m-d"))?><br />
<strong>Name:</strong> <?= $model->member->fullname?><br />
<strong>Email:</strong> <?= $model->member->email?><br />
<?php if($model->requested_date!=null){?>
<strong>Requested Date:</strong> <?= Yii::$app->formatter->asDate($model->requested_date)?><br />
<?php }?>
</p>
<?php if($model->booking!=null){?>
<p>
<strong>Booking Detail:</strong> <br />
<strong>City:</strong> <?= $model->booking->city->name?><br />
<strong>Marina:</strong> <?= $model->booking->marina->name?><br />
<strong>Boat:</strong> <?= $model->booking->boat->name?><br />
<strong>Date:</strong> <?= Yii::$app->formatter->asDate($model->booking->booking_date)?><br />
<strong>Time:</strong> <?= $model->booking->timeSlot->name?><br />
</p>
<?php }?>
<p>
  <strong>Comments:</strong> <?= nl2br($model->descp)?>
</p>
</div>
