<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<div class="new-request">
<p>Dear Captain <?= $model->member->fullname?>,</p>

<p>Your deposit has been received and credited to your account.</p>

<p>
<strong>Amount:</strong> AED <?= $model->amount?><br />
<strong>Date:</strong> <?= Yii::$app->formatter->asDate(date("Y-m-d"))?><br />
</p>
<p>Enjoy Boating With Relief</p>
</div>
