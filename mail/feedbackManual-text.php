<?php

/* @var $this yii\web\View */
/* @var $user app\models\FeedbackForm */
?>
Hello Admin,

A new feedback has been posted, following is the information:

Name: <?= $model->member->fullname?>
Email: <?= $model->member->email?>
Date: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>
Rating: <?= $model->rating.' star'?>
Comments: <?= $model->descp?>
