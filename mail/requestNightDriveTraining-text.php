<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Hello Admin,

A new night drive training request has been posted, following is the information:

Posted Date: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>
Name: <?= $model->member->fullname?>
Email: <?= $model->member->email?>
Comments: <?= $model->descp?>
