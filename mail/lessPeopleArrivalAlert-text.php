<?php

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
Dear Admin,

Below is the booking detail where number of people arriving are less then the number of people departed.

Member: <?= $booking->member->fullname?>

Date & Time: <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?>

Boat: <?= $booking->boat->name?>

Marina: <?= $booking->marina->name?>


No. of People on Departure: <?= $model->dep_ppl?>

No. of People on Arrival: <?= $model->arr_ppl?>
