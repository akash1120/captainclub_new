<?php

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
<?= $template->template_text?>


Member: <?= $booking->member->fullname?>

City: <?= $booking->city->name?>

Marina: <?= $booking->marina->name?>

Boat: <?= $booking->boat->name?>

Date & Time: <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?>
