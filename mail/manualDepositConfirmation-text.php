<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Dear Captain <?= $model->member->fullname?>,

Your deposit has been received and credited to your account.

Amount:	AED <?= $model->amount?>
Date: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>

Enjoy Boating With Relief
