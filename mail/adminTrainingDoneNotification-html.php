<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<div class="new-request">
<p>Hello Admin,</p>

<p>You have a pending request to met,</p>

<p>Following are the details:</p>

<p>
<strong>Request:</strong> Night Drive Training<br />
<strong>Member:</strong> <?= $model->request->member->fullname?><br />
<strong>Member Comments:</strong> <?= nl2br($model->request->descp)?><br /><br />
<strong>Training Date:</strong> <?= Yii::$app->formatter->asDate($model->training_date)?><br />
<strong>Training Marina:</strong> <?= $model->marina->name?><br />
<strong>Training Remarks:</strong> <?= nl2br($model->remarks)?>
</p>
</div>
