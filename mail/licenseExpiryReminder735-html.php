<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<p>Dear Captain <?= $model->member->fullname?>,</p>

<p>I hope everything is going well with you.</p>

<p>We would like to inform you that your FTA local license will expire on <strong><?= Yii::$app->formatter->asDate($model->license_expiry)?></strong> and providing you the guidelines (attached file) on how to renew your license online.</p>

<p>Once you successfully done with the application, kindly provide us the copy of your Renewed FTA Local License to update our records in our system.</p>

<p><strong>Note:</strong> Expired FTA license is not allowed to get a boat in any Marina as per the Coast guard regulations.</p>

<p>If you need further assistance concerning the application, please don't hesitate to contact me.</p>

<p>Thank you.</p>

<p>Best regards,</p>
