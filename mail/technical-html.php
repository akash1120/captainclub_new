<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
<div class="late-arrival">
	<?= $template->template_html?><br />
    <p>
        <table cellpadding="10" cellspacing="0" border="1">
        	<tr>
            	<th>Member</th>
            	<th>City</th>
            	<th>Marina</th>
            	<th>Boat</th>
            	<th>Date & Time</th>
            </tr>
            <tr>
            	<td><?= $booking->memberName?></td>
            	<td><?= $booking->city->name?></td>
            	<td><?= $booking->port->name?></td>
            	<td><?= $booking->boat->name?></td>
            	<td><?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?></td>
            </tr>
        </table>
    </p>
</div>
