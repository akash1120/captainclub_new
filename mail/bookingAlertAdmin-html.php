<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\BookingAlert */
?>
<div class="new-request">
<p>Hello Admin,</p>

<p>A new <?= ($model->alert_type==1 ? 'Warning' : 'Hold')?> alert has been posted, following is the information:</p>

<p>
<strong>Posted Date:</strong> <?= Yii::$app->formatter->asDate(date("Y-m-d"))?><br />
<strong>Marina:</strong> <?= $model->marina->name?><br />
<strong>Alert Date:</strong> <?= Yii::$app->formatter->asDate($model->alert_date)?><br />
<strong>Timings:</strong> <?= $model->timingHtml?><br />
<strong>Message:</strong> <?= nl2br($model->message)?>
</p>
</div>
