<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<div class="new-request">
<p>Hello Admin,</p>

<p><?= $model->member->fullname?> wants to remind you of his/her night drive training request, following is the information:</p>

<p>
<strong>Posted Date:</strong> <?= date("l, jS M Y")?><br />
<strong>Name:</strong> <?= $model->member->fullname?><br />
<strong>Email:</strong> <?= $model->member->email?><br />
<strong>Comments:</strong> <?= nl2br($model->comments)?>
</p>
</div>
