<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BulkBooking */
?>
<div class="bulk-booking-request">
  <p>Dear team</p>
  <p>Kindly note the operation team placed the following bulk booking</p>

  <p>
    <strong>Marina:</strong> <?= $model->boat->marina->name?><br />
    <strong>Boat:</strong> <?= $model->boat->name?><br />
    <strong>Starting Date:</strong> <?= Yii::$app->formatter->asDate($model->start_date)?><br />
    <strong>Finish Date:</strong> <?= Yii::$app->formatter->asDate($model->end_date)?><br />
    <strong>Remarks:</strong> <?= ($model->booking_type!=null ? Yii::$app->helperFunctions->adminBookingTypes[$model->booking_type].' - ' : '').$model->booking_comments?>
  </p>
</div>
