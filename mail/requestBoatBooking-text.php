<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Hello Admin,

A new request type Boat Booking has been posted, following is the information:

Name: <?= $model->member->fullname?>
Email: <?= $model->member->email?>
Date: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>

Request Details:
City: <?= $model->city->name;?>
Marina: <?= $model->marina;?>
Requested Date: <?= Yii::$app->formatter->asDate($model->requested_date);?>
Time: <?= $model->time_slot;?>
Comments:</strong> <?= nl2br($model->descp);?>
