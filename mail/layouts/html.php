<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body>
  <?php $this->beginBody() ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="top" bgcolor="#eeeeee" style="color:#000;font-family: 'Open Sans', sans-serif; font-size:12px;"><table width="610" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="48" align="center" valign="middle" style="color:#000;font-family: 'Open Sans', sans-serif; font-size:12px;">To ensure delivery, add <?= \Yii::$app->params['supportEmail']?> to your address book.</td>
        </tr>
        <tr>
          <td align="center" valign="top" style="background:#FFF; border:1px solid #c7c7c7;"><table width="560" align="center" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td align="left" valign="top" style="color:#000;font-family: 'Open Sans', sans-serif; font-size:12px;line-height:20px;"><?= $content ?></td>
            </tr>
            <?php if(Yii::$app->params['includeDefSignature']==true){?>
            <tr>
              <td align="center" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td align="left" valign="top"><img src="<?= Yii::$app->params['siteUrl']?>images/email_logo.png" alt="<?= Yii::$app->params['siteName']?>" width="136" height="105" /></td>
            </tr>
            <tr>
              <td align="center" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td align="left" valign="top" style="color:#000;font-family: 'Open Sans', sans-serif; font-size:12px;"><strong>Customer Service Team</strong></td>
            </tr>
            <tr>
              <td align="center" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td align="left" valign="top" style="color:#000;font-family: 'Open Sans', sans-serif; font-size:12px;line-height:20px;">
                <strong>M:</strong> +97152 641 4172<br />
                <strong>E:</strong> icare@thecaptainsclub.ae<br />
                <strong>T:</strong> 800 82278
              </td>
            </tr>
            <?php }?>
            <tr>
              <td>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
