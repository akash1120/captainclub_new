<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
<?= $content ?>

<?php if(Yii::$app->params['includeDefSignature']==true){?>
Customer Service Team

M: +97152 641 4172
E: icare@thecaptainsclub.ae
T: 800 82278
<?php }?>
<?php $this->endBody() ?>
<?php $this->endPage() ?>
