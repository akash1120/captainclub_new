<?php

/* @var $this yii\web\View */
/* @var $user app\models\BookingAlert */

$timings=$model->timingHtml;
$timings=str_replace("<ul>","\n\n",$timings);
$timings=str_replace("</ul>","\n\n",$timings);
$timings=str_replace("<li>","",$timings);
$timings=str_replace("</li>","\n",$timings);
?>
Hello Admin,

A new <?= ($model->alert_type==1 ? 'Warning' : 'Hold')?> alert has been posted, following is the information:

Posted Date: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>
Marina: <?= $model->marina->name?>
Alert Date: <?= Yii::$app->formatter->asDate($model->alert_date)?>
Timings: <?= $timings?>
Message: <?= $model->message?>
