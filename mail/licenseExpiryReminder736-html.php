<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<p>Dear Captain <?= $model->member->fullname?>,</p>

<p>Hope everything is going well with you.</p>

<p>We would like to inform you that your DMCA License will expire on <strong><?= Yii::$app->formatter->asDate($model->license_expiry)?></strong>.</p>

<p>For your reference, providing to you below the requirements for the renewal of DMCA license application which can be done online (http://dmca.ae/en/) if you have an existing DMCA account.</p>

<p>Please prepare <?= Yii::$app->helperFunctions->dubaiLicenseFee?> for the payment of the license valid for five (5) years.</p>

<p>
1. Expired DMCA license<br />
2. Passport copy<br />
3. Visa Copy<br />
4. Emirates ID copy<br />
5. Picture ( to be use in the license)<br />
6. Medical Certificate - MED3 attached - (just fill up the form and no need to go for the Doctor)<br />
</p>

<p>Once you successfully done with the application, kindly send us the copy of your Renewed DMCA License for our records.</p>

<p>If you need further assistance concerning the application, please don't hesitate to contact me.</p>

<p>Thank you.</p>

<p>Best regards,</p>
