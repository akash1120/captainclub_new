<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Hello Admin,

You have a pending request to met,

Following are the details:

Request: Night Drive Training
Member: <?= $model->request->member->fullname?>
Member Comments: <?= $model->request->descp?>


Training Date: <?= Yii::$app->formatter->asDate($model->training_date)?>
Training Marina: <?= $model->marina->name?>
Training Remarks: <?= $model->remarks?>
