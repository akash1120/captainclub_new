<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Hello Admin,

<?= $model->member->fullname?> wants to remind you of his/her night drive training request, following is the information:

Posted Date: <?= date("l, jS M Y")?>
Name: <?= $model->member->fullname?>
Email: <?= $model->member->email?>
Comments: <?= $model->comments?>
