<?php

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
Member: <?= $booking->member->fullname?>

Date & Time: <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?>

Boat: <?= $booking->boat->name?>

Marina: <?= $booking->marina->name?>

Type: <?= $flagType?>

Overall Remarks: <?= $model->overall_remarks?>
