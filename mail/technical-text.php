<?php

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
<?= $template->template_text?>


Member: <?= $booking->memberName?>

City: <?= $booking->city->name?>

Marina: <?= $booking->port->name?>

Boat: <?= $booking->boat->name?>

Date & Time: <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?>
