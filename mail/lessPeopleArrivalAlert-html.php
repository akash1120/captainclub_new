<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
Dear Admin,<br /><br />

Below is the booking detail where number of people arriving are less then the number of people departed.<br /><br />

<strong>Member:</strong> <?= $booking->member->fullname?><br /><br />
<strong>Date & Time:</strong> <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?><br /><br />
<strong>Boat:</strong> <?= $booking->boat->name?><br /><br />
<strong>Marina:</strong> <?= $booking->marina->name?><br /><br /><br /><br />

<strong>No. of People on Departure:</strong> <?= $model->dep_ppl?><br /><br />
<strong>No. of People on Arrival:</strong> <?= $model->arr_ppl?><br /><br />
