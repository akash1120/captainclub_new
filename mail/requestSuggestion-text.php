<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Hello Admin,

A new request type Suggestion/Complaints has been posted, following is the information:

Name: <?= $model->member->fullname?>
Email: <?= $model->member->email?>
Date: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>
Comments: <?= $model->descp?>
