<?php

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
$memberDUICount=$model->memberDUICount;
?>
Dear Admin,

DUI is reported by the team, Below are the details.

Member: <?= $booking->member->fullname?>

Date & Time: <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?>

Boat: <?= $booking->boat->name?>

Marina: <?= $booking->marina->name?>


DUI Comments: <?= $model->dui_comments?>

<?php if($memberDUICount>0){?>Previously Reported DUI: <?= $model->memberDUICount?><?php }?>
