<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
<strong>Member:</strong> <?= $booking->member->fullname?><br /><br />
<strong>Date & Time:</strong> <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?><br /><br />
<strong>Boat:</strong> <?= $booking->boat->name?><br /><br />
<strong>Marina:</strong> <?= $booking->marina->name?><br /><br />
<strong>Type:</strong> <?= $flagType?><br /><br />
<strong>Overall Remarks:</strong> <?= $model->overall_remarks?><br /><br />
