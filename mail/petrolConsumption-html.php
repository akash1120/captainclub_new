<?php
use yii\helpers\Html;
use app\models\FuelConsumptionType;
use app\models\BoatFuelConsumptionRate;
use app\models\BookingInOutChangedBoat;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $template app\models\EmailTemplate */
$creditBalance = 0;
if($model->booking!=null && $model->booking->member!=null){
	$creditBalance = $model->booking->member->profileInfo->credit_balance;
}
$strPetrolInfo = '
	<table style="color:#000;font-family: \'Open Sans\', sans-serif; font-size:12px;line-height:20px;">
		<tr>
			<td>Petrol Consumption: </td><td>'.$model->fuel_cost.' AED'.($fuelBill!='' && $fuelBill!=null ? ', <span style="color:blue;">Bill attached</span>' : '').'</td>
		</tr>
		<tr>
			<td>Petrol Credit: </td><td style="color:'.($creditBalance<0 ? 'red' : 'green').';">'.$creditBalance.' AED</td>
		</tr>
	</table>';

$bookingInOutInfo = '
	<table cellpadding="10" cellspacing="0" border="1" style="color:#000;font-family: \'Open Sans\', sans-serif; font-size:12px;line-height:20px;">
		<tr>
			<th></th>
			<th>Departure</th>
			<th>Arrival</th>
		<tr>
		<tr>
			<th align="left">Engine Hours</th>
			<td>'.$model->dep_engine_hours.'</td>
			<td>'.$model->arr_engine_hours.'</td>
		</tr>
		<tr>
			<th align="left">Time</th>
			<td>'.$model->dep_time.'</td>
			<td>'.$model->arr_time.'</td>
		</tr>
		<tr>
			<th align="left">Fuel Level</th>
			<td>'.$model->dep_fuel_level.'</td>
			<td>'.$model->arr_fuel_level.'</td>
		</tr>
	</table>
	';

$boatFuelConsumptionChart='';
if($model->booking->port_id!=Yii::$app->params['emp_id']){
$fuelConsumptionTypes=FuelConsumptionType::find()->asArray()->all();
if($fuelConsumptionTypes!=null){
$boatFuelConsumptionChart='
Below is the Boat’s Approximate Fuel Consumption Rates:<br /><br />
	<table cellpadding="10" cellspacing="0" border="1">
		<tr>';
foreach($fuelConsumptionTypes as $fuelConsumptionType){
$boatFuelConsumptionChart.='			<th>'.$fuelConsumptionType['title'].'</th>';
}
$boatFuelConsumptionChart.='		</tr>
		<tr>';
			foreach($fuelConsumptionTypes as $fuelConsumptionType){
				$valRow=BoatFuelConsumptionRate::find()->where(['boat_id'=>$model->boatProvided->id,'type_id'=>$fuelConsumptionType['id']])->asArray()->one();
				if($valRow!=null){
$boatFuelConsumptionChart.='			<th>'.$valRow['rate_value'].'</th>';
				}
			}
$boatFuelConsumptionChart.='		</tr>
	</table>';
}
}

$boatName = $model->boatProvided->name;

$vals = [
	'{marinaName}' => $model->booking->marina->name,
	'{boatname}' => $boatName,
	'{bookingDate}' => Yii::$app->formatter->asDate($model->booking->booking_date).' - '.$model->booking->timeZone->name,
	'{petrolInfo}' => $strPetrolInfo,
	'{bookingInOutInfo}' => $bookingInOutInfo,
	'{boatFuelConsumptionChart}' => $boatFuelConsumptionChart,
];
$htmlBody=$template->searchReplace($template->template_html,$vals);
?>
<div class="late-arrival">
	<?= $htmlBody?><br /><br />
</div>
