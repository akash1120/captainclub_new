<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Hello Admin,

A new request type Freeze has been posted, following is the information:

Name: <?= $model->member->fullname?>
Email: <?= $model->member->email?>
Date Posted: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>
Start Date: <?= Yii::$app->formatter->asDate($model->start_date)?>
End Date: <?= Yii::$app->formatter->asDate($model->end_date)?>
