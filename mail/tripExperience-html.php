<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
?>
<div class="late-arrival">
	<?= $msgHtml?><br />
    <p>
        <table cellpadding="10" cellspacing="0" border="1" style="border:1px solid #000; font-size:12px;">
        	<tr>
            	<th>Member</th>
            	<th>City</th>
            	<th>Marina</th>
            	<th>Boat</th>
            	<th>Date & Time</th>
            </tr>
            <tr>
            	<td><?= $booking->member->fullname?></td>
            	<td><?= $booking->city->name?></td>
            	<td><?= $booking->marina->name?></td>
            	<td><?= $booking->boat->name?></td>
            	<td><?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?></td>
            </tr>
        </table>
    </p>
</div>
