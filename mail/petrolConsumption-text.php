<?php
use app\models\FuelConsumptionType;
use app\models\BoatFuelConsumptionRate;
use app\models\BookingInOutChangedBoat;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $template app\models\EmailTemplate */
$creditBalance = 0;
if($model->booking!=null && $model->booking->member!=null){
	$creditBalance = $model->booking->member->profileInfo->credit_balance;
}
$strPetrolInfo = "
  Petrol Consumption: ".$model->fuel_cost." AED".($fuelBill!='' && $fuelBill!=null ? ', <span style="color:blue;">Bill attached</span>' : '')."
  Petrol Credit: ".$creditBalance." AED";

$bookingInOutInfo = '
  Engine Hours
  Departure: '.$model->dep_engine_hours.'
  Arrival: '.$model->arr_engine_hours.'

  Time
  Departure: '.$model->dep_time.'
  Arrival: '.$model->arr_time.'

  Fuel Level
  Departure: '.$model->dep_fuel_level.'
  Arrival: '.$model->arr_fuel_level.'
	';

$boatFuelConsumptionChart='';
$fuelConsumptionTypes=FuelConsumptionType::find()->asArray()->all();
if($fuelConsumptionTypes!=null){
	foreach($fuelConsumptionTypes as $fuelConsumptionType){
		$valRow=BoatFuelConsumptionRate::find()->where(['boat_id'=>$model->boatProvided->id,'type_id'=>$fuelConsumptionType['id']])->asArray()->one();
		if($valRow!=null){
$boatFuelConsumptionChart.=$fuelConsumptionType['title'].': '.$valRow['rate_value'].'
';
		}
	}
if($boatFuelConsumptionChart!=''){
$boatFuelConsumptionChart='Below is the Boat’s Approximate Fuel Consumption Rates:

'.$boatFuelConsumptionChart;
}
}

$boatName = $model->boatProvided->name;

$vals = [
	'{marinaName}' => $model->booking->marina->name,
	'{boatname}' => $boatName,
	'{bookingDate}' => Yii::$app->formatter->asDate($model->booking->booking_date).' - '.$model->booking->timeZone->name,
	'{petrolInfo}' => $strPetrolInfo,
	'{bookingInOutInfo}' => $bookingInOutInfo,
	'{boatFuelConsumptionChart}' => $boatFuelConsumptionChart,
];
$textBody=$template->searchReplace($template->template_text,$vals);
?>
<?= $textBody?>
