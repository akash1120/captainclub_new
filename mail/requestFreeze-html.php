<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<div class="new-request">
<p>Hello Admin,</p>

<p>A new request type Freeze has been posted, following is the information:</p>

<p>
<strong>Name:</strong> <?= $model->member->fullname?><br />
<strong>Email:</strong> <?= $model->member->email?><br />
<strong>Date Posted:</strong> <?= Yii::$app->formatter->asDate(date("Y-m-d"))?><br />
<strong>Start Date:</strong> <?= Yii::$app->formatter->asDate($model->start_date)?><br />
<strong>End Date:</strong> <?= Yii::$app->formatter->asDate($model->end_date)?><br />
</p>
</div>
