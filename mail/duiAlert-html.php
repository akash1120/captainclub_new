<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
$memberDUICount=$model->memberDUICount;
?>
Dear Admin,<br /><br />

DUI is reported by the team, Below are the details.<br /><br />

<strong>Member:</strong> <?= $booking->member->fullname?><br /><br />
<strong>Date & Time:</strong> <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?><br /><br />
<strong>Boat:</strong> <?= $booking->boat->name?><br /><br />
<strong>Marina:</strong> <?= $booking->marina->name?><br /><br /><br /><br />

<strong>DUI Comments:</strong> <?= $model->dui_comments?><br /><br />
<?php if($memberDUICount>0){?>
<strong>Previously Reported DUI:</strong> <?= $memberDUICount?><br /><br />
<?php }?>
