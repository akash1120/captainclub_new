<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
<div class="new-request">
<p>Hello Admin,</p>

<p>A new request type Boat Booking has been posted, following is the information:</p>

<p>
<strong>Name:</strong> <?= $model->member->fullname?><br />
<strong>Email:</strong> <?= $model->member->email?><br />
<strong>Date:</strong> <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>
</p>
<p>
<strong>Request Details:</strong><br />
<strong>City:</strong> <?= $model->city->name;?><br />
<strong>Marina:</strong> <?= $model->marina;?><br />
<strong>Requested Date:</strong> <?= Yii::$app->formatter->asDate($model->requested_date);?><br />
<strong>Time:</strong> <?= $model->time_slot;?><br />
<strong>Comments:</strong> <?= nl2br($model->descp);?><br />
</p>
</div>
