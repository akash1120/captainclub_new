<?php

/* @var $this yii\web\View */
/* @var $booking app\models\Booking */
/* @var $template app\models\EmailTemplate */
$bookingActivity=$booking->bookingActivity;
$lateTime = $bookingActivity->member_exp_late_hr.' hr'.($bookingActivity->member_exp_late_min!='' ? ' & '.$bookingActivity->member_exp_late_min.' mins' : '');
?>
<?= str_replace("{lateMinutes}",$lateTime,$template->template_text)?>


Member: <?= $booking->member->fullname?>

City: <?= $booking->city->name?>

Marina: <?= $booking->marina->name?>

Boat: <?= $booking->boat->name?>

Date & Time: <?= Yii::$app->formatter->asDate($booking->booking_date).' ('.$booking->timeZone->name.')'?>
