<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Hello Admin,

A new request type Other has been posted, following is the information:

Posted Date: <?= Yii::$app->formatter->asDate(date("Y-m-d"))?>
Name: <?= $model->member->fullname?>
Email: <?= $model->member->email?>
<?php if($model->requested_date!=null){?>
Requested Date: <?= Yii::$app->formatter->asDate($model->requested_date)?>
<?php }?>

<?php if($model->booking!=null){?>
Booking Detail:
City: <?= $model->booking->city->name?>
Marina: <?= $model->booking->marina->name?>
Boat: <?= $model->booking->boat->name?>
Date: <?= Yii::$app->formatter->asDate($model->booking->booking_date)?>
Time: <?= $model->booking->timeSlot->name?>
<?php }?>

Comments: <?= $model->descp?>
