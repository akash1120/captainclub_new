<?php

/* @var $this yii\web\View */
/* @var $user app\models\RequestForm */
?>
Dear Captain <?= $model->member->fullname?>,

I hope everything is going well with you.

We would like to inform you that your FTA local license will expire on <?= Yii::$app->formatter->asDate($model->license_expiry)?> and providing you the guidelines (attached file) on how to renew your license online.

Once you successfully done with the application, kindly provide us the copy of your Renewed FTA Local License to update our records in our system.

Note: Expired FTA license is not allowed to get a boat in any Marina as per the Coast guard regulations.

If you need further assistance concerning the application, please don't hesitate to contact me.

Thank you.

Best regards,
