<?php

return [
  'traceLevel' => YII_DEBUG ? 3 : 0,
  'targets' => [
    [
      'class' => 'yii\log\FileTarget',
      'levels' => ['error', 'warning'],
      'except' => [
        'Swift_TransportException',
        'yii\debug\Module::checkAccess',
        'yii\web\HttpException:400',
        'yii\web\HttpException:404',
        'yii\web\HttpException:405',
        'yii\db\Exception',
      ],
    ],
    [
      'class' => 'yii\log\FileTarget',
      'levels' => ['error', 'warning'],
      'categories' => ['yii\web\HttpException:404'],
      'logFile' => '@runtime/logs/404.log',
    ],
    [
      'class' => 'yii\log\FileTarget',
      'levels' => ['error', 'warning'],
      'categories' => ['Swift_TransportException'],
      'logFile' => '@runtime/logs/swift_mailer.log',
    ],
    [
      'class' => 'yii\log\FileTarget',
      'levels' => ['error', 'warning'],
      'categories' => ['yii\debug\Module::checkAccess'],
      'logFile' => '@runtime/logs/debug_access.log',
    ],
    [
      'class' => 'yii\log\FileTarget',
      'levels' => ['error', 'warning'],
      'categories' => ['yii\web\HttpException:405'],
      'logFile' => '@runtime/logs/methodnotallowed405.log',
    ],
    [
      'class' => 'yii\log\FileTarget',
      'levels' => ['error', 'warning'],
      'categories' => ['yii\web\HttpException:400'],
      'logFile' => '@runtime/logs/unverifieddata400.log',
    ],
    [
      'class' => 'yii\log\FileTarget',
      'levels' => ['error'],
      'categories' => ['yii\db\Exception'],
      'logFile' => '@runtime/logs/db_errors.log',
    ],
  ],
];
