<?php
use yii\helpers\Url;
use app\models\AdminMenu;
use app\models\AdminGroupPermissions;
date_default_timezone_set('Asia/Dubai');

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';
$urlManager = require __DIR__ . '/urlmanager.php';
$logs = require __DIR__ . '/logs.php';

$config = [
  'id' => 'theCaptainClubAE',
  'timezone' => 'Asia/Dubai',
  'language' => 'en-US',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
  ],
  'components' => [
    'assetManager'=>[
  		'basePath' => '@webroot/appassets',
		'baseUrl'=>'/appassets',
		'bundles' => [
        'yii\bootstrap\BootstrapAsset' => [
          'css' => ['plugins/bootstrap/css/bootstrap.css'],
        ],
        'yii\bootstrap\BootstrapPluginAsset' => [
           'js' => ['plugins/bootstrap/js/bootstrap.js']
        ],
      ],
  	],
		'formatter' => [
			'class' => 'yii\i18n\Formatter',
			'dateFormat' => 'php:D, jS M Y',
			'datetimeFormat' => 'php:j M, Y / h:i a',
			'timeFormat' => 'php:h:i a',
			'timeZone' => 'Asia/Dubai',
		],
    'session' => [
        'name' => 'PHPTCCSESSID',
    ],
    'request' => [
      // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
      'cookieValidationKey' => 'PlgF8crd8jEFssTeKOcclS2y-iv8rMHc',
    ],
    'jsFunctions' => [
      'class' => 'app\components\helpers\JavaScriptFunctions',
    ],
    'statsFunctions' => [
      'class' => 'app\components\helpers\StatsFunctions',
    ],
    'appHelperFunctions' => [
      'class' => 'app\components\helpers\AppHelperFunction',
    ],
    'menuHelperFunction' => [
      'class' => 'app\components\helpers\MenuHelperFunction',
    ],
    'activityHelper' => [
      'class' => 'app\components\helpers\ActivityHelper',
    ],
    'helperFunctions' => [
      'class' => 'app\components\helpers\HelperFunctions',
    ],
    'fileHelperFunctions' => [
      'class' => 'app\components\helpers\FileHelperFunctions',
    ],
    'bookingHelperFunctions' => [
      'class' => 'app\components\helpers\BookingHelperFunctions',
    ],
    'paytabsHelperFunctions' => [
      'class' => 'app\components\helpers\PayTabsFunctions',
    ],
    'operationHelperFunctions' => [
      'class' => 'app\components\helpers\OperationsFunctions',
    ],
    'waitingListHelper' => [
      'class' => 'app\components\helpers\WaitingListHelper',
    ],
    's3' => [
        'class' => 'frostealth\yii2\aws\s3\Service',
        'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
            'key' => 'AKIAIEXNAC4XAERHV6TQ',
            'secret' => '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',
        ],
        'region' => 'ap-south-1',
        'defaultBucket' => 'tcc-signatures',
        'defaultAcl' => 'public-read',
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'user' => [
      'identityClass' => 'app\models\User',
      'enableAutoLogin' => true,
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'mailer' => $mailer,
    'log' => $logs,
    'db' => $db,
    'urlManager' => $urlManager,
    'assetManager'=>[
        'appendTimestamp' => true,
    ],
    'view' => [
      'class' => '\rmrevin\yii\minify\View',
      'enableMinify' => false,
      'concatCss' => true, // concatenate css
      'minifyCss' => true, // minificate css
      'concatJs' => true, // concatenate js
      'minifyJs' => true, // minificate js
      'minifyOutput' => true, // minificate result html page
      'webPath' => '@web', // path alias to web base
      'basePath' => '@webroot', // path alias to web base
      'minifyPath' => '@webroot/minify', // path alias to save minify result
      'jsPosition' => [ \yii\web\View::POS_END ], // positions of js files to be minified
      'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
      'expandImports' => true, // whether to change @import on content
      'compressOptions' => ['extra' => true], // options for compress
      'excludeFiles' => [
        'jquery.js',
      ],
      'excludeBundles' => [
        \app\assets\AppTinyMceAsset::class,
      ],
    ]
  ],
  'params' => $params,
  'on beforeAction' => function ($event) {
    if (
      (Yii::$app->controller->id=='site') ||
      (Yii::$app->controller->id=='request-service') ||
      (Yii::$app->controller->id=='request-break-down') ||
      (Yii::$app->controller->id=='request-inspection') ||
      (Yii::$app->controller->id=='fix-rec') ||
      (Yii::$app->controller->id=='operations-fix-rec') ||
      (Yii::$app->controller->id=='mail-gun') ||
      (
        Yii::$app->controller->id=='booking' && Yii::$app->controller->action->id=='notification-response'
      )
    ) {

    }else{
        if(
            Yii::$app->controller->id=='site' &&
            (
		      Yii::$app->controller->action->id=='update-device-id' ||
		      Yii::$app->controller->action->id=='auto-login-device' ||
		      Yii::$app->controller->action->id=='login' ||
		      Yii::$app->controller->action->id=='logout' ||
		      Yii::$app->controller->action->id=='request-password-reset' ||
		      Yii::$app->controller->action->id=='request-password' ||
		      Yii::$app->controller->action->id=='reset-password' ||
		      Yii::$app->controller->action->id=='error'
		    )
        ){
            
        }else{
          if (Yii::$app->user->isGuest) {
            die('<script>window.location.href="'.Url::to(['site/login']).'"</script>');
          }else{
            if(Yii::$app->user->identity->profileInfo->changed_pass==0 || (Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->profileInfo->profile_updated==0)){
              if(Yii::$app->controller->id!='account' || (Yii::$app->controller->id=='account' && Yii::$app->controller->action->id!='confirm-profile')){
                die('<script>window.location.href="'.Url::to(['account/confirm-profile']).'"</script>');
              }
            }
            if(
              Yii::$app->controller->id=='site' ||
              Yii::$app->controller->id=='account' ||
              Yii::$app->controller->id=='suggestion' ||
              Yii::$app->controller->id=='booking' ||
              Yii::$app->controller->id=='request' ||
              Yii::$app->controller->id=='order' ||
                (
                  Yii::$app->controller->id=='claim' &&
                  (
                    Yii::$app->controller->action->id=='add-comments' ||
                    Yii::$app->controller->action->id=='add-order-comments' ||
                    Yii::$app->controller->action->id=='claim-bulk' ||
                    Yii::$app->controller->action->id=='claim-amount'
                  )
                ) ||
              (Yii::$app->controller->id=='bulk-booking' && Yii::$app->controller->action->id=='book-for-bulk-booking') ||
              (Yii::$app->controller->id=='user-requests' && Yii::$app->user->identity->user_type!=0 && (Yii::$app->controller->action->id=='boat-assign'))
            ){
    
            }else{
              if(Yii::$app->controller->id=='booking' && Yii::$app->controller->action->id=='all' && Yii::$app->user->identity->user_type==1){
    
              }else{
                $menu=AdminMenu::find()->where(['controller_id'=>Yii::$app->controller->id,'action_id'=>Yii::$app->controller->action->id])->one();
                if($menu!=null){
                  $group_id=Yii::$app->user->identity->permission_group_id;
                  $result=AdminGroupPermissions::find()->where(['group_id'=>$group_id,'menu_id'=>$menu->id,'is_allowed'=>1])->one();
                  if($result==null){
                    die('<script>window.location.href="'.Url::to(['site/not-allowed']).'"</script>');
                  }
                }else{
                  die('<script>window.location.href="'.Url::to(['site/not-allowed']).'"</script>');
                }
              }
            }
          }
        }
    }
  },
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  /*$config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  ];*/
}

return $config;
