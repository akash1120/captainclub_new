<?php

return [
    'siteName' => 'The Captains Club',
    'siteUrl' => 'http://local.captainclub_new/',
    'adminEmail' => 'admin@example.com',

    'user.passwordResetTokenExpire' => 3600,

    'requestEmail' => 'request@thecaptainsclub.ae',
  	'mdEmail' => 'md@thecaptainsclub.ae',
  	'oprHeadEmail' => 'ahmed@thecaptainsclub.ae',
  	'ahmedEmail' => 'ahmed@thecaptainsclub.ae',
  	'careNEmail' => 'caren@thecaptainsclub.ae',
  	'tcadminEmail' => 'admin@thecaptainsclub.ae',
  	'supportEmail' => 'icare@thecaptainsclub.ae',
  	'icareEmail' => 'icare@thecaptainsclub.ae',
  	'adminEmail' => 'admin@thecaptainsclub.ae',
  	'feedbackCc2' => 'samer@thecaptainsclub.ae',
    'boatRequiredEmailTo' => 'caren@thecaptainsclub.ae',
  	'expiryingAlertFrom' => 'samer@thecaptainsclub.ae',
  	'samerEmail' => 'samer@thecaptainsclub.ae',

    'bouncedEmail' => 'malick.naeem@gmail.com',

  	'smsSigNumber' => '+97152 641 4172',
  	'smsSigEmail' => 'icare@thecaptainsclub.ae',

    'emp_id' => 789,
  	'nightDriveTimeSlot' => 5,
  	'dropOffTimeSlot' => 6,

  	'ExpiryDaysToBookInNegative' => 75,
  	'ExpiryDaysToBookInNegativeBalance' => "-100",

    'memberPermGroup' => '2',

  	'pageSizeArray' => [
  		'25'=>'25',
  		'50'=>'50',
  		'75'=>'75',
  		'100'=>'100',
  		'150'=>'150',
  		'200'=>'200',
  	],

  	'inputTypes' => [
  		'1'=>'Text',
  		'2'=>'Textarea'
  	],

  	'boatRequiredBookUserId' => 458,
  	'boatRequiredBookType' => 5,

    'boat_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/boats/',
    'early_depart_asset_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/early_departure/',
    'discount_uploads_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/discount/',
    'wse_uploads_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/water_sports/',
    'member_uploads_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/members/',
    'member_license_uploads_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/member_license/',
    'ticket_uploads_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/ticket/',
    'newsletter_uploads_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/newsletter/',

    'fuelbill_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/fuelbill/',
  	'claimbill_uploads_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/claimbill/',

    'signatures_abs_path' => dirname(dirname(__DIR__)) . '/public_html/uploads/signature/',

    'temp_abs_path' => dirname(dirname(__DIR__)) . '/public_html/temp_uploads/',
    'temp_rel_path' => '/temp_uploads/',

  	'cache_abs_path' => dirname(dirname(__DIR__)) . '/public_html/cache/',
  	'cache_rel_path' => '/cache/',
    'images_uploads_rel_path' => '/images/',

    'downloads_abs_Path' => dirname(dirname(__DIR__)) . '/public_html/downloads/',

    'includeDefSignature' => true,
];
