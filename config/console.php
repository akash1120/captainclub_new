<?php
date_default_timezone_set('Asia/Dubai');

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';
$urlManager = require __DIR__ . '/urlmanager.php';
$logs = require __DIR__ . '/logs.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'formatter' => [
          'class' => 'yii\i18n\Formatter',
          'dateFormat' => 'php:D, jS M Y',
          'datetimeFormat' => 'php:j M, Y / h:i a',
          'timeFormat' => 'php:h:i a',
          'timeZone' => 'Asia/Dubai',
        ],
    's3' => [
        'class' => 'frostealth\yii2\aws\s3\Service',
        'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
            'key' => 'AKIAIEXNAC4XAERHV6TQ',
            'secret' => '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',
        ],
        'region' => 'ap-south-1',
        'defaultBucket' => 'tcc-signatures',
        'defaultAcl' => 'public-read',
    ],
    'statsFunctions' => [
      'class' => 'app\components\helpers\StatsFunctions',
    ],
    'appHelperFunctions' => [
      'class' => 'app\components\helpers\AppHelperFunction',
    ],
    'activityHelper' => [
      'class' => 'app\components\helpers\ActivityHelper',
    ],
        'helperFunctions' => [
          'class' => 'app\components\helpers\HelperFunctions',
        ],
    'fileHelperFunctions' => [
      'class' => 'app\components\helpers\FileHelperFunctions',
    ],
    'bookingHelperFunctions' => [
      'class' => 'app\components\helpers\BookingHelperFunctions',
    ],
    'paytabsHelperFunctions' => [
      'class' => 'app\components\helpers\PayTabsFunctions',
    ],
    'operationHelperFunctions' => [
      'class' => 'app\components\helpers\OperationsFunctions',
    ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => $mailer,
        'log' => $logs,
        'db' => $db,
        'urlManager' => $urlManager,
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
