<?php

return [
  'class' => 'app\components\helpers\MyMailer',
  'testmode' => true,
  'useFileTransport' => false,
  'transport' => [
    'class' => 'Swift_SmtpTransport',
    'host' => 'local.captainclub_new',
    'username' => 'noreply@local.captainclub_new',
    'password' => '3jHc[s*1XMT;',
    'port' => '465',
    'encryption' => 'ssl',
  ],
];