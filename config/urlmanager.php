<?php

return [
  'enablePrettyUrl' => true,
  'showScriptName' => false,
  'baseUrl' => 'http://local.captainclub_new/',
  'rules' => [
    'operations' => 'site/operations-login',
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
  ],
];
