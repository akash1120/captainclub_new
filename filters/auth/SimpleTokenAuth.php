<?php
namespace app\api\filters\auth;

use Yii;
use yii\filters\auth\AuthMethod;

class SimpleTokenAuth extends AuthMethod
{
  public $realm='api';
  /**
   * @inheritdoc
   */
  public function authenticate($user, $request, $response)
  {
      $authHeader = $request->getHeaders()->get('Authorization');
      if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
          $identity = $this->findIdentityByAccessToken($matches[1], get_class($this));
          if ($identity === null) {
              $this->handleFailure($response);
          }
          return $identity;
      }

      return null;
  }
  	/**
  	* @inheritdoc
  	*/
  	public static function findIdentityByAccessToken($token, $type = null)
  	{
  		if($token=='ACCESSTOKEN321'){
  			return 'Yes';
  		}
  		return false;
  	}
}
