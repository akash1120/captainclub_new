<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* ChargeUserCredit is the model behind the member charge form.
*/
class ChargeUserCredit extends Model
{
  public $user_id,$credits,$descp;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['user_id','credits','descp'], 'required'],
      ['user_id','integer'],
      ['credits','number'],
      [['descp','descp'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'credits' => 'Credits',
      'descp' => 'Comments',
    ];
  }

  /**
  * Save credits to user account
  */
  public function save()
  {
    if ($this->validate()) {
      //Creating Cr Transaction
      $crtransaction=new UserAccounts;
      $crtransaction->user_id=$this->user_id;
      $crtransaction->trans_type='dr';
      $crtransaction->descp=$this->descp;
      $crtransaction->account_id=10;
      $crtransaction->amount=$this->credits;
      $crtransaction->booking_id=0;
      $crtransaction->save();
      return true;
    }
    return false;
  }
}
