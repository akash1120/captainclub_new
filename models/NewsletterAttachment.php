<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* NewsletterAttachment
*/
class NewsletterAttachment extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%newsletter_attachment}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['newsletter_id','attachment_file'], 'required'],
      [['newsletter_id'], 'integer'],
      [['file_title', 'attachment_file'], 'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'file_title' => Yii::t('app', 'File Title'),
      'attachment_file' => Yii::t('app', 'File'),
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getNewsletter()
  {
    return $this->hasOne(Newsletter::className(), ['id' => 'newsletter_id']);
  }
}
