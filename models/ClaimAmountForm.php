<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
* ClaimAmountForm is the model behind the amount based claiming form.
*/
class ClaimAmountForm extends Model
{
  public $city_id,$port_id,$amount,$comments;
  public $file,$bill_image;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'svg', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['city_id', 'port_id', 'amount', 'bill_image'], 'required'],
      [['city_id', 'port_id'], 'integer'],
      [['amount'], 'number'],
      [['comments'], 'string'],
      [['amount'], 'validateAmount'],
      [['bill_image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes)],
    ];
  }

  /**
   * Validates the password.
   * This method serves as the inline validation for password.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function validateAmount($attribute, $params)
  {
      if (!$this->hasErrors()) {
        $amountToClaim=Yii::$app->appHelperFunctions->getUnClaimedMarinaAmount($this->port_id);
          if ($this->amount>$amountToClaim) {
              $this->addError($attribute, 'Too much to claim?');
          }
          if ($this->amount<1) {
              $this->addError($attribute, 'Too less to claim?');
          }
      }
  }

  /**
   * @inheritdoc
   */
  public function beforeValidate()
	{
    //Uploading Image
    if(UploadedFile::getInstance($this, 'bill_image')){
      $this->file = UploadedFile::getInstance($this, 'bill_image');
      // if no image was uploaded abort the upload
      if (!empty($this->file)) {
        $pInfo=pathinfo($this->file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->file->tempName);
          if (preg_match('/\<\?php/i', $content)) {

          }else{
            $name = Yii::$app->fileHelperFunctions->generateName();
            if (getimagesize($this->file->tempName)) {
              // generate a unique file name
              $this->bill_image = $name.".{$ext}";
            }
          }
        }
      }
    }
    if($this->bill_image!=''){
      return true;
    }else{
      return false;
    }
		return parent::beforeValidate();
  }

  public function save()
  {
    if ($this->validate()) {

      if ($this->file!== null && $this->bill_image!=null) {
  			$this->bill_image=Yii::$app->fileHelperFunctions->generateImageFromUploadedFile($this->file,$this->bill_image);
  		}

      $order=new ClaimOrder;
      $order->user_id=Yii::$app->user->identity->id;
      $order->city_id=$this->city_id;
      $order->marina_id=$this->port_id;
      $order->total_amount=$this->amount;
      $order->bill_image=$this->bill_image;
      $order->process_type=0;
      $order->user_type=1;
      $order->comments=$this->comments;
      $order->save();
      return true;
    }
    return false;
  }
}
