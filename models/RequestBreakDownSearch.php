<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RequestBreakDown;

/**
* RequestBreakDownSearch represents the model behind the search form of `app\models\RequestBreakDown`.
*/
class RequestBreakDownSearch extends RequestBreakDown
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','boat_id','status','completed','created_by','updated_by','pageSize'],'integer'],
      [['description', 'break_down_level', 'reason', 'user_remarks', 'req_date', 'admin_remarks', 'admin_action_date','completed_date','created_at','updated_at'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = RequestBreakDown::find()
    ->select([
      RequestBreakDown::tableName().'.id',
      'req_date',
      'description',
      'break_down_level',
      'reason',
      'user_remarks',
      RequestBreakDown::tableName().'.status',
      'admin_remarks',
      'admin_action_date',
      'completed',
      'completed_date',
      'boat_name'=>Boat::tableName().".name",
      RequestBreakDown::tableName().'.created_at',
    ])
    ->innerJoin(Boat::tableName(),Boat::tableName().".id=".RequestBreakDown::tableName().".boat_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort' => ['defaultOrder'=>['id'=>SORT_DESC]],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      RequestBreakDown::tableName().'.id' => $this->id,
      'boat_id' => $this->boat_id,
      'break_down_level' => $this->break_down_level,
      'reason' => $this->reason,
      RequestBreakDown::tableName().'.status' => $this->status,
      'completed' => $this->completed,
    ]);

    if($this->req_date!=null){
      //Check if its range
      if(strpos($this->req_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->req_date);
        $query->andWhere(['and',['>=','req_date',$start_date],['<=','req_date',$end_date]]);
      }else{
        $query->andWhere(['req_date'=>$this->req_date]);
      }
    }

    if($this->admin_action_date!=null){
      //Check if its range
      if(strpos($this->admin_action_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->admin_action_date);
        $query->andWhere(['and',['>=','admin_action_date',$start_date],['<=','admin_action_date',$end_date]]);
      }else{
        $query->andWhere(['admin_action_date'=>$this->admin_action_date]);
      }
    }

    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $query->andWhere(['and',['>=',RequestBreakDown::tableName().'.created_at',$start_date],['<=',RequestBreakDown::tableName().'.created_at',$end_date]]);
      }else{
        $query->andWhere([RequestBreakDown::tableName().'.created_at'=>$this->created_at]);
      }
    }

    $query->andFilterWhere(['like','description',$this->description])
    ->andFilterWhere(['like','user_remarks',$this->user_remarks])
    ->andFilterWhere(['like','admin_remarks',$this->admin_remarks]);

    return $dataProvider;
  }
}
