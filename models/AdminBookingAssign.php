<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%admin_booking_assign}}".
*
* @property integer $id
* @property integer $boat_required_id
* @property integer $booking_id
* @property integer $from_user_id
* @property integer $to_user_id
* @property string $comments
* @property integer $created_by
* @property string $created_at
* @property integer $updated_by
* @property string $updated_at
*/
class AdminBookingAssign extends ActiveRecord
{
  public $member_name;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%admin_booking_assign}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
        [['member_name','booking_id','from_user_id','to_user_id','comments'], 'required'],
        [['boat_required_id', 'booking_id', 'booking_type', 'from_user_id', 'to_user_id', 'created_by', 'updated_by'], 'integer'],
        [['member_name','booking_comments','comments'], 'string'],
        [['created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_required_id' => Yii::t('app', 'Boat Required Request'),
      'booking_id' => Yii::t('app', 'Booking'),
      'from_user_id' => Yii::t('app', 'From'),
      'to_user_id' => Yii::t('app', 'Assign To'),
      'member_name' => Yii::t('app', 'Assign To'),
      'created_by' => Yii::t('app', 'Who'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBooking()
  {
      return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
		if($insert){
      $connection = \Yii::$app->db;
      $connection->createCommand(
       "update ".Booking::tableName()." set user_id=:user_id,booking_type=0,booking_comments=NULL where id=:id",
       [':user_id'=>$this->to_user_id,':id'=>$this->booking_id]
      )->execute();

      $checkRequest=BoatRequiredBooking::find()->select(['request_id'])->where(['booking_id'=>$this->booking_id])->asArray()->one();
      if($checkRequest!=null){
        $connection->createCommand(
         "update ".self::tableName()." set boat_required_id=:boat_required_id where id=:id",
         [':boat_required_id'=>$checkRequest['request_id'],':id'=>$this->id]
        )->execute();
      }
    }
		parent::afterSave($insert, $changedAttributes);
  }
}
