<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%bulk_booking_replacement}}".
*
* @property integer $id
* @property integer $user_booking_id
* @property integer $bulk_booking_id
*/
class BulkBookingReplacement extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%bulk_booking_replacement}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_booking_id', 'bulk_booking_id'], 'required'],
    	[['user_booking_id', 'bulk_booking_id', 'created_by', 'updated_by'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
        'user_booking_id' => Yii::t('app', 'Member Booking'),
        'bulk_booking_id' => Yii::t('app', 'Admin Booking'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMemberBooking()
  {
      return $this->hasOne(Booking::className(), ['id' => 'user_booking_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAdminBooking()
  {
      return $this->hasOne(Booking::className(), ['id' => 'bulk_booking_id']);
  }

  public function sendEmail()
  {
    //Bulk Booking - Boat Replaced
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_bulkbookboatreplaced_u');
    $template=EmailTemplate::findOne($templateId);
    if($template!=null){
      $vals = [
        '{oldBoatName}' => $this->adminBooking->boat->name,
        '{oldMarinaName}' => $this->adminBooking->marina->name,
        '{oldTime}' => $this->adminBooking->timeZone->name,
        '{bookingDate}' => date("l, jS M Y",strtotime($this->memberBooking->booking_date)),
        '{newBoatName}' => $this->memberBooking->boat->name,
        '{newMarinaName}' => $this->memberBooking->marina->name,
        '{newTime}' => $this->memberBooking->timeZone->name,
      ];
      $htmlBody=$template->searchReplace($template->template_html,$vals);
      $textBody=$template->searchReplace($template->template_text,$vals);
      Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => nl2br($htmlBody), 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setTo($this->memberBooking->member->email)
        ->setSubject('Boat Replaced')
        ->send();
    }
    //Bulk Booking - SMS
    if($this->memberBooking->member->profileInfo->profile_updated==1 && $this->memberBooking->member->profileInfo->mobile!=null && $this->memberBooking->member->profileInfo->mobile!=''){
      $mobileNumber=Yii::$app->helperFunctions->fullMobileNumber($this->memberBooking->member->profileInfo->mobile);
      if($mobileNumber!=null && $mobileNumber!=''){
        $smsMsg="Dear Captain\n
Your booking for ".$this->adminBooking->boat->name." from ".$this->adminBooking->marina->name." on ".Yii::$app->formatter->asDate($this->memberBooking->booking_date)." is replaced with ".$this->memberBooking->boat->name." from ".$this->memberBooking->marina->name." at ".$this->memberBooking->timeZone->name.". Original boat is temporary Not Available.\n
Sorry for the inconvenience.\n
M: ".Yii::$app->params['smsSigNumber']."
E: ".Yii::$app->params['smsSigEmail']."";
        Yii::$app->helperFunctions->sendSms($mobileNumber,$smsMsg);
      }
    }
  }
}
