<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* ReportForm
*/
class ReportForm extends Model
{
  public $date,$start_date,$end_date;

  /**
  * @param  string                          $token
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  * @throws \yii\base\InvalidParamException if token is empty or not valid
  */
  public function __construct($config = [],$date)
  {
    if($date==null)$date=(date("Y-m-d", strtotime("-1 WEEK", strtotime(date("Y-m-d")))).' - '.date("Y-m-d"));
    $this->date = $date;
    list($this->start_date,$this->end_date)=explode(" - ",$this->date);
    parent::__construct($config);
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['date'], 'string'],
    ];
  }
}
