<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%admin_menu}}".
*
* @property integer $id
* @property integer $parent
* @property string $icon
* @property string $title
* @property string $controller_id
* @property string $action_id
* @property string $param1
* @property string $value1
* @property integer $show_in_menu
* @property integer $rank
*/
class AdminMenu extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%admin_menu}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['parent', 'show_in_menu','rank'], 'integer'],
      [['title'], 'required'],
      [['title'],'trim'],
      [['icon'], 'string', 'max' => 20],
      [['title', 'controller_id', 'action_id'], 'string', 'max' => 50],
      [['param1', 'value1'], 'string', 'max' => 15]
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'parent' => Yii::t('app', 'Parent'),
      'icon' => Yii::t('app', 'Icon'),
      'title' => Yii::t('app', 'Title'),
      'controller_id' => Yii::t('app', 'Controller'),
      'action_id' => Yii::t('app', 'Action'),
      'param1' => Yii::t('app', 'Param1'),
      'value1' => Yii::t('app', 'Value1'),
      'show_in_menu' => Yii::t('app', 'Show In Menu'),
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getSubOptions()
  {
    return $this->hasMany(AdminMenu::className(), ['parent' => 'id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getSubMenuOptions()
  {
    return $this->hasMany(AdminMenu::className(), ['parent' => 'id'])->andWhere(['show_in_menu'=>1]);
  }
}
