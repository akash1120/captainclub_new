<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%arrival_timings}}".
*
* @property integer $id
* @property integer $time_slot_id
* @property string $title
* @property integer $sort_order
*/
class ArrivalTimings extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%arrival_timings}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['time_slot_id', 'title', 'sort_order'], 'required'],
      [['time_slot_id', 'sort_order'], 'integer'],
      [['title'], 'string', 'max' => 50]
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'time_slot_id' => Yii::t('app', 'Time Slot'),
      'title' => Yii::t('app', 'Title'),
      'sort_order' => Yii::t('app', 'Order'),
    ];
  }
}
