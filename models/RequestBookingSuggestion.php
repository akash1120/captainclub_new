<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
* RequestBookingSuggestion is the model behind the request type boat booking suggestion form.
*/
class RequestBookingSuggestion extends Model
{
  public $city_id,$port_id,$boat_id,$time_slot_id,$date,$port_boat_time_id;
  public $user_id;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['user_id','city_id','date','port_boat_time_id'], 'required'],
      [['date'], 'safe'],
      [['user_id','city_id'], 'integer'],
      [['port_boat_time_id'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'city_id' => 'City',
      'date' => 'Date',
      'port_boat_time_id' => 'Boat',
    ];
  }

  /**
  * Sends an email to the specified email address using the information collected by this model.
  * @param  string  $email the target email address
  * @return boolean whether the model passes validation
  */
  public function send()
  {
    if ($this->validate()) {
      if($this->port_boat_time_id){
        list($this->port_id,$this->boat_id,$this->time_slot_id)=explode("_",$this->port_boat_time_id);
        $booking=new Booking;
        $booking->booking_source='member';
        $booking->city_id=$this->city_id;
        $booking->port_id=$this->port_id;
        $booking->boat_id=$this->boat_id;
        $booking->booking_date=$this->date;
        $booking->booking_time_slot=$this->boat_id.'_'.$this->time_slot_id;
        if($booking->save()){
          return true;
        }else{
          foreach($booking->getErrors() as $error){
            foreach($error as $key=>$val){
              $this->addError('city_id',$val);
            }
          }
          return false;
        }
        return true;
      }else{
        return false;
      }
    }
    return false;
  }
}
