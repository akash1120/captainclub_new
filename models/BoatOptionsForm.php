<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
* BoatOptionsForm is the model behind the boat addons & night drive access assignment
*/
class BoatOptionsForm extends Model
{
  public $marina_id,$boat_id,$boat_timings;
  public $watersports,$bbq,$early_departure,$ice,$kids_life_jacket,$overnight_camp,$late_arrival,$wake_boarding,$wake_surfing;
  public $marina_city_id,$marina_name,$marina_short_name;
  public $marina_captains,$marina_bbq,$marina_no_of_early_departures,$marina_no_of_late_arrivals,$marina_no_of_overnight_camps,$marina_night_drive_limit,$marina_wake_boarding_limit,$marina_wake_surfing_limit;
  public $marina_fuel_profit;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['marina_id','boat_timings'], 'required'],
      [['marina_fuel_profit'], 'number'],
      [['marina_name','marina_short_name'], 'string'],
      [['marina_city_id','marina_captains','marina_bbq','marina_no_of_early_departures','marina_no_of_late_arrivals','marina_no_of_overnight_camps','marina_night_drive_limit','marina_wake_boarding_limit','marina_wake_surfing_limit'], 'integer'],
      [['boat_timings','watersports','bbq','early_departure','ice','kids_life_jacket','overnight_camp','late_arrival','wake_boarding','wake_surfing'], 'safe'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'marina_city_id' => 'City',
      'marina_name' => 'Name',
      'marina_short_name' => 'Short Name',
      'marina_captains' => 'Captains Limit',
      'marina_bbq' => 'BBQ Limit',
      'marina_no_of_early_departures' => 'Early Departure Limit',
      'marina_no_of_late_arrivals' => 'Late Arrivals Limit',
      'marina_no_of_overnight_camps' => 'Overnight Camping Limit',
      'marina_night_drive_limit' => 'Night Drive Session Limit',
      'marina_wake_boarding_limit' => 'Wake Boarding Limit',
      'marina_wake_surfing_limit' => 'Wake Surfing Limit',
      'marina_fuel_profit' => 'Fuel Profit',
    ];
  }

  /**
  * @return boolean whether the model passes validation
  */
  public function save()
  {
    if ($this->validate()) {
      $marina=Marina::findOne($this->marina_id);
      $marina->city_id=$this->marina_city_id;
      $marina->name=$this->marina_name;
      $marina->short_name=$this->marina_short_name;
      $marina->fuel_profit=$this->marina_fuel_profit;
      $marina->captains=$this->marina_captains;
      $marina->bbq=$this->marina_bbq;
      $marina->no_of_early_departures=$this->marina_no_of_early_departures;
      $marina->no_of_late_arrivals=$this->marina_no_of_late_arrivals;
      $marina->no_of_overnight_camps=$this->marina_no_of_overnight_camps;
      $marina->night_drive_limit=$this->marina_night_drive_limit;
      $marina->wake_boarding_limit=$this->marina_wake_boarding_limit;
      $marina->wake_surfing_limit=$this->marina_wake_surfing_limit;
      $marina->save();
      $connection = \Yii::$app->db;
      foreach($this->boat_timings as $boatId=>$timings){
        $boat=Boat::findOne($boatId);
        BoatToTimeSlot::deleteAll(['and', 'boat_id = :boat_id', ['not in', 'time_slot_id', $timings]], [':boat_id' => $boatId]);
        foreach($timings as $key=>$val){
          $boatTimeSlotRow=BoatToTimeSlot::find()->where(['boat_id'=>$boatId,'time_slot_id'=>$val])->one();
          if($boatTimeSlotRow==null){
            $boatTimeSlotRow=new BoatToTimeSlot;
            $boatTimeSlotRow->boat_id=$boatId;
            $boatTimeSlotRow->time_slot_id=$val;
            $boatTimeSlotRow->save();
          }
        }
        $watersports=isset($this->watersports[$boatId]) && $this->watersports[$boatId]==1 ? 1 : 0;
        $bbq=isset($this->bbq[$boatId]) && $this->bbq[$boatId]==1 ? 1 : 0;
        $early_departure=isset($this->early_departure[$boatId]) && $this->early_departure[$boatId]==1 ? 1 : 0;
        $ice=isset($this->ice[$boatId]) && $this->ice[$boatId]==1 ? 1 : 0;
        $kids_life_jacket=isset($this->kids_life_jacket[$boatId]) && $this->kids_life_jacket[$boatId]==1 ? 1 : 0;
        $overnight_camp=isset($this->overnight_camp[$boatId]) && $this->overnight_camp[$boatId]==1 ? 1 : 0;
        $late_arrival=isset($this->late_arrival[$boatId]) && $this->late_arrival[$boatId]==1 ? 1 : 0;
        $wake_boarding=isset($this->wake_boarding[$boatId]) && $this->wake_boarding[$boatId]==1 ? 1 : 0;
        $wake_surfing=isset($this->wake_surfing[$boatId]) && $this->wake_surfing[$boatId]==1 ? 1 : 0;

        $connection->createCommand(
          "update ".Boat::tableName()." set
          watersports=:watersports,
          bbq=:bbq,
          early_departure=:early_departure,
          ice=:ice,
          kids_life_jacket=:kids_life_jacket,
          overnight_camp=:overnight_camp,
          late_arrival=:late_arrival,
          wake_boarding=:wake_boarding,
          wake_surfing=:wake_surfing
           where id=:id",
          [
            ':watersports'=>$watersports,
            ':bbq'=>$bbq,
            ':early_departure'=>$early_departure,
            ':ice'=>$ice,
            ':kids_life_jacket'=>$kids_life_jacket,
            ':overnight_camp'=>$overnight_camp,
            ':late_arrival'=>$late_arrival,
            ':wake_boarding'=>$wake_boarding,
            ':wake_surfing'=>$wake_surfing,
            ':id'=>$boatId,
          ]
        )
        ->execute();
      }
      return true;
    }
    return false;
  }
}
