<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Discount;

/**
* DiscountSearch represents the model behind the search form of `app\models\Discount`.
*/
class DiscountSearch extends Discount
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['title','descp','status','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['title'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = Discount::find()
    ->select([
      'id',
      'title',
      'descp',
      'image',
      'status',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like','title',$this->title]);

    return $dataProvider;
  }
}
