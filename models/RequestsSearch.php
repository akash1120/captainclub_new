<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\data\ActiveDataProvider;

/**
* RequestsSearch
*/
class RequestsSearch extends AppColor
{
  public $listType,$user_id,$membername,$item_type,$requested_date,$status,$expected_reply,$remarks,$pageSize;
  public $contract_id;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','user_id','status','contract_id','pageSize'],'integer'],
      [['listType','membername','item_type','requested_date','expected_reply','created_at','remarks'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    if(Yii::$app->user->identity->user_type!=0){
      $activeShowTill=date("Y-m-d");
    }else{
      $activeShowTill=date("Y-m-d", strtotime("-2 day", strtotime(date("Y-m-d"))));
    }

    $userIdz=[];
    if($this->contract_id!=null){
      $contractMembers=ContractMember::find()->select(['user_id'])->where(['contract_id'=>$this->contract_id])->asArray()->all();
      if($contractMembers!=null){
        foreach($contractMembers as $contractMember){
          $userIdz[]=$contractMember['user_id'];
        }
      }
    }else{
      if($this->user_id!=null){
        $user=User::find()->where(['id'=>$this->user_id])->one();
        $contractMembers=ContractMember::find()->select(['user_id'])->where(['contract_id'=>$user->active_contract_id])->asArray()->all();
        if($contractMembers!=null){
          foreach($contractMembers as $contractMember){
            $userIdz[]=$contractMember['user_id'];
          }
        }
      }
    }

    //Freeze Requests
    $freeze_requests=UserFreezeRequest::find()
    ->select([
      UserFreezeRequest::tableName().'.id',
      new Expression("'freeze' as item_type"),
      new Expression("'' as booking_id"),
      UserFreezeRequest::tableName().'.user_id',
      new Expression("'' as descp"),
      UserFreezeRequest::tableName().'.requested_date',
      UserFreezeRequest::tableName().'.status',
      new Expression("'' as reason"),
      new Expression("'' as remarks"),
      new Expression("'' as admin_action_date"),
      UserFreezeRequest::tableName().'.active_show_till',
      new Expression("'' as expected_reply"),
      UserFreezeRequest::tableName().'.created_at',
      UserFreezeRequest::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".UserFreezeRequest::tableName().".user_id")
    ->where(['and',
      [UserFreezeRequest::tableName().'.trashed'=>0],
    ]);

    $freeze_requests->andFilterWhere([
      UserFreezeRequest::tableName().'.user_id' => $userIdz,
      UserFreezeRequest::tableName().'.status' => $this->status,
    ]);
    $freeze_requests->andFilterWhere(['like',UserFreezeRequest::tableName().'.remarks',$this->remarks])
    ->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$freeze_requests->andWhere(['or',['is',UserFreezeRequest::tableName().'.active_show_till',NULL],['>=',UserFreezeRequest::tableName().'.active_show_till',date("Y-m-d")]]);
      $freeze_requests->andWhere(['>=',UserFreezeRequest::tableName().'.requested_date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $freeze_requests->andWhere(['<',UserFreezeRequest::tableName().'.requested_date',date("Y-m-d")]);
    }
    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $freeze_requests->andWhere(['and',['>=',UserFreezeRequest::tableName().'.requested_date',$start_date],['<=',UserFreezeRequest::tableName().'.requested_date',$end_date]]);
      }else{
        $freeze_requests->andWhere([UserFreezeRequest::tableName().'.requested_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $freeze_requests->andWhere(['and',['>=','DATE('.UserFreezeRequest::tableName().'.created_at)',$start_date],['<=','DATE('.UserFreezeRequest::tableName().'.created_at)',$end_date]]);
      }else{
        $freeze_requests->andWhere(['DATE('.UserFreezeRequest::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Boat Booking Requests
    $boat_booking_requests=UserBookingRequest::find()
    ->select([
      UserBookingRequest::tableName().'.id',
      new Expression("'boatbooking' as item_type"),
      new Expression("'' as booking_id"),
      UserBookingRequest::tableName().'.user_id',
      UserBookingRequest::tableName().'.descp',
      UserBookingRequest::tableName().'.requested_date',
      UserBookingRequest::tableName().'.status',
      new Expression("'' as reason"),
      UserBookingRequest::tableName().'.remarks',
      UserBookingRequest::tableName().'.admin_action_date',
      UserBookingRequest::tableName().'.active_show_till',
      new Expression("'' as expected_reply"),
      UserBookingRequest::tableName().'.created_at',
      UserBookingRequest::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".UserBookingRequest::tableName().".user_id")
    ->where(['and',
      [UserBookingRequest::tableName().'.trashed'=>0],
    ]);

    $boat_booking_requests->andFilterWhere([
      UserBookingRequest::tableName().'.user_id' => $userIdz,
      UserBookingRequest::tableName().'.status' => $this->status,
    ]);
    $boat_booking_requests->andFilterWhere(['like',UserBookingRequest::tableName().'.remarks',$this->remarks])
    ->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$boat_booking_requests->andWhere(['or',['is',UserBookingRequest::tableName().'.active_show_till',NULL],['>=',UserBookingRequest::tableName().'.active_show_till',date("Y-m-d")]]);
      $boat_booking_requests->andWhere(['>=',UserBookingRequest::tableName().'.requested_date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $boat_booking_requests->andWhere(['<',UserBookingRequest::tableName().'.requested_date',date("Y-m-d")]);
    }
    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $boat_booking_requests->andWhere(['and',['>=',UserBookingRequest::tableName().'.requested_date',$start_date],['<=',UserBookingRequest::tableName().'.requested_date',$end_date]]);
      }else{
        $boat_booking_requests->andWhere([UserBookingRequest::tableName().'.requested_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $boat_booking_requests->andWhere(['and',['>=','DATE('.UserBookingRequest::tableName().'.created_at)',$start_date],['<=','DATE('.UserBookingRequest::tableName().'.created_at)',$end_date]]);
      }else{
        $boat_booking_requests->andWhere(['DATE('.UserBookingRequest::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Nightdrive Training Requests
    $nd_training_requests=UserNightdriveTrainingRequest::find()
    ->select([
      UserNightdriveTrainingRequest::tableName().'.id',
      new Expression("'nightdrivetraining' as item_type"),
      new Expression("'' as booking_id"),
      UserNightdriveTrainingRequest::tableName().'.user_id',
      UserNightdriveTrainingRequest::tableName().'.descp',
      UserNightdriveTrainingRequest::tableName().'.requested_date',
      UserNightdriveTrainingRequest::tableName().'.status',
      new Expression("'' as reason"),
      UserNightdriveTrainingRequest::tableName().'.remarks',
      UserNightdriveTrainingRequest::tableName().'.admin_action_date',
      UserNightdriveTrainingRequest::tableName().'.active_show_till',
      new Expression("'' as expected_reply"),
      UserNightdriveTrainingRequest::tableName().'.created_at',
      UserNightdriveTrainingRequest::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".UserNightdriveTrainingRequest::tableName().".user_id")
		->where(['and',
      [UserNightdriveTrainingRequest::tableName().'.trashed'=>0],
		]);

    $nd_training_requests->andFilterWhere([
      UserNightdriveTrainingRequest::tableName().'.user_id' => $userIdz,
      UserNightdriveTrainingRequest::tableName().'.status' => $this->status,
    ]);
    $nd_training_requests->andFilterWhere(['like',UserNightdriveTrainingRequest::tableName().'.remarks',$this->remarks])
    ->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$nd_training_requests->andWhere(['or',['is',UserNightdriveTrainingRequest::tableName().'.active_show_till',NULL],['>=',UserNightdriveTrainingRequest::tableName().'.active_show_till',date("Y-m-d")]]);
      $nd_training_requests->andWhere(['>=',UserNightdriveTrainingRequest::tableName().'.requested_date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $nd_training_requests->andWhere(['<',UserNightdriveTrainingRequest::tableName().'.requested_date',date("Y-m-d")]);
    }
    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $nd_training_requests->andWhere(['and',['>=',UserNightdriveTrainingRequest::tableName().'.requested_date',$start_date],['<=',UserNightdriveTrainingRequest::tableName().'.requested_date',$end_date]]);
      }else{
        $nd_training_requests->andWhere([UserNightdriveTrainingRequest::tableName().'.requested_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $nd_training_requests->andWhere(['and',['>=','DATE('.UserNightdriveTrainingRequest::tableName().'.created_at)',$start_date],['<=','DATE('.UserNightdriveTrainingRequest::tableName().'.created_at)',$end_date]]);
      }else{
        $nd_training_requests->andWhere(['DATE('.UserNightdriveTrainingRequest::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Upgrade City Requests Query
    $upgradecity_requests=UserCityUpgradeRequest::find()
    ->select([
      UserCityUpgradeRequest::tableName().'.id',
      new Expression("'upgradecity' as item_type"),
      new Expression("'' as booking_id"),
      UserCityUpgradeRequest::tableName().'.user_id',
      UserCityUpgradeRequest::tableName().'.descp',
      UserCityUpgradeRequest::tableName().'.requested_date',
      UserCityUpgradeRequest::tableName().'.status',
      new Expression("'' as reason"),
      UserCityUpgradeRequest::tableName().'.remarks',
      UserCityUpgradeRequest::tableName().'.admin_action_date',
      UserCityUpgradeRequest::tableName().'.active_show_till',
      new Expression("'' as expected_reply"),
      UserCityUpgradeRequest::tableName().'.created_at',
      UserCityUpgradeRequest::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".UserCityUpgradeRequest::tableName().".user_id")
		->where(['and',
      [UserCityUpgradeRequest::tableName().'.trashed'=>0],
		]);

    $upgradecity_requests->andFilterWhere([
      UserCityUpgradeRequest::tableName().'.user_id' => $userIdz,
      UserCityUpgradeRequest::tableName().'.status' => $this->status,
    ]);
    $upgradecity_requests->andFilterWhere(['like',UserCityUpgradeRequest::tableName().'.remarks',$this->remarks])
    ->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$upgradecity_requests->andWhere(['or',['is',UserCityUpgradeRequest::tableName().'.active_show_till',NULL],['>=',UserCityUpgradeRequest::tableName().'.active_show_till',date("Y-m-d")]]);
      $upgradecity_requests->andWhere(['>=',UserCityUpgradeRequest::tableName().'.requested_date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $upgradecity_requests->andWhere(['<',UserCityUpgradeRequest::tableName().'.requested_date',date("Y-m-d")]);
    }
    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $upgradecity_requests->andWhere(['and',['>=',UserCityUpgradeRequest::tableName().'.requested_date',$start_date],['<=',UserCityUpgradeRequest::tableName().'.requested_date',$end_date]]);
      }else{
        $upgradecity_requests->andWhere([UserCityUpgradeRequest::tableName().'.requested_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $upgradecity_requests->andWhere(['and',['>=','DATE('.UserCityUpgradeRequest::tableName().'.created_at)',$start_date],['<=','DATE('.UserCityUpgradeRequest::tableName().'.created_at)',$end_date]]);
      }else{
        $upgradecity_requests->andWhere(['DATE('.UserCityUpgradeRequest::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Other Requests Query
    $other_requests=UserOtherRequest::find()
    ->select([
      UserOtherRequest::tableName().'.id',
      new Expression("'other' as item_type"),
      UserOtherRequest::tableName().'.booking_id',
      UserOtherRequest::tableName().'.user_id',
      UserOtherRequest::tableName().'.descp',
      UserOtherRequest::tableName().'.requested_date',
      UserOtherRequest::tableName().'.status',
      UserOtherRequest::tableName().'.reason',
      UserOtherRequest::tableName().'.remarks',
      UserOtherRequest::tableName().'.admin_action_date',
      UserOtherRequest::tableName().'.active_show_till',
      UserOtherRequest::tableName().'.expected_reply',
      UserOtherRequest::tableName().'.created_at',
      UserOtherRequest::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".UserOtherRequest::tableName().".user_id")
		->where(['and',
      [UserOtherRequest::tableName().'.trashed'=>0],
		]);

    $other_requests->andFilterWhere([
      UserOtherRequest::tableName().'.user_id' => $userIdz,
      UserOtherRequest::tableName().'.status' => $this->status,
    ]);
    $other_requests->andFilterWhere(['like',UserOtherRequest::tableName().'.remarks',$this->remarks])
    ->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$other_requests->andWhere(['or',['is',UserOtherRequest::tableName().'.active_show_till',NULL],['>=',UserOtherRequest::tableName().'.active_show_till',date("Y-m-d")]]);
      $other_requests->andWhere(['or',['>=',UserOtherRequest::tableName().'.requested_date',$activeShowTill],['and',[UserOtherRequest::tableName().'.status'=>0],['is',UserOtherRequest::tableName().'.requested_date',null]]]);
    }elseif($this->listType=='history'){
      $other_requests->andWhere(['or',['<',UserOtherRequest::tableName().'.requested_date',date("Y-m-d")],['and',[UserOtherRequest::tableName().'.status'=>1],['is',UserOtherRequest::tableName().'.requested_date',null]]]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $other_requests->andWhere(['and',['>=',UserOtherRequest::tableName().'.requested_date',$start_date],['<=',UserOtherRequest::tableName().'.requested_date',$end_date]]);
      }else{
        $other_requests->andWhere([UserOtherRequest::tableName().'.requested_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $other_requests->andWhere(['and',['>=','DATE('.UserOtherRequest::tableName().'.created_at)',$start_date],['<=','DATE('.UserOtherRequest::tableName().'.created_at)',$end_date]]);
      }else{
        $other_requests->andWhere(['DATE('.UserOtherRequest::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Ice Met Query
    $booking_ice_requests=Booking::find()
    ->select([
      Booking::tableName().'.id',
      new Expression("'bookingice' as item_type"),
      'booking_id'=>Booking::tableName().'.id',
      Booking::tableName().'.user_id',
      new Expression("'' as descp"),
      'requested_date'=>Booking::tableName().'.booking_date',
      'status'=>Booking::tableName().'.ice',
      new Expression("'' as reason"),
      new Expression("'' as remarks"),
      new Expression("'' as admin_action_date"),
      new Expression("'' as active_show_till"),
      new Expression("'' as expected_reply"),
      Booking::tableName().'.created_at',
      Booking::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".Booking::tableName().".user_id");

    $booking_ice_requests->andFilterWhere([
      Booking::tableName().'.user_id' => $userIdz,
      Booking::tableName().'.ice' => 1,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $booking_ice_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$booking_ice_requests->andWhere(['or',['is',Booking::tableName().'.booking_date',NULL],['>=',Booking::tableName().'.booking_date',date("Y-m-d")]]);
      $booking_ice_requests->andWhere(['>=',Booking::tableName().'.booking_date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $booking_ice_requests->andWhere(['<',Booking::tableName().'.booking_date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $booking_ice_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $booking_ice_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $booking_ice_requests->andWhere(['and',['>=','DATE('.Booking::tableName().'.created_at)',$start_date],['<=','DATE('.Booking::tableName().'.created_at)',$end_date]]);
      }else{
        $booking_ice_requests->andWhere(['DATE('.Booking::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Kids Life Jacket Met Query
    $booking_jackets_requests=Booking::find()
    ->select([
      Booking::tableName().'.id',
      new Expression("'bookingkidsjackets' as item_type"),
      'booking_id'=>Booking::tableName().'.id',
      Booking::tableName().'.user_id',
      new Expression("'' as descp"),
      'requested_date'=>Booking::tableName().'.booking_date',
      'status'=>Booking::tableName().'.kids_life_jacket',
      new Expression("'' as reason"),
      new Expression("'' as remarks"),
      new Expression("'' as admin_action_date"),
      new Expression("'' as active_show_till"),
      new Expression("'' as expected_reply"),
      Booking::tableName().'.created_at',
      Booking::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".Booking::tableName().".user_id");

    $booking_jackets_requests->andFilterWhere([
      Booking::tableName().'.user_id' => $userIdz,
      Booking::tableName().'.kids_life_jacket' => 1,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $booking_jackets_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$booking_jackets_requests->andWhere(['or',['is',Booking::tableName().'.booking_date',NULL],['>=',Booking::tableName().'.booking_date',date("Y-m-d")]]);
      $booking_jackets_requests->andWhere(['>=',Booking::tableName().'.booking_date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $booking_jackets_requests->andWhere(['<',Booking::tableName().'.booking_date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $booking_jackets_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $booking_jackets_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $booking_jackets_requests->andWhere(['and',['>=','DATE('.Booking::tableName().'.created_at)',$start_date],['<=','DATE('.Booking::tableName().'.created_at)',$end_date]]);
      }else{
        $booking_jackets_requests->andWhere(['DATE('.Booking::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Captain Waiting List Query

    $waiting_captain_requests=WaitingListCaptain::find()
    ->select([
      WaitingListCaptain::tableName().'.id',
      new Expression("'waitingcaptain' as item_type"),
      WaitingListCaptain::tableName().'.booking_id',
      WaitingListCaptain::tableName().'.user_id',
      new Expression("'' as descp"),
      'requested_date'=>WaitingListCaptain::tableName().'.date',
      WaitingListCaptain::tableName().'.status',
      WaitingListCaptain::tableName().'.reason',
      WaitingListCaptain::tableName().'.remarks',
      WaitingListCaptain::tableName().'.admin_action_date',
      WaitingListCaptain::tableName().'.active_show_till',
      WaitingListCaptain::tableName().'.expected_reply',
      WaitingListCaptain::tableName().'.created_at',
      WaitingListCaptain::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListCaptain::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListCaptain::tableName().".booking_id");

    $waiting_captain_requests->andFilterWhere([
      WaitingListCaptain::tableName().'.user_id' => $userIdz,
      WaitingListCaptain::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_captain_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_captain_requests->andWhere(['or',['is',WaitingListCaptain::tableName().'.active_show_till',NULL],['>=',WaitingListCaptain::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_captain_requests->andWhere(['>=',WaitingListCaptain::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_captain_requests->andWhere(['<',WaitingListCaptain::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_captain_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_captain_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_captain_requests->andWhere(['and',['>=','DATE('.WaitingListCaptain::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListCaptain::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_captain_requests->andWhere(['DATE('.WaitingListCaptain::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Equipment Waiting List Query
    $waiting_equipment_requests=WaitingListSportsEquipment::find()
    ->select([
      WaitingListSportsEquipment::tableName().'.id',
      new Expression("'waitingequipment' as item_type"),
      WaitingListSportsEquipment::tableName().'.booking_id',
      WaitingListSportsEquipment::tableName().'.user_id',
      new Expression("'Waiting List - Sports Equipment' as descp"),
      'requested_date'=>WaitingListSportsEquipment::tableName().'.date',
      WaitingListSportsEquipment::tableName().'.status',
      WaitingListSportsEquipment::tableName().'.reason',
      WaitingListSportsEquipment::tableName().'.remarks',
      WaitingListSportsEquipment::tableName().'.admin_action_date',
      WaitingListSportsEquipment::tableName().'.active_show_till',
      WaitingListSportsEquipment::tableName().'.expected_reply',
      WaitingListSportsEquipment::tableName().'.created_at',
      WaitingListSportsEquipment::tableName().'.created_at',
      WaitingListSportsEquipment::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListSportsEquipment::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListSportsEquipment::tableName().".booking_id");

    $waiting_equipment_requests->andFilterWhere([
      WaitingListSportsEquipment::tableName().'.user_id' => $userIdz,
      WaitingListSportsEquipment::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_equipment_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_equipment_requests->andWhere(['or',['is',WaitingListSportsEquipment::tableName().'.active_show_till',NULL],['>=',WaitingListSportsEquipment::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_equipment_requests->andWhere(['>=',WaitingListSportsEquipment::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_equipment_requests->andWhere(['<',WaitingListSportsEquipment::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_equipment_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_equipment_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_equipment_requests->andWhere(['and',['>=','DATE('.WaitingListSportsEquipment::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListSportsEquipment::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_equipment_requests->andWhere(['DATE('.WaitingListSportsEquipment::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //BBQ Waiting List Query
    $waiting_bbq_requests=WaitingListBbq::find()
    ->select([
      WaitingListBbq::tableName().'.id',
      new Expression("'waitingbbq' as item_type"),
      WaitingListBbq::tableName().'.booking_id',
      WaitingListBbq::tableName().'.user_id',
      new Expression("'Waiting List - BBQ Set' as descp"),
      'requested_date'=>WaitingListBbq::tableName().'.date',
      WaitingListBbq::tableName().'.status',
      WaitingListBbq::tableName().'.reason',
      WaitingListBbq::tableName().'.remarks',
      WaitingListBbq::tableName().'.admin_action_date',
      WaitingListBbq::tableName().'.active_show_till',
      WaitingListBbq::tableName().'.expected_reply',
      WaitingListBbq::tableName().'.created_at',
      WaitingListBbq::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListBbq::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListBbq::tableName().".booking_id");

    $waiting_bbq_requests->andFilterWhere([
      WaitingListBbq::tableName().'.user_id' => $userIdz,
      WaitingListBbq::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_bbq_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_bbq_requests->andWhere(['or',['is',WaitingListBbq::tableName().'.active_show_till',NULL],['>=',WaitingListBbq::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_bbq_requests->andWhere(['>=',WaitingListBbq::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_bbq_requests->andWhere(['<',WaitingListBbq::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_bbq_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_bbq_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_bbq_requests->andWhere(['and',['>=','DATE('.WaitingListBbq::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListBbq::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_bbq_requests->andWhere(['DATE('.WaitingListBbq::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Wake Boarding Waiting List Query
    $waiting_wake_boarding_requests=WaitingListWakeBoarding::find()
    ->select([
      WaitingListWakeBoarding::tableName().'.id',
      new Expression("'waitingwakeboarding' as item_type"),
      WaitingListWakeBoarding::tableName().'.booking_id',
      WaitingListWakeBoarding::tableName().'.user_id',
      new Expression("'Waiting List - Wake Boarding' as descp"),
      'requested_date'=>WaitingListWakeBoarding::tableName().'.date',
      WaitingListWakeBoarding::tableName().'.status',
      WaitingListWakeBoarding::tableName().'.reason',
      WaitingListWakeBoarding::tableName().'.remarks',
      WaitingListWakeBoarding::tableName().'.admin_action_date',
      WaitingListWakeBoarding::tableName().'.active_show_till',
      WaitingListWakeBoarding::tableName().'.expected_reply',
      WaitingListWakeBoarding::tableName().'.created_at',
      WaitingListWakeBoarding::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListWakeBoarding::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListWakeBoarding::tableName().".booking_id");

    $waiting_wake_boarding_requests->andFilterWhere([
      WaitingListWakeBoarding::tableName().'.user_id' => $userIdz,
      WaitingListWakeBoarding::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_wake_boarding_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_wake_boarding_requests->andWhere(['or',['is',WaitingListWakeBoarding::tableName().'.active_show_till',NULL],['>=',WaitingListWakeBoarding::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_wake_boarding_requests->andWhere(['>=',WaitingListWakeBoarding::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_wake_boarding_requests->andWhere(['<',WaitingListWakeBoarding::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_wake_boarding_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_wake_boarding_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_wake_boarding_requests->andWhere(['and',['>=','DATE('.WaitingListWakeBoarding::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListWakeBoarding::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_wake_boarding_requests->andWhere(['DATE('.WaitingListWakeBoarding::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Wake Boarding Waiting List Query
    $waiting_wake_surfing_requests=WaitingListWakeSurfing::find()
    ->select([
      WaitingListWakeSurfing::tableName().'.id',
      new Expression("'waitingwakesurfing' as item_type"),
      WaitingListWakeSurfing::tableName().'.booking_id',
      WaitingListWakeSurfing::tableName().'.user_id',
      new Expression("'Waiting List - Wake Surfing' as descp"),
      'requested_date'=>WaitingListWakeSurfing::tableName().'.date',
      WaitingListWakeSurfing::tableName().'.status',
      WaitingListWakeSurfing::tableName().'.reason',
      WaitingListWakeSurfing::tableName().'.remarks',
      WaitingListWakeSurfing::tableName().'.admin_action_date',
      WaitingListWakeSurfing::tableName().'.active_show_till',
      WaitingListWakeSurfing::tableName().'.expected_reply',
      WaitingListWakeSurfing::tableName().'.created_at',
      WaitingListWakeSurfing::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListWakeSurfing::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListWakeSurfing::tableName().".booking_id");

    $waiting_wake_surfing_requests->andFilterWhere([
      WaitingListWakeSurfing::tableName().'.user_id' => $userIdz,
      WaitingListWakeSurfing::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_wake_surfing_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_wake_surfing_requests->andWhere(['or',['is',WaitingListWakeSurfing::tableName().'.active_show_till',NULL],['>=',WaitingListWakeSurfing::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_wake_surfing_requests->andWhere(['>=',WaitingListWakeSurfing::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_wake_surfing_requests->andWhere(['<',WaitingListWakeSurfing::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_wake_surfing_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_wake_surfing_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_wake_surfing_requests->andWhere(['and',['>=','DATE('.WaitingListWakeSurfing::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListWakeSurfing::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_wake_surfing_requests->andWhere(['DATE('.WaitingListWakeSurfing::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Late Arrival Waiting List Query
    $waiting_late_arrival_requests=WaitingListLateArrival::find()
    ->select([
      WaitingListLateArrival::tableName().'.id',
      new Expression("'waitinglatearrival' as item_type"),
      WaitingListLateArrival::tableName().'.booking_id',
      WaitingListLateArrival::tableName().'.user_id',
      new Expression("'Waiting List - Late Arrival' as descp"),
      'requested_date'=>WaitingListLateArrival::tableName().'.date',
      WaitingListLateArrival::tableName().'.status',
      WaitingListLateArrival::tableName().'.reason',
      WaitingListLateArrival::tableName().'.remarks',
      WaitingListLateArrival::tableName().'.admin_action_date',
      WaitingListLateArrival::tableName().'.active_show_till',
      WaitingListLateArrival::tableName().'.expected_reply',
      WaitingListLateArrival::tableName().'.created_at',
      WaitingListLateArrival::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListLateArrival::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListLateArrival::tableName().".booking_id");

    $waiting_late_arrival_requests->andFilterWhere([
      WaitingListLateArrival::tableName().'.user_id' => $userIdz,
      WaitingListLateArrival::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_late_arrival_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_late_arrival_requests->andWhere(['or',['is',WaitingListLateArrival::tableName().'.active_show_till',NULL],['>=',WaitingListLateArrival::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_late_arrival_requests->andWhere(['>=',WaitingListLateArrival::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_late_arrival_requests->andWhere(['<',WaitingListLateArrival::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_late_arrival_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_late_arrival_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_late_arrival_requests->andWhere(['and',['>=','DATE('.WaitingListLateArrival::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListLateArrival::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_late_arrival_requests->andWhere(['DATE('.WaitingListLateArrival::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Overnight Camping Waiting List Query
    $waiting_overnight_camping_requests=WaitingListOvernightCamping::find()
    ->select([
      WaitingListOvernightCamping::tableName().'.id',
      new Expression("'waitingovernightcamping' as item_type"),
      WaitingListOvernightCamping::tableName().'.booking_id',
      WaitingListOvernightCamping::tableName().'.user_id',
      new Expression("'Waiting List - Overnight Camping' as descp"),
      'requested_date'=>WaitingListOvernightCamping::tableName().'.date',
      WaitingListOvernightCamping::tableName().'.status',
      WaitingListOvernightCamping::tableName().'.reason',
      WaitingListOvernightCamping::tableName().'.remarks',
      WaitingListOvernightCamping::tableName().'.admin_action_date',
      WaitingListOvernightCamping::tableName().'.active_show_till',
      WaitingListOvernightCamping::tableName().'.expected_reply',
      WaitingListOvernightCamping::tableName().'.created_at',
      WaitingListOvernightCamping::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListOvernightCamping::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListOvernightCamping::tableName().".booking_id");

    $waiting_overnight_camping_requests->andFilterWhere([
      WaitingListOvernightCamping::tableName().'.user_id' => $userIdz,
      WaitingListOvernightCamping::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_overnight_camping_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_overnight_camping_requests->andWhere(['or',['is',WaitingListOvernightCamping::tableName().'.active_show_till',NULL],['>=',WaitingListOvernightCamping::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_overnight_camping_requests->andWhere(['>=',WaitingListOvernightCamping::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_overnight_camping_requests->andWhere(['<',WaitingListOvernightCamping::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_overnight_camping_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_overnight_camping_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_overnight_camping_requests->andWhere(['and',['>=','DATE('.WaitingListOvernightCamping::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListOvernightCamping::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_overnight_camping_requests->andWhere(['DATE('.WaitingListOvernightCamping::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    //Early Departure Waiting List Query
    $waiting_early_departure_requests=WaitingListEarlyDeparture::find()
    ->select([
      WaitingListEarlyDeparture::tableName().'.id',
      new Expression("'waitingearlydeparture' as item_type"),
      WaitingListEarlyDeparture::tableName().'.booking_id',
      WaitingListEarlyDeparture::tableName().'.user_id',
      new Expression("'Waiting List - Early Departure' as descp"),
      'requested_date'=>WaitingListEarlyDeparture::tableName().'.date',
      WaitingListEarlyDeparture::tableName().'.status',
      WaitingListEarlyDeparture::tableName().'.reason',
      WaitingListEarlyDeparture::tableName().'.remarks',
      WaitingListEarlyDeparture::tableName().'.admin_action_date',
      WaitingListEarlyDeparture::tableName().'.active_show_till',
      WaitingListEarlyDeparture::tableName().'.expected_reply',
      WaitingListEarlyDeparture::tableName().'.created_at',
      WaitingListEarlyDeparture::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".WaitingListEarlyDeparture::tableName().".user_id")
    ->innerJoin(Booking::tableName(),Booking::tableName().".id=".WaitingListEarlyDeparture::tableName().".booking_id");

    $waiting_early_departure_requests->andFilterWhere([
      WaitingListEarlyDeparture::tableName().'.user_id' => $userIdz,
      WaitingListEarlyDeparture::tableName().'.status' => $this->status,
      Booking::tableName().'.status' => 1,
      Booking::tableName().'.trashed' => 0,
    ]);
    $waiting_early_departure_requests->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active' || $this->listType=='future'){
      //$waiting_early_departure_requests->andWhere(['or',['is',WaitingListEarlyDeparture::tableName().'.active_show_till',NULL],['>=',WaitingListEarlyDeparture::tableName().'.active_show_till',date("Y-m-d")]]);
      $waiting_early_departure_requests->andWhere(['>=',WaitingListEarlyDeparture::tableName().'.date',$activeShowTill]);
    }elseif($this->listType=='history'){
      $waiting_early_departure_requests->andWhere(['<',WaitingListEarlyDeparture::tableName().'.date',date("Y-m-d")]);
    }

    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $waiting_early_departure_requests->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $waiting_early_departure_requests->andWhere([Booking::tableName().'.booking_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $waiting_early_departure_requests->andWhere(['and',['>=','DATE('.WaitingListEarlyDeparture::tableName().'.created_at)',$start_date],['<=','DATE('.WaitingListEarlyDeparture::tableName().'.created_at)',$end_date]]);
      }else{
        $waiting_early_departure_requests->andWhere(['DATE('.WaitingListEarlyDeparture::tableName().'.created_at)'=>$this->created_at]);
      }
    }
    /*
    **********************************************************************************************
    **********************************************************************************************
    */
    if($this->status==1){
      $queryUnion = (new yii\db\Query())
        ->select('*')
        ->from([
          'user_req'=>$freeze_requests
          ->union($boat_booking_requests)
          ->union($nd_training_requests)
          ->union($upgradecity_requests)
          ->union($other_requests)
          ->union($waiting_captain_requests)
          ->union($waiting_equipment_requests)
          ->union($waiting_bbq_requests)
          ->union($waiting_wake_boarding_requests)
          ->union($waiting_wake_surfing_requests)
          ->union($booking_ice_requests)
          ->union($booking_jackets_requests)
          ->union($waiting_late_arrival_requests)
          ->union($waiting_overnight_camping_requests)
          ->union($waiting_early_departure_requests)
        ]);
    }else{
      $queryUnion = (new yii\db\Query())
        ->select('*')
        ->from([
          'user_req'=>$freeze_requests
          ->union($boat_booking_requests)
          ->union($nd_training_requests)
          ->union($upgradecity_requests)
          ->union($other_requests)
          ->union($waiting_captain_requests)
          ->union($waiting_equipment_requests)
          ->union($waiting_bbq_requests)
          ->union($waiting_wake_boarding_requests)
          ->union($waiting_wake_surfing_requests)
          ->union($waiting_late_arrival_requests)
          ->union($waiting_overnight_camping_requests)
          ->union($waiting_early_departure_requests)
        ]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $queryUnion,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort'=> [
        'defaultOrder' => [
          'requested_date'=>SORT_DESC,
        ],
        'attributes' => [
          'requested_date',
        ],
      ]
    ]);

    $queryUnion->andFilterWhere([
      'item_type' => $this->item_type,
      'expected_reply' => $this->expected_reply,
    ]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    if(Yii::$app->controller->id=='booking' && Yii::$app->controller->action->id=='all' && Yii::$app->user->identity->user_type!=0){
      return Yii::$app->helperFunctions->getAllBookingTabs($this);
    }elseif(Yii::$app->controller->id=='site' && Yii::$app->controller->action->id=='my-requests' && Yii::$app->user->identity->user_type==0){
      return Yii::$app->helperFunctions->getUserBookingTabs($this);
    }elseif(Yii::$app->controller->id=='user-requests'){
      return Yii::$app->helperFunctions->getRequestTabs($this);
    }elseif(Yii::$app->controller->id=='user' && Yii::$app->controller->action->id=='requests'){
      return Yii::$app->helperFunctions->getMemberRequestTabs($this->user_id,$this);
    }
  }
}
