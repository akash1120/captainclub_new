<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserNote;

/**
* UserNoteSearch represents the model behind the search form about `app\models\UserNote`.
*/
class UserNoteSearch extends UserNote
{
  public $listType,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'user_id', 'created_by', 'updated_by', 'trashed', 'trashed_by','pageSize'], 'integer'],
      [['listType','comments', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = UserNote::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort' => ['defaultOrder' => ['updated_at'=>SORT_DESC],]
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'user_id' => $this->user_id,
      'trashed' => 0,
    ]);

    if($this->listType!=null){
      if($this->listType=='future'){
        $query->andWhere(['>=', 'DATE(reminder)', date("Y-m-d")]);
      }
      if($this->listType=='pending'){
        $query->andWhere(['and',['<','DATE(reminder)',date("Y-m-d")],['or',['remarks'=>NULL],['=','remarks','']]]);
      }
    }

    $query->andFilterWhere(['like', 'comments', $this->comments]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    $futureCount=Yii::$app->statsFunctions->getUserFutureTaskCount($this->user_id);
    $pendingCount=Yii::$app->statsFunctions->getUserPendingTaskCount($this->user_id);
    return '
    <li class="nav-item'.($this->listType=='all' ? ' active' : '').'">
      <a class="nav-link" href="'.Url::to(['index','id'=>$this->user_id,'listType'=>'all']).'">All</a>
    </li>
    <li class="nav-item'.($this->listType=='pending' ? ' active' : '').'">
      <a class="nav-link" href="'.Url::to(['index','id'=>$this->user_id,'listType'=>'pending']).'">Pending'.($pendingCount>0 ? ' ('.$pendingCount.')' : '').'</a>
    </li>
    <li class="nav-item'.($this->listType=='future' ? ' active' : '').'">
      <a class="nav-link" href="'.Url::to(['index','id'=>$this->user_id,'listType'=>'future']).'">Future'.($futureCount>0 ? ' ('.$futureCount.')' : '').'</a>
    </li>';
  }
}
