<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppColor;

/**
* AppColorSearch represents the model behind the search form of `app\models\AppColor`.
*/
class AppColorSearch extends AppColor
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['title','code','text_color','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['title','code','text_color'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = AppColor::find()
    ->select([
      'id',
      'title',
      'code',
      'text_color',
      'status',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like','title',$this->title])
    ->andFilterWhere(['like','code',$this->code])
    ->andFilterWhere(['like','text_color',$this->text_color]);

    return $dataProvider;
  }
}
