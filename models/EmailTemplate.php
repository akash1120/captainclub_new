<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%email_template}}".
*
* @property integer $id
* @property string $title
* @property string $template_html
* @property string $template_text
*/
class EmailTemplate extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%email_template}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['title', 'template_html'], 'required'],
      [['template_html' ,'template_text'], 'string'],
      [['title'], 'string', 'max' => 32],
      [['title'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'title' => Yii::t('app', 'Title'),
      'template_html' => Yii::t('app', 'Html Template'),
      'template_text' => Yii::t('app', 'Text Template'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  public function searchReplace($string,$vals,$type=null)
  {
    foreach($vals as $key=>$val){
      if($type=='text' && $key=='{logo}'){
        $string = str_replace($key,'',$string);
      }elseif($type=='text' && $key=='{adminRemarks}'){
        $string = str_replace($key,strip_tags($val),$string);
      }elseif($type=='text' && $key=='{requestDetail}'){
        $string = str_replace($key,strip_tags($val),$string);
      }else{
        $string = str_replace($key,$val,$string);
      }
    }
    return $string;
  }

  /**
  * Mark record as deleted and hides fron list.
  * @return boolean
  */
  public function softDelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Email Template ('.$this->title.') trashed successfully');
    return true;
  }
}
