<?php

namespace app\models\import;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\web\UploadedFile;

/**
* This is the model class for table "{{%booking_activity}}".
*
* @property integer $id
* @property string $title
* @property string $code
* @property string $text_color
* @property integer $status
*/
class BookingActivity extends ActiveRecord
{
  public $booking_exp,$booking_extra;

  public $depCrew_sign,$depMem_sign;
  public $arrCrew_sign,$arrMem_sign;

  public $item_id=[],$item_condition=[],$item_comments=[];

  public $billFile,$oldBillFile;
  public $allowedbillTypes=['jpg','jpeg','png','gif','JPG','JPEG','PNG','GIF'];

  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_activity}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      //[['booking_id'],'required'],
      //[['booking_id','fuel_chargeable','booking_exp','trip_exp','member_exp','member_exp_late_hr','member_exp_late_min','charge_bbq','charge_captain','fuel_chargeable','boat_provided','hide_other','created_by','updated_by'],'integer'],
      [['damage_comments','no_charge_comment','dep_crew_sign','depCrew_sign','dep_mem_sign','depMem_sign','arr_crew_sign','arrCrew_sign','arr_mem_sign','arrMem_sign','trip_exp_comments','member_exp_comments','overall_remarks'],'string'],
      [['created_at','updated_at','fuel_cost'],'safe'],
      [['booking_id'],'unique'],
      [['dep_time','arr_time','dep_engine_hours','arr_engine_hours'],'string','max' => 20],
      [['fuel_cost_paid'],'number'],
      //[['dep_ppl','arr_ppl'],'integer','message' => 'Please enter numbers only'],
      [['dep_fuel_level','arr_fuel_level'],'string','max' => 15],
      [['item_id'],'each','rule' => ['integer']],
      [['item_condition','item_comments'],'each','rule' => ['string']],
      [['booking_extra'],'each','rule' => ['integer']],
      [['bill_image'],'file', 'extensions'=>implode(",",$this->allowedbillTypes)],
      //[['arr_engine_hours'],'checkValidEngineHours'],
      //[['hide_other'],'checkFinalForm'],
    ];
  }

  /**
  * checks booking, if swapped is same give error
  */
  public function checkValidEngineHours($attribute, $params)
  {
    if($this->arr_engine_hours<$this->dep_engine_hours){
      $this->addError($attribute,'Arrival Engine hours can not be less then departure hours');
      return false;
    }
  }

  /**
  * checks booking, if swapped is same give error
  */
  public function checkFinalForm($attribute, $params)
  {
    if($this->hide_other==1){
      if($this->dep_time==''){
        $this->addError($attribute,'Departure Time is required');
        return false;
      }
      if($this->arr_time==''){
        $this->addError($attribute,'Arrival Time is required');
        return false;
      }
      if($this->dep_engine_hours==''){
        $this->addError($attribute,'Departure Engine Hours are required');
        return false;
      }
      if($this->arr_engine_hours==''){
        $this->addError($attribute,'Arrival Engine Hours are required');
        return false;
      }
      if($this->dep_ppl==''){
        $this->addError($attribute,'Departure No. of People are required');
        return false;
      }
      if($this->arr_ppl==''){
        $this->addError($attribute,'Arrival No. of People are required');
        return false;
      }
      if($this->dep_fuel_level==''){
        $this->addError($attribute,'Departure Fuel Level is required');
        return false;
      }
      if($this->arr_fuel_level==''){
        $this->addError($attribute,'Arrival Fuel Level is required');
        return false;
      }
      if($this->fuel_chargeable==0 && $this->no_charge_comment==''){
        $this->addError($attribute,'Reason for no charge is required');
        return false;
      }
      if($this->fuel_chargeable==1 && $this->fuel_cost==''){
        $this->addError($attribute,'Fuel Cost is required');
        return false;
      }
      if($this->booking->port_id==Yii::$app->params['emp_id'] && $this->bill_image==''){
        $this->addError($attribute,'Upload Fuel Bill');
        return false;
      }
      if($this->dep_crew_sign==''){
        $this->addError($attribute,'Departure Crew Signature is required');
        return false;
      }
      if($this->dep_mem_sign==''){
        $this->addError($attribute,'Departure Member Signature is required');
        return false;
      }
      if($this->arr_crew_sign==''){
        $this->addError($attribute,'Arrival Crew Signature is required');
        return false;
      }
      if($this->arr_mem_sign==''){
        $this->addError($attribute,'Arrival Member Signature is required');
        return false;
      }
      if($this->boat_provided!=$this->booking->boat_id && $this->booking_exp==1){
        $this->addError($attribute,'Please select the reason for changing the boat!');
        return false;
      }
      if($this->trip_exp==''){
        $this->addError($attribute,'Trip Experience is required');
        return false;
      }
      if($this->trip_exp>1 && $this->trip_exp_comments==''){
        $this->addError($attribute,'Trip Experience remarks are required');
        return false;
      }
      if($this->member_exp>1 && $this->member_exp_comments==''){
        $this->addError($attribute,'Trip Experience remarks are required');
        return false;
      }
      if($this->member_exp>1 && $this->member_exp_late_hr=='' && $this->member_exp_late_min==''){
        $this->addError($attribute,'Please specify the duration of late arrival');
        return false;
      }
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'booking_id' => Yii::t('app', 'Booking ID'),
      'dep_time' => Yii::t('app', 'Time'),
      'arr_time' => Yii::t('app', 'Time'),
      'dep_engine_hours' => Yii::t('app', 'Engine Hours'),
      'arr_engine_hours' => Yii::t('app', 'Engine Hours'),
      'dep_ppl' => Yii::t('app', 'No. of People'),
      'arr_ppl' => Yii::t('app', 'No. of People'),
      'dep_fuel_level' => Yii::t('app', 'Fuel Level'),
      'arr_fuel_level' => Yii::t('app', 'Fuel Level'),
      'fuel_cost' => Yii::t('app', 'Fuel Cost'),
      'fuel_cost_paid' => Yii::t('app', 'Fuel Cost Paid'),
      'bill_image' => Yii::t('app', 'Bill Photo'),
      'damage_comments' => Yii::t('app', 'Damage Comments'),
      'dep_crew_sign' => Yii::t('app', 'Signature'),
      'dep_mem_sign' => Yii::t('app', 'Signature'),
      'arr_crew_sign' => Yii::t('app', 'Signature'),
      'arr_mem_sign' => Yii::t('app', 'Signature'),
      'hide_other' => Yii::t('app', 'Is This Form Final?'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'charge_bbq' => Yii::t('app', 'Charge BBQ'),
      'charge_captain' => Yii::t('app', 'Charge Captain'),
      'fuel_chargeable' => Yii::t('app', 'Charge Fuel'),
      'no_charge_comment' => Yii::t('app', 'Please specify the reason for no charge'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBoatProvided()
  {
    return $this->hasOne(Boat::className(), ['id' => 'boat_provided']);
  }

  public function sendFuelConsumptionAlert()
  {
    $eaType='Fuel';
    $bookingId=$this->booking_id;
    $eNotification=BookingActivityEmailNotification::find()->where(['booking_id'=>$bookingId,'email_type'=>$eaType]);
    if(!$eNotification->exists()){
      $connection = \Yii::$app->db;
      $emailNotification=new BookingActivityEmailNotification;
      $emailNotification->booking_id=$bookingId;
      $emailNotification->email_type=$eaType;
      $emailNotification->email_to=$this->booking->member->email;
      $emailNotification->save();

      $isValidEmail=true;
      //Check if user is not black listed
      $isRed=BlackListedEmail::find()->select(['email'])->where(['email'=>$this->booking->member->email,'trashed'=>0])->asArray()->one();
      if($isRed!=null){
        $connection->createCommand(
          "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
          [':remarks'=>'Blacklisted',':booking_id'=>$bookingId,':email_type'=>$eaType]
        )->execute();
        $isValidEmail=false;
      }
      //Check if user is not in dropped list
      $isRed=DroppedEmail::find()->select(['email'])->where(['email'=>$this->booking->member->email,'trashed'=>0])->asArray()->one();
      if($isRed!=null){
        $connection->createCommand(
          "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
          [':remarks'=>'Dropped',':booking_id'=>$bookingId,':email_type'=>$eaType]
        )->execute();
        $isValidEmail=false;
      }
      if($isValidEmail==true){
        //Petrol Consumption
        $templateId=Yii::$app->appHelperFunctions->getSetting('opPetrolConsumtion');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          $fuelBill = '';
          $folder = Yii::$app->params['fuelbill_abs_path'];
          if($this->bill_image!=null && file_exists($folder.$this->bill_image)){
            $fuelBill=$folder.$this->bill_image;
          }

          $message = Yii::$app->mailer
          ->compose(['html' => 'petrolConsumption-html', 'text' => 'petrolConsumption-text'], ['model' => $this, 'template' => $template, 'fuelBill' => $fuelBill])
          ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
          ->setTo($this->booking->member->email)
          ->setSubject('Petrol Consumption');

          if($fuelBill!=null && $fuelBill!=''){
            $message->attach($fuelBill);
          }

          $headers = $message->getSwiftMessage()->getHeaders();
          $headers->addTextHeader('X-Mailgun-Variables', json_encode(['mail_type'=>'newsletter','campaign_id'=>$this->booking_id]));

          if($message->send()){
            $connection->createCommand(
              "update ".BookingActivityEmailNotification::tableName()." set status=1,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
              [':remarks'=>'Sent',':booking_id'=>$bookingId,':email_type'=>$eaType]
            )->execute();
          }else{
            $connection->createCommand(
              "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
              [':remarks'=>'Error while sending',':booking_id'=>$bookingId,':email_type'=>$eaType]
            )->execute();
          }
        }
      }
    }
  }

  public function sendLessPeopleArrivalAlert()
  {
    $eaType='lateArrival';
    $bookingId=$this->booking_id;
    $eNotification=BookingActivityEmailNotification::find()->where(['booking_id'=>$bookingId,'email_type'=>$eaType]);
    if(!$eNotification->exists()){
      $connection = \Yii::$app->db;
      $emailNotification=new BookingActivityEmailNotification;
      $emailNotification->booking_id=$bookingId;
      $emailNotification->email_type=$eaType;
      $emailNotification->email_to=$this->booking->member->email;
      $emailNotification->save();

      $isValidEmail=true;
      //Check if user is not black listed
      $isRed=BlackListedEmail::find()->select(['email'])->where(['email'=>$this->booking->member->email,'trashed'=>0])->asArray()->one();
      if($isRed!=null){
        $connection->createCommand(
          "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
          [':remarks'=>'Blacklisted',':booking_id'=>$bookingId,':email_type'=>$eaType]
        )->execute();
        $isValidEmail=false;
      }
      //Check if user is not in dropped list
      $isRed=DroppedEmail::find()->select(['email'])->where(['email'=>$this->booking->member->email,'trashed'=>0])->asArray()->one();
      if($isRed!=null){
        $connection->createCommand(
          "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
          [':remarks'=>'Dropped',':booking_id'=>$bookingId,':email_type'=>$eaType]
        )->execute();
        $isValidEmail=false;
      }
      if($isValidEmail==true){
        $message = Yii::$app->mailer
          ->compose(['html' => 'lessPeopleArrivalAlert-html', 'text' => 'lessPeopleArrivalAlert-text'], ['model' => $this, 'booking'=>$this->booking])
          ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
          ->setTo([
            Yii::$app->params['mdEmail'],
            Yii::$app->params['careNEmail'],
            Yii::$app->params['samerEmail'],
            Yii::$app->params['ahmedEmail'],
          ])
          ->setSubject('Arriving people are less then Departure');
        if($message->send()){
          $connection->createCommand(
            "update ".BookingActivityEmailNotification::tableName()." set status=1,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
            [':remarks'=>'Sent',':booking_id'=>$bookingId,':email_type'=>$eaType]
          )->execute();
        }else{
          $connection->createCommand(
            "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
            [':remarks'=>'Error while sending',':booking_id'=>$bookingId,':email_type'=>$eaType]
          )->execute();
        }
      }
    }
  }

  //If trip Experience is not smooth or late arrival
  public function sendActivityAlert()
  {
    $eaType='flagged';
    $bookingId=$this->booking_id;

    $flagged=false;
    $flagType='';
    $flagMessage='';
    if($this->trip_exp!=1){
      $flagged=true;
      $tripExperiences=Yii::$app->helperFunctions->tripExperience;
      $flagType='<strong>'.$tripExperiences[$this->trip_exp].'</strong>'.($this->trip_exp_comments!='' ? '<br /><strong>Remarks:</strong> '.nl2br($this->trip_exp_comments) : '');
    }

    if($this->member_exp==1){
      $flagged=true;
      $flagType=($flagType!='' ? $flagType.'<br /><br />' : '');
      $flagType.='<strong>Late Arrival</strong> ('.($this->member_exp_late_hr>0 ? $this->member_exp_late_hr.' hr'.($this->member_exp_late_hr>1 ? 's' : '').' & ' : '').$this->member_exp_late_min.' min'.($this->member_exp_late_min>1 ? 's' : '').')<br /><strong>Remarks:</strong> '.$this->member_exp_comments;
    }

    if($flagged==true){
      $eNotification=BookingActivityEmailNotification::find()->where(['booking_id'=>$bookingId,'email_type'=>$eaType]);
      if(!$eNotification->exists()){
        $connection = \Yii::$app->db;
        $emailNotification=new BookingActivityEmailNotification;
        $emailNotification->booking_id=$bookingId;
        $emailNotification->email_type=$eaType;
        $emailNotification->email_to=$this->booking->member->email;
        $emailNotification->save();

        $isValidEmail=true;
        //Check if user is not black listed
        $isRed=BlackListedEmail::find()->select(['email'])->where(['email'=>$this->booking->member->email,'trashed'=>0])->asArray()->one();
        if($isRed!=null){
          $connection->createCommand(
            "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
            [':remarks'=>'Blacklisted',':booking_id'=>$bookingId,':email_type'=>$eaType]
          )->execute();
          $isValidEmail=false;
        }
        //Check if user is not in dropped list
        $isRed=DroppedEmail::find()->select(['email'])->where(['email'=>$this->booking->member->email,'trashed'=>0])->asArray()->one();
        if($isRed!=null){
          $connection->createCommand(
            "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
            [':remarks'=>'Dropped',':booking_id'=>$bookingId,':email_type'=>$eaType]
          )->execute();
          $isValidEmail=false;
        }
        if($isValidEmail==true){
          $message = Yii::$app->mailer
            ->compose(['html' => 'bookinInOutAlert-html', 'text' => 'bookinInOutAlert-text'], ['model' => $this, 'booking'=>$this->booking, 'flagType'=>$flagType])
            ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
            ->setTo([
              'samer@thecaptainsclub.ae',
              'caren@thecaptainsclub.ae',
              'md@thecaptainsclub.ae',
              'ahmed@thecaptainsclub.ae'
            ])
            ->setSubject('Booking Activity Alert - '.Yii::$app->params['siteName']);
          if($message->send()){
            $connection->createCommand(
              "update ".BookingActivityEmailNotification::tableName()." set status=1,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
              [':remarks'=>'Sent',':booking_id'=>$bookingId,':email_type'=>$eaType]
            )->execute();
          }else{
            $connection->createCommand(
              "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
              [':remarks'=>'Error while sending',':booking_id'=>$bookingId,':email_type'=>$eaType]
            )->execute();
          }
        }
      }
    }
  }

  public function getClaimAmount()
  {
    $profit=0;
    $marinaProfit=Marina::find()->select(['fuel_profit'])->where(['id'=>$this->booking->port_id])->asArray()->one();
    if($marinaProfit!=null){
      $profit=$marinaProfit['fuel_profit'];
    }
    return $this->fuel_cost-$profit;
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBookingInOutDamages()
  {
    return $this->hasMany(BookingActivityDamages::className(), ['booking_id' => 'id']);
  }

  /* Getter for damaged items*/
  public function getBookingInOutDamagedItems()
  {
    $damages='';
    if($this->bookingInOutDamages!=null){
      $n=1;
      foreach($this->bookingInOutDamages as $damagedItem){
        if($damages!='')$damages.='<br />';
        $damages.='<strong>'.$n.'.</strong>'.$damagedItem->item->title.' ('.$damagedItem->item_condition.' - '.$damagedItem->item_comments.')';
        $n++;
      }
    }
    return $damages;
  }
}
