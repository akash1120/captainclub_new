<?php

namespace app\models\import;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
* User model
*
* @property integer $id
* @property integer $user_type
* @property integer $permission_group_id
* @property integer $active_contract_id
* @property integer $active_package_id
* @property string $username
* @property string $firstname
* @property string $lastname
* @property string $email
* @property string $auth_key
* @property string $password_hash
* @property string $image
* @property integer $status
* @property string $start_date
* @property string $end_date
*/
class User extends ActiveRecord implements IdentityInterface
{
  public $file,$oldfile;
  public $licensefile,$oldlicensefile;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  public $new_password,$mobile,$city_id,$update_note,$is_licensed,$security_deposit,$deposit_type;
  public $marina_permits;
  public $license_image,$license_expiry;

  const STATUS_ACTIVE = '1';
  const STATUS_HOLD = '2';
  const STATUS_CANCEL = '3';
  const STATUS_PENDIND = '20';
  const STATUS_DELETED = '0';

  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user}}';
  }

  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
    ];
  }


  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_type' => Yii::t('app', 'Member Type'),
      'permission_group_id' => Yii::t('app', 'Permission Group'),
      'active_contract_id' => Yii::t('app', 'Active Contract'),
      'active_package_id' => Yii::t('app', 'Active Package'),
      'username' => Yii::t('app', 'Username'),
      'firstname' => Yii::t('app', 'First Name'),
      'lastname' => Yii::t('app', 'Last Name'),
      'email' => Yii::t('app', 'Email'),
      'active_package_id' => Yii::t('app', 'Package'),
      'permission_group_id' => Yii::t('app', 'Permission'),
      'permission_group' => Yii::t('app', 'Permission'),
      'city_name' => Yii::t('app', 'City'),
      'city_id' => Yii::t('app', 'City'),
      'image' => Yii::t('app', 'Photo'),
      'security_deposit' => Yii::t('app', 'Deposit'),
      'is_licensed' => Yii::t('app', 'Licensed'),
      'license_image' => Yii::t('app', 'License Image'),
      'license_expiry' => Yii::t('app', 'License Expiry'),
      'marina_permits' => Yii::t('app', 'Night Drive Permits'),
      'package_name' => Yii::t('app', 'Package'),
      'credit_balance' => Yii::t('app', 'Balance'),
      'end_date' => Yii::t('app', 'Expiry'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      ['username','unique'],
      ['email','email'],
      ['email','unique'],
      [['firstname','lastname','new_password','update_note','deposit_type'],'string'],
      [['username', 'firstname','lastname','email','mobile'], 'trim'],
      [['city_id','is_licensed'], 'integer'],
      [['security_deposit'], 'number'],
      [['note', 'group_id', 'password', 'subUser','start_date','end_date','license_expiry'],'safe'],
      [['marina_permits'], 'each', 'rule' => ['integer']],
      [['image','license_image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB']
    ];
  }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => 1, 'trashed'=>0]);
    }

  /**
  * {@inheritdoc}
  */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    return static::findOne(['auth_key' => $token, 'status' => 1, 'trashed' => 0]);
  }

  /**
  * Finds user by username
  *
  * @param string $username
  * @return static|null
  */
  public static function findByUsername($username)
  {
    return static::find()->where(['and',['or',['username'=>$username],['email'=>$username]],['trashed'=>0]])->one();
  }

  /**
  * {@inheritdoc}
  */
  public function getId()
  {
      return $this->getPrimaryKey();
  }

  /**
  * {@inheritdoc}
  */
  public function getAuthKey()
  {
    return $this->auth_key;
  }

  /**
  * {@inheritdoc}
  */
  public function validateAuthKey($authKey)
  {
    return $this->auth_key === $authKey;
  }

  /**
  * Validates password
  *
  * @param string $password password to validate
  * @return bool if password provided is valid for current user
  */
  public function validatePassword($password)
  {
    return Yii::$app->security->validatePassword($password, $this->password_hash);
  }

  /**
   * Generates password hash from password and sets it to the model
   *
   * @param string $password
   */
  public function setPassword($password)
  {
    $this->password_hash = Yii::$app->security->generatePasswordHash($password);
  }

  /**
   * Generates "remember me" authentication key
   */
  public function generateAuthKey()
  {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  /**
   * Generates "login from backend" authentication key
   */
  public function generateVerifyToken()
  {
    $this->verify_token = Yii::$app->security->generateRandomString();
  }

  /**
   * Generates new password reset token
   */
  public function generatePasswordResetToken()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".UserReset::tableName()." set status=:status where user_id=:user_id",
      [
        ':status' => 10,
        ':user_id' => $this->id,
      ]
    )
    ->execute();

    $newResetInfo = new UserReset;
    $newResetInfo->user_id = $this->id;
    $newResetInfo->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    $newResetInfo->status = 0;
    $newResetInfo->save();
  }

  public function getPasswordResetToken()
  {
    $token = '';
    $resetInfo=UserReset::find()->where(['user_id'=>$this->id])->orderBy(['created_at'=>SORT_DESC])->one();
    if($resetInfo!=null){
      $token = $resetInfo->password_reset_token;
    }
    return $token;
  }

  /**
   * Finds user by password reset token
   *
   * @param string $token password reset token
   * @return static|null
   */
  public static function findByPasswordResetToken($token)
  {
    if (!static::isPasswordResetTokenValid($token)) {
      return null;
    }
    return UserReset::find()
    ->innerJoin("user",User::tableName().".id=".UserReset::tableName().".user_id")
    ->where([
      UserReset::tableName().'.password_reset_token' => $token,
      UserReset::tableName().'.status' => 0,
      User::tableName().'.status' => 1,
    ])->one();
  }

  /**
   * Finds out if password reset token is valid
   *
   * @param string $token password reset token
   * @return boolean
   */
  public static function isPasswordResetTokenValid($token)
  {
    if (empty($token) || $token==null) {
      return false;
    }
    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int) end($parts);
    return $timestamp + $expire >= time();
  }

  /**
   * return full name
   */
  public function getName()
  {
    $name = $this->firstname.($this->lastname!='' ? ' '.$this->lastname : '');
  	return (trim($name)=='' ? $this->username : $name);
  }

  /**
   * return full name
   */
  public function getFullName()
  {
  	return $this->name;
  }

  /**
   * return full name
   */
  public function getFullnameWithUsername()
  {
  	return $this->username.($this->firstname!='' ? ' - '.$this->firstname : '').($this->lastname!='' ? ' '.$this->lastname : '');
  }

  public function getPackageWithExpiry()
  {
    $html='';
    $activeContract=$this->activeContract;
    if($activeContract!=null){
      $html='<small>'.$activeContract->package->name.'</small><br /><span class="badge badge-warning">'.Yii::$app->formatter->asDate($activeContract->end_date).'</span>';
    }
    $credits=$this->profileInfo!=null ? $this->profileInfo->credit_balance : 0;
    $html.=($html!='' ? '<br />' : '').'<span class="badge badge-'.($credits<0 ? 'danger' : 'success').'">'.$credits.'</span>';
    return $html;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProfileInfo()
  {
	   return $this->hasOne(UserProfileInfo::className(), ['user_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getResetInfo()
  {
	   return $this->hasOne(UserReset::className(), ['user_id' => 'id'])->where(['status'=>0]);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCity()
  {
	   return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getActiveContract()
  {
	   return $this->hasOne(Contract::className(), ['id' => 'active_contract_id']);
  }

  public function getActiveContractMembers()
  {
    $subQueryContractUser=ContractMember::find()->select(['user_id'])->where(['contract_id'=>$this->active_contract_id]);
    return User::find()->where(['id'=>$subQueryContractUser,'status'=>1,'trashed'=>0])->all();
  }

  public function getMemberIdz()
  {
    $idz=[];
    $activeContractMembers=$this->activeContractMembers;
    if($activeContractMembers!=null){
      foreach($activeContractMembers as $key=>$val){
        $idz[]=$val['id'];
      }
    }
    return $idz;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getRenewedContract()
  {
    if($this->activeContract->renewed_contract_id>0){
      return Contract::find()->where(['id'=>$this->activeContract->renewed_contract_id])->one();
    }
    return null;

  }

  public function getMaxPackageEndDate()
  {
    return $this->end_date;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getActivePackage()
  {
	   return $this->hasOne(Package::className(), ['id' => 'active_package_id']);
  }

  public function getLastLoginInfo()
  {
    $history=UserLoginHistory::find()
      ->select(['created_at'])
      ->where(['user_id'=>$this->id,'login'=>1])
      ->orderBy(['created_at'=>SORT_DESC])
      ->offset(1)
      ->asArray()->one();
    if($history!=null){
      return 'on '.Yii::$app->formatter->asDate($history['created_at']);
    }else{
      return ' first time';
    }
  }

  public function getCanBookBoat()
  {
    $can=true;
    if($this->user_type==0){
      $fuelCredit = $this->profileInfo->credit_balance;
      $minBalReq=Yii::$app->appHelperFunctions->getSetting('booking_min_balance');

      $daysRemaining=Yii::$app->helperFunctions->getNumberOfDaysBetween(date("Y-m-d"),$this->maxPackageEndDate);
      if($daysRemaining>Yii::$app->params['ExpiryDaysToBookInNegative']){
        $minBalReq=Yii::$app->params['ExpiryDaysToBookInNegativeBalance'];
      }
      if($fuelCredit<$minBalReq){
        $can=false;
      }
    }
    return $can;
  }

	public function getAllowedMarinaPermits()
	{
		$allowed=[];
    $userPermits=UserNightPermit::find()->select(['port_id'])->where(['user_id'=>$this->id]);
		$permits=Marina::find()
			->select([
				'id',
				'name',
			])
			->where(['id'=>$userPermits])
			->asArray()->all();
		if($permits!=null){
			foreach($permits as $permit){
				$allowed[$permit['id']]=$permit['name'];
			}
		}
		return implode(", ",$allowed);
	}

  /**
   * @return boolean
   */
  public function getPrivilege($keyword)
  {
  	$permission=false;
  	$services=ArrayHelper::map(PackageOption::find()->all(),'keyword','id');
  	$result=PackageAllowedOptions::find()->where(['package_id'=>$this->activeContract->package_id,'service_id'=>$services[$keyword]])->one();
		if($result!=null){
			$permission=true;
		}

    return $permission;
  }

  /**
   * @inheritdoc
   * Generates auth_key if it is a new user
   */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      if ($this->isNewRecord) {
        $this->generateAuthKey();
      }
      return true;
    }
    return false;
  }

  public function getSavedCards()
  {
    $ccArr=[];
    $results=UserSavedCc::find()->select(['id','card_brand','last_4_digits'])->where(['user_id'=>$this->id,'trashed'=>0])->AsArray()->all();
    if($results!=null){
      foreach ($results as $result) {
        $ccArr[$result['id']]=$result['card_brand'].' ending '.$result['last_4_digits'];
      }
    }
    return $ccArr;
  }

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.($this->user_type==1 ? 'Staff Member' : 'User').' ('.$this->fullname.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

  /**
   * Mark record as deleted and hides from list.
   * @return boolean
   */
  public function Softdelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed' => 1,
        ':trashed_at' => date("Y-m-d H:i:s"),
        ':trashed_by' => Yii::$app->user->identity->id,
        ':id' => $this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.($this->user_type==1 ? 'Staff Member' : 'User').' ('.$this->fullname.') trashed successfully');
		return true;
  }

  /*
  * Check if there is any hold entry
  */
  public function getPackageHoldDate()
	{
		$userInHold=UserStatusReason::find()->where(['and',['user_id'=>$this->id,'status'=>2],['>=','date',date("Y-m-d")]])->one();
		if($userInHold!=null && $userInHold->date!=null && $userInHold->date!=''){
			return $userInHold->date;
		}
	}

  /*
  * Check if there is any cancel entry
  */
	public function getPackageCancelDate()
	{
		$userInCancel=UserStatusReason::find()->where(['and',['user_id'=>$this->id,'status'=>3],['>=','date',date("Y-m-d")]])->one();
		if($userInCancel!=null && $userInCancel->date!=null && $userInCancel->date!=''){
			return $userInCancel->date;
		}
	}

  /**
   * @return \yii\db\ActiveQuery
   */
	public function getFreezeRequest()
	{
		return UserFreezeRequest::find()->where([
      'and',['status'=>1,'trashed'=>0],['in','created_by',$this->memberIdz]
    ])->orderBy(['id'=>SORT_DESC])->one();
	}

  public function getActiveFreezePeriod()
  {
    return UserFreezeRequest::find()
    ->where(['and',['user_id'=>$this->memberIdz,'status'=>1,'trashed'=>0],['<=','start_date',date("Y-m-d")],['>=','end_date',date("Y-m-d")]])
    ->asArray()->one();
  }

  /**
   * @return \yii\db\ActiveQuery
   */
	public function getFreezeStartDate()
	{
		$freezeRequest=$this->freezeRequest;
		if($freezeRequest!=null){
			return $freezeRequest->start_date;
		}
	}

  /**
   * @return \yii\db\ActiveQuery
   */
	public function getFreezeEndDate()
	{
		$freezeRequest=$this->freezeRequest;
		if($freezeRequest!=null){
			return $freezeRequest->end_date;
		}
	}
}
