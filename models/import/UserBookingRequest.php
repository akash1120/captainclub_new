<?php

namespace app\models\import;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%user_booking_request}}".
*
* @property integer $id
* @property string $user_id
* @property integer $city_id
* @property string $marina
* @property string $time_slot
* @property string $descp
* @property string $requested_date
*/
class UserBookingRequest extends ActiveRecord
{
  public $sendEmailAlert;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_booking_request}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      //[['user_id','city_id','marina','time_slot','requested_date','descp'],'required'],
      [['user_id','city_id','status','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['marina','time_slot','descp','requested_date','remarks','email_message','admin_action_date','active_show_till','sendEmailAlert'],'string'],
      ['status','default','value'=>0],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'Member'),
      'city_id' => Yii::t('app', 'City'),
      'marina' => Yii::t('app', 'Marina'),
      'time_slot' => Yii::t('app', 'Time'),
      'requested_date' => Yii::t('app', 'Date'),
      'descp' => Yii::t('app', 'Specify your request'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCity()
  {
    return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMember()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
  * @return string, name of user
  */
  public function getMemberName()
  {
    if($this->member!=null){
      return $this->member->fullname;
    }
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      /*if($this->sendEmailAlert!='no'){
        Yii::$app->mailer->compose(['html' => 'requestBoatBooking-html', 'text' => 'requestBoatBooking-text'], ['model' => $this])
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
        ->setReplyTo($this->member->email)
        ->setTo(Yii::$app->appHelperFunctions->getSetting('requestEmail'))
        ->setSubject('New Request Type Boat Booking - ' . Yii::$app->params['siteName'])
        ->send();

        $templateId=Yii::$app->appHelperFunctions->getSetting('general_response');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          $vals = [
            '{captainName}' => $this->member->firstname,
            '{emailMessage}' => 'Your boat booking request is under process.',
          ];
          $htmlBody=$template->searchReplace($template->template_html,$vals);
          $textBody=$template->searchReplace($template->template_text,$vals);
          $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
          ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
          ->setSubject('Request Sent - ' . Yii::$app->params['siteName'])
          ->setTo($this->member->email)
          ->send();
        }
      }*/
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /**
  * Mark record as deleted and hides fron list.
  * @return boolean
  */
  public function softDelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Member Boat Booking Request ('.$this->member->fullname.') trashed successfully');
    return true;
  }

  /**
  * @return ports array
  */
  public function getPorts()
  {
    $ports=[];
    $results=Ports::find()->select(['id'=>'ports.id', 'marina'=>'ports.name', 'cityName'=>'cities.name','city_id'=>'ports.city_id'])->joinWith(['city'])->andWhere(['city_id'=>Yii::$app->params['cityIdz']['onlyabudhabi']])->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $ports[$result['cityName'].' - '.$result['marina']]=$result['marina'];//$result['cityName'].' - '.
      }
    }
    $ports['Any Marina']=Yii::t('app','Any Marina');
    return $ports;
  }

  /**
  * @return array
  */
  public function getTimeZones()
  {
    $times=[];
    $times['Morning']=Yii::t('app','Morning');
    $times['Afternoon']=Yii::t('app','Afternoon');
    $times['Anytime (morning or afternoon)']=Yii::t('app','Anytime (morning or afternoon)');
    $times['Night Drive']=Yii::t('app','Night Drive');
    $times['Night Cruise']=Yii::t('app','Night Cruise');
    $times['Marina Stay in']=Yii::t('app','Marina Stay in');
    $times['Drop Off']=Yii::t('app','Drop Off');
    return $times;
  }
}
