<?php

namespace app\models\import;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%user_city_upgrade_request}}".
*
* @property integer $id
* @property integer $user_id
* @property string $descp
* @property string $requested_date
* @property integer $status
*/
class UserCityUpgradeRequest extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_city_upgrade_request}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      //[['user_id','descp'],'required'],
      [['user_id','status','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['descp','requested_date','remarks','email_message','admin_action_date','active_show_till'],'string'],
      ['status','default','value'=>0],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'Member'),
      'requested_date' => Yii::t('app', 'If your request is for a Specific date, specify it please'),
      'descp' => Yii::t('app', 'please type your request below and we will get back to you soon'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMember()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return string, name of user
   */
  public function getMemberName()
  {
    if($this->member!=null){
	   return $this->member->fullname;
    }
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      /*Yii::$app->mailer->compose(['html' => 'requestUpgrade-html', 'text' => 'requestUpgrade-text'], ['model' => $this])
      ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
      ->setReplyTo($this->member->email)
      ->setTo(Yii::$app->params['samerEmail'])
      ->setCc([Yii::$app->params['mdEmail'],Yii::$app->params['careNEmail']])
      ->setSubject('New Request Type Upgrade City Access - ' . Yii::$app->params['siteName'])
      ->send();*/
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /**
  * Mark record as deleted and hides fron list.
  * @return boolean
  */
  public function softDelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
      )
      ->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Member Other Request ('.$this->member->fullname.') trashed successfully');
      return true;
    }
  }
