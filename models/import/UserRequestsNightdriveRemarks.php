<?php

namespace app\models\import;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%user_requests_nightdrive_remarks}}".
*
* @property integer $id
* @property integer $user_request_id
* @property string $training_date
* @property integer $marina_id
* @property string $remarks
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class UserRequestsNightdriveRemarks extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_requests_nightdrive_remarks}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      //[['user_request_id','training_date','marina_id','remarks'], 'required'],
      [['user_request_id','marina_id','created_by','updated_by'], 'integer'],
      [['training_date','remarks'], 'string'],
      [['created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_request_id' => Yii::t('app', 'Reques ID'),
      'training_date' => Yii::t('app', 'Date'),
      'marina_id' => Yii::t('app', 'Marina'),
      'remarks' => Yii::t('app', 'Remarks'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMarina()
  {
      return $this->hasOne(Marina::className(), ['id' => 'marina_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getRequest()
  {
      return $this->hasOne(UserNightdriveTrainingRequest::className(), ['id' => 'user_request_id']);
  }
}
