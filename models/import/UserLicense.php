<?php

namespace app\models\import;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\web\UploadedFile;

/**
* This is the model class for table "{{%user_license}}".
*
* @property integer $id
* @property integer $user_id
* @property integer $city_id
* @property string $license_image
* @property string $license_start
* @property string $license_expiry
*/
class UserLicense extends ActiveRecord
{
  public $imageFile,$oldImage;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_license}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      //[['user_id','city_id','license_start','license_expiry'],'required'],
      [['user_id','city_id','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['license_start','license_expiry'],'string'],
      //[['license_expiry'],'checkStartEnd'],
      //[['license_expiry'],'checkDuplicate'],
      [['license_image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB']
    ];
  }

  /**
  * Validates the if its overlaping or duplicate.
  * @param string $attribute the attribute currently being validated
  * @param array $params the additional name-value pairs given in the rule
  */
  public function checkStartEnd($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->license_start!=null && $this->license_start>$this->license_expiry){
        $this->addError($attribute, 'Please provide valid start and expiry dates');
      }
    }
  }

  /**
  * Validates the if its overlaping or duplicate.
  * @param string $attribute the attribute currently being validated
  * @param array $params the additional name-value pairs given in the rule
  */
  public function checkDuplicate($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $already=self::find()
      ->where(['city_id'=>$this->city_id,'license_expiry'=>$this->license_expiry,'trashed'=>0])
      ->andFilterWhere(['!=','id',$this->id]);
      if($already->exists()){
        $this->addError($attribute, 'Record already exists.');
      }
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'Member'),
      'city_id' => Yii::t('app', 'City'),
      'license_image' => Yii::t('app', 'License Photo'),
      'license_start' => Yii::t('app', 'License Start'),
      'license_expiry' => Yii::t('app', 'License Expiry'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMember()
  {
	   return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'license_image')){
      $this->imageFile = UploadedFile::getInstance($this, 'license_image');
      // if no file was uploaded abort the upload
      if (!empty($this->imageFile)) {
        $pInfo=pathinfo($this->imageFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->imageFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('license_image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->imageFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->license_image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->license_image==null && $this->oldImage!=null){
      $this->license_image=$this->oldImage;
    }
    return parent::beforeValidate();
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if ($this->imageFile!== null && $this->license_image!=null) {
      if($this->oldImage!=null && $this->license_image!=$this->oldImage && file_exists(Yii::$app->params['member_license_uploads_abs_path'].$this->oldImage)){
        unlink(Yii::$app->params['member_license_uploads_abs_path'].$this->oldImage);
      }
      $this->imageFile->saveAs(Yii::$app->params['member_license_uploads_abs_path'].$this->license_image);
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'License record ('.$this->member->fullname.') trashed successfully');
		return true;
	}
}
