<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%contract_member}}".
*
* @property integer $id
* @property integer $contract_id
* @property integer $user_id
* @property integer $status
*/
class ContractMember extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%contract_member}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
        [['contract_id', 'user_id'], 'required'],
        [['contract_id', 'user_id', 'status'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'contract_id' => Yii::t('app', 'Contract'),
      'user_id' => Yii::t('app', 'Member'),
      'status' => Yii::t('app', 'Status'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getContract()
  {
      return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMember()
  {
      return $this->hasOne(User::className(), ['id' => 'user_id']);
  }
}
