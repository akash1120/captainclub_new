<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Marina;

/**
* MarinaSearch represents the model behind the search form about `app\models\Marina`.
*/
class MarinaSearch extends Marina
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','city_id','captains','bbq','no_of_early_departures','no_of_late_arrivals','no_of_overnight_camps','night_drive_limit','wake_boarding_limit','wake_surfing_limit','status','pageSize'],'integer'],
      [['name','short_name','fuel_profit'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = Marina::find()
    ->select([
      Marina::tableName().'.id',
      'city_id',
      'city_name'=>City::tableName().'.name',
      Marina::tableName().'.name',
      'short_name',
      'captains',
      'bbq',
      'no_of_early_departures',
      'no_of_overnight_camps',
      'no_of_late_arrivals',
      'fuel_profit',
      'night_drive_limit',
      'wake_boarding_limit',
      'wake_surfing_limit',
      Marina::tableName().'.status',
    ])
    ->innerJoin(City::tableName(),City::tableName().".id=".Marina::tableName().".city_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $query->andFilterWhere([
      Marina::tableName().'.id' => $this->id,
      'city_id' => $this->city_id,
      'captains' => $this->captains,
      'bbq' => $this->bbq,
      'fuel_profit' => $this->fuel_profit,
      Marina::tableName().'.no_of_early_departures' => $this->no_of_early_departures,
      Marina::tableName().'.no_of_overnight_camps' => $this->no_of_overnight_camps,
      Marina::tableName().'.no_of_late_arrivals' => $this->no_of_late_arrivals,
      Marina::tableName().'.night_drive_limit' => $this->night_drive_limit,
      Marina::tableName().'.wake_boarding_limit' => $this->wake_boarding_limit,
      Marina::tableName().'.wake_surfing_limit' => $this->wake_surfing_limit,
      Marina::tableName().'.status' => $this->status,
      Marina::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['like',Marina::tableName().'.name',$this->name])
    ->andFilterWhere(['like','short_name',$this->short_name]);

    return $dataProvider;
  }
}
