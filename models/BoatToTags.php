<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%boat_to_tags}}".
*
* @property integer $boat_id
* @property integer $tag_id
*/
class BoatToTags extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_to_tags}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id','tag_id'], 'required'],
      [['boat_id','tag_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_id' => Yii::t('app', 'Boat'),
      'tag_id' => Yii::t('app', 'Tag'),
    ];
  }
}
