<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%request_break_down}}".
*
* @property integer $id
* @property integer $boat_id
* @property string $req_date
* @property string $description
* @property string $break_down_level
* @property string $reason
* @property string $user_remarks
* @property integer $status
* @property string $admin_remarks
* @property string $admin_action_date
* @property integer $completed
* @property string $completed_date
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class RequestBreakDown extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%request_break_down}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id', 'req_date', 'description', 'break_down_level', 'reason'], 'required'],
      [['description', 'user_remarks', 'admin_remarks'], 'string'],
      [['admin_action_date','completed_date', 'created_at', 'updated_at'], 'safe'],
      [['boat_id', 'status','completed', 'created_by', 'updated_by'], 'integer'],
      [['break_down_level', 'reason'], 'string', 'max' => 3],
      [['user_remarks'], 'checkReason'],
    ];
  }

  public function checkReason($attribute, $params)
  {
    if($this->reason=='mem' && $this->user_remarks==''){
      $this->addError($attribute, 'Please specify remarks');
      return false;
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_id' => Yii::t('app', 'Boat'),
      'description' => Yii::t('app', 'Description'),
      'break_down_level' => Yii::t('app', 'Level'),
      'reason' => Yii::t('app', 'Reason'),
      'user_remarks' => Yii::t('app', 'Member Remarks'),
      'status' => Yii::t('app', 'Status'),
      'admin_remarks' => Yii::t('app', 'Remarks'),
      'admin_action_date' => Yii::t('app', 'Date'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBoat()
  {
	   return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus($status)
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Service Request marked '.Yii::$app->operationHelperFunctions->requestStatus[$status].' successfully');
		return true;
	}
}
