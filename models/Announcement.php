<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%announcement}}".
*
* @property integer $id
* @property string $heading
* @property string $date
* @property string $descp
* @property string $start_date
* @property string $end_date
* @property integer $status
*/
class Announcement extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%announcement}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['heading','date','descp','start_date','end_date'],'required'],
      [['heading'],'trim'],
      [['status','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['heading','date','descp','start_date','end_date'],'string'],
      ['status','default','value'=>1],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'heading' => Yii::t('app', 'Title'),
      'date' => Yii::t('app', 'Date'),
      'descp' => Yii::t('app', 'Description'),
      'start_date' => Yii::t('app', 'Start Date'),
      'end_date' => Yii::t('app', 'End Date'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Announcement ('.$this->heading.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Announcement ('.$this->heading.') trashed successfully');
		return true;
	}
}
