<?php
namespace app\models;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Change Password form
 */
class ChangePasswordForm extends Model
{
  public $old_password,$new_password,$confirm_new_password;

  private $_user;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['old_password','new_password','confirm_new_password'], 'required'],
      ['confirm_new_password', 'compare', 'compareAttribute' => 'new_password'],
      [['old_password','new_password','confirm_new_password'], 'string', 'min' => 6],
      ['old_password', 'validatePassword'],
      [['old_password','new_password','confirm_new_password'], 'filter', 'filter' => 'trim'],
    ];
  }

  /**
   * Validates the password.
   * This method serves as the inline validation for password.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function validatePassword($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $user = Yii::$app->user->identity;
      if (!$user || !$user->validatePassword($this->old_password)) {
          $this->addError($attribute, Yii::t('app','Incorrect old password.'));
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'old_password' => Yii::t('app', 'Old Password'),
      'new_password' => Yii::t('app', 'New Password'),
      'confirm_new_password' => Yii::t('app', 'Confirm Password'),
    ];
  }

  /**
  * Saves profile info
  *
  * @return boolean whether the profile is saved
  */
  public function save()
  {
    if ($this->validate()) {
      $profile = Yii::$app->user->identity;
      $profile->password=$this->new_password;
      if(!$profile->save()){
        if($profile->hasErrors()){
          foreach($profile->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $this->addError('',$val);
                return false;
              }
            }
          }
        }
      }
      $connection = \Yii::$app->db;
  		$connection->createCommand(
        "update ".UserProfileInfo::tableName()." set changed_pass=:changed_pass where user_id=:id",
        [
          ':changed_pass'=>1,
          ':id'=>$profile->id,
        ]
      )
      ->execute();
      return true;
    } else {
      return false;
    }
  }
}
