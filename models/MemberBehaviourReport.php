<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
* MemberBehaviourReport
*/
class MemberBehaviourReport extends Model
{
  public $status_type,$status;
  public $date,$start_date,$end_date;
  public $pageSize,$bsmb_counter;

  /**
  * @param  string                          $token
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  * @throws \yii\base\InvalidParamException if token is empty or not valid
  */
  public function __construct($config = [],$date,$status_type,$status)
  {
    if($date==null)$date=(date("Y-m-d", strtotime("-1 WEEK", strtotime(date("Y-m-d")))).' - '.date("Y-m-d"));
    $this->date = $date;
    $this->status_type = $status_type;
    $this->status = $status;
    list($this->start_date,$this->end_date)=explode(" - ",$this->date);

    $this->bsmb_counter = Yii::$app->appHelperFunctions->getSetting('behaviour_report_counter');
    parent::__construct($config);
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['date'], 'string'],
    ];
  }

  public function search($params)
  {
    $this->load($params);
    $query = Booking::find()
    ->where(['user_id'=>Yii::$app->statsFunctions->membersSubQuery,'trashed'=>0])
    ->andWhere(['<=','booking_date',date("Y-m-d")])
    ->andFilterWhere(['>=','booking_date',$this->start_date])
    ->andFilterWhere(['<=','booking_date',$this->end_date]);

    if($this->status_type=='be'){
      $query->andWhere(['status'=>[1,2],'booking_exp'=>2]);
    }

    if($this->status_type=='te'){
      $query->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id");
      $query->andWhere(['status'=>[1,2],'trip_exp'=>$this->status]);
    }

    if($this->status_type=='bs'){
      //more than 3 times
      //$membersSubQuery=User::find()->select(['id'])->where(['user_type'=>0,'s']);//new Expression('"select id from user where user_type=0"');
      $subQueryBSMembers=
      $query->andWhere(['status'=>$this->status])
      ->andWhere('user_id in (select user_id from (SELECT user_id, COUNT(user_id) FROM booking where status='.$this->status.' GROUP BY user_id HAVING COUNT(user_id) > '.$this->bsmb_counter.') as tmpsql)');
    }

    if($this->status_type=='mb'){
      $query->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id");
      if($this->status==1){
        //more then 45 mins and 3 times
        $query->andWhere(['status'=>[1,2],'member_exp'=>1])
        ->andWhere('user_id in (select user_id from (SELECT user_id, COUNT(user_id) FROM booking inner join booking_activity on booking_activity.booking_id=booking.id where member_exp=1 and (member_exp_late_hr>0 or member_exp_late_min>45) GROUP BY user_id HAVING COUNT(user_id) > '.$this->bsmb_counter.') as tmpsql)');
      }
      if($this->status==2){
        $query->andWhere(['status'=>[1,2],'dui'=>1]);
      }
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[3],
      ],
			'sort'=> [
        'defaultOrder' => [Booking::tableName().'.id'=>SORT_DESC],
        'attributes' => [
          Booking::tableName().'.id',
        ],
      ],
    ]);

    return $dataProvider;
  }

  public function getTotalCount($status_type,$status)
  {
    if($status_type=='be'){
      $query=Booking::find()
      ->where(['status'=>[1,2],'booking_exp'=>$status]);
    }
    if($status_type=='te'){
      $query=Booking::find()
      ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
      ->where(['status'=>1,'trip_exp'=>$status]);
    }
    if($status_type=='bs'){
      $query=Booking::find()
      ->where(['status'=>$status])
      ->andWhere('user_id in (select user_id from (SELECT user_id, COUNT(user_id) FROM booking where status='.$status.' GROUP BY user_id HAVING COUNT(user_id) > '.$this->bsmb_counter.') as tmpsql)');
    }
    if($status_type=='mb'){
      $col='member_exp';
      if($status==2)$col='dui';
      $query=Booking::find()
      ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
      ->where(['status'=>1,$col=>1]);
      if($col=='member_exp'){
        $query->andWhere('user_id in (select user_id from (SELECT user_id, COUNT(user_id) FROM booking inner join booking_activity on booking_activity.booking_id=booking.id where member_exp=1 and (member_exp_late_hr>0 or member_exp_late_min>45) GROUP BY user_id HAVING COUNT(user_id) > '.$this->bsmb_counter.') as tmpsql)');
      }
    }
    if(isset($query)){
      $query->andFilterWhere([
        'and',
        ['user_id'=>Yii::$app->statsFunctions->membersSubQuery,'trashed'=>0],
        ['>=','booking_date',$this->start_date],
        ['<=','booking_date',$this->end_date],
      ]);
      return $query->count(Booking::tableName().'.id');
    }else{
      return 0;
    }
  }
}
