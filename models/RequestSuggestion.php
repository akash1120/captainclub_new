<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* RequestSuggestion
*/
class RequestSuggestion extends Model
{
  public $comments;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['comments'], 'required'],
      [['comments'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'comments' => 'Comments or any other request',
    ];
  }

  /**
  * Sends an email to the specified email address using the information collected by this model.
  * @param  string  $email the target email address
  * @return boolean whether the model passes validation
  */
  public function send()
  {
    if ($this->validate()) {
      $req=new UserRequests;
      $req->item_type='suggestion';
      $req->descp=nl2br($this->comments);
      $req->save();

      Yii::$app->mailer->compose(['html' => 'requestSuggestion-html', 'text' => 'requestSuggestion-text'], ['request' => $this])
      ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
      ->setReplyTo(Yii::$app->user->identity->email)
      ->setTo(Yii::$app->controller->getSetting('suggestionEmail'))
      ->setCc([Yii::$app->controller->getSetting('suggestionEmail2'),Yii::$app->params['icareEmail'],Yii::$app->params['feedbackCc2']])
      ->setSubject('New Feedback - ' . Yii::$app->params['siteName'])
      ->send();

      $templateId=Yii::$app->controller->getSetting('feedback_response');
      $template=EmailTemplate::findOne($templateId);
      if($template!=null){
        $vals = [
          '{captainName}' => Yii::$app->user->identity->username,
          '{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'/images/email_logo.png" alt="'.Yii::$app->params['siteName'].'">',
        ];
        $htmlBody=$template->searchReplace($template->template_html,$vals);
        $textBody=$template->searchReplace($template->template_text,$vals);
        $message=Yii::$app->mailer->compose()
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setReplyTo(Yii::$app->controller->getSetting('suggestionEmail'))
        ->setSubject('Feedback/Suggestion Received.')
        ->setHtmlBody($htmlBody)
        ->setTextBody($textBody);

        foreach(Yii::$app->user->identity->activePackage->contract->members as $packageMember){
          $message->setTo($packageMember->member->email);
          $message->send();
        }
      }
      return true;
    }
    return false;
  }
}
