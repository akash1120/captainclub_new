<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* UserPasswordResetForm is the model behind the member password reset.
*/
class UserPasswordResetForm extends Model
{
  public $user_id,$new_password,$force_change_password;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['user_id','new_password','force_change_password'], 'required'],
      [['user_id','force_change_password'],'integer'],
      [['new_password'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'new_password' => 'New Password',
      'force_change_password' => 'Force Change Password',
    ];
  }

  /**
  * Save amount to user account
  */
  public function save()
  {
    if ($this->validate()) {
      $user=User::findOne($this->user_id);
      $password=Yii::$app->security->generatePasswordHash($this->new_password);
      $connection = \Yii::$app->db;
  		$connection->createCommand(
        "update ".User::tableName()." set password_hash=:password_hash where id=:id",
        [
          ':password_hash'=>$password,
          ':id'=>$user->id,
        ]
      )
      ->execute();
      if($this->force_change_password==0){
    		$connection->createCommand(
          "update ".UserProfileInfo::tableName()." set changed_pass=:changed_pass where user_id=:user_id",
          [
            ':changed_pass'=>$this->force_change_password,
            ':user_id'=>$user->id,
          ]
        )
        ->execute();
      }
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Member('.$user->id.' - '.$user->fullname.') password reset successfully');
      return true;
    }
    return false;
  }
}
