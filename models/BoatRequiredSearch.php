<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BoatRequired;

/**
* BoatRequiredSearch represents the model behind the search form of `app\models\BoatRequired`.
*/
class BoatRequiredSearch extends BoatRequired
{
  public $listType,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'city_id', 'marina_id', 'time_id', 'no_of_boats', 'pageSize'], 'integer'],
      [['listType', 'boat_type', 'date', 'comments'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = BoatRequired::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'city_id' => $this->city_id,
      'marina_id' => $this->marina_id,
      'time_id' => $this->time_id,
      'no_of_boats' => $this->no_of_boats,
      'boat_type' => $this->boat_type,
    ]);

    if($this->listType=='future'){
      $query->andWhere(['>=', 'date', date("Y-m-d")]);
      $query->andWhere([BoatRequired::tableName().'.trashed'=>0]);
    }elseif($this->listType=='history'){
      $query->andWhere(['<', 'date', date("Y-m-d")]);
      $query->andWhere([BoatRequired::tableName().'.trashed'=>0]);
    }elseif($this->listType=='deleted'){
      $query->andWhere([BoatRequired::tableName().'.trashed'=>1]);
    }

    if($this->date!=null){
      //Check if its range
      if(strpos($this->date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->date);
        $query->andWhere(['and',['>=','date',$start_date],['<=','date',$end_date]]);
      }else{
        $query->andWhere(['date'=>$this->date]);
      }
    }

    $query->andFilterWhere(['like', 'comments', $this->comments]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    return Yii::$app->helperFunctions->getBoatRequiredTabs($this);
  }
}
