<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%order}}".
*
* @property integer $id
* @property int $user_id
* @property string $order_id
* @property double $amount_payable
* @property string $qty
* @property double $profit
* @property string $payment_date
* @property string $detail
* @property double $amount
* @property string $payment_method
* @property string $status
* @property double $response_code
* @property double $trans_date
* @property int $transaction_id
* @property string $currency
* @property string $customer_name
* @property string $customer_email
* @property string $customer_phone
* @property string $first_4_digits
* @property string $last_4_digits
* @property string $card_brand
* @property string $secure_sign
* @property int $use_saved_cc_id
*/
class Order extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%order}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','amount_payable','detail'], 'required'],
      [['detail'], 'string'],
      [['user_id', 'created_by', 'updated_by'], 'integer'],
      [['amount_payable','amount'], 'number'],
      [['created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User ID'),
      'order_id' => Yii::t('app', 'Order ID'),
      'amount' => Yii::t('app', 'Amount'),
      'amount_payable' => Yii::t('app', 'Amount Payable'),
      'transaction_id' => Yii::t('app', 'Transaction ID'),
      'response_code' => Yii::t('app', 'Response Code'),
      'detail' => Yii::t('app', 'Detail'),
      'payment_date' => Yii::t('app', 'Payment Date'),
      'status' => Yii::t('app', 'Status'),
      'payment_method' => Yii::t('app', 'Payment Method'),
      'currency' => Yii::t('app', 'Currency'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMember()
  {
	   return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return string, name of user
   */
  public function getMemberName()
  {
    if($this->member!=null){
	   return $this->member->fullname;
    }
  }
}
