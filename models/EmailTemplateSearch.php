<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmailTemplate;

/**
* EmailTemplateSearch represents the model behind the search form about `app\models\EmailTemplate`.
*/
class EmailTemplateSearch extends EmailTemplate
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'pageSize'], 'integer'],
      [['title', 'template_html', 'template_text'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = EmailTemplate::find()
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like', 'title', $this->title])
    ->andFilterWhere(['like', 'template_html', $this->template_html])
    ->andFilterWhere(['like', 'template_text', $this->template_text]);

    return $dataProvider;
  }
}
