<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%booking_activity_addons}}".
*
* @property integer $booking_id
* @property integer $addon_id
*/
class BookingActivityAddons extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_activity_addons}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['booking_id','addon_id'], 'required'],
      [['booking_id','addon_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'booking_id' => Yii::t('app', 'Booking'),
      'addon_id' => Yii::t('app', 'Addon'),
    ];
  }
}
