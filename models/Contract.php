<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%contract}}".
*
* @property integer $id
* @property string $reference_no
* @property integer $package_id
* @property string $package_name
* @property string $start_date
* @property string $end_date
* @property integer $allowed_captains
* @property integer $remaining_captains
* @property integer $allowed_freeze_days
* @property integer $remaining_freeze_days
* @property integer $expiry_alert
* @property integer $is_activated
*/
class Contract extends ActiveRecord
{
  public $member_idz;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%contract}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['package_id', 'start_date', 'end_date'], 'required'],
      [['package_id', 'allowed_captains', 'remaining_captains', 'allowed_freeze_days', 'remaining_freeze_days', 'created_by', 'updated_by'], 'integer'],
      [['reference_no', 'package_name', 'start_date', 'end_date'], 'string'],
      [['remaining_captains', 'remaining_freeze_days'], 'default', 'value'=>0],
      [['member_idz'], 'each', 'rule'=>['integer']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'package_id' => Yii::t('app', 'Package'),
      'package_name' => Yii::t('app', 'Package Name'),
      'start_date' => Yii::t('app', 'Start Date'),
      'end_date' => Yii::t('app', 'End Date'),
      'allowed_captains' => Yii::t('app', 'Total Captains'),
      'remaining_captains' => Yii::t('app', 'Remaining Captains'),
      'allowed_freeze_days' => Yii::t('app', 'Total Freezing Days'),
      'remaining_freeze_days' => Yii::t('app', 'Remaining Freezing Days'),
      'package_name' => Yii::t('app', 'Package'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getPackage()
  {
    return $this->hasOne(Package::className(), ['id' => 'package_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMembers()
  {
    return $this->hasMany(ContractMember::className(), ['contract_id' => 'id']);
  }

  /**
  * @return array of active contract members
  */
  public function getMemberIdz()
  {
    $idz=[];
    $members=$this->members;
    if($members!=null){
      foreach($members as $member){
        $idz[]=$member->user_id;
      }
    }
    return $idz;
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      $isActivated=0;
      if($this->member_idz!=null){
        foreach($this->member_idz as $key=>$val){
          if($val!=null && $val>0){
            $contractMember=new ContractMember;
            $contractMember->contract_id=$this->id;
            $contractMember->user_id=$val;
            $contractMember->status=1;
            $contractMember->save();

            $user = User::findOne($val);
            $connection = \Yii::$app->db;

            //Updading renewed contract in old contract
            $connection->createCommand(
              "update ".Contract::tableName()." set renewed_contract_id=:renewed_contract_id where id=:id",
              [
                ':renewed_contract_id'=>$this->id,
                ':id'=>$user->active_contract_id,
              ]
            )
            ->execute();
            $generateRenewalPassword=false;
            $contractCount=Yii::$app->statsFunctions->getUserContractCount($user->id);
            $this->setReminders($val,$contractCount,$this->start_date,$this->end_date);
            if($contractCount==1){
              $connection->createCommand(
                "update ".User::tableName()." set active_contract_id=:active_contract_id,active_package_id=:active_package_id,status=:status,end_date=:end_date where id=:id",
                [':active_contract_id'=>$this->id,':active_package_id'=>$this->package_id,':status'=>1,':end_date'=>$this->end_date,':id'=>$val]
              )
              ->execute();
              $isActivated=1;
              $user = User::findOne($val);
              $user->sendNewMemberEmail();
            }else{
              if($user->status!=1){

                $connection->createCommand(
                  "update ".User::tableName()." set active_contract_id=:active_contract_id,active_package_id=:active_package_id,status=:status,end_date=:end_date where id=:id",
                  [':active_contract_id'=>$this->id,':active_package_id'=>$this->package_id,':status'=>1,':end_date'=>$this->end_date,':id'=>$val]
                )
                ->execute();
                $connection->createCommand(
                  "update ".UserProfileInfo::tableName()." set changed_pass=0,profile_updated=0 where user_id=:id",
                  [':id'=>$val]
                )
                ->execute();
                $isActivated=1;

                $user = User::findOne($val);
                $user->sendActivatedRenewAlert();
              }else{
                if($user->activeContract->end_date>=date("Y-m-d")){
                  //still have active contract

                  //Check if its continous
                  $nextContractStartBe=date ("Y-m-d", strtotime("+1 day", strtotime($user->activeContract->end_date)));
                  if($this->start_date==$nextContractStartBe){
                    $connection->createCommand(
                      "update ".User::tableName()." set status=:status,end_date=:end_date where id=:id",
                      [':status'=>1,':end_date'=>$this->end_date,':id'=>$val]
                    )
                    ->execute();
                  }
                }else{
                  //already expired
                  $generateRenewalPassword=true;
                  $connection->createCommand(
                    "update ".User::tableName()." set active_contract_id=:active_contract_id,active_package_id=:active_package_id,status=:status,end_date=:end_date where id=:id",
                    [':active_contract_id'=>$this->id,':active_package_id'=>$this->package_id,':status'=>1,':end_date'=>$this->end_date,':id'=>$val]
                  )
                  ->execute();
                  $connection->createCommand(
                    "update ".UserProfileInfo::tableName()." set changed_pass=0,profile_updated=0 where user_id=:id",
                    [':id'=>$val]
                  )
                  ->execute();
                  $isActivated=1;
                }

                $user = User::findOne($val);
                $user->sendRenewAlert($generateRenewalPassword);
              }
            }
          }
        }
      }
      $reference='TCCC-'.date('y').'-'.sprintf('%03d', $this->id);
      $connection = \Yii::$app->db;
      $connection->createCommand("update ".self::tableName()." set reference_no=:reference_no,is_activated=:is_activated where id=:id",[':reference_no'=>$reference,':is_activated'=>$isActivated,':id'=>$this->id])->execute();
    }
    parent::afterSave($insert, $changedAttributes);
  }


  /**
  * Set 1 month, 5 month & 1 month before expiry reminders
  */
  public function setReminders($user_id,$contractCount,$start_date,$end_date)
  {
    list($sy,$sm,$sd)=explode("-",$start_date);
    list($ey,$em,$ed)=explode("-",$end_date);
    if($contractCount==1){
      //1 month feedback
      $reminder = new UserNote;
      $reminder->user_id=$user_id;
      $reminder->reminder_type=1;
      $reminder->is_feedback=1;
      $reminder->comments='1 month feedback';
      $reminder->reminder=date("Y-m-d H:i:s",mktime(10,0,0,$sm,($sd+30),$sy));
      $reminder->save();
    }
    //5 months feedback
    $reminder = new UserNote;
    $reminder->user_id=$user_id;
    $reminder->reminder_type=1;
    $reminder->is_feedback=1;
    $reminder->comments='5 months feedback';
    $reminder->reminder=date("Y-m-d H:i:s",mktime(10,0,0,($sm+5),$sd,$sy));
    $reminder->save();

    //1 month before expiry feedback
    $reminder = new UserNote;
    $reminder->user_id=$user_id;
    $reminder->reminder_type=1;
    $reminder->is_feedback=1;
    $reminder->comments='1 month before expiry feedback';
    $reminder->reminder=date("Y-m-d H:i:s",mktime(10,0,0,($em-1),$ed,$ey));
    $reminder->save();
  }

  /**
  * Calculates remaining Captains
  * @return integer
  */
  public function getRemainingCaptains()
  {
    $captainStats=Yii::$app->statsFunctions->getCaptainsByContract($this->id);
    return $captainStats['remaining'];
  }

  /**
  * Calculates remaining Freeze Days
  * @return integer
  */
  public function getRemainingFreezeDays()
  {
    $freezeDaysStats=Yii::$app->statsFunctions->getFreezeDaysByContract($this->id);
    return $freezeDaysStats['remaining'];
  }

  /**
  * Mark record as deleted and hides fron list.
  * @return boolean
  */
  public function softDelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Contract ('.$this->reference_no.') trashed successfully');
    return true;
  }

	public function sendExpiryAlert()
	{
		//Expiry Member Alert
		$templateId=Yii::$app->appHelperFunctions->getSetting('e_expiry_u');
		$template=EmailTemplate::findOne($templateId);
		if($template!=null){
      Yii::$app->params['includeDefSignature']=false;
			$members = $this->members;
			if($members!=null){
				foreach($members as $member){
					$vals = [
						'{resetlink}' => '<a href="'.Yii::$app->params['siteUrl'].Yii::$app->params['resetUrl'].'">'.Yii::$app->params['siteUrl'].Yii::$app->params['resetUrl'].'</a>',
						'{loginlink}' => Yii::$app->params['siteUrl'],
						'{username}' => $member->member->fullname,
						'{email}' => $member->member->email,
						'{packageStart}' => Yii::$app->formatter->asDate($this->start_date),
						'{packageEnd}' => Yii::$app->formatter->asDate($this->end_date),
						'{packageName}' => (($this->package_id!=null && $this->package_id>0) ? $this->package->name : 'No Package'),
						'{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'images/email_logo.png" width="136" height="105" alt="" />',
					];
					$htmlBody=$template->searchReplace($template->template_html,$vals);
					$textBody=$template->searchReplace($template->template_text,$vals);
					Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
						->setFrom([Yii::$app->params['expiryingAlertFrom'] => Yii::$app->params['siteName']])
						->setTo($member->member->email)
						->setSubject('Membership Renewal')
						->send();
				}
			}
			$connection = \Yii::$app->db;
			$connection->createCommand("update ".self::tableName()." set expiry_alert=1 where id=:id",[':id'=>$this->id])->execute();
		}
	}
}
