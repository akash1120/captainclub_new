<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ContractChangeHistory;

/**
* ContractChangeHistorySearch represents the model behind the search form of `app\models\ContractChangeHistory`.
*/
class ContractChangeHistorySearch extends ContractChangeHistory
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
			[['contract_id','pageSize'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = ContractChangeHistory::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
			'contract_id' => $this->contract_id,
    ]);

    return $dataProvider;
  }
}
