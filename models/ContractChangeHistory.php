<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%contract_change_history}}".
*
* @property integer $id
* @property integer $contract_id
* @property integer $old_package_id
* @property integer $new_package_id
* @property string $old_package_name
* @property string $new_package_name
* @property integer $old_allowed_captains
* @property integer $new_allowed_captains
* @property integer $old_allowed_freeze_days
* @property integer $new_allowed_freeze_days
* @property integer $days_extended
* @property string $note
*/
class ContractChangeHistory extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%contract_change_history}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['contract_id'], 'required'],
      [['contract_id', 'old_package_id', 'new_package_id', 'old_allowed_captains', 'new_allowed_captains', 'old_allowed_freeze_days', 'new_allowed_freeze_days','days_extended'], 'integer'],
      [['old_package_name', 'new_package_name', 'note'], 'string'],
      [['old_allowed_captains', 'new_allowed_captains', 'old_allowed_freeze_days', 'new_allowed_freeze_days','days_extended'], 'default', 'value'=>0],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'old_package_id' => Yii::t('app', 'Old Package'),
      'new_package_id' => Yii::t('app', 'New Package'),
      'old_package_name' => Yii::t('app', 'Old Package Name'),
      'new_package_name' => Yii::t('app', 'New Package Name'),
      'old_allowed_captains' => Yii::t('app', 'Old Total Captains'),
      'new_allowed_captains' => Yii::t('app', 'New Total Captains'),
      'old_allowed_freeze_days' => Yii::t('app', 'Old Total Freezing Days'),
      'new_allowed_freeze_days' => Yii::t('app', 'New Total Freezing Days'),
      'note' => Yii::t('app', 'Note'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getOldPackage()
  {
    return $this->hasOne(Package::className(), ['id' => 'old_package_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getNewPackage()
  {
    return $this->hasOne(Package::className(), ['id' => 'new_package_id']);
  }
}
