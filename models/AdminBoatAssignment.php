<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
 * This is the model class for table "{{%admin_boat_assignment}}".
 *
 * @property integer $id
 * @property integer $from_booking
 * @property integer $to_booking
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class AdminBoatAssignment extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
      return '{{%admin_boat_assignment}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['from_booking', 'to_booking'], 'required'],
      [['from_booking', 'to_booking', 'created_by', 'updated_by'], 'integer'],
      [['created_at', 'updated_at'], 'string'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'from_booking' => Yii::t('app', 'From Booking'),
      'to_booking' => Yii::t('app', 'To Booking'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getFromBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'from_booking']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getToBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'to_booking']);
  }
}
