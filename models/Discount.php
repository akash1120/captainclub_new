<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\web\UploadedFile;

/**
* This is the model class for table "{{%discount}}".
*
* @property integer $id
* @property string $title
* @property string $descp
* @property string $image
* @property integer $status
*/
class Discount extends ActiveRecord
{
  public $imageFile,$oldImage;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%discount}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['title'],'required'],
      [['status','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['title','descp'],'string'],
      [['title'],'trim'],
      ['status','default','value'=>1],
      [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'title' => Yii::t('app', 'Title'),
      'descp' => Yii::t('app', 'Detail'),
      'image' => Yii::t('app', 'Image'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'image')){
      $this->imageFile = UploadedFile::getInstance($this, 'image');
      // if no file was uploaded abort the upload
      if (!empty($this->imageFile)) {
        $pInfo=pathinfo($this->imageFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->imageFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->imageFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->image==null && $this->oldImage!=null){
      $this->image=$this->oldImage;
    }
    return parent::beforeValidate();
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if ($this->imageFile!== null && $this->image!=null) {
      if($this->oldImage!=null && $this->image!=$this->oldImage && file_exists(Yii::$app->params['discount_uploads_abs_path'].$this->oldImage)){
        unlink(Yii::$app->params['discount_uploads_abs_path'].$this->oldImage);
      }
      $this->imageFile->saveAs(Yii::$app->params['discount_uploads_abs_path'].$this->image);
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /**
  * Enable / Disable the record
  * @return boolean
  */
  public function updateStatus()
  {
    $status=1;
    if($this->status==1)$status=0;
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
      )
      ->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'City ('.$this->title.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
      return true;
    }

    /**
    * Mark record as deleted and hides fron list.
    * @return boolean
    */
    public function softDelete()
    {
      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
        [
          ':trashed'=>1,
          ':trashed_at'=>date("Y-m-d H:i:s"),
          ':trashed_by'=>Yii::$app->user->identity->id,
          ':id'=>$this->id,
        ]
        )
        ->execute();
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'City ('.$this->title.') trashed successfully');
        return true;
      }
    }
