<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%fuel_consumption_type}}".
*
* @property integer $id
* @property string $title
*/
class FuelConsumptionType extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%fuel_consumption_type}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['title'], 'required'],
      [['title'], 'string', 'max' => 32],
      [['title'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'title' => Yii::t('app', 'Title'),
    ];
  }
}
