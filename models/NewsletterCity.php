<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%newsletter_city}}".
*
* @property integer $newsletter_id
* @property integer $city_id
*/
class NewsletterCity extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%newsletter_city}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['newsletter_id','city_id'], 'required'],
      [['newsletter_id','city_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'newsletter_id' => Yii::t('app', 'Newsletter'),
      'city_id' => Yii::t('app', 'City'),
    ];
  }
}
