<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%support_ticket}}".
*
* @property integer $id
* @property string $rssid
* @property integer $ref_id
* @property string $ref_no
* @property integer $parent_id
* @property integer $user_id
* @property string $title
* @property integer $priority_id
* @property string $due_date
* @property string $descp
* @property integer $status
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class SupportTicket extends ActiveRecord
{
	public $username;
	public $attachmentFile;
	public $attachmentFileR;
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%support_ticket}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title','priority_id','descp'], 'required'],
			[['title','priority_id','descp'], 'required', 'on' => 'ticket'],
			[['created_at', 'updated_at', 'trashed_at'], 'safe'],
			[['ref_id','parent_id','user_id','priority_id','status','created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
			[['rssid','ref_no','title','username','due_date','descp'], 'string'],
			[['attachmentFile'], 'each', 'rule'=>['string']],
      [['title'],'trim'],
		];
	}

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios['reply'] = ['descp'];//Scenario Values Only Accepted
		return $scenarios;
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'ref_no' => Yii::t('app', 'Reference#'),
			'user_id' => Yii::t('app', 'Member'),
			'username' => Yii::t('app', 'Member'),
			'title' => Yii::t('app', 'Title'),
			'priority_id' => Yii::t('app', 'Priority'),
			'due_date' => Yii::t('app', 'Due Date'),
			'descp' => Yii::t('app', 'Description'),
			'status' => Yii::t('app', 'Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getParent()
	{
		return $this->hasOne(SupportTicket::className(), ['ref_id' => 'parent_id']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getMember()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getPriority()
	{
		return $this->hasOne(AppPriority::className(), ['id' => 'priority_id']);
	}

	public function afterSave($insert, $changedAttributes)
	{
		$connection = \Yii::$app->db;

		if($insert){
			$rssid = Yii::$app->helperFunctions->generateToken(20).$this->id;
			$ref_id=Yii::$app->helperFunctions->ticketRefId;
			$this->ref_id = $ref_id;
			$ref_no='TCC-'.date('y').'-'.sprintf('%03d', $ref_id);

			$connection->createCommand(
				"update ".self::tableName()." set rssid=:rssid,ref_id=:ref_id,ref_no=:ref_no where id=:id",
				[
					':rssid'=>$rssid,
					':ref_id'=>$ref_id,
					':ref_no'=>$ref_no,
					':id'=>$this->id
				]
				)->execute();
			}
			//Saving Attachment files
			if($this->attachmentFile!=null){
				$n=0;
				foreach($this->attachmentFile as $key=>$val){
					if($val!=null && file_exists(Yii::$app->params['temp_abs_path'].$val)){
						$tmpfile=pathinfo($val);
						$fileName=Yii::$app->fileHelperFunctions->generateName().".".$tmpfile['extension'];
						rename(Yii::$app->params['temp_abs_path'].$val,Yii::$app->params['ticket_uploads_abs_path'].$fileName);
						$attRow=new SupportTicketAttachment;
						$attRow->ticket_ref_id=$this->ref_id;
						$attRow->file_name=$fileName;
						$attRow->save();
					}
					$n++;
				}
			}
			if(isset($_POST['SupportTicket']['attachmentFileR']) && $_POST['SupportTicket']['attachmentFileR']!=null){
				$n=0;
				foreach($_POST['SupportTicket']['attachmentFileR'] as $key=>$val){
					if($val!=null && file_exists(Yii::$app->params['temp_abs_path'].$val)){
						$tmpfile=pathinfo($val);
						$fileName=Yii::$app->fileHelperFunctions->generateName().".".$tmpfile['extension'];
						rename(Yii::$app->params['temp_abs_path'].$val,Yii::$app->params['ticket_uploads_abs_path'].$fileName);
						$attRow=new SupportTicketAttachment;
						$attRow->ticket_ref_id=$this->ref_id;
						$attRow->file_name=$fileName;
						$attRow->save();
					}
					$n++;
				}
			}
			return parent::afterSave($insert, $changedAttributes);
		}

		/**
		* @return \yii\db\ActiveQuery
		*/
		public function getAttachments()
		{
			return $this->hasMany(SupportTicketAttachment::className(), ['ticket_ref_id' => 'id'])->andWhere(['trashed'=>0]);
		}

		/**
		* @return array attachments
		*/
		public function getAttachmentInfo()
		{
			$arrFiles=[];
			$results=SupportTicketAttachment::find()->where(['ticket_ref_id'=>$this->ref_id,'trashed'=>0])->asArray()->all();
			if($results!=null){
				$otherFilesArr=['doc','docx','xls','xlsx','pdf'];
				foreach($results as $attachment){
					if($attachment['file_name']!=null && file_exists(Yii::$app->params['ticket_uploads_abs_path'].$attachment['file_name'])){
						$fpInfo=pathinfo(Yii::$app->params['ticket_uploads_abs_path'].$attachment['file_name']);
						$fileLink=Yii::$app->fileHelperFunctions->getImagePath('ticket',$attachment['file_name'],'medium');
						if(in_array($fpInfo['extension'],$otherFilesArr)){
							$icon='images/icons/'.$fpInfo['extension'].'.jpg';
						}else{
							$icon=Yii::$app->fileHelperFunctions->getImagePath('ticket',$attachment['file_name'],'small');
						}
						$arrFiles[]=[
							'id'=>$attachment['id'],
							'iconPath'=>$icon,
							'link'=>$fileLink,
						];
					}
				}
			}
			return $arrFiles;
		}

		public function getGalleryImages()
		{
			$html = '';
			$images=$this->attachmentInfo;
			if($images!=null){
				$html.= '<div class="row">';
				foreach($images as $image){
					$html.= '
					<div class="col-xs-12 col-sm-2">
					<div class="thumbnail"><center><a href="'.$image['link'].'" target="_blank"><img src="'.$image['imagePath'].'" class="img-responsive" /></a></center></div>
					</div>
					';
				}
				$html.= '</div>';
			}
			return $html;
		}

	  /**
	   * Enable / Disable the record
	   * @return boolean
	   */
		public function updateStatus()
		{
	    $status=1;
	    if($this->status==1)$status=0;
			$connection = \Yii::$app->db;
			$connection->createCommand(
	      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
	      [
	        ':status'=>$status,
	        ':updated_at'=>date("Y-m-d H:i:s"),
	        ':updated_by'=>Yii::$app->user->identity->id,
	        ':id'=>$this->id,
	      ]
	    )
	    ->execute();
	    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Ticket ('.$this->title.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
			return true;
		}

	  /**
	   * Mark record as deleted and hides fron list.
	   * @return boolean
	   */
		public function softDelete()
		{
			$connection = \Yii::$app->db;
			$connection->createCommand(
	      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
	      [
	        ':trashed'=>1,
	        ':trashed_at'=>date("Y-m-d H:i:s"),
	        ':trashed_by'=>Yii::$app->user->identity->id,
	        ':id'=>$this->id,
	      ]
	    )
	    ->execute();
	    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Ticket ('.$this->title.') trashed successfully');
			return true;
		}
	}
