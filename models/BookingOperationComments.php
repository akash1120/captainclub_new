<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%booking_operation_comments}}".
*
* @property integer $id
* @property integer $booking_id
* @property integer $opr_booking_id
* @property string $comments
* @property integer $process_type
* @property integer $user_type
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class BookingOperationComments extends ActiveRecord
{
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%booking_operation_comments}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['booking_id','comments'], 'required'],
			[['actions', 'created_at', 'trashed_at'], 'safe'],
			[['booking_id', 'opr_booking_id', 'process_type', 'user_type', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
			[['comments'], 'string'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'booking_id' => Yii::t('app', 'Booking'),
			'opr_booking_id' => Yii::t('app', 'Operations Booking'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}
}
