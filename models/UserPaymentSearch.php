<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserPayment;

/**
* UserPaymentSearch represents the model behind the search form of `app\models\UserPayment`.
*/
class UserPaymentSearch extends UserPayment
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'user_id', 'is_opening_bal', 'booking_id','created_by','updated_by','pageSize'],'integer'],
      [['amount_to_pay', 'amount_paid', 'message','created_at','updated_at'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = UserPayment::find()
    ->select([
      'id',
      'created_at',
      'message',
      'amount_to_pay',
      'amount_paid',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions

    $query->andFilterWhere([
      'user_id' => $this->user_id,
    ]);
    $query->andFilterWhere([
      '<=','DATE(created_at)','2018-12-18',
    ]);

    return $dataProvider;
  }

  public function getRowBalanceAmount($rowId)
  {
    $totalAmtToPaySoFar=UserPayment::find()->where(['and',['<=','id',$rowId],['user_id'=>$this->user_id]])->sum('amount_to_pay');
    $totalAmtPaidSoFar=UserPayment::find()->where(['and',['<=','id',$rowId],['user_id'=>$this->user_id]])->sum('amount_paid');
    $balance=($totalAmtPaidSoFar-$totalAmtToPaySoFar);
    $html = '<span class="badge grid-badge badge-'.($balance<0 ? 'danger' : 'success').'">AED '.$balance.'</span>';
    return $html;
  }

  public function getTabItems()
  {
    return Yii::$app->helperFunctions->getUserPaymentsTabs($this->user_id);
  }
}
