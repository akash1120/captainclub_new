<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%package_services}}".
*
* @property string $package_id
* @property string $service_id
*/
class PackageAllowedOptions extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%package_services}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['package_id','service_id'],'required'],
      [['package_id','service_id'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'package_id' => Yii::t('app', 'Package'),
      'service_id' => Yii::t('app', 'Option'),
    ];
  }
}
