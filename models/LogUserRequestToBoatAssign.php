<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%log_user_request_to_boat_assign}}".
*
* @property integer $id
* @property string $request_type
* @property integer $request_id
* @property integer $booking_id
* @property integer $city_id
* @property integer $marina_id
* @property string $boat_id
* @property integer $date
* @property integer $time_id
*/
class LogUserRequestToBoatAssign extends ActiveRecord
{
  public $selected_boat=0;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%log_user_request_to_boat_assign}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['request_type','request_id','city_id','marina_id','boat_id','date','time_id'],'required'],
      [['request_id','booking_id','city_id','marina_id','boat_id','time_id','selected_boat'],'integer'],
      [['date'],'string'],
      [['boat_id'],'checkAlreadyBooked'],
    ];
  }

  /**
  * checks already booked
  */
  public function checkAlreadyBooked($attribute, $params)
  {
    $checkBooking=Booking::find()
    ->where([
      'and',
      ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
      [
        'city_id'=>$this->city_id,
        'port_id'=>$this->marina_id,
        'boat_id'=>$this->boat_id,
        'booking_date'=>$this->date,
        'booking_time_slot'=>$this->time_id,
        'status'=>1,
        'trashed'=>0,
      ]
    ]);
    if($checkBooking->exists()){
      $this->addError('','Sorry selected boat is already booked, please choose another available boat.');
      return false;
    }
    return true;
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'request_type' => Yii::t('app', 'Request Type'),
      'request_id' => Yii::t('app', 'Request'),
      'booking_id' => Yii::t('app', 'Booking'),
      'city_id' => Yii::t('app', 'City'),
      'marina_id' => Yii::t('app', 'Marina'),
      'boat_id' => Yii::t('app', 'Boat'),
      'date' => Yii::t('app', 'Date'),
      'time_id' => Yii::t('app', 'Time'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @inheritdoc
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      $adminBookingID=0;
      $result=Booking::find()
      ->where([
        'city_id'=>$this->marina->city_id,
        'port_id'=>$this->marina_id,
        'boat_id'=>$this->boat_id,
        'booking_date'=>$this->date,
        'booking_time_slot'=>$this->time_id,
        'trashed'=>0,
        'user_id'=>Yii::$app->appHelperFunctions->adminIdz
      ])
      ->asArray()->one();
      if($result!=null){
        $adminBookingID=$result['id'];
        $connection = \Yii::$app->db;
        $connection->createCommand("update ".Booking::tableName()." set trashed=1 where id=:id",[':id'=>$adminBookingID])->execute();
      }
      $booking=new Booking;
      $booking->booking_source='request';
      $booking->user_id=$this->request->user_id;
      $booking->city_id=$this->city_id>0 ? $this->city_id : $this->marina->city_id;
      $booking->port_id=$this->marina_id;
      $booking->boat_id=$this->boat_id;
      $booking->booking_date=$this->date;
      $booking->booking_time_slot=$this->time_id;
      if($booking->boat->special_boat==1 && $booking->boat->boat_selection==1){
        $booking->selected_boat=$this->selected_boat;
      }
      $booking->booking_exp=3;
      if($booking->save()){
        $this->booking_id=$booking->id;
        if($adminBookingID>0){
          $adminBoatAssignment=new AdminBoatAssignment;
          $adminBoatAssignment->from_booking=$adminBookingID;
          $adminBoatAssignment->to_booking=$booking->id;
          $adminBoatAssignment->save();
        }
        return true;
      }else{
        if($booking->hasErrors()){
          foreach($booking->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $this->addError('',$val);
              }
            }
          }
        }
        return false;
      }
    } else {
      return false;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getRequest()
  {
    if($this->request_type=='boatbooking'){
      return UserBookingRequest::findOne($this->request_id);
    }
    if($this->request_type=='nightdrivetraining'){
      return UserNightdriveTrainingRequest::findOne($this->request_id);
    }
    if($this->request_type=='upgradecity'){
      return UserCityUpgradeRequest::findOne($this->request_id);
    }
    if($this->request_type=='other'){
      return UserOtherRequest::findOne($this->request_id);
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCity()
  {
    return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMarina()
  {
    return $this->hasOne(Marina::className(), ['id' => 'marina_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBoat()
  {
    return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTime()
  {
    return $this->hasOne(TimeSlot::className(), ['id' => 'time_id']);
  }
}
