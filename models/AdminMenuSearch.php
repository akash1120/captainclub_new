<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
* AdminMenuSearch represents the model behind the search form about `backend\models\AdminMenu`.
*/
class AdminMenuSearch extends AdminMenu
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'parent', 'show_in_menu','pageSize'], 'integer'],
      [['icon', 'title', 'controller_id', 'action_id', 'param1', 'value1'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = AdminMenu::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'parent' => $this->parent,
      'show_in_menu' => $this->show_in_menu,
    ]);

    $query->andFilterWhere(['like', 'icon', $this->icon])
    ->andFilterWhere(['like', 'title', $this->title])
    ->andFilterWhere(['like', 'controller_id', $this->controller_id])
    ->andFilterWhere(['like', 'action_id', $this->action_id])
    ->andFilterWhere(['like', 'param1', $this->param1])
    ->andFilterWhere(['like', 'value1', $this->value1]);

    return $dataProvider;
  }
}
