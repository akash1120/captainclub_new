<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%contract_member}}".
*/
class OldContractMember extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%contract_member}}';
  }
}
