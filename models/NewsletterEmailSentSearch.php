<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NewsletterEmailSent;

/**
* NewsletterEmailSentSearch represents the model behind the search form about `app\models\NewsletterEmailSent`.
*/
class NewsletterEmailSentSearch extends NewsletterEmailSent
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','newsletter_id','user_id','email_status','opens','clicks','pageSize'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = NewsletterEmailSent::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $query->andFilterWhere([
      'newsletter_id' => $this->newsletter_id,
    ]);

    return $dataProvider;
  }
}
