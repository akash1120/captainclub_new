<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%user_device}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $device_type
 * @property string $device_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class UserDevice extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%user_device}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id','device_type','device_id'], 'required'],
      [['user_id', 'created_by', 'updated_by'], 'integer'],
      [['device_type','device_id'], 'string'],
      [['created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
      'device_type' => Yii::t('app', 'Device Type'),
      'device_id' => Yii::t('app', 'Device ID'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
          'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }
}
