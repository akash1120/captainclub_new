<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%request_inspection}}".
*
* @property integer $id
* @property integer $boat_id
* @property string $inspection_date
* @property string $remarks
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class RequestInspection extends ActiveRecord
{
  public $item_id=[],$item_condition=[],$item_comments=[];
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%request_inspection}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id', 'inspection_date', 'remarks'], 'required'],
      [['inspection_date', 'completed_date', 'created_at', 'updated_at'], 'safe'],
      [['remarks'], 'string'],
      [['boat_id','completed', 'created_by', 'updated_by'], 'integer'],
      [['item_id'], 'each', 'rule' => ['integer']],
      [['item_condition', 'item_comments'], 'each', 'rule' => ['string']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_id' => Yii::t('app', 'Boat'),
      'inspection_date' => Yii::t('app', 'Date'),
      'remarks' => Yii::t('app', 'Remarks'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBoat()
  {
    return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getInspectionItems()
  {
    return $this->hasMany(RequestInspectionItems::className(), ['inspection_id' => 'id']);
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($this->item_id!=null){
      foreach($this->item_id as $key=>$val){
        $damgesRow=RequestInspectionItems::find()->where(['inspection_id'=>$this->id,'item_id'=>$val])->one();
        if($damgesRow==null){
          $damgesRow=new RequestInspectionItems;
          $damgesRow->inspection_id=$this->id;
          $damgesRow->item_id=$val;
        }
        $damgesRow->item_condition=$this->item_condition[$key];
        $damgesRow->item_comments=$this->item_comments[$key];
        $damgesRow->save();
      }
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /* Getter for damaged items*/
  public function getInspectionItemsDetail()
  {
    $detail='';
    if($this->inspectionItems!=null){
      $n=1;
      foreach($this->inspectionItems as $item){
        if($detail!='')$detail.='<br />';
        $detail.='<strong>'.$n.'.</strong>'.$item->item->title.' ('.$item->item_condition.' - '.$item->item_comments.')';
        $n++;
      }
    }
    return $detail;
  }
}
