<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Staff;

/**
* StaffSearch represents the model behind the search form of `app\models\Staff`.
*/
class StaffSearch extends Staff
{
  public $permission_group,$name,$mobile,$city_name,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','permission_group','city_name','status','created_by','updated_by','pageSize'],'integer'],
      [['username','name','email','mobile'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = Staff::find()
    ->select([
      Staff::tableName().'.id',
      'permission_group_id',
      'permission_group'=>AdminGroup::tableName().'.title',
      'image',
      'username',
      'name'=>'CONCAT(firstname," ",lastname)',
      'email',
      UserProfileInfo::tableName().'.mobile',
      'city_name'=>City::tableName().'.name',
      Staff::tableName().'.status',
    ])
    ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.Staff::tableName().'.id')
    ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.Staff::tableName().'.permission_group_id')
    ->leftJoin(City::tableName(),City::tableName().'.id='.UserProfileInfo::tableName().'.city_id')
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      Staff::tableName().'.id' => $this->id,
      'user_type' => 1,
      'permission_group_id' => $this->permission_group,
      'city_id' => $this->city_name,
      Staff::tableName().'.status' => $this->status,
      Staff::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['like','username',$this->username])
    ->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
    ->andFilterWhere(['like','email',$this->email])
    ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

    return $dataProvider;
  }
}
