<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%user_accounts}}".
*
* @property integer $id
* @property integer $user_id
* @property integer $order_id
* @property string $trans_type
* @property string $descp
* @property integer $account_id
* @property integer $amount
* @property integer $profit
* @property integer $booking_id
*/
class UserAccounts extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_accounts}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','trans_type','descp','account_id','amount'], 'required'],
      [['trans_type','descp'], 'string'],
      [['user_id','booking_id', 'created_by', 'updated_by'], 'integer'],
      [['amount'], 'number'],
      [['created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
      'trans_type' => Yii::t('app', 'Type'),
      'descp' => Yii::t('app', 'Detail'),
      'amount' => Yii::t('app', 'Amount'),
      'booking_id' => Yii::t('app', 'Booking'),
      'created_at' => Yii::t('app', 'Date & Time'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMember()
  {
      return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getOrder()
  {
      return $this->hasOne(Order::className(), ['id' => 'order_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBooking()
  {
      return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
    $amountsToPay = UserAccounts::find()->where(['trans_type'=>'dr','user_id'=>$this->user_id])->sum('amount');
    $ammountsPaid = UserAccounts::find()->where(['trans_type'=>'cr','user_id'=>$this->user_id])->sum('amount');
    $total = $ammountsPaid-$amountsToPay;
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".UserProfileInfo::tableName()." set credit_balance=:credit_balance where user_id=:id",
      [':credit_balance'=>$total,':id'=>$this->user_id]
    )
    ->execute();
    parent::afterSave($insert, $changedAttributes);
  }
}
