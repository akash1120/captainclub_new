<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%package_group_to_package}}".
*
* @property string $package_group_id
* @property string $package_id
*/
class PackageGroupToPackage extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%package_group_to_package}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['package_group_id','package_id'],'required'],
      [['package_group_id','package_id'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'package_group_id' => Yii::t('app', 'Package Group'),
      'package_id' => Yii::t('app', 'Package'),
    ];
  }
}
