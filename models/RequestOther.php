<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* RequestOther is the model behind the request type other form.
*/
class RequestOther extends Model
{
  public $comments,$req_date;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['comments'], 'required'],
      [['comments','req_date'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'req_date' => 'If your request is for a Specific date, specify it please',
      'comments' => 'please type your request below and we will get back to you soon',
    ];
  }

  /**
  * Save credits to user account
  */
  public function send()
  {
    if ($this->validate()) {

      $req=new UserRequests;
      $req->item_type='other';
      $req->descp=nl2br($this->comments);
      $req->requested_date=$this->req_date;
      $req->save();

      Yii::$app->mailer->compose(['html' => 'requestOther-html', 'text' => 'requestOther-text'], ['request' => $this])
      ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
      ->setReplyTo(Yii::$app->user->identity->email)
      ->setTo(Yii::$app->params['icareEmail'])
      ->setCc([Yii::$app->params['mdEmail']])
      ->setSubject('New Request Type Other - ' . Yii::$app->params['siteName'])
      ->send();

      $templateId=Yii::$app->controller->getSetting('general_response');
      $template=EmailTemplate::findOne($templateId);
      if($template!=null){
        $vals = [
          '{captainName}' => $this->member->username,
          '{emailMessage}' => 'Your request type other is under process.',
          '{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'/images/email_logo.png" alt="'.Yii::$app->params['siteName'].'">',
        ];
        $htmlBody=$template->searchReplace($template->template_html,$vals);
        $textBody=$template->searchReplace($template->template_text,$vals);
        $message=Yii::$app->mailer->compose()
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
        ->setSubject('New Request Posted')
        ->setHtmlBody($htmlBody)
        ->setTextBody($textBody);

        $memberContract=$this->member->activeContract;

        foreach($memberContract->members as $contractMember){
          $message->setTo($contractMember->member->email);
          $message->send();
        }
      }
      return true;
    }
    return false;
  }
}
