<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
* This is the model class for table "{{%user_night_drive_request_reminder}}".
*
* @property integer $id
* @property integer $user_id
* @property string $comments
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class UserNightDriveRequestReminder extends ActiveRecord
{
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%user_night_drive_request_reminder}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['created_at', 'updated_at'], 'safe'],
			[['user_id', 'created_by', 'updated_by'], 'integer'],
			[['comments'], 'string']
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User'),
			'comments' => Yii::t('app', 'Comments'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
		];
	}

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMember()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
		if($insert){
			Yii::$app->mailer->compose(['html' => 'requestNightDriveTrainingReminder-html', 'text' => 'requestNightDriveTrainingReminder-text'], ['model' => $this])
				->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
				->setTo("sajjad@thecaptainsclub.ae ")
				->setSubject('Night Drive Training Reminder - ' . Yii::$app->params['siteName'])
				->send();

			$templateId=Yii::$app->appHelperFunctions->getSetting('general_response');
			$template=EmailTemplate::findOne($templateId);
			if($template!=null){
				$vals = [
					'{captainName}' => $this->member->fullname,
          '{emailMessage}' => 'Your night drive training request is under process.',
				];
				$htmlBody=$template->searchReplace($template->template_html,$vals);
				$textBody=$template->searchReplace($template->template_text,$vals);
				$message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
					->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
	        ->setSubject('Night Drive Training Request Posted')
					->setTo($this->member->email)
					->send();
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}
}
