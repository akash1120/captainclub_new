<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RequestDiscussion;

/**
* RequestDiscussionSearch represents the model behind the search form about `app\models\RequestDiscussion`.
*/
class RequestDiscussionSearch extends RequestDiscussion
{
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'request_id', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
      [['request_type','comments', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = RequestDiscussion::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['updated_at'=>SORT_DESC],]
    ]);

    $this->load($params);

    $query->andFilterWhere([
      'id' => $this->id,
      'request_type' => $this->request_type,
      'request_id' => $this->request_id,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like', 'comments', $this->comments]);

    return $dataProvider;
  }
}
