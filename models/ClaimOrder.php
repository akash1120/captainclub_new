<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%claim_order}}".
*
* @property integer $id
* @property string $ref_no
* @property integer $user_id
* @property string $total_amount
* @property string $bill_image
* @property integer $status
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class ClaimOrder extends ActiveRecord
{
	public $items,$process_type,$user_type,$comments;

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%claim_order}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['user_id','city_id','marina_id'], 'required'],
			[['user_id','city_id','marina_id', 'process_type', 'user_type', 'status', 'created_by', 'updated_by'], 'integer'],
			[['total_amount'], 'number'],
			[['ref_no', 'selections', 'comments', 'bill_image', 'created_at', 'updated_at'], 'string'],
			[['items'], 'each', 'rule'=>['integer']],
			[['items'], 'validateItems'],
			['items', 'required', 'when' => function($model) {
				return $model->marina_id == Yii::$app->params['emp_id'];
			}],
		];
	}

	/**
	* Validates the password.
	* This method serves as the inline validation for password.
	*
	* @param string $attribute the attribute currently being validated
	* @param array $params the additional name-value pairs given in the rule
	*/
	public function validateItems($attribute, $params)
	{
		if (!$this->hasErrors()) {
			if($this->marina_id==Yii::$app->params['emp_id']){
				if($this->items==null){
					$this->addError('','Please select bookings.');
					return false;
				}
			}
		}
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'ref_no' => Yii::t('app', 'Ref#'),
			'user_id' => Yii::t('app', 'Staff'),
			'total_amount' => Yii::t('app', 'Total Amount'),
			'created_at' => Yii::t('app', 'Date'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getClaimOrderItems()
	{
		return $this->hasMany(ClaimOrderItem::className(), ['order_id' => 'id']);
	}

	/**
	* @inheritdoc
	*/
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if($this->items!=null){
				$this->selections=implode(",",$this->items);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		if($this->comments!=null){
			$coComment=new ClaimOrderComments;
			$coComment->claim_order_id=$this->id;
			$coComment->comments=$this->comments;
			$coComment->process_type=$this->process_type;
			$coComment->user_type=$this->user_type;
			$coComment->save();
		}
		$connection = \Yii::$app->db;
		$totalAmount=0;
		if($this->items!=null){
			foreach($this->items as $key=>$val){
				$amount=0;
				$booking=Booking::findOne(['id'=>$val]);
				if($booking!=null){
					$amount=$booking->bookingActivity->claimAmount;

				}
				$totalAmount+=$amount;
				$orderItem=new ClaimOrderItem;
				$orderItem->order_id=$this->id;
				$orderItem->main_booking_id=$val;
				$orderItem->amount=$amount;
				if($orderItem->save()){

					$connection->createCommand(
						"update ".BookingActivity::tableName()." set claimed=1,claimed_at=:claimed_at,claimed_by=:claimed_by where booking_id=:booking_id",
						[
							':claimed_at'=>date("Y-m-d H:i:s"),
							':claimed_by'=>Yii::$app->user->identity->id,
							':booking_id'=>$val,
						]
						)->execute();
					}else{
						print_r($orderItem->getErrors());
					}
				}
			}
			if($insert){
				if($totalAmount>0){
					$connection->createCommand()
					->update(
						self::tableName(),
						[
							'total_amount'=>$totalAmount,
						],
						['id' => $this->id]
						)->execute();
					}
					$reference='TCC'.'-'.date('y').'-'.sprintf('%03d', $this->id);

					$connection->createCommand()
					->update(
						self::tableName(),
						[
							'ref_no'=>$reference,
						],
						['id' => $this->id]
						)->execute();

						if($this->marina_id!=Yii::$app->params['emp_id']){
							$amountToClaim=Yii::$app->appHelperFunctions->getUnClaimedMarinaAmount($this->marina_id);
							if($amountToClaim==0){
								//Update claimed all pending bookings
								$connection->createCommand("update ".BookingActivity::tableName()." set claimed=1,claimed_at='".date("Y-m-d H:i:s")."',claimed_by=".Yii::$app->user->identity->id." where booking_id in (select id from ".Booking::tableName()." where port_id=".$this->marina_id.") and hide_other=1 and claimed=0 and fuel_chargeable=1")->execute();
								//Update all orders done
								$connection->createCommand(
									"update ".self::tableName()." set amt_claimed=:amt_claimed where marina_id=:marina_id and amt_claimed=0",
									[
										':amt_claimed'=>1,
										':marina_id'=>$this->marina_id,
									]
									)->execute();
							}
						}
					}
					parent::afterSave($insert, $changedAttributes);
				}
			}
