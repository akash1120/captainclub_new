<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * BulkBooking form
 */
class NewUserBooking extends Model
{
  public $user_id,$username,$city_id,$marina_id,$boat_id,$time_id,$date,$booking_type,$comments;
  public $reason;
  public $selected_boat=0;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id', 'username', 'city_id', 'marina_id', 'boat_id', 'time_id', 'date', 'booking_type','comments'], 'required'],
    	[['user_id', 'city_id', 'marina_id', 'boat_id', 'time_id', 'booking_type','reason','selected_boat'],'integer'],
    	[['username', 'date', 'comments'],'string'],
    	[['boat_id'],'checkAlreadyBooked'],
    ];
  }

  /**
   * checks already booked
   */
	public function checkAlreadyBooked($attribute, $params)
	{
    $result=Booking::find()
      ->where([
        'and',
        ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
        [
          'city_id'=>$this->city_id,
          'port_id'=>$this->marina_id,
          'boat_id'=>$this->boat_id,
          'booking_date'=>$this->date,
          'booking_time_slot'=>$this->time_id,
          'status'=>1,
          'trashed'=>0
        ]
      ]);
    if($result->exists()){
      $this->addError($attribute,'Sorry selected boat is already booked, please choose another available boat.');
      return false;
    }
    return true;
	}

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'username' => Yii::t('app', 'Member'),
      'city_id' => Yii::t('app', 'City'),
      'marina_id' => Yii::t('app', 'Marina'),
      'boat_id' => Yii::t('app', 'Boat'),
      'time_id' => Yii::t('app', 'Time'),
      'date' => Yii::t('app', 'Date'),
      'booking_type' => Yii::t('app', 'Type'),
      'comments' => Yii::t('app', 'Comments'),
      'reason' => Yii::t('app', 'Reason'),
    ];
  }

  /**
   * @return boolean whether the model passes validation
   */
  public function save()
  {
    if ($this->validate()) {
      $connection = \Yii::$app->db;

      $marina=$this->marina['name'];
      $timeSlot=$this->timeSlot['dashboard_name'];
      //Create a request, then user request boat assign.
      $req=new UserBookingRequest;
			$req->descp='New Booking from Admin';
			$req->user_id=$this->user_id;
			$req->city_id=$this->city_id;
			$req->marina=$marina;
			$req->time_slot=$timeSlot;
			$req->requested_date=$this->date;
      $req->sendEmailAlert = 'no';
			if($req->save()){
        $boatAssignment = new LogUserRequestToBoatAssign();
        $boatAssignment->request_type = 'boatbooking';
        $boatAssignment->request_id = $req->id;
        $boatAssignment->city_id = $this->city_id;
        $boatAssignment->date = $this->date;
        $boatAssignment->marina_id = $this->marina_id;
        $boatAssignment->time_id = $this->time_id;
        $boatAssignment->boat_id = $this->boat_id;
        $boatAssignment->selected_boat = $this->selected_boat;
        if($boatAssignment->save()){
          $active_show_till=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")+3),date("Y")));
          $connection->createCommand("update ".UserBookingRequest::tableName()." set status=1,active_show_till='".$active_show_till."',admin_action_date=:admin_action_date where id=:id",[':admin_action_date'=>date("Y-m-d H:i:s"),':id'=>$req->id])->execute();

          $connection->createCommand("update ".Booking::tableName()." set booking_type=:booking_type,booking_comments=:booking_comments where id=:id",[':booking_type'=>$this->booking_type,':booking_comments'=>$this->comments,':id'=>$boatAssignment->booking_id])->execute();
        }else{
          Yii::info('Error while creating booking from request - admin side - User = '.$this->user_id,'my_custom_log');
          if($boatAssignment->hasErrors()){
            foreach($boatAssignment->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  Yii::info('Error: '.$val,'my_custom_log');
                }
              }
            }
          }
          Yii::info('End Error for '.$this->user_id,'my_custom_log');
        }
        return true;
      }else{
        if($req->hasErrors()){
          foreach($req->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $this->addError('',$val);
              }
            }
          }
        }
        return false;
      }
    }
    return false;
  }

  public function getMarina()
  {
    return Marina::find()->where(['id'=>$this->marina_id])->asArray()->one();
  }

  public function getTimeSlot()
  {
    return TimeSlot::find()->where(['id'=>$this->time_id])->asArray()->one();
  }
}
