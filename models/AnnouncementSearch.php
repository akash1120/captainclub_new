<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Announcement;

/**
* AnnouncementSearch represents the model behind the search form of `app\models\Announcement`.
*/
class AnnouncementSearch extends Announcement
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['heading','date','start_date','end_date','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['heading'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = Announcement::find()
    ->select([
      'id',
      'heading',
      'date',
      'descp',
      'start_date',
      'end_date',
      'status',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'date' => $this->date,
      'start_date' => $this->start_date,
      'end_date' => $this->end_date,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like','heading',$this->heading])
    ->andFilterWhere(['like','descp',$this->descp]);

    return $dataProvider;
  }
}
