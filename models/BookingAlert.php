<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%booking_alert}}".
*
* @property integer $id
* @property integer $alert_type
* @property string $alert_date
* @property integer $port_id
* @property string $message
* @property string $email_message
* @property integer $is_sent
*/
class BookingAlert extends ActiveRecord
{
  public $time_slot_id;

  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_alert}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['alert_type','alert_date','port_id','time_slot_id','message', 'email_message'], 'required'],
      [['alert_type','port_id'], 'integer'],
      [['alert_date','message', 'email_message'], 'string'],
      [['time_slot_id'],'each', 'rule'=>['integer']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'alert_date' => Yii::t('app', 'Date'),
      'port_id' => Yii::t('app', 'Marina'),
      'message' => Yii::t('app', 'Message'),
      'email_message' => Yii::t('app','Email Message'),
      'time_slot_id' => Yii::t('app', 'Timing'),
      'timingHtml' => Yii::t('app', 'Timing'),
      'is_sent' => Yii::t('app', 'Sent'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMarina()
  {
    return $this->hasOne(Marina::className(), ['id' => 'port_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTimings()
  {
    $alertTimings=BookingAlertTiming::find()->select(['time_slot_id'])->where(['alert_id'=>$this->id]);
    return TimeSlot::find()->where(['id'=>$alertTimings])->asArray()->all();
  }

  public function getTimingHtml()
  {
    $html='';
    $timings=$this->timings;
    if($timings!=null){
      $html.='<ul>';
      foreach($timings as $timing){
        $name=$timing['name'];
        $name=str_replace(" (".$timing['timing'].")","",$name);
        $name=str_replace($timing['timing'],"",$name);
        $html.='<li>'.$name.' ('.$timing['timing'].')</li>';
      }
      $html.='</ul>';
    }
    return $html;
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
		if($this->time_slot_id!=null){
      BookingAlertTiming::deleteAll(['and', 'alert_id = :alert_id', ['not in', 'time_slot_id', $this->time_slot_id]], [':alert_id' => $this->id]);
			foreach($this->time_slot_id as $key=>$val){
				$childRow=BookingAlertTiming::find()->where(['alert_id'=>$this->id,'time_slot_id'=>$val])->one();
				if($childRow==null){
					$childRow=new BookingAlertTiming;
					$childRow->alert_id=$this->id;
					$childRow->time_slot_id=$val;
					$childRow->save();
				}
			}
		}
    if($insert){
			Yii::$app->mailer->compose(['html' => 'bookingAlertAdmin-html', 'text' => 'bookingAlertAdmin-text'], ['model' => $this])
				->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
				->setTo(Yii::$app->params['mdEmail'])
				->setSubject('New '.($this->alert_type==1 ? 'Warning' : 'Hold').' Alert Added - ' . Yii::$app->params['siteName'])
				->send();
		}
    parent::afterSave($insert, $changedAttributes);
  }

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Booking Alert ('.($this->alert_type==1 ? 'Warning' : 'Hold').') ('.$this->alert_date.') trashed successfully');
		return true;
	}
}
