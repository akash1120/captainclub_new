<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%operations_requests}}".
*
* @property integer $id
* @property integer $boat_id
* @property string $req_type
* @property integer $req_id
* @property integer $completed
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class OperationRequests extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%operations_requests}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id', 'req_type', 'req_id'], 'required'],
      [['boat_id', 'req_id', 'completed', 'created_by', 'updated_by'], 'integer'],
      [['completed_date', 'created_at', 'updated_at'], 'safe'],
      [['req_type'], 'string', 'max' => 3],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_id' => Yii::t('app', 'Boat ID'),
      'req_type' => Yii::t('app', 'Req Type'),
      'req_id' => Yii::t('app', 'Req ID'),
      'completed' => Yii::t('app', 'Completed'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getRequestDetail()
  {
    if($this->req_type=='S'){
      return RequestService::findOne($this->req_id);
    }elseif($this->req_type=='BD'){
      return RequestBreakDown::findOne($this->req_id);
    }elseif($this->req_type=='I'){
      return RequestInspection::findOne($this->req_id);
    }
  }

  public function getLevel()
  {
    if($this->req_type=='BD'){
      return $this->requestDetail->break_down_level=='maj' ? 'Major' : 'Minor';
    }
  }

  public function getDescp()
  {
    if($this->req_type=='BD'){
      return $this->requestDetail->description;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBoat()
  {
    return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }
}
