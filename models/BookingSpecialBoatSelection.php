<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%booking_special_boat_selection}}".
*
* @property integer $booking_id
* @property integer $boat_id
*/
class BookingSpecialBoatSelection extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_special_boat_selection}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['booking_id','boat_id'], 'required'],
      [['booking_id','boat_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'booking_id' => Yii::t('app', 'Booking'),
      'boat_id' => Yii::t('app', 'Boat'),
    ];
  }
}
