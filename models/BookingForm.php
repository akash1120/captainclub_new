<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
* BookingForm is the model behind the booking search.
*/
class BookingForm extends Model
{
  public $city_id,$port_id,$date,$booking_type,$booking_comments,$pageSize;
  public $totalSessions,$availableSessions;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['city_id','date'], 'required'],
      [['city_id','date','booking_type','booking_comments'], 'required', 'on'=>['admbook']],
      [['city_id','port_id','booking_type','pageSize'],'integer'],
      [['date','booking_comments'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'city_id' => 'City',
      'date' => 'Date',
      'booking_type' => 'Type',
      'booking_comments' => 'Comments',
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  public function getTimeSlotsArray()
  {
    $timeSlotBoatsArray=[];
    $timeSlots=Yii::$app->appHelperFunctions->bookingTimeSlotList;
    if($timeSlots!=null){
      list($sy,$sm,$sd)=explode("-",$this->date);
      $dayOfWeek=date("w",mktime(0,0,0,$sm,$sd,$sy));
      foreach($timeSlots as $timeSlot){
        $timeIdz=[];
        $thisTimeBoats=[];
        $timeIdz=Yii::$app->appHelperFunctions->getChildTimeSlots($timeSlot['id']);
        $subQueryAvailbleDays=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);

        //Package Allowed
        $subQueryPackageAllowedPurposes=Yii::$app->user->identity->activePackageAllowedPurposes;

        $subQueryBulkBooked=BulkBooking::find()->select(['boat_id'])->where(['and',['trashed'=>0],['<=','start_date',$this->date],['>=','end_date',$this->date]]);
        $subQueryBookings=Booking::find()->select(['boat_id'])
        ->where([
          'city_id'=>$this->city_id,
          'port_id'=>$this->port_id,
          'booking_date'=>$this->date,
          'booking_time_slot'=>$timeIdz,
          'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,
          'trashed'=>0,
        ]);

        $subQueryBookingSpecialBoatSelection=null;
        $subQueryNightDriveLimit=null;

        if($timeSlot['id']==Yii::$app->params['nightDriveTimeSlot']){
          $subQueryDateBookings=Booking::find()->select(['id'])
          ->where([
            'city_id'=>$this->city_id,
            'port_id'=>$this->port_id,
            'booking_date'=>$this->date,
            'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,
            'trashed'=>0,
          ]);
          $subQueryBookingSpecialBoatSelection=BookingSpecialBoatSelection::find()->select(['boat_id'])
          ->where(['booking_id'=>$subQueryDateBookings]);

          //Check Limit & hide all
          $thisMarina=Marina::findOne($this->port_id);
          $ndAllowedLimitInMarina=$thisMarina->night_drive_limit;
          $bookedCount=Booking::find()->where(['port_id'=>$this->port_id,'booking_date'=>$this->date,'booking_time_slot'=>$timeIdz,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'is_bulk'=>0,'trashed'=>0])->count('id');
          if($bookedCount>=$ndAllowedLimitInMarina){
            $subQueryNightDriveLimit=Boat::find()->select(['id']);
          }
        }

        $thisTimeBoats=BoatToTimeSlot::find()
        ->select([
          BoatToTimeSlot::tableName().'.time_slot_id',
          'boat_id'=>Boat::tableName().'.id',
          Boat::tableName().'.name',
          Boat::tableName().'.boat_purpose_id',
          Boat::tableName().'.special_boat',
          Boat::tableName().'.special_boat_type',
          Boat::tableName().'.boat_selection',
          TimeSlot::tableName().'.dashboard_name',
          TimeSlot::tableName().'.start_time',
          TimeSlot::tableName().'.end_time',
        ])
        ->innerJoin(Boat::tableName(),Boat::tableName().".id=".BoatToTimeSlot::tableName().".boat_id")
        ->innerJoin(TimeSlot::tableName(),TimeSlot::tableName().".id=".BoatToTimeSlot::tableName().".time_slot_id")
        ->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'time_slot_id'=>$timeIdz,Boat::tableName().'.status'=>1,Boat::tableName().'.trashed'=>0])
        ->andWhere([Boat::tableName().'.id'=>$subQueryAvailbleDays])
        ->andWhere(['or',[Boat::tableName().'.boat_purpose_id'=>$subQueryPackageAllowedPurposes],['special_boat'=>[1,2]]])
        ->andWhere(['not in',Boat::tableName().'.id',$subQueryBulkBooked])
        ->andWhere(['not in',Boat::tableName().'.id',$subQueryBookings])
        ->andFilterWhere(['not in',Boat::tableName().'.id',$subQueryBookingSpecialBoatSelection])
        ->andFilterWhere(['not in',Boat::tableName().'.id',$subQueryNightDriveLimit])
        ->orderBy([Boat::tableName().'.rank'=>SORT_ASC])
        ->asArray()->all();

        $timeSlotBoatsArray[]=[
          'id'=>$timeSlot['id'],
          'title'=>$timeSlot['dashboard_name'],
          'start_time'=>$timeSlot['start_time'],
          'end_time'=>$timeSlot['end_time'],
          'is_special'=>$timeSlot['is_special'],
          'available_till'=>$timeSlot['available_till'],
          'expiry'=>$timeSlot['expiry'],
          'permit_required'=>$timeSlot['permit_required'],
          'availableBoats'=>$thisTimeBoats,
        ];
      }
    }
    return $timeSlotBoatsArray;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    if($this->load($params) && $this->city_id!=null){
      if($this->port_id==null){
        $this->port_id=Yii::$app->appHelperFunctions->getFirstActiveCityMarina($this->city_id)['id'];
      }
      $query = Boat::find();
      // grid filtering conditions
      $query->andFilterWhere([
        'city_id' => $this->city_id,
        'port_id' => $this->port_id,
        'status' => 1,
        'trashed' => 0,
      ]);
    }else{
      $query = Boat::find()
      ->asArray()
      ->where(['id'=>-1]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort'=> ['defaultOrder' => ['rank'=>SORT_ASC]],
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    $marinas=Yii::$app->appHelperFunctions->getActiveCityMarinaList($this->city_id);
    $html='';
    if($marinas!=null){
      foreach($marinas as $marina){
        $tabUrl=Url::to(['create','BookingForm[city_id]'=>$this->city_id,'BookingForm[port_id]'=>$marina['id'],'BookingForm[date]'=>$this->date]);

        $marinaAvailableCount=Yii::$app->statsFunctions->getMarinaAvailableCount($this->city_id,$marina['id'],$this->date);

        $html.='
        <li class="nav-item'.($this->port_id==$marina['id'] ? ' active' : '').'">
          <a class="nav-link" href="'.$tabUrl.'"><i class="fa fa-anchor"></i> '.$marina['short_name'].' ('.$marinaAvailableCount.' Available)</a>
        </li>';
      }
    }
    return $html;
  }
}
