<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RequestService;

/**
* RequestServiceSearch represents the model behind the search form of `app\models\RequestService`.
*/
class RequestServiceSearch extends RequestService
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
        [['id', 'boat_id', 'status', 'created_by', 'updated_by'], 'integer'],
        [['current_hours', 'scheduled_hours', 'remarks', 'req_date', 'admin_remarks', 'admin_action_date', 'created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = RequestService::find()
    ->select([
      RequestService::tableName().'.id',
      'current_hours',
      'scheduled_hours',
      'req_date',
      'remarks',
      RequestService::tableName().'.status',
      'admin_remarks',
      'admin_action_date',
      'boat_id',
      'boat_name'=>Boat::tableName().'.name',
    ])
    ->innerJoin(Boat::tableName(),Boat::tableName().".id=".RequestService::tableName().".boat_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort' => ['defaultOrder'=>['id'=>SORT_DESC]],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      RequestService::tableName().'.id' => $this->id,
      'boat_id' => $this->boat_id,
      'req_date' => $this->req_date,
      RequestService::tableName().'.status' => $this->status,
      'admin_action_date' => $this->admin_action_date,
      RequestService::tableName().'.created_by' => $this->created_by,
      RequestService::tableName().'.created_at' => $this->created_at,
      RequestService::tableName().'.updated_by' => $this->updated_by,
      RequestService::tableName().'.updated_at' => $this->updated_at,
    ]);

    $query->andFilterWhere(['like', 'current_hours', $this->current_hours])
        ->andFilterWhere(['like', 'scheduled_hours', $this->scheduled_hours])
        ->andFilterWhere(['like', 'remarks', $this->remarks])
        ->andFilterWhere(['like', 'admin_remarks', $this->admin_remarks]);

    return $dataProvider;
  }
}
