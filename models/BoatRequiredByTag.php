<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%boat_required_by_tag}}".
*
* @property integer $boat_required_id
* @property integer $tag_id
*/
class BoatRequiredByTag extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_required_by_tag}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_required_id','tag_id'], 'required'],
      [['boat_required_id','tag_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_required_id' => Yii::t('app', 'Boat Required'),
      'tag_id' => Yii::t('app', 'Boat'),
    ];
  }
}
