<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
* This is the model class for table "{{%captain_request}}".
*
* @property integer $id
* @property integer $user_id
* @property integer $booking_id
* @property integer $status
* @property integer $created_by
* @property string $created_at
* @property integer $updated_by
* @property string $updated_at
*/
class CaptainRequest extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%captain_request}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id', 'booking_id'], 'required'],
      [['user_id', 'booking_id', 'status', 'created_by', 'updated_by'], 'integer'],
      [['created_at', 'updated_at'], 'safe'],
      [['status'], 'default', 'value' => 0],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User ID'),
      'booking_id' => Yii::t('app', 'Booking'),
      'status' => Yii::t('app', 'Status'),
      'created_by' => Yii::t('app', 'Created By'),
      'created_at' => Yii::t('app', 'Created At'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

  /**
  * @return integer
  */
  public function getBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
  * @return integer
  */
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }
}
