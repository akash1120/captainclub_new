<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
* This is the model class for table "{{%boats}}".
*
* @property integer $id
* @property integer $city_id
* @property integer $port_id
* @property string $name
* @property integer $boat_purpose_id
* @property string $descp
* @property integer $rank
* @property string $repair
* @property string $image
* @property string $timeslots
* @property integer $watersports
* @property integer $bbq
* @property integer $early_departure
* @property integer $ice
* @property integer $kids_life_jacket
* @property integer $overnight_camp
* @property integer $late_arrival
*/
class Boat extends ActiveRecord
{
  public $fuel_consumption=[];
  public $info_field;
  public $available_days;
  public $tag_id;

  public $imageFile,$oldImage;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boats}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['city_id', 'port_id', 'name', 'rank', 'timeslots', 'watersports','bbq','early_departure','ice','kids_life_jacket','overnight_camp','late_arrival'], 'required'],
      [['city_id', 'port_id', 'boat_purpose_id', 'watersports', 'bbq', 'rank','special_boat','special_boat_type','boat_selection', 'status','early_departure','ice','kids_life_jacket','overnight_camp','late_arrival','wake_boarding','wake_surfing'], 'integer'],
      [['descp', 'repair'], 'string'],
      [['name'], 'string', 'max' => 55],
      [['name'],'trim'],
      [['image'], 'string', 'max' => 100],
      ['fuel_consumption','each','rule'=>['string']],
      ['info_field','each','rule'=>['string']],
      [['available_days','tag_id','timeslots'],'each','rule'=>['integer']],
      [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'city_id' => Yii::t('app', 'City'),
      'port_id' => Yii::t('app', 'Marina'),
      'name' => Yii::t('app', 'Name'),
      'boat_purpose_id' => Yii::t('app', 'Boat Purpose'),
      'descp' => Yii::t('app', 'Note'),
      'rank' => Yii::t('app', 'Order'),
      'repair' => Yii::t('app', 'Repair Text'),
      'image' => Yii::t('app', 'Image'),
      'watersports' => Yii::t('app', 'Water Sports Available?'),
      'bbq' => Yii::t('app', 'BBQ Equipment Available?'),
      'early_departure' => Yii::t('app', 'Early Departure'),
      'ice' => Yii::t('app', 'Ice'),
      'kids_life_jacket' => Yii::t('app', 'Kids Life Jacket'),
      'overnight_camp' => Yii::t('app', 'Overnight Camping'),
      'late_arrival' => Yii::t('app', 'Late Arrival'),
      'special_boat' => Yii::t('app', 'Boat Type'),
      'special_boat_type' => Yii::t('app', 'Type'),
      'boat_selection' => Yii::t('app', 'Can Select Boat'),
      'status' => Yii::t('app', 'Status'),
      'txtStatus' => Yii::t('app', 'Status'),
      'fuel_consumption' => Yii::t('app', 'Fuel Consumption Chart'),
      'info_field' => Yii::t('app', 'Information'),
      'available_days' => Yii::t('app', 'Available Days'),
      'tag_id' => Yii::t('app', 'Tags'),

      'wake_boarding' => Yii::t('app', 'Wake Boarding'),
      'wake_surfing' => Yii::t('app', 'Wake Surfing'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCity()
  {
    return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMarina()
  {
    return $this->hasOne(Marina::className(), ['id' => 'port_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTimeSlots()
  {
    return $this->hasMany(BoatToTimeSlot::className(), ['boat_id' => 'id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTimings()
  {
    $boatTimings=BoatToTimeSlot::find()->select(['time_slot_id'])->where(['boat_id'=>$this->id]);
    return TimeSlot::find()->where(['id'=>$boatTimings])->asArray()->all();
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTags()
  {
    $boatToTags=BoatToTags::find()->select(['tag_id'])->where(['boat_id'=>$this->id]);
    return BoatTag::find()->where(['id'=>$boatToTags])->asArray()->all();
  }

  public function getAvailableTags()
  {
    $html=[];
    $tags=$this->tags;
    if($tags!=null){
      foreach($tags as $tag){
        $html[]='<span class="badge badge-info">'.$tag['title'].'</span>';
      }
    }
    return implode(" ",$html);
  }

  /**
  * @inheritdoc
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      $this->timeslots=serialize($this->timeslots);
      return true;
    } else {
      return false;
    }
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'image')){
      $this->imageFile = UploadedFile::getInstance($this, 'image');
      // if no file was uploaded abort the upload
      if (!empty($this->imageFile)) {
        $pInfo=pathinfo($this->imageFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->imageFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->imageFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->image==null && $this->oldImage!=null){
      $this->image=$this->oldImage;
    }
    return parent::beforeValidate();
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($this->fuel_consumption!=null){
      foreach($this->fuel_consumption as $key=>$val){
        $childRow=BoatFuelConsumptionRate::find()->where(['boat_id'=>$this->id,'type_id'=>$key])->one();
        if($childRow==null){
          $childRow=new BoatFuelConsumptionRate;
          $childRow->boat_id=$this->id;
          $childRow->type_id=$key;
        }
        $childRow->rate_value=$val;
        $childRow->save();
      }
    }

    if($this->timeslots!=null){
      $selectedTimeSlots=unserialize($this->timeslots);
      BoatToTimeSlot::deleteAll(['and', 'boat_id = :boat_id', ['not in', 'time_slot_id', $selectedTimeSlots]], [':boat_id' => $this->id]);
      foreach($selectedTimeSlots as $key=>$val){
        $boatTimeSlotRow=BoatToTimeSlot::find()->where(['boat_id'=>$this->id,'time_slot_id'=>$val])->one();
        if($boatTimeSlotRow==null){
          $boatTimeSlotRow=new BoatToTimeSlot;
          $boatTimeSlotRow->boat_id=$this->id;
          $boatTimeSlotRow->time_slot_id=$val;
          $boatTimeSlotRow->save();
        }
      }
    }


    if($this->info_field!=null){
      BoatInfoValue::deleteAll(['and', 'boat_id = :boat_id', ['not in', 'info_field_id', $this->info_field]], [':boat_id' => $this->id]);
      foreach($this->info_field as $key=>$val){
        $childRow=BoatInfoValue::find()->where(['boat_id'=>$this->id,'info_field_id'=>$key])->one();
        if($childRow==null){
          $childRow=new BoatInfoValue;
          $childRow->boat_id=$this->id;
          $childRow->info_field_id=$key;
        }
        $childRow->field_value=$val;
        $childRow->save();
      }
    }

    if($this->available_days!=null){
      BoatAvailableDays::deleteAll(['and', 'boat_id = :boat_id', ['not in', 'available_day', $this->available_days]], [':boat_id' => $this->id]);
      foreach($this->available_days as $key=>$val){
        $childRow=BoatAvailableDays::find()->where(['boat_id'=>$this->id,'available_day'=>$val])->one();
        if($childRow==null){
          $childRow=new BoatAvailableDays;
          $childRow->boat_id=$this->id;
          $childRow->available_day=$val;
          $childRow->save();
        }
      }
    }

    if($this->tag_id!=null){
      BoatToTags::deleteAll(['and', 'boat_id = :boat_id', ['not in', 'tag_id', $this->tag_id]], [':boat_id' => $this->id]);
      foreach($this->tag_id as $key=>$val){
        $childRow=BoatToTags::find()->where(['boat_id'=>$this->id,'tag_id'=>$val])->one();
        if($childRow==null){
          $childRow=new BoatToTags;
          $childRow->boat_id=$this->id;
          $childRow->tag_id=$val;
          $childRow->save();
        }
      }
    }
    if ($this->imageFile!== null && $this->image!=null) {
      if($this->oldImage!=null && $this->image!=$this->oldImage && file_exists(Yii::$app->params['boat_abs_path'].$this->oldImage)){
        unlink(Yii::$app->params['boat_abs_path'].$this->oldImage);
      }
      $this->imageFile->saveAs(Yii::$app->params['boat_abs_path'].$this->image);
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function getNameWithTimeSlots()
  {
    $fullName=$this->name;
    $fullName.=$this->purpose;
    $fullName.=$this->passengers;
    $weekDays=Yii::$app->helperFunctions->weekDayNames;
    $fullName.='<div style="font-size:10px;">';
    foreach($weekDays as $key=>$val){
      $lblCls="default";
      $availableDays=BoatAvailableDays::find()->where(['boat_id'=>$this->id,'available_day'=>$key]);
      if($availableDays->exists()){
        $lblCls="success";
      }
      $fullName.='<span class="badge badge-'.$lblCls.'">'.substr($val,0,3).'</span>&nbsp;';
    }
    $fullName.='</div>';
    $boatTimings=BoatToTimeSlot::find()->select(['time_slot_id'])->where(['boat_id'=>$this->id]);
    $timeSlots=TimeSlot::find()->where(['id'=>$boatTimings])->asArray()->all();
    if($timeSlots!=null){
      $fullName.='<div style="font-size:10px;">';
      $n=1;
      foreach($timeSlots as $timeSlot){
        if($n>1)$fullName.='<br />';
        $fullName.=str_replace($timeSlot['timing'],"",$timeSlot['name']).' '.$timeSlot['timing'];
        $n++;
      }
      $fullName.=' <a href="javascript:;" data-boat_id="'.$this->id.'" class="boatInfo">?</a></div>';
    }
    return $fullName;
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getPurposeRow()
  {
    return $this->hasOne(BoatPurpose::className(), ['id' => 'boat_purpose_id']);
  }

  public function getPurpose()
  {
    $purposeRow=$this->purposeRow;
    if($purposeRow!=null){
      return ' - '.$purposeRow->title;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBoatPassengerRow()
  {
    $seetingPessengerInfoField=Yii::$app->appHelperFunctions->getSetting('passenger_info_id');
    return BoatInfoValue::find()->select(['field_value'])->where(['boat_id'=>$this->id,'info_field_id'=>$seetingPessengerInfoField])->asArray()->one();
  }

  public function getPassengers()
  {
    $passengerRow=$this->boatPassengerRow;
    if($passengerRow!=null){
      $label=" Persons";
      if(strpos(strtolower($passengerRow['field_value']),"person") !== false)$label="";
      if(strpos(strtolower($passengerRow['field_value']),"passenger") !== false)$label="";
      return ' ('.ucwords($passengerRow['field_value']).''.$label.')';
    }
  }

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Boat ('.$this->name.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Boat ('.$this->name.') trashed successfully');
		return true;
	}
}
