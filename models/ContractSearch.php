<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contract;

/**
* ContractSearch represents the model behind the search form of `app\models\Contract`.
*/
class ContractSearch extends Contract
{
  public $user_id,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','pageSize'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = Contract::find()
    ->select([
      Contract::tableName().'.id',
      'package_name'=>Package::tableName().'.name',
      'start_date',
      'end_date',
      ContractMember::tableName().'.status',
    ])
    ->innerJoin(ContractMember::tableName(),ContractMember::tableName().".contract_id=".Contract::tableName().".id")
    ->innerJoin(Package::tableName(),Package::tableName().".id=".Contract::tableName().".package_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort'=> ['defaultOrder' => ['end_date'=>SORT_DESC]],
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere(['user_id' => $this->user_id]);

    return $dataProvider;
  }
}
