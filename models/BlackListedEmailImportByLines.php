<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\BlackListedEmail;

/**
* BlackListedEmailImportByLines import form
*/
class BlackListedEmailImportByLines extends Model
{
	public $emails;

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['emails'], 'required'],
			[['emails'], 'string']
		];
	}


	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'emails' => Yii::t('app', 'Enter Emails, 1 per line'),
		];
	}

	/**
	* import the entries into table
	*/
	public function save()
	{
		$saved=0;
		$unsaved=0;
		$errNames='';
		if($this->validate()) {
			if($this->emails!=''){
				$emails=explode("\n", $this->emails);
				$n=1;
				foreach($emails as $key=>$email){
					$checkAlready=BlackListedEmail::find()->where(['email'=>trim($email)])->one();
					if($checkAlready!=null){
						if($checkAlready->trashed==1){
							$checkAlready->trashed=0;
							$checkAlready->save();
							$saved++;
						}else{
							$errNames.='<br />'.$email.' already existed';
							$unsaved++;
						}
					}else{
						$model=new BlackListedEmail;
						$model->email=trim($email);
						if($model->save()){
							$saved++;
						}else{
							$errNames.='<br />'.$email;
							$unsaved++;
						}
					}
					$n++;
				}
			}
			if($saved>0){
				Yii::$app->getSession()->setFlash('success', $saved.' - '.Yii::t('app','Emails saved successfully'));
			}
			if($unsaved>0){
				Yii::$app->getSession()->setFlash('error', $unsaved.' - '.Yii::t('app','Emails were not saved!').$errNames);
			}
			return true;
		} else {
			return false;
		}
	}
}
