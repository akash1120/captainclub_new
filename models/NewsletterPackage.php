<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%newsletter_package}}".
*
* @property integer $newsletter_id
* @property integer $package_id
*/
class NewsletterPackage extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%newsletter_package}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['newsletter_id','package_id'], 'required'],
      [['newsletter_id','package_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'newsletter_id' => Yii::t('app', 'Newsletter'),
      'package_id' => Yii::t('app', 'Package'),
    ];
  }
}
