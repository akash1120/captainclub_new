<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClaimOrderItem;

/**
* ClaimOrderItemSearch represents the model behind the search form about `app\models\ClaimOrderItem`.
*/
class ClaimOrderItemSearch extends ClaimOrderItem
{
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['order_id'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = ClaimOrderItem::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    $query->andFilterWhere([
      'order_id' => $this->order_id,
    ]);

    return $dataProvider;
  }
}
