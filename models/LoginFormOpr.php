<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* LoginForm is the model behind the login form.
*
* @property User|null $user This property is read-only.
*
*/
class LoginFormOpr extends Model
{
  public $city_id,$marina_id;
  public $username;
  public $password;
  public $rememberMe = true;

  private $_user = false;


  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // username and password are both required
      [['city_id','marina_id','username','password'], 'required'],
      [['username'], 'validateExpiration'],
      // rememberMe must be a boolean value
      ['rememberMe', 'boolean'],
      // password is validated by validatePassword()
      ['password', 'validatePassword'],
      ['username', 'filter', 'filter' => 'trim'],
    ];
  }

  /**
  * Validates the status.
  *
  * @param string $attribute the attribute currently being validated
  * @param array $params the additional name-value pairs given in the rule
  */
  public function validateExpiration($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $user = $this->getUser();

      if ($user!=null) {
        if($user->status==2){
          //Hold
          $this->addError($attribute, 'Dear Captain, your membership is hold, to resume your membership please Email <a href="mailto:md@thecptainsclub.ae">md@thecptainsclub.ae</a>');
        }elseif($user->status==3){
          //Cancelled
          $this->addError($attribute, 'Dear Captain, your membership is cancelled, to Renew your membership please Email <a href="mailto:md@thecptainsclub.ae">md@thecptainsclub.ae</a>');
        }elseif($user->status==4 || $user->status==5){
          //Expired & Dead
          $this->addError($attribute, 'Dear Captain, your membership is expired, to Renew your membership please Email <a href="mailto:md@thecptainsclub.ae">md@thecptainsclub.ae</a>');
        }elseif($user->status==0 || $user->status==20){
          $this->addError($attribute, 'Dear Captain, your membership is not active, please Email <a href="mailto:md@thecptainsclub.ae">md@thecptainsclub.ae</a>');
        }
      }else{
        $this->addError($attribute, 'Incorrect username or password.');
      }
    }
  }

  /**
  * Validates the password.
  * This method serves as the inline validation for password.
  *
  * @param string $attribute the attribute currently being validated
  * @param array $params the additional name-value pairs given in the rule
  */
  public function validatePassword($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $user = $this->getUser();

      if (!$user || !$user->validatePassword($this->password)) {
        $this->addError($attribute, 'Incorrect username or password.');
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'city_id' => Yii::t('app', 'City'),
      'marina_id' => Yii::t('app', 'Marina'),
      'username' => Yii::t('app', 'Username'),
      'password' => Yii::t('app', 'Password'),
      'rememberMe' => Yii::t('app', 'Remember Me'),
    ];
  }

  /**
  * Logs in a user using the provided username and password.
  * @return bool whether the user is logged in successfully
  */
  public function login()
  {
    $member = $this->getUser();
    if ($this->validate()) {
      if(Yii::$app->user->login($member, $this->rememberMe ? 3600*24*30 : 0)){

        $modelOpr=OperationUserCityMarina::find()->where(['user_id'=>$member->id])->one();
        if($modelOpr==null){
          $modelOpr = new OperationUserCityMarina();
          $modelOpr->user_id=$member->id;
        }
        $modelOpr->city_id=$this->city_id;
        $modelOpr->marina_id=$this->marina_id;
        $modelOpr->save();

        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, $member->name.' logged in successfully');
        $loginTry=new UserLoginHistory;
        $loginTry->user_id=$member->id;
        $loginTry->ip=ip2long(Yii::$app->getRequest()->getUserIP());
        $loginTry->http_referrer=Yii::$app->request->referrer;
        $loginTry->created_at=date("Y-m-d H:i:s");
        $loginTry->login=1;
        $loginTry->save();
        return true;
      }else{
        $loginTry=new UserLoginHistory;
        $loginTry->user_id=$member->id;
        $loginTry->ip=ip2long(Yii::$app->getRequest()->getUserIP());
        $loginTry->http_referrer=Yii::$app->request->referrer;
        $loginTry->created_at=date("Y-m-d H:i:s");
        $loginTry->login=0;
        $loginTry->save();
        return false;
      }
    }else{
      if($member!=null){
        $loginTry=new UserLoginHistory;
        $loginTry->user_id=$member->id;
        $loginTry->ip=ip2long(Yii::$app->getRequest()->getUserIP());
        $loginTry->http_referrer=Yii::$app->request->referrer;
        $loginTry->created_at=date("Y-m-d H:i:s");
        $loginTry->login=0;
        $loginTry->save();
        return false;
      }
    }
    return false;
  }

  /**
  * Finds user by [[username]]
  *
  * @return User|null
  */
  public function getUser()
  {
    if ($this->_user === false) {
      $this->_user = User::findByUsername($this->username);
    }

    return $this->_user;
  }
}
