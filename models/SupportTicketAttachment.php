<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%support_ticket_attachment}}".
*
* @property integer $id
* @property integer $ticket_ref_id
* @property string $file_name
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class SupportTicketAttachment extends ActiveRecord
{
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%support_ticket_attachment}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['ticket_ref_id'], 'required'],
			[['created_at', 'updated_at', 'trashed_at'], 'safe'],
			[['ticket_ref_id','created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
			[['file_name'], 'string']
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'ticket_ref_id' => Yii::t('app', 'Ticket ID'),
			'file_name' => Yii::t('app', 'File'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Ticket Attachment ('.$this->file_name.') trashed successfully');
		return true;
	}
}
