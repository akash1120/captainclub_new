<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%ports}}".
*
* @property integer $id
* @property integer $city_id
* @property string $name
* @property string $short_name
* @property integer $captains
* @property integer $bbq
* @property integer $no_of_early_departures
* @property integer $dashboard_rank
* @property integer $fuel_profit
* @property integer $night_drive_limit
* @property integer $status
*/
class Marina extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%ports}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['city_id','name','short_name'],'required'],
      [['city_id','captains','bbq','no_of_early_departures','no_of_overnight_camps','no_of_late_arrivals','night_drive_limit','wake_boarding_limit','wake_surfing_limit','status'],'integer'],
      [['name'],'string'],
      [['name'],'trim'],
      ['fuel_profit','number']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'city_id' => Yii::t('app', 'City'),
      'name' => Yii::t('app', 'Name'),
      'short_name' => Yii::t('app', 'Short Name'),
      'captains' => Yii::t('app', 'Captains'),
      'bbq' => Yii::t('app', 'BBQ Set'),
      'no_of_early_departures' => Yii::t('app', 'No Of Early Departure Trips Allowed'),
      'night_drive_limit' => Yii::t('app', 'Night Drive Sessions'),
      'fuel_profit' => Yii::t('app', 'Fuel Profit'),

      'wake_boarding_limit' => Yii::t('app', 'Wake Boarding Limit'),
      'wake_surfing_limit' => Yii::t('app', 'Wake Surfing Limit'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCity()
  {
    return $this->hasOne(Cities::className(), ['id' => 'city_id']);
  }

  /**
  * Getter for full name as city and marina name
  * @return full name
  */
  public function getWithCityTitle()
  {
    return $this->city->name.' > '.$this->name;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Marina ('.$this->name.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Marina ('.$this->name.') trashed successfully');
		return true;
	}
}
