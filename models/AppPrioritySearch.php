<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppPriority;

/**
* AppPrioritySearch represents the model behind the search form of `app\models\AppPriority`.
*/
class AppPrioritySearch extends AppPriority
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','color_id','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['title','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['title'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = AppPriority::find()
    ->select([
      AppPriority::tableName().'.id',
      AppPriority::tableName().'.title',
      'color_id',
      'color_title'=>AppColor::tableName().'.title',
      'code',
      'text_color',
      AppPriority::tableName().'.status',
    ])
    ->innerJoin("app_color",AppColor::tableName().".id=".AppPriority::tableName().".color_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      AppPriority::tableName().'.id' => $this->id,
      'color_id' => $this->color_id,
      AppPriority::tableName().'.status' => $this->status,
      AppPriority::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['like',AppPriority::tableName().'.title',$this->title]);

    return $dataProvider;
  }
}
