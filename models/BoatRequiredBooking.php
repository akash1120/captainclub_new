<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%boat_required_booking}}".
*
* @property integer $id
* @property integer $request_id
* @property integer $booking_id
* @property integer $created_by
* @property string $created_at
* @property integer $updated_by
* @property string $updated_at
*/
class BoatRequiredBooking extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_required_booking}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['request_id', 'booking_id'], 'required'],
      [['request_id', 'booking_id', 'created_by', 'updated_by'], 'integer'],
      [['created_at', 'updated_at'], 'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'request_id' => Yii::t('app', 'Request'),
      'booking_id' => Yii::t('app', 'Booking'),
      'created_by' => Yii::t('app', 'From'),
      'created_at' => Yii::t('app', 'Booked On'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".BoatRequired::tableName()." set boats_booked=(boats_booked+1) where id=:id",
        [':id'=>$this->request_id]
      )->execute();
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getRequest()
  {
    return $this->hasOne(BoatRequired::className(), ['id' => 'request_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }
}
