<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BlackListedEmail;

/**
* BlackListedEmailSearch represents the model behind the search form about `app\models\BlackListedEmail`.
*/
class BlackListedEmailSearch extends BlackListedEmail
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'pageSize'], 'integer'],
      [['email'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = BlackListedEmail::find()
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like', 'email', $this->email]);

    return $dataProvider;
  }
}
