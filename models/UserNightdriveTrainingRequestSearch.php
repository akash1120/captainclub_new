<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\data\ActiveDataProvider;
use app\models\UserNightdriveTrainingRequest;

/**
* UserNightdriveTrainingRequestSearch represents the model behind the search form of `app\models\UserNightdriveTrainingRequest`.
*/
class UserNightdriveTrainingRequestSearch extends UserNightdriveTrainingRequest
{
  public $listType,$membername,$item_type,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','user_id','status','pageSize'],'integer'],
      [['listType','membername','requested_date','remarks','created_at'],'safe'],
      [['membername','item_type','requested_date','created_at'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $userIdz=[];
    if($this->user_id!=null){
      $user=User::findOne($this->user_id);
      $userIdz=$user->memberIdz;
    }

    $query = UserNightdriveTrainingRequest::find()
    ->select([
      UserNightdriveTrainingRequest::tableName().'.id',
      new Expression("'nightdrivetraining' as item_type"),
      UserNightdriveTrainingRequest::tableName().'.user_id',
      UserNightdriveTrainingRequest::tableName().'.descp',
      UserNightdriveTrainingRequest::tableName().'.requested_date',
      UserNightdriveTrainingRequest::tableName().'.status',
      UserNightdriveTrainingRequest::tableName().'.remarks',
      UserNightdriveTrainingRequest::tableName().'.admin_action_date',
      UserNightdriveTrainingRequest::tableName().'.active_show_till',
      UserNightdriveTrainingRequest::tableName().'.created_at',
      UserNightdriveTrainingRequest::tableName().'.created_by',
    ])
    ->innerJoin(User::tableName(),User::tableName().".id=".UserNightdriveTrainingRequest::tableName().".user_id")
		->where(['and',
      [UserNightdriveTrainingRequest::tableName().'.trashed'=>0],
		]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $query->andFilterWhere([
      UserNightdriveTrainingRequest::tableName().'.user_id' => $userIdz,
      UserNightdriveTrainingRequest::tableName().'.status' => $this->status,
      UserNightdriveTrainingRequest::tableName().'.requested_date' => $this->requested_date,
      UserNightdriveTrainingRequest::tableName().'.created_at' => $this->created_at,
    ]);
    $query->andFilterWhere(['like',UserNightdriveTrainingRequest::tableName().'.remarks',$this->remarks])
    ->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->membername],
      ['like',User::tableName().'.firstname',$this->membername],
      ['like',User::tableName().'.lastname',$this->membername],
    ]);
    if($this->listType=='active'){
      $query->andWhere(['or',['is',UserNightdriveTrainingRequest::tableName().'.active_show_till',NULL],['>=',UserNightdriveTrainingRequest::tableName().'.active_show_till',date("Y-m-d")]]);
    }elseif($this->listType=='history'){
      $query->andWhere(['<',UserNightdriveTrainingRequest::tableName().'.active_show_till',date("Y-m-d")]);
    }
    if($this->requested_date!=null){
      //Check if its range
      if(strpos($this->requested_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->requested_date);
        $query->andWhere(['and',['>=',UserNightdriveTrainingRequest::tableName().'.requested_date',$start_date],['<=',UserNightdriveTrainingRequest::tableName().'.requested_date',$end_date]]);
      }else{
        $query->andWhere([UserNightdriveTrainingRequest::tableName().'.requested_date'=>$this->requested_date]);
      }
    }
    if($this->created_at!=null){
      //Check if its range
      if(strpos($this->created_at," - ")){
        list($start_date,$end_date)=explode(" - ",$this->created_at);
        $query->andWhere(['and',['>=',UserNightdriveTrainingRequest::tableName().'.created_at',$start_date],['<=',UserNightdriveTrainingRequest::tableName().'.created_at',$end_date]]);
      }else{
        $query->andWhere([UserNightdriveTrainingRequest::tableName().'.created_at'=>$this->created_at]);
      }
    }

    return $dataProvider;
  }

  public function getTabItems()
  {
    return Yii::$app->helperFunctions->getRequestTabs($this);
  }
}
