<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BoatPurpose;

/**
* BoatPurposeSearch represents the model behind the search form of `app\models\BoatPurpose`.
*/
class BoatPurposeSearch extends BoatPurpose
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['title','descp','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['title'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = BoatPurpose::find()
    ->select([
      'id',
      'title',
      'descp',
      'status',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like','title',$this->title])
    ->andFilterWhere(['like','descp',$this->descp]);

    return $dataProvider;
  }
}
