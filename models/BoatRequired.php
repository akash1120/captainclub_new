<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%boat_required}}".
*
* @property integer $id
* @property integer $city_id
* @property string $marina_id
* @property string $date
* @property string $time_id
* @property integer $no_of_boats
* @property integer $boats_booked
* @property string $comments
* @property string $boat_type
*/
class BoatRequired extends ActiveRecord
{
  public $boat_id,$tag_id;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_required}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['city_id', 'marina_id', 'date', 'time_id', 'no_of_boats'], 'required'],
    	[['city_id','marina_id','time_id','no_of_boats','boats_booked'],'integer'],
    	[['date','comments','boat_type'],'string'],
      [['boat_id','tag_id'],'each','rule'=>['integer']],
      ['boat_id', 'required', 'when' => function ($model) {
          return $model->boat_type == 'byboat';
      }, 'whenClient' => "function (attribute, value) {
          return $('#boatrequired-boat_type').val() == 'byboat';
      }"],
      ['tag_id', 'required', 'when' => function ($model) {
          return $model->boat_type == 'bytag';
      }, 'whenClient' => "function (attribute, value) {
          return $('#boatrequired-boat_type').val() == 'bytag';
      }"],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'city_id' => Yii::t('app', 'City'),
      'marina_id' => Yii::t('app', 'Marina'),
      'date' => Yii::t('app', 'Date'),
      'time_id' => Yii::t('app', 'Time'),
      'no_of_boats' => Yii::t('app', 'No Of Boats'),
      'boats_booked' => Yii::t('app', 'Booked Boats'),
      'comments' => Yii::t('app', 'Comments'),
      'bookingsDone' => 'Boats Booked',
      'boat_type' => 'Look For',
      'boat_id' => 'Specify Boats',
      'tag_id' => 'Specify Tags',
      'boatsByIdz' => 'Boats',
      'boatsByTagz' => 'Tags',
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCity()
  {
    return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPort()
  {
    return $this->hasOne(Marina::className(), ['id' => 'marina_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTimeZone()
  {
    return $this->hasOne(TimeSlot::className(), ['id' => 'time_id']);
  }

  public function getBookingsDone()
  {
    $html='None';
    $results=BoatRequiredBooking::find()->innerJoin("booking","booking.id=boat_required_booking.booking_id")->where(['request_id'=>$this->id])->all();//,'trashed'=>0
    if($results!=null){
      $html='<table class="table table-bordered">';
      foreach($results as $result){
        $boatName=$result->booking->boat->name;
        if($this->date>=date("Y-m-d")){
          $finalBoatName='<a href="javascript:;" class="assign-booking" data-booking_id="'.$result->booking_id.'">'.$boatName.'</a>';
        }else{
          $finalBoatName=$boatName;
        }
        $assignment=AdminBookingAssign::find()->select(['id'])->where(['booking_id'=>$result->booking_id])->asArray()->one();
        if($assignment!=null){
          $finalBoatName='<span class="badge badge-success"><i class="fa fa-check"></i></span> '.$boatName;
        }
        if($result->booking->trashed==1){
          $finalBoatName.=' <small><span class="badge badge-danger">(Booking Deleted)</span></small>';
        }
        $html.='<tr><td class="nosd">'.$finalBoatName.'</td></tr>';
      }
      $html.='</table>';
    }
    return $html;
  }

  public function getBoatsByIdz()
  {
    $boats=[];
    if($this->boat_type=='byboat'){
      $boatIdz=BoatRequiredByBoat::find()->select(['boat_id'])->where(['boat_required_id'=>$this->id]);
      $results=Boat::find()->where(['id'=>$boatIdz])->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $boats[]='<span class="badge badge-info">'.$result['name'].'</span>';
        }
      }
    }
    return implode(" ",$boats);
  }

  public function getBoatsByTagz()
  {
    $tags=[];
    if($this->boat_type=='bytag'){
      $TagIdz=BoatRequiredByTag::find()->select(['tag_id'])->where(['boat_required_id'=>$this->id]);
      $results=BoatTag::find()->where(['id'=>$TagIdz])->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $tags[]='<span class="badge badge-info">'.$result['title'].'</span>';
        }
      }
    }
    return implode(" ",$tags);
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
		if($insert){
      //Insert Specified Boats
      if($this->boat_type=='byboat' && $this->boat_id!=null){
        BoatRequiredByBoat::deleteAll(['and', 'boat_required_id = :boat_required_id', ['not in', 'boat_id', $this->boat_id]], [':boat_required_id' => $this->id]);
        foreach($this->boat_id as $key=>$val){
          $childRow=BoatRequiredByBoat::find()->where(['boat_required_id'=>$this->id,'boat_id'=>$val])->one();
          if($childRow==null){
            $childRow=new BoatRequiredByBoat;
            $childRow->boat_required_id=$this->id;
            $childRow->boat_id=$val;
            $childRow->save();
          }
        }
      }

      //Insert Specified Tags
      if($this->boat_type=='bytag' && $this->tag_id!=null){
        BoatRequiredByTag::deleteAll(['and', 'boat_required_id = :boat_required_id', ['not in', 'tag_id', $this->tag_id]], [':boat_required_id' => $this->id]);
        foreach($this->tag_id as $key=>$val){
          $childRow=BoatRequiredByTag::find()->where(['boat_required_id'=>$this->id,'tag_id'=>$val])->one();
          if($childRow==null){
            $childRow=new BoatRequiredByTag;
            $childRow->boat_required_id=$this->id;
            $childRow->tag_id=$val;
            $childRow->save();
          }
        }
      }

      $this->searchAndBookBoats();
    }
		parent::afterSave($insert, $changedAttributes);
  }

  public function searchAndBookBoats()
  {
    //Check and book
    $boatId=null;
    $tagBoatIdz=null;
    $timeSlotIdz=null;
    if($this->boat_type=='byboat'){
      $boatId=BoatRequiredByBoat::find()->select(['boat_id'])->where(['boat_required_id'=>$this->id]);
    }
    if($this->boat_type=='bytag'){
      $tagIdz=BoatRequiredByTag::find()->select(['tag_id'])->where(['boat_required_id'=>$this->id]);
      $tagBoatIdz=BoatToTags::find()->select(['boat_id'])->where(['tag_id'=>$tagIdz]);
    }
    $timeSlotIdz=BoatToTimeSlot::find()->select(['boat_id'])->where(['time_slot_id'=>$this->time_id]);

    list($sy,$sm,$sd)=explode("-",$this->date);
    $dayOfWeek=date("w",mktime(0,0,0,$sm,$sd,$sy));
    $availableOnDay=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);


    $boats=Boat::find()
    ->where(['id'=>$timeSlotIdz,'city_id'=>$this->city_id,'port_id'=>$this->marina_id,'status'=>1])
    ->andFilterWhere(['id'=>$boatId])
    ->andFilterWhere(['id'=>$tagBoatIdz])
    ->andFilterWhere(['id'=>$availableOnDay])
    ->asArray()->all();
    if($boats!=null){
      foreach($boats as $boat){
        $count = BoatRequiredBooking::find()->where(['request_id'=>$this->id])->count('id');
        if($count<$this->no_of_boats){
          $checkFree=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->marina_id,'boat_id'=>$boat['id'],'booking_date'=>$this->date,'booking_time_slot'=>$this->time_id,'status'=>1,'trashed'=>0]);
          if(!$checkFree->exists()){
            $booking=new Booking;
            $booking->booking_source='boat-required';
            $booking->user_id=Yii::$app->params['boatRequiredBookUserId'];
            $booking->city_id=$this->city_id;
            $booking->port_id=$this->marina_id;
            $booking->boat_id=$boat['id'];
            $booking->booking_date=$this->date;
            $booking->booking_time_slot=$this->time_id;
            $booking->booking_type=Yii::$app->params['boatRequiredBookType'];
            $booking->booking_comments=$this->comments;
            if($booking->save()){
              //Save into request sub table
              $requiredBoatBooking=new BoatRequiredBooking;
              $requiredBoatBooking->request_id=$this->id;
              $requiredBoatBooking->booking_id=$booking->id;
              $requiredBoatBooking->save();
              $booking->sendAdminBoatAllocated();
            }else{
              echo $boat['id'];
              echo '<pre>';print_r($booking->getErrors());echo '</pre>';
              die();
            }
          }
        }
      }
    }
  }

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Boat Required ('.$this->date.') trashed successfully');
		return true;
	}
}
