<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WaterSportEquipment;

/**
* WaterSportEquipmentSearch represents the model behind the search form about `app\models\WaterSportEquipment`.
*/
class WaterSportEquipmentSearch extends WaterSportEquipment
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'city_id', 'port_id', 'qty', 'created_by', 'updated_by','pageSize'], 'integer'],
      [['title', 'created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = WaterSportEquipment::find()
    ->select([
      WaterSportEquipment::tableName().'.id',
      'image',
      'title',
      WaterSportEquipment::tableName().'.city_id',
      'city_name'=>City::tableName().".name",
      'port_id',
      'marina_name'=>Marina::tableName().".name",
      'qty',
      WaterSportEquipment::tableName().'.status',
    ])
    ->innerJoin(City::tableName(),City::tableName().".id=".WaterSportEquipment::tableName().".city_id")
    ->innerJoin(Marina::tableName(),Marina::tableName().".id=".WaterSportEquipment::tableName().".port_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $this->load($params);

    $query->andFilterWhere([
      WaterSportEquipment::tableName().'.id' => $this->id,
      WaterSportEquipment::tableName().'.city_id' => $this->city_id,
      WaterSportEquipment::tableName().'.port_id' => $this->port_id,
      'qty' => $this->qty,
      WaterSportEquipment::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['like', WaterSportEquipment::tableName().'.title', $this->title]);

    return $dataProvider;
  }
}
