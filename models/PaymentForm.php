<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\helpers\PayTabs;

/**
* PaymentForm is the model behind the new order form.
*/
class PaymentForm extends Model
{
  public $credits,$order_id,$save_info,$use_already;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['credits'], 'required'],
      [['credits','save_info','use_already'], 'integer'],
      ['save_info','default','value'=>0],
      ['order_id','string'],
      [['credits'], 'checkMinimum'],
    ];
  }

    /**
     * checks the unique domain
     */
  public function checkMinimum($attribute, $params)
  {
    //Check end date is after start date
    if($this->credits<Yii::$app->helperFunctions->minimumCredits){
      $this->addError($attribute,Yii::t('app', 'Minimum '.Yii::$app->helperFunctions->minimumCredits.' can be pruchased'));
      return false;
    }
    return true;
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'credits' => 'Amount',
      'save_info' => 'Save Info for future usage',
      'use_already' => 'Already Saved Card',
    ];
  }

  /**
  * Generate Order
  *
  * @return boolean whether the company is saved
  */
  public function save()
  {
    if ($this->validate()) {
      $payTabsOptions = Yii::$app->paytabsHelperFunctions->paytabOptions;
      
      $userId=Yii::$app->user->identity->id;
      $orderId=$userId.Yii::$app->helperFunctions->generateToken(20);
      $credits=$this->credits;
      $amount=Yii::$app->paytabsHelperFunctions->calculateFinalAmount($credits);
      $profit=Yii::$app->paytabsHelperFunctions->calculateProfitAmount($credits);
      $this->order_id=$orderId;
      $order=new Order;
      $order->user_id=$userId;
      $order->order_id=$orderId;
      $order->amount_payable=$amount;
      $order->qty=$credits;
      $order->profit=$profit;
      $order->payment_date=date('Y-m-d H:i:s');
      $order->detail='Purchase of '.$credits.' credits';
      $order->currency=$payTabsOptions['currency'];
      $order->use_saved_cc_id=($this->use_already!=null && $this->use_already>0 ? $this->use_already : 0);
      if($order->save()){
        if($this->save_info==1){
          $connection = \Yii::$app->db;
      		$connection->createCommand(
            "update ".UserProfileInfo::tableName()." set save_cc='1' where user_id='".$userId."'"
          )->execute();
        }
        return true;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $this->addError('',$val);
              }
            }
          }
        }
        return false;
      }
    } else {
      return false;
    }
  }

  public function UseAlreadyCard()
  {
    $order = Order::findOne(['order_id'=>$this->order_id,'status'=>'pending']);
    if($order!=null){
      $savedInfo=UserSavedCc::find()->where(['user_id'=>Yii::$app->user->identity->id,'id'=>$order->use_saved_cc_id])->asArray()->one();
      if($savedInfo!=null){
        $payTabsOptions = Yii::$app->paytabsHelperFunctions->paytabOptions;
        list($firstName,$lastName)=explode(" ",$savedInfo['customer_name']);

        $pt = new PayTabs($payTabsOptions['merchentEmail'], $payTabsOptions['secretKey']);
        $result = $pt->create_tokenized_payment(array(
          'merchant_email' => $payTabsOptions['merchentEmail'],
          'secret_key' => $payTabsOptions['secretKey'],
          'title' => $order->createdBy->username,
          'cc_first_name' => $firstName,
          'cc_last_name' => $lastName,
          "order_id" => $order->order_id,
          "product_name" => $order->createdBy->username,
          'customer_email' => $savedInfo['customer_email'],
          'amount' => $order->amount_payable,
          'pt_token' => $savedInfo['pt_token'],
          'pt_customer_email' => $savedInfo['customer_email'],
          'pt_customer_password' => $savedInfo['pt_customer_password'],
          'billing_shipping_details' => 'no',
          'currency' => $order->currency,
          'phone_number' => $savedInfo['customer_phone'],
        ));
        if($result->response_code=='100'){
          $order->response_code = $result->response_code;
          $order->trans_date = date("Y-m-d H:i:s");
          $order->transaction_id = $result->transaction_id;
          $order->amount = $order->amount_payable;
          $order->status = 'done';
          $order->save();

          $crtransaction=new UserAccounts;
          $crtransaction->user_id=$order->user_id;
          $crtransaction->order_id=$order->id;
          $crtransaction->trans_type='cr';
          $crtransaction->descp=$order->detail;
          $crtransaction->account_id=0;
          if($order->amount_payable==$order->amount){
            $crtransaction->amount=$order->qty;
            $crtransaction->profit=$order->profit;
          }
          $crtransaction->booking_id=0;
          $crtransaction->save();
          return true;
        }else{
          $order->response_code = $result->response_code;
          $order->trans_date = date("Y-m-d H:i:s");
          $order->transaction_id = $result->transaction_id;
          $order->amount = $order->amount_payable;
          $order->status = 'rejected';
          $order->save();
          $this->addError('',Yii::t('app','Payment rejected'));
        }
      }
    }
    return false;
  }
}
