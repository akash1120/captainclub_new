<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PackageGroup;

/**
* PackageGroupSearch represents the model behind the search form of `app\models\PackageGroup`.
*/
class PackageGroupSearch extends PackageGroup
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','sort_order','package_id','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['title','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['title'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = PackageGroup::find()
    ->select([
      'id',
      'title',
      'sort_order',
      'status',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    if($this->package_id!=null){
      $subQueryPackage=PackageGroupToPackage::find()->select(['package_group_id'])->where(['package_id'=>$this->package_id]);
      $query->andFilterWhere(['id' => $subQueryPackage]);
    }

    $query->andFilterWhere(['like','title',$this->title]);

    return $dataProvider;
  }
}
