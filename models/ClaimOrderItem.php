<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%claim_order_item}}".
*
* @property integer $id
* @property integer $order_id
* @property integer $booking_id
* @property integer $main_booking_id
* @property string $amount
*/
class ClaimOrderItem extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%claim_order_item}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
			[['order_id','main_booking_id'], 'required'],
			[['order_id','booking_id','main_booking_id'], 'integer'],
			[['amount'], 'number'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
			'order_id' => Yii::t('app', 'Order'),
			'booking_id' => Yii::t('app', 'Operation Booking'),
			'main_booking_id' => Yii::t('app', 'Booking'),
			'amount' => Yii::t('app', 'Amount'),
    ];
  }

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getClaimOrder()
	{
		return $this->hasOne(ClaimOrder::className(), ['id' => 'order_id']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getBooking()
	{
		return $this->hasOne(Booking::className(), ['id' => 'main_booking_id']);
	}
}
