<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%boat_fuel_consumption_rate}}".
*
* @property integer $id
* @property integer $boat_id
* @property integer $type_id
* @property string $rate_value
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class BoatFuelConsumptionRate extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_fuel_consumption_rate}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id','type_id','rate_value'], 'required'],
      [['boat_id','type_id','created_by','updated_by'], 'integer'],
      [['rate_value'], 'string'],
      [['rate_value'],'trim'],
      [['created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_id' => Yii::t('app', 'Boat'),
      'type_id' => Yii::t('app', 'Title'),
      'rate_value' => Yii::t('app', 'Value'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }
}
