<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
* DashboardPlan is the model behind the Admin dashboard plan.
*/
class DashboardPlan extends Model
{
  public $forCity,$forDate,$tomorrowDate;
  public $bulkBookedBoatsForDate;

  /**
  * Creates a form model given a rssid.
  *
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  */
  public function __construct($config = [])
  {
    $this->tomorrowDate=date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")));
    if($this->forDate==null)$this->forDate=$this->tomorrowDate;
    if($this->forCity==null)$this->forCity=Yii::$app->appHelperFunctions->firstActiveCity['id'];
    parent::__construct($config);
  }

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['forCity','forDate'], 'required'],
      ['forDate', 'validateDate'],
    ];
  }

  /**
  * Validates the date.
  * This method serves as the inline validation for date.
  */
  public function validateDate($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->forDate<=date("Y-m-d")){
        $this->addError($attribute, 'Please select date in future');
      }
    }
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'forDate' => 'Date',
    ];
  }

  public function getMarinaList()
  {
    return Yii::$app->appHelperFunctions->getActiveCityMarinaList($this->forCity);
  }

  public function getTimeSlotList()
  {
    return Yii::$app->appHelperFunctions->timeSlotListDashboard;
  }

  public function getMarinaBulkBookedBoats()
  {
    $list=[];
    $marinas=$this->marinaList;
    if($marinas!=null){
      foreach($marinas as $marina){
        $marinaBoats=[];
        $subQueryBulkBookings=BulkBooking::find()->select(['boat_id'])
        ->where([
          'and',
          ['trashed'=>0],
          ['<=','start_date',$this->forDate],
          ['>=','end_date',$this->forDate]
        ]);
        $boats=Boat::find()->select(['id','name'])->where(['id'=>$subQueryBulkBookings,'port_id'=>$marina['id']])->asArray()->all();
        if($boats!=null){
          foreach($boats as $boat){
            $this->bulkBookedBoatsForDate[]=$boat['id'];
            $marinaBoats[]=['id'=>$boat['id'],'name'=>$boat['name']];
          }
          $list[]=[
            'id'=>$marina['id'],
            'name'=>$marina['name'],
            'boats'=>$marinaBoats,
          ];
        }
      }
    }
    return $list;
  }

  public function getTimeSlots()
  {
    $data=[];
    $marinas=$this->marinaList;
    $timeSlotList=$this->timeSlotList;
    if($timeSlotList!=null){
      foreach($timeSlotList as $timing){
        if($marinas!=null){
          foreach($marinas as $marina){
            $data[]=[
              'bg'=>'',
              'title'=>$marina['short_name'].' '.$timing['short_name'],
              'boat_required_request'=>$this->getBoatRequiredRequests($this->forCity,$marina['id'],$timing['id']),
              'admin_bookings'=>$this->getAdminBookings($this->forCity,$marina['id'],$timing['id']),
              'free_boats'=>$this->getFreeBoats($this->forCity,$marina['id'],$timing['id']),
              'bulk_bookings'=>$this->getBulkBookings($this->forCity,$marina['id'],$timing['id']),
              'user_requests'=>$this->getUserRequests($this->forCity,$marina['name'],$timing['dashboard_name']),
            ];
            if($timing['id']==2){
              //Any Marina
              $data[]=[
                'bg'=>'#d7d6d6',
                'title'=>$marina['short_name'].' Anytime (morning or afternoon)',
                'user_requests'=>$this->getUserRequests($this->forCity,$marina['name'],'Anytime (morning or afternoon)'),
              ];
            }
          }
        }
        //Any Marina
        $data[]=[
          'bg'=>'#d7d6d6',
          'title'=>'Any Marina'.' '.$timing['short_name'],
          'user_requests'=>$this->getUserRequests($this->forCity,'Any Marina',$timing['dashboard_name']),
        ];
        if($timing['id']==2){
          //Any Marina
          $data[]=[
            'bg'=>'#d7d6d6',
            'title'=>'Any Marina'.' Anytime (morning or afternoon)',
            'user_requests'=>$this->getUserRequests($this->forCity,'Any Marina','Anytime (morning or afternoon)'),
          ];
        }
      }
    }
    return $data;
  }

  public function getBoatRequiredRequests($city,$marina,$time)
  {
    $action=$this->forDate<date("Y-m-d") ? 'history' : 'index';
    $Required=BoatRequired::find()->where(['city_id'=>$city,'marina_id'=>$marina,'date'=>$this->forDate,'time_id'=>$time,'trashed'=>0])->sum("no_of_boats");
    $Booked=BoatRequired::find()->where(['city_id'=>$city,'marina_id'=>$marina,'date'=>$this->forDate,'time_id'=>$time,'trashed'=>0])->sum("boats_booked");
    $html = $Required>0 ? '<small><a href="'.Url::to(['boat-required/'.$action,'BoatRequiredSearch[city_id]'=>$city,'BoatRequiredSearch[marina_id]'=>$marina,'BoatRequiredSearch[date]'=>$this->forDate.' - '.$this->forDate,'BoatRequiredSearch[time_id]'=>$time]).'" data-pjax="0" target="_blank">Boat Required: '.$Booked.'/'.$Required.'</a><small>' : '';
    return $html;
  }

  public function getAdminBookings($city,$marina,$time)
  {
    $data=[];
    $bookings=Booking::find()
      ->select([
        'id'=>Booking::tableName().'.id',
        'booking_type',
        'booking_comments',
        'boat'=>Boat::tableName().'.name',
        'captain',
        'sport_eqp_id',
        'bbq'=>Booking::tableName().'.bbq',
        'is_bulk',
      ])
      ->innerJoin(Boat::tableName(),Booking::tableName().".boat_id=".Boat::tableName().".id")
      ->where([
        Booking::tableName().'.port_id'=>$marina,
        'booking_date'=>$this->forDate,
        'booking_time_slot'=>$time,
        //'is_bulk'=>0,
        Booking::tableName().'.trashed'=>0,
        'user_id'=>Yii::$app->appHelperFunctions->adminIdz,
      ])
      ->andFilterWhere(['not in','boat_id',$this->bulkBookedBoatsForDate])
      ->orderBy(['is_bulk'=>SORT_ASC])
      ->asArray()->all();
    if($bookings!=null){
      foreach($bookings as $booking){
        $addons='';
        if($booking['captain']==1)$addons.=($addons!='' ? ', ' : '').'Captain';
        if($booking['sport_eqp_id']>0)$addons.=($addons!='' ? ', ' : '').'Sports Equipment';
        if($booking['bbq']==1)$addons.=($addons!='' ? ', ' : '').'BBQ';
        $data[]=[
          'id'=>$booking['id'],
          'is_bulk'=>$booking['is_bulk'],
          'boat'=>$booking['boat'].($addons!='' ? ' <small style="color:red;display:inline-block;line-height:12px;"><b>Addons:</b> '.$addons.'</small>' : ''),
          'remarks'=>($booking['booking_type']!=null && isset(Yii::$app->helperFunctions->adminBookingTypes[$booking['booking_type']]) ? Yii::$app->helperFunctions->adminBookingTypes[$booking['booking_type']] : '').': '.$booking['booking_comments'],
        ];
      }
    }
    return $data;
  }

  public function getFreeBoats($city,$marina,$time)
  {
    $data=[];
    $dayOfWeek=Yii::$app->helperFunctions->WeekendDay($this->forDate);

    $subQueryTiming=BoatToTimeSlot::find()->select(['boat_id'])->where(['time_slot_id'=>$time]);
    $subQueryAvailability=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);
    //$subBulkBooked=BulkBooking::find()->select(['boat_id'])->where(['and',['trashed'=>0],['<=','start_date',$this->forDate],['>=','end_date',$this->forDate]]);
    $boats=Boat::find()
      ->where([
        'status'=>1,
        'port_id'=>$marina,
      ])
      ->andWhere(['id'=>$subQueryTiming])
      ->andWhere(['id'=>$subQueryAvailability])
      //->andWhere(['not in','id',$subBulkBooked])
      ->asArray()->all();
    if($boats!=null){
      foreach($boats as $boat){
        $existingBooking=Booking::find()
        ->where([
          'booking_date'=>$this->forDate,
          'boat_id'=>$boat['id'],
          'port_id'=>$boat['port_id'],
          'booking_time_slot'=>$time,
          'trashed'=>0,
        ])->asArray()->one();
        if($existingBooking!=null){
        }else{
          if(Yii::$app->appHelperFunctions->checkBoatAvailabilityByBulkBooking($boat['id'],$this->forDate)){

          }else{
            $data[]=['boat'=>$boat['name']];
          }
        }
      }
    }
    return $data;
  }

  public function getBulkBookings($city,$marina,$time)
  {
    $data=[];
    if($this->bulkBookedBoatsForDate!=null){
      $bookings=Booking::find()
        ->select([
          'id'=>Booking::tableName().'.id',
          'username',
          'firstname',
          'lastname',
          'boat_name'=>Boat::tableName().'.name',
          'booking_type',
          'booking_comments',
        ])
        ->innerJoin(Boat::tableName(),Boat::tableName().".id=".Booking::tableName().".boat_id")
        ->innerJoin(User::tableName(),User::tableName().".id=".Booking::tableName().".user_id")
        ->where([
          Booking::tableName().'.port_id'=>$marina,
          'booking_date'=>$this->forDate,
          'booking_time_slot'=>$time,
          'is_bulk'=>0,
          Booking::tableName().'.trashed'=>0,
        ])
        ->andFilterWhere(['boat_id'=>$this->bulkBookedBoatsForDate])
        ->asArray()->all();
      if($bookings!=null){
        foreach($bookings as $booking){
          $data[]=[
            'id'=>$booking['id'],
            'username'=>$booking['username'],
            'fullname'=>$booking['firstname'].($booking['lastname']!='' ? ' '.$booking['lastname'] : $booking['username']),
            'boat'=>$booking['boat_name'],
            'booking_type'=>$booking['booking_type'],
            'booking_comments'=>$booking['booking_comments'],
          ];
        }
      }
    }
    return $data;
  }

  public function getUserRequests($city,$marina,$time)
  {
    //echo $cityId.','.$portName.','.$timeSlot.'<hr />';
    $data=[];
    $requests=UserBookingRequest::find()
      ->select([
        UserBookingRequest::tableName().'.id',
        UserBookingRequest::tableName().'.user_id',
        'username'=>User::tableName().'.username',
        'firstname'=>User::tableName().'.firstname',
        'lastname'=>User::tableName().'.lastname',
        'status'=>UserBookingRequest::tableName().'.status',
        'descp'=>UserBookingRequest::tableName().'.descp',
      ])
      ->innerJoin(User::tableName(),User::tableName().".id=".UserBookingRequest::tableName().".user_id")
      ->where([
        UserBookingRequest::tableName().'.city_id'=>$city,
        UserBookingRequest::tableName().'.marina'=>$marina,
        UserBookingRequest::tableName().'.requested_date'=>$this->forDate,
        UserBookingRequest::tableName().'.time_slot'=>$time,
        UserBookingRequest::tableName().'.trashed'=>0,
      ])
      ->orderBy(['id'=>SORT_DESC])
      ->asArray()->all();
      //var_dump($requests->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
      //echo '<hr />';
    if($requests!=null){
      foreach($requests as $request){
        $status=Yii::$app->helperFunctions->requestStatusIcon[$request['status']];

        $isPending=true;
        if($request['status']!=0){
          $isPending=false;
        }else{
          $isAssigned = LogUserRequestToBoatAssign::find()->where(['request_type'=>'boatbooking','request_id'=>$request['id']]);
          if($isAssigned->exists()){
            $isPending=false;
          }
        }
        $data[]=[
          'id'=>$request['id'],
          'user_id'=>$request['user_id'],
          'username'=>$request['username'],
          'firstname'=>$request['firstname'],
          'lastname'=>$request['lastname'],
          'status'=>$request['status'],
          'txtStatus'=>$status,
          'descp'=>$request['descp'],
          'isPending' => $isPending
        ];
      }
    }
    return $data;
  }
}
