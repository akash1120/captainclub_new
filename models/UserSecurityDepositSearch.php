<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserSecurityDeposit;

/**
* UserSecurityDepositSearch represents the model behind the search form of `app\models\UserSecurityDeposit`.
*/
class UserSecurityDepositSearch extends UserSecurityDeposit
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','user_id','pageSize'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = UserSecurityDeposit::find()
    ->select([
      'id',
      'descp',
      'trans_type',
      'amount',
      'created_at',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
			'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions

    $query->andFilterWhere([
      'user_id' => $this->user_id,
    ]);

    return $dataProvider;
  }

  public function getRowBalanceAmount($rowId)
  {
    $totalAmtToPaySoFar=self::find()->where(['and',['<=','id',$rowId],['trans_type'=>'dr','user_id'=>$this->user_id]])->sum('amount');
    $totalAmtPaidSoFar=self::find()->where(['and',['<=','id',$rowId],['trans_type'=>'cr','user_id'=>$this->user_id]])->sum('amount');
    $balance=($totalAmtPaidSoFar-$totalAmtToPaySoFar);
    $html = '<span class="badge grid-badge badge-'.($balance<0 ? 'danger' : 'success').'">AED '.$balance.'</span>';
    return $html;
  }

  public function getTabItems()
  {
    return Yii::$app->helperFunctions->getUserPaymentsTabs($this->user_id);
  }
}
