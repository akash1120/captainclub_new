<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%request_inspection_items}}".
*
* @property integer $id
* @property integer $inspection_id
* @property integer $item_id
* @property string $item_condition
* @property string $item_comments
*/
class RequestInspectionItems extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%request_inspection_items}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
        [['inspection_id', 'item_id'], 'required'],
        [['inspection_id', 'item_id'], 'integer'],
        [['item_condition', 'item_comments'], 'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
        'id' => Yii::t('app', 'ID'),
        'inspection_id' => Yii::t('app', 'Inspection ID'),
        'item_id' => Yii::t('app', 'Item ID'),
        'item_condition' => Yii::t('app', 'Item Condition'),
        'item_comments' => Yii::t('app', 'Item Comments'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getItem()
  {
      return $this->hasOne(InspectionItems::className(), ['id' => 'item_id']);
  }
}
