<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%user_note_user}}".
*
* @property integer $id
* @property integer note_id
* @property integer $staff_id
*/
class UserNoteUser extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_note_user}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['note_id','staff_id'], 'required'],
      [['note_id','staff_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'note_id' => Yii::t('app', 'Task'),
      'staff_id' => Yii::t('app', 'Staff Member'),
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getNote()
  {
    return $this->hasOne(UserNote::className(), ['id' => 'note_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getStaff()
  {
    return $this->hasOne(User::className(), ['id' => 'staff_id']);
  }
}
