<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* AddSecurityDeposit is the model behind the member add security deposit form.
*/
class AddSecurityDeposit extends Model
{
  public $user_id,$amount,$deposit_type,$descp;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['user_id','amount','deposit_type','descp'], 'required'],
      ['user_id','integer'],
      ['amount','number'],
      [['deposit_type','descp'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'amount' => 'Amount',
      'deposit_type' => 'Type',
      'descp' => 'Comments',
    ];
  }

  /**
  * Save amount to user account
  */
  public function save()
  {
    if ($this->validate()) {
      //Creating Cr Transaction
      $crtransaction=new UserSecurityDeposit;
      $crtransaction->user_id=$this->user_id;
      $crtransaction->trans_type='cr';
      $crtransaction->descp=$this->descp;
      $crtransaction->amount=$this->amount;
      $crtransaction->deposit_type=$this->deposit_type;
      $crtransaction->save();
      return true;
    }
    return false;
  }
}
