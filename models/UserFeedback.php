<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%user_feedback}}".
*
* @property integer $id
* @property integer $user_id
* @property string $descp
* @property integer $rating
*/
class UserFeedback extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_feedback}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','descp'],'required'],
      [['user_id','rating','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['descp'],'string'],
      ['rating','default','value'=>0],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'rating' => 'Rating',
      'descp' => 'Comments or any other request',
      'created_at' => 'Date & Time',
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMember()
  {
	   return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return string, name of user
   */
  public function getMemberName()
  {
    if($this->member!=null){
	   return $this->member->fullname;
    }
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      if($this->rating==0){
        Yii::$app->mailer->compose(['html' => 'requestSuggestion-html', 'text' => 'requestSuggestion-text'], ['model' => $this])
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
        ->setReplyTo($this->member->email)
        ->setTo(Yii::$app->appHelperFunctions->getSetting('suggestionEmail'))
        ->setCc([Yii::$app->appHelperFunctions->getSetting('suggestionEmail2'),Yii::$app->params['icareEmail'],Yii::$app->params['feedbackCc2']])
        ->setSubject('New Feedback - ' . Yii::$app->params['siteName'])
        ->send();
      }else{
        Yii::$app->mailer->compose(['html' => 'feedbackManual-html', 'text' => 'feedbackManual-text'], ['model' => $this])
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
        ->setReplyTo($this->member->email)
        ->setTo(Yii::$app->appHelperFunctions->getSetting('suggestionEmail'))
        ->setCc([Yii::$app->appHelperFunctions->getSetting('suggestionEmail2'),Yii::$app->params['icareEmail']])
        ->setSubject('New Feedback - ' . Yii::$app->params['siteName'])
        ->send();
      }

      $templateId=Yii::$app->appHelperFunctions->getSetting('feedback_response');
      $template=EmailTemplate::findOne($templateId);
      if($template!=null){
        $vals = [
          '{captainName}' => $this->memberName,
        ];
        $htmlBody=$template->searchReplace($template->template_html,$vals);
        $textBody=$template->searchReplace($template->template_text,$vals);
        $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
        ->setReplyTo(Yii::$app->appHelperFunctions->getSetting('suggestionEmail'))
        ->setSubject('Feedback/Suggestion Received.');

        $message->setTo($this->member->email);
        $message->send();
      }
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Feedback ('.$this->title.') trashed successfully');
		return true;
	}
}
