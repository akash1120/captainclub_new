<?php
namespace app\models;

use Yii;
use app\models\User;
use yii\base\Model;

/**
 * Forget Password form
 */
class ForgetPasswordForm extends Model
{
  public $email;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      ['email', 'filter', 'filter' => 'trim'],
      ['email', 'required'],
      ['email', 'email','message'=>'Email address is invalid'],
      [['email'], 'filter', 'filter' => 'trim'],
      ['email', 'exist',
        'targetClass' => '\app\models\User',
        'targetAttribute' => 'email',
        'filter' => ['status' => 1, 'trashed'=>0],
        'message' => 'The email address is incorrect'
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'email' => Yii::t('app', 'Email'),
    ];
  }

  /**
   * Sends an email with a link, for resetting the password.
   *
   * @return boolean whether the email was send
   */
  public function sendEmail()
  {
    /* @var $user User */
    $user = User::findOne([
      'status' => 1,
      'email' => $this->email,
      'trashed' => 0,
    ]);
    if ($user) {
      if ($user->resetInfo==null || !User::isPasswordResetTokenValid($user->passwordResetToken)) {
        $user->generatePasswordResetToken();
      }
      if ($user->save()) {
        return Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
          ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
          ->setTo($this->email)
          ->setSubject('Password reset for ' . Yii::$app->params['siteName'])
          ->send();
      }
    }
    return false;
  }
}
