<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%request_service}}".
*
* @property integer $id
* @property string $boat_id
* @property string $req_date
* @property string $current_hours
* @property string $scheduled_hours
* @property string $remarks
* @property integer $created_by
* @property string $created_at
* @property integer $updated_by
* @property string $updated_at
*/
class RequestService extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%request_service}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id', 'req_date', 'current_hours', 'scheduled_hours', 'remarks'], 'required'],
      [['remarks', 'admin_remarks'], 'string'],
      [['boat_id', 'status','completed', 'created_by', 'updated_by'], 'integer'],
      [['admin_action_date', 'completed_date', 'created_at', 'updated_at'], 'safe'],
      [['current_hours', 'scheduled_hours'], 'string', 'max' => 15],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'req_date' => Yii::t('app', 'Date'),
      'boat_id' => Yii::t('app', 'Boat'),
      'current_hours' => Yii::t('app', 'Current Hours'),
      'scheduled_hours' => Yii::t('app', 'Scheduled Hours'),
      'remarks' => Yii::t('app', 'Remarks'),
      'status' => Yii::t('app', 'Status'),
      'admin_remarks' => Yii::t('app', 'Remarks'),
      'admin_action_date' => Yii::t('app', 'Date'),
      'created_by' => Yii::t('app', 'Created By'),
      'created_at' => Yii::t('app', 'Created At'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBoat()
  {
	   return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus($status)
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Service Request marked '.Yii::$app->operationHelperFunctions->requestStatus[$status].' successfully');
		return true;
	}
}
