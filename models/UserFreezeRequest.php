<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%user_freeze_request}}".
*
* @property integer $id
* @property string $title
* @property string $code
* @property string $text_color
* @property integer $status
*/
class UserFreezeRequest extends ActiveRecord
{
  public $noofdays;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_freeze_request}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','start_date','noofdays'],'required'],
      [['user_id','noofdays','status','freeze_finish_alert','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['start_date','end_date','requested_date','active_show_till'],'string'],
      ['status','default','value'=>1],
      [['start_date'], 'validateFreeze'],
    ];
  }

  /**
  * Validates freeze date & days
  */
  public function validateFreeze($attribute, $params)
  {
    $alreadyFreezed=false;
    $noofdays=$this->noofdays;

    $currentUser=$this->member;
    $activeContract=$currentUser->activeContract;
    if($activeContract!=null){
      $renewalReminder=UserNote::find()->where(['user_id'=>$this->member->id,'reminder_type'=>1,'is_feedback'=>1])->one();

      $myContractFreezingStats=Yii::$app->statsFunctions->getFreezeDaysByContract($this->member->active_contract_id);
      $remainingFreezeDays=$myContractFreezingStats['remaining'];

      $userIdz=$activeContract->memberIdz;

      $contractEnd=$activeContract->end_date;

      $freeze_start_date=$this->start_date;
      list($sy,$sm,$sd)=explode("-",$freeze_start_date);
      $freeze_end_date=date("Y-m-d",mktime(0,0,0,$sm,($sd+$noofdays),$sy));

      //Check existing freeze
      $anyFreezeRequest=self::find()->where(['and',['status'=>1,'trashed'=>0],['in','created_by',$userIdz],['>=','end_date',date("Y-m-d")]]);
      if($anyFreezeRequest->exists()){
        $this->addError($attribute,Yii::t('app', 'You already have an active freeze, You can not request freeze untill that period is passed'));
        return false;
      }

      //Check existing booking
      $booking=Booking::find()->where(['and',['status'=>1,'trashed'=>0],['between', 'booking_date', $freeze_start_date, $freeze_end_date],['in','user_id',$userIdz]]);
      if($booking->exists()){
        $this->addError($attribute,Yii::t('app', 'You have an active booking in the selected period, Please delete that booking first.'));
        return false;
      }

      //Check number of days are equal or less then allowed
      $freeeDaysDiff=strtotime($freeze_end_date)-strtotime($freeze_start_date);
      $freeeDays=ceil(abs($freeeDaysDiff) / 86400);
      if(($freeeDays)>$remainingFreezeDays){
        $this->addError($attribute,Yii::t('app', 'Duration of freeze can not be more than '.$remainingFreezeDays.' days'));
        return false;
      }

      //Check Package end day
      if($contractEnd<=$freeze_start_date){
        $this->addError($attribute,Yii::t('app', 'Freeze period should be before renewal date'));
        return false;
      }
    }else{
      $this->addError($attribute,Yii::t('app', 'You dont have any active contract'));
      return false;
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'Member'),
      'noofdays' => 'I would like to freeze my membership for',
      'start_date' => 'Starting from',
      'end_date' => Yii::t('app', 'End Date'),
      'requested_date' => Yii::t('app', 'Requested Date'),
      'active_show_till' => Yii::t('app', 'Show Untill'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMember()
  {
	   return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  public function sendFreezeFinishAlert()
  {
    //Freeze Finish Member Alert
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_freezeopenuser_u');
    $template=EmailTemplate::findOne($templateId);
    if($template!=null){

      $vals = [
        '{resetlink}' => '<a href="'.Yii::$app->params['siteUrl'].Yii::$app->params['resetUrl'].'">'.Yii::$app->params['siteUrl'].Yii::$app->params['resetUrl'].'</a>',
        '{loginlink}' => Yii::$app->params['siteUrl'],
        '{username}' => $this->member->fullname,
        '{email}' => $this->member->email,
        '{freezeStart}' => Yii::$app->formatter->asDate($this->start_date),
        '{freezeEnd}' => Yii::$app->formatter->asDate($this->end_date),
        '{packageName}' => $this->member->activeContract->package->name,
      ];
      $htmlBody=$template->searchReplace($template->template_html,$vals);
      $textBody=$template->searchReplace($template->template_text,$vals);
      Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setTo($this->member->email)
        ->setSubject('Account Freeze Period finishing ' . Yii::$app->params['siteName'])
        ->send();
      $this->freeze_finish_alert=1;
      $this->save();
    }

    //Freeze Finish Admin Alert
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_freezeopenuser_a');
    $template=EmailTemplate::findOne($templateId);
    if($template!=null){
      $vals = [
        '{loginlink}' => Yii::$app->params['siteUrl'],
        '{username}' => $this->fullname,
        '{email}' => $this->email,
        '{freezeStart}' => Yii::$app->formatter->asDate($this->freeze_start),
        '{freezeEnd}' => Yii::$app->formatter->asDate($this->freeze_end),
        '{packageName}' => (($this->package_id!=null && $this->package_id>0) ? $this->package->name : 'No Package'),
      ];
      $htmlBody=$template->searchReplace($template->template_html,$vals);
      $textBody=$template->searchReplace($template->template_text,$vals);
      Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName'] . ' robot'])
        ->setTo(Yii::$app->controller->getSetting('adminEmail'))
        ->setSubject('Account Freeze Period finishing ' . Yii::$app->params['siteName'])
        ->send();
    }
  }

  /**
  * @inheritdoc
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      $noofdays=$this->noofdays;
      $this->requested_date=$this->start_date;
      $this->active_show_till=$this->start_date;
      $currentUser=$this->member;
      $activeContract=$currentUser->activeContract;
      if($activeContract!=null){
        $myContractFreezingStats=Yii::$app->statsFunctions->getFreezeDaysByContract($currentUser->active_contract_id);
        $remainingFreezeDays=$myContractFreezingStats['remaining'];

        $userIdz=$activeContract->memberIdz;

        $contractEnd=$activeContract->end_date;

        $freeze_start_date=$this->start_date;
        list($sy,$sm,$sd)=explode("-",$freeze_start_date);
        $freeze_end_date=date("Y-m-d",mktime(0,0,0,$sm,($sd+$noofdays),$sy));
        $this->end_date=$freeze_end_date;
      }
      return true;
    } else {
      return false;
    }
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{//1 month before expiry feedback
    if($insert){
      $connection = \Yii::$app->db;
      $noofdays=$this->noofdays;
      $currentUser=$this->member;
      $activeContract=$currentUser->activeContract;

      $remaining_freeze_days=$activeContract->remaining_freeze_days-$noofdays;

      //Updating Current Contract
      list($cey,$cem,$ced)=explode("-",$activeContract->end_date);
      $activeContractEndDate=date("Y-m-d",mktime(0,0,0,$cem,($ced+$noofdays),$cey));
      $userMaxEndDate=$activeContractEndDate;

  		$connection->createCommand(
        "update ".Contract::tableName()." set remaining_freeze_days=:remaining_freeze_days,end_date=:end_date where id=:id",
        [
          ':remaining_freeze_days'=>$remaining_freeze_days,
          ':end_date'=>$activeContractEndDate,
          ':id'=>$activeContract->id,
        ]
      )
      ->execute();

      //Updating Renewed Contract
      if($activeContract->renewed_contract_id>0){
        $renewedContract=Contract::findOne($activeContract->renewed_contract_id);
        if($renewedContract->start_date<$activeContractEndDate){
          $renewedContractStartDate=date("Y-m-d", strtotime("+1 day", strtotime($activeContractEndDate)));
          $differenceOfDays=Yii::$app->helperFunctions->getNumberOfDaysBetween($renewedContract->start_date,$activeContractEndDate);
          $differenceOfDays++;
          //list($rcsy,$rcsm,$rcsd)=explode("-",$renewedContract->start_date);
          //$renewedContractStartDate=date("Y-m-d",mktime(0,0,0,$rcsm,($rcsd+$noofdays),$rcsy));

          list($rcey,$rcem,$rced)=explode("-",$renewedContract->end_date);
          $renewedContractEndDate=date("Y-m-d",mktime(0,0,0,$rcem,($rced+$differenceOfDays),$rcey));
          $userMaxEndDate=$renewedContractEndDate;

      		$connection->createCommand(
            "update ".Contract::tableName()." set start_date=:start_date,end_date=:end_date where id=:id",
            [
              ':start_date'=>$renewedContractStartDate,
              ':end_date'=>$renewedContractEndDate,
              ':id'=>$activeContract->renewed_contract_id,
            ]
          )
          ->execute();
        }
      }

      //Updating User End Date
      $connection->createCommand(
        "update ".User::tableName()." set end_date=:end_date where id=:id",
        [
          ':end_date'=>$userMaxEndDate,
          ':id'=>$currentUser->id,
        ]
      )
      ->execute();


      //Send Emails
      Yii::$app->mailer->compose(['html' => 'requestFreeze-html', 'text' => 'requestFreeze-text'], ['model' => $this])
      ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
      ->setReplyTo($currentUser->email)
      ->setTo(Yii::$app->params['icareEmail'])
      ->setCc([Yii::$app->params['mdEmail']])
      ->setSubject('New Request Type Freeze - ' . Yii::$app->params['siteName'])
      ->send();

      $templateId=Yii::$app->appHelperFunctions->getSetting('auto_freeze_response');
      $template=EmailTemplate::findOne($templateId);
      if($template!=null){
        $vals = [
          '{captainName}' => $currentUser->fullname,
          '{freezePeriod}' => 'from '.Yii::$app->formatter->asDate($this->start_date).' to '.Yii::$app->formatter->asDate($this->end_date),
        ];
        $htmlBody=$template->searchReplace($template->template_html,$vals);
        $textBody=$template->searchReplace($template->template_text,$vals);
        $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setSubject('Freeze Confirmation');

        foreach($this->member->activeContractMembers as $contractMember){
          $message->setTo($contractMember->email);
          $message->send();
        }
      }
    }
		parent::afterSave($insert, $changedAttributes);
	}

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
    if($this->trashed==0){
    $connection = \Yii::$app->db;
    //If Freeze is not finished, Pull back the days
    if($this->end_date>date("Y-m-d")){
      $currentUser=$this->member;
      $activeContract=$currentUser->activeContract;

      if($this->start_date<=date("Y-m-d")){
        $noOfDaysToPull=ceil((strtotime($this->end_date) - strtotime(date("Y-m-d")))/60/60/24)+1;
      }else{
        $noOfDaysToPull=ceil((strtotime($this->end_date) - strtotime($this->start_date))/60/60/24);
      }

      $remaining_freeze_days=$activeContract->remaining_freeze_days+$noOfDaysToPull;

      //Updating Current Contract
      $contractEndDate=$activeContract->end_date;
      list($cey,$cem,$ced)=explode("-",$contractEndDate);
      $activeContractEndDate=date("Y-m-d",mktime(0,0,0,$cem,($ced-$noOfDaysToPull),$cey));
      $userMaxEndDate=$activeContractEndDate;

  		$connection->createCommand(
        "update ".Contract::tableName()." set remaining_freeze_days=:remaining_freeze_days,end_date=:end_date where id=:id",
        [
          ':remaining_freeze_days'=>$remaining_freeze_days,
          ':end_date'=>$activeContractEndDate,
          ':id'=>$activeContract->id,
        ]
      )
      ->execute();

      //Updating Renewed Contract
      if($activeContract->renewed_contract_id>0){
        $renewedContract=Contract::findOne($activeContract->renewed_contract_id);
        if($renewedContract->start_date==date("Y-m-d", strtotime("+1 day", strtotime($contractEndDate)))){
          list($rcsy,$rcsm,$rcsd)=explode("-",$renewedContract->start_date);
          $renewedContractStartDate=date("Y-m-d",mktime(0,0,0,$rcsm,($rcsd-$noOfDaysToPull),$rcsy));

          list($rcey,$rcem,$rced)=explode("-",$renewedContract->end_date);
          $renewedContractEndDate=date("Y-m-d",mktime(0,0,0,$rcem,($rced-$noOfDaysToPull),$rcey));
          $userMaxEndDate=$renewedContractEndDate;

      		$connection->createCommand(
            "update ".Contract::tableName()." set start_date=:start_date,end_date=:end_date where id=:id",
            [
              ':start_date'=>$renewedContractStartDate,
              ':end_date'=>$renewedContractEndDate,
              ':id'=>$activeContract->renewed_contract_id,
            ]
          )
          ->execute();
        }
      }

      //Updating User End Date
      $connection->createCommand(
        "update ".User::tableName()." set end_date=:end_date where id=:id",
        [
          ':end_date'=>$userMaxEndDate,
          ':id'=>$currentUser->id,
        ]
      )
      ->execute();
    }
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Freeze Request ('.$this->member->fullname.') trashed successfully');
    }
		return true;
	}
}
