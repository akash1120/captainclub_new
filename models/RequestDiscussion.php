<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%request_discussion}}".
*
* @property integer $id
* @property string $request_type
* @property integer $request_id
* @property string $comments
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class RequestDiscussion extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%request_discussion}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
        [['request_id','comments'], 'required'],
        [['created_at', 'updated_at', 'trashed_at'], 'safe'],
        [['created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
        [['request_type','comments'], 'string']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'request_id' => Yii::t('app', 'Request'),
      'comments' => Yii::t('app', 'Comments'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'trashed' => Yii::t('app', 'Trashed'),
      'trashed_at' => Yii::t('app', 'Trashed At'),
      'trashed_by' => Yii::t('app', 'Trashed By'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  public function getRequest()
  {
    if($this->request_type=='freeze'){
      return UserFreezeRequest::findOne($this->request_id);
    }
    if($this->request_type=='boatbooking'){
      return UserBookingRequest::findOne($this->request_id);
    }
    if($this->request_type=='nightdrivetraining'){
      return UserNightdriveTrainingRequest::findOne($this->request_id);
    }
    if($this->request_type=='upgradecity'){
      return UserCityUpgradeRequest::findOne($this->request_id);
    }
    if($this->request_type=='other'){
      return UserOtherRequest::findOne($this->request_id);
    }
  }

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Priority ('.$this->title.') trashed successfully');
		return true;
	}
}
