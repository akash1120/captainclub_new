<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Boat;

/**
* BoatSearch represents the model behind the search form about `app\models\Boat`.
*/
class BoatSearch extends Boat
{
  public $listType,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','tag_id','city_id','port_id','watersports','bbq','rank','status','special_boat','wake_boarding','wake_surfing','listType','pageSize'], 'integer'],
      [['name'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = Boat::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort'=> ['defaultOrder' => ['rank'=>SORT_ASC]]
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'city_id' => $this->city_id,
      'port_id' => $this->port_id,
      'rank' => $this->rank,
      'watersports' => $this->watersports,
      'bbq' => $this->bbq,
      'show_in_list' => $this->show_in_list,
      'special_boat' => $this->special_boat,
      'wake_boarding' => $this->wake_boarding,
      'wake_surfing' => $this->wake_surfing,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    if($this->tag_id!=null){
      $boatToTags=BoatToTags::find()->select(['boat_id'])->where(['tag_id'=>$this->tag_id]);
      $query->andWhere(['id'=>$boatToTags]);
    }

    $query->andFilterWhere(['like', 'name', $this->name])
    ->andFilterWhere(['like', 'descp', $this->descp]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    return '
    <li class="nav-item'.($this->listType==0 ? ' active' : '').'">
      <a class="nav-link" href="'.Url::to(['index','listType'=>0]).'">Normal Boats</a>
    </li>
    <li class="nav-item'.($this->listType==1 ? ' active' : '').'">
      <a class="nav-link" href="'.Url::to(['index','listType'=>1]).'">Special Boats</a>
    </li>';
  }
}
