<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
* BookingForm is the model behind the booking search.
*/
class BookingForm extends Model
{
  public $city_id,$port_id,$date,$booking_type,$booking_comments,$pageSize;
  public $totalSessions,$availableSessions;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['city_id','date'], 'required'],
      [['city_id','date','booking_type','booking_comments'], 'required', 'on'=>['admbook']],
      [['city_id','port_id','booking_type','pageSize'],'integer'],
      [['date','booking_comments'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'city_id' => 'City',
      'date' => 'Date',
      'booking_type' => 'Type',
      'booking_comments' => 'Comments',
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    if($this->load($params) && $this->city_id!=null){
      if($this->port_id==null){
        $this->port_id=Yii::$app->appHelperFunctions->getFirstActiveCityMarina($this->city_id)['id'];
      }
      $query = Boat::find();
      // grid filtering conditions
      $query->andFilterWhere([
        'city_id' => $this->city_id,
        'port_id' => $this->port_id,
        'status' => 1,
        'trashed' => 0,
      ]);
    }else{
      $query = Boat::find()
      ->asArray()
      ->where(['id'=>-1]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort'=> ['defaultOrder' => ['rank'=>SORT_ASC]],
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    $marinas=Yii::$app->appHelperFunctions->getActiveCityMarinaList($this->city_id);
    $html='';
    if($marinas!=null){
      foreach($marinas as $marina){
        $totalSessions=0;
        $availableSessions=0;
        $ndAllowedLimitInMarina=$marina['night_drive_limit'];

        if($this->port_id==null)$this->port_id=$marina['id'];

        $tabUrl=Url::to(['create','BookingForm[city_id]'=>$this->city_id,'BookingForm[port_id]'=>$marina['id'],'BookingForm[date]'=>$this->date]);

        $models = Boat::find()->where([
          'city_id' => $this->city_id,
          'port_id' => $marina['id'],
          'status' => 1,
          'trashed' => 0,
        ])->orderBy(['special_boat'=>SORT_DESC,'rank'=>SORT_ASC])->all();
        //Loop Boats
        foreach ($models as $model) {
          $boatIsAvailableOnDay=Yii::$app->appHelperFunctions->checkBoatAvailability($model['id'],$this->date);
          $bulkBooked=Yii::$app->appHelperFunctions->checkBoatAvailabilityByBulkBooking($model['id'],$this->date);
          if($bulkBooked==true){
            $boatIsAvailableOnDay=false;
          }
          $timings=$model->timings;
          if($timings!=null){
            foreach($timings as $timing){
              if($boatIsAvailableOnDay!=false){
                $alreadyBooking=Booking::find()
                ->where([
                  'booking_date'=>$this->date,
                  'city_id'=>$this->city_id,
                  'port_id'=>$marina['id'],
                  'boat_id'=>$model['id'],
                  'booking_time_slot'=>$timing['id'],
                  'is_archive'=>0,
                  'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,
                  'trashed'=>0,
                ]);
                if(!$alreadyBooking->exists()){
                  if($model['special_boat']==1){
                    $availableSessions++;
                  }else{
                    $showOption=true;
                    if($timing['id']==Yii::$app->params['nightDriveTimeSlot']){
                      //Today check if there is any night drive.
                      if($this->date==date("Y-m-d")){
                        $alreadyHaveNightDrive=Booking::find()->where(['port_id'=>$marina['id'],'booking_date'=>$this->date,'booking_time_slot'=>Yii::$app->params['nightDriveTimeSlot'],'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'trashed'=>0,'is_bulk'=>0]);
                        if(!$alreadyHaveNightDrive->exists()){
                          $showOption=false;
                        }
                      }

                      //If Night Drive and limit already reached
                      $bookedCount=Booking::find()->where(['port_id'=>$marina['id'],'booking_date'=>$this->date,'booking_time_slot'=>Yii::$app->params['nightDriveTimeSlot'],'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'trashed'=>0])->count('id');
                      if($bookedCount>=$ndAllowedLimitInMarina){
                        $showOption=false;
                      }

                      //Check if this boat is already selected by special boat
                      $subQueryActiveBooking=Booking::find()->select(['id'])->where(['port_id'=>$marina['id'],'booking_date'=>$this->date,'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'trashed'=>0]);
                      $checkBooked=BookingSpecialBoatSelection::find()->where(['booking_id'=>$subQueryActiveBooking,'boat_id'=>$model['id']])->asArray()->one();
                      if($checkBooked!=null){
                        $showOption=false;
                      }
                    }
                    if($showOption==true){
                      $availableSessions++;
                    }
                  }
                  $totalSessions++;
                }else{
                  $totalSessions++;
                }
              }else{
                $totalSessions++;
              }
            }
          }
        }

        if($this->port_id==$marina['id']){
          $this->totalSessions=$totalSessions;
          $this->availableSessions=$availableSessions;
        }

        $html.='
        <li class="nav-item'.($this->port_id==$marina['id'] ? ' active' : '').'">
          <a class="nav-link" href="'.$tabUrl.'"><i class="fa fa-anchor"></i> '.$marina['short_name'].' ('.$availableSessions.' available out of '.$totalSessions.')</a>
        </li>';
      }
    }
    return $html;
  }
}
