<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* RequestFreeze is the model behind the Freeze Request.
*/
class RequestFreeze extends Model
{
  public $start_date,$noofdays;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['start_date','noofdays'], 'required'],
      ['noofdays','integer'],
      [['start_date'], 'string'],
      [['start_date'], 'validateFreeze'],
    ];
  }

  /**
  * Validates freeze date & days
  */
  public function validateFreeze($attribute, $params)
  {
    $alreadyFreezed=false;

    $currentUser=Yii::$app->user->identity;
    $activeContract=$currentUser->activeContract;
    if($activeContract!=null){
      $myContractFreezingStats=Yii::$app->statsFunctions->getFreezeDaysByContract(Yii::$app->user->identity->active_contract_id);
      $remainingFreezeDays=$myContractFreezingStats['remaining'];

      $userIdz=$activeContract->memberIdz;

      $contractEnd=$activeContract->end_date;

      $freeze_start_date=$this->start_date;
      list($sy,$sm,$sd)=explode("-",$freeze_start_date);
      $freeze_end_date=date("Y-m-d",mktime(0,0,0,$sm,($sd+$noofdays),$sy));

      //Check existing freeze
      $anyFreezeRequest=UserRequests::find()->where(['and',['item_type'=>'freeze','status'=>1,'trashed'=>0],['in','created_by',$userIdz],['>=','freeze_end',date("Y-m-d")]]);
      if($anyFreezeRequest->exists()){
        $this->addError($attribute,Yii::t('app', 'You already have an active freeze, You can not request freeze untill that period is passed'));
        return false;
      }

      //Check existing booking
      $booking=Booking::find()->where(['and',['status'=>1,'trashed'=>0],['between', 'booking_date', $freeze_start_date, $freeze_end_date],['in','user_id',$userIdz]]);
      if($booking->exists()){
        $this->addError($attribute,Yii::t('app', 'You have an active booking in the selected period, Please delete that booking first.'));
        return false;
      }

      //Check number of days are equal or less then allowed
      $freeeDaysDiff=strtotime($freeze_end_date)-strtotime($freeze_start_date);
      $freeeDays=ceil(abs($freeeDaysDiff) / 86400);
      if(($freeeDays+1)>$remainingFreezeDays){
        $this->addError($attribute,Yii::t('app', 'Duration of freeze can not be more than '.$remainingFreezeDays.' days'));
        return false;
      }

      //Check Package end day
      if($contractEnd<=$freeze_start_date){
        $this->addError($attribute,Yii::t('app', 'Freeze period should be before renewal date'));
        return false;
      }
    }else{
      $this->addError($attribute,Yii::t('app', 'You dont have any active contract'));
      return false;
    }
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'noofdays' => 'I would like to freeze my membership for',
      'start_date' => 'Starting from',
    ];
  }

  /**
  * Save credits to user account
  */
  public function save()
  {
    if ($this->validate()) {
      $currentUser=Yii::$app->user->identity;
      $activeContract=$currentUser->activeContract;
      if($activeContract!=null){
        $myContractFreezingStats=Yii::$app->statsFunctions->getFreezeDaysByContract(Yii::$app->user->identity->active_contract_id);
        $remainingFreezeDays=$myContractFreezingStats['remaining'];

        $userIdz=$activeContract->memberIdz;

        $contractEnd=$activeContract->end_date;

        $freeze_start_date=$this->start_date;
        list($sy,$sm,$sd)=explode("-",$freeze_start_date);
        $freeze_end_date=date("Y-m-d",mktime(0,0,0,$sm,($sd+$noofdays),$sy));

        /*

        $freeeDaysDiff=strtotime($this->end_date)-strtotime($this->start_date);
        $freeeDays=ceil(abs($freeeDaysDiff) / 86400);
        $this->oldEndDate = $currentUser->packageEndDate;
        list($y,$m,$d)=explode("-",$this->oldEndDate);
        $freeeDays+=1;
        $this->newEndDate = date("Y-m-d",mktime(0,0,0,$m,($d+$freeeDays),$y));

        $connection = \Yii::$app->db;
        $connection->createCommand("update ".Contract::tableName()." set end_date='".$this->newEndDate."',remaining_freeze_days=(remaining_freeze_days-$freeeDays) where id='".$activePackage->contract->id."'")->execute();
*/

        $req=new UserRequests;
        $req->item_type='freeze';
        $req->freeze_start=$freeze_start_date;
        $req->freeze_end=$freeze_end_date;
        $req->status=1;
        $req->requested_date=$freeze_start_date;
        $req->active_show_till=$freeze_start_date;
        $req->save();

        /*Yii::$app->mailer->compose(['html' => 'requestFreeze-html', 'text' => 'requestFreeze-text'], ['request' => $this])
        ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
        ->setReplyTo($currentUser->email)
        ->setTo(Yii::$app->params['icareEmail'])
        ->setCc([Yii::$app->params['mdEmail']])
        ->setSubject('New Request Type Freeze - ' . Yii::$app->params['siteName'])
        ->send();

        $templateId=Yii::$app->controller->getSetting('auto_freeze_response');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          $vals = [
            '{captainName}' => $currentUser->username,
            '{freezePeriod}' => 'from '.date("l, jS M Y",strtotime($this->start_date)).' to '.date("l, jS M Y",strtotime($this->end_date)),
            '{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'/images/email_logo.png" alt="'.Yii::$app->params['siteName'].'">',
          ];
          $htmlBody=$template->searchReplace($template->template_html,$vals);
          $textBody=$template->searchReplace($template->template_text,$vals);
          $message=Yii::$app->mailer->compose()
          ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
          ->setSubject('Freeze Confirmation')
          ->setHtmlBody($htmlBody)
          ->setTextBody($textBody);

          $members=User::find()->select('email')->where(['in','id',$userIdz])->all();
          if($members!=null){
            foreach($members as $member){
              $message->setTo($member->email);
              $message->send();
            }
          }
        }*/
      }
      return true;
    }
    return false;
  }
}
