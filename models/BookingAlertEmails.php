<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%booking_alert_emails}}".
*
* @property int $id
* @property int $alert_id
* @property int $booking_id
*/
class BookingAlertEmails extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_alert_emails}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['alert_id', 'booking_id'], 'required'],
      [['alert_id', 'booking_id'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'alert_id' => Yii::t('app', 'Alert ID'),
      'booking_id' => Yii::t('app', 'Booking ID'),
    ];
  }

  /**
  * Get boat row
  * @return \yii\db\ActiveQuery
  */
  public function getAlert()
  {
    return $this->hasOne(BookingAlert::className(), ['id' => 'alert_id']);
  }

  /**
  * Get time slot row
  * @return \yii\db\ActiveQuery
  */
  public function getBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }
}
