<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
* BoatPurposeAssignmentForm is the model behind the boat purpose assignment and boat name update
*/
class BoatPurposeAssignmentForm extends Model
{
  public $marina_id,$boat_id,$boat_name,$boat_purpose_id;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['boat_name','boat_purpose_id'], 'safe'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'boat_name' => 'Boat Name',
      'boat_purpose_id' => 'Boat Purpose',
    ];
  }

  /**
  * @return boolean whether the model passes validation
  */
  public function save()
  {
    if ($this->validate()) {
      $boats=Boat::find()->where(['port_id'=>$this->marina_id,'status'=>1,'trashed'=>0])->asArray()->all();
      if($boats!=null){
        $connection = \Yii::$app->db;
        foreach($boats as $boat){
          $boatName=$this->boat_name[$boat['id']];
          if($boatName=='')$boatName=$boat['name'];
          $boatPurposeId=$this->boat_purpose_id[$boat['id']];
          if($boatPurposeId=='')$boatPurposeId=$boat['boat_purpose_id'];

          $connection->createCommand(
            "update ".Boat::tableName()." set
            name=:name,
            boat_purpose_id=:boat_purpose_id
             where id=:id",
            [
              ':name'=>$boatName,
              ':boat_purpose_id'=>$boatPurposeId,
              ':id'=>$boat['id'],
            ]
          )
          ->execute();
        }
      }
      return true;
    }
    return false;
  }
}
