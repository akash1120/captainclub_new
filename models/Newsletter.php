<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%newsletter}}".
*
* @property integer $id
* @property string $title
* @property string $code
* @property string $text_color
* @property integer $status
*/
class Newsletter extends ActiveRecord
{
  public $package_idz,$city_idz;
  public $ccAdminId,$attachmentFile,$attachmentFileCaption;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%newsletter}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['subject'], 'required'],
      [['email_from', 'cc_admin_idz','keyword','username', 'credit_balance','expiry_month', 'message', 'password_hash', 'email', 'end_date'], 'string'],
      [['is_licensed', 'package_id', 'city_id', 'status','tasks'], 'integer'],
      [['package_idz','city_idz'], 'safe'],
      [['attachmentFile', 'attachmentFileCaption'], 'each', 'rule'=>['string']],
      [['ccAdminId'],'each','rule'=>['integer']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'subject' => Yii::t('app', 'Subject'),
      'message' => Yii::t('app', 'Message'),
      'UniqueOpens' => Yii::t('app', 'Unique Opens'),
      'uniqueClicks' => Yii::t('app', 'Unique Clicks'),
      'ccAdminId' => Yii::t('app', 'Cc.'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @inheritdoc
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      if($this->ccAdminId!=null){
        $this->cc_admin_idz=implode(",",$this->ccAdminId);
      }
      return true;
    } else {
      return false;
    }
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    //Saving Package Idz
    if($this->package_idz!=null){
      $packageIdz=explode(",",$this->package_idz);
      NewsletterPackage::deleteAll(['and', 'newsletter_id = :newsletter_id', ['not in', 'package_id', $packageIdz]], [':newsletter_id' => $this->id]);
      foreach($packageIdz as $key=>$val){
        $childRow=NewsletterPackage::find()->where(['newsletter_id'=>$this->id,'package_id'=>$val])->one();
        if($childRow==null){
          $childRow=new NewsletterPackage;
          $childRow->newsletter_id=$this->id;
          $childRow->package_id=$val;
          $childRow->save();
        }
      }
    }
    //Saving City Idz
    if($this->city_idz!=null){
      $cityIdz=explode(",",$this->city_idz);
      NewsletterCity::deleteAll(['and', 'newsletter_id = :newsletter_id', ['not in', 'city_id', $cityIdz]], [':newsletter_id' => $this->id]);
      foreach($cityIdz as $key=>$val){
        $childRow=NewsletterCity::find()->where(['newsletter_id'=>$this->id,'city_id'=>$val])->one();
        if($childRow==null){
          $childRow=new NewsletterCity;
          $childRow->newsletter_id=$this->id;
          $childRow->city_id=$val;
          $childRow->save();
        }
      }
    }
    //Saving Attachment files
    if($this->attachmentFile!=null){
      $n=0;
      foreach($this->attachmentFile as $key=>$val){
        if($val!=null && file_exists(Yii::$app->params['temp_abs_path'].$val)){
          $tmpfile=pathinfo($val);
          $fileName=Yii::$app->fileHelperFunctions->generateName().".".$tmpfile['extension'];
          rename(Yii::$app->params['temp_abs_path'].$val,Yii::$app->params['newsletter_uploads_abs_path'].$fileName);
          $attRow=new NewsletterAttachment;
          $attRow->newsletter_id=$this->id;
          $attRow->file_title=$this->attachmentFileCaption[$n];
          $attRow->attachment_file=$fileName;
          $attRow->save();
        }
        $n++;
      }
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function getSentStatus()
  {
    $txtStatus='<span class="badge grid-badge badge-info">In Queue</span>';
    if($this->sent==1){
      $txtStatus='<span class="badge grid-badge badge-success">Sent</span>';
    }
    return $txtStatus;
  }

  public function getUniqueOpens()
  {
    return NewsletterEmailSent::find()->where(['and',['newsletter_id'=>$this->id],['>','opens',0]])->count('id');
  }

  public function getUniqueClicks()
  {
    return NewsletterEmailSent::find()->where(['and',['newsletter_id'=>$this->id],['>','clicks',0]])->count('id');
  }

  public function getSoftBounces()
  {
    return NewsletterEmailSent::find()->where(['and',['newsletter_id'=>$this->id],['>','soft_bounced',0]])->count('id');
  }

  public function getHardBounces()
  {
    return NewsletterEmailSent::find()->where(['and',['newsletter_id'=>$this->id],['>','hard_bounced',0]])->count('id');
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getAttachments()
  {
    return $this->hasMany(NewsletterAttachment::className(), ['newsletter_id' => 'id']);
  }

  /**
  * @return array attachments
  */
  public function getAttachmentInfo()
  {
    $arrFiles=[];
    $results=NewsletterAttachment::find()->where(['newsletter_id'=>$this->id])->asArray()->all();
    if($results!=null){
      foreach($results as $attachment){
        if($attachment['attachment_file']!=null && file_exists(Yii::$app->params['newsletter_uploads_abs_path'].$attachment['attachment_file'])){
          $fpInfo=pathinfo(Yii::$app->params['newsletter_uploads_abs_path'].$attachment['attachment_file']);
          $icon='<img src="images/icons/'.$fpInfo['extension'].'.jpg" width="25" height="25" alt="" />';
          $arrFiles[]=[
            'icon'=>$icon,
            'caption'=>$attachment['file_title'],
            'linkText'=>$icon.' '.$attachment['file_title'],
            'link'=>'',//Yii::$app->params['siteUrl'].Yii::$app->params['newsletter_uploads_rel_path'].$attachment['attachment_file']
          ];
        }
      }
    }
    return $arrFiles;
  }

  public function getCcAdminEmails()
  {
    $html='';
    $cc_admin_idz=$this->cc_admin_idz;
    if($cc_admin_idz!=null){
      $idz=explode(",",$cc_admin_idz);
      foreach($idz as $key=>$val){
        $admin=User::find()->select(['username','email'])->where(['id'=>$val])->asArray()->one();
        if($admin!=null){
          if($html!='')$html.=', ';
          $html.=$admin['username'].'('.$admin['email'].')';
        }
      }
    }
    return $html;
  }
}
