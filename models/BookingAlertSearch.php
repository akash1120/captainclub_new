<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BookingAlert;

/**
* BookingAlertSearch represents the model behind the search form of `app\models\BookingAlert`.
*/
class BookingAlertSearch extends BookingAlert
{
  public $listType,$timingHtml,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','alert_type','port_id','timingHtml','is_sent','created_by','updated_by','pageSize'],'integer'],
      [['listType','message','alert_date','created_at','updated_at'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = BookingAlert::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'alert_type' => $this->alert_type,
      'alert_date' => $this->alert_date,
      'port_id' => $this->port_id,
      'is_sent' => $this->is_sent,
      'trashed' => 0,
    ]);

    if($this->timingHtml!=null){
      $subQueryTiming=BookingAlertTiming::find()->select(['alert_id'])->where(['time_slot_id'=>$this->timingHtml]);
      $query->andWhere(['id'=>$subQueryTiming]);
    }

    if($this->listType=='active'){
      $query->andWhere(['>=','DATE(alert_date)',date("Y-m-d")]);
    }else{
      $query->andWhere(['<','DATE(alert_date)',date("Y-m-d")]);
    }

    $query->andFilterWhere(['like','message',$this->message]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    $action=$this->alert_type==1 ? 'warning' : 'hold';
    return '
    <li class="nav-item'.($this->listType=='active' ? ' active' : '').'">
      <a class="nav-link" href="'.Url::to([$action,'listType'=>'active']).'">Active ('.Yii::$app->statsFunctions->getActiveAlertCount($this->alert_type).')</a>
    </li>
    <li class="nav-item'.($this->listType=='history' ? ' active' : '').'">
      <a class="nav-link" href="'.Url::to([$action,'listType'=>'history']).'">History ('.Yii::$app->statsFunctions->getHistoryAlertCount($this->alert_type).')</a>
    </li>';
  }
}
