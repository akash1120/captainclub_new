<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
* StatusForm is the model behind the user status form.
*/
class StatusForm extends Model
{
  public $user_id,$status,$date,$reason,$delete_booking,$delete_other_req;

  public $_user;

  /**
  * Creates a form model given a rssid.
  *
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  */
  public function __construct($id, $config = [])
  {
    $this->_user = User::findOne($id);
    $this->user_id = $this->_user->id;
    $this->date = date("Y-m-d",mktime(0,0,0,date("m"),(date("d")+1),date("Y")));

    parent::__construct($config);
  }

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['status','date','reason'], 'required'],
      [['status','delete_booking','delete_other_req'], 'integer'],
      [['date','reason'], 'string'],
      [['status'], 'checkExisting'],
    ];
  }

  /**
  * Validates the expiration date.
  * This method serves as the inline validation for expiry.
  */
  public function checkExisting($attribute, $params)
  {
    return true;
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'status' => 'Status',
      'date' => 'Start Date',
      'reason' => 'Reason',
      'delete_booking' => 'Delete all future bookings, starting this date',
      'delete_other_req' => 'Delete other request',
    ];
  }

  /**
  * @return boolean whether the model passes validation
  */
  public function save()
  {
    if ($this->validate()) {
      $connection = \Yii::$app->db;
      if($this->delete_booking==1){
        $connection->createCommand("update ".Booking::tableName()." set trashed='1',trashed_at='".date("Y-m-d H:i:s")."',trashed_by='".Yii::$app->user->identity->id."' where user_id=:user_id and booking_date>:date",[':user_id'=>$this->_user->id,':date'=>$this->date])->execute();
      }
      UserStatusReason::deleteAll(['and',['user_id'=>$this->_user->id,'status'=>$this->status],['>=','date',date("Y-m-d")]]);

      $result=new UserStatusReason;
      $result->user_id=$this->_user->id;
      $result->status=$this->status;
      $result->date=$this->date;
      $result->reason=$this->reason;
      $result->save();
      $user_status = $this->_user->status;
      if($this->date<=date("Y-m-d")){
        $user_status=$this->status;
        $activeContract=$this->_user->activeContract;
        if($activeContract!=null){
          $connection->createCommand("update ".ContractMember::tableName()." set status=:user_status where contract_id=:contract_id and user_id=:user_id",[':user_status'=>$user_status,':contract_id'=>$activeContract->id,':user_id'=>$this->_user->id])->execute();
          $connection->createCommand("update ".User::tableName()." set status=:user_status where id=:user_id",[':user_status'=>$user_status,':user_id'=>$this->_user->id])->execute();
        }
        $connection->createCommand("update ".UserStatusReason::tableName()." set is_updated=1 where id=:id",[':id'=>$result->id])->execute();
      }
      return true;
    }
    return false;
  }
}
