<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* WithdrawPaytabsForm is the model behind the paystab Withdrawal form.
*/
class WithdrawPaytabsForm extends Model
{
  public $credits,$descp;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['credits','descp'], 'required'],
      ['credits','number'],
      [['descp','descp'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'credits' => 'Amount',
      'descp' => 'Comments',
    ];
  }

  /**
  * Save credits to user account
  */
  public function save()
  {
    if ($this->validate()) {
      //Creating Cr Transaction
      $crtransaction=new UserAccounts;
      $crtransaction->user_id=0;
      $crtransaction->trans_type='dr';
      $crtransaction->descp=$this->descp;
      $crtransaction->account_id=11;
      $crtransaction->amount=$this->credits;
      $crtransaction->booking_id=0;
      $crtransaction->save();
      return true;
    }
    return false;
  }
}
