<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%boat_required_by_boat}}".
*
* @property integer $boat_required_id
* @property integer $boat_id
*/
class BoatRequiredByBoat extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_required_by_boat}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_required_id','boat_id'], 'required'],
      [['boat_required_id','boat_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_required_id' => Yii::t('app', 'Boat Required'),
      'boat_id' => Yii::t('app', 'Boat'),
    ];
  }
}
