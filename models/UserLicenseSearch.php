<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserLicense;

/**
* UserLicenseSearch represents the model behind the search form of `app\models\UserLicense`.
*/
class UserLicenseSearch extends UserLicense
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','user_id','city_id','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['license_start','license_expiry','created_at','updated_at','trashed','trashed_at'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = UserLicense::find()
    ->select([
      UserLicense::tableName().'.id',
      UserLicense::tableName().'.city_id',
      'city_name'=>City::tableName().'.name',
      User::tableName().'.username',
      User::tableName().'.firstname',
      User::tableName().'.lastname',
      UserLicense::tableName().'.license_image',
      UserLicense::tableName().'.license_start',
      UserLicense::tableName().'.license_expiry',
    ])
    ->innerJoin(City::tableName(),City::tableName().".id=".UserLicense::tableName().".city_id")
    ->innerJoin(User::tableName(),User::tableName().".id=".UserLicense::tableName().".user_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort' => ['defaultOrder'=>['license_expiry'=>SORT_DESC]],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      UserLicense::tableName().'.id' => $this->id,
      'user_id' => $this->user_id,
      UserLicense::tableName().'.license_start' => $this->license_start,
      UserLicense::tableName().'.license_expiry' => $this->license_expiry,
      UserLicense::tableName().'.trashed' => 0,
    ]);

    return $dataProvider;
  }
}
