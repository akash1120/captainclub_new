<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%operation_user_city_marina}}".
*
* @property integer $user_id
* @property integer $city_id
* @property integer $marina_id
*/
class OperationUserCityMarina extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%operation_user_city_marina}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','city_id','marina_id'], 'required'],
      [['user_id','city_id','marina_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'user_id' => Yii::t('app', 'User'),
      'city_id' => Yii::t('app', 'City'),
      'marina_id' => Yii::t('app', 'Marina'),
    ];
  }
}
