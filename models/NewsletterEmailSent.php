<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* NewsletterEmailSent
*/
class NewsletterEmailSent extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%newsletter_email_sent}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['newsletter_id','user_id','email_status'], 'required'],
      [['newsletter_id', 'user_id', 'email_status'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'softBouncedStatus' => Yii::t('app', 'Soft Bounced'),
      'hardBouncedStatus' => Yii::t('app', 'Hard Bounced'),
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getNewsletter()
  {
    return $this->hasOne(Newsletter::className(), ['id' => 'newsletter_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  public function getUsername()
  {
    $user=$this->user;
    if($user!=null){
      return $user->fullname;
    }
  }

  public function getEmail()
  {
    $user=$this->user;
    if($user!=null){
      $email=$user->email;
      $bounced=BlackListedEmail::find()->where(['email'=>$email]);
      return $email.($bounced->exists() ? ' <span class="label label-danger">Bounced</span>' : '');
    }
  }

  public function getSoftBouncedStatus()
  {
    if($this->soft_bounced==1){
      $reason='';
      $droppedEntry=DroppedEmail::find()->select(['reason'])->where(['email'=>$this->email])->asArray()->one();
      if($droppedEntry!=null && $droppedEntry['reason']!=null && $droppedEntry['reason']!=''){
        $reason='<br />'.$droppedEntry['reason'];
      }
      return '<span class="label label-warning">'.Yii::t('app','Soft Bounced').$reason.'</span>';
    }else{
      return '';
    }
  }

  public function getHardBouncedStatus()
  {
    if($this->hard_bounced==1){
      $reason='';
      $bouncedEntry=BlackListedEmail::find()->select(['reason'])->where(['email'=>$this->email])->asArray()->one();
      if($bouncedEntry!=null && $bouncedEntry['reason']!=null && $bouncedEntry['reason']!=''){
        $reason='<br />'.$bouncedEntry['reason'];
      }
      return '<span class="label label-danger">'.Yii::t('app','Hard Bounced').$reason.'</span>';
    }else{
      return '';
    }
  }
}
