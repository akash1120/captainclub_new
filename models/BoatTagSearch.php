<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BoatTag;

/**
* BoatTagSearch represents the model behind the search form of `app\models\BoatTag`.
*/
class BoatTagSearch extends BoatTag
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['title','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['title'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = BoatTag::find()
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      BoatTag::tableName().'.id' => $this->id,
      BoatTag::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['like',BoatTag::tableName().'.title',$this->title]);

    return $dataProvider;
  }
}
