<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Newsletter;

/**
* NewsletterSearch represents the model behind the search form about `app\models\Newsletter`.
*/
class NewsletterSearch extends Newsletter
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','sentStatus','opens','clicks','pageSize'], 'integer'],
      [['email_from','subject'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = Newsletter::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);


    $dataProvider->setSort([
      'defaultOrder' => ['id'=>SORT_DESC],
      'attributes' => [
        'id',
        'subject' => [
          'asc' => ["TRIM(subject)" => SORT_ASC,],
          'desc' => ["TRIM(subject)" => SORT_DESC,],
        ],
        'sentStatus' => [
          'asc' => ['sent' => SORT_ASC,],
          'desc' => ['sent' => SORT_DESC,],
        ],
        'opens',
        'clicks',
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
    ]);

    $query->andFilterWhere(['like', 'subject', $this->subject])
    ->andFilterWhere(['like', 'email_from', $this->email_from]);

    return $dataProvider;
  }
}
