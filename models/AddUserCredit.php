<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* AddUserCredit is the model behind the member add credit form.
*/
class AddUserCredit extends Model
{
  public $user_id,$credits,$descp;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['user_id','credits','descp'], 'required'],
      ['user_id','integer'],
      ['credits','number'],
      [['descp'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'credits' => 'Credits',
      'descp' => 'Comments',
    ];
  }

  /**
  * Save credits to user account
  */
  public function save()
  {
    if ($this->validate()) {
      //Creating Cr Transaction
      $crtransaction=new UserAccounts;
      $crtransaction->user_id=$this->user_id;
      $crtransaction->trans_type='cr';
      $crtransaction->descp=$this->descp;
      $crtransaction->account_id=0;
      $crtransaction->amount=$this->credits;
      $crtransaction->booking_id=0;
      $crtransaction->save();

      Yii::$app->mailer->compose(['html' => 'manualDepositConfirmation-html', 'text' => 'manualDepositConfirmation-text'], ['model' => $crtransaction])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setTo($crtransaction->member->email)
        ->setSubject('Deposit Confirmation - ' . Yii::$app->params['siteName'])
        ->send();
      return true;
    }
    return false;
  }
}
