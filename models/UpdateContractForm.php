<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Contract;

/**
* Update Contract form
*/
class UpdateContractForm extends Model
{
  public $captains_to_add,$captains_to_reduce;

  public $freeze_days_to_add,$freeze_days_to_reduce;

  public $days_to_add,$days_to_reduce;

  public $contract_id,$package_id,$package_name,$note;

  public $allowed_captains,$used_captains,$remaining_captains;
  public $allowed_freeze_days,$used_freeze_days,$remaining_freeze_days;
  public $contract_days,$passed_contract_days,$remaining_contract_days;

  private $_contract;
  public $start_date,$end_date;

  /**
  * Creates a form model given a rssid.
  *
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  */
  public function __construct($id, $config = [])
  {
    $this->_contract = Contract::findOne($id);

    $this->package_id=$this->_contract->package_id;
    $this->start_date=$this->_contract->start_date;
    $this->end_date=$this->_contract->end_date;
    $this->package_name=$this->_contract->package_name;

    $this->contract_days=Yii::$app->helperFunctions->getNumberOfDaysBetween($this->start_date,$this->end_date);
    $this->passed_contract_days=Yii::$app->helperFunctions->getNumberOfDaysBetween($this->start_date,date("Y-m-d"));
    $this->remaining_contract_days=Yii::$app->helperFunctions->getNumberOfDaysBetween(date("Y-m-d"),$this->end_date);

    $this->allowed_captains=$this->_contract->allowed_captains;
    $this->remaining_captains=$this->_contract->remainingCaptains;
    $this->used_captains=($this->allowed_captains-$this->remaining_captains);

    $this->allowed_freeze_days=$this->_contract->allowed_freeze_days;
    $this->remaining_freeze_days=$this->_contract->remainingFreezeDays;
    $this->used_freeze_days=($this->allowed_freeze_days-$this->remaining_freeze_days);

    parent::__construct($config);
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['package_id','note'], 'required'],
      [['contract_id','package_id','captains_to_add','captains_to_reduce','freeze_days_to_add','freeze_days_to_reduce','days_to_add','days_to_reduce'],'integer'],
      [['package_name','note'],'string'],
      [['days_to_add'],'checkRenewedPackage'],
    ];
  }

  /**
  * Validates the renewed date.
  */
  public function checkRenewedPackage($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->days_to_add>0){
        $canBeExtended=true;
        foreach($this->_contract->memberIdz as $key=>$val){
          if($canBeExtended==true){
            $renewedContract=ContractMember::find()
            ->innerJoin("contract",Contract::tableName().".id=".ContractMember::tableName().".contract_id")
            ->where([
              'and',
              ['user_id'=>$val],
              ['>','start_date',$this->_contract->end_date]
            ]);
            if($renewedContract->exists()){
              $canBeExtended=false;
            }
          }
        }

        if($canBeExtended==false){
          $this->addError('','You can not extend this contract, as member is already renewed');
          return false;
        }
      }
      return true;
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'package_id' => Yii::t('app', 'Package'),
      'member_idz' => Yii::t('app', 'Member'),
      'allowed_captains' => Yii::t('app', 'Allowed Captains'),
      'allowed_freeze_days' => Yii::t('app', 'Allowed Freeze Days'),
      'captains_to_add' => Yii::t('app', 'Add Captain(s)'),
      'captains_to_reduce' => Yii::t('app', 'Reduce Captain(s)'),
      'freeze_days_to_add' => Yii::t('app', 'Freeze Days to Extend'),
      'freeze_days_to_reduce' => Yii::t('app', 'Freeze Days to Reduce'),
      'days_to_add' => Yii::t('app', 'Days to Extend'),
      'days_to_reduce' => Yii::t('app', 'Days to Reduce'),
      'note' => Yii::t('app', 'Reason for update'),
    ];
  }

  /**
  * Saves the company details
  *
  * @return boolean whether the company is saved
  */
  public function save()
  {
    if ($this->validate()) {
      $contractHistory=new ContractChangeHistory;
      $contractHistory->contract_id=$this->_contract->id;
      $contractHistory->old_package_id=$this->_contract->package_id;
      $contractHistory->new_package_id=$this->package_id;
      $contractHistory->old_package_name=$this->_contract->package_name;
      $contractHistory->new_package_name=$this->package_name;
      $contractHistory->note=$this->note;
      $contractHistory->days_added=$this->days_to_add;
      $contractHistory->days_reduced=$this->days_to_reduce;
      $contractHistory->captains_added=$this->captains_to_add;
      $contractHistory->captains_reduced=$this->captains_to_reduce;
      $contractHistory->freeze_days_added=$this->freeze_days_to_add;
      $contractHistory->freeze_days_reduced=$this->freeze_days_to_reduce;
      $contractHistory->save();

      //Contract Expiry
      $oldEndDate=$this->end_date;
      $newEndDate=$oldEndDate;
      //Extention
      if($this->days_to_add>0){
        list($y,$m,$d)=explode("-",$oldEndDate);
        $newEndDate=date("Y-m-d",mktime(0,0,0,$m,($d+$this->days_to_add),$y));
      }
      //Reduction
      if($this->days_to_reduce>0){
        list($y,$m,$d)=explode("-",$oldEndDate);
        $newEndDate=date("Y-m-d",mktime(0,0,0,$m,($d-$this->days_to_reduce),$y));
      }

      //Captains
      $oldAllowedCaptains=$this->allowed_captains;
      $newAllowedCaptains=$oldAllowedCaptains;
      $oldRemainingCaptains=$this->remaining_captains;
      $newRemainingCaptains=$oldRemainingCaptains;
      //Add
      if($this->captains_to_add>0){
        $newAllowedCaptains+=$this->captains_to_add;
        $newRemainingCaptains+=$this->captains_to_add;
      }
      //Reduce
      if($this->captains_to_reduce>0){
        $newAllowedCaptains-=$this->captains_to_reduce;
        $newRemainingCaptains-=$this->captains_to_reduce;
      }

      //Freeze Days
      $oldAllowedFreezeDays=$this->allowed_freeze_days;
      $newAllowedFreezeDays=$oldAllowedFreezeDays;
      $oldRemainingFreezeDays=$this->remaining_freeze_days;
      $newRemainingFreezeDays=$oldRemainingFreezeDays;
      //Add
      if($this->freeze_days_to_add>0){
        $newAllowedFreezeDays+=$this->freeze_days_to_add;
        $newRemainingFreezeDays+=$this->freeze_days_to_add;
      }
      //Reduce
      if($this->freeze_days_to_reduce>0){
        $newAllowedFreezeDays-=$this->freeze_days_to_reduce;
        $newRemainingFreezeDays-=$this->freeze_days_to_reduce;
      }

      $contractHistory->old_allowed_captains=$oldAllowedCaptains;
      $contractHistory->old_remaining_captains=$oldRemainingCaptains;
      $contractHistory->new_allowed_captains=$newAllowedCaptains;
      $contractHistory->new_remaining_captains=$newRemainingCaptains;

      $contractHistory->old_allowed_freeze_days=$oldAllowedFreezeDays;
      $contractHistory->old_remaining_freeze_days=$oldRemainingFreezeDays;
      $contractHistory->new_allowed_freeze_days=$newAllowedFreezeDays;
      $contractHistory->new_remaining_freeze_days=$newRemainingFreezeDays;

      $contractHistory->old_end_date=$oldEndDate;
      $contractHistory->new_end_date=$newEndDate;
      $contractHistory->save();

      $contract=Contract::findOne($this->_contract->id);
      $contract->package_id=$this->package_id;
      $contract->package_name=$this->package_name;
      $contract->allowed_captains=$newAllowedCaptains;
      $contract->allowed_freeze_days=$newAllowedFreezeDays;

      $contract->remaining_captains=$newRemainingCaptains;
      $contract->remaining_freeze_days=$newRemainingFreezeDays;

      $contract->end_date=$newEndDate;
      $contract->save();
      return true;
    } else {
      return false;
    }
  }
}
