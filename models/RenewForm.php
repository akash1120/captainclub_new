<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
* Renew form
*/
class RenewForm extends Model
{
  public $package_id,$package_name,$start_date,$end_date,$allowed_captains,$allowed_freeze_days,$member_idz;

  public $_user;

  /**
  * Creates a form model given a rssid.
  *
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  */
  public function __construct($id, $config = [])
  {
    $this->_user = User::findOne($id);
    $activeContract = $this->_user->activeContract;
    $this->package_id=$this->_user->active_package_id;

    $renewableStatus=[0,3,4,5,20];
    if(in_array($this->_user->status,$renewableStatus)){
      $this->start_date=date("Y-m-d");
      $this->end_date=date("Y-m-d",mktime(0,0,0,date("m"),date("d"),(date("Y")+1)));
    }else{
      list($eY,$eM,$eD)=explode("-",$activeContract->end_date);
      $this->start_date=date("Y-m-d",mktime(0,0,0,$eM,($eD+1),$eY));
      $this->end_date=date("Y-m-d",mktime(0,0,0,$eM,($eD+1),($eY+1)));
    }
    if($activeContract!=null && $activeContract->package->hasMultiUser==true){
      $this->member_idz=$activeContract->memberIdz;
    }else{
      $this->member_idz=[$this->_user->id];
    }


    $this->allowed_captains=0;
    $this->allowed_freeze_days=0;

    parent::__construct($config);
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['package_id', 'start_date', 'end_date', 'allowed_captains', 'allowed_freeze_days'], 'required'],
      [['start_date', 'end_date'], 'safe'],
      [['package_id', 'allowed_captains', 'allowed_freeze_days'], 'integer'],
      [['package_name'], 'string'],
      [['start_date'], 'validateActiveExpiration'],
      [['start_date'], 'validateOverlapping'],
      [['start_date'], 'validateStartEndDate'],
      [['member_idz'], 'each', 'rule'=>['integer']],
    ];
  }

  /**
  * Validates the expiration date.
  * This method serves as the inline validation for expiry.
  */
  public function validateActiveExpiration($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $activeContract=$this->_user->activeContract;
      if($activeContract!=null){
        if($activeContract->renewed_contract_id>0){
          $this->addError('start_date','Member is already renewed');
          return false;
        }
        $checkIfAlreadyRenewed=Contract::find()
        ->innerJoin(ContractMember::tableName(),ContractMember::tableName().".contract_id=".Contract::tableName().".id and user_id='".$this->_user->id."'")
        ->where(['and',['status'=>1],['>','end_date',$activeContract->end_date]]);
        if($checkIfAlreadyRenewed->exists()){
          $this->addError('start_date','Member is already renewed');
          return false;
        }
      }
    }
  }

  /**
  * Validates the expiration date.
  * This method serves as the inline validation for expiry.
  */
  public function validateOverlapping($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $Overlaping=Contract::find()
      ->innerJoin(ContractMember::tableName(),ContractMember::tableName().".contract_id=".Contract::tableName().".id and user_id='".$this->_user->id."'")
      ->where(['and',['<=','start_date',$this->start_date],['>=','end_date',$this->start_date],['status'=>1]]);
      if($Overlaping->exists()){
        $this->addError('start_date',$this->_user->username.' already have an active contract in the selected period.');
        return false;
      }
    }
  }

  /**
  * Validates the expiration date.
  * This method serves as the inline validation for expiry.
  */
  public function validateStartEndDate($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->start_date>$this->end_date){
        $this->addError('start_date','Please provide valid end date.');
        return false;
      }
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'package_id' => Yii::t('app', 'Package'),
      'member_idz' => Yii::t('app', 'Member'),
      'start_date' => Yii::t('app', 'Start Date'),
      'end_date' => Yii::t('app', 'End Date'),
      'allowed_captains' => Yii::t('app', 'Allowed Captains'),
      'allowed_freeze_days' => Yii::t('app', 'Allowed Freeze Days'),
    ];
  }

  /**
  * Saves the company details
  *
  * @return boolean whether the company is saved
  */
  public function save()
  {
    if ($this->validate()) {

      $selectedPackage=Package::findOne($this->package_id);
      if($selectedPackage->hasMultiUser==false){
        $this->member_idz=[$this->_user->id];
      }
      $contract=new Contract;
      $contract->package_id=$this->package_id;
      $contract->package_name=$this->package_name;
      $contract->start_date=$this->start_date;
      $contract->end_date=$this->end_date;
      $contract->allowed_captains=$this->allowed_captains;
      $contract->remaining_captains=$this->allowed_captains;
      $contract->allowed_freeze_days=$this->allowed_freeze_days;
      $contract->remaining_freeze_days=$this->allowed_freeze_days;

      $contract->member_idz=$this->member_idz;
      if($contract->save()){
        
      }
      return true;
    } else {
      return false;
    }
  }
}
