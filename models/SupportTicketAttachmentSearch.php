<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SupportTicketAttachment;

/**
 * SupportTicketAttachmentSearch represents the model behind the search form about `app\models\SupportTicketAttachment`.
 */
class SupportTicketAttachmentSearch extends SupportTicketAttachment
{
  public $pageSize;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['ticket_ref_id','status','created_by','pageSize'], 'integer'],
      [['descp'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $this->load($params);
    $query = SupportTicketAttachment::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    $query->andFilterWhere([
      'created_by' => $this->created_by,
      'ticket_ref_id' => $this->ticket_ref_id,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like', 'descp', $this->descp]);

    return $dataProvider;
  }
}
