<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%user_device_notification}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $booking_id
 * @property string $user_device_id
 * @property string $json_data
 * @property string $notification_response
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class UserDeviceNotification extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%user_device_notification}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id','user_device_id'], 'required'],
      [['user_id', 'booking_id', 'created_by', 'updated_by'], 'integer'],
      [['user_device_id','json_data','notification_response'], 'string'],
      [['created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
      'booking_id' => Yii::t('app', 'Booking'),
      'user_device_id' => Yii::t('app', 'Device ID'),
      'json_data' => Yii::t('app', 'Data'),
      'notification_response' => Yii::t('app', 'Response'),
      'created_at' => Yii::t('app', 'Created At'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
          'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }
}
