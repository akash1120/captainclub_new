<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
* User model
*
* @property integer $id
* @property integer $user_type
* @property integer $permission_group_id
* @property integer $active_contract_id
* @property integer $active_package_id
* @property string $username
* @property string $firstname
* @property string $lastname
* @property string $email
* @property string $auth_key
* @property string $password_hash
* @property string $image
* @property integer $status
* @property string $start_date
* @property string $end_date
*/
class User extends ActiveRecord implements IdentityInterface
{
  public $file,$oldfile;
  public $licensefile,$oldlicensefile;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  public $new_password,$mobile,$city_id,$update_note,$is_licensed,$security_deposit,$deposit_type;
  public $marina_permits;
  public $license_image,$license_expiry;
  public $membership_number,$joining_date;

  public $city_license_start,$city_license_expiry,$city_license_image,$licImageFile;

  const STATUS_ACTIVE = '1';
  const STATUS_HOLD = '2';
  const STATUS_CANCEL = '3';
  const STATUS_PENDIND = '20';
  const STATUS_DELETED = '0';

  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user}}';
  }

  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
    ];
  }


  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_type' => Yii::t('app', 'Member Type'),
      'permission_group_id' => Yii::t('app', 'Permission Group'),
      'active_contract_id' => Yii::t('app', 'Active Contract'),
      'active_package_id' => Yii::t('app', 'Active Package'),
      'username' => Yii::t('app', 'Username'),
      'firstname' => Yii::t('app', 'First Name'),
      'lastname' => Yii::t('app', 'Last Name'),
      'email' => Yii::t('app', 'Email'),
      'active_package_id' => Yii::t('app', 'Package'),
      'permission_group_id' => Yii::t('app', 'Permission'),
      'permission_group' => Yii::t('app', 'Permission'),
      'city_name' => Yii::t('app', 'City'),
      'city_id' => Yii::t('app', 'City'),
      'image' => Yii::t('app', 'Photo'),
      'security_deposit' => Yii::t('app', 'Deposit'),
      'is_licensed' => Yii::t('app', 'Licensed'),
      'license_image' => Yii::t('app', 'License Image'),
      'license_expiry' => Yii::t('app', 'License Expiry'),
      'marina_permits' => Yii::t('app', 'Night Drive Permits'),
      'package_name' => Yii::t('app', 'Package'),
      'credit_balance' => Yii::t('app', 'Balance'),
      'end_date' => Yii::t('app', 'Expiry'),

      'city_license_start' => Yii::t('app', 'Start Date'),
      'city_license_expiry' => Yii::t('app', 'Expiry Date'),
      'city_license_image' => Yii::t('app', 'Photo'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['email', 'firstname', 'lastname', 'mobile', 'update_note'], 'required', 'on' => 'update'],
      [['email', 'firstname', 'lastname', 'mobile'], 'required', 'on' => 'new'],
      //['username','unique'],
      ['email','email'],
      ['email','unique'],
      [['firstname','lastname','new_password','update_note','deposit_type','membership_number'],'string'],
      [['username', 'firstname','lastname','email','mobile'], 'trim'],
      [['mobile','city_id','is_licensed'], 'integer'],
      [['security_deposit'], 'number'],
      [['note', 'group_id', 'password', 'subUser','start_date','end_date','joining_date','license_expiry','city_license_start','city_license_expiry','city_license_image'],'safe'],
      [['marina_permits'], 'each', 'rule' => ['integer']],
      [['image','license_image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB']
    ];
  }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => 1, 'trashed'=>0]);
    }

  /**
  * {@inheritdoc}
  */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    return static::findOne(['auth_key' => $token, 'status' => 1, 'trashed' => 0]);
  }

  /**
  * Finds user by username
  *
  * @param string $username
  * @return static|null
  */
  public static function findByUsername($username)
  {
    return static::find()->where(['and',['or',['username'=>$username],['email'=>$username]],['trashed'=>0]])->one();
  }

  /**
  * {@inheritdoc}
  */
  public function getId()
  {
      return $this->getPrimaryKey();
  }

  /**
  * {@inheritdoc}
  */
  public function getAuthKey()
  {
    return $this->auth_key;
  }

  /**
  * {@inheritdoc}
  */
  public function validateAuthKey($authKey)
  {
    return $this->auth_key === $authKey;
  }

  /**
  * Validates password
  *
  * @param string $password password to validate
  * @return bool if password provided is valid for current user
  */
  public function validatePassword($password)
  {
    return Yii::$app->security->validatePassword($password, $this->password_hash);
  }

  /**
   * Generates password hash from password and sets it to the model
   *
   * @param string $password
   */
  public function setPassword($password)
  {
    $this->password_hash = Yii::$app->security->generatePasswordHash($password);
  }

  /**
   * Generates "remember me" authentication key
   */
  public function generateAuthKey()
  {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  /**
   * Generates "login from backend" authentication key
   */
  public function generateVerifyToken()
  {
    $this->verify_token = Yii::$app->security->generateRandomString();
  }

  /**
   * Generates new password reset token
   */
  public function generatePasswordResetToken()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".UserReset::tableName()." set status=:status where user_id=:user_id",
      [
        ':status' => 10,
        ':user_id' => $this->id,
      ]
    )
    ->execute();

    $newResetInfo = new UserReset;
    $newResetInfo->user_id = $this->id;
    $newResetInfo->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    $newResetInfo->status = 0;
    $newResetInfo->save();
  }

  public function getPasswordResetToken()
  {
    $token = '';
    $resetInfo=UserReset::find()->where(['user_id'=>$this->id])->orderBy(['created_at'=>SORT_DESC])->one();
    if($resetInfo!=null){
      $token = $resetInfo->password_reset_token;
    }
    return $token;
  }

  /**
   * Finds user by password reset token
   *
   * @param string $token password reset token
   * @return static|null
   */
  public static function findByPasswordResetToken($token)
  {
    if (!static::isPasswordResetTokenValid($token)) {
      return null;
    }
    return UserReset::find()
    ->innerJoin("user",User::tableName().".id=".UserReset::tableName().".user_id")
    ->where([
      UserReset::tableName().'.password_reset_token' => $token,
      UserReset::tableName().'.status' => 0,
      User::tableName().'.status' => 1,
    ])->one();
  }

  /**
   * Finds out if password reset token is valid
   *
   * @param string $token password reset token
   * @return boolean
   */
  public static function isPasswordResetTokenValid($token)
  {
    if (empty($token) || $token==null) {
      return false;
    }
    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int) end($parts);
    return $timestamp + $expire >= time();
  }

  /**
   * return full name
   */
  public function getName()
  {
    $name = $this->firstname;//.($this->lastname!='' ? ' '.$this->lastname : '');
  	return (trim($name)=='' ? $this->username : $name);
  }

  /**
   * return full name
   */
  public function getFullName()
  {
  	return $this->firstLastName;
  }

  /**
   * return full name
   */
  public function getFirstLastName()
  {
    $name=$this->firstname.($this->lastname!='' ? ' '.$this->lastname : '');
    if($name==''){
      $name=$this->username;
    }
    if($name==''){
      $name=$this->email;
    }
  	return $name;
  }

  /**
   * return full name
   */
  public function getFullnameWithUsername()
  {
  	return $this->username.($this->firstname!='' ? ' - '.$this->firstname : '').($this->lastname!='' ? ' '.$this->lastname : '');
  }

  public function getPackageWithExpiry()
  {
    $html='';
    $activeContract=$this->activeContract;
    if($activeContract!=null){
      $html='<small>'.$activeContract->package->name.'</small><br /><span class="badge badge-warning">'.Yii::$app->formatter->asDate($activeContract->end_date).'</span>';
    }
    $credits=$this->profileInfo!=null ? $this->profileInfo->credit_balance : 0;
    $html.=($html!='' ? '<br />' : '').'<span class="badge badge-'.($credits<0 ? 'danger' : 'success').'">'.$credits.'</span>';
    return $html;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProfileInfo()
  {
	   return $this->hasOne(UserProfileInfo::className(), ['user_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getResetInfo()
  {
	   return $this->hasOne(UserReset::className(), ['user_id' => 'id'])->where(['status'=>0]);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCity()
  {
      if($this->profileInfo!=null){
        return $this->profileInfo->city;
      }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getActiveContract()
  {
	   return $this->hasOne(Contract::className(), ['id' => 'active_contract_id']);
  }

  public function getActiveContractMembers()
  {
    $subQueryContractUser=ContractMember::find()->select(['user_id'])->where(['contract_id'=>$this->active_contract_id]);
    return User::find()->where(['id'=>$subQueryContractUser,'trashed'=>0])->all();
  }

  public function getMemberIdz()
  {
    $idz=[];
    if($this->user_type==1){
      $idz[]=$this->id;
    }else{
      $activeContractMembers=$this->activeContractMembers;
      if($activeContractMembers!=null){
        foreach($activeContractMembers as $key=>$val){
          $idz[]=$val['id'];
        }
      }
    }
    return $idz;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getRenewedContract()
  {
    if($this->activeContract!=null && $this->activeContract->renewed_contract_id>0){
      return Contract::find()->where(['id'=>$this->activeContract->renewed_contract_id])->one();
    }
    return null;
  }

  public function getLastFiveContracts()
  {
    $memberContracts=ContractMember::find()->select(['contract_id'])->where(['user_id'=>$this->id]);
    return Contract::find()
    ->select([
      Contract::tableName().'.id',
      'start_date',
      'end_date',
      'package_name'=>Package::tableName().'.name',
    ])
    ->leftJoin(Package::tableName(),Package::tableName().".id=".Contract::tableName().".package_id")
    ->where([Contract::tableName().'.id'=>$memberContracts])
    ->orderBy(['end_date'=>SORT_DESC])->limit(5)->asArray()->all();
  }

  public function getMaxPackageEndDate()
  {
    if($this->renewedContract!=null){
      return $this->renewedContract->end_date;
    }else{
      return $this->activeContract!=null ? $this->activeContract->end_date : '';
    }

  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getActivePackage()
  {
	   return $this->hasOne(Package::className(), ['id' => 'active_package_id']);
  }

  public function getLastLoginInfo()
  {
    $history=UserLoginHistory::find()
      ->select(['created_at'])
      ->where(['user_id'=>$this->id,'login'=>1])
      ->orderBy(['created_at'=>SORT_DESC])
      ->offset(1)
      ->asArray()->one();
    if($history!=null){
      return 'on '.Yii::$app->formatter->asDate($history['created_at']);
    }else{
      return ' first time';
    }
  }

  public function getCanBookBoat()
  {
    $can=true;
    if($this->user_type==0){
      $fuelCredit = $this->profileInfo->credit_balance;
      $minBalReq=Yii::$app->appHelperFunctions->getSetting('booking_min_balance');

      $daysRemaining=Yii::$app->helperFunctions->getNumberOfDaysBetween(date("Y-m-d"),$this->maxPackageEndDate);
      if($daysRemaining>Yii::$app->params['ExpiryDaysToBookInNegative']){
        $minBalReq=Yii::$app->params['ExpiryDaysToBookInNegativeBalance'];
      }
      if($fuelCredit<$minBalReq){
        $can=false;
      }
    }
    return $can;
  }

	public function getAllowedMarinaPermits()
	{
		$allowed=[];
    $userPermits=UserNightPermit::find()->select(['port_id'])->where(['user_id'=>$this->id]);
		$permits=Marina::find()
			->select([
				'id',
				'name',
			])
			->where(['id'=>$userPermits])
			->asArray()->all();
		if($permits!=null){
			foreach($permits as $permit){
				$allowed[$permit['id']]=$permit['name'];
			}
		}
		return implode(", ",$allowed);
	}

  /**
   * @return boolean
   */
  public function getPrivilege($keyword)
  {
  	$permission=false;
  	$services=ArrayHelper::map(PackageOption::find()->all(),'keyword','id');
  	$result=PackageAllowedOptions::find()->where(['package_id'=>$this->activeContract->package_id,'service_id'=>$services[$keyword]])->one();
		if($result!=null){
			$permission=true;
		}

    return $permission;
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'image')){
      $this->file = UploadedFile::getInstance($this, 'image');
      // if no file was uploaded abort the upload
      if (!empty($this->file)) {
        $pInfo=pathinfo($this->file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->file->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->image==null && $this->oldfile!=null){
      $this->image=$this->oldfile;
    }

    //Uploading License Photos
    $cities=Yii::$app->appHelperFunctions->activeCityList;
    foreach($cities as $city){
      if(UploadedFile::getInstance($this, 'city_license_image['.$city['id'].']')){
        $this->licImageFile[$city['id']] = UploadedFile::getInstance($this, 'city_license_image['.$city['id'].']');
        // if no file was uploaded abort the upload
        if (!empty($this->licImageFile[$city['id']])) {
          $pInfo=pathinfo($this->licImageFile[$city['id']]->name);
          $ext = $pInfo['extension'];
          if (in_array($ext,$this->allowedImageTypes)) {
            // Check to see if any PHP files are trying to be uploaded
            $content = file_get_contents($this->licImageFile[$city['id']]->tempName);
            if (preg_match('/\<\?php/i', $content)) {
              $this->addError('city_license_image['.$city['id'].']', Yii::t('app', 'Invalid file provided!'));
              return false;
            }else{
              if (filesize($this->licImageFile[$city['id']]->tempName) <= $this->allowedImageSize) {
                // generate a unique file name
                $this->city_license_image[$city['id']] = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
              }else{
                $this->addError('city_license_image['.$city['id'].']', Yii::t('app', 'File size is too big!'));
                return false;
              }
            }
          }else{
            $this->addError('city_license_image['.$city['id'].']', Yii::t('app', 'Invalid file provided!'));
            return false;
          }
        }
      }
    }
    return parent::beforeValidate();
  }

  /**
   * @inheritdoc
   * Generates auth_key if it is a new user
   */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      if ($this->isNewRecord) {
        $this->generateAuthKey();
        //$this->generateVerifyToken();
      }
      if($this->new_password!=null){
        $this->password = $this->new_password;
      }
      return true;
    }
    return false;
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
    if($this->marina_permits!=null){
			UserNightPermit::deleteAll(['and', 'user_id = :user_id', ['not in', 'port_id', $this->marina_permits]], [':user_id' => $this->id]);
			if($this->marina_permits!=null){
				foreach($this->marina_permits as $key=>$val){
					$marinaPermitRow=UserNightPermit::find()->where(['user_id'=>$this->id,'port_id'=>$val])->one();
					if($marinaPermitRow==null){
						$marinaPermitRow=new UserNightPermit;
						$marinaPermitRow->user_id=$this->id;
						$marinaPermitRow->port_id=$val;
						$marinaPermitRow->save();
					}
				}
			}
		}

    $userProfileInfo = UserProfileInfo::find()->where(['user_id'=>$this->id])->one();
    if($userProfileInfo==null){
      $userProfileInfo=new UserProfileInfo;
      $userProfileInfo->user_id=$this->id;
    }
    if($this->city_id!=null)$userProfileInfo->city_id=$this->city_id;
    //if($this->license_expiry!=null)$userProfileInfo->license_expiry=$this->license_expiry;
    //if($this->license_image!=null)$userProfileInfo->license_image=$this->license_image;
    if($this->mobile!=null)$userProfileInfo->mobile=$this->mobile;
    if($this->is_licensed!=null)$userProfileInfo->is_licensed=$this->is_licensed;

    if($this->membership_number!=null)$userProfileInfo->membership_number=$this->membership_number;
    if($this->joining_date!=null)$userProfileInfo->joining_date=$this->joining_date;

    /*$oldAmount=$userProfileInfo->security_deposit;
    $oldType=$userProfileInfo->deposit_type;
    $saveDepositHistory=false;
    if($this->security_deposit!=null && $this->security_deposit!=$userProfileInfo->security_deposit){
      $saveDepositHistory=true;
      $userProfileInfo->security_deposit=$this->security_deposit;
    }
    if($this->deposit_type!=null && $this->deposit_type!=$userProfileInfo->deposit_type){
      $saveDepositHistory=true;
      $userProfileInfo->deposit_type=$this->deposit_type;
    }*/
    $userProfileInfo->save();
    //Save first Entry to Log
    /*if($oldAmount==0){
      $newDeposit=new UserSecurityDeposit;
      $newDeposit->user_id=$this->id;
      $newDeposit->trans_type='cr';
      $newDeposit->descp='Security Deposit';
      $newDeposit->amount=$this->security_deposit;
      $newDeposit->deposit_type=$this->deposit_type;
      $newDeposit->save();
    }*/


    //Save Change History
    /*if($saveDepositHistory==true){
      $depositChangeHistory=new UserSecurityDepositChangeHistory;
      $depositChangeHistory->user_id=$this->id;
      $depositChangeHistory->old_amount=$oldAmount;
      $depositChangeHistory->new_amount=$this->security_deposit;
      $depositChangeHistory->old_type=$oldType;
      $depositChangeHistory->new_type=$this->deposit_type;
      $depositChangeHistory->save();
    }*/

    //Update License Info
    if($this->city_license_start!=null && $this->city_license_expiry!=null){
      $cities=Yii::$app->appHelperFunctions->activeCityList;
      foreach($cities as $city){
        if(
          isset($this->city_license_start[$city['id']]) && isset($this->city_license_start[$city['id']])!=null &&
          isset($this->city_license_expiry[$city['id']]) && isset($this->city_license_expiry[$city['id']])!=null
        ){
          $licStartDate=$this->city_license_start[$city['id']];
          $licEndDate=$this->city_license_expiry[$city['id']];

          $licImage=$this->city_license_image[$city['id']];

          $cityLicenseInfo=new UserLicense;
          $cityLicenseInfo->user_id=$this->id;
          $cityLicenseInfo->city_id=$city['id'];
          $cityLicenseInfo->license_start=$licStartDate;
          $cityLicenseInfo->license_expiry=$licEndDate;
          $cityLicenseInfo->license_image=$licImage;
          if($cityLicenseInfo->save()){
            if ($this->licImageFile[$city['id']]!== null && $this->city_license_image[$city['id']]!=null) {
              $this->licImageFile[$city['id']]->saveAs(Yii::$app->params['member_license_uploads_abs_path'].$this->city_license_image[$city['id']]);
            }
          }
        }
      }
    }

    if($this->update_note!=null){
      $notemodel = new UserNote();
  		$notemodel->user_id=$this->id;
  		$notemodel->comments=$this->update_note;
  		$notemodel->save();
    }
    if ($this->file!== null && $this->image!=null) {
      if($this->oldfile!=null && $this->image!=$this->oldfile && file_exists(Yii::$app->params['member_uploads_abs_path'].$this->oldfile)){
        unlink(Yii::$app->params['member_uploads_abs_path'].$this->oldfile);
      }
      $this->file->saveAs(Yii::$app->params['member_uploads_abs_path'].$this->image);
    }
    if ($this->licensefile!== null && $this->license_image!=null) {
      if($this->oldlicensefile!=null && $this->license_image!=$this->oldlicensefile && file_exists(Yii::$app->params['member_license_uploads_abs_path'].$this->oldlicensefile)){
        unlink(Yii::$app->params['member_license_uploads_abs_path'].$this->oldlicensefile);
      }
      $this->licensefile->saveAs(Yii::$app->params['member_license_uploads_abs_path'].$this->license_image);
    }
    if($insert){
      //$this->notifyAdminToCreateContract();
    }
    parent::afterSave($insert, $changedAttributes);
  }

	public function sendNewMemberEmail(){
		//New User Alert
			$templateId=Yii::$app->appHelperFunctions->getSetting('e_newuser_u');
			$template=EmailTemplate::findOne($templateId);
			if($template!=null){
        Yii::$app->params['includeDefSignature']=false;

        $tempSysGeneratedPassword = Yii::$app->helperFunctions->generateToken(6);//Generating temp password
        $hashedPassword=Yii::$app->security->generatePasswordHash($tempSysGeneratedPassword); //generating hash for the password
        //Updating the hashed password for member
        $connection = \Yii::$app->db;
    		$connection->createCommand(
          "update ".self::tableName()." set password_hash=:password where id=:id",
          [
            ':password'=>$hashedPassword,
            ':id'=>$this->id,
          ]
        )
        ->execute();

				$vals = [
					'{loginlink}' => Yii::$app->params['siteUrl'],
					'{username}' => $this->email,
					'{name}' => $this->fullname,
					'{email}' => $this->email,
					'{password}' => $tempSysGeneratedPassword,
					'{packageStart}' => Yii::$app->formatter->asDate($this->activeContract->start_date),
					'{packageEnd}' => Yii::$app->formatter->asDate($this->activeContract->end_date),
					'{userFreezingDays}' => $this->activeContract->allowed_freeze_days,
					'{packageCategory}' => $this->activeContract->package_name,
					'{userCity}' => ($this->city!=null ? $this->city->name : ''),
					'{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'images/email_logo.png" width="136" height="105" alt="" />',
				];
				$htmlBody=$template->searchReplace($template->template_html,$vals);
				$textBody=$template->searchReplace($template->template_text,$vals);
				Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
					->setFrom([Yii::$app->params['mdEmail'] => Yii::$app->params['siteName']])
					->setTo($this->email)
					->setBcc([Yii::$app->params['careNEmail'],Yii::$app->params['adminEmail']])
					->setSubject('The Captain\'s Club Booking Access')
					->send();
			}
      //$this->notifyStaffToUpdateDeposit();
	}

	public function sendRenewAlert($generatePass=false)
	{
		//Renew User Alert
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_renewuser_u');
		$template=EmailTemplate::findOne($templateId);
		if($template!=null){
      Yii::$app->params['includeDefSignature']=false;
      $passwordString='';
      //Updating the hashed password for member if is not continuous renewal
      if($generatePass==true){
        $tempSysGeneratedPassword = Yii::$app->helperFunctions->generateToken(6);//Generating temp password
        $hashedPassword=Yii::$app->security->generatePasswordHash($tempSysGeneratedPassword); //generating hash for the password

        $passwordString='<br />Password: <strong>'.$tempSysGeneratedPassword.'</strong>';
        $connection = \Yii::$app->db;
        $connection->createCommand(
          "update ".self::tableName()." set password_hash=:password where id=:id",
          [
            ':password'=>$hashedPassword,
            ':id'=>$this->id,
          ]
        )
        ->execute();
        $connection->createCommand(
          "update ".UserProfileInfo::tableName()." set changed_pass=0 where id=:id",
          [
            ':id'=>$this->id,
          ]
        )
        ->execute();
      }

			$vals = [
				'{loginlink}' => Yii::$app->params['siteUrl'],
				'{username}' => $this->email,
        '{name}' => $this->fullname,
				'{email}' => $this->email,
				'{password}' => $passwordString,
				'{userFreezingDays}' => $this->renewedContract->allowed_freeze_days,
				'{packageStart}' => Yii::$app->formatter->asDate($this->renewedContract->start_date),
				'{packageEnd}' => Yii::$app->formatter->asDate($this->renewedContract->end_date),
				'{packageCategory}' => $this->renewedContract->package_name,
				'{userCity}' => ($this->city!=null ? $this->city->name : ''),
				'{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'images/email_logo.png" width="136" height="105" alt="" />',
			];
			$htmlBody=$template->searchReplace($template->template_html,$vals);
			$textBody=$template->searchReplace($template->template_text,$vals);
			Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
				->setFrom([Yii::$app->params['mdEmail'] => Yii::$app->params['siteName']])
				->setTo($this->email)
				->setBcc([Yii::$app->params['careNEmail'],Yii::$app->params['adminEmail']])
				->setSubject('The Captain\'s Club Booking Access')
				->send();
		}
	}

	public function sendActivatedRenewAlert()
	{
		//Renew User Alert
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_renewuser_u');
		$template=EmailTemplate::findOne($templateId);
		if($template!=null){
      Yii::$app->params['includeDefSignature']=false;
      //Updating the hashed password for member if is not continuous renewal
      $tempSysGeneratedPassword = Yii::$app->helperFunctions->generateToken(6);//Generating temp password
      $hashedPassword=Yii::$app->security->generatePasswordHash($tempSysGeneratedPassword); //generating hash for the password

      $passwordString='<br />Password: <strong>'.$tempSysGeneratedPassword.'</strong>';
      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".self::tableName()." set password_hash=:password where id=:id",
        [
          ':password'=>$hashedPassword,
          ':id'=>$this->id,
        ]
      )
      ->execute();
      $connection->createCommand(
        "update ".UserProfileInfo::tableName()." set changed_pass=0 where id=:id",
        [
          ':id'=>$this->id,
        ]
      )
      ->execute();


			$vals = [
				'{loginlink}' => Yii::$app->params['siteUrl'],
				'{username}' => $this->email,
        '{name}' => $this->fullname,
				'{email}' => $this->email,
				'{password}' => $passwordString,
				'{userFreezingDays}' => $this->activeContract->allowed_freeze_days,
				'{packageStart}' => Yii::$app->formatter->asDate($this->activeContract->start_date),
				'{packageEnd}' => Yii::$app->formatter->asDate($this->activeContract->end_date),
				'{packageCategory}' => $this->activeContract->package_name,
				'{userCity}' => ($this->city!=null ? $this->city->name : ''),
				'{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'images/email_logo.png" width="136" height="105" alt="" />',
			];
			$htmlBody=$template->searchReplace($template->template_html,$vals);
			$textBody=$template->searchReplace($template->template_text,$vals);
			Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
				->setFrom([Yii::$app->params['mdEmail'] => Yii::$app->params['siteName']])
				->setTo($this->email)
				->setBcc([Yii::$app->params['careNEmail'],Yii::$app->params['adminEmail']])
				->setSubject('The Captain\'s Club Booking Access')
				->send();
		}
	}

	public function sendEmailChangedAlert()
	{
		//Renew User Alert
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_useremailchanged_u');
		$template=EmailTemplate::findOne($templateId);
		if($template!=null){
			$vals = [
				'{name}' => $this->name,
				'{email}' => $this->email,
			];
			$htmlBody=$template->searchReplace($template->template_html,$vals);
			$textBody=$template->searchReplace($template->template_text,$vals);
			Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
				->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
				->setTo($this->email)
				->setSubject('Username Changed')
				->send();
		}
	}

  /**
   * Notify Admin User is created, Add Contract
   * @return boolean
   */
	public function notifyAdminToCreateContract()
	{
		//Renew User Alert
    $msg="Dear Admin,\n\r\n\r";
    $msg.=$this->fullname."'s profile is created, Please add contract information for the member,\n\r";
    $msg.='Name: '.$this->firstname.' '.$this->lastname."\n\r";
    $msg.='Email: '.$this->email."\n\r";
    $msg.='Mobile: '.$this->profileInfo->mobile."\n\r";
    $msg.='City: '.$this->profileInfo->city->name."\n\r";
    $htmlBody=nl2br($msg);
		$textBody=$msg;
		Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
			->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
			->setTo([Yii::$app->params['mdEmail']])
			->setSubject('Member Profile Created')
			->send();
    return true;
	}

  /**
   * Notify Staff to update deposit
   * @return boolean
   */
	public function notifyStaffToUpdateDeposit()
	{
		//Renew User Alert
    $msg="Dear Admin,\n\r\n\r";
    $msg.=$this->fullname."'s contract is created, Please update security deposit for the member,\n\r";
    $msg.='Name: '.$this->firstname.' '.$this->lastname."\n\r";
    $msg.='Email: '.$this->email."\n\r";
    $msg.='Mobile: '.$this->profileInfo->mobile."\n\r";
    $msg.='City: '.$this->profileInfo->city->name."\n\r";
    $htmlBody=nl2br($msg);
		$textBody=$msg;
		Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
			->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
			->setTo([Yii::$app->params['mdEmail']])
			->setSubject('Update Security Deposit')
			->send();
    return true;
	}

  public function getSavedCards()
  {
    $ccArr=[];
    $results=UserSavedCc::find()->select(['id','card_brand','last_4_digits'])->where(['user_id'=>$this->id,'trashed'=>0])->AsArray()->all();
    if($results!=null){
      foreach ($results as $result) {
        $ccArr[$result['id']]=$result['card_brand'].' ending '.$result['last_4_digits'];
      }
    }
    return $ccArr;
  }

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.($this->user_type==1 ? 'Staff Member' : 'User').' ('.$this->fullname.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

  /**
   * Mark record as deleted and hides from list.
   * @return boolean
   */
  public function Softdelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed' => 1,
        ':trashed_at' => date("Y-m-d H:i:s"),
        ':trashed_by' => Yii::$app->user->identity->id,
        ':id' => $this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.($this->user_type==1 ? 'Staff Member' : 'User').' ('.$this->fullname.') trashed successfully');
		return true;
  }

  /*
  * Check if there is any hold entry
  */
  public function getPackageHoldDate()
	{
		$userInHold=UserStatusReason::find()->where(['and',['user_id'=>$this->id,'status'=>2],['>=','date',date("Y-m-d")]])->one();
		if($userInHold!=null && $userInHold->date!=null && $userInHold->date!=''){
			return $userInHold->date;
		}
	}

  /*
  * Check if there is any cancel entry
  */
	public function getPackageCancelDate()
	{
		$userInCancel=UserStatusReason::find()->where(['and',['user_id'=>$this->id,'status'=>3],['>=','date',date("Y-m-d")]])->one();
		if($userInCancel!=null && $userInCancel->date!=null && $userInCancel->date!=''){
			return $userInCancel->date;
		}
	}

  /**
   * @return \yii\db\ActiveQuery
   */
	public function getFreezeRequest()
	{
		return UserFreezeRequest::find()->where([
      'and',['status'=>1,'trashed'=>0],['in','created_by',$this->memberIdz]
    ])->orderBy(['id'=>SORT_DESC])->one();
	}

  public function getActiveFreezePeriod()
  {
    return UserFreezeRequest::find()
    ->where(['and',['user_id'=>$this->memberIdz,'status'=>1,'trashed'=>0],['<=','start_date',date("Y-m-d")],['>=','end_date',date("Y-m-d")]])
    ->asArray()->one();
  }

  /**
   * @return \yii\db\ActiveQuery
   */
	public function getFreezeStartDate()
	{
		$freezeRequest=$this->freezeRequest;
		if($freezeRequest!=null){
			return $freezeRequest->start_date;
		}
	}

  /**
   * @return \yii\db\ActiveQuery
   */
	public function getFreezeEndDate()
	{
		$freezeRequest=$this->freezeRequest;
		if($freezeRequest!=null){
			return $freezeRequest->end_date;
		}
	}

  public function getActivePackageAllowedPurposes()
  {
    $idz=[];
    $packagePurposes=PackageAllowedPurpose::find()->select(['purpose_id'])->where(['package_id'=>$this->activeContract->package_id])->asArray()->all();
    if($packagePurposes!=null){
      foreach($packagePurposes as $packagePurpose){
        $idz[]=$packagePurpose['purpose_id'];
      }
    }
    return $idz;
  }
}
