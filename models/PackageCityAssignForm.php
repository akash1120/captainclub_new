<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
* PackageCityAssignForm is the model behind the package city assignment
*/
class PackageCityAssignForm extends Model
{
  public $package_city;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['package_city'], 'required'],
    ];
  }

  /**
  * @return boolean whether the model passes validation
  */
  public function save()
  {
    if ($this->validate()) {
      $connection = \Yii::$app->db;
      $connection->createCommand("TRUNCATE ".PackageCity::tableName()."")->execute();
      if($this->package_city!=null){
        foreach($this->package_city as $package_id=>$citiesArr){
          if($citiesArr!=null){
            foreach($citiesArr as $key=>$city_id){
              $packageCity=PackageCity::find()->where(['package_id'=>$package_id,'city_id'=>$city_id]);
              if(!$packageCity->exists()){
                $packageCity=new PackageCity;
                $packageCity->package_id=$package_id;
                $packageCity->city_id=$city_id;
                $packageCity->save();
              }
            }
          }
        }
      }
      return true;
    }
    return false;
  }
}
