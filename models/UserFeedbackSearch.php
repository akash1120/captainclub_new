<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserFeedback;

/**
* UserFeedbackSearch represents the model behind the search form of `app\models\UserFeedback`.
*/
class UserFeedbackSearch extends UserFeedback
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','user_id','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['descp','created_at','updated_at','trashed','trashed_at'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = UserFeedback::find()
    ->select([
      'id',
      'created_at',
      'user_id',
      'rating',
      'descp',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'user_id' => $this->user_id,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like','descp',$this->descp]);

    return $dataProvider;
  }
}
