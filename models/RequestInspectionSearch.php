<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RequestInspection;

/**
* RequestInspectionSearch represents the model behind the search form of `app\models\RequestInspection`.
*/
class RequestInspectionSearch extends RequestInspection
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
        [['id', 'boat_id', 'created_by', 'updated_by'], 'integer'],
        [['inspection_date', 'remarks', 'created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = RequestInspection::find()
    ->select([
      RequestInspection::tableName().'.id',
      'inspection_date',
      'remarks',
      'boat_id',
      'boat_name'=>Boat::tableName().'.name',
    ])
    ->innerJoin(Boat::tableName(),Boat::tableName().".id=".RequestInspection::tableName().".boat_id")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort' => ['defaultOrder'=>['id'=>SORT_DESC]],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'boat_id' => $this->boat_id,
      'inspection_date' => $this->inspection_date,
      'created_at' => $this->created_at,
      'created_by' => $this->created_by,
      'updated_at' => $this->updated_at,
      'updated_by' => $this->updated_by,
    ]);


    $query->andFilterWhere(['like', 'remarks', $this->remarks]);

    return $dataProvider;
  }
}
