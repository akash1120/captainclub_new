<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * UserNoteUpdateForm is the model behind the user note update form.
 */
class UserNoteUpdateForm extends Model
{
  public $parent,$remarks;
  public $ad_reminder;
  public $comments,$reminder_type,$reminder,$staffIdz;

  /**
   * @return array the validation rules.
   */
  public function rules()
  {
    return [
      [['parent','remarks'], 'required'],
      [['parent','ad_reminder','reminder_type'], 'integer'],
      ['ad_reminder','checkReminderInfo'],
      [['comments','remarks'], 'string'],
      ['staffIdz','each','rule'=>['integer']],
      [['reminder'], 'safe'],
    ];
  }

  /**
   * Validates the reminder info.
   */
  public function checkReminderInfo($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->ad_reminder==1){
        if($this->reminder=='' || $this->reminder==null){
          $this->addError('reminder', 'Please enter date time');
        }
        if($this->comments=='' || $this->comments==null){
          $this->addError('comments', 'Please enter title of note');
        }
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'comments' => Yii::t('app', 'Title'),
      'remarks' => Yii::t('app', 'Remarks'),
      'reminder_type' => Yii::t('app', 'Type'),
      'reminder' => Yii::t('app', 'Reminder Date/Time'),
      'staffIdz' => Yii::t('app', 'Staff Member(s)'),
    ];
  }

  /**
   * @return boolean whether the model passes validation
   */
  public function save()
  {
    if ($this->validate()) {
      $parentModel = UserNote::find()->where(['id'=>$this->parent])->one();
      if($parentModel!=null){

      	$connection = \Yii::$app->db;
      	$connection->createCommand(
          "update ".UserNote::tableName()." set remarks=:remarks,updated_at=:updated_at,updated_by=:updated_by where id=:id",
          [
            ':remarks'=>$this->remarks,
            ':updated_at'=>date("Y-m-d H:i:s"),
            ':updated_by'=>Yii::$app->user->identity->id,
            ':id'=>$this->parent,
          ]
        )
        ->execute();


      	$result=new UserNote;
      	$result->user_id=$parentModel->user_id;
      	$result->parent=$parentModel->id;
      	$result->comments=$this->comments;
      	$result->reminder_type=$this->reminder_type;
      	$result->reminder=$this->reminder;
      	$result->staffIdz=$this->staffIdz;
      	$result->save();
      	return true;
      }
    }
    return false;
  }
}
