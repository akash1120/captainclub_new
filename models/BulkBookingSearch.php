<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BulkBooking;

/**
* BulkBookingSearch represents the model behind the search form of `app\models\BulkBooking`.
*/
class BulkBookingSearch extends BulkBooking
{
  public $listType,$date_range,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'boat_id', 'booking_type', 'created_by', 'updated_by'], 'integer'],
      [['listType','date_range','start_date', 'end_date', 'booking_comments', 'created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = BulkBooking::find()
    ->select([
      BulkBooking::tableName().'.id',
      BulkBooking::tableName().'.boat_id',
      'boat_name'=>Boat::tableName().'.name',
      BulkBooking::tableName().'.start_date',
      BulkBooking::tableName().'.end_date',
      BulkBooking::tableName().'.booking_type',
      BulkBooking::tableName().'.booking_comments',
      BulkBooking::tableName().'.created_at',
      'created_by_user'=>User::tableName().'.username',
    ])
    ->innerJoin(Boat::tableName(),Boat::tableName().".id=".BulkBooking::tableName().".boat_id")
    ->innerJoin(User::tableName(),User::tableName().".id=".BulkBooking::tableName().".created_by")
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    if($this->listType=='active'){
      $query->andWhere(['>=', BulkBooking::tableName().'.end_date', date("Y-m-d")]);
      $query->andWhere([BulkBooking::tableName().'.trashed'=>0]);
    }elseif($this->listType=='history'){
      $query->andWhere(['<', BulkBooking::tableName().'.end_date', date("Y-m-d")]);
      $query->andWhere([BulkBooking::tableName().'.trashed'=>0]);
    }elseif($this->listType=='deleted'){
      $query->andWhere([BulkBooking::tableName().'.trashed'=>1]);
    }

    $query->andFilterWhere([
      BulkBooking::tableName().'.id' => $this->id,
      'boat_id' => $this->boat_id,
      BulkBooking::tableName().'.start_date' => $this->start_date,
      BulkBooking::tableName().'.end_date' => $this->end_date,
      BulkBooking::tableName().'.booking_type' => $this->booking_type,
      'DATE('.BulkBooking::tableName().'.created_at)' => $this->created_at,
      BulkBooking::tableName().'.created_by' => $this->created_by,
    ]);


    if($this->date_range!=null){
      list($startDate,$endDate)=explode(" - ",$this->date_range);
      $query->andWhere([
        'or',
				['between',BulkBooking::tableName().'.start_date',$startDate,$endDate],
				['between',BulkBooking::tableName().'.end_date',$startDate,$endDate],
      ]);
    }

    $query->andFilterWhere(['like',BulkBooking::tableName().'.booking_comments',$this->booking_comments]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    return Yii::$app->helperFunctions->getBulkBookingTabs($this);
  }
}
