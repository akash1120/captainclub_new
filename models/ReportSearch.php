<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
* ReportSearch
*/
class ReportSearch extends Announcement
{
  public $deposit_type,$user_id,$username,$date_range,$pageSize;
  public $paytab_code;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['deposit_type','user_id','pageSize'], 'integer'],
      [['username','paytab_code','date_range'], 'string'],
    ];
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = UserAccounts::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[3],
      ],
			'sort'=> ['defaultOrder' => ['created_at'=>SORT_ASC]],
    ]);

    $query->andFilterWhere([
      'user_id' => $this->user_id,
    ]);

		if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
			$query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
		}

    if($this->paytab_code!=null){
      $subQueryOrder=Order::find()->select(['id'])->where(['transaction_id'=>$this->paytab_code]);
      $query->andFilterWhere([
        'order_id' => $subQueryOrder,
      ]);
    }

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchManualDeposits($params)
  {
    $this->load($params);
    $query = UserAccounts::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[3],
      ],
			'sort'=> ['defaultOrder' => ['created_at'=>SORT_ASC]],
    ]);

    $query->andFilterWhere([
      'order_id' => 0,
      'account_id' => 0,
      'user_id' => $this->user_id,
    ]);

		if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
			$query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
		}

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchManualCharges($params)
  {
    $this->load($params);
    $query = UserAccounts::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[3],
      ],
			'sort'=> ['defaultOrder' => ['created_at'=>SORT_ASC]],
    ]);

    $query->andFilterWhere([
      'order_id' => 0,
      'account_id' => 10,
      'user_id' => $this->user_id,
    ]);

		if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
			$query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
		}

    return $dataProvider;
  }

  public function getMember()
  {
    return User::findOne($this->user_id);
  }

  public function getTotalDeposits()
  {
    $query=UserAccounts::find();
  	$query->where([
      'and',
      ['trans_type'=>'cr','account_id'=>0],
      ['>','order_id',0],
    ]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    $sum=$query->sum('amount');
    return $sum>0 ? $sum : 0;
  }

  public function getTotalManual()
  {
    $query=UserAccounts::find();
  	$query->where([
      'and',
      ['trans_type'=>'cr','account_id'=>0,'order_id'=>0],
    ]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    $sum=$query->sum('amount');
    return $sum>0 ? $sum : 0;
  }

  public function getTotalCharge()
  {
    $query=UserAccounts::find();
  	$query->where([
      'and',
      ['trans_type'=>'dr','account_id'=>10,'order_id'=>0],
    ]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    $sum=$query->sum('amount');
    return $sum>0 ? $sum : 0;
  }

  public function getOpeningBalance()
  {
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $depositQuery=UserAccounts::find()
        ->where(['and',['account_id'=>0],['>','order_id',0],]);
      $depositQuery->andWhere(['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date]);
      $deposits = $depositQuery->sum("amount+profit");
      $withdrawalQuery=UserAccounts::find()
        ->where(['account_id'=>11]);
      $withdrawalQuery->andWhere(['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date]);
      $withdrawals = $withdrawalQuery->sum("amount");
      return $deposits-$withdrawals;
    }else{
      return 0;
    }
  }

  public function getUnConsumedOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $crQuery=UserAccounts::find()
    	->where([
        'and',
        ['trans_type'=>'cr'],
        ['!=','account_id',11],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
    	$sumOfCr=$crQuery->sum('amount');
      $drQuery=UserAccounts::find()
    	->where([
        'and',
        ['trans_type'=>'dr'],
        ['!=','account_id',11],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
    	$sumOfDr=$drQuery->sum('amount');
      $value=($sumOfCr-$sumOfDr);
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getUnConsumedTotal()
  {
    $crQuery=UserAccounts::find()
  	->where(['and',['trans_type'=>'cr'],['!=','account_id',11]]);
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $crQuery->andWhere([
        'and',
        ['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
        ['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
      ]);
    }
  	$sumOfCr=$crQuery->sum('amount');

  	$drQuery=UserAccounts::find()
  	->where(['and',['trans_type'=>'dr'],['!=','account_id',11]]);
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $drQuery->andWhere([
        'and',
        ['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
        ['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
      ]);
    }
  	$sumOfDr=$drQuery->sum('amount');
  	return $sumOfCr-$sumOfDr;
  }

  public function getDepositTotal()
  {
    $query = UserAccounts::find()
      ->where(['account_id'=>0]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum("amount");
  }

  public function getChargesOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query = UserAccounts::find()
      ->innerJoin("order",Order::tableName().".id=".UserAccounts::tableName().".order_id")
      ->where([
        'and',
        ['account_id'=>0],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $value=$query->sum(Order::tableName().".amount_payable-".UserAccounts::tableName().".amount-".UserAccounts::tableName().".profit");
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getChargesTotal()
  {
    $query = UserAccounts::find()
      ->innerJoin("order",Order::tableName().".id=".UserAccounts::tableName().".order_id")
      ->where(['account_id'=>0]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum(Order::tableName().".amount_payable-".UserAccounts::tableName().".amount-".UserAccounts::tableName().".profit");
  }

  public function getIncomeOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query = UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>0],
        ['>','order_id',0],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $value=$query->sum("profit");
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getIncomeTotal()
  {
    $query = UserAccounts::find()
      ->where(['and',['account_id'=>0],['>','order_id',0]]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum("profit");
  }

  public function marinaFuelOB($port_id)
  {
    $value='';
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query = UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>1,'port_id'=>$port_id],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ])
      ->innerJoin("booking",Booking::tableName().".id=".UserAccounts::tableName().".booking_id");
      $value=$query->sum("amount-profit");
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function marinaFuelTotal($port_id)
  {
    $query = UserAccounts::find()
      ->where(['account_id'=>1,'port_id'=>$port_id])
      ->innerJoin("booking",Booking::tableName().".id=".UserAccounts::tableName().".booking_id");
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum("amount-profit");
  }

  public function getFuelProfitOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query = UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>1],
        ['>','booking_id',0],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $value=$query->sum("profit");
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getFuelProfitTotal()
  {
    $query = UserAccounts::find()
      ->where(['and',['account_id'=>1],['>','booking_id',0]]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum("profit");
  }

  public function getCaptainOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query = UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>2],
        ['>','booking_id',0],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $value=$query->sum("amount");
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getCaptainTotal()
  {
    $query = UserAccounts::find()
      ->where(['and',['account_id'=>2],['>','booking_id',0]]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum("amount");
  }

  public function getBbqOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query = UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>3],
        ['>','booking_id',0],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $value=$query->sum("amount");
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getBbqTotal()
  {
    $query = UserAccounts::find()
      ->where(['and',['account_id'=>3],['>','booking_id',0]]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum("amount");
  }

  public function getOtherOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query = UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>10],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $value=$query->sum("amount");
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getOtherTotal()
  {
    $query = UserAccounts::find()
      ->where(['account_id'=>10]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $query->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    return $query->sum("amount");
  }

  public function getPaytabsOB()
  {
    $value='';
    if($this->date_range!=null){
      list($start_date,$end_date)=explode(" - ",$this->date_range);
      $depositQuery=UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>0],
        ['>','order_id',0],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $deposits = $depositQuery->sum("amount+profit");

      $withdrawalQuery=UserAccounts::find()
      ->where([
        'and',
        ['account_id'=>11],
        ['<','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
      ]);
      $withdrawals = $withdrawalQuery->sum("amount");
      $value=$deposits-$withdrawals;
      $value='<br /><span class="label label-default">c/f '.($value!=null ? $value : '0').'</span>';
    }
    return $value;
  }

  public function getPaytabsTotal()
  {
    $depositQuery=UserAccounts::find()
      ->where(['and',['account_id'=>0],['>','order_id',0],]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $depositQuery->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    $deposits = $depositQuery->sum("amount+profit");

    $withdrawalQuery=UserAccounts::find()
      ->where(['account_id'=>11]);
    if($this->date_range!=null){
			list($start_date,$end_date)=explode(" - ",$this->date_range);
      $withdrawalQuery->andWhere([
				'and',
				['>=','DATE('.UserAccounts::tableName().'.created_at)',$start_date],
				['<=','DATE('.UserAccounts::tableName().'.created_at)',$end_date],
			]);
    }
    $withdrawals = $withdrawalQuery->sum("amount");
    return $deposits-$withdrawals;
  }
}
