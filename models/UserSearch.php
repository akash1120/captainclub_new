<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
* UserSearch represents the model behind the search form of `app\models\User`.
*/
class UserSearch extends User
{
  public $keyword;
  public $security_deposit,$credit_balance,$mobile,$city_name,$package_name,$tasks,$pageSize;
  public $license_expiry,$expiry_month;
  public $freeze_start,$freeze_end,$freeze_date_range;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','city_name','is_licensed','package_name','tasks','status','created_by','updated_by','pageSize'],'integer'],
      [['keyword','expiry_month','username','firstname','lastname','license_expiry','security_deposit','credit_balance','email','mobile','end_date','freeze_start','freeze_end','freeze_date_range'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  public function generateQuery($params)
  {
    $this->load($params);
    $query = User::find()
    ->select([
      User::tableName().'.id',
      'image',
      'username',
      'name'=>'CONCAT(firstname," ",lastname)',
      'firstname',
      'lastname',
      UserProfileInfo::tableName().'.security_deposit',
      UserProfileInfo::tableName().'.deposit_type',
      UserProfileInfo::tableName().'.is_licensed',
      UserProfileInfo::tableName().'.license_image',
      UserProfileInfo::tableName().'.license_expiry',
      'email',
      UserProfileInfo::tableName().'.mobile',
      UserProfileInfo::tableName().'.credit_balance',
      'city_name'=>City::tableName().'.name',
      'package_name'=>Package::tableName().'.name',
      User::tableName().'.status',
      Contract::tableName().'.end_date',
    ])
    ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
    ->leftJoin(Contract::tableName(),Contract::tableName().'.id='.User::tableName().'.active_contract_id')
    ->leftJoin(Package::tableName(),Package::tableName().'.id='.Contract::tableName().'.package_id')
    ->leftJoin(City::tableName(),City::tableName().'.id='.UserProfileInfo::tableName().'.city_id')
    ->asArray();

    // grid filtering conditions
    $query->andFilterWhere([
      User::tableName().'.id' => $this->id,
      'user_type' => 0,
      UserProfileInfo::tableName().'.city_id' => $this->city_name,
      Contract::tableName().'.package_id' => $this->package_name,
      UserProfileInfo::tableName().'.security_deposit' => $this->security_deposit,
      UserProfileInfo::tableName().'.is_licensed' => $this->is_licensed,
      UserProfileInfo::tableName().'.license_expiry' => $this->license_expiry,
      User::tableName().'.status' => $this->status,
      User::tableName().'.trashed' => 0,
    ]);

    $query
    ->andFilterWhere([
      'or',
      ['like','username',$this->keyword],
      ['like','firstname',$this->keyword],
      ['like','lastname',$this->keyword],
      ['like','email',$this->keyword],
      ['like',UserProfileInfo::tableName().'.mobile',$this->keyword],
    ]);
    $query->andFilterWhere(['like','username',$this->username])
      ->andFilterWhere(['like','firstname',$this->firstname])
      ->andFilterWhere(['like','lastname',$this->lastname])
      ->andFilterWhere(['like','email',$this->email])
      ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

    if($this->credit_balance!=null){
      if($this->credit_balance=='+'){
        $query->andWhere(['>',UserProfileInfo::tableName().'.credit_balance',0]);
      }
      if($this->credit_balance=='-'){
        $query->andWhere(['<',UserProfileInfo::tableName().'.credit_balance',0]);
      }
    }

    if($this->tasks!=''){
      if($this->tasks==1){
        //No Task
        $subQuery = UserNote::find()
          ->select([UserNote::tableName().".user_id"])
          ->groupBy(UserNote::tableName().".user_id");
        $query->andWhere(['not in',User::tableName().'.id',$subQuery]);
      }
      if($this->tasks==2){
        //No Pending Task
        $subQuery = UserNote::find()
          ->select([UserNote::tableName().".user_id"])
          ->where(['and',['<','DATE(reminder)',date("Y-m-d")],['or',['remarks'=>NULL],['=','remarks','']]])
          ->groupBy(UserNote::tableName().".user_id");
        $query->andWhere(['in',User::tableName().'.id',$subQuery]);
      }
      if($this->tasks==3){
        //No Future Task
        $subQuery = UserNote::find()
          ->select([UserNote::tableName().".user_id"])
          ->where(['>=','DATE(reminder)',date("Y-m-d")])
          ->groupBy(UserNote::tableName().".user_id");
        $query->andWhere(['in',User::tableName().'.id',$subQuery]);
      }
    }
    if(isset($this->expiry_month) && $this->expiry_month != null ) {
      $query->andFilterWhere(['like', Contract::tableName() . '.end_date', $this->expiry_month]);
      $query->andFilterWhere([
          Contract::tableName() . '.renewed_contract_id' => 0,
      ]);
    }

    return $query;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query=$this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchFreezed($params)
  {
    $query=$this->generateQuery($params);
    $query->addSelect([
      'freeze_start'=>UserFreezeRequest::tableName().'.start_date',
      'freeze_end'=>UserFreezeRequest::tableName().'.end_date'
    ]);
    $query->innerJoin(UserFreezeRequest::tableName(),UserFreezeRequest::tableName().".user_id=".User::tableName().".id");

    $query->andFilterWhere([UserFreezeRequest::tableName().'.start_date'=>$this->freeze_start]);
		$query->andFilterWhere([UserFreezeRequest::tableName().'.end_date'=>$this->freeze_end]);

		if($this->freeze_date_range!=null){
			list($freezeStart,$freezeEnd)=explode(" - ",$this->freeze_date_range);
			$query->andWhere([
        'and',
				['<=',UserFreezeRequest::tableName().'.start_date',date("Y-m-d")],
        ['>=',UserFreezeRequest::tableName().'.end_date',date("Y-m-d")],
        //'or',
        //['between',UserFreezeRequest::tableName().'.start_date',$freezeStart,$freezeEnd],
				//['between',UserFreezeRequest::tableName().'.end_date',$freezeStart,$freezeEnd],
			]);
		}
    if($this->freeze_date_range==null && $this->freeze_start==null && $this->freeze_end==null){
			$query->andWhere(['<=',UserFreezeRequest::tableName().'.start_date',date("Y-m-d")]);
			$query->andWhere(['>=',UserFreezeRequest::tableName().'.end_date',date("Y-m-d")]);
		}

    //$query->andWhere([User::tableName().'.id'=>$subQueryContractMembers]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    return $dataProvider;
  }
}
