<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
* This is the model class for table "{{%user_security_deposit_change_history}}".
*
* @property integer $id
* @property integer $user_id
* @property integer $old_amount
* @property integer $new_amount
* @property string $old_type
* @property string $new_type
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class UserSecurityDepositChangeHistory extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_security_deposit_change_history}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id', 'old_amount', 'new_amount'], 'required'],
      [['user_id','created_by','updated_by'], 'integer'],
      [['old_amount','new_amount'], 'number'],
      [['created_at', 'updated_at'], 'safe'],
      [['old_type', 'new_type'], 'string'],
    ];
  }
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }
}
