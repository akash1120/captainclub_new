<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
* ClaimOrderReport.
*/
class ClaimOrderReport extends Model
{
  public $port_id,$date;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['port_id'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchForEmp($params)
  {
    $query = ClaimOrderItem::find()
    ->innerJoin(ClaimOrder::tableName(),ClaimOrder::tableName().".id=".ClaimOrderItem::tableName().".order_id");

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    $query->andFilterWhere([
      'marina_id' => $this->port_id,
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchForOther($params)
  {
    $this->load($params);

    $profit=0;
		$marinaProfit=Marina::find()->select(['fuel_profit'])->where(['id'=>$this->port_id])->asArray()->one();
		if($marinaProfit!=null){
			$profit=$marinaProfit['fuel_profit'];
		}

    $query1=Booking::find()
    ->select([
      Booking::tableName().'.id',
      new Expression('"booking" as rec_type'),
      "(fuel_cost-$profit) as amount",
      BookingActivity::tableName().'.updated_at as created_at',
      'user_id',
    ])
    ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
    ->where(['port_id'=>$this->port_id,'claimed'=>[0,1],'hide_other'=>1,'fuel_chargeable'=>1]);

    $query2=ClaimOrder::find()
    ->select([
      'id',
      new Expression('"order" as rec_type'),
      'total_amount as amount',
      'created_at',
      'created_by as user_id',
    ])
    ->where(['marina_id'=>$this->port_id]);

		$query = (new yii\db\Query())
			->select('*')
			->from(['other_marina_claims'=>$query1->union($query2)])
      ->orderBy(['created_at'=>SORT_ASC]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    return $dataProvider;
  }
}
