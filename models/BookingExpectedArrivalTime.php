<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%booking_expected_arrival_time}}".
*
* @property integer $id
* @property integer $booking_id
* @property integer $user_id
* @property string $arrival_time
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class BookingExpectedArrivalTime extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_expected_arrival_time}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['booking_id', 'user_id', 'arrival_time'], 'required'],
      [['created_at', 'updated_at'], 'safe'],
      [['booking_id', 'user_id', 'created_by', 'updated_by'], 'integer'],
      [['arrival_time'], 'string', 'max' => 50]
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'booking_id' => Yii::t('app', 'Booking'),
      'user_id' => Yii::t('app', 'User'),
      'arrival_time' => Yii::t('app', 'Expected Arriving Time'),
      'created_by' => Yii::t('app', 'Created By'),
      'created_at' => Yii::t('app', 'Created At'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'updated_at' => Yii::t('app', 'Updated At'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
    ];
  }
}
