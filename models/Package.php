<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%packages}}".
*
* @property integer $id
* @property string $name
* @property integer $status
*/
class Package extends ActiveRecord
{
	public $allowed_options,$allowed_cities,$allowed_purposes;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%packages}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['name'],'required'],
      [['status','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['name'],'string'],
      [['name'],'trim'],
      ['status','default','value'=>1],
      [['allowed_options','allowed_cities','allowed_purposes'],'each','rule'=>['integer']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'name' => Yii::t('app', 'Title'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  public function getHasMultiUser()
  {
    $twoUserOption=Yii::$app->appHelperFunctions->getPackageOptionByKeyword('2users');
    $subQueryMultiUserAllowed=PackageAllowedOptions::find()->select(['package_id'])->where(['package_id'=>$this->id,'service_id'=>$twoUserOption['id']]);
    $isThisMultiUserPackage=Package::find()->where(['id'=>$subQueryMultiUserAllowed]);
    if($isThisMultiUserPackage->exists()){
      return true;
    }else{
      return false;
    }
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		if($this->allowed_options!=null){
      PackageAllowedOptions::deleteAll(['and', 'package_id = :package_id', ['not in', 'service_id', $this->allowed_options]], [':package_id' => $this->id]);
			foreach($this->allowed_options as $key=>$val){
        $checkAlreadyAllowed=PackageAllowedOptions::find()->where(['package_id'=>$this->id,'service_id'=>$val])->one();
        if($checkAlreadyAllowed==null){
  				$allowedOption=new PackageAllowedOptions;
  				$allowedOption->package_id=$this->id;
  				$allowedOption->service_id=$val;
  				$allowedOption->save();
        }
			}
		}
		if($this->allowed_cities!=null){
      PackageCity::deleteAll(['and', 'package_id = :package_id', ['not in', 'city_id', $this->allowed_cities]], [':package_id' => $this->id]);
			foreach($this->allowed_cities as $key=>$val){
        $checkAlreadyAllowed=PackageCity::find()->where(['package_id'=>$this->id,'city_id'=>$val])->one();
        if($checkAlreadyAllowed==null){
  				$allowedOption=new PackageCity;
  				$allowedOption->package_id=$this->id;
  				$allowedOption->city_id=$val;
  				$allowedOption->save();
        }
			}
		}
		if($this->allowed_purposes!=null){
      PackageAllowedPurpose::deleteAll(['and', 'package_id = :package_id', ['not in', 'purpose_id', $this->allowed_purposes]], [':package_id' => $this->id]);
			foreach($this->allowed_purposes as $key=>$val){
        $checkAlreadyAllowed=PackageAllowedPurpose::find()->where(['package_id'=>$this->id,'purpose_id'=>$val])->one();
        if($checkAlreadyAllowed==null){
  				$allowedPurpose=new PackageAllowedPurpose;
  				$allowedPurpose->package_id=$this->id;
  				$allowedPurpose->purpose_id=$val;
  				$allowedPurpose->save();
        }
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Package ('.$this->name.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Package ('.$this->name.') trashed successfully');
		return true;
	}
}
