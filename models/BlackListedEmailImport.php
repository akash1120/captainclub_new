<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\BlackListedEmail;

/**
* BlackListedEmail import form
*/
class BlackListedEmailImport extends Model
{
	public $mail_gun_importfile;
	public $allowedImageTypes=['csv'];//'xls', 'xlsx',

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['mail_gun_importfile'], 'safe'],
			[['mail_gun_importfile'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes)]
		];
	}


	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'mail_gun_importfile' => Yii::t('app', 'Mail Gun File'),
		];
	}

	/**
	* import the entries into table
	*/
	public function save()
	{
		$saved=0;
		$unsaved=0;
		$errNames='';
		if ($this->validate()) {
			if(UploadedFile::getInstance($this, 'mail_gun_importfile')){
				$importedFile = UploadedFile::getInstance($this, 'mail_gun_importfile');
				// if no image was uploaded abort the upload
				if (!empty($importedFile)) {
					$pInfo=pathinfo($importedFile->name);
					$ext = $pInfo['extension'];
					if (in_array($ext,$this->allowedImageTypes)) {
						// Check to see if any PHP files are trying to be uploaded
						$content = file_get_contents($importedFile->tempName);
						if (preg_match('/\<\?php/i', $content)) {

						}else{
							$this->mail_gun_importfile = Yii::$app->params['temp_abs_path'].$importedFile->name;
							$importedFile->saveAs($this->mail_gun_importfile);

							$csvFile = fopen($this->mail_gun_importfile, 'r');
							$data = [];
							$n=1;
							while (($line = fgetcsv($csvFile)) !== FALSE) {
								$email = $line[4];
								if($n>1){

									$checkAlready=BlackListedEmail::find()->where(['email'=>$email])->one();
									if($checkAlready!=null){
										if($checkAlready->trashed==1){
											$checkAlready->trashed=0;
											$checkAlready->save();
											$saved++;
										}else{
											$errNames.='<br />'.$email.' already existed';
											$unsaved++;
										}
									}else{
										$model=new BlackListedEmail;
										$model->email=$email;
										if($model->save()){
											$saved++;
										}else{
											$errNames.='<br />'.$email;
											$unsaved++;
										}
									}
								}
								$n++;
							}
							fclose($csvFile);
							//Unlink File
							unlink($this->mail_gun_importfile);
						}
					}
				}
			}
			if($saved>0){
				Yii::$app->getSession()->setFlash('success', $saved.' - '.Yii::t('app','Emails saved successfully'));
			}
			if($unsaved>0){
				Yii::$app->getSession()->setFlash('error', $unsaved.' - '.Yii::t('app','Emails were not saved!').$errNames);
			}
			return true;
		} else {
			return false;
		}
	}
}
