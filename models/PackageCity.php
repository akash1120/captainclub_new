<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%package_city}}".
*
* @property integer $id
* @property string $package_id
* @property string $city_id
*/
class PackageCity extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%package_city}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['package_id','city_id'],'required'],
      [['package_id','city_id'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'package_id' => Yii::t('app', 'Package'),
      'city_id' => Yii::t('app', 'City'),
    ];
  }
}
