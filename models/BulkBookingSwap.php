<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * BulkBookingSwap form
 */
class BulkBookingSwap extends Model
{
  public $booking_id,$new_port_id,$new_time,$new_boat;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['booking_id', 'new_port_id', 'new_time', 'new_boat'], 'required'],
    	[['booking_id', 'new_port_id', 'new_time', 'new_boat'],'integer'],
    	[['new_boat'],'checkAlreadyBooked'],
    	[['booking_id'],'checkBoatStillBooked'],
    ];
  }

  /**
   * checks already booked
   */
	public function checkAlreadyBooked($attribute, $params)
	{
    $booking=Booking::find()->where(['id'=>$this->booking_id,'trashed'=>0])->asArray()->one();
    if($booking!=null){
      $result=Booking::find()->where(['and',['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],['city_id'=>$booking['city_id'],'port_id'=>$this->new_port_id,'boat_id'=>$this->new_boat,'booking_date'=>$booking['booking_date'],'booking_time_slot'=>$this->new_time,'trashed'=>0]]);
      if($result->exists()){
        $this->addError($attribute,'Selected Boat is already booked, Please choose another');
        return false;
      }
    }
    return true;
	}

  /**
   * checks still booked
   */
	public function checkBoatStillBooked($attribute, $params)
	{
    $booking=Booking::find()->where(['id'=>$this->booking_id])->asArray()->one();
    if($booking!=null && $booking['trashed']==1){
      $this->addError($attribute,'Booking deleted by member');
      return false;
    }
    return true;
	}

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'booking_id' => Yii::t('app', 'Booking'),
      'new_port_id' => Yii::t('app', 'Marina'),
      'new_time' => Yii::t('app', 'Time'),
      'new_boat' => Yii::t('app', 'Boat'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBooking()
  {
      return Booking::find()->where(['id'=>$this->booking_id,'trashed'=>0])->one();
  }

  /**
   * @return boolean whether the model passes validation
   */
  public function save()
  {
    if ($this->validate()) {

      //Bulk Book
      $memberBooking=$this->booking;
      $BulkBooking = BulkBooking::find()->where(['and',['boat_id'=>$memberBooking->boat_id,'trashed'=>0],['<=','start_date',$memberBooking->booking_date],['>=','end_date',$memberBooking->booking_date]])->asArray()->one();
      if($BulkBooking!=null){

        $adminBookingID=0;
        $result=Booking::find()->where(['city_id'=>$memberBooking->city_id,'port_id'=>$this->new_port_id,'boat_id'=>$this->new_boat,'booking_date'=>$memberBooking->booking_date,'booking_time_slot'=>$this->new_time,'trashed'=>0,'user_id'=>Yii::$app->appHelperFunctions->adminIdz])->asArray()->one();
        if($result!=null){
          $adminBookingID=$result['id'];
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set trashed=1 where id=:id",[':id'=>$adminBookingID])->execute();
        }

        //Change the boat of user
        $connection = \Yii::$app->db;
        $connection->createCommand(
          "update ".Booking::tableName()." set port_id=:port_id,boat_id=:boat_id,booking_time_slot=:time_id,booking_exp=:booking_exp where id=:id",
          [
            ':port_id' => $this->new_port_id,
            ':boat_id' => $this->new_boat,
            ':time_id' => $this->new_time,
            ':booking_exp' => 2,
            ':id' => $this->booking_id,
          ]
        )
        ->execute();
        if($adminBookingID>0){
          //Log admin booking as shifted to member
          $adminBoatAssignment=new AdminBoatAssignment;
          $adminBoatAssignment->from_booking=$adminBookingID;
          $adminBoatAssignment->to_booking=$this->booking_id;
          $adminBoatAssignment->save();
        }

        //Book boat for bulk booking
      	$booking=new Booking;
        $booking->booking_source='bulk';
        $booking->user_id=$BulkBooking['created_by'];
  			$booking->city_id=$memberBooking->city_id;
  			$booking->port_id=$memberBooking->port_id;
  			$booking->boat_id=$memberBooking->boat_id;
  			$booking->booking_date=$memberBooking->booking_date;
  			$booking->booking_time_slot=$memberBooking->booking_time_slot;
  			$booking->booking_type=$BulkBooking['booking_type'];
  			$booking->booking_comments=$BulkBooking['booking_comments'];
  			$booking->is_bulk=1;
  			if($booking->save()){
          //Log this booking was made against which member booking
          $bulkReplacement=new BulkBookingReplacement;
          $bulkReplacement->user_booking_id = $this->booking_id;
          $bulkReplacement->bulk_booking_id = $booking->id;
          $bulkReplacement->save();
          $bulkReplacement->sendEmail();
        }else{
          foreach($booking->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $this->addError('',$val);
              }
            }
          }
          return false;
        }
      }else{
        $this->addError('',"No Bulk Booking found!");
      }
      return true;
    }
    return false;
  }
}
