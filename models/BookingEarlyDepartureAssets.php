<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\web\UploadedFile;

/**
* This is the model class for table "{{%booking_early_departure_assets}}".
*
* @property integer $id
* @property integer $booking_id
* @property string $photo
* @property string $berth_no
* @property string $battery_switch
* @property string $registration_card_location
* @property string $keys_of_the_boat
* @property string $petro_guage
* @property string $comments
* @property string $staff_comments
*/
class BookingEarlyDepartureAssets extends ActiveRecord
{
  public $photoFile,$oldPhoto;
  public $batterySwitchFile,$oldBatterySwitch;
  public $regCardLocationFile,$oldRegCardLocation;
  public $keysOfBoatFile,$oldKeysOfBoat;
  public $petroGuageFile,$oldPetroGuage;
  public $allowedImageSize = 10048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_early_departure_assets}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['booking_id','berth_no'],'required'],
      [['booking_id','created_by','updated_by'],'integer'],
      [['berth_no','comments','staff_comments'],'string'],
      [['photo','battery_switch','registration_card_location','keys_of_the_boat','petro_guage'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'photo' => Yii::t('app', 'Boat Photo'),
      'berth_no' => Yii::t('app', 'Berth No'),
      'battery_switch' => Yii::t('app', 'Battery Switch'),
      'registration_card_location' => Yii::t('app', 'Registration Card Location'),
      'keys_of_the_boat' => Yii::t('app', 'Keys Of The Boat'),
      'petro_guage' => Yii::t('app', 'Petro Guage'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBooking()
  {
	   return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    die("I am here?");
    //Uploading Photo
    if(UploadedFile::getInstance($this, 'photo')){
      $this->photoFile = UploadedFile::getInstance($this, 'photo');
      // if no file was uploaded abort the upload
      if (!empty($this->photoFile)) {
        $pInfo=pathinfo($this->photoFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->photoFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('photo', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->photoFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->photo = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('photo', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('photo', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->photo==null && $this->oldPhoto!=null){
      $this->photo=$this->oldPhoto;
    }

    //Uploading Battery Switch
    if(UploadedFile::getInstance($this, 'battery_switch')){
      $this->batterySwitchFile = UploadedFile::getInstance($this, 'battery_switch');
      // if no file was uploaded abort the upload
      if (!empty($this->batterySwitchFile)) {
        $pInfo=pathinfo($this->batterySwitchFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->batterySwitchFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('battery_switch', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->batterySwitchFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->battery_switch = 'bs'.Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('battery_switch', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('battery_switch', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->battery_switch==null && $this->oldBatterySwitch!=null){
      $this->battery_switch=$this->oldBatterySwitch;
    }

    //Uploading Registration Card Location
    if(UploadedFile::getInstance($this, 'registration_card_location')){
      $this->regCardLocationFile = UploadedFile::getInstance($this, 'registration_card_location');
      // if no file was uploaded abort the upload
      if (!empty($this->regCardLocationFile)) {
        $pInfo=pathinfo($this->regCardLocationFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->regCardLocationFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('registration_card_location', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->regCardLocationFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->registration_card_location = 'rcl'.Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('registration_card_location', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('registration_card_location', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->registration_card_location==null && $this->oldRegCardLocation!=null){
      $this->registration_card_location=$this->oldRegCardLocation;
    }

    //Uploading Keys Of The Boat
    if(UploadedFile::getInstance($this, 'keys_of_the_boat')){
      $this->keysOfBoatFile = UploadedFile::getInstance($this, 'keys_of_the_boat');
      // if no file was uploaded abort the upload
      if (!empty($this->keysOfBoatFile)) {
        $pInfo=pathinfo($this->keysOfBoatFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->keysOfBoatFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('keys_of_the_boat', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->keysOfBoatFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->keys_of_the_boat = 'bk'.Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('keys_of_the_boat', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('keys_of_the_boat', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->keys_of_the_boat==null && $this->oldKeysOfBoat!=null){
      $this->keys_of_the_boat=$this->oldKeysOfBoat;
    }

    //Uploading Petro Guage
    if(UploadedFile::getInstance($this, 'petro_guage')){
      $this->petroGuageFile = UploadedFile::getInstance($this, 'petro_guage');
      // if no file was uploaded abort the upload
      if (!empty($this->petroGuageFile)) {
        $pInfo=pathinfo($this->petroGuageFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->petroGuageFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('petro_guage', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->petroGuageFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->petro_guage = 'bk'.Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('petro_guage', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('petro_guage', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->petro_guage==null && $this->oldPetroGuage!=null){
      $this->petro_guage=$this->oldPetroGuage;
    }

    return parent::beforeValidate();
  }

  /**
  * @inheritdoc
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      if($this->photo!='' && !filter_var($this->photo, FILTER_VALIDATE_URL)){
        $this->photoFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->photo);
        //$this->photo=Yii::$app->fileHelperFunctions->copyToAwsAndRemoveLocal(Yii::$app->params['early_depart_asset_abs_path'],$this->photo);
      }
      if($this->battery_switch!='' && !filter_var($this->battery_switch, FILTER_VALIDATE_URL)){
        $this->batterySwitchFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->battery_switch);
        //$this->battery_switch=Yii::$app->fileHelperFunctions->copyToAwsAndRemoveLocal(Yii::$app->params['early_depart_asset_abs_path'],$this->battery_switch);
      }
      if($this->registration_card_location!='' && !filter_var($this->registration_card_location, FILTER_VALIDATE_URL)){
        $this->regCardLocationFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->registration_card_location);
        //$this->registration_card_location=Yii::$app->fileHelperFunctions->copyToAwsAndRemoveLocal(Yii::$app->params['early_depart_asset_abs_path'],$this->registration_card_location);
      }
      if($this->keys_of_the_boat!='' && !filter_var($this->keys_of_the_boat, FILTER_VALIDATE_URL)){
        $this->keysOfBoatFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->keys_of_the_boat);
        //$this->keys_of_the_boat=Yii::$app->fileHelperFunctions->copyToAwsAndRemoveLocal(Yii::$app->params['early_depart_asset_abs_path'],$this->keys_of_the_boat);
      }
      if($this->petro_guage!='' && !filter_var($this->petro_guage, FILTER_VALIDATE_URL)){
        $this->petroGuageFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->petro_guage);
        //$this->petro_guage=Yii::$app->fileHelperFunctions->copyToAwsAndRemoveLocal(Yii::$app->params['early_depart_asset_abs_path'],$this->petro_guage);
      }
      return true;
    } else {
      return false;
    }
  }


  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    /*if ($this->photoFile!== null && $this->photo!=null) {
      if($this->oldPhoto!=null && $this->photo!=$this->oldPhoto && file_exists(Yii::$app->params['early_depart_asset_abs_path'].$this->oldPhoto)){
        unlink(Yii::$app->params['early_depart_asset_abs_path'].$this->oldPhoto);
      }
      $this->photoFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->photo);
    }

    if ($this->batterySwitchFile!== null && $this->battery_switch!=null) {
      if($this->oldBatterySwitch!=null && $this->battery_switch!=$this->oldBatterySwitch && file_exists(Yii::$app->params['early_depart_asset_abs_path'].$this->oldBatterySwitch)){
        unlink(Yii::$app->params['early_depart_asset_abs_path'].$this->oldBatterySwitch);
      }
      $this->batterySwitchFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->battery_switch);
    }

    if ($this->regCardLocationFile!== null && $this->registration_card_location!=null) {
      if($this->oldRegCardLocation!=null && $this->registration_card_location!=$this->oldRegCardLocation && file_exists(Yii::$app->params['early_depart_asset_abs_path'].$this->oldRegCardLocation)){
        unlink(Yii::$app->params['early_depart_asset_abs_path'].$this->oldRegCardLocation);
      }
      $this->regCardLocationFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->registration_card_location);
    }

    if ($this->keysOfBoatFile!== null && $this->keys_of_the_boat!=null) {
      if($this->oldKeysOfBoat!=null && $this->keys_of_the_boat!=$this->oldKeysOfBoat && file_exists(Yii::$app->params['early_depart_asset_abs_path'].$this->oldKeysOfBoat)){
        unlink(Yii::$app->params['early_depart_asset_abs_path'].$this->oldKeysOfBoat);
      }
      $this->keysOfBoatFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->keys_of_the_boat);
    }

    if ($this->petroGuageFile!== null && $this->petro_guage!=null) {
      if($this->oldPetroGuage!=null && $this->petro_guage!=$this->oldPetroGuage && file_exists(Yii::$app->params['early_depart_asset_abs_path'].$this->oldPetroGuage)){
        unlink(Yii::$app->params['early_depart_asset_abs_path'].$this->oldPetroGuage);
      }
      $this->petroGuageFile->saveAs(Yii::$app->params['early_depart_asset_abs_path'].$this->petro_guage);
    }*/

    if($insert){
      $this->sendEmail();
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function sendEmail()
  {
    $booking=$this->booking;
    $textBody = "Dear captain ".$booking->member->fullname.",\n\r\n\r";
		$textBody.= "Please click on the link below to find all the details required for your early departure.\n\r\n\r";
		$textBody.= "".Yii::$app->urlManager->createAbsoluteUrl(['site/early-departure','id'=>$booking->id])."\n\r\n\r";

		$textBody.= "Enjoy Boating With Relief\n\r";
		$textBody.= "Best Regards\n\r\n\r";

		$htmlBody = "Dear captain ".$booking->member->fullname.",<br /><br />";
		$htmlBody.= "Please click on the link below to find all the details required for your early departure.<br /><br />";
		$htmlBody.= "".Yii::$app->urlManager->createAbsoluteUrl(['site/early-departure','id'=>$booking->id])."<br /><br />";

		$htmlBody.= "Enjoy Boating With Relief<br />";
		$htmlBody.= "Best Regards<br /><br />";

		$message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
		->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
		->setSubject('Early Departure Details - ' . Yii::$app->params['siteName'])
		->setTo($booking->member->email)
		->send();

		$smsMsg="Dear Captain\n
		Please click on the link below to find all the details required for your early departure.\n
		".Yii::$app->urlManager->createAbsoluteUrl(['site/early-departure','id'=>$booking->id]).".\n
		M: ".Yii::$app->params['smsSigNumber']."
		E: ".Yii::$app->params['smsSigEmail']."";

		if($booking->member->profileInfo->profile_updated==1 && $booking->member->profileInfo->mobile!=null && $booking->member->profileInfo->mobile!=''){
			$mobileNumber=Yii::$app->helperFunctions->fullMobileNumber($booking->member->profileInfo->mobile);
			if($mobileNumber!=null && $mobileNumber!=''){
				Yii::$app->helperFunctions->sendSms($mobileNumber,$smsMsg);
			}
		}
  }
}
