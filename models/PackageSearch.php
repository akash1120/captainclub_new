<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Package;

/**
* PackageSearch represents the model behind the search form of `app\models\Package`.
*/
class PackageSearch extends Package
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['name','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['name'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = Package::find()
    ->select([
      'id',
      'name',
      'status',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like','name',$this->name]);

    return $dataProvider;
  }
}
