<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%services}}".
*
* @property integer $id
* @property string $name
* @property string $keyword
*/
class PackageOption extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%services}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['name','keyword'],'required'],
      [['name','keyword'],'string'],
      [['name','keyword'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'name' => Yii::t('app', 'Title'),
      'keyword' => Yii::t('app', 'Keyword'),
    ];
  }
}
