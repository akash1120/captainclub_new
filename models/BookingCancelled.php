<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%booking_cancelled}}".
*/
class BookingCancelled extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_cancelled}}';
  }
}
