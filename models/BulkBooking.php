<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%bulk_booking}}".
*
* @property integer $id
* @property integer $boat_id
* @property string $start_date
* @property string $end_date
* @property string $old_end_date
* @property integer $booking_type
* @property string $booking_comments
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class BulkBooking extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%bulk_booking}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id', 'start_date', 'end_date', 'booking_type', 'booking_comments'], 'required'],
      [['switchedBookings', 'start_date', 'end_date', 'old_end_date', 'created_at', 'updated_at'], 'safe'],
      [['booking_comments'], 'string'],
      [['boat_id', 'booking_type', 'created_by', 'updated_by'], 'integer'],
			[['end_date'],'checkStartEndDate'],
    ];
  }

  /**
  * checks end date is greater then start date
  */
  public function checkStartEndDate($attribute, $params)
  {
    if($this->end_date<$this->start_date){
      $this->addError($attribute,'End date should be greater then start date');
      return false;
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_id' => Yii::t('app', 'Boat'),
      'start_date' => Yii::t('app', 'Start Date'),
      'end_date' => Yii::t('app', 'End Date'),
      'booking_type' => Yii::t('app', 'Type'),
      'booking_comments' => Yii::t('app', 'Comments'),
      'created_at' => Yii::t('app', 'Created On'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBoat()
  {
    return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
    if($insert){
      Yii::$app->mailer->compose(['html' => 'newBulkBooking-html', 'text' => 'newBulkBooking-text'], ['model' => $this])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setTo(Yii::$app->params['icareEmail'])
        ->setCc([Yii::$app->params['mdEmail']])
        ->setSubject('New Bulk Booking request placed')
        ->send();
    }
    parent::afterSave($insert, $changedAttributes);
  }

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
    if($this->end_date>=date("Y-m-d")){
      $connection = \Yii::$app->db;
      //if it is in future or today
      //Delete all bookings
      if($this->start_date>=date("Y-m-d")){
        $connection->createCommand(
          "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
          [
            ':trashed'=>1,
            ':trashed_at'=>date("Y-m-d H:i:s"),
            ':trashed_by'=>Yii::$app->user->identity->id,
            ':id'=>$this->id,
          ]
        )
        ->execute();
        /*$bookings=Booking::find()
          ->where([
            'and',
            ['is_bulk'=>1,'boat_id'=>$this->boat_id,'trashed'=>0],
            ['>=','booking_date',$this->start_date],
            ['<=','booking_date',$this->end_date],
          ])
          ->all();
        if($bookings!=null){
          foreach($bookings as $booking){
            $booking->softDelete();
          }
        }*/
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Bulk Booking ('.$this->start_date.' - '.$this->end_date.') trashed successfully');
      }else{
        //Is already active,
        //Delete only remaining days
        $connection->createCommand(
          "update ".self::tableName()." set old_end_date=end_date where id=:id",
          [
            ':id'=>$this->id
          ]
        )->execute();
        $connection->createCommand(
          "update ".self::tableName()." set end_date=:end_date where id=:id",
          [
            ':end_date'=>date("Y-m-d",mktime(0,0,0,date("m"),(date("d")-1),date("Y"))),
            ':id'=>$this->id
          ]
        )->execute();
        /*$bookings=Booking::find()
          ->where([
            'and',
            ['is_bulk'=>1,'boat_id'=>$this->boat_id,'trashed'=>0],
            ['>=','booking_date',date("Y-m-d")],
            ['<=','booking_date',$this->end_date],
          ])
          ->all();
        if($bookings!=null){
          foreach($bookings as $booking){
            $booking->softDelete();
          }
        }*/
      }
    }
		return true;
	}
}
