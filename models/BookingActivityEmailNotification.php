<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%booking_activity_email_notification}}".
*
* @property integer $id
* @property integer $booking_id
* @property string $email_type
* @property string $email_to
* @property integer $status
* @property string $remarks
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class BookingActivityEmailNotification extends ActiveRecord
{
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%booking_activity_email_notification}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['booking_id','email_type','email_to'], 'required'],
			[['created_at', 'updated_at'], 'safe'],
			[['booking_id', 'status', 'created_by', 'updated_by'], 'integer'],
			[['email_type', 'email_to', 'remarks'], 'string']
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'booking_id' => Yii::t('app', 'Booking'),
			'email_type' => Yii::t('app', 'Email Type'),
			'email_to' => Yii::t('app', 'To'),
			'status' => Yii::t('app', 'Status'),
			'remarks' => Yii::t('app', 'Remarks'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
		];
	}
}
