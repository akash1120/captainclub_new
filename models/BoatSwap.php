<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
 * This is the model class for table "{{%boat_swap}}".
 *
 * @property integer $id
 * @property integer $booking_id
 * @property integer $old_port_id
 * @property integer $old_time
 * @property integer $old_boat
 * @property integer $new_port_id
 * @property integer $new_time
 * @property integer $new_boat
 * @property integer $reason
 */
class BoatSwap extends ActiveRecord
{
  public $user_id,$username,$tmp_date;
  public $selected_boat=0;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
      return '{{%boat_swap}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id', 'booking_id', 'new_port_id', 'new_time', 'new_boat', 'reason'], 'required'],
    	[['user_id', 'booking_id', 'old_port_id','old_time','old_boat', 'new_port_id', 'new_time','new_boat','reason','selected_boat'],'integer'],
    	[['booking_id'],'checkBoats'],
      [['new_boat'],'checkAlreadyBooked'],
    ];
  }

  /**
   * checks booking, if swapped is same give error
   */
	public function checkBoats($attribute, $params)
	{
    $result=Booking::find()->where(['id'=>$this->booking_id])->asArray()->one();
    if($result!=null){
      if($result['boat_id']==$this->new_boat && $result['booking_time_slot']==$this->new_time){
        $this->addError($attribute,'Please swap boat, new boat selected is same as old');
        return false;
      }
    }else{
      $this->addError($attribute,'No booking found!');
      return false;
    }
	}

  /**
   * checks already booked
   */
	public function checkAlreadyBooked($attribute, $params)
	{
    $booking=Booking::find()->where(['id'=>$this->booking_id,'trashed'=>0])->asArray()->one();
    if($booking!=null){
      $result=Booking::find()->where(['and',['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],['city_id'=>$booking['city_id'],'port_id'=>$this->new_port_id,'boat_id'=>$this->new_boat,'booking_date'=>$booking['booking_date'],'booking_time_slot'=>$this->new_time,'status'=>1,'trashed'=>0]]);
      if($result->exists()){
        $this->addError($attribute,'Boat is already booked, Please choose another');
        return false;
      }
    }
    return true;
	}

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'user_id' => Yii::t('app', 'Member'),
      'booking_id' => Yii::t('app', 'Booking'),
      'old_port_id' => Yii::t('app', 'Marina'),
      'old_boat' => Yii::t('app', 'Old Boat'),
      'new_port_id' => Yii::t('app', 'Marina'),
      'new_boat' => Yii::t('app', 'New Boat'),
      'reason' => Yii::t('app', 'Reason'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
        return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
    ];
  }

  /**
   * @inheritdoc
   */
  public function beforeValidate()
	{
    if($this->booking_id!=null){
      $booking=Booking::find()->where(['id'=>$this->booking_id,'trashed'=>0])->asArray()->one();
      if($booking!=null){
        $this->old_port_id=$booking['port_id'];
        $this->old_time=$booking['booking_time_slot'];
        $this->old_boat=$booking['boat_id'];
      }else{
        return false;
      }
    }else{
      return false;
    }
		return parent::beforeValidate();
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
		if($insert){
      $connection = \Yii::$app->db;
      $logMsg='Boat Swap:';
      //check if booked by admin
      $adminBookingID=0;
      $booking=Booking::find()->where(['id'=>$this->booking_id,'trashed'=>0])->asArray()->one();
      if($booking!=null){
        $result=Booking::find()->where(['city_id'=>$booking['city_id'],'port_id'=>$this->new_port_id,'boat_id'=>$this->new_boat,'booking_date'=>$booking['booking_date'],'booking_time_slot'=>$this->new_time,'trashed'=>0,'user_id'=>Yii::$app->appHelperFunctions->adminIdz])->asArray()->one();
        if($result!=null){
          $adminBookingID=$result['id'];
          $connection->createCommand("update ".Booking::tableName()." set trashed=1 where id=:id",[':id'=>$adminBookingID])->execute();
        }
        $logMsg.='Member('.$booking['user_id'].'), Booking:'.$this->booking_id.', City:'.$booking['city_id'].', Date:'.$booking['booking_date'].' ';
        $logMsg.='Before (Marina:'.$booking['port_id'].',Boat:'.$booking['boat_id'].',Time:'.$booking['booking_time_slot'].') ';
        $logMsg.='After (Marina:'.$this->new_port_id.',Boat:'.$this->new_boat.',Time:'.$this->new_time.') ';
        $logMsg.='Reason('.$this->reason.') = '.($this->reason==2 ? 'Original NA' : 'Request' ).' ';
      }
      $connection->createCommand("update ".Booking::tableName()." set port_id=:port_id,boat_id=:boat_id,booking_time_slot=:booking_time_slot,booking_exp=:booking_exp where id=:id",[':port_id'=>$this->new_port_id,':boat_id'=>$this->new_boat,':booking_time_slot'=>$this->new_time,':booking_exp'=>$this->reason,':id'=>$this->booking_id])->execute();
      if($this->booking->boat->special_boat==1 && $this->booking->boat->boat_selection==1 && $this->selected_boat!=null && $this->selected_boat>0){
        $bookingSpBSelection=BookingSpecialBoatSelection::find()->where(['booking_id'=>$this->booking_id])->one();
        if($bookingSpBSelection==null){
          $bookingSpBSelection=new BookingSpecialBoatSelection;
          $bookingSpBSelection->booking_id=$this->booking_id;
          $bookingSpBSelection->boat_id=$this->selected_boat;
          $bookingSpBSelection->save();
        }else{
          $connection->createCommand("update ".BookingSpecialBoatSelection::tableName()." set boat_id=:boat_id where booking_id=:booking_id",[':boat_id'=>$this->selected_boat,':booking_id'=>$this->booking_id])->execute();
        }
      }
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, $logMsg);
      if($adminBookingID>0){
        $adminBoatAssignment=new AdminBoatAssignment;
        $adminBoatAssignment->from_booking=$adminBookingID;
        $adminBoatAssignment->to_booking=$this->booking_id;
        $adminBoatAssignment->save();
      }
      if($this->reason==2){
        //Book old boat under operation head
        $booking=new Booking;
        $booking->booking_source='swap';
        $booking->user_id=Yii::$app->params['boatRequiredBookUserId'];
        $booking->city_id=$this->booking->city_id;
        $booking->port_id=$this->old_port_id;
        $booking->boat_id=$this->old_boat;
        $booking->booking_date=$this->booking->booking_date;
        $booking->booking_time_slot=$this->booking->booking_time_slot;
        $booking->booking_type=Yii::$app->params['boatRequiredBookType'];
        $booking->booking_comments='Swapped, Original Not Available';
        $booking->save();
      }else{
        Yii::$app->bookingHelperFunctions->checkBulkOrBoatRequired(
          $this->booking->city_id,
          $this->old_port_id,
          $this->old_boat,
          $this->booking->booking_date,
          $this->booking->booking_time_slot,
          'no'
        );
      }
      //Send Swap Mail
      $this->sendMail();
    }
		parent::afterSave($insert, $changedAttributes);
  }

  public function sendMail()
  {
    //Swap Email
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_boatswaped_u');
    $template=EmailTemplate::findOne($templateId);
    if($template!=null){
      $vals = [
        '{bookingOldMarina}' => $this->oldMarina->name,
        '{bookingOldTime}' => $this->oldTime->name,
        '{bookingOldBoat}' => $this->oldBoat->name,
        '{bookingDate}' => Yii::$app->formatter->asDate($this->booking->booking_date),
        '{bookingNewMarina}' => $this->newMarina->name,
        '{bookingNewTime}' => $this->newTime->name,
        '{bookingNewBoat}' => $this->newBoat->name,
        '{swapReason}' => Yii::$app->helperFunctions->boatSwapReason[$this->reason],
        '{swapReasonLine}' => Yii::$app->helperFunctions->boatSwapReasonDetail[$this->reason],
      ];
      $htmlBody=$template->searchReplace($template->template_html,$vals);
      $textBody=$template->searchReplace($template->template_text,$vals);
      Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setTo($this->booking->member->email)
        ->setSubject('Boat Swapped')
        ->send();
    }

    //send sms
    if($this->booking->member->profileInfo->profile_updated==1 && $this->booking->member->profileInfo->mobile!=null && $this->booking->member->profileInfo->mobile!=''){
      $mobileNumber=Yii::$app->helperFunctions->fullMobileNumber($this->booking->member->profileInfo->mobile);
      if($mobileNumber!=null && $mobileNumber!=''){
        $smsMsg="Dear Captain\n
Your booking for ".$this->oldBoat->name." from ".$this->oldMarina->name." on ".date("l, jS M Y",strtotime($this->booking->booking_date)).", Was swapped with ".$this->newBoat->name." from ".$this->newMarina->name." at ".$this->newTime->name.".\n
Remarks: ".Yii::$app->helperFunctions->boatSwapReason[$this->reason]."
".Yii::$app->helperFunctions->boatSwapReasonDetail[$this->reason]."\n
M: ".Yii::$app->params['smsSigNumber']."
E: ".Yii::$app->params['smsSigEmail']."";
        Yii::$app->helperFunctions->sendSms($mobileNumber,$smsMsg);
      }
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBooking()
  {
      return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getOldMarina()
  {
      return $this->hasOne(Marina::className(), ['id' => 'old_port_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getOldTime()
  {
      return $this->hasOne(TimeSlot::className(), ['id' => 'old_time']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getOldBoat()
  {
      return $this->hasOne(Boat::className(), ['id' => 'old_boat']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getNewMarina()
  {
      return $this->hasOne(Marina::className(), ['id' => 'new_port_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getNewTime()
  {
      return $this->hasOne(TimeSlot::className(), ['id' => 'new_time']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getNewBoat()
  {
      return $this->hasOne(Boat::className(), ['id' => 'new_boat']);
  }
}
