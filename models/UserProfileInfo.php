<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%user_profile_info}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $city_id
 * @property int $mobile
 * @property int $pref_marina_id
 * @property int $is_licensed
 * @property string $package_name
 * @property int $security_deposit
 * @property string $deposit_type
 * @property int $credit_balance
 * @property int $save_cc
 * @property string $note
 * @property int $changed_pass
 * @property int $profile_updated
 * @property int $mail_subscribed
 * @property string $un_subscribe_reason
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class UserProfileInfo extends ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%user_profile_info}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id'], 'required'],
      [['user_id','city_id','mobile','pref_marina_id','is_licensed','save_cc','changed_pass','profile_updated','mail_subscribed','created_by','updated_by'], 'integer'],
      [['package_name','deposit_type','note','un_subscribe_reason'], 'string'],
      [['security_deposit','credit_balance'], 'number'],
      [['user_id','city_id','mobile','pref_marina_id','is_licensed','save_cc','changed_pass','profile_updated','mail_subscribed'], 'default', 'value' => 0],
      [['created_at','updated_at'], 'safe'],
      [['package_name'], 'filter', 'filter' => 'trim'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCity()
  {
    return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPreferredMarina()
  {
    return $this->hasOne(Marina::className(), ['id' => 'pref_marina_id']);
  }
}
