<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%package_group}}".
*
* @property integer $id
* @property string $title
* @property integer $sort_order
* @property integer $status
*/
class PackageGroup extends ActiveRecord
{
	public $package_id;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%package_group}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['title','sort_order'],'required'],
      [['sort_order','status','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['title'],'string'],
      [['title'],'trim'],
      ['status','default','value'=>1],
      [['package_id'],'each','rule'=>['integer']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'title' => Yii::t('app', 'Name'),
      'sort_order' => Yii::t('app', 'Display Order'),
      'package_id' => Yii::t('app', 'Packages'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		if($this->package_id!=null){
      PackageGroupToPackage::deleteAll(['and', 'package_group_id = :package_group_id', ['not in', 'package_id', $this->package_id]], [':package_group_id' => $this->id]);
			foreach($this->package_id as $key=>$val){
        $checkAlreadyAllowed=PackageGroupToPackage::find()->where(['package_group_id'=>$this->id,'package_id'=>$val])->one();
        if($checkAlreadyAllowed==null){
  				$allowedOption=new PackageGroupToPackage;
  				$allowedOption->package_group_id=$this->id;
  				$allowedOption->package_id=$val;
  				$allowedOption->save();
        }
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Package Group ('.$this->title.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

  /**
   * Mark record as deleted and hides fron list.
   * @return boolean
   */
	public function softDelete()
	{
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
      [
        ':trashed'=>1,
        ':trashed_at'=>date("Y-m-d H:i:s"),
        ':trashed_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Package Group ('.$this->title.') trashed successfully');
		return true;
	}
}
