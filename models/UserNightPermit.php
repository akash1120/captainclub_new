<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%user_night_permit}}".
*
* @property integer $id
* @property integer $user_id
* @property integer $port_id
*/
class UserNightPermit extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_night_permit}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','port_id'], 'required'],
      [['user_id','port_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
      'port_id' => Yii::t('app', 'Marina'),
    ];
  }
}
