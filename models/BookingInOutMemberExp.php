<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%booking_in_out_member_exp}}".
*/
class BookingInOutMemberExp extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_in_out_member_exp}}';
  }
}
