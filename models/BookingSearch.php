<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Booking;

/**
* BookingSearch represents the model behind the search form of `app\models\Booking`.
*/
class BookingSearch extends Booking
{
  public $listType,$pageSize,$sortBy;
  public $member_name,$is_licensed;
  public $boat_provided,$booking_exp,$trip_exp,$member_exp,$booking_extra,$booking_damages;
  public $contract_id;
  public $report_trip_exp;
  public $search_source;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [[
        'id','user_id','is_licensed','city_id','port_id','boat_id','boat_provided','booking_time_slot',
        'booking_type','booking_exp','trip_exp','member_exp','booking_extra','booking_damages',
        'status','is_bulk','early_departure','is_archive','export','created_by','updated_by','trashed_by',
        'pageSize','contract_id',
      ],'integer'],
      [['booking_date','member_name','booking_comments','report_trip_exp','search_source','created_at','updated_at','trashed','trashed_at','sortBy'],'safe'],
      [['member_name'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generaQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);
    if(Yii::$app->controller->action->id=='activity'){
      $dataProvider->sort->defaultOrder = [
        'captain' => SORT_DESC,
        'sport_eqp_id' => SORT_DESC,
        'bbq' => SORT_DESC,
        'ice' => SORT_DESC,
        'kids_life_jacket' => SORT_DESC,
        'early_departure' => SORT_DESC,
        'late_arrival' => SORT_DESC,
        'overnight_camping' => SORT_DESC,
      ];
    }else{
      $dataProvider->sort->defaultOrder = ['booking_date'=>($this->listType=='future' ? SORT_ASC : SORT_DESC)];
    }


    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchUnClosed($params)
  {
    $query = $this->generaQuery($params);
    $subQueryNonClosed=BookingActivity::find()->select(['booking_id'])->where(['hide_other'=>0]);

    $subQueryClosed=BookingActivity::find()->select(['booking_id'])->where(['hide_other'=>1]);
    $subQueryNotUpdated=Booking::find()->select(['id'])->where(['status'=>1])->andWhere(['not in','id',$subQueryClosed]);
    $query->andWhere([
      'or',
      [Booking::tableName().'.id'=>$subQueryNonClosed],
      [Booking::tableName().'.id'=>$subQueryNotUpdated],
    ]);
    $query->andWhere([
      'and',
      ['<=',Booking::tableName().'.booking_date',date("Y-m-d")],
      [Booking::tableName().'.status'=>1],
    ]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchReport($params)
  {
    $query = $this->generaQuery($params);
    $query->leftJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id");

    // grid filtering conditions
    $query->andFilterWhere([
      Booking::tableName().'.booking_exp' => $this->booking_exp,
      BookingActivity::tableName().'.trip_exp' => $this->trip_exp,
      BookingActivity::tableName().'.member_exp' => $this->member_exp,
    ]);
    if($this->report_trip_exp!=null){
      if(strpos($this->report_trip_exp,"booking") !== false){
        $query->andWhere([
          Booking::tableName().'.status' => str_replace("booking","",$this->report_trip_exp),
        ]);
      }
      if(strpos($this->report_trip_exp,"trip") !== false){
        $query->andWhere([
          BookingActivity::tableName().'.trip_exp' => str_replace("trip","",$this->report_trip_exp),
        ]);
      }
    }

    if($this->booking_extra!=null){
      $subQueryAddons=BookingActivityAddons::find()->select(['booking_id'])->where(['addon_id'=>$this->booking_extra]);
      $query->andWhere([Booking::tableName().'.id'=>$subQueryAddons]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort'=> ['defaultOrder' => ['booking_date'=> SORT_DESC]]
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchForClaim($params)
  {
    $query = $this->generaQuery($params);
    $query->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id");

    // grid filtering conditions
    $query->andFilterWhere([
      BookingActivity::tableName().'.hide_other' => 1,
			BookingActivity::tableName().'.claimed' => 0,
			BookingActivity::tableName().'.approved' => 0,
    ]);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
      'sort'=> ['defaultOrder' => ['booking_date'=> SORT_DESC]]
    ]);

    return $dataProvider;
  }

  public function generaQuery($params)
  {
    $this->load($params);
    $query = Booking::find()
    ->leftJoin(User::tableName(),User::tableName().".id=".Booking::tableName().".user_id")
    ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id");

    // grid filtering conditions
    $query->andFilterWhere([
      Booking::tableName().'.id' => $this->id,
      Booking::tableName().'.city_id' => $this->city_id,
      Booking::tableName().'.port_id' => $this->port_id,
      Booking::tableName().'.boat_id' => $this->boat_id,
      Booking::tableName().'.booking_time_slot' => $this->booking_time_slot,
      Booking::tableName().'.booking_type' => $this->booking_type,
      Booking::tableName().'.status' => $this->status,
      Booking::tableName().'.is_bulk' => $this->is_bulk,
      Booking::tableName().'.early_departure' => $this->early_departure,
      UserProfileInfo::tableName().'.is_licensed' => $this->is_licensed,
    ]);
    if($this->contract_id!=null){
      $contractMembers=ContractMember::find()->select(['user_id'])->where(['contract_id'=>$this->contract_id]);
      $contract=Contract::find()->select(['start_date','end_date'])->where(['id'=>$this->contract_id])->asArray()->one();
      $query->andFilterWhere([Booking::tableName().'.user_id' => $contractMembers,]);
      $query->andWhere(['and',['>=',Booking::tableName().'.booking_date',$contract['start_date']],['<=',Booking::tableName().'.booking_date',$contract['end_date']]]);
    }else{
      if($this->user_id!=null){
        $user=User::find()->where(['id'=>$this->user_id])->one();
        $query->andWhere([Booking::tableName().'.user_id' => $user->memberIdz,]);
      }
    }

    if($this->listType=='future'){
      $query->andWhere(['>=', 'booking_date', date("Y-m-d")]);
      $query->andWhere([Booking::tableName().'.trashed'=>0]);
    }elseif($this->listType=='archived'){
      $query->andWhere([Booking::tableName().'.is_archive'=>1]);
    }elseif($this->listType=='history'){
      $query->andWhere(['<', 'booking_date', date("Y-m-d")]);
      $query->andWhere([Booking::tableName().'.trashed'=>0]);
    }elseif($this->listType=='deleted'){
      $query->andWhere([Booking::tableName().'.trashed'=>1]);
    }else{
      $query->andWhere([Booking::tableName().'.trashed'=>0]);
    }

    if($this->booking_date!=null){
      //Check if its range
      if(strpos($this->booking_date," - ")){
        list($start_date,$end_date)=explode(" - ",$this->booking_date);
        $query->andWhere(['and',['>=',Booking::tableName().'.booking_date',$start_date],['<=',Booking::tableName().'.booking_date',$end_date]]);
      }else{
        $query->andWhere([Booking::tableName().'.booking_date'=>$this->booking_date]);
      }
    }
    $memberOnly=false;
    $adminOnly=false;
    if($this->search_source!=""){
      foreach($this->search_source as $key=>$val){
        if($val=="mem"){
          $memberOnly=true;
        }
        if($val=="admin"){
          $adminOnly=true;
        }
        if($val=="bulk"){
          $query->andWhere([Booking::tableName().'.is_bulk'=>1]);
        }
      }
    }
    if($memberOnly==false || $adminOnly==false){
      if($memberOnly==true){
        $query->andWhere([Booking::tableName().'.user_id'=>User::find()->select(['id'])->where(['user_type'=>0])]);
      }
      if($adminOnly==true){
        $query->andWhere([Booking::tableName().'.is_bulk'=>0,Booking::tableName().'.user_id'=>User::find()->select(['id'])->where(['!=','user_type',0])]);
      }
    }

    $query->andFilterWhere([
      'or',
      ['like',User::tableName().'.username',$this->member_name],
      ['like',User::tableName().'.firstname',$this->member_name],
      ['like',User::tableName().'.lastname',$this->member_name],
    ])
    ->andFilterWhere(['like',Booking::tableName().'.booking_comments',$this->booking_comments]);

    return $query;
  }

  public function getTabItems()
  {
    if(Yii::$app->controller->id=='booking' && Yii::$app->controller->action->id=='all' && Yii::$app->user->identity->user_type!=0){
      return Yii::$app->helperFunctions->getAllBookingTabs($this);
    }elseif(Yii::$app->controller->id=='user' && Yii::$app->controller->action->id=='bookings' && Yii::$app->user->identity->user_type!=0){
      return Yii::$app->helperFunctions->getUserProfileBookingTabs($this);
    }else{
      return Yii::$app->helperFunctions->getUserBookingTabs($this);
    }
  }
}
