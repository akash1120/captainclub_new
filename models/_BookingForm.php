<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
* BookingForm is the model behind the booking search.
*/
class BookingForm extends Model
{
  public $city_id,$port_id,$date,$booking_type,$booking_comments,$pageSize;
  public $totalSessions,$availableSessions;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['city_id','date'], 'required'],
      [['city_id','date','booking_type','booking_comments'], 'required', 'on'=>['admbook']],
      [['city_id','port_id','booking_type','pageSize'],'integer'],
      [['date','booking_comments'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'city_id' => 'City',
      'date' => 'Date',
      'booking_type' => 'Type',
      'booking_comments' => 'Comments',
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    if($this->load($params) && $this->city_id!=null){
      if($this->port_id==null){
        $this->port_id=Yii::$app->appHelperFunctions->getFirstActiveCityMarina($this->city_id)['id'];
      }
      $query = Boat::find();
      // grid filtering conditions
      $query->andFilterWhere([
        'city_id' => $this->city_id,
        'port_id' => $this->port_id,
        'status' => 1,
        'trashed' => 0,
      ]);
    }else{
      $query = Boat::find()
      ->asArray()
      ->where(['id'=>-1]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort'=> ['defaultOrder' => ['special_boat'=>SORT_DESC,'rank'=>SORT_ASC]],
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    return $dataProvider;
  }

  public function getTabItems()
  {
    $marinas=Yii::$app->appHelperFunctions->getActiveCityMarinaList($this->city_id);
    $html='';
    if($marinas!=null){
      foreach($marinas as $marina){
        if($this->port_id==null)$this->port_id=$marina['id'];

        $tabUrl=Url::to(['create','BookingForm[city_id]'=>$this->city_id,'BookingForm[port_id]'=>$marina['id'],'BookingForm[date]'=>$this->date]);


        $totalSessions=Yii::$app->statsFunctions->getMarinaTotalSessions($marina['id'],$this->date);
        $unAvailableSessions=Yii::$app->statsFunctions->getMarinaUnavailableSessions($marina['id'],$this->date);
        $availableSessions=($totalSessions-$unAvailableSessions);

        if($this->port_id==$marina['id']){
          $this->totalSessions=$totalSessions;
          $this->availableSessions=$availableSessions;
        }

        $html.='
        <li class="nav-item'.($this->port_id==$marina['id'] ? ' active' : '').'">
          <a class="nav-link" href="'.$tabUrl.'"><i class="fa fa-anchor"></i> '.$marina['short_name'].' ('.$availableSessions.' available out of '.$totalSessions.')</a>
        </li>';
      }
    }
    return $html;
  }
}
