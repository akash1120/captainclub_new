<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%waiting_list_overnight_camping}}".
*
* @property integer $user_id
* @property integer $booking_id
* @property integer $city_id
* @property integer $marina_id
* @property string $date
* @property integer $time_id
* @property integer $status
* @property integer $reason
* @property integer $remarks
* @property integer $email_message
* @property integer $admin_action_date
* @property integer $active_show_till
* @property integer $expected_reply
*/
class WaitingListOvernightCamping extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%waiting_list_overnight_camping}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','booking_id','city_id','marina_id','date','time_id','status'],'required'],
      [['user_id','booking_id','city_id','marina_id','time_id','status','created_by','updated_by'],'integer'],
      ['status','default','value'=>0],
      [['date','admin_action_date','active_show_till','expected_reply'],'safe'],
      [['reason','remarks','email_message'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'user_id' => Yii::t('app', 'Member'),
      'booking_id' => Yii::t('app', 'Booking'),
      'status' => Yii::t('app', 'Status'),
      'expected_reply' => Yii::t('app', 'Estimated Time Of Reply'),
    ];
  }

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCreatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
   * @return string, name of user
   */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
	   return $this->createdByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUpdatedByUser()
  {
	   return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
   * @return string, name of user
   */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getBooking()
  {
	   return Booking::find()->where(['id'=>$this->booking_id])->one();
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMember()
  {
    return $this->booking->member;
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      if($this->status==0){
        $type='Overnight Camping';
        Yii::$app->helperFunctions->sendAminAlert($type,$this->booking);
        Yii::$app->helperFunctions->sendWaitingListEmail($type,$this->booking);
      }
    }
    parent::afterSave($insert, $changedAttributes);
  }
}
