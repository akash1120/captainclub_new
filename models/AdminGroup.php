<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%admin_group}}".
*
* @property integer $id
* @property string $title
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class AdminGroup extends ActiveRecord
{
	public $actions;

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%admin_group}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['actions', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
			[['created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
			[['title'], 'string', 'max' => 255],
      [['title'],'trim'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
   * Enable / Disable the record
   * @return boolean
   */
	public function updateStatus()
	{
    $status=1;
    if($this->status==1)$status=0;
		$connection = \Yii::$app->db;
		$connection->createCommand(
      "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
      [
        ':status'=>$status,
        ':updated_at'=>date("Y-m-d H:i:s"),
        ':updated_by'=>Yii::$app->user->identity->id,
        ':id'=>$this->id,
      ]
    )
    ->execute();
    ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'City ('.$this->title.') '.($status==1 ? 'enabled' : 'disabled').' successfully');
		return true;
	}

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		if($this->actions!=null){
			AdminGroupPermissions::deleteAll(['group_id'=>$this->id]);
			foreach($this->actions as $key=>$val){
				$permission=new AdminGroupPermissions;
				$permission->group_id=$this->id;
				$permission->menu_id=$val;
				$permission->is_allowed=1;
				$permission->save();
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}
}
