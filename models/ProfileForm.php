<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* ContactForm is the model behind the contact form.
*/
class ProfileForm extends Model
{
  public $firstname,$lastname,$mobile,$email;
  public $pref_marina_id;

  /**
  * @param  string                          $token
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  * @throws \yii\base\InvalidParamException if token is empty or not valid
  */
  public function __construct($config = [])
  {
    $user = Yii::$app->user->identity;
    $this->firstname = $user->firstname;
    $this->lastname = $user->lastname;
    $this->email = $user->email;
    $this->mobile = $user->profileInfo->mobile;
    $this->pref_marina_id = $user->profileInfo->pref_marina_id;
    parent::__construct($config);
  }

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['firstname', 'lastname', 'mobile', 'email'], 'required'],
      [['firstname', 'lastname','email'],'string'],
      ['email', 'email'],
      [['mobile','pref_marina_id'], 'integer'],
      [['firstname','lastname','email','mobile'], 'trim'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'pref_marina_id' => 'Preferred Marina',
    ];
  }

  /**
  * Save Profile Info
  */
  public function save()
  {
    if ($this->validate()) {
      $user=User::findOne(Yii::$app->user->identity->id);
      $user->firstname=$this->firstname;
      $user->lastname=$this->lastname;
      $user->email=$this->email;
      $user->save();

      $connection = \Yii::$app->db;
			$connection->createCommand("update ".UserProfileInfo::tableName()." set mobile=:mobile,profile_updated=1 where user_id=:id",[
        ':mobile'=>$this->mobile,
        ':id'=>Yii::$app->user->identity->id
      ])->execute();
      if($this->pref_marina_id!=null){
        $connection = \Yii::$app->db;
  			$connection->createCommand("update ".UserProfileInfo::tableName()." set pref_marina_id=:pref_marina_id,profile_updated=1 where user_id=:id",[
          ':pref_marina_id'=>$this->pref_marina_id,
          ':id'=>Yii::$app->user->identity->id
        ])->execute();
      }
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.Yii::$app->user->identity->name.' updated profile');
      return true;
    }
    return false;
  }
}
