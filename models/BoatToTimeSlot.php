<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%boat_to_time_slot}}".
*
* @property int $id
* @property int $boat_id
* @property int $time_slot_id
*/
class BoatToTimeSlot extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_to_time_slot}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id', 'time_slot_id'], 'required'],
      [['boat_id', 'time_slot_id'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'boat_id' => Yii::t('app', 'Boat ID'),
      'time_slot_id' => Yii::t('app', 'Time Slot ID'),
    ];
  }

  /**
  * Get boat row
  * @return \yii\db\ActiveQuery
  */
  public function getBoat()
  {
    return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }

  /**
  * Get time slot row
  * @return \yii\db\ActiveQuery
  */
  public function getTimeSlot()
  {
    return $this->hasOne(TimeSlot::className(), ['id' => 'time_slot_id']);
  }
}
