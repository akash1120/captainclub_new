<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%package_allowed_purpose}}".
*
* @property integer $package_id
* @property integer $purpose_id
*/
class PackageAllowedPurpose extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%package_allowed_purpose}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['package_id','purpose_id'], 'required'],
      [['package_id','purpose_id'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'package_id' => Yii::t('app', 'Boat'),
      'purpose_id' => Yii::t('app', 'Day'),
    ];
  }
}
