<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "activity_log".
 *
 * @property int $id
 * @property string $controller_id
 * @property string $action_id
 * @property string $message
 * @property string $user_ip
 * @property string $page_referrer
 * @property string $status
 */
class ActivityLog extends ActiveRecord
{
  const LOG_STATUS_SUCCESS = 'success';
  const LOG_STATUS_INFO = 'info';
  const LOG_STATUS_WARNING = 'warning';
  const LOG_STATUS_ERROR = 'error';

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%activity_log}}';
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

  /**
  * Adds a message to ActionLog model
  *
  * @param string $status The log status information
  * @param mixed $message The log message
  * @param int $uID The user id
  */
  public static function add($status = null, $message = null)
  {
      $model = Yii::createObject(__CLASS__);
      $model->controller_id = Yii::$app->requestedAction->controller->id;
      $model->action_id = Yii::$app->requestedAction->id;
      $model->message = ($message !== null) ? serialize($message) : null;
      $model->user_ip = Yii::$app instanceof \yii\web\Application ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
      $model->page_referrer = Yii::$app instanceof \yii\web\Application ? Yii::$app->request->referrer : 'Console';
      $model->status = $status;

      return $model->save();
  }

  /**
  * Get the current user ID
  *
  * @return int The user ID
  */
  public static function getUserID()
  {
    $user = Yii::$app->getUser();

    return $user && !$user->getIsGuest() ? $user->getId() : 0;
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'controller_id' => Yii::t('app', 'Controller'),
      'action_id' => Yii::t('app', 'Action'),
      'message' => Yii::t('app', 'Message'),
      'user_ip' => Yii::t('app', 'Ip'),
      'page_referrer' => Yii::t('app', 'Referrer'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created On'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated On'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }
}
