<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* RequestKidsLifeJacketForm is the model behind the kids life jacket request.
*/
class RequestKidsLifeJacketForm extends Model
{
  public $booking_id,$no_of_one_to_three_jackets,$no_of_four_to_twelve_jackets;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['booking_id'], 'required'],
      [['booking_id', 'no_of_one_to_three_jackets','no_of_four_to_twelve_jackets'], 'integer'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'no_of_one_to_three_jackets' => Yii::t('app', 'No. of kids age 1-3 years'),
      'no_of_four_to_twelve_jackets' => Yii::t('app', 'No. of kids age 4-12 years'),
    ];
  }

  public function save()
  {
    if ($this->validate()) {
      if($this->no_of_one_to_three_jackets=='' && $this->no_of_four_to_twelve_jackets==''){
        $this->addError('no_of_one_to_three_jackets', 'Please select the number of jackets according to their age');
        return false;
      }

      if(Yii::$app->user->identity->user_type==0){
        $user_id=Yii::$app->user->identity->id;
      }else{
        $user_id='';
      }
      $booking=Booking::find()->where(['id'=>$this->booking_id,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
      if($booking!=null){

        if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
          $this->addError('',Yii::$app->helperFunctions->shortNoticeKidsLifeJacketMsg);
          return false;
        }
        if($booking->kids_life_jacket==0){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set kids_life_jacket=1,no_of_one_to_three_jackets=:no_of_one_to_three_jackets,no_of_four_to_twelve_jackets=:no_of_four_to_twelve_jackets where id=:booking_id",[':no_of_one_to_three_jackets'=>$this->no_of_one_to_three_jackets,':no_of_four_to_twelve_jackets'=>$this->no_of_four_to_twelve_jackets,':booking_id'=>$booking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Kids Life Jackets Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          return true;
        }else{
          $this->addError('',Yii::t('app','You have already confirmed Kids Life Jacket Set for this booking'));
          return false;
        }
      }else{
        $this->addError('',Yii::t('app','No Booking found!'));
        return false;
      }
    }
    return false;
  }
}
