<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClaimOrder;

/**
* ClaimOrderSearch represents the model behind the search form about `app\models\ClaimOrder`.
*/
class ClaimOrderSearch extends ClaimOrder
{
  public $list_type,$date_range,$start_date,$end_date,$port_id;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id', 'port_id', 'created_by', 'updated_by'], 'integer'],
      [['list_type', 'ref_no', 'total_amount', 'date_range', 'start_date', 'end_date', 'created_at', 'updated_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = ClaimOrder::find()
      ->joinWith("user");

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => array_values(Yii::$app->params['pageSizeArray'])[3],
      ],
    ]);

    $dataProvider->setSort([
      'defaultOrder' => ['id'=>SORT_DESC],
      'attributes' => [
        'id',
        'ref_no',
        'user_id'=>[
          'asc' => [User::tableName().'.email' => SORT_ASC],
          'desc' => [User::tableName().'.email' => SORT_DESC],
        ],
        'total_amount',
      ]
    ]);

    $this->load($params);

    $query->andFilterWhere([
      'id' => $this->id,
      'user_id' => $this->user_id,
      'marina_id' => $this->port_id,
      ClaimOrder::tableName().'.status' => $this->status,
      ClaimOrder::tableName().'.created_by' => $this->created_by,
      ClaimOrder::tableName().'.created_at' => $this->created_at,
    ]);

    $query->andFilterWhere(['like', 'total_amount', $this->total_amount])
    ->andFilterWhere(['like', 'ref_no', $this->ref_no]);

    if($this->date_range!=null){
      //Check if its range
      if(strpos($this->date_range," - ")){
        list($start_date,$end_date)=explode(" - ",$this->date_range);
        $query->andWhere(['and',['>=','DATE('.ClaimOrder::tableName().'.created_at)',$start_date],['<=','DATE('.ClaimOrder::tableName().'.created_at)',$end_date]]);
      }else{
        $query->andWhere(['DATE('.ClaimOrder::tableName().'.created_at)'=>$this->date_range]);
      }
    }
    if($this->start_date!=''){
      $query->andFilterWhere(['>=','DATE('.ClaimOrder::tableName().'.created_at)',$this->start_date]);
    }

    if($this->end_date!=''){
      $query->andFilterWhere(['<=','DATE('.ClaimOrder::tableName().'.created_at)',$this->end_date]);
    }

    return $dataProvider;
  }
}
