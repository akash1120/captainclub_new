<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\helpers\Url;

/**
* This is the model class for table "{{%booking}}".
*
* @property integer $id
* @property string $title
* @property string $code
* @property string $text_color
* @property integer $status
*/
class Booking extends ActiveRecord
{
  public $booking_source='member';
  public $selected_boat;
  public $ignore_bulk_booking_check='no';
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id','city_id','port_id','boat_id','booking_date','booking_time_slot'],'required'],
      [['user_id','city_id','port_id','boat_id','booking_date','booking_time_slot', 'booking_type', 'booking_comments'],'required', 'on'=>'admbook'],
      [['user_id','city_id','port_id','boat_id','booking_time_slot','booking_type','captain','sport_eqp_id','bbq','is_archive','is_bulk','status','selected_boat','created_by','updated_by','trashed','trashed_by'],'integer'],
      [['booking_date','booking_comments','captain_type','booking_source'],'string'],
      [['booking_date','booking_comments'],'trim'],
      ['status','default','value'=>1],
      [['captain','sport_eqp_id','bbq'],'default','value'=>0],
      [['booking_date'], 'checkMinBalance'],
      [['booking_date'], 'checkLicenseExpiry'],
      [['booking_date'], 'checkNightDrivePermit'],
      [['booking_date'], 'checkHold'],
      [['booking_date'], 'isInPackageDuration'],
      [['booking_date'], 'isInHold'],
      [['booking_date'], 'isInCancel'],
      [['booking_date'], 'isBoatAvailable'],
      [['booking_date'], 'isInFreeze'],
      [['booking_date'], 'isBulkBooked'],
      [['booking_date'], 'cantBeBookedToday'],
      [['booking_date'], 'lateMorningBooking'],
      [['booking_date'], 'expiredSessionTime'],
      [['booking_date'], 'lateTime'],
      [['booking_date'], 'nightDriveSameDay'],
      [['booking_date'], 'isSpecialBoatTwo'],
      [['booking_date'], 'checkNoPrivilege'],
      [['booking_date'], 'checkAlready'],
      [['booking_date'], 'checkTimeZoneAllowed'],
      [['booking_date'], 'checkRollingBookings'],
      [['booking_date'], 'checkWeekDaysOnly'],
      [['booking_date'], 'checkOneWeekEndOnly'],
      [['booking_date'], 'checkDxbToAD'],
      [['booking_date'], 'checkADToDXB'],
      [['booking_date'], 'checkBronzePackage'],
      [['booking_date'], 'checkNightDriveLimit'],
      [['boat_id'], 'checkSelectedBoatNotBooked'],
    ];
  }

  /**
  * Validates If member has min balance to book
  */
  public function checkSelectedBoatNotBooked($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->boat->special_boat==1 && $this->boat->boat_selection==1){
        //Check If selected Boat is EmptyIterator
        if($this->selected_boat<=0){
          $this->addError('','Dear Captain, Please select a boat for your '.$this->boat->name);
          return false;
        }
        //Check if boat not already booked
        $selectedBoatId=$this->selected_boat;
        $queryCheck=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'boat_id'=>$selectedBoatId,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'status'=>1,'trashed'=>0])->count('id');
        if($queryCheck>0){
          $this->addError('','Sorry, selected boat is not available now');
          return false;
        }

        $boat=Boat::find()->where(['id'=>$selectedBoatId,'status'=>1,'trashed'=>0]);
        if(!$boat->exists()){
          $this->addError('','Sorry, selected boat is not available now');
          return false;
        }

        list($sy,$sm,$sd)=explode("-",$this->booking_date);
        $dayOfWeek=date("w",mktime(0,0,0,$sm,$sd,$sy));
        $checkAvailableDay=BoatAvailableDays::find()->where(['boat_id'=>$selectedBoatId,'available_day'=>$dayOfWeek]);
        if(!$checkAvailableDay->exists()){
          $this->addError('','Sorry, selected boat is not available at the moment');
          return false;
        }
        return true;
      }
    }
  }

  /**
  * Validates If booking is under bulk booking
  */
  public function checkNightDriveLimit($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        if($this->booking_time_slot==Yii::$app->params['nightDriveTimeSlot']){
          $ndAllowedLimitInMarina=$this->marina->night_drive_limit;
          $bookedCount=Booking::find()->where(['port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'is_bulk'=>0,'trashed'=>0])->count('id');
          //Check Limit
          if($bookedCount>=$ndAllowedLimitInMarina){
            $this->addError('','Sorry, selected boat is not available at the moment');
            return false;
          }

          //Check if this boat is already selected by special boat
          $subQueryActiveBooking=Booking::find()->select(['id'])->where(['port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'trashed'=>0]);
          $checkBooked=BookingSpecialBoatSelection::find()->where(['booking_id'=>$subQueryActiveBooking,'boat_id'=>$this->boat_id])->asArray()->one();
          if($checkBooked!=null){
            $this->addError('','Sorry, selected boat is not available at the moment');
            return false;
          }
        }
      }
    }
  }

  /**
  * Validates If booking is under bulk booking
  */
  public function isBulkBooked($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        $bulkBooked = BulkBooking::find()->where(['and',['boat_id'=>$this->boat_id,'trashed'=>0],['<=','start_date',$this->booking_date],['>=','end_date',$this->booking_date]])->one();
        if($bulkBooked!=null){
          $this->addError('','Sorry, selected boat is not available at the moment');
          return false;
        }
      }
    }
  }

  /**
  * Validates If member has min balance to book
  */
  public function checkMinBalance($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->canBookBoat==false){
        $userCredits=$this->member->profileInfo->credit_balance;
        $this->addError('','Dear Captain, Your current balance is '.$userCredits.' AED. Please deposit funds first. Enjoy Boating With Relief');
        return false;
      }
    }
  }

  /**
  * Validates If member has valid license
  */
  public function checkLicenseExpiry($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        $userLatestLicense=UserLicense::find()->select(['license_expiry'])->where(['user_id'=>$this->user_id,'city_id'=>$this->city_id,'trashed'=>0])->orderBy(['license_expiry'=>SORT_DESC])->asArray()->one();
        if($userLatestLicense!=null){
          if($this->booking_date>$userLatestLicense['license_expiry']){
            $this->addError('','<div style="text-align:left">
            Your License Expires on the '.Yii::$app->formatter->asDate($userLatestLicense['license_expiry']).',<br /><br />
            Please renew your license to continue your booking process.<br /><br />
            Please contact: Mrs Marie<br />
            Mobile #: 0521041705<br />
            Email: admin@thecaptainsclub.ae<br /><br />
            Sorry for the inconvenience.</div>');
            return false;
          }
        }else{
          $this->addError('','You License not found in our system. Please provide us with your license info');
          return false;
        }
      }
    }
  }

  /**
  * Validates If permit is required and user has it.
  */
  public function checkNightDrivePermit($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        $timeSlot=Yii::$app->appHelperFunctions->getTimeSlotById($this->booking_time_slot);
        if($timeSlot['permit_required']==1){
          $nightDrivePermit=true;
          $nightDrivePermission=UserNightPermit::find()->where(['user_id'=>$this->user_id,'port_id'=>$this->port_id])->asArray()->one();
          if($nightDrivePermission==null){
            $this->addError('','1 hour orientation required prior to boating at night for your safety and as per the terms and conditions. You can request a night drive training from the request panel.');
            return false;
          }
        }
      }
    }
  }

  /**
  * Validates If there is any hold warning placed.
  */
  public function checkHold($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $holdAlert=BookingAlert::find()
      ->innerJoin(BookingAlertTiming::tableName(),BookingAlertTiming::tableName().".alert_id=".BookingAlert::tableName().".id")
      ->where([
        'alert_type'=>2,
        'time_slot_id'=>$this->booking_time_slot,
        'port_id'=>$this->port_id,
        'alert_date'=>$this->booking_date,
        'trashed'=>0
      ])
      ->asArray()->one();
      if($holdAlert!=null){
        $this->addError('',nl2br($holdAlert['message']));
        return false;
      }
    }
  }

  /**
  * Validates the package date.
  * This method serves as the inline hold for expiry.
  */
  public function isInPackageDuration($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        $checkPackageDurations=Contract::find()
        ->innerJoin(ContractMember::tableName(),ContractMember::tableName().".contract_id=".Contract::tableName().".id")
        ->where([
          'and',
          [ContractMember::tableName().'.user_id'=>Yii::$app->user->identity->id,ContractMember::tableName().'.status'=>1],
          ['<=','start_date',$this->booking_date],
          ['>=','end_date',$this->booking_date],
        ]);
        if(!$checkPackageDurations->exists()){
          $this->addError('','Selected date is not covered in your package. new error');
          return false;
        }
        $isInsidePackageDates=true;
        if($this->booking_date>$this->member->maxPackageEndDate){
          $this->addError('','Selected date is not covered in your package.');
          return false;
        }
      }
      return true;
    }
  }

  /**
  * Validates the availability of boat.
  */
  public function isBoatAvailable($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $boat=Boat::find()->where(['id'=>$this->boat_id,'status'=>1,'trashed'=>0]);
      if(!$boat->exists()){
        $this->addError('','Sorry, selected boat is not available now');
        return false;
      }

      list($sy,$sm,$sd)=explode("-",$this->booking_date);
      $dayOfWeek=date("w",mktime(0,0,0,$sm,$sd,$sy));
      $checkAvailableDay=BoatAvailableDays::find()->where(['boat_id'=>$this->boat_id,'available_day'=>$dayOfWeek]);
      if(!$checkAvailableDay->exists()){
        $this->addError('','Sorry, selected boat is not available at the moment');
        return false;
      }
      return true;
    }
  }

  /**
  * Validates the hold date.
  * This method serves as the inline hold for expiry.
  */
  public function isInHold($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->packageHoldDate!=null && Yii::$app->user->identity->packageHoldDate!='' && $this->booking_date>=Yii::$app->user->identity->packageHoldDate){
        $this->addError('','Dear Captain, Your account is on hold for this date.');
        return false;
      }
      return true;
    }
  }

  /**
  * Validates the cancel date.
  * This method serves as the inline cancel for expiry.
  */
  public function isInCancel($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->packageCancelDate!=null && Yii::$app->user->identity->packageCancelDate!='' && $this->booking_date>=Yii::$app->user->identity->packageCancelDate){
        $this->addError('','Dear Captain, Your account is on cancel for this date.');
        return false;
      }
      return true;
    }
  }

  /**
  * Validates the freeze date.
  */
  public function isInFreeze($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $freeze_start=$this->member->freezeStartDate;
      $freeze_end=$this->member->freezeEndDate;

      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0 && $freeze_start!=null && $freeze_start!='' && $freeze_end!=null && $freeze_end!='' && $this->booking_date>=$freeze_start && $this->booking_date<=$freeze_end){
        $this->addError('',"Error! As per your request, you account is on freeze from ".Yii::$app->formatter->asDate($freeze_start)." To ".Yii::$app->formatter->asDate($freeze_end)."");
        return false;
      }
      return true;
    }
  }

  /**
  * Validates the not available for todays boat booking.
  */
  public function cantBeBookedToday($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        if($this->booking_date==date("Y-m-d") && in_array($this->boat_id,Yii::$app->appHelperFunctions->restrictedTodayBookingBoats)){
          $this->addError('',"Sorry for the inconvenience. It is a short notice to book ".$this->boat->name." for today.");
          return false;
        }
      }
      return true;
    }
  }

  /**
  * Validates the late morning booking.
  */
  public function lateMorningBooking($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0 && $this->booking_time_slot==1){
        if($this->booking_date==date("Y-m-d",strtotime("+1 day",strtotime(date("Y-m-d"))))){
          if(date("H")>date("H",mktime(Yii::$app->helperFunctions->lateMroningCheckTill,0,0,date("m"),date("d"),date("Y")))){
            $morningUserBooking=Booking::find()
            ->where([
              'and',
              [
                'city_id'=>$this->city_id,
                'port_id'=>$this->port_id,
                'booking_date'=>$this->booking_date,
                'booking_time_slot'=>$this->booking_time_slot,
                'status'=>1,
                'is_bulk'=>0,
                'trashed'=>0,
              ],
              ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
            ]);
            if(!$morningUserBooking->exists()){
              $this->addError('',"Sorry for the inconvenience. It is a short notice to book this session.");
              return false;
            }
          }
        }elseif($this->booking_date==date("Y-m-d")){
          $timeSlot=Yii::$app->appHelperFunctions->getTimeSlotById($this->booking_time_slot);
          if($timeSlot['available_till']!=null && date("H:i")<=$timeSlot['available_till']){
            $checkMorningBookingExists=Booking::find()
            ->where([
              'and',
              [
                'city_id'=>$this->city_id,
                'port_id'=>$this->port_id,
                'booking_date'=>$this->booking_date,
                'booking_time_slot'=>$this->booking_time_slot,
                'status'=>1,
                'is_bulk'=>0,
                'trashed'=>0,
              ],
              ['not in','user_id',Yii::$app->appHelperFunctions->adminIdz],
            ]);
            if(!$checkMorningBookingExists->exists()){
              $this->addError('',"Sorry for the inconvenience. It is a short notice to book this session.");
              return false;
            }
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates the expiry/already passed time for todays booking.
  */
  public function expiredSessionTime($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        if($this->booking_date==date("Y-m-d")){
          $timeSlot=Yii::$app->appHelperFunctions->getTimeSlotById($this->booking_time_slot);
          if($timeSlot['expiry']!=null && date("H:i")>=$timeSlot['expiry']){
            $this->addError('',"This session is already expired. Please try another session.<br />Best Regards");
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates the late time for todays booking.
  */
  public function lateTime($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        if($this->booking_date==date("Y-m-d")){
          $timeSlot=Yii::$app->appHelperFunctions->getTimeSlotById($this->booking_time_slot);
          if($timeSlot['available_till']!=null && date("H:i")>=$timeSlot['available_till']){
            $this->addError('',"Sorry for the inconvenience. It is a short notice to book this session.");
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates the night drive session for todays booking.
  */
  public function nightDriveSameDay($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        if($this->booking_date==date("Y-m-d") && $this->booking_time_slot==Yii::$app->params['nightDriveTimeSlot']){
          //Check if there is any night drive
          $nightDriveBooking=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'status'=>1,'trashed'=>0,'is_bulk'=>0]);
          if(!$nightDriveBooking->exists()){
            $this->addError('',"Sorry for the inconvenience. It is a short notice to book this session.");
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates the Special Boat Type2.
  */
  public function isSpecialBoatTwo($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0 && $this->boat->special_boat==2){
        $wakeSurfingTimes=[9,10,11,12];
        $wakeSurfingBoats=Boat::find()->select(['id'])->where(['special_boat'=>2]);
        //If booking date is tomorrow
        $tomorrowDate=Yii::$app->helperFunctions->getNextDay(date("Y-m-d"));
        if(($this->booking_date==$tomorrowDate && date("H")>=18) || ($this->booking_date==date("Y-m-d"))){
          $previousTimeBooking=Booking::find()
          ->where([
            'and',
            ['city_id'=>$this->city_id,'port_id'=>$this->port_id,'boat_id'=>$wakeSurfingBoats,'booking_date'=>$this->booking_date,'booking_time_slot'=>$wakeSurfingTimes,'trashed'=>0],
            ['<','booking_time_slot',$this->booking_time_slot],
          ]);
          if(!$previousTimeBooking->exists()){
            $this->addError('',"Dear Captain, Sorry for the inconvenience. It is a short notice to book a Wake Surfing in for ".Yii::$app->formatter->asDate($this->booking_date));
            return false;
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates the package allows booking.
  */
  public function checkNoPrivilege($attribute, $params)
  {
    if (!$this->hasErrors()) {
      if($this->booking_source=='member' && Yii::$app->user->identity->user_type==0){
        if(Yii::$app->user->identity->getPrivilege('onlydubai')!=true && Yii::$app->user->identity->getPrivilege('onlyabudhabi')!=true && Yii::$app->user->identity->getPrivilege('unlimitted')!=true){
          $this->addError('','Sorry your package does not allow booking in either of the cities.');
        }
      }
      return true;
    }
  }

  /**
  * Validates the unique booking.
  */
  public function checkAlready($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $queryCheck=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'boat_id'=>$this->boat_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'status'=>Yii::$app->helperFunctions->bookingActiveStatusForAvailability,'trashed'=>0])->count('id');
      if($queryCheck>0){
        if(in_array($this->boat_id,Yii::$app->appHelperFunctions->nightCruiseIds)){
          $this->addError('','Sorry Night cruise not available on this date. Please choose another date.');
        }elseif(in_array($this->boat_id,Yii::$app->appHelperFunctions->marinaStayInIds)){
          $this->addError('','Sorry All marina Stay in are booked!');
        }else{
          $this->addError('','Sorry this boat is already booked. Please select other boat!');
        }
        return false;
      }
      return true;
    }
  }

  /**
  * Validates allowed timezones.
  */
  public function checkTimeZoneAllowed($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $thisUser=Yii::$app->user->identity;
      if($this->booking_source=='member' && $thisUser->user_type==0){
        $packageUsers=$thisUser->memberIdz;
        $marinaStayInBoatIdz=Yii::$app->appHelperFunctions->marinaStayInIds;
        $dropOffBoatIdz=Yii::$app->appHelperFunctions->dropOffIds;
        $spBoatIdz=array_merge($marinaStayInBoatIdz,$dropOffBoatIdz);
        if(!in_array($this->boat_id,$spBoatIdz)){
          $selectedDateBookingCount=Booking::find()->where(['and',['booking_date'=>$this->booking_date,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['not in','boat_id',$spBoatIdz]])->count('id');
          //One time zone
          if($thisUser->getPrivilege('onetimezone')==true){
            if($selectedDateBookingCount>0){
              $this->addError('','Error! You have an existing booking on this date. Thank you');
            }
          }
          //Two time zone
          elseif($thisUser->getPrivilege('twotimezone')==true){
            if($selectedDateBookingCount>1){
              $this->addError('','You already made two bookings on this date and you can not book more');
            }
          }

          else{
            $this->addError('','You dont have any slots');
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates rolling bookings.
  */
  public function checkRollingBookings($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $thisUser=Yii::$app->user->identity;
      if($this->booking_source=='member' && $thisUser->user_type==0){
        if($thisUser->getPrivilege('unlimitted')==true){

        }else{
          if(!in_array($this->boat_id,[Yii::$app->appHelperFunctions->marinaStayInIds])){
            $packageUsers=$thisUser->memberIdz;
            $messageCount = Booking::find()->where(['and',['status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['>=','booking_date',date("Y-m-d")],['not in','boat_id',Yii::$app->appHelperFunctions->marinaStayInIds]])->count('id');//,Yii::$app->params['dropOffId']

            //3 Rolling bookings
            if($thisUser->getPrivilege('3rollingbooking')==true){
              if($messageCount>2){
                $this->addError('','You already made 3 bookings');
              }
            }

            //4 Rolling bookings
            elseif($thisUser->getPrivilege('4rollingbooking')==true){
              if($messageCount>3){
                $this->addError('','You already made 4 bookings');
              }
            }

            else{
              $this->addError('','You dont have any slots');
            }
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates Weekdays bookings.
  */
  public function checkWeekDaysOnly($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $thisUser=Yii::$app->user->identity;
      if($this->booking_source=='member' && $thisUser->user_type==0){
        $packageUsers=$thisUser->memberIdz;

        $isWeekend=Yii::$app->helperFunctions->isWeekend($this->booking_date);
        if($thisUser->getPrivilege('oneweekendday')!=true && $thisUser->getPrivilege('twoweekendday')!=true && $isWeekend==true){
          $this->addError('','Your package doest not allow booking on weekends');
        }
      }
      return true;
    }
  }

  /**
  * Validates One Weekend bookings.
  */
  public function checkOneWeekEndOnly($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $thisUser=Yii::$app->user->identity;
      if($this->booking_source=='member' && $thisUser->user_type==0){
        $packageUsers=$thisUser->memberIdz;
        //One Weekend booking
        if($thisUser->getPrivilege('oneweekendday')==true){
          $isWeekend=Yii::$app->helperFunctions->isWeekend($this->booking_date);
          if($isWeekend==true){
            $whichWeekendDay=Yii::$app->helperFunctions->WeekendDay($this->booking_date);
            list($y,$m,$d)=explode("-",$this->booking_date);

            $spBoatIdz=array_merge(Yii::$app->appHelperFunctions->marinaStayInIds,Yii::$app->appHelperFunctions->dropOffIds);
            if($whichWeekendDay==5){
              $row=Booking::find()->where(['and',['booking_date'=>date("Y-m-d",mktime(0,0,0,$m,($d+1),$y)),'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['not in','boat_id',$spBoatIdz]])->count('id');
              if($row>0){
                $this->addError('','You can only book one on weekend and you have a booking on saturday');
              }
            }elseif($whichWeekendDay==6){
              $row=Booking::find()->where(['and',['booking_date'=>date("Y-m-d",mktime(0,0,0,$m,($d-1),$y)),'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['not in','boat_id',$spBoatIdz]])->count('id');
              if($row>0){
                $this->addError('','You can only book one on weekend and you have a booking on friday');
              }
            }
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates Dxb to AD all days.
  */
  public function checkDxbToAD($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $timeslot=$this->booking_time_slot;
      $thisUser=Yii::$app->user->identity;
      if($this->booking_source=='member' && $thisUser->user_type==0){
        $packageUsers=$thisUser->memberIdz;

        //Only Dubai Allowed
        if($thisUser->getPrivilege('onlydubai')==true){
          $dubaiCity=Yii::$app->appHelperFunctions->cityIdz['onlydubai'];
          if($this->city_id!=$dubaiCity){
            $spBoatIdz=array_merge(Yii::$app->appHelperFunctions->marinaStayInIds,Yii::$app->appHelperFunctions->dropOffIds);
            //AD Future Booking
            $messageCount = Booking::find()->where(['and',['status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['!=','city_id',$dubaiCity],['>=','booking_date',date("Y-m-d")],['not in','boat_id',$spBoatIdz]])->count('id');
            //AD This month Booking
            list($by,$bm,$bd)=explode("-",$this->booking_date);
            $messageCountMonth = Booking::find()->where(['and',['MONTH(booking_date)'=>date("m",mktime(0,0,0,$bm,$bd,$by)),'YEAR(booking_date)'=>date("Y",mktime(0,0,0,$bm,$bd,$by)),'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['!=','city_id',$dubaiCity],['not in','boat_id',$spBoatIdz]])->count('id');

            if($thisUser->getPrivilege('1adallweek')==true){
              if($messageCount>0){
                $this->addError('','Error! You have an existing booking in the other city');
              }else{

                if($messageCountMonth>0){
                  $this->addError('','You already booked this month in selected city, you can not book more this month, try booking next month!');
                }
              }
            }
            elseif($thisUser->getPrivilege('1adweekdays')==true){
              $whichWeekendDay=Yii::$app->helperFunctions->WeekendDay($this->booking_date);
              if($whichWeekendDay==5 || $whichWeekendDay==6){
                $this->addError('','You can not book on weekends!');
              }else{
                if($messageCount>0){
                  $this->addError('','Error! You have an existing booking in the other city');
                }else{
                  if($messageCountMonth>0){
                    $this->addError('','You already booked this month in selected city, you can not book more this month, try booking next month!');
                  }
                }
              }
            }
            elseif($thisUser->getPrivilege('unlimitedocity')==true){
              $whichWeekendDay=Yii::$app->helperFunctions->WeekendDay($this->booking_date);
              if(($thisUser->getPrivilege('2weekendocity')==true) && $whichWeekendDay==5 || $whichWeekendDay==6){
                list($by,$bm,$bd)=explode("-",$this->booking_date);
                $thisMonthMessageCount = Booking::find()->where(['and',['or',['DAYOFWEEK(booking_date)'=>6,'DAYOFWEEK(booking_date)'=>7]],['user_id'=>$packageUsers,'MONTH(booking_date)'=>date("m",mktime(0,0,0,$bm,$bd,$by)),'YEAR(booking_date)'=>date("Y",mktime(0,0,0,$bm,$bd,$by)),'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['!=','city_id',$dubaiCity]])->count("id");
                if($thisMonthMessageCount>1){
                  $this->addError('','You already have 2 weekend bookings this month.');
                }elseif($thisMonthMessageCount>0 && $timeslot!=Yii::$app->params['nightDriveTimeSlot']){
                  $this->addError('','You already have a weekend booking in this month, and second booking is strictly night drive.');
                }
              }elseif(($thisUser->getPrivilege('2weekendocity')!=true) && $whichWeekendDay==5 || $whichWeekendDay==6){
                $this->addError('','You can not book on weekends in other city');
              }
            }
            else{
              $this->addError('','You can not book in other city');
            }
          }
        }
      }
      return true;
    }
  }

  /**
  * Validates AD to DXB all days.
  */
  public function checkADToDXB($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $timeslot=$this->booking_time_slot;
      $thisUser=Yii::$app->user->identity;
      if($this->booking_source=='member' && $thisUser->user_type==0){
        $packageUsers=$thisUser->memberIdz;

        //Only Abu Dhabi Allowed
        if($thisUser->getPrivilege('onlyabudhabi')==true){
          $abuDhabiCity=Yii::$app->appHelperFunctions->cityIdz['onlyabudhabi'];
          if($this->city_id!=$abuDhabiCity){
            $spBoatIdz=array_merge(Yii::$app->appHelperFunctions->marinaStayInIds,Yii::$app->appHelperFunctions->dropOffIds);
            //DXB Future Booking
            $messageCount = Booking::find()->where(['and',['status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['!=','city_id',$abuDhabiCity],['>=','booking_date',date("Y-m-d")],['not in','boat_id',$spBoatIdz]])->count('id');
            //DXB This month Booking
            list($by,$bm,$bd)=explode("-",$this->booking_date);
            $messageCountMonth = Booking::find()->where(['and',['MONTH(booking_date)'=>date("m",mktime(0,0,0,$bm,$bd,$by)),'YEAR(booking_date)'=>date("Y",mktime(0,0,0,$bm,$bd,$by)),'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['!=','city_id',$abuDhabiCity],['not in','boat_id',$spBoatIdz]])->count('id');

            if($thisUser->getPrivilege('1adallweek')==true){
              if($messageCount>0){
                $this->addError('','Error! You have an existing booking in the other city');
              }else{

                if($messageCountMonth>0){
                  $this->addError('','You already booked this month in selected city, you can not book more this month, try booking next month!');
                }
              }
            }
            elseif($thisUser->getPrivilege('1adweekdays')==true){
              $whichWeekendDay=Yii::$app->helperFunctions->WeekendDay($this->booking_date);
              if($whichWeekendDay==5 || $whichWeekendDay==6){
                $this->addError('','You can not book on weekends!');
              }else{
                if($messageCount>0){
                  $this->addError('','Error! You have an existing booking in the other city');
                }else{
                  if($messageCountMonth>0){
                    $this->addError('','You already booked this month in selected city, you can not book more this month, try booking next month!');
                  }
                }
              }
            }
            elseif($thisUser->getPrivilege('unlimitedocity')==true){
              $whichWeekendDay=Yii::$app->helperFunctions->WeekendDay($this->booking_date);
              if(($thisUser->getPrivilege('2weekendocity')==true) && $whichWeekendDay==5 || $whichWeekendDay==6){
                list($by,$bm,$bd)=explode("-",$this->booking_date);
                $thisMonthMessageCount = Booking::find()->where(['and',['or',['DAYOFWEEK(booking_date)'=>6,'DAYOFWEEK(booking_date)'=>7]],['user_id'=>$packageUsers,'MONTH(booking_date)'=>date("m",mktime(0,0,0,$bm,$bd,$by)),'YEAR(booking_date)'=>date("Y",mktime(0,0,0,$bm,$bd,$by)),'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['!=','city_id',$abuDhabiCity]])->count("id");
                if($thisMonthMessageCount>1){
                  $this->addError('','You already have 2 weekend bookings this month.');
                }elseif($thisMonthMessageCount>0 && $timeslot!=Yii::$app->params['nightDriveTimeSlot']){
                  $this->addError('','You already have a weekend booking in this month, and second booking is strictly night drive.');
                }
              }elseif(($thisUser->getPrivilege('2weekendocity')!=true) && $whichWeekendDay==5 || $whichWeekendDay==6){
                $this->addError('','You can not book on weekends in other city');
              }
            }
            else{
              $this->addError('','You can not book in other city');
            }
          }
        }
      }
      return true;
    }
  }

  public function checkBronzePackage()
  {
    if (!$this->hasErrors()) {
      $timeslot=$this->booking_time_slot;
      $thisUser=Yii::$app->user->identity;
      if($this->booking_source=='member' && $thisUser->user_type==0){
        $packageUsers=$thisUser->memberIdz;
        $whichWeekendDay=Yii::$app->helperFunctions->WeekendDay($this->booking_date);
        $cityId=$this->city_id;
        if($thisUser->getPrivilege('onlydubai')==true)$cityId=Yii::$app->appHelperFunctions->cityIdz['onlydubai'];
        if($thisUser->getPrivilege('onlyabudhabi')==true)$cityId=Yii::$app->appHelperFunctions->cityIdz['onlyabudhabi'];

        //Same City
        //if($this->city_id==Yii::$app->params['cityIdz']['onlyabudhabi']){
        list($by,$bm,$bd)=explode("-",$this->booking_date);
        //Max 3 Weekends am/pm in 60 days

        if($thisUser->getPrivilege('3weekendampm60days')==true && in_array($timeslot,Yii::$app->helperFunctions->ampmTimeSlots) && ($whichWeekendDay==5 || $whichWeekendDay==6)){
          $allowed=true;
          $daysLimit=60;
          $daysDif=0;
          $type='';
          //Get three past
          $threeAMPMWeekendBookings = Booking::find()->where(['and',['city_id'=>$cityId,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['in','booking_time_slot',Yii::$app->helperFunctions->ampmTimeSlots],['<=','booking_date',$this->booking_date],['or',['DAYOFWEEK(booking_date)'=>6],['DAYOFWEEK(booking_date)'=>7]]])->orderBy(['booking_date'=>SORT_DESC])->limit(3)->all();
          if($threeAMPMWeekendBookings!=null){
            $startFromDate='';
            $n=0;
            foreach($threeAMPMWeekendBookings as $threeAMPMWeekendBooking){
              $startFromDate=$threeAMPMWeekendBooking->booking_date;
              $n++;
            }
            if($startFromDate!='' && $n==3){
              $daysDif=Yii::$app->helperFunctions->daysDifference($startFromDate,$this->booking_date);
              if($daysDif<=$daysLimit){
                $allowed=false;
                $type='Old';
              }
            }
          }

          //Get three future
          $threeAMPMWeekendBookings = Booking::find()->where(['and',['city_id'=>$cityId,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['in','booking_time_slot',Yii::$app->helperFunctions->ampmTimeSlots],['>=','booking_date',$this->booking_date],['or',['DAYOFWEEK(booking_date)'=>6],['DAYOFWEEK(booking_date)'=>7]]])->orderBy(['booking_date'=>SORT_ASC])->limit(3)->all();
          if($threeAMPMWeekendBookings!=null){
            $EndToDate='';
            $n=0;
            foreach($threeAMPMWeekendBookings as $threeAMPMWeekendBooking){
              $EndToDate=$threeAMPMWeekendBooking->booking_date;
              $n++;
            }
            if($EndToDate!='' && $n==3){
              $daysDif=Yii::$app->helperFunctions->daysDifference($this->booking_date,$EndToDate);
              if($daysDif<=$daysLimit){
                $allowed=false;
                $type='future';
              }
            }
          }

          //Get past 1 and future 2
          //Get past 1
          $OldAMPMWeekendBooking = Booking::find()->where(['and',['city_id'=>$cityId,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['in','booking_time_slot',Yii::$app->helperFunctions->ampmTimeSlots],['<=','booking_date',$this->booking_date],['or',['DAYOFWEEK(booking_date)'=>6],['DAYOFWEEK(booking_date)'=>7]]])->orderBy(['booking_date'=>SORT_DESC])->one();
          if($OldAMPMWeekendBooking!=null){
            $startFromDate=$threeAMPMWeekendBooking->booking_date;
            //Get future 2
            $futureAMPMWeekendBookings = Booking::find()->where(['and',['city_id'=>$cityId,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['in','booking_time_slot',Yii::$app->helperFunctions->ampmTimeSlots],['>=','booking_date',$this->booking_date],['or',['DAYOFWEEK(booking_date)'=>6],['DAYOFWEEK(booking_date)'=>7]]])->orderBy(['booking_date'=>SORT_ASC])->limit(2)->all();
            if($futureAMPMWeekendBookings!=null){
              $EndToDate='';
              $n=0;
              foreach($threeAMPMWeekendBookings as $threeAMPMWeekendBooking){
                $EndToDate=$threeAMPMWeekendBooking->booking_date;
                $n++;
              }
              if($EndToDate!='' && $n==2){
                $daysDif=Yii::$app->helperFunctions->daysDifference($startFromDate,$EndToDate);
                if($daysDif<=$daysLimit){
                  $allowed=false;
                  $type='past1future2';
                }
              }
            }
          }

          //Get past 2 and future 1
          //Get past 2
          $OldAMPMWeekendBookings = Booking::find()->where(['and',['city_id'=>$cityId,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['in','booking_time_slot',Yii::$app->helperFunctions->ampmTimeSlots],['<=','booking_date',$this->booking_date],['or',['DAYOFWEEK(booking_date)'=>6],['DAYOFWEEK(booking_date)'=>7]]])->orderBy(['booking_date'=>SORT_DESC])->limit(2)->all();
          if($OldAMPMWeekendBookings!=null){
            $startFromDate='';
            $n=0;
            foreach($OldAMPMWeekendBookings as $OldAMPMWeekendBooking){
              $startFromDate=$OldAMPMWeekendBooking->booking_date;
              $n++;
            }

            //Get future 1
            $futureAMPMWeekendBooking = Booking::find()->where(['and',['city_id'=>$cityId,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0],['in','user_id',$packageUsers],['in','booking_time_slot',Yii::$app->helperFunctions->ampmTimeSlots],['>=','booking_date',$this->booking_date],['or',['DAYOFWEEK(booking_date)'=>6],['DAYOFWEEK(booking_date)'=>7]]])->orderBy(['booking_date'=>SORT_ASC])->one();
            if($futureAMPMWeekendBooking!=null){
              $EndToDate=$futureAMPMWeekendBooking->booking_date;
              if($startFromDate!='' && $n==2){
                $daysDif=Yii::$app->helperFunctions->daysDifference($startFromDate,$EndToDate);
                if($daysDif<=$daysLimit){
                  $allowed=false;
                  $type='past2future1';
                }
              }
            }
          }


          //If not allowed to fulfill 60 days
          if($allowed==false){
            $this->addError('','Maximum 3 Am/Pm weekends allowed in 60 days.');//.$daysDif.' - '.$type
          }

          //Max 2 Weekends am/pm in same month
          if($thisUser->getPrivilege('2weekendampmsamemonth')==true && ($whichWeekendDay==5 || $whichWeekendDay==6)){
            $thisMonthMessageCount = (new yii\db\Query())->from('booking')->where("user_id in (".implode(",",$packageUsers).") and city_id='".$cityId."' and (DAYOFWEEK(booking_date) = 6 || DAYOFWEEK(booking_date) = 7) and trashed=0 and MONTH(booking_date)='".date("m",mktime(0,0,0,$bm,$bd,$by))."' and YEAR(booking_date)='".date("Y",mktime(0,0,0,$bm,$bd,$by))."' and booking_time_slot in (".implode(",",Yii::$app->helperFunctions->ampmTimeSlots).")")->count();
            if($thisMonthMessageCount>1){
              $this->addError('','Maximum 2 Am/Pm weekends allowed per month.');
            }
          }
        }
        //}
      }
      return true;
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
      'city_id' => Yii::t('app', 'City'),
      'port_id' => Yii::t('app', 'Marina'),
      'boat_id' => Yii::t('app', 'Boat'),
      'booking_date' => Yii::t('app', 'Date'),
      'booking_time_slot' => Yii::t('app', 'Time Slot'),
      'booking_comments' => Yii::t('app', 'Remarks'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'updated_at' => Yii::t('app', 'Updated'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
      'LoggableBehavior' => [
        'class' => ActivityLogBehaviorUser::className(),
      ],
    ];
  }

  /**
  * return Boat Name
  */
  public function getBoatName()
  {
    $name='';
    $boatInfo=$this->boat;
    if($boatInfo!=null){
      $name=$boatInfo->name;
      if($boatInfo->special_boat==1 && $boatInfo->boat_selection==1){
        $SelectedBoatQuery=BookingSpecialBoatSelection::find()->select(['boat_id'])->where(['booking_id'=>$this->id]);
        $selBoat=Boat::find()->where(['id'=>$SelectedBoatQuery])->asArray()->one();
        if($selBoat!=null){
          $name.=' <span class="badge grid-badge badge-success">'.$selBoat['name'].'</span>';
        }
      }
    }else{
      $name='Not set';
    }
    return $name;
  }

  /**
  * return Boat Name
  */
  public function getEmailBoatName()
  {
    $name='';
    $boatInfo=$this->boat;
    if($boatInfo!=null){
      $name=$boatInfo->name;
      if($boatInfo->special_boat==1 && $boatInfo->boat_selection==1){
        $SelectedBoatQuery=BookingSpecialBoatSelection::find()->select(['boat_id'])->where(['booking_id'=>$this->id]);
        $selBoat=Boat::find()->where(['id'=>$SelectedBoatQuery])->asArray()->one();
        if($selBoat!=null){
          $name.=' ('.$selBoat['name'].')';
        }
      }
    }else{
      $name='Not set';
    }
    return $name;
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCreatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'created_by']);
  }

  /**
  * @return string, name of user
  */
  public function getCreatedBy()
  {
    if($this->createdByUser!=null){
      return $this->createdByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getUpdatedByUser()
  {
    return $this->hasOne(User::className(), ['id' => 'updated_by']);
  }

  /**
  * @return string, name of user
  */
  public function getUpdatedBy()
  {
    if($this->updatedByUser!=null){
      return $this->updatedByUser->fullname;
    }
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getSwapInfo()
  {
    return BoatSwap::find()->where(['booking_id'=>$this->id])->one();
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBulkBookingSwapInfo()
  {
    return BulkBookingReplacement::find()->where(['user_booking_id'=>$this->id])->one();
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCity()
  {
    return $this->hasOne(City::className(), ['id' => 'city_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMarina()
  {
    return $this->hasOne(Marina::className(), ['id' => 'port_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBoat()
  {
    return $this->hasOne(Boat::className(), ['id' => 'boat_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTimeSlot()
  {
    return $this->hasOne(TimeSlot::className(), ['id' => 'booking_time_slot']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getTimeZone()
  {
    return $this->hasOne(TimeSlot::className(), ['id' => 'booking_time_slot']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getMember()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBookingActivity()
  {
    return $this->hasOne(BookingActivity::className(), ['booking_id' => 'id']);
  }

  /************************************Requested Items************************************/
  /**
  * Check if Captain Addon is requested
  * @return String
  */
  public function getIsCaptainRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->captain==1){
      $statusStr='Yes';
      $statusArr=$this->captainStatusArray;
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.='<a href="javascript:;" class="delete-red-btn cancel-captain" data-toggle="tooltip" title="'.Yii::t('app','Cancel Captain').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      if(in_array($this->boat_id,Yii::$app->appHelperFunctions->nightCruiseIds)){
        $priceStr='';
        $statusStr='<span class="badge badge-success">'.Yii::t('app','Complimentary').'</span>';
      }else{
        $waitingList=WaitingListCaptain::find()->where(['booking_id'=>$this->id,'status'=>0]);
        if($waitingList->exists()){
          $statusStr='<span class="badge badge-info">'.Yii::t('app','Under Process').'</span>';
          $statusArr=['status'=>$statusStr,'price'=>''];
          if($this->booking_date>date("Y-m-d")){
            $deleteStr.='<a href="javascript:;" class="delete-red-btn cancel-captain-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel Captain Request').'"><i class="fa fa-trash"></i></a>';
          }
        }
      }
    }
    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Inhouse Captain'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if Equipment is requested
  * @return String
  */
  public function getIsEquipmentRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->sport_eqp_id>0){
      $statusStr='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      $statusArr=['status'=>$statusStr,'price'=>Yii::t('app','Free')];
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-equipment" data-toggle="tooltip" title="'.Yii::t('app','Cancel Donuts').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      $waitingList=WaitingListSportsEquipment::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $statusStr='<span class="badge badge-info">'.Yii::t('app','Under Process').'</span>';
        $statusArr=['status'=>$statusStr,'price'=>''];
        if($this->booking_date>date("Y-m-d")){
          $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-equipment-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel Donuts').'"><i class="fa fa-trash"></i></a>';
        }
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Donuts'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if BBQ Set is requested
  * @return String
  */
  public function getIsBBQSetRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->bbq==1){
      $statusStr='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      $statusArr=['status'=>$statusStr,'price'=>Yii::$app->appHelperFunctions->bbqPrice.' AED'];
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-bbq" data-toggle="tooltip" title="'.Yii::t('app','Cancel BBQ Set').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      $waitingList=WaitingListBbq::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $statusStr='<span class="badge badge-info">'.Yii::t('app','Under Process').'</span>';
        $statusArr=['status'=>$statusStr,'price'=>''];
        if($this->booking_date>date("Y-m-d")){
          $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-bbq-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel BBQ Set Request').'"><i class="fa fa-trash"></i></a>';
        }
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','BBQ Set'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if Ice is requested
  * @return String
  */
  public function getIsIceRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->ice==1){
      $statusStr='<span class="badge badge-info">'.Yii::t('app','Noted').'</span>';
      $statusArr=['status'=>$statusStr,'price'=>''];
      if($this->booking_date>date("Y-m-d")){
        //$deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-ice" data-toggle="tooltip" title="'.Yii::t('app','Cancel Ice Request').'"><i class="fa fa-trash"></i></a>';
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Ice'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if Kids Life Jacket is requested
  * @return String
  */
  public function getIsKidsLifeJacketRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->kids_life_jacket==1){
      $statusStr='<span class="badge badge-info">Noted</span>';
      $statusArr=['status'=>$statusStr,'price'=>''];
      if($this->booking_date>date("Y-m-d")){
        //$deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-kids-life-jacket" data-toggle="tooltip" title="'.Yii::t('app','Cancel Kids Life Jackets Request').'"><i class="fa fa-trash"></i></a>';
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Kids Life Jacket'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if Early Departure is requested
  * @return String
  */
  public function getIsEarlyDepartureRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->early_departure==1){
      $statusStr='<span class="badge badge-success">Confirmed</span>';
      $statusArr=['status'=>$statusStr,'price'=>Yii::t('app','Free')];
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-early-departure" data-toggle="tooltip" title="'.Yii::t('app','Cancel Early Departure').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      $waitingList=WaitingListEarlyDeparture::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $statusStr='<span class="badge badge-info">Under Process</span>';
        $statusArr=['status'=>$statusStr,'price'=>''];
        if($this->booking_date>date("Y-m-d")){
          $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-early-departure-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel Early Departure Request').'"><i class="fa fa-trash"></i></a>';
        }
      }
    }
    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Early Departure'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if Over Night Camping is requested
  * @return String
  */
  public function getIsLateArrivalRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->late_arrival==1){
      $statusStr='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      $statusArr=['status'=>$statusStr,'price'=>Yii::t('app','Free')];
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-late-arrival" data-toggle="tooltip" title="'.Yii::t('app','Cancel Late Arrival').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      $waitingList=WaitingListLateArrival::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $statusStr='<span class="badge badge-info">'.Yii::t('app','Under Process').'</span>';
        $statusArr=['status'=>$statusStr,'price'=>''];
        if($this->booking_date>date("Y-m-d")){
          $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-late-arrival-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel Late Arrival Request').'"><i class="fa fa-trash"></i></a>';
        }
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Late Arrival'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;

    $str ='';
    $status='';


    if($status!=''){
      $str.='<tr>';
      $str.=' <td></td>';
      $str.=' <td>'.$status.'</td>';
      $str.='</tr>';
    }
    return $str;
  }

  /**
  * Check if Over Night Camping is requested
  * @return String
  */
  public function getIsOvernightCampingRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->overnight_camping==1){
      $statusStr='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      $statusArr=['status'=>$statusStr,'price'=>Yii::t('app','Free')];
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-overnight-camping" data-toggle="tooltip" title="'.Yii::t('app','Cancel Oernight Camping').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      $waitingList=WaitingListOvernightCamping::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $statusStr='<span class="badge badge-info">'.Yii::t('app','Under Process').'</span>';
        $statusArr=['status'=>$statusStr,'price'=>''];
        if($this->booking_date>date("Y-m-d")){
          $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-overnight-camping-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel Overnight Camping Request').'"><i class="fa fa-trash"></i></a>';
        }
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Overnight Camping'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if Wake Boarding is requested
  * @return String
  */
  public function getIsWakeBoardingRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->wake_boarding==1){
      $statusStr='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      $statusArr=['status'=>$statusStr,'price'=>Yii::t('app','Free')];
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-wake-boarding" data-toggle="tooltip" title="'.Yii::t('app','Cancel Wake Boarding').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      $waitingList=WaitingListWakeBoarding::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $statusStr='<span class="badge badge-info">'.Yii::t('app','Under Process').'</span>';
        $statusArr=['status'=>$statusStr,'price'=>''];
        if($this->booking_date>date("Y-m-d")){
          $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-wake-boarding-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel Wake Boarding Request').'"><i class="fa fa-trash"></i></a>';
        }
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Wake Boarding'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  /**
  * Check if Wake Boarding is requested
  * @return String
  */
  public function getIsWakeSurfingRequested()
  {
    $reqArr=[];
    $deleteStr='';
    $statusStr='';
    $statusArr=[];
    if($this->wake_surfing==1){
      $statusStr='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      $statusArr=['status'=>$statusStr,'price'=>Yii::t('app','Free')];
      if($this->booking_date>date("Y-m-d")){
        $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-wake-surfing" data-toggle="tooltip" title="'.Yii::t('app','Cancel Wake Surfing').'"><i class="fa fa-trash"></i></a>';
      }
    }else{
      $waitingList=WaitingListWakeSurfing::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $statusStr='<span class="badge badge-info">'.Yii::t('app','Under Process').'</span>';
        $statusArr=['status'=>$statusStr,'price'=>''];
        if($this->booking_date>date("Y-m-d")){
          $deleteStr.=' <a href="javascript:;" class="delete-red-btn cancel-wake-surfing-waiting" data-toggle="tooltip" title="'.Yii::t('app','Cancel Wake Surfing Request').'"><i class="fa fa-trash"></i></a>';
        }
      }
    }

    if($statusStr!=null){
      $reqArr=[
        'title'=>Yii::t('app','Wake Surfing'),
        'status'=>$statusArr['status'],
        'price'=>$statusArr['price'],
        'deleteBtn'=>$deleteStr,
      ];
    }
    return $reqArr;
  }

  public function getCardRequestedItems()
  {
    $itemsArray = [];
    $isCaptainRequested=$this->isCaptainRequested;
    if($isCaptainRequested!=null){
      $itemsArray[]=$isCaptainRequested;
    }
    $isEquipmentRequested=$this->isEquipmentRequested;
    if($isEquipmentRequested!=null){
      $itemsArray[]=$isEquipmentRequested;
    }
    $isBBQSetRequested=$this->isBBQSetRequested;
    if($isBBQSetRequested!=null){
      $itemsArray[]=$isBBQSetRequested;
    }
    $isIceRequested=$this->isIceRequested;
    if($isIceRequested!=null){
      $itemsArray[]=$isIceRequested;
    }
    $isKidsLifeJacketRequested=$this->isKidsLifeJacketRequested;
    if($isKidsLifeJacketRequested!=null){
      $itemsArray[]=$isKidsLifeJacketRequested;
    }
    $isEarlyDepartureRequested=$this->isEarlyDepartureRequested;
    if($isEarlyDepartureRequested!=null){
      $itemsArray[]=$isEarlyDepartureRequested;
    }
    $isLateArrivalRequested=$this->isLateArrivalRequested;
    if($isLateArrivalRequested!=null){
      $itemsArray[]=$isLateArrivalRequested;
    }
    $isOvernightCampingRequested=$this->isOvernightCampingRequested;
    if($isOvernightCampingRequested!=null){
      $itemsArray[]=$isOvernightCampingRequested;
    }
    $isWakeBoardingRequested=$this->isWakeBoardingRequested;
    if($isWakeBoardingRequested!=null){
      $itemsArray[]=$isWakeBoardingRequested;
    }
    $isWakeSurfingRequested=$this->isWakeSurfingRequested;
    if($isWakeSurfingRequested!=null){
      $itemsArray[]=$isWakeSurfingRequested;
    }
    return $itemsArray;
  }

  public function getAddonsPopupButton()
  {
    $html='
    <div class="col-sm-12 p0 pb5">
      <div class="btn-containers">
        <button class="p-1 bg-yellow-600 text-white rounded btn-containers shadow-lg hover:shadow-none hover:bg-yellow-400 text-xs" data-toggle="modal" data-target="#addonsModal'.$this->id.'">
          <i class="fas fa-plus-circle text-white"></i> '.Yii::t('app','Addons').'
        </button>
      </div>
    </div>
    <div class="modal fade bca-modal" id="addonsModal'.$this->id.'" role="dialog">
      <div class="modal-dialog modal-md" style="top: 25%; padding: 20px;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">'.$this->boatName.' on '.Yii::$app->formatter->asDate($this->booking_date).' - '.$this->timeZone->dashboard_name.'</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="table-responsive">
              '.$this->addonsRequestTable.'
            </div>
          </div>
        </div>
      </div>
    </div>';
    return $html;
  }

  public function getAddonsRequestedHtml()
  {
    $html='';
    $cardRequestedItems=$this->cardRequestedItems;
    if($cardRequestedItems!=null){
    $html.='<div class="row">
      <table class="table table-booking-custom">
        <thead>
          <tr>
            <th scope="col">Type</th>
            <th scope="col">Status</th>
            <th scope="col">Price</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>';
          foreach($cardRequestedItems as $cardRequestedItem){
          $html.='<tr>
            <td>'.$cardRequestedItem['title'].'</td>
            <td>'.$cardRequestedItem['status'].'</td>
            <td>'.$cardRequestedItem['price'].'</td>
            <td>'.$cardRequestedItem['deleteBtn'].'</td>
          </tr>';
          }
        $html.='</tbody>
      </table>
    </div>';
    }
    return $html;
  }

  public function getAddonsRequestTable()
  {
    if($this->status!=1){
      return '';
    }
    if($this->trashed==1){
      return '';
    }
    $table='';
    //Show Menu Only if its future
    if($this->booking_date>=date("Y-m-d")){
      $table.='   <table class="table table-sm table-striped table-bordered table-hover table_addons">';
      $table.='     <tr>';
      $table.='       <th class="addons-col-1">Type</th>';
      $table.='       <th class="addons-col-2">Availability</th>';
      $table.='       <th class="addons-col-3">Charge</th>';
      $table.='     </tr>';

      //Captain
      $table.=$this->requestCaptainAddon;

      //Equipment
      $table.=$this->requestEquipment;

      //BBQ Set
      $table.=$this->requestBBQSet;

      //Wake Boarding
      $table.=$this->requestWakeBoarding;

      //Wake Surfing
      $table.=$this->requestWakeSurfing;

      //Ice
      $table.=$this->requestIce;

      //Kids Life Jacket
      $table.=$this->requestKidsLifeJacket;

      //Early Departure
      $table.=$this->requestEarlyDeparture;

      //Late Arrival
      $table.=$this->requestLateArrival;

      //Overnight Camping
      $table.=$this->requestOvernightCamping;

      //Other
      if($this->showRequestOther==true){
        $table.='     <tr>';
        $table.='       <th>Other</th>';
        $table.='       <td>'.$this->hasOther.'</td>';
        $table.='       <td></td>';
        $table.='     </tr>';
      }
      $table.='   </table>';
    }
    return $table;
  }

  /**
  * Check if Captain Addon is available
  * @return String
  */
  public function getRequestCaptainAddon()
  {
    if($this->marina->captains==0){
      return '';
    }
    if($this->captain==1){
      return '';
    }
    if($this->boat->special_boat==1){
      return '';
    }
    $waitingList=WaitingListCaptain::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }
    $showPrice=true;
    $availbility='';

    if($this->captain_type!=null){
      $captain_type=$this->captain_type;
    }else{
      $captain_type=Yii::$app->bookingHelperFunctions->getCaptainStatus($this->member,$this);
    }

    if($this->booking_time_slot==Yii::$app->params['nightDriveTimeSlot']){
      $availbility='<a href="javascript:;" class="nd-captain">'.Yii::t('app','Book').'</a>';
    }else{
      $availbility='<a href="javascript:;" class="'.($captain_type=='free' ? 'request-captain' : 'request-buy-captain').'">'.Yii::t('app','Book').'</a>';
    }

    //If its today or day before and before 6pm
    if($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_captain'))){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
    }
    //If early departure is requested
    elseif($this->earlyDepartureBookedOrRequested==true){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','N/A Early Departure Confirmed or Requested').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::t('app','Early Departure is either confirmed or requested for this booking. Therefore an in house captain cant be provided.').'" style="color:#fff;">(i)</a>';
    }

    //If overnight camping is requested
    elseif($this->overnightCampingBookedOrRequested==true){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','N/A Over Night Camping Confirmed or Requested').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::t('app','Over Night Camping is either confirmed or requested for this booking. Therefore an in house captain cant be provided.').'" style="color:#fff;">(i)</a>';
    }

    //If overnight camping is requested
    elseif($this->lateArrivalBookedOrRequested==true){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','N/A Late Arrival Confrimed or Requested').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::t('app','Late Arrival is either confirmed or requested for this booking. Therefore an in house captain cant be provided.').'" style="color:#fff;">(i)</a>';
    }else{
      $totalCaptainsInMarina=$this->marina->captains;
      $usedOnDate=self::find()->where(['booking_date'=>$this->booking_date,'port_id'=>$this->port_id,'booking_time_slot'=>$this->booking_time_slot,'captain'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
      if($usedOnDate>=$totalCaptainsInMarina){
        $showPrice=false;
        $availbility='N/A. <a href="javascript:;" class="waitlist-captain" data-reason="'.Yii::$app->bookingHelperFunctions->captainLimitReachedReason.'">'.Yii::t('app','Request Wait List').'</a> <a href="javascript:;"data-toggle="tooltip" data-title="'.Yii::t('app','At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.').'">(i)</a>';
      }
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Captain').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? ($captain_type=='free' ? Yii::t('app','Free') : ''.Yii::$app->appHelperFunctions->captainPrice.' AED') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if Sports Equipment is available
  * @return String
  */
  public function getRequestEquipment()
  {
    if($this->sport_eqp_id>0){
      return '';
    }
    $waitingList=WaitingListSportsEquipment::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-equip">'.Yii::t('app','Book').'</a>';

    //If Boat is not suitable
    if($this->boat->watersports==0){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Boat not Suitables').'</span>';
    }
    //If its today or day before and before 6pm
    elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_donut'))){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff">(i)</a>';
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Equipment').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::t('app','Free') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if BBQ Set is available
  * @return String
  */
  public function getRequestBBQSet()
  {
    if($this->bbq==1){
      return '';
    }
    if($this->marina->bbq==0){
      return '';
    }
    if($this->boat->bbq==0){
      return '';
    }
    $waitingList=WaitingListBbq::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-bbq">'.Yii::t('app','Book').'</a>';

    //If its today or day before and before 6pm
    if($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_bbq'))){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff">(i)</a>';
    }else{
      $totalEquipmentsInMarina=$this->marina->bbq;
      $usedOnDate=self::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'bbq'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
      if($usedOnDate>=$totalEquipmentsInMarina){
        $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-bbq" data-reason="'.Yii::$app->bookingHelperFunctions->bbqSetLimitReachedReason.'">'.Yii::t('app','Request Wait List').'</a>';
      }
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','BBQ Set').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::$app->appHelperFunctions->bbqPrice.' AED' : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if Ice is available
  * @return String
  */
  public function getRequestIce()
  {
    if($this->city_id==Yii::$app->appHelperFunctions->cityIdz['onlydubai']){
      return '';
    }
    if($this->ice==1){
      return '';
    }
    if($this->boat->ice==0){
      return '';
    }

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-ice" data-booking_date="'.Yii::$app->formatter->asDate($this->booking_date).'">'.Yii::t('app','Request').'</a>';

    //If its today or day before and before 6pm
    if($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_ice'))){
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff;">(i)</a>';
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Ice').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::t('app','Free') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if Kids Life Jacket is available
  * @return String
  */
  public function getRequestKidsLifeJacket()
  {
    if($this->kids_life_jacket==1){
      return '';
    }
    if($this->boat->kids_life_jacket==0){
      return '';
    }

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="load-modal" data-url="'.Url::to(['request/book-kids-life-jacket','id'=>$this->id]).'" data-heading="'.Yii::t('app','Request Kids Life Jackets').'">'.Yii::t('app','Request').'</a>';

    //If its today or day before and before 6pm
    if($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_kids_life_jacket'))){
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff;">(i)</a>';
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Kids Life Jackets').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::t('app','Free') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if Early Departure is available
  * @return String
  */
  public function getRequestEarlyDeparture()
  {
    if($this->early_departure==1){
      return '';
    }
    if($this->booking_time_slot!=1){
      return '';
    }
    if($this->marina->no_of_early_departures==0){
      return '';
    }
    if($this->boat->early_departure==0){
      return '';
    }
    $waitingList=WaitingListEarlyDeparture::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-early-departure">'.Yii::t('app','Request').'</a>';

    if($this->inHouseCaptainBookedOrRequested==true){
      //it has night drive session, so Not Available
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','N/A in house captain confirmed or Requested').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::t('app','In house Captain is either confirmed or requested for this booking. Therefore an in house captain cant be provided.').'" style="color:#fff;">(i)</a>';
    }
    //If its today or day before and before 6pm
    elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_early_departure'))){
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
    }else{
      $totalEarlyDeparturesAllowedInMarina=$this->marina->no_of_early_departures;
      $usedOnDate=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
      if($usedOnDate>=$totalEarlyDeparturesAllowedInMarina){
        $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-early-departure" data-reason="'.Yii::$app->bookingHelperFunctions->earlyDepartureLimitReachedReason.'">'.Yii::t('app','Request Wait List').'</a> <a href="javascript:;"data-toggle="tooltip" data-title="'.Yii::t('app','At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.').'">(i)</a>';
      }else{
        $previousDate=Yii::$app->helperFunctions->getDayBefore($this->booking_date);
        $previousDayOvernightCampingBookingForThisBoat=self::find()->where(['city_id'=>$this->city_id,'boat_id'=>$this->boat_id,'port_id'=>$this->port_id,'booking_date'=>$previousDate,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
        if($previousDayOvernightCampingBookingForThisBoat->exists()){
          $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-early-departure" data-reason="'.Yii::$app->bookingHelperFunctions->earlyDeparturePreviousDayOvernightCampReason.'">'.Yii::t('app','Request Wait List').'</a> <a href="javascript:;"data-toggle="tooltip" data-title="'.Yii::t('app','At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.').'">(i)</a>';
        }
      }
    }


    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Early Departure').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? 'Free' : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if Late Arrival is available
  * @return String
  */
  public function getRequestLateArrival()
  {
    if($this->booking_time_slot!=2){
      //Check its time is PM only
      return '';
    }
    if($this->marina->no_of_late_arrivals==0){
      //Marina doesnt have it
      return '';
    }
    if($this->boat->late_arrival==0){
      //Boat is not suitable
      return '';
    }
    if($this->late_arrival==1){
      //Boat is not suitable
      return '';
    }
    $boatTiming=BoatToTimeSlot::find()->where(['boat_id'=>$this->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
    if($boatTiming->exists()){
      //Check if its one of night drives not available
      //return '';
    }

    $waitingList=WaitingListLateArrival::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }

    list($startHr,$startMin,$startSec)=explode(":",$this->timeZone->start_time);

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-late-arrival">'.Yii::t('app','Request').'</a>';
    $nightDriveBooking=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>Yii::$app->params['nightDriveTimeSlot'],'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);

    if($this->inHouseCaptainBookedOrRequested==true){
      //it has night drive session, so Not Available
      $availbility='<span class="badge badge-warning">'.Yii::t('app','N/A in house captain confirmed or Requested').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::t('app','In house Captain is either confirmed or requested for this booking. Therefore an in house captain cant be provided.').'" style="color:#fff;">(i)</a>';
    }
    elseif(!$nightDriveBooking->exists() && date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_late_arrival_wo_nd')){
      //No night drive for 1 day before & its already passed the time
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
    }
    //If its today
    elseif($nightDriveBooking->exists() && $this->booking_date==date("Y-m-d") && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_late_arrival_w_nd')){// && !$nightDriveBooking->exists()
      //Time is already passed
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
    }

    $totalLateArrivalsAllowedInMarina=$this->marina->no_of_late_arrivals;
    $usedOnDate=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'late_arrival'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
    if($usedOnDate>=$totalLateArrivalsAllowedInMarina){
      $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-late-arrival" data-reason="'.Yii::$app->bookingHelperFunctions->lateArrivalLimitReachedReason.'">'.Yii::t('app','Request Wait List').'</a> <a href="javascript:;" data-toggle="tooltip" data-title="'.Yii::t('app','At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.').'">(i)</a>';
    }

    $permit=UserNightPermit::find()->where(['user_id'=>$this->user_id,'port_id'=>$this->port_id]);
    if(!$permit->exists()){
      //it has night drive session, so Not Available
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Night Orienation Not Done').'</span> <a href="javascript:;" class="load-modal" data-url="'.Url::to(['request/night-drive-training']).'" data-heading="'.Yii::t('app','Request Night Drive Training').'">'.Yii::t('app','Request Night Orienation').'</a>';
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Late Arrival').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::t('app','Free') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if OvernightCamping is available
  * @return String
  */
  public function getRequestOvernightCamping()
  {
    if($this->booking_time_slot!=2 && $this->booking_time_slot!=Yii::$app->params['nightDriveTimeSlot']){
      //Check its time is not AM
      return '';
    }
    if($this->marina->no_of_overnight_camps==0){
      //Marina doesnt have it
      return '';
    }
    if($this->boat->overnight_camp==0){
      return '';
    }
    if($this->overnight_camping==1){
      return '';
    }

    $waitingList=WaitingListOvernightCamping::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }

    list($startHr,$startMin,$startSec)=explode(":",$this->timeZone->start_time);

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-overnight-camping">'.Yii::t('app','Request').'</a>';

    if($this->inHouseCaptainBookedOrRequested==true){
      //it has night drive session, so Not Available
      $showPrice=true;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','N/A in house captain confirmed or Requested').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::t('app','In house Captain is either confirmed or requested for this booking. Therefore an in house captain cant be provided.').'" style="color:#fff;">(i)</a>';
    }
    //If its today or day before and 2 hrs before time
    elseif($this->booking_date==date("Y-m-d") && date("H")>=($startHr-Yii::$app->appHelperFunctions->getSetting('sn_overnight_camp'))){
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
    }else{
      $totalOvernightCampsAllowedInMarina=$this->marina->no_of_overnight_camps;
      $usedOnDate=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
      if($usedOnDate>=$totalOvernightCampsAllowedInMarina){
        $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-overnight-camping" data-reason="'.Yii::$app->bookingHelperFunctions->overnightCampingLimitReachedReason.'">'.Yii::t('app','Request Wait List').'</a> <a href="javascript:;" data-toggle="tooltip" data-title="'.Yii::t('app','At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.').'">(i)</a>';
      }else{
        //Check this boat is booked for early departure next day
        $nextDate=Yii::$app->helperFunctions->getNextDay($this->booking_date);
        $nextDayEarlyDepartureBookingForThisBoat=Booking::find()->where(['city_id'=>$this->city_id,'boat_id'=>$this->boat_id,'port_id'=>$this->port_id,'booking_date'=>$nextDate,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
        if($nextDayEarlyDepartureBookingForThisBoat->exists()){
          $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-overnight-camping" data-reason="'.Yii::$app->bookingHelperFunctions->overnightCampingBoatIsEarlyDepartureNextDayReason.'">'.Yii::t('app','Request Wait List').'</a> <a href="javascript:;" data-toggle="tooltip" data-title="'.Yii::t('app','At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.').'">(i)</a>';
        }

        if($this->booking_time_slot==2){
          //If this boat has night sessions available
          //hide if its booked for ND
          $nightDriveSlot=BoatToTimeSlot::find()->where(['boat_id'=>$this->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
          if($nightDriveSlot->exists()){
            //it has night drive session, so Not Available
            $availbility='<a href="javascript:;" class="waitlist-overnight-camping" data-reason="'.Yii::$app->bookingHelperFunctions->overnightCampingBoatIsOneOfNightDrivesReason.'">'.Yii::t('app','Request').'</a>';
          }
        }
      }
    }

    $permit=UserNightPermit::find()->where(['user_id'=>$this->user_id,'port_id'=>$this->port_id]);
    if(!$permit->exists()){
      //it has night drive session, so Not Available
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Night Orienation Not Done').'</span> <a href="javascript:;" class="load-modal" data-url="'.Url::to(['request/night-drive-training']).'" data-heading="'.Yii::t('app','Request Night Drive Training').'">'.Yii::t('app','Request Night Orienation').'</a>';
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Overnight Camping').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::t('app','Free') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if Wake Boarding is available
  * @return String
  */
  public function getRequestWakeBoarding()
  {
    if($this->wake_surfing==1){
      return '';
    }
    if($this->wake_boarding==1){
      return '';
    }
    if($this->marina->wake_boarding_limit==0){
      return '';
    }
    if($this->boat->wake_boarding==0){
      return '';
    }
    $waitingList=WaitingListWakeBoarding::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }
    $waitingList=WaitingListWakeSurfing::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-wake-boarding">'.Yii::t('app','Book').'</a>';

    //If its today or day before and before 6pm
    if($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_wakeboarding'))){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff">(i)</a>';
    }else{
      $totalEquipmentsInMarina=$this->marina->wake_boarding_limit;
      $usedOnDate=self::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'wake_boarding'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
      if($usedOnDate>=$totalEquipmentsInMarina){
        $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-wake-boarding" data-reason="'.Yii::$app->bookingHelperFunctions->wakeBoardingLimitReachedReason.'">'.Yii::t('app','Request Wait List').'</a>';
      }
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Wake Boarding').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::t('app','Free') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Check if Wake Surfing is available
  * @return String
  */
  public function getRequestWakeSurfing()
  {
    if($this->wake_surfing==1){
      return '';
    }
    if($this->wake_boarding==1){
      return '';
    }
    if($this->marina->wake_surfing_limit==0){
      return '';
    }
    if($this->boat->wake_surfing==0){
      return '';
    }
    $waitingList=WaitingListWakeSurfing::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }
    $waitingList=WaitingListWakeBoarding::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($waitingList->exists()){
      return '';
    }

    $showPrice=true;
    $availbility='';

    $availbility='<a href="javascript:;" class="request-wake-surfing">'.Yii::t('app','Book').'</a>';

    //If its today or day before and before 6pm
    if($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_wakesurfing'))){
      $showPrice=false;
      $availbility='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff">(i)</a>';
    }else{
      $totalEquipmentsInMarina=$this->marina->wake_surfing_limit;
      $usedOnDate=self::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'wake_surfing'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
      if($usedOnDate>=$totalEquipmentsInMarina){
        $availbility=Yii::t('app','N/A.').' <a href="javascript:;" class="waitlist-wake-surfing" data-reason="'.Yii::$app->bookingHelperFunctions->wakeSurfingLimitReachedReason.'">'.Yii::t('app','Request Wait List').'</a>';
      }
    }

    $str ='';
    $str.='     <tr>';
    $str.='       <th>'.Yii::t('app','Wake Surfing').'</th>';
    $str.='       <td>'.$availbility.'</td>';
    $str.='       <td>'.($showPrice==true ? Yii::t('app','Free') : '').'</td>';
    $str.='     </tr>';
    return $str;
  }

  /**
  * Generates Addons Dropdown for the admin all listing
  * @return String
  */
  public function getAddonsTable()
  {
    $str ='<table class="table">';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>Captain:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasCaptain : '').'</td>';
    $str.=' </tr>';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>Equipment:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasEquipment : '').'</td>';
    $str.=' </tr>';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>BBQ Set:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasBBQ : '').'</td>';
    $str.=' </tr>';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>Ice:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasIce : '').'</td>';
    $str.=' </tr>';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>Kids Life Jackets:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasKidsLifeJacket : '').'</td>';
    $str.=' </tr>';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>Early Departure:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasEarlyDeparture : '').'</td>';
    $str.=' </tr>';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>Overnight Camping:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasOvernightCamping : '').'</td>';
    $str.=' </tr>';
    $str.=' <tr data-key="'.$this->id.'">';
    $str.='   <td>Late Arrival:</td>';
    $str.='   <td>'.($this->trashed==0 ? $this->hasLateArrival : '').'</td>';
    $str.=' </tr>';
    $str.='</table>';
    return $str;
  }

  /**
  * Generates Addons for booking report
  * @return String
  */
  public function getAddonsSpan()
  {
    $hasCaptain='';
    $hasEquipment='';
    $hasIce='';
    $hasBBQ='';
    $hasKidsLifeJackets='';
    if($this->captain==1){
      $hasCaptain=$this->captainStatus;
    }
    if($this->sport_eqp_id>0){
      $hasEquipment='<span class="badge badge-success">'.$this->waterSportsEquipment->title.'</span>';
    }
    if($this->bbq==1){
      $hasBBQ='<span class="badge badge-info">'.Yii::$app->appHelperFunctions->bbqPrice.' AED</span>';
    }
    if($this->ice==1){
      $hasIce='<span class="badge badge-info">Noted</span>';
    }
    if($this->kids_life_jacket==1){
      $hasKidsLifeJackets='<span class="badge badge-info">Noted</span>';
    }
    $str = 'Captain:'.($this->trashed==0 ? $hasCaptain : '').'<br />';
    $str.= 'Equipment:'.($this->trashed==0 ? $hasEquipment : '').'<br />';
    $str.= 'BBQ Set:'.($this->trashed==0 ? $hasBBQ : '').'<br />';
    $str.= 'Ice:'.($this->trashed==0 ? $hasIce : '').'<br />';
    $str.= 'Kids Life Jackets:'.($this->trashed==0 ? $hasKidsLifeJackets : '').'<br />';
    return $str;
  }

  /**
  * Generates Addons Table for the Operation Activity
  * @return String
  */
  public function getOprAddonsTable()
  {
    $hasCaptain='';
    $hasEquipment='';
    $hasBBQ='';
    $hasIce='';
    $hasKidsLifeJackets='';
    $hasEarlyDeparture='';
    $hasLateArrival='';
    $hasOvernightCamping='';
    $hasWakeBoarding='';
    $hasWakeSurfing='';
    if(in_array($this->boat_id,Yii::$app->appHelperFunctions->nightCruiseIds)){
      $hasCaptain='<span class="badge badge-success">'.Yii::t('app','Complimentary').'</span>';
    }else{
      if($this->captain==1){
        $hasCaptain=$this->captainStatus;
      }
    }
    if($this->sport_eqp_id>0){
      $hasEquipment='<span class="badge badge-success">'.$this->waterSportsEquipment->title.'</span>';
    }
    if($this->bbq==1){
      $hasBBQ='<span class="badge badge-info">'.Yii::$app->appHelperFunctions->bbqPrice.' AED</span>';
    }
    if($this->ice==1){
      $hasIce='<span class="badge badge-info">Free</span>';
    }
    if($this->kids_life_jacket==1){
      $hasKidsLifeJackets=($this->no_of_one_to_three_jackets>0 ? '<span class="badge badge-info">Age (1-3) = '.$this->no_of_one_to_three_jackets.'</span>' : '').($this->no_of_four_to_twelve_jackets>0 ? '<span class="badge badge-info">Age (4-12) = '.$this->no_of_four_to_twelve_jackets.'</span>' : '');
    }
    if($this->early_departure==1){
      $hasEarlyDeparture='<span class="badge badge-info">Free</span>';
    }
    if($this->late_arrival==1){
      $hasLateArrival='<span class="badge badge-info">Free</span>';
    }
    if($this->overnight_camping==1){
      $hasOvernightCamping='<span class="badge badge-info">Free</span>';
    }
    if($this->wake_boarding==1){
      $hasWakeBoarding='<span class="badge badge-info">Free</span>';
    }
    if($this->wake_surfing==1){
      $hasWakeSurfing='<span class="badge badge-info">Free</span>';
    }
    $str ='<table class="table">';
    $str.=' <tr>';
    $str.='   <td>Captain:</td>';
    $str.='   <td>'.$hasCaptain.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Equipment:</td>';
    $str.='   <td>'.$hasEquipment.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>BBQ Set:</td>';
    $str.='   <td>'.$hasBBQ.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Ice:</td>';
    $str.='   <td>'.$hasIce.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Kids Life Jackets:</td>';
    $str.='   <td>'.$hasKidsLifeJackets.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Early Departure:</td>';
    $str.='   <td>'.$hasEarlyDeparture.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Late Arrival:</td>';
    $str.='   <td>'.$hasLateArrival.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Overnight Camping:</td>';
    $str.='   <td>'.$hasOvernightCamping.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Wake Boarding:</td>';
    $str.='   <td>'.$hasWakeBoarding.'</td>';
    $str.=' </tr>';
    $str.=' <tr>';
    $str.='   <td>Wake Surfing:</td>';
    $str.='   <td>'.$hasWakeSurfing.'</td>';
    $str.=' </tr>';
    $str.='</table>';
    return $str;
  }

  /**
  * Check if Late Arrival is available
  * @return String
  */
  public function getShowRequestLateArrival()
  {
    if($this->booking_time_slot!=2){
      //Check its time is PM only
      return false;
    }elseif($this->marina->no_of_late_arrivals==0){
      //Marina doesnt have it
      return false;
    }elseif($this->boat->late_arrival==0){
      //Boat is not suitable
      return false;
    }

    //Check night drive session
    $nightDriveSlot=BoatToTimeSlot::find()->where(['boat_id'=>$this->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
    if($nightDriveSlot->exists()){
      //it has night drive session, so Not Available
      return false;
    }
    return true;
  }

  /**
  * Check if Other is available
  * @return String
  */
  public function getShowRequestOther()
  {
    return true;
  }

  /**
  * Generates Addons Dropdown for the listing
  * @return String
  */
  public function getAddonsMenu()
  {
    $str='';
    $table ='';
    if($this->status!=1){
      return '';
    }
    if($this->trashed==1){
      return '';
    }
    //Show Menu Only if its future
    if($this->booking_date>=date("Y-m-d")){
      $table.='   <table class="table table-sm table-striped table-bordered table-hover table_div">';
      $table.='     <tr>';
      $table.='       <th class="addons-col-1">Type</th>';
      $table.='       <th class="addons-col-2">Availability</th>';
      $table.='       <th class="addons-col-3">Charge</th>';
      $table.='     </tr>';

      //Captain
      $table.=$this->requestCaptainAddon;

      //Equipment
      $table.=$this->requestEquipment;

      //BBQ Set
      $table.=$this->requestBBQSet;

      //Wake Boarding
      $table.=$this->requestWakeBoarding;

      //Wake Surfing
      $table.=$this->requestWakeSurfing;

      //Ice
      $table.=$this->requestIce;

      //Kids Life Jacket
      $table.=$this->requestKidsLifeJacket;

      //Early Departure
      $table.=$this->requestEarlyDeparture;

      //Overnight Camping
      $table.=$this->requestOvernightCamping;

      //Late Arrival
      $table.=$this->requestLateArrival;

      //Other
      if($this->showRequestOther==true){
        $table.='     <tr>';
        $table.='       <th>Other</th>';
        $table.='       <td>'.$this->hasOther.'</td>';
        $table.='       <td></td>';
        $table.='     </tr>';
      }
      $table.='   </table>';

      $str ='<div class="d-block d-sm-none">';
      $str .='  <button type="button" class="mb-1 mt-1 mr-1 btn btn-default btn-xs" data-toggle="modal" data-target="#myModal'.$this->id.'">Addons <span class="caret"></span></button>';

      $str .='  <div class="modal fade sm_book_model" id="myModal'.$this->id.'" role="dialog">';
      $str .='    <div class="modal-dialog modal-sm" style="top: 25%; padding: 20px;">';
      $str .='      <div class="modal-content">';
      $str .='        <div class="modal-header">';
      $str .='          <h5 class="modal-title">Addons / '.$this->boat->name.' on '.Yii::$app->formatter->asDate($this->booking_date).' - '.$this->timeZone->name.'</h5>';
      $str .='          <button type="button" class="close" data-dismiss="modal">&times;</button>';

      $str .='        </div>';
      $str .='        <div class="modal-body">';
      $str .='          <div class="table-responsive">';
      $str .= $table;
      $str .='          </div>';
      $str .= '       </div>';
      $str .= '     </div>';
      $str .= '   </div>';
      $str .= ' </div>';
      $str .= '</div>';
    //  $str .= '</div>';

      $str.='<div class="btn-group dropright d-none d-sm-block">';
      $str.=' <button type="button" class="mb-1 mt-1 mr-1 btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Addons <span class="caret"></span></button>';
      $str.=' <div class="dropdown-menu dropdown-menu-right" role="menu">';
      $str .= $table;
      $str.=' </div>';
      $str.='</div>';
    }
    $captainRequestedRow=$this->isCaptainRequested;
    $equipmentRequestedRow=$this->isEquipmentRequested;
    $bbqSetRequestedRow=$this->isBBQSetRequested;
    $wakeBoardingRequestedRow=$this->isWakeBoardingRequested;
    $wakeSurfingRequestedRow=$this->isWakeSurfingRequested;
    $iceRequestedRow=$this->isIceRequested;
    $kidsLifeJacketRequestedRow=$this->isKidsLifeJacketRequested;
    $earlyDepartureRequestedRow=$this->isEarlyDepartureRequested;
    $overnightCampingRequestedRow=$this->isOvernightCampingRequested;
    $lateArrivalRequestedRow=$this->isLateArrivalRequested;

    if(
      $captainRequestedRow!=''
      || $equipmentRequestedRow!=''
      || $bbqSetRequestedRow!=''
      || $wakeBoardingRequestedRow!=''
      || $wakeSurfingRequestedRow!=''
      || $iceRequestedRow!=''
      || $kidsLifeJacketRequestedRow!=''
      || $earlyDepartureRequestedRow!=''
      || $overnightCampingRequestedRow!=''
      || $lateArrivalRequestedRow!=''
    ){
      $str.='<table class="table requested-addons-list">';
      //$str.=$captainRequestedRow;
      $str.=$equipmentRequestedRow;
      $str.=$bbqSetRequestedRow;
      $str.=$wakeBoardingRequestedRow;
      $str.=$wakeSurfingRequestedRow;
      $str.=$iceRequestedRow;
      $str.=$kidsLifeJacketRequestedRow;
      $str.=$earlyDepartureRequestedRow;
      $str.=$overnightCampingRequestedRow;
      $str.=$lateArrivalRequestedRow;
      $str.='</table>';
    }
    return $str;
  }

  public function getCaptainStatus()
  {
    if($this->captain_type!=null){
      if($this->captain_type=='free'){
        $status='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span> <span class="badge badge-info">Free</span>';
      }else{
        $status='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span> <span class="badge badge-info">'.Yii::$app->appHelperFunctions->captainPrice.'AED</span>';
      }
    }else{
      $captainType=Yii::$app->bookingHelperFunctions->getCaptainStatus($this->member,$this);
      if($captainType=='free'){
        $status='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span> <span class="badge badge-info">Free</span>';
      }else{
        $status='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span> <span class="badge badge-info">'.Yii::$app->appHelperFunctions->captainPrice.'AED</span>';
      }
    }
    return $status;
  }

  public function getCaptainStatusArray()
  {
    $status=[];
    if($this->captain_type!=null){
      $status['status']='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      if($this->captain_type=='free'){
        $status['price']=Yii::t('app','Free');
      }else{
        $status['price']=Yii::$app->appHelperFunctions->captainPrice.Yii::t('app','AED');
      }
    }else{
      $captainType=Yii::$app->bookingHelperFunctions->getCaptainStatus($this->member,$this);
      $status['status']='<span class="badge badge-success">'.Yii::t('app','Confirmed').'</span>';
      if($captainType=='free'){
        $status['price']=Yii::t('app','Free');
      }else{
        $status['price']=Yii::$app->appHelperFunctions->captainPrice.Yii::t('app','AED');
      }
    }
    return $status;
  }

  public function getHasCaptain()
  {
    if($this->captain==1){
      $status=$this->captainStatus;
      if($this->booking_date>date("Y-m-d")){
        $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-captain"><i class="fa fa-times"></i></a>';
      }
    }else{
      $waitingList=WaitingListCaptain::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $status='<span class="badge badge-info">Under Process</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-captain-waiting"><i class="fa fa-times"></i></a>';
        }
        return $status;
      }
      if($this->booking_date<date("Y-m-d")){
        $status='';
      }elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
      }else{
        if(in_array($this->boat_id,Yii::$app->appHelperFunctions->dropOffIds)){
          return '<span class="badge badge-warning">'.Yii::t('app','Not Applicable').'</span>';
        }elseif(in_array($this->boat_id,Yii::$app->appHelperFunctions->nightCruiseIds)){
          return '<span class="badge badge-success">'.Yii::t('app','Complimentary').'</span>';
        }elseif(in_array($this->boat_id,Yii::$app->appHelperFunctions->marinaStayInIds)){
          return '<span class="badge badge-warning">'.Yii::t('app','Not Applicable').'</span>';
        }else{


          $totalCaptainsInMarina=$this->marina->captains;
          $usedOnDate=self::find()->where(['booking_date'=>$this->booking_date,'port_id'=>$this->port_id,'booking_time_slot'=>$this->booking_time_slot,'captain'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
          if($usedOnDate>=$totalCaptainsInMarina){
            return 'N/A. <a href="javascript:;" class="waitlist-captain">Request Wait List</a> <a href="javascript:;"data-toggle="tooltip" data-title="At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.">(i)</a>';
          }

          if($this->booking_time_slot==Yii::$app->params['nightDriveTimeSlot']){
            return '<a href="javascript:;" class="nd-captain">'.Yii::t('app','Book').'</a>';
          }else{
            $status='<a href="javascript:;" class="request-captain">'.Yii::t('app','Book').'</a>';
          }
        }
      }
    }
    return $status;
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getWaterSportsEquipment()
  {
    return $this->hasOne(WaterSportEquipment::className(), ['id' => 'sport_eqp_id']);
  }

  public function getHasEquipment()
  {
    if($this->sport_eqp_id>0){
      $status='<span class="badge badge-success">'.$this->waterSportsEquipment->title.'</span>';
      if($this->booking_date>date("Y-m-d")){
        $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-equipment"><i class="fa fa-times"></i></a>';
      }
    }else{
      $waitingList=WaitingListSportsEquipment::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $status='<span class="badge badge-info">Under Process</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-equipment-waiting"><i class="fa fa-times"></i></a>';
        }
        return $status;
      }
      if($this->booking_date<date("Y-m-d")){
        $status='';
      }elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_donut'))){
        $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff">(i)</a>';
      }else{
        $status='<a href="javascript:;" class="request-equip">'.Yii::t('app','Book').'</a>';
      }
    }
    return $status;
  }

  public function getHasBBQ()
  {
    if($this->boat->bbq==0){
      return '<span class="badge badge-warning">Not available</span>';
    }elseif($this->city_id==Yii::$app->appHelperFunctions->cityIdz['onlydubai']){
      return '<span class="badge badge-warning">Not available</span>';
    }else{
      $waitingList=WaitingListBbq::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $status='<span class="badge badge-info">Under Process</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-bbq-waiting"><i class="fa fa-times"></i></a>';
        }
        return $status;
      }
      if($this->bbq==1){
        $status='<span class="badge badge-info">'.Yii::$app->appHelperFunctions->bbqPrice.' AED</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-bbq"><i class="fa fa-times"></i></a>';
        }
      }elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_bbq'))){
        $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff">(i)</a>';
      }elseif($this->booking_date<date("Y-m-d")){
        $status='';
      }else{
        $totalEquipmentsInMarina=$this->marina->bbq;
        $usedOnDate=self::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'bbq'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate>=$totalEquipmentsInMarina){
          return 'N/A. <a href="javascript:;" class="waitlist-bbq">Request Wait List</a>';
        }
        $status='<a href="javascript:;" class="request-bbq">'.Yii::t('app','Book').'</a>';
      }
    }
    return $status;
  }

  public function getHasIce()
  {
    if($this->boat->ice==0){
      return '<span class="badge badge-warning">Not available</span>';
    }else{
      if($this->ice==1){
        $status='<span class="badge badge-info">Noted</span>';
        if($this->booking_date>date("Y-m-d")){
          //$status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-ice"><i class="fa fa-times"></i></a>';
        }
      }elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_ice'))){
        $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff;">(i)</a>';
      }elseif($this->booking_date<date("Y-m-d")){
        $status='';
      }else{
        $status='<a href="javascript:;" class="request-ice">'.Yii::t('app','Request').'</a>';
      }
    }
    return $status;
  }

  public function getHasKidsLifeJacket()
  {
    if($this->boat->kids_life_jacket==0){
      return '<span class="badge badge-warning">Not available</span>';
    }else{
      if($this->kids_life_jacket==1){
        $status='<span class="badge badge-info">Noted</span>';
        if($this->booking_date>date("Y-m-d")){
          //$status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-kids-life-jacket"><i class="fa fa-times"></i></a>';
        }
      }elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_kids_life_jacket'))){
        $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="'.Yii::$app->helperFunctions->generalShortNoticeMessage.'" style="color:#fff;">(i)</a>';
      }elseif($this->booking_date<date("Y-m-d")){
        $status='';
      }else{
        $status='<a href="javascript:;" class="load-modal" data-url="'.Url::to(['request/book-kids-life-jacket','id'=>$this->id]).'" data-heading="Request Kids Life Jackets">'.Yii::t('app','Request').'</a>';
      }
    }
    return $status;
  }

  public function getHasOther()
  {
    if($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
      $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
    }elseif($this->booking_date<date("Y-m-d")){
      $status='';
    }else{
      $status='<a href="javascript:;" class="load-modal" data-url="'.Url::to(['request/other','user_id'=>$this->user_id,'booking_id'=>$this->id]).'" data-heading="Request Other" data-mid="general-modal-secondary">'.Yii::t('app','Request').'</a>';
    }
    return $status;
  }

  public function getHasEarlyDeparture()
  {
    if($this->booking_time_slot!=1){
      return '<span class="badge badge-warning no-am">Not available</span>';
    }elseif($this->marina->no_of_early_departures==0){
      return '<span class="badge badge-warning">Not available</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="Sorry Early Departures are not available from DMYC, this service is available from JMH only." style="color:#fff;">(!)</a>';
    }elseif($this->boat->early_departure==0){
      return '<span class="badge badge-warning">Not available</span>';
    }else{
      //If In House Captain is requested
      if($this->inHouseCaptainBookedOrRequested==true){
        //it has night drive session, so Not Available
        return '<span class="badge badge-warning">N/A in house captain confirmed or Requested</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="In house Captain is either confirmed or requested for this booking. Therefore an in house captain cant be provided." style="color:#fff;">(i)</a>';
      }

      $waitingList=WaitingListEarlyDeparture::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $status='<span class="badge badge-info">Under Process</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-early-departure-waiting"><i class="fa fa-times"></i></a>';
        }
        return $status;
      }

      if($this->early_departure==1){
        $status='<span class="badge badge-info">Free</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-early-departure"><i class="fa fa-times"></i></a>';
        }
      }elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_early_departure'))){
        $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
      }elseif($this->booking_date<date("Y-m-d")){
        $status='';
      }else{
        $totalEarlyDeparturesAllowedInMarina=$this->marina->no_of_early_departures;
        $usedOnDate=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate>=$totalEarlyDeparturesAllowedInMarina){
          return 'N/A. <a href="javascript:;" class="waitlist-early-departure">Request Wait List</a> <a href="javascript:;"data-toggle="tooltip" data-title="At the moment this service is booked. by clicking on the "Wait list" button, it will automatically book it for you if it baceame available.">(i)</a>';
        }else{
          $previousDate=Yii::$app->helperFunctions->getDayBefore($this->booking_date);
          $previousDayOvernightCampingBookingForThisBoat=self::find()->where(['city_id'=>$this->city_id,'boat_id'=>$this->boat_id,'port_id'=>$this->port_id,'booking_date'=>$previousDate,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
          if($previousDayOvernightCampingBookingForThisBoat->exists()){
            return 'N/A. <a href="javascript:;" class="waitlist-early-departure">Request Wait List</a> <a href="javascript:;"data-toggle="tooltip" data-title="At the moment this service is booked. by clicking on the "Wait list" button, it will automatically book it for you if it baceame available.">(i)</a>';
          }
        }
        $status='<a href="javascript:;" class="request-early-departure">'.Yii::t('app','Request').'</a>';
      }
    }
    return $status;
  }

  public function getHasOvernightCamping()
  {
    if($this->booking_time_slot!=2 && $this->booking_time_slot!=Yii::$app->params['nightDriveTimeSlot']){
      //Check its time is AM
      return '<span class="badge badge-warning no-suitable-time">Not available</span>';
    }elseif($this->marina->no_of_overnight_camps==0){
      //Marina doesnt have it
      return '<span class="badge badge-warning marina-not-allowed">Not available</span>';
    }elseif($this->boat->overnight_camp==0){
      return '<span class="badge badge-warning boat-not-suitable">Not available</span>';
    }else{
      //If In House Captain is requested
      if($this->inHouseCaptainBookedOrRequested==true){
        //it has night drive session, so Not Available
        return '<span class="badge badge-warning">N/A in house captain confirmed or Requested</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="In house Captain is either confirmed or requested for this booking. Therefore an in house captain cant be provided." style="color:#fff;">(i)</a>';
      }

      //Check member has night drive permit
      $permit=UserNightPermit::find()->where(['user_id'=>$this->user_id,'port_id'=>$this->port_id]);
      if(!$permit->exists()){
        //it has night drive session, so Not Available
        return '<span class="badge badge-warning">Night Orienation Not Done</span> <a href="javascript:;" class="load-modal" data-url="'.Url::to(['request/night-drive-training']).'" data-heading="Request Night Drive Training">Request Night Orienation</a>';
      }

      $waitingList=WaitingListOvernightCamping::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $status='<span class="badge badge-info">Under Process</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-overnight-camping-waiting"><i class="fa fa-times"></i></a>';
        }
        return $status;
      }

      if($this->overnight_camping==1){
        $status='<span class="badge badge-info">Free</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-overnight-camping"><i class="fa fa-times"></i></a>';
        }
      }elseif($this->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date) && date("H")>=Yii::$app->appHelperFunctions->getSetting('sn_overnight_camp'))){
        $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
      }elseif($this->booking_date<date("Y-m-d")){
        $status='';
      }else{
        $totalOvernightCampsAllowedInMarina=$this->marina->no_of_overnight_camps;
        $usedOnDate=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate>=$totalOvernightCampsAllowedInMarina){
          return 'N/A. <a href="javascript:;" class="waitlist-overnight-camping">Request Wait List</a> <a href="javascript:;" data-toggle="tooltip" data-title="At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.">(i)</a>';
        }else{
          //Check this boat is booked for early departure next day
          $nextDate=Yii::$app->helperFunctions->getNextDay($this->booking_date);
          $nextDayEarlyDepartureBookingForThisBoat=Booking::find()->where(['city_id'=>$this->city_id,'boat_id'=>$this->boat_id,'port_id'=>$this->port_id,'booking_date'=>$nextDate,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
          if($nextDayEarlyDepartureBookingForThisBoat->exists()){
            return 'N/A. <a href="javascript:;" class="waitlist-overnight-camping">Request Wait List</a> <a href="javascript:;" data-toggle="tooltip" data-title="At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.">(i)</a>';
          }

          if($this->booking_time_slot==2){
            //If this boat has night sessions available
            $nightDriveSlot=BoatToTimeSlot::find()->where(['boat_id'=>$this->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
            if($nightDriveSlot->exists()){
              //it has night drive session, so Not Available
              return 'N/A. <a href="javascript:;" class="waitlist-overnight-camping">Request Wait List</a> <a href="javascript:;" data-toggle="tooltip" data-title="At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.">(i)</a>';
            }
          }
        }
        $status='<a href="javascript:;" class="request-overnight-camping">'.Yii::t('app','Request').'</a>';
      }
    }
    return $status;
  }

  public function getHasLateArrival()
  {
    if($this->booking_time_slot!=2){
      //Check its time is PM only
      return '<span class="badge badge-warning no-pm">Not available</span>';
    }elseif($this->marina->no_of_late_arrivals==0){
      //Marina doesnt have it
      return '<span class="badge badge-warning marina-not-allowed">Not available</span>';
    }elseif($this->boat->late_arrival==0){
      //Boat is not suitable
      return '<span class="badge badge-warning boat-not-suitable">Not available</span>';
    }else{
      //If In House Captain is requested
      if($this->inHouseCaptainBookedOrRequested==true){
        //it has night drive session, so Not Available
        return '<span class="badge badge-warning">N/A in house captain confirmed or Requested</span> <a href="javascript:;" class="badge badge-info" data-toggle="tooltip" data-title="In house Captain is either confirmed or requested for this booking. Therefore an in house captain cant be provided." style="color:#fff;">(i)</a>';
      }
      //Check night drive session
      $nightDriveSlot=BoatToTimeSlot::find()->where(['boat_id'=>$this->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
      if($nightDriveSlot->exists()){
        //it has night drive session, so Not Available
        return '<span class="badge badge-warning night-session-exists">Not available</span>';
      }

      //Check member has night drive permit
      $permit=UserNightPermit::find()->where(['user_id'=>$this->user_id,'port_id'=>$this->port_id]);
      if(!$permit->exists()){
        //it has night drive session, so Not Available
        return '<span class="badge badge-warning">Night Orienation Not Done</span> <a href="javascript:;" class="load-modal" data-url="'.Url::to(['request/night-drive-training']).'" data-heading="Request Night Drive Training">Request Night Orienation</a>';
      }

      $waitingList=WaitingListLateArrival::find()->where(['booking_id'=>$this->id,'status'=>0]);
      if($waitingList->exists()){
        $status='<span class="badge badge-info">Under Process</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-late-arrival-waiting"><i class="fa fa-times"></i></a>';
        }
        return $status;
      }

      if($this->late_arrival==1){
        $status='<span class="badge badge-info">Free</span>';
        if($this->booking_date>date("Y-m-d")){
          $status.=' <a href="javascript:;" class="btn btn-xs btn-danger cancel-late-arrival"><i class="fa fa-times"></i></a>';
        }
      }
      //Date is 1 day before
      //elseif((date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($this->booking_date)){
      //  $status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
      //}
      //Its today and time is already 10
      elseif($this->booking_date==date("Y-m-d") && date("H")>=10){
        //Check if there is any night drive booking on date
        $nightDriveBooking=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>Yii::$app->params['nightDriveTimeSlot'],'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
        if(!$nightDriveBooking->exists()){
          //No night drive for date, so Not Available
          return '<span class="badge badge-warning no-night-drive-booking">Not available</span>';
        }else{
          //Night drive exists can book
          $status='<a href="javascript:;" class="request-late-arrival">'.Yii::t('app','Request').'</a>';
        }
        //$status='<span class="badge badge-warning">'.Yii::t('app','Short Notice').'</span>';
      }elseif($this->booking_date<date("Y-m-d")){
        $status='';
      }else{
        $totalLateArrivalsAllowedInMarina=$this->marina->no_of_late_arrivals;
        $usedOnDate=Booking::find()->where(['city_id'=>$this->city_id,'port_id'=>$this->port_id,'booking_date'=>$this->booking_date,'booking_time_slot'=>$this->booking_time_slot,'late_arrival'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate>=$totalLateArrivalsAllowedInMarina){
          return 'N/A. <a href="javascript:;" class="waitlist-late-arrival">Request Wait List</a> <a href="javascript:;" data-toggle="tooltip" data-title="At the moment this service is booked. by clicking on the Wait list button, it will automatically book it for you if it baceame available.">(i)</a>';
        }

        $status='<a href="javascript:;" class="request-late-arrival">'.Yii::t('app','Request').'</a>';
      }
    }
    return $status;
  }

  public function getInHouseCaptainBookedOrRequested()
  {
    if($this->captain==1){
      return true;
    }
    $captainWaiting=WaitingListCaptain::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($captainWaiting->exists()){
      return true;
    }
    return false;
  }

  public function getEarlyDepartureBookedOrRequested()
  {
    if($this->early_departure==1){
      return true;
    }
    $earlyDepartureWaiting=WaitingListEarlyDeparture::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($earlyDepartureWaiting->exists()){
      return true;
    }
    return false;
  }

  public function getOvernightCampingBookedOrRequested()
  {
    if($this->overnight_camping==1){
      return true;
    }
    $overnightCamptingWaiting=WaitingListOvernightCamping::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($overnightCamptingWaiting->exists()){
      return true;
    }
    return false;
  }

  public function getLateArrivalBookedOrRequested()
  {
    if($this->late_arrival==1){
      return true;
    }
    $lateArrivalWaiting=WaitingListLateArrival::find()->where(['booking_id'=>$this->id,'status'=>0]);
    if($lateArrivalWaiting->exists()){
      return true;
    }
    return false;
  }

  /**
  * Generates Remarks html
  */
  public function getRemarks()
  {
    $remarks="";
    if(Yii::$app->user->identity->user_type!=0){
      $remarks=(isset(Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type]) ? Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type].': ' : '').$this->booking_comments;
      if(Yii::$app->user->identity->user_type!=0 && Yii::$app->menuHelperFunction->checkActionAllowed('update-comments','booking') && $this->trashed==0 && $this->booking_date>=date("Y-m-d")){
        $remarks.='<br /><a href="javascript:;" class="update-comments"><i class="fa fa-edit"></i></a>';
      }
    }
    return $remarks;
  }

  public function getTripExperienceComments()
  {
    $html = '';
    if($this->bookingActivity!=null && $this->bookingActivity->trip_exp>0){
      $html.= Yii::$app->helperFunctions->tripExperience[$this->bookingActivity->trip_exp];

      if($this->bookingActivity->trip_exp_comments!=''){
        $html.=($html!='' ? ' - ' : '').$this->bookingActivity->trip_exp_comments;
      }
    }
    return $html;
  }

  /**
  * Generates Remarks html
  */
  public function getOprRemarks()
  {
    $html = '';
    if(isset(Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type])){
      $html.= 'Booking: '.Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type];
    }
    if($this->booking_comments!=''){
      $html.=($html!='' ? ' - ' : '').$this->booking_comments;
    }
    if($this->bookingActivity!=null){
      if($this->bookingActivity->overall_remarks!=''){
        $html.=($html!='' ? '<hr />' : '').'Operations: '.$this->bookingActivity->overall_remarks;
      }
    }
    return $html;
  }

  /**
  * Generates Activity Extras/Addons html
  */
  public function getBookingExtras()
  {
    $html='';
    $results=BookingActivityAddons::find()->select(['addon_id'])->where(['booking_id'=>$this->id])->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $html.='<span class="badge badge-info">'.Yii::$app->helperFunctions->bookingExtras[$result['addon_id']].'</span>';
      }
    }
    return $html;
  }

  /**
  * Generates Activity Damages html
  */
  public function getBookingDamages()
  {
    $html='';
    $results=BookingActivityDamages::find()
    ->select([
      'item_id',
      DamageCheckItems::tableName().'.title',
      'item_condition',
      'item_comments',
    ])
    ->innerJoin(DamageCheckItems::tableName(),DamageCheckItems::tableName().".id=".BookingActivityDamages::tableName().".item_id")
    ->where(['booking_id'=>$this->id])->asArray()->all();
    if($results!=null){
      $n=1;
      foreach($results as $result){
        $html.='<strong>'.$n.'.</strong>'.$result['title'].' ('.$result['item_condition'].' - '.$result['item_comments'].')<br />';
        $n++;
      }
    }
    return $html;
  }

  public function getEarlyDepartureAssets()
  {
    return BookingEarlyDepartureAssets::find()->where(['booking_id'=>$this->id])->asArray()->one();
  }

  /**
  * Sends user email for confirmed early departure with booking details
  */
  public function sendEarlyDepartureConfirmationEmail()
  {
    $html = "Dear Captain ".$this->member->fullname.",\n\r\n\r";
    $html.= "your Early Departure request is Confirmed.\n\r\n\r";
    $html.= "Since The Captains Club team will not hand over the boat Physically in the marina:\n\r\n\r";
    $html.= "The boat considered under your reponsibilty upon recieving the hand over email or whatsapp that will include all the required details\n\r\n\r";
    $html.= "The Captain's Club reserves the right to cancel this request in case of any last minute emergencies.\n\r\n\r";

    $htmlBody=nl2br($html);

    Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $html])
    ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
    ->setTo($this->member->email)
    ->setSubject('Early Departure Confirmation Alert')
    ->send();
  }

  /**
  * Sends user email with booking details
  */
  public function sendUserEmail()
  {
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_newbooking_u');
    $template=EmailTemplate::findOne($templateId);
    if($template!=null){
      $boatInfoHtml=Yii::$app->appHelperFunctions->getBoatInfoHtml($this->boat_id);
      $boatInfoText=Yii::$app->appHelperFunctions->getBoatInfoText($this->boat_id);
      if($this->port_id==Yii::$app->params['emp_id']){
        $boatFuelConsumptionChartHtml='';
        $boatFuelConsumptionChartText='';
      }else{
        $boatFuelConsumptionChartHtml=Yii::$app->appHelperFunctions->getBoatFuelConsumptionChartHtml($this->boat_id);
        $boatFuelConsumptionChartText=Yii::$app->appHelperFunctions->getBoatFuelConsumptionChartText($this->boat_id);
      }
      $vals = [
        '{username}' => $this->member->fullname,
        '{email}' => $this->member->email,
        '{bookingCity}' => $this->city->name,
        '{bookingMarina}' => $this->marina->name,
        '{bookingBoat}' => $this->emailBoatName,
        '{bookingDate}' => Yii::$app->formatter->asDate($this->booking_date),
        '{bookingTime}' => $this->timeZone->name,
        '{bookingComments}' => ($this->booking_type!=null && $this->booking_type>0 ? '<strong>Comments:</strong> '.Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type].' - '.$this->booking_comments : ''),
        '{boatInfoHtml}' => $boatInfoHtml,
        '{boatInfoText}' => $boatInfoText,
        '{boatFuelConsumptionChartHtml}' => $boatFuelConsumptionChartHtml,
        '{boatFuelConsumptionChartText}' => $boatFuelConsumptionChartText,
      ];
      $htmlBody=$template->searchReplace($template->template_html,$vals);
      $textBody=$template->searchReplace($template->template_text,$vals);
      $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
      ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
      ->setSubject('Booking Confirmation Alert');

      if($this->member->user_type==0){
        foreach($this->member->activeContractMembers as $contractMember){
          $canSend=true;
          //Check if user is not black listed
          $isRed=BlackListedEmail::find()->select(['email'])->where(['email'=>$contractMember->email,'trashed'=>0]);
          if($isRed->exists()){
            $canSend=false;
          }
          //Check if user is not in dropped list
          $isRed=DroppedEmail::find()->select(['email'])->where(['email'=>$contractMember->email,'trashed'=>0]);
          if($isRed->exists()){
            $canSend=false;
          }
          if($canSend==true){
            $message->setTo($contractMember->email);
            $message->send();
          }
        }
      }else{
        $message->setTo($this->member->email);
        $message->send();
      }
    }
  }

  /**
  * Sends Admin email with booking details
  */
  public function sendAdminEmail()
  {
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_newbooking_a');
    $template=EmailTemplate::findOne($templateId);
    if($template!=null){
      $boatInfoHtml=Yii::$app->appHelperFunctions->getBoatInfoHtml($this->boat_id);
      $boatInfoText=Yii::$app->appHelperFunctions->getBoatInfoText($this->boat_id);
      if($this->port_id==Yii::$app->params['emp_id']){
        $boatFuelConsumptionChartHtml='';
        $boatFuelConsumptionChartText='';
      }else{
        $boatFuelConsumptionChartHtml=Yii::$app->appHelperFunctions->getBoatFuelConsumptionChartHtml($this->boat_id);
        $boatFuelConsumptionChartText=Yii::$app->appHelperFunctions->getBoatFuelConsumptionChartText($this->boat_id);
      }
      $vals = [
        '{username}' => $this->member->fullname,
        '{email}' => $this->member->email,
        '{bookingCity}' => $this->city->name,
        '{bookingMarina}' => $this->marina->name,
        '{bookingBoat}' => $this->emailBoatName,
        '{bookingDate}' => Yii::$app->formatter->asDate($this->booking_date),
        '{bookingTime}' => $this->timeZone->name,
        '{bookingComments}' => ($this->booking_type!=null && $this->booking_type>0 ? Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type].': '.$this->booking_comments : ''),
        '{boatInfoHtml}' => $boatInfoHtml,
        '{boatInfoText}' => $boatInfoText,
        '{boatFuelConsumptionChartHtml}' => $boatFuelConsumptionChartHtml,
        '{boatFuelConsumptionChartText}' => $boatFuelConsumptionChartText,
      ];
      $htmlBody=$template->searchReplace($template->template_html,$vals);
      $textBody=$template->searchReplace($template->template_text,$vals);
      Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
      ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
      ->setTo(Yii::$app->appHelperFunctions->getSetting('adminEmail'))
      ->setSubject('Booking Confirmation Alert')
      ->send();
    }
  }

  /**
  * Sends admin email Boat is allocated
  */
  public function sendAdminBoatAllocated()
  {
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_newrequiredbooking_a');
    $template=EmailTemplate::findOne($templateId);
    if($template!=null){
      $vals = [
        '{bookingMarina}' => $this->marina->name,
        '{bookingBoat}' => $this->emailBoatName,
        '{bookingDate}' => Yii::$app->formatter->asDate($this->booking_date),
        '{bookingTime}' => $this->timeZone->name,
      ];
      $htmlBody=$template->searchReplace($template->template_html,$vals);
      $textBody=$template->searchReplace($template->template_text,$vals);
      Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
      ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
      ->setTo(Yii::$app->params['boatRequiredEmailTo'])
      ->setCc([Yii::$app->params['mdEmail'],Yii::$app->params['icareEmail']])
      ->setSubject('Boat Allocated')
      ->send();
    }
    $smsMsg="Dear Caren\n
    Upon you request the following was found\n
    ".$this->boat->name."
    ".$this->marina->name."
    ".$this->timeZone->name."
    ".Yii::$app->formatter->asDate($this->booking_date).",\n
    Best Regards";
    Yii::$app->helperFunctions->sendSms("971558118017",$smsMsg);
  }

  public function metRequest($type,$equip_id=null)
  {
    $sendEmail=false;
    $request=null;
    if($type=='captain')$request=WaitingListCaptain::find()->where(['booking_id'=>$this->id])->one();
    if($type=='bbq')$request=WaitingListBbq::find()->where(['booking_id'=>$this->id])->one();
    if($type=='wakeboarding')$request=WaitingListWakeBoarding::find()->where(['booking_id'=>$this->id])->one();
    if($type=='wakesurfing')$request=WaitingListWakeSurfing::find()->where(['booking_id'=>$this->id])->one();
    if($type=='equipment')$request=WaitingListSportsEquipment::find()->where(['booking_id'=>$this->id])->one();
    if($type=='late_arrival')$request=WaitingListLateArrival::find()->where(['booking_id'=>$this->id])->one();
    if($type=='overnight_camping')$request=WaitingListOvernightCamping::find()->where(['booking_id'=>$this->id])->one();

    if($request==null){
      if($type=='captain')$request=new WaitingListCaptain;
      if($type=='bbq')$request=new WaitingListBbq;
      if($type=='wakeboarding')$request=new WaitingListWakeBoarding;
      if($type=='wakesurfing')$request=new WaitingListWakeSurfing;
      if($type=='equipment'){
        $request=new WaitingListSportsEquipment;
        $request->equipment_id=$equip_id;
      }
      if($type=='early_departure')$request=new WaitingListEarlyDeparture;
      if($type=='late_arrival')$request=new WaitingListLateArrival;
      if($type=='overnight_camping')$request=new WaitingListOvernightCamping;

      $request->user_id=$this->user_id;
      $request->booking_id=$this->id;
      $request->city_id=$this->city_id;
      $request->marina_id=$this->port_id;
      $request->date=$this->booking_date;
      $request->time_id=$this->booking_time_slot;
      $request->reason='';
      $request->expected_reply='';
    }else{
      $sendEmail=true;
    }
    $request->active_show_till=date("Y-m-d", strtotime("+2 day", strtotime($this->booking_date)));
    $request->status=1;
    $request->save();
    if($sendEmail==true)Yii::$app->helperFunctions->sendMetEmail($type,$this);
  }

  public function sendExperienceMail($eaType,$subject,$column,$template,$result)
  {

    $connection = \Yii::$app->db;

    $emailNotification=new BookingActivityEmailNotification;
    $emailNotification->booking_id=$result->id;
    $emailNotification->email_type=$eaType;
    $emailNotification->email_to=$result->member->email;
    $emailNotification->save();

    if($eaType=='lateArrival'){
      $bookingActivity=$result->bookingActivity;
      $lateTime = $bookingActivity->member_exp_late_hr.' hr'.($bookingActivity->member_exp_late_min!='' ? ' & '.$bookingActivity->member_exp_late_min.' mins' : '');

      $msgHtml=str_replace("{lateMinutes}",'<span style="color:red">'.$lateTime.'</span>',$template->template_html).'<br />';
      $msgText=str_replace("{lateMinutes}",''.$lateTime.'',$template->template_text).'<br />';
    }else{
      $msgHtml=$template->template_html;
      $msgText=$template->template_text;
    }

    $message=Yii::$app->mailer->compose(['html' => 'tripExperience-html', 'text' => 'tripExperience-text'], ['booking' => $result, 'msgHtml' => $msgHtml, 'msgText' => $msgText])
      ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
      ->setTo($result->member->email)
      ->setSubject($subject);

    if($eaType=='noShow'){
      $connection->createCommand(
        "update ".Booking::tableName()." set ".$column."=1 where id=:id",
        [':id'=>$result->id]
      )->execute();
    }else{
      $connection->createCommand(
        "update ".BookingActivity::tableName()." set ".$column."=1 where booking_id=:booking_id",
        [':booking_id'=>$result->id]
      )->execute();
    }

    if($message->send()){
      $connection->createCommand(
        "update ".BookingActivityEmailNotification::tableName()." set status=1,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
        [':remarks'=>'Sent',':booking_id'=>$result->id,':email_type'=>$eaType]
      )->execute();
    }else{
      $connection->createCommand(
        "update ".BookingActivityEmailNotification::tableName()." set status=0,remarks=:remarks where booking_id=:booking_id and email_type=:email_type",
        [':remarks'=>'Error while sending',':booking_id'=>$result->id,':email_type'=>$eaType]
      )->execute();
    }
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      $booking_rssid=Yii::$app->helperFunctions->generateTimeId().$this->id;
      $this->booking_rssid=$booking_rssid;
      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".self::tableName()." set booking_rssid=:booking_rssid where id=:id",
        [
          ':booking_rssid'=>$booking_rssid,
          ':id'=>$this->id,
        ]
        )
        ->execute();
        if($this->boat->special_boat==1 && $this->boat->boat_selection==1 && $this->selected_boat!=null && $this->selected_boat>0){
          $bookingSpBSelection=new BookingSpecialBoatSelection;
          $bookingSpBSelection->booking_id=$this->id;
          $bookingSpBSelection->boat_id=$this->selected_boat;
          $bookingSpBSelection->save();
        }
      }
      parent::afterSave($insert, $changedAttributes);
    }

    /**
    * @inheritdoc
    */
    public function beforeDelete()
    {
      $this->softDelete();
      return false;
    }

    /**
    * Mark record as deleted and hides fron list.
    * @return boolean
    */
    public function updateStatus($status)
    {
      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",
        [
          ':status'=>$status,
          ':updated_at'=>date("Y-m-d H:i:s"),
          ':updated_by'=>Yii::$app->user->identity->id,
          ':id'=>$this->id,
        ]
        )
        ->execute();
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Booking ('.$this->booking_date.' / '.$this->boat->name.' from '.$this->marina->name.') marked as '.Yii::$app->helperFunctions->bookingStatus[$status].' successfully');
        return true;
      }

      public function sendMemberReminder($template)
      {
        $boatInfoHtml=Yii::$app->appHelperFunctions->getBoatInfoHtml($this->boat_id);
        $boatInfoText=Yii::$app->appHelperFunctions->getBoatInfoText($this->boat_id);
        if($this->port_id==Yii::$app->params['emp_id']){
          $boatFuelConsumptionChartHtml='';
          $boatFuelConsumptionChartText='';
        }else{
          $boatFuelConsumptionChartHtml=Yii::$app->appHelperFunctions->getBoatFuelConsumptionChartHtml($this->boat_id);
          $boatFuelConsumptionChartText=Yii::$app->appHelperFunctions->getBoatFuelConsumptionChartText($this->boat_id);
        }

        $hasCaptain='';
        $hasEquipment='';
        $hasBBQ='';
        $hasIce='';
        $hasKidsLifeJackets='';
        $hasEarlyDeparture='';
        $hasLateArrival='';
        $hasOvernightCamping='';
        $hasWakeBoarding='';
        $hasWakeSurfing='';
        if($this->captain==1){
          if($this->captain_type!=null){
            $captain_type=$this->captain_type;
          }else{
            $captain_type=Yii::$app->bookingHelperFunctions->getCaptainStatus($this->member,$this);
          }
          $hasCaptain='<strong>Captain:</strong> '.$captain_type;
        }
        if($this->sport_eqp_id>0){
          $hasEquipment='<strong>Water Sports Equipment:</strong> '.$this->waterSportsEquipment->title;
        }
        if($this->bbq==1){
          $hasBBQ='<strong>BBQ Set:</strong> '.Yii::$app->appHelperFunctions->bbqPrice.' AED';
        }
        if($this->ice==1){
          $hasIce='<strong>Ice:</strong> Free';
        }
        if($this->kids_life_jacket==1){
          $hasKidsLifeJackets='<strong>Kids Life Jackets:</strong> '.($this->no_of_one_to_three_jackets>0 ? 'Age (1-3 years) = '.$this->no_of_one_to_three_jackets.' ' : '').($this->no_of_four_to_twelve_jackets>0 ? ' Age (4-12 years) = '.$this->no_of_four_to_twelve_jackets.'' : '');
        }
        if($this->early_departure==1){
          $hasEarlyDeparture='<strong>Early Departure:</strong> Free';
        }
        if($this->late_arrival==1){
          $hasLateArrival='<strong>Late Arrival:</strong> Free';
        }
        if($this->overnight_camping==1){
          $hasOvernightCamping='<strong>Overnight Camping</strong> Free';
        }
        if($this->wake_boarding==1){
          $hasWakeBoarding='<strong>Wake Boarding</strong> Free';
        }
        if($this->wake_surfing==1){
          $hasWakeSurfing='<strong>Wake Surfing</strong> Free';
        }

        $vals = [
          '{username}' => $this->member->fullname,
          '{email}' => $this->member->email,
          '{bookingCity}' => $this->city->name,
          '{bookingMarina}' => $this->marina->name,
          '{bookingBoat}' => $this->emailBoatName,
          '{bookingDate}' => Yii::$app->formatter->asDate($this->booking_date),
          '{bookingTime}' => $this->timeZone->name,
          '{tomorrowDate}' => Yii::$app->formatter->asDate($this->booking_date),

          '{bookingCaptain}' => $hasCaptain!='' ? $hasCaptain : '',
          '{bookingEquipment}' => $hasEquipment!='' ? $hasEquipment : '',
          '{bookingBBQ}' => $hasBBQ!='' ? $hasBBQ : '',
          '{bookingIce}' => $hasIce!='' ? $hasIce : '',
          '{bookingKidsJackets}' => $hasKidsLifeJackets!='' ? $hasKidsLifeJackets : '',
          '{bookingEarlyDeparture}' => $hasEarlyDeparture!='' ? $hasEarlyDeparture : '',
          '{bookingLateArrival}' => $hasLateArrival!='' ? $hasLateArrival : '',
          '{bookingOvernightCamping}' => $hasOvernightCamping!='' ? $hasOvernightCamping : '',
          '{bookingWakeBoarding}' => $hasWakeBoarding!='' ? $hasWakeBoarding : '',
          '{bookingWakeSurfing}' => $hasWakeSurfing!='' ? $hasWakeSurfing : '',

          '{bookingComments}' => ($this->booking_type!=null ? '<strong>Comments:</strong> '.Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type].' - '.$this->booking_comments : ''),
          '{boatInfoHtml}' => $boatInfoHtml,
          '{boatInfoText}' => $boatInfoText,
          '{boatFuelConsumptionChartHtml}' => $boatFuelConsumptionChartHtml,
          '{boatFuelConsumptionChartText}' => $boatFuelConsumptionChartText,
          '{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'/images/email_logo.png" alt="'.Yii::$app->params['siteName'].'">',
        ];
        $htmlBody=$template->searchReplace($template->template_html,$vals);
        $textBody=$template->searchReplace($template->template_text,$vals);

        $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setSubject('Booking Reminder');

        foreach($this->member->activeContractMembers as $contractMember){
          $canSend=true;
          //Check if user is not black listed
          $isRed=BlackListedEmail::find()->select(['email'])->where(['email'=>$contractMember->email,'trashed'=>0]);
          if($isRed->exists()){
            $canSend=false;
          }
          //Check if user is not in dropped list
          $isRed=DroppedEmail::find()->select(['email'])->where(['email'=>$contractMember->email,'trashed'=>0]);
          if($isRed->exists()){
            $canSend=false;
          }
          if($canSend==true){
            $message->setTo($contractMember->email);
            $message->send();
          }
        }
        $connection = \Yii::$app->db;
        $connection->createCommand("update ".self::tableName()." set reminder_sent=1 where id=:id",[':id'=>$this->id])->execute();

        $devices=UserDevice::find()->where(['user_id'=>$this->user_id])->asArray()->all();
        if($devices!=null){

          $title = "Booking Reminder";
          $message = "Please confirm your booking of tomorrow (".Yii::$app->formatter->asDate($this->booking_date).")";
          $actionUrl = Yii::$app->params['siteUrl']."index.php?r=mobile%2Fbooking%2Fnotification-response&id=".$this->id;
          $messageArr=[
            "title" => $title,
            "body" => $message,
            "click_url" => $actionUrl,
          ];
          $dataArr=[
            "title" => $title,
            "message" => $message,
            "action" => "url",
            "action_destination" => $actionUrl,
            "click_url" => $actionUrl,
          ];
          foreach($devices as $device){
            Yii::$app->helperFunctions->sendNotification($device['device_id'],$messageArr,$dataArr,$this->user_id,$this->id);
          }
        }
      }

      /**
      * Mark record as deleted and hides fron list.
      * @return boolean
      */
      public function softDelete()
      {
        if($this->bookingActivity!=null){
          return false;
        }
        $connection = \Yii::$app->db;
        $connection->createCommand("delete from ".WaitingListCaptain::tableName()." where booking_id='".$this->id."'")->execute();
        $connection->createCommand("delete from ".WaitingListBbq::tableName()." where booking_id='".$this->id."'")->execute();
        $connection->createCommand("delete from ".WaitingListSportsEquipment::tableName()." where booking_id='".$this->id."'")->execute();
        $connection->createCommand("delete from ".WaitingListWakeSurfing::tableName()." where booking_id='".$this->id."'")->execute();
        $connection->createCommand("delete from ".WaitingListWakeBoarding::tableName()." where booking_id='".$this->id."'")->execute();
        $connection->createCommand("delete from ".WaitingListLateArrival::tableName()." where booking_id='".$this->id."'")->execute();
        $connection->createCommand("delete from ".WaitingListEarlyDeparture::tableName()." where booking_id='".$this->id."'")->execute();
        $connection->createCommand("delete from ".WaitingListOvernightCamping::tableName()." where booking_id='".$this->id."'")->execute();

        if($this->is_bulk==0 && $this->booking_date<=date("Y-m-d")){
          $connection->createCommand("update ".self::tableName()." set status=:status,updated_at=:updated_at,updated_by=:updated_by where id=:id",[':status'=>Yii::$app->helperFunctions->sameDayCancelID,':updated_at'=>date("Y-m-d H:i:s"),':updated_by'=>Yii::$app->user->identity->id,':id'=>$this->id,])->execute();
        }else{
          $connection->createCommand("update ".self::tableName()." set trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",[':trashed'=>1,':trashed_at'=>date("Y-m-d H:i:s"),':trashed_by'=>Yii::$app->user->identity->id,':id'=>$this->id,])->execute();
        }

        if($this->booking_date==date("Y-m-d") && strtotime(date("H:i:s"))>=strtotime($this->timeZone->start_time)){
          //Today and session is already started, no assignment
        }else{
            Yii::$app->waitingListHelper->checkAndAssignCaptain($this->city_id,$this->port_id,$this->booking_date,$this->booking_time_slot);
            Yii::$app->waitingListHelper->checkAndAssignSportsEquipment($this->city_id,$this->port_id,$this->booking_date,$this->booking_time_slot);
            Yii::$app->waitingListHelper->checkAndAssignBBQSet($this->city_id,$this->port_id,$this->booking_date,$this->booking_time_slot);
            Yii::$app->waitingListHelper->checkAndAssignWakeBoarding($this->city_id,$this->port_id,$this->booking_date,$this->booking_time_slot);
            Yii::$app->waitingListHelper->checkAndAssignWakeSurfing($this->city_id,$this->port_id,$this->booking_date,$this->booking_time_slot);
        }

          //Delete Booking User Alert
          $templateId=Yii::$app->appHelperFunctions->getSetting('e_cancelbooking_u');
          $template=EmailTemplate::findOne($templateId);
          if($template!=null){
            $vals = [
              '{username}' => $this->member->fullname,
              '{email}' => $this->member->email,
              '{bookingCity}' => $this->city->name,
              '{bookingMarina}' => $this->marina->name,
              '{bookingBoat}' => $this->emailBoatName,
              '{bookingDate}' => Yii::$app->formatter->asDate($this->booking_date),
              '{bookingTime}' => $this->timeZone->name,
              '{bookingComments}' => ($this->booking_type!=null ? '<strong>Comments:</strong> '.Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type].' - '.$this->booking_comments : ''),
              '{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'/images/email_logo.png" alt="'.Yii::$app->params['siteName'].'">',
            ];
            $htmlBody=$template->searchReplace($template->template_html,$vals);
            $textBody=$template->searchReplace($template->template_text,$vals);
            $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
            ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
            ->setSubject('Booking Deleted Alert');

            foreach($this->member->activeContractMembers as $contractMember){
              $canSend=true;
              //Check if user is not black listed
              $isRed=BlackListedEmail::find()->select(['email'])->where(['email'=>$contractMember->email,'trashed'=>0]);
              if($isRed->exists()){
                $canSend=false;
              }
              //Check if user is not in dropped list
              $isRed=DroppedEmail::find()->select(['email'])->where(['email'=>$contractMember->email,'trashed'=>0]);
              if($isRed->exists()){
                $canSend=false;
              }
              if($canSend==true){
                $message->setTo($contractMember->email);
                $message->send();
              }
            }
          }

          //Delete Booking Admin Alert
          $templateId=Yii::$app->appHelperFunctions->getSetting('e_cancelbooking_a');
          $template=EmailTemplate::findOne($templateId);
          if($template!=null){
            $vals = [
              '{username}' => $this->member->fullname,
              '{email}' => $this->member->email,
              '{bookingCity}' => $this->city->name,
              '{bookingMarina}' => $this->marina->name,
              '{bookingBoat}' => $this->emailBoatName,
              '{bookingDate}' => Yii::$app->formatter->asDate($this->booking_date),
              '{bookingTime}' => $this->timeZone->name,
              '{bookingComments}' => ($this->booking_type!=null ? '<strong>Comments:</strong> '.Yii::$app->helperFunctions->adminBookingTypes[$this->booking_type].': '.$this->booking_comments : ''),
              '{logo}' => '<img src="'.Yii::$app->params['siteUrl'].'/images/email_logo.png" alt="'.Yii::$app->params['siteName'].'">',
            ];
            $htmlBody=$template->searchReplace($template->template_html,$vals);
            $textBody=$template->searchReplace($template->template_text,$vals);
            Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
            ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
            ->setTo(Yii::$app->appHelperFunctions->getSetting('adminEmail'))
            ->setSubject('Booking Deleted Alert')
            ->send();
          }

          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Booking ('.$this->booking_date.' / '.$this->boat->name.' from '.$this->marina->name.') trashed successfully');
          Yii::$app->bookingHelperFunctions->checkBulkOrBoatRequired($this->city_id,$this->port_id,$this->boat_id,$this->booking_date,$this->booking_time_slot,$this->ignore_bulk_booking_check);
          return true;
        }
      }
