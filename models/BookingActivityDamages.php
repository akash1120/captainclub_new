<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%booking_activity_damages}}".
*
* @property integer $id
* @property integer $booking_id
* @property integer $item_id
* @property string $item_condition
* @property string $item_comments
*/
class BookingActivityDamages extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_activity_damages}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['booking_id','item_id'], 'required'],
      [['booking_id','item_id'], 'integer'],
      [['item_condition','item_comments'], 'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'booking_id' => Yii::t('app', 'Booking'),
      'item_id' => Yii::t('app', 'Damage Item'),
      'item_condition' => Yii::t('app', 'Departure'),
      'item_comments' => Yii::t('app', 'Arrival'),
    ];
  }
}
