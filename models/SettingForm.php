<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* SettingForm is the model behind the setting form.
*/
class SettingForm extends Model
{
  public $adminEmail,$e_newuser_u,$e_renewuser_u,$e_freezeuser_u,$e_freezeuser_a,$e_newbooking_u,$e_newbooking_a,$e_cancelbooking_u,$e_cancelbooking_a,$e_tomorrowreminder_u,$e_alert_a,$e_alert_days,$freeze_finish_alert_days,$e_freezeopenuser_u,$e_freezeopenuser_a,$requestEmail,$auto_freeze_response;
  public $metEmail,$notmetEmail,$requestCancelledEmail,$ndtrainingmet,$e_expiry_u,$e_expiry_a;
  public $e_newrequiredbooking_a,$e_boatswaped_u,$e_requestboatalloc_u,$e_bulkbookboatreplaced_u;
  public $e_useremailchanged_u;

  public $opLateArrival,$opNoShow,$opStuck,$opTechnical,$opPetrolConsumtion;

  public $paytab_servicefees_u,$paytab_servicefees_pt,$paytab_addiotional_pt;
  public $feedback_staff;
  public $expiry_alert_days,$passenger_info_id,$bank_details;

  public $sn_captain,$sn_donut,$sn_bbq,$sn_ice,$sn_kids_life_jacket,$sn_early_departure,$sn_late_arrival_wo_nd,$sn_late_arrival_w_nd,$sn_overnight_camp,$sn_wakeboarding,$sn_wakesurfing,$sn_bvlgari_access;

  public $behaviour_report_counter;


  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['adminEmail', 'e_newbooking_u', 'e_newbooking_a', 'e_cancelbooking_u', 'e_cancelbooking_a', 'e_tomorrowreminder_u', 'e_alert_a', 'e_alert_days', 'freeze_finish_alert_days', 'paytab_servicefees_u','paytab_servicefees_pt'], 'required'],
      [[
        'e_newuser_u', 'e_renewuser_u', 'auto_freeze_response', 'e_freezeuser_u', 'e_freezeuser_a',
        'e_newbooking_u', 'e_newbooking_a', 'e_cancelbooking_u', 'e_cancelbooking_a', 'e_tomorrowreminder_u',
        'e_alert_a', 'freeze_finish_alert_days', 'e_freezeopenuser_u', 'e_freezeopenuser_a', 'metEmail',
        'notmetEmail','requestCancelledEmail', 'e_expiry_u', 'e_expiry_a', 'opLateArrival', 'opNoShow',
        'opStuck' ,'opTechnical', 'opPetrolConsumtion', 'e_newrequiredbooking_a','e_boatswaped_u',
        'e_requestboatalloc_u','e_bulkbookboatreplaced_u','ndtrainingmet','e_useremailchanged_u','expiry_alert_days','passenger_info_id',
        'behaviour_report_counter',
      ], 'integer'],
      [['adminEmail','requestEmail'], 'email'],
      [[
        'bank_details','sn_captain','sn_donut','sn_bbq','sn_ice','sn_kids_life_jacket','sn_early_departure'
        ,'sn_late_arrival_wo_nd','sn_late_arrival_w_nd','sn_overnight_camp','sn_bvlgari_access','sn_wakeboarding','sn_wakesurfing'
      ], 'string'],
      [['paytab_servicefees_u','paytab_servicefees_pt','paytab_addiotional_pt'],'number'],
      [['feedback_staff'],'each','rule'=>['integer']],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'adminEmail' => 'Admin Email',
      'requestEmail' => 'Request Email',
      'e_newuser_u' => 'New Account Member Alert',
      'e_renewuser_u' => 'Renew Member Alert',
      'auto_freeze_response' => 'Member Account Freeze Alert',
      'e_freezeuser_a' => 'Admin Account Freeze Alert',
      'e_newbooking_u' => 'New Booking Member Alert',
      'e_newbooking_a' => 'New Booking Admin Alert',
      'e_cancelbooking_u' => 'Cancel Booking Member Alert',
      'e_cancelbooking_a' => 'Cancel Booking Admin Alert',
      'e_tomorrowreminder_u' => 'Tomorrow\'s Booking Member Reminder',
      'e_alert_a' => 'Admin Daily Alert for Coming Bookings',
      'e_alert_days' => 'How many days booking to alert',
      'freeze_finish_alert_days' => 'How many days before the alert about freeze finishes',
      'e_freezeopenuser_u' => 'Freeze Finishing Member Alert',
      'e_freezeopenuser_a' => 'Freeze Finishing Admin Alert',
      'metEmail' => 'Met Email',
      'ndtrainingmet' => 'Night Drive Training Met Email',
      'notmetEmail' => 'Not Met Email',
      'requestCancelledEmail' => 'Cancelled Request Email',
      'e_expiry_u' => 'Member Expiry Alert',
      'e_expiry_a' => 'Admin Expiry Alert',
      'opLateArrival' => 'Late Arrival Alert',
      'opNoShow' => 'No Show Alert',
      'opStuck' => 'Stuck Alert',
      'opTechnical' => 'Technical Alert',
      'opPetrolConsumtion' => 'Petrol Consumption',
      'e_newrequiredbooking_a' => 'Boat Required - Boat Allocated Email',
      'e_boatswaped_u' => 'Boat Swapped Member Email',
      'e_requestboatalloc_u' => 'Request - Boat Allocated Email',
      'e_bulkbookboatreplaced_u' => 'Bulk Booking - Boat Replaced Email',
      'paytab_servicefees_u' => 'Service Fees From User(%)',
      'paytab_servicefees_pt' => 'Service Fees To Pay Tabs(%)',
      'paytab_addiotional_pt' => 'Paytabs Additional +',
      'feedback_staff' => 'Feedback Reminder',
      'bank_details' => 'Bank Details',
      'e_useremailchanged_u' => 'Member Email Changed Template',
      'expiry_alert_days' => 'Days before expiry alert should go',
      'passenger_info_id' => 'Passengers Info Field',

      'sn_captain' => 'Captain',
      'sn_donut' => 'Donuts',
      'sn_bbq' => 'BBQ',
      'sn_ice' => 'Ice',
      'sn_kids_life_jacket' => 'Kids Life Jackets',
      'sn_early_departure' => 'Early Departure',
      'sn_late_arrival_wo_nd' => 'Late Arrival (No Night Drive)',
      'sn_late_arrival_w_nd' => 'Late Arrival (Night Drive)',
      'sn_overnight_camp' => 'Overnight Camping',
      'sn_wakesurfing' => 'Wake Surfing',
      'sn_wakeboarding' => 'Wake Boarding',
      'sn_bvlgari_access' => 'Bvlgari Access',

      'behaviour_report_counter' => 'Report Old Count',
    ];
  }

  /**
  * save the settings data.
  */
  public function save()
  {
    if ($this->validate()) {
      $post=Yii::$app->request->post();
      foreach($post['SettingForm'] as $key=>$val){
        $setting=Setting::find()->where("config_name='$key'")->one();
        if($setting==null){
          $setting=new Setting;
          $setting->config_name=$key;
        }
        if($key=='feedback_staff'){
          $val=implode(",",$this->feedback_staff);
        }
        $setting->config_value=$val;
        $setting->save();
      }
      return true;
    } else {
      return false;
    }
  }
}
