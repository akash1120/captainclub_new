<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SupportTicket;

/**
 * SupportTicketSearch represents the model behind the search form about `app\models\SupportTicket`.
 */
class SupportTicketSearch extends SupportTicket
{
  public $pageSize;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['parent_id','priority_id','status','created_by','pageSize'], 'integer'],
      [['ref_no','username','title','due_date','descp'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $this->load($params);
    $query = SupportTicket::find()
    ->leftJoin("user",User::tableName().".id=".SupportTicket::tableName().".user_id");

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);
    $dataProvider->setSort([
      'attributes' => [
        'id',
        'ref_no',
        'username' => [
          'asc' => [User::tableName().'.username' => SORT_ASC],
          'desc' => [User::tableName().'.username' => SORT_DESC],
        ],
        'created_by' => [
          'asc' => [User::tableName().'.username' => SORT_ASC],
          'desc' => [User::tableName().'.username' => SORT_DESC],
        ],
        'title',
        'priority_id',
        'due_date',
        'status',
      ],
      'defaultOrder' => [
        'id' => SORT_DESC
      ]
    ]);

    $query->andFilterWhere([
      'parent_id' => $this->parent_id,
      SupportTicket::tableName().'.created_by' => $this->created_by,
      'parent_id' => $this->parent_id,
      'priority_id' => $this->priority_id,
      'DATE(due_date)' => $this->due_date,
      SupportTicket::tableName().'.status' => $this->status,
      SupportTicket::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['like', 'ref_no', $this->ref_no])
    ->andFilterWhere(['like', 'title', $this->title])
    ->andFilterWhere(['like', 'username', $this->username]);

    return $dataProvider;
  }
}
