<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;

/**
* This is the model class for table "{{%booking_update_history}}".
*
* @property integer $id
* @property integer $booking_id
* @property integer $old_booking_type
* @property string $old_booking_comments
* @property integer $new_booking_type
* @property string $new_booking_comments
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class BookingUpdateHistory extends ActiveRecord
{
	public $actions;

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%booking_update_history}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
      [['booking_id','new_booking_type','new_booking_comments'], 'required'],
      [['booking_id', 'old_booking_type', 'new_booking_type'], 'integer'],
      [['old_booking_comments', 'new_booking_comments'], 'string'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
      'booking_id' => Yii::t('app', 'Booking'),
      'old_booking_type' => Yii::t('app', 'Old Type'),
      'old_booking_comments' => Yii::t('app', 'Old Comments'),
      'new_booking_type' => Yii::t('app', 'Type'),
      'booking_comments' => Yii::t('app', 'Comments'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
      'LoggableBehavior' => [
				'class' => ActivityLogBehaviorUser::className(),
			],
		];
	}

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getBooking()
  {
    return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($insert){
      $connection = \Yii::$app->db;
      $connection->createCommand()
        ->update(
          Booking::tableName(),
          [
            'booking_type'=>$this->new_booking_type,
            'booking_comments'=>$this->new_booking_comments,
            'updated_at'=>date("Y-m-d H:i:s"),
            'updated_by'=>Yii::$app->user->identity->id,
          ],
          ['id'=>$this->booking_id]
        )
        ->execute();
    }
    parent::afterSave($insert, $changedAttributes);
  }
}
