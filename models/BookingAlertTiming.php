<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%booking_alert_timing}}".
*
* @property int $id
* @property int $alert_id
* @property int $time_slot_id
*/
class BookingAlertTiming extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%booking_alert_timing}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['alert_id', 'time_slot_id'], 'required'],
      [['alert_id', 'time_slot_id'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'alert_id' => Yii::t('app', 'Alert ID'),
      'time_slot_id' => Yii::t('app', 'Time Slot ID'),
    ];
  }

  /**
  * Get boat row
  * @return \yii\db\ActiveQuery
  */
  public function getAlert()
  {
    return $this->hasOne(BookingAlert::className(), ['id' => 'alert_id']);
  }

  /**
  * Get time slot row
  * @return \yii\db\ActiveQuery
  */
  public function getTimeSlot()
  {
    return $this->hasOne(TimeSlot::className(), ['id' => 'time_slot_id']);
  }
}
