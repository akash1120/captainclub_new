<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
* This is the model class for table "{{%user_note}}".
*
* @property integer $id
* @property integer $parent
* @property integer $user_id
* @property string $comments
* @property string $remarks
* @property integer color
* @property integer reminder_type
* @property string reminder
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class UserNote extends ActiveRecord
{
	public $actions;
	public $staffIdz;

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%user_note}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['user_id','comments'], 'required'],
			[['reminder', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
			[['parent', 'color', 'reminder_type', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
			[['parent'], 'default', 'value'=>0],
			[['comments','remarks'], 'string'],
			['staffIdz','each','rule'=>['integer']]
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'user_id' => Yii::t('app', 'User'),
			'comments' => Yii::t('app', 'Comments'),
			'remarks' => Yii::t('app', 'Remarks'),
			'color' => Yii::t('app', 'Color'),
			'reminder_type' => Yii::t('app', 'Type'),
			'reminder' => Yii::t('app', 'Reminder Date/Time'),
			'staffIdz' => Yii::t('app', 'Staff Member(s)'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Date & Time'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

	/**
	* @inheritdoc
	*/
	public function behaviors()
	{
		return [
			'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
				],
				'value' => function($event) {
					return date("Y-m-d H:i:s");
				},
			],
			'blameable' => [
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			],
		];
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getCreatedBy()
	{
		return $this->hasOne(User::className(), ['id' => 'created_by']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getForUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getRequest()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		if($this->reminder!=null){
			$childRow=UserNoteUser::find()->where(['note_id'=>$this->id,'staff_id'=>Yii::$app->user->identity->id])->one();
			if($childRow==null){
				$childRow=new UserNoteUser;
				$childRow->note_id=$this->id;
				$childRow->staff_id=Yii::$app->user->identity->id;
				$childRow->save();
			}
			if($this->staffIdz!=null){
				foreach($this->staffIdz as $key=>$val){
					$childRow=UserNoteUser::find()->where(['note_id'=>$this->id,'staff_id'=>$val])->one();
					if($childRow==null){
						$childRow=new UserNoteUser;
						$childRow->note_id=$this->id;
						$childRow->staff_id=$val;
						$childRow->save();
					}
				}
			}
		}
		if($this->is_feedback==1){
			$reminderStaff=Yii::$app->appHelperFunctions->getSetting('feedback_staff');
			if($reminderStaff!=null){
				$reminderStaff=explode(",",$reminderStaff);
			}
			foreach($reminderStaff as $key=>$val){
				$childRow=UserNoteUser::find()->where(['note_id'=>$this->id,'staff_id'=>$val])->one();
				if($childRow==null){
					$childRow=new UserNoteUser;
					$childRow->note_id=$this->id;
					$childRow->staff_id=$val;
					$childRow->save();
				}
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getStaffMembers()
	{
		return $this->hasMany(UserNoteUser::className(), ['note_id' => 'id']);
	}
}
