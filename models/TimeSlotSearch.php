<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TimeSlot;

/**
* TimeSlotSearch represents the model behind the search form of `app\models\TimeSlot`.
*/
class TimeSlotSearch extends TimeSlot
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','dashboard_rank','status','created_by','updated_by','trashed_by','pageSize'],'integer'],
      [['name','timing','created_at','updated_at','trashed','trashed_at'],'safe'],
      [['name','timing'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = TimeSlot::find()
    ->select([
      'id',
      'name',
      'timing',
      'dashboard_rank',
      'status',
    ])
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->params['pageSizeArray'])[0],
      ],
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'dashboard_rank' => $this->dashboard_rank,
      'status' => $this->status,
      'trashed' => 0,
    ]);

    $query->andFilterWhere(['like','name',$this->name])
    ->andFilterWhere(['like','timing',$this->timing]);

    return $dataProvider;
  }
}
