<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%boat_available_days}}".
*
* @property integer $boat_id
* @property integer $available_day
*/
class BoatAvailableDays extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%boat_available_days}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['boat_id','available_day'], 'required'],
      [['boat_id','available_day'], 'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'boat_id' => Yii::t('app', 'Boat'),
      'available_day' => Yii::t('app', 'Day'),
    ];
  }

  public static function primaryKey()
  {
    return ['boat_id','available_day'];
  }
}
