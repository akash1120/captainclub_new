<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* DeductSecurityDeposit is the model behind the member deduct security deposit form.
*/
class DeductSecurityDeposit extends Model
{
  public $user_id,$amount,$deposit_type,$descp;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['user_id','amount','deposit_type','descp'], 'required'],
      ['user_id','integer'],
      ['amount','number'],
      ['amount','validateAmount'],
      [['descp','deposit_type'], 'string'],
    ];
  }

  /**
  * Validates the amount.
  * This method serves as the inline validation for amount.
  */
  public function validateAmount($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $user=User::findOne($this->user_id);
      $currentDeposit=$user->profileInfo->security_deposit;
      $willBe=$currentDeposit-$this->amount;
      if($willBe<0){
        $this->addError('amount','Please provide valid amount, It can not be more then '.$currentDeposit.'.');
        return false;
      }
    }
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'amount' => 'Amount',
      'deposit_type' => 'Type',
      'descp' => 'Comments',
    ];
  }

  /**
  * Save amount to user account
  */
  public function save()
  {
    if ($this->validate()) {
      //Creating Cr Transaction
      $drtransaction=new UserSecurityDeposit;
      $drtransaction->user_id=$this->user_id;
      $drtransaction->trans_type='dr';
      $drtransaction->descp=$this->descp;
      $drtransaction->amount=$this->amount;
      $drtransaction->deposit_type=$this->deposit_type;
      $drtransaction->save();
      return true;
    }
    return false;
  }
}
