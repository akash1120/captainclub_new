<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* SecurityDepositForm
*/
class SecurityDepositForm extends Model
{
  public $user_id,$amount,$deposit_type;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // comments are required
      [['user_id','amount','deposit_type'], 'required'],
      [['user_id'], 'integer'],
      [['amount'], 'number'],
      [['deposit_type'], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'amount' => 'Amount',
      'deposit_type' => 'Type',
    ];
  }

  /**
  * Sends an email to the specified email address using the information collected by this model.
  * @param  string  $email the target email address
  * @return boolean whether the model passes validation
  */
  public function send()
  {
    if ($this->validate()) {
      $oldAmount=$this->user->profileInfo->security_deposit;
      $oldType=$this->user->profileInfo->deposit_type;

      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".UserProfileInfo::tableName()." set security_deposit=:security_deposit,deposit_type=:deposit_type where user_id=:id",
        [':security_deposit'=>$this->amount,':deposit_type'=>$this->deposit_type,':id'=>$this->user_id]
        )->execute();

        $history=new UserSecurityDepositChangeHistory;
        $history->user_id=$this->user_id;
        $history->old_amount=$oldAmount;
        $history->new_amount=$this->amount;

        $history->old_type=$oldType;
        $history->new_type=$this->deposit_type;
        $history->save();
        return true;
      }
      return false;
    }

    public function getUser()
    {
      return User::findOne($this->user_id);
    }
  }
