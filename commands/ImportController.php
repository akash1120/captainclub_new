<?php
/**
* @link http://www.yiiframework.com/
* @copyright Copyright (c) 2008 Yii Software LLC
* @license http://www.yiiframework.com/license/
*/

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

use app\models\OperationRequests;
use app\models\RequestBreakDown;
use app\models\RequestService;
use app\models\RequestInspection;
use app\models\OperationsUser;

use app\models\Booking;
use app\models\import\BookingActivity;
use app\models\OperationBooking;
use app\models\OperationBookingInOut;
use app\models\BookingInOutBookingExp;
use app\models\BookingInOutTripExp;
use app\models\BookingInOutMemberExp;
use app\models\BookingNoShow;
use app\models\BookingCancelled;
use app\models\BookingInOutChangedBoat;
use app\models\BookingInOutDamages;
use app\models\BookingActivityDamages;
use app\models\BookingInOutExtras;
use app\models\BookingActivityAddons;
use app\models\ClaimOrderItem;
use app\models\BookingOperationComments;
use app\models\ClaimOrderComments;
use app\models\ClaimOrder;

use app\models\import\User;
use app\models\import\UserProfileInfo;

/**
* This command is for importing
*/
class ImportController extends Controller
{
  private $limit = 1;
  /**
  * This command Updates Cancel/Hold Status
  */
  public function actionBookingActivity()
  {
    $subQueryAddedActivity=BookingActivity::find()->select(['booking_id']);
    $subQueryOperations=OperationBooking::find()->select(['booking_id'])->where(['not in','booking_id',$subQueryAddedActivity]);
    $results=Booking::find()
    ->where(['id'=>$subQueryOperations])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit($this->limit)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $operationBooking=OperationBooking::find()->where(['booking_id'=>$result['id']])->asArray()->one();
        if($operationBooking!=null){
          //In Out
          $operationBookingInOut=OperationBookingInOut::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
          if($operationBookingInOut!=null){

            $bookingActivity=BookingActivity::find()->where(['booking_id'=>$result['id']])->one();
            if($bookingActivity==null){
              $bookingActivity=new BookingActivity;
              $bookingActivity->booking_id=$result['id'];
            }
            $bookingActivity->dep_time=$operationBookingInOut['dep_time'];
            $bookingActivity->arr_time=$operationBookingInOut['arr_time'];
            $bookingActivity->dep_engine_hours=$operationBookingInOut['dep_engine_hours'];
            $bookingActivity->arr_engine_hours=$operationBookingInOut['arr_engine_hours'];
            $bookingActivity->dep_ppl=$operationBookingInOut['dep_ppl'];
            $bookingActivity->arr_ppl=$operationBookingInOut['arr_ppl'];
            $bookingActivity->dep_fuel_level=$operationBookingInOut['dep_fuel_level'];
            $bookingActivity->arr_fuel_level=$operationBookingInOut['arr_fuel_level'];
            $bookingActivity->fuel_chargeable=$operationBookingInOut['fuel_chargeable'];
            $bookingActivity->no_charge_comment=$operationBookingInOut['no_charge_comment'];
            $bookingActivity->fuel_cost=$operationBookingInOut['fuel_cost']!='' ? $operationBookingInOut['fuel_cost'] : 0;
            $bookingActivity->fuel_cost_paid=$operationBookingInOut['fuel_cost_paid'];
            $bookingActivity->charge_bbq=$operationBookingInOut['bbq'];
            $bookingActivity->charge_captain=$operationBookingInOut['captain'];
            $bookingActivity->charge_wake_boarding=$operationBookingInOut['charge_wake_boarding'];
            $bookingActivity->charge_wake_surfing=$operationBookingInOut['charge_wake_surfing'];
            $bookingActivity->bill_image=$operationBookingInOut['bill_image'];
            $bookingActivity->damage_comments=$operationBookingInOut['damage_comments'];
            $bookingActivity->hide_other=$operationBookingInOut['hide_other'];


            $bookingStatus=1;
            //Check Stay In
            if($operationBooking['stayin']==1)$bookingStatus=2;

            //Check No show row
            $noShowStatus=BookingNoShow::find()->where(['booking_id'=>$operationBooking['id']]);
            if($noShowStatus->exists())$bookingStatus=3;

            //Check TCC Cancelled
            $tccCancelledStatus=BookingCancelled::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            //Weather
            if($tccCancelledStatus!=null && $tccCancelledStatus['remarks']==1)$bookingStatus=4;
            //Boat Not Available
            if($tccCancelledStatus!=null && $tccCancelledStatus['remarks']==2)$bookingStatus=5;

            //Check Admin
            if($operationBooking['is_admin']==1)$bookingStatus=6;

            //Check Same Day Cancel
            if($operationBooking['same_day_cancel']==1)$bookingStatus=7;

            //Check Weather Warning Cancel
            if($operationBooking['weather_warning_cancel']==1)$bookingStatus=8;

            //Check Training
            if($operationBooking['is_training']==1)$bookingStatus=9;

            //Check Emergency
            if($operationBooking['is_emergency']==1)$bookingStatus=10;

            //Check Out Of Service
            if($operationBooking['is_emergency']==1)$bookingStatus=11;
            //////////////////Update Booking Status//////////////////
            $connection->createCommand(
              "update ".Booking::tableName()." set status=:status where id=:id",
              [
                ':status'=>$bookingStatus,
                ':id'=>$result['id'],
              ]
            )->execute();

            $bookingExperienceTblRow=BookingInOutBookingExp::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            //Smooth
            $bookingExp=1;
            if($bookingExperienceTblRow!=null && $bookingExperienceTblRow['exp_id']==1)$bookingExp=1;
            //Smooth
            if($bookingExperienceTblRow!=null && $bookingExperienceTblRow['exp_id']==2)$bookingExp=2;
            //Smooth
            if($bookingExperienceTblRow!=null && $bookingExperienceTblRow['exp_id']==3)$bookingExp=3;
            //////////////////Update Booking Experience//////////////////
            $connection->createCommand(
              "update ".Booking::tableName()." set booking_exp=:booking_exp where id=:id",
              [
                ':booking_exp'=>$bookingExp,
                ':id'=>$result['id'],
              ]
            )->execute();
            ////////////////////////////////////

            //Boat Provided
            $boatProvidedId=$operationBooking['boat_id'];
            $boatProvidedComments='';
            $boatProvidedRow=BookingInOutChangedBoat::find()->select(['boat_id','remarks'])->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            if($boatProvidedRow!=null){
              $boatProvidedId=$boatProvidedRow['boat_id'];
              $boatProvidedComments=$boatProvidedRow['remarks'];
            }
            $bookingActivity->boat_provided=$boatProvidedId;

            //Trip Experience
            $tripExperienceStatus=1;
            $tripExperienceComments='';
            $tripExperienceTblRow=BookingInOutTripExp::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            if($tripExperienceTblRow!=null){
              $tripExperienceStatus=$tripExperienceTblRow['exp_id'];
              $tripExperienceComments=$tripExperienceTblRow['remarks'];
              $tripExperienceComments.=($tripExperienceComments!='' && $tripExperienceComments!=null && $tripExperienceComments!=NULL) ? "\n\r" : '';
              $tripExperienceComments.=$tripExperienceTblRow['trip_comment'];
            }
            //////////////////Update Trip Status//////////////////
            $bookingActivity->trip_exp=$tripExperienceStatus;
            $bookingActivity->trip_exp_comments=$tripExperienceComments;
            ////////////////////////////////////

            //Member Experience
            $memberExperienceStatus=0;
            $member_exp_late_hr=0;
            $member_exp_late_min=0;
            $memberExperienceComments='';
            $memberExperienceTblRow=BookingInOutMemberExp::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            if($memberExperienceTblRow!=null){
              $memberExperienceStatus=$memberExperienceTblRow['exp_id'];
              $member_exp_late_hr=$memberExperienceTblRow['late_hr'];
              $member_exp_late_min=$memberExperienceTblRow['late_min'];
              $memberExperienceComments=$memberExperienceTblRow['late_time'];
              $memberExperienceComments.=($memberExperienceComments!='' && $memberExperienceComments!=null && $memberExperienceComments!=NULL) ? "\n\r" : '';
              $memberExperienceComments.=$memberExperienceTblRow['late_comments'];
            }
            //////////////////Update Member Status//////////////////
            $bookingActivity->member_exp=$memberExperienceStatus;
            $bookingActivity->member_exp_late_hr=$member_exp_late_hr;
            $bookingActivity->member_exp_late_min=$member_exp_late_min;
            $bookingActivity->member_exp_comments=$memberExperienceComments;
            ////////////////////////////////////

            if($operationBookingInOut['dep_crew_sign']!=''){
              $depCrewSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['dep_crew_sign']);
              $bookingActivity->dep_crew_sign=$depCrewSignFile;
            }

            if($operationBookingInOut['dep_mem_sign']!=''){
              $depMemSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['dep_mem_sign']);
              $bookingActivity->dep_mem_sign=$depMemSignFile;
            }

            if($operationBookingInOut['arr_crew_sign']!=''){
              $arrCrewSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['arr_crew_sign']);
              $bookingActivity->arr_crew_sign=$arrCrewSignFile;
            }

            if($operationBookingInOut['arr_mem_sign']!=''){
              $arrMemSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['arr_mem_sign']);
              $bookingActivity->arr_mem_sign=$arrMemSignFile;
            }

            $bookingActivity->overall_remarks=$boatProvidedComments;

            //Extra
            $extraAddons=BookingInOutExtras::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->all();
            if($extraAddons!=null){
              foreach($extraAddons as $extraAddon){
                $baAddon=BookingActivityAddons::find()->where(['booking_id'=>$result['id'],'addon_id'=>$extraAddon['extra_id']])->one();
                if($baAddon==null){
                  $baAddon=new BookingActivityAddons;
                  $baAddon->booking_id=$result['id'];
                  $baAddon->addon_id=$extraAddon['extra_id'];
                  $baAddon->save();
                }
              }
            }

            //Damages
            $damgesRows=BookingInOutDamages::find()->where(['booking_inout_id'=>$operationBookingInOut['id']])->asArray()->all();
            if($damgesRows!=null){
              foreach($damgesRows as $damgesRow){
                $badamage=BookingActivityDamages::find()->where(['booking_id'=>$result['id'],'item_id'=>$damgesRow['item_id']])->one();
                if($badamage==null){
                  $badamage=new BookingActivityDamages;
                  $badamage->booking_id=$result['id'];
                  $badamage->item_id=$damgesRow['item_id'];
                }
                $badamage->item_condition=$damgesRow['item_condition'];
                $badamage->item_comments=$damgesRow['item_comments'];
                $badamage->save();
              }
            }


            $claimedById=0;
            $claimedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBooking['claimed_by']])->asArray()->one();
            if($claimedByUser!=null)$claimedById=$claimedByUser['booking_staff_id'];

            $approvedById=0;
            $approvedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBooking['approved_by']])->asArray()->one();
            if($approvedByUser!=null)$approvedById=$approvedByUser['booking_staff_id'];

            $bookingActivity->claimed=$operationBooking['claimed'];
            $bookingActivity->claimed_at=$operationBooking['claimed_at'];
            $bookingActivity->claimed_by=$claimedById;
            $bookingActivity->approved=$operationBooking['approved'];
            $bookingActivity->approved_at=$operationBooking['approved_at'];
            $bookingActivity->approved_by=$approvedById;

            if($bookingActivity->save()){

              //Update Staff Ids
              $createdById=0;
              $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBookingInOut['created_by']])->asArray()->one();
              if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

              $updatedById=0;
              $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBookingInOut['updated_by']])->asArray()->one();
              if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

              $connection->createCommand(
                "update ".BookingActivity::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                [
                  ':created_at'=>$operationBookingInOut['created_at'],
                  ':created_by'=>$createdById,
                  ':updated_at'=>$operationBookingInOut['updated_at'],
                  ':updated_by'=>$updatedById,
                  ':id'=>$bookingActivity['id']
                ]
              )->execute();
              //Generate Signature Images
            }else{
              echo 'In/Out ID = '.$operationBooking['id'].'<br />';
              print_r($bookingActivity->getErrors());
              die();
            }
          }
        }
      }
    }
    return 1;
  }
}
