<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\EmailTemplate;
use app\models\Booking;
use app\models\BookingActivity;
use app\models\BookingActivityEmailNotification;

/**
* This command Updates Cancel/Hold Status
*/
class OperationsController extends Controller
{
  private $hrToStart = 4;
  private $limit = 15;
  /**
  * This command sends alerts to members for late arrivals
  */
  public function actionLateArrivals()
  {
    $eaType='lateArrival';
    if(date("H")>$this->hrToStart){
      $results = Booking::find()
      ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
      ->where([
        'trashed'=>0,
        'status'=>1,
        'member_exp'=>1,
        'alert_sent_late_arrival'=>0,
        'booking_date'=>date("Y-m-d", strtotime("-1 day", strtotime(date("Y-m-d")))),
      ])
      ->limit($this->limit)->all();
      if($results!=null){
        //Late Arrival Alert
        $templateId=Yii::$app->appHelperFunctions->getSetting('opLateArrival');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          foreach($results as $result){
            $result->sendExperienceMail($eaType,'Late Arrival','alert_sent_late_arrival',$template,$result);
      		}
        }
      }
    }
    return 1;
  }

  /**
  * This command sends alerts to members for no show
  */
  public function actionNoShow()
  {
    $eaType='noShow';
    if(date("H")>$this->hrToStart){
      $results = Booking::find()
      ->where([
        'trashed'=>0,
        'status'=>3,
        'alert_sent_no_show'=>0,
        'booking_date'=>date("Y-m-d", strtotime("-1 day", strtotime(date("Y-m-d")))),
      ])
      ->limit($this->limit)->all();
      if($results!=null){
        //Late Arrival Alert
        $templateId=Yii::$app->appHelperFunctions->getSetting('opNoShow');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          $connection = \Yii::$app->db;
          foreach($results as $result){
            $result->sendExperienceMail($eaType,'No Show','alert_sent_no_show',$template,$result);
      		}
        }
      }
    }
    return 1;
  }

  /**
  * This command sends alerts to members for stuck
  */
  public function actionStuck()
  {
    $eaType='stuck';
    if(date("H")>$this->hrToStart){
      $results = Booking::find()
      ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
      ->where([
        'trashed'=>0,
        'status'=>1,
        'trip_exp'=>3,
        'alert_sent_stuck'=>0,
        'booking_date'=>date("Y-m-d", strtotime("-1 day", strtotime(date("Y-m-d")))),
      ])
      ->limit($this->limit)->all();
      if($results!=null){
        //Late Arrival Alert
        $templateId=Yii::$app->appHelperFunctions->getSetting('opStuck');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          $connection = \Yii::$app->db;
          foreach($results as $result){
            $result->sendExperienceMail($eaType,'Minor Incident','alert_sent_stuck',$template,$result);
      		}
        }
      }
    }
    return 1;
  }

  /**
  * This command sends alerts to members for Technical
  */
  public function actionTechnical()
  {
    $eaType='technical';
    if(date("H")>$this->hrToStart){
      $results = Booking::find()
      ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
      ->where([
        'trashed'=>0,
        'status'=>1,
        'trip_exp'=>2,
        'alert_sent_technical'=>0,
        'booking_date'=>date("Y-m-d", strtotime("-1 day", strtotime(date("Y-m-d")))),
      ])
      ->limit($this->limit)->all();
      if($results!=null){
        //Late Arrival Alert
        $templateId=Yii::$app->appHelperFunctions->getSetting('opTechnical');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          $connection = \Yii::$app->db;
          foreach($results as $result){
            $result->sendExperienceMail($eaType,'Technical Incident','alert_sent_technical',$template,$result);
      		}
        }
      }
    }
    return 1;
  }

  public function actionReport()
  {
    $templateId=Yii::$app->appHelperFunctions->getSetting('e_alert_a');
		$template=EmailTemplate::findOne($templateId);
		if($template!=null){
      list($start,$end)=explode("-",Yii::$app->appHelperFunctions->getSetting('e_alert_days'));
			$startDate=date("Y-m-d",mktime(0,0,0,date("m"),date("d")+$start,date("Y")));
			$endDate=date("Y-m-d",mktime(0,0,0,date("m"),date("d")+$end,date("Y")));
			$bookings=Booking::find()
      ->where([
        'and',
        ['>=','DATE(booking_date)',$startDate],
        ['<=','DATE(booking_date)',$endDate],
        ['trashed'=>0],
      ])
      ->all();
			if($bookings!=null){
				$listHtml  = '<table width="100%" border="1" cellspacing="0" cellpadding="0" style="border:1px solid #e1e1e1">';
				$listHtml .= '	<tr>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">User<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">City<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Marina<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Fleet<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Time<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Captain<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Equipment<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">BBQ Set<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Wake Boarding<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Wake Surfing<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Ice<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Kids Life Jackets<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Early Departure<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Late Arrival<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Overnight Camping<td>';
				$listHtml .= '		<td align="center" style="background-color:#999;color:#003;font-size:13px;font-family:tahoma;padding:5px;">Remarks<td>';
				$listHtml .= '	</tr>';
				foreach($bookings as $booking){
          $hasCaptain='';
          if($booking->captain==1){
            $hasCaptain=$booking->captainStatus;
          }
          $hasEquipment='';
          if($booking->sport_eqp_id>0){
            $hasEquipment=''.$booking->waterSportsEquipment->title.'';
          }
          $hasBBQ='';
          if($booking->bbq==1){
            $hasBBQ=''.Yii::$app->appHelperFunctions->bbqPrice.' AED';
          }
          $hasIce='';
          if($booking->ice==1){
            $hasIce='Free';
          }
          $hasKidsLifeJackets='';
          if($booking->kids_life_jacket==1){
            $hasKidsLifeJackets=($booking->no_of_one_to_three_jackets>0 ? 'Age (1-3) = '.$booking->no_of_one_to_three_jackets.'' : '').($booking->no_of_four_to_twelve_jackets>0 ? ', Age (4-12) = '.$booking->no_of_four_to_twelve_jackets.'' : '');
          }
          $hasEarlyDeparture='';
          if($booking->early_departure==1){
            $hasEarlyDeparture='Free';
          }
          $hasLateArrival='';
          if($booking->late_arrival==1){
            $hasLateArrival='Free';
          }
          $hasOvernightCamping='';
          if($booking->overnight_camping==1){
            $hasOvernightCamping='Free';
          }
          $hasWakeBoarding='';
          if($booking->wake_boarding==1){
            $hasWakeBoarding='Free';
          }
          $hasWakeSurfing='';
          if($booking->wake_surfing==1){
            $hasWakeSurfing='Free';
          }
					$listHtml .= '	<tr>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$booking->member->fullname.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$booking->city->name.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$booking->marina->name.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$booking->boat->name.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$booking->timeZone->name.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasCaptain.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasEquipment.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasBBQ.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasWakeBoarding.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasWakeSurfing.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasIce.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasKidsLifeJackets.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasEarlyDeparture.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasLateArrival.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.$hasOvernightCamping.'<td>';
					$listHtml .= '		<td align="center" style="background-color:#E8E8E8;font-size:12px;font-family:tahoma">'.($booking->booking_type!=null ? Yii::$app->params['adminBookingTypes'][$booking->booking_type].': '.$booking->booking_comments : '').'<td>';
					$listHtml .= '	</tr>';
				}
				$listHtml .= '	</table>';
				if(Yii::$app->appHelperFunctions->getSetting('e_alert_days')=="1-1"){
					$bookingDuration="tomorrow";
				}else{
					$bookingDuration = Yii::$app->formatter->asDate($startDate).' to '.Yii::$app->formatter->asDate($endDate);
				}
				$vals = [
					'{loginlink}' => Yii::$app->params['siteUrl'],
					'{bookingList}' => $listHtml,
					'{bookingDuration}' => $bookingDuration,
				];
				$htmlBody=$template->searchReplace($template->template_html,$vals);
				$textBody=$template->searchReplace($template->template_text,$vals);
				$message = Yii::$app->mailer->compose()
					->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
					->setTo(Yii::$app->appHelperFunctions->getSetting('adminEmail'))
					->setSubject('Booking for '.$bookingDuration.' - '.Yii::$app->params['siteName'])
					->setHtmlBody($htmlBody)
					->setTextBody($textBody)
					->send();
			}
		}
  }
}
