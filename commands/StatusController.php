<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\UserStatusReason;
use app\models\User;
use app\models\ContractMember;

/**
 * This command Updates Cancel/Hold Status
 */
class StatusController extends Controller
{
  private $limit = 15;
    /**
     * This command Updates Cancel/Hold Status
     */
    public function actionUpdateStatus()
    {
      //Check if a Cancel/Hold request is scheduled
      $results=UserStatusReason::find()
      ->where(['date'=>date("Y-m-d"),'is_updated'=>0])
      ->asArray()->limit($this->limit)->all();
      if($results!=null){
        $connection = \Yii::$app->db;
        foreach($results as $result){
          $user=User::find()->where(['id'=>$result['user_id']])->one();
          if($user!=null){
            $activeContract=$user->activeContract;
            if($activeContract!=null){
              $connection->createCommand("update ".ContractMember::tableName()." set status=:user_status where contract_id=:contract_id and user_id=:user_id",[':user_status'=>$result['status'],':contract_id'=>$activeContract->id,':user_id'=>$result['user_id']])->execute();
              $connection->createCommand("update ".User::tableName()." set status=:user_status where id=:user_id",[':user_status'=>$result['status'],':user_id'=>$result['user_id']])->execute();
            }
          }
          //Mark record as processed
          $connection->createCommand("update ".UserStatusReason::tableName()." set is_updated=1 where id=:id",[':id'=>$result['id']])->execute();
        }
      }
      return 1;
    }
}
