<?php
/**
* @link http://www.yiiframework.com/
* @copyright Copyright (c) 2008 Yii Software LLC
* @license http://www.yiiframework.com/license/
*/

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\User;
use app\models\UserLicense;
use app\models\Booking;
use app\models\Contract;
use app\models\EmailTemplate;
use app\models\UserFreezeRequest;

/**
* This command sends alerts to members
*/
class _MemberAlertController extends Controller
{
  private $hrToStart = 4;
  public $limit = 25;
  /**
  * This command sends alerts to members for package expiry
  */
  public function actionExpiryReminder()
  {
    $results = Contract::find()
    ->where([
      'expiry_alert'=>0,
      'end_date'=>date("Y-m-d",mktime(0,0,0,date("m"),(date("d")+Yii::$app->params['expiryAlert']),date("Y")))
    ])
    ->limit(25)->all();
    if($results!=null){
      foreach($results as $result){
        $result->sendExpiryAlert();
      }
    }
    return 1;
  }

  /**
  * This command sends alerts to users whoes freeze period is going to over
  */
  public function actionFreeze()
  {
    $results=UserFreezeRequest::find()
    ->where([
      'freeze_finish_alert'=>0,
      'trashed'=>0,
      'end_date'=>date("Y-m-d", strtotime("+".Yii::$app->appHelperFunctions->getSetting('freeze_finish_alert_days')." day", strtotime(date("Y-m-d")))),
    ])
    ->limit($this->limit)->all();
    if($results!=null){
      foreach($results as $result){
        $result->sendFreezeFinishAlert();
      }
    }
    return 0;
  }

  public function actionLicenseExpiry()
  {
    $oneMonthFwd=date("Y-m-d",strtotime("+1 month", strtotime(date("Y-m-d"))));
    $results=UserLicense::find()
    ->innerJoin(User::tableName(),User::tableName().".id=".UserLicense::tableName().".user_id")
    ->where([
      UserLicense::tableName().'.reminder_sent'=>0,
      UserLicense::tableName().'.license_expiry'=>$oneMonthFwd,
      User::tableName().'.status'=>1,
      User::tableName().'.trashed'=>0
    ])
    ->limit(5)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        if($result->sendExpiryEmail()){
          $connection->createCommand("update ".UserLicense::tableName()." set reminder_sent=:reminder_sent,reminder_sent_date=:reminder_sent_date where id=:id",[':reminder_sent'=>1,':reminder_sent_date'=>date("Y-m-d H:i:s"),':id'=>$result->id,])->execute();
        }
      }
    }
  }

  /**
  * This command sends alerts to members for coming bookings
  */
  public function actionBookingReminder()
  {
    if(date("H")>$this->hrToStart){
      $bookings=Booking::find()
      ->where([
        'reminder_sent'=>0,
        'trashed'=>0,
        'DATE(booking_date)'=>date("Y-m-d", strtotime("+1 day", strtotime(date("Y-m-d"))))
      ])
      ->limit($this->limit)->all();
      if($bookings!=null){
        $templateId=Yii::$app->appHelperFunctions->getSetting('e_tomorrowreminder_u');
        $template=EmailTemplate::findOne($templateId);
        if($template!=null){
          foreach($bookings as $booking){
            $booking->sendMemberReminder($template);
          }
        }
      }
    }
    return 1;
  }

    public function actionActivateContract()
    {
      $results=Contract::find()
      ->select(['id','end_date'])
      ->where(['start_date'=>date("Y-m-d"),'is_activated'=>0])
      ->asArray()->limit(15)->all();
      if($results!=null){
        $connection = \Yii::$app->db;
        foreach($results as $result){
          $contractMembers=ContractMember::find()
          ->select(['user_id','status'])
          ->where(['contract_id'=>$result['id']])
          ->asArray()->all();
          if($contractMembers!=null){
            foreach($contractMembers as $contractMember){
              $connection->createCommand(
                "update ".User::tableName()." set active_contract_id=:active_contract_id,end_date=:end_date,status=:status where id=:id",
                [':active_contract_id'=>$result['id'],':end_date'=>$result['end_date'],':status'=>$contractMember['status'],':id'=>$contractMember['user_id']]
                )
                ->execute();
              }
            }
          }
          $connection->createCommand(
            "update ".Contract::tableName()." set is_activated=:is_activated where id=:id",
            [':is_activated'=>1,':id'=>$result['id']]
            )
            ->execute();
          }
          return 1;
        }
        /**
        * This command updates users hold status
        */
        public function actionHoldStatus()
        {
          $results = UserStatusReason::find()->select([UserStatusReason::tableName().'.id','user_id','old_status'=>User::tableName().'.status','reason'])
          ->innerJoin("user","user.id=user_status_reason.user_id")
          ->where(['and',[UserStatusReason::tableName().'.status'=>2,'is_updated'=>0],['<=','date',date("Y-m-d")]])
          ->asArray()->all();
          if($results!=null){
            $connection = \Yii::$app->db;
            foreach($results as $result){
              $cronStat=new UserStatusCron;
              $cronStat->user_id=$result['user_id'];
              $cronStat->old_status=$result['old_status'];
              $cronStat->new_status=2;
              $cronStat->reason=$result['reason'];
              $cronStat->created_at=date("Y-m-d H:i:s");
              if($cronStat->save()){
                $connection->createCommand("update ".User::tableName()." set status=2,hold_start=NULL,hold_reason=NULL where id=:id",[':id'=>$result['user_id']])->execute();
                $thisUser = User::find()->where(['id'=>$result['user_id']])->one();
                if($thisUser!=null){
                  $activePackage=$thisUser->activePackage;
                  $connection->createCommand("update ".ContractMember::tableName()." set status=2 where contract_id=:contract_id and user_id=:user_id",[':contract_id'=>$activePackage->contract_id,':user_id'=>$result['user_id']])->execute();
                }
                $connection->createCommand("update ".UserStatusReason::tableName()." set is_updated=1 where id=:id",[':id'=>$result['id']])->execute();
              }
            }
          }
          return 1;
        }

        /**
        * This command updates users cancel status
        */
        public function actionCancelStatus()
        {
          $results = UserStatusReason::find()->select([UserStatusReason::tableName().'.id','user_id','old_status'=>User::tableName().'.status','reason'])
          ->innerJoin("user","user.id=user_status_reason.user_id")
          ->where(['and',[UserStatusReason::tableName().'.status'=>3,'is_updated'=>0],['<=','date',date("Y-m-d")]])
          ->asArray()->all();
          if($results!=null){
            $connection = \Yii::$app->db;
            foreach($results as $result){
              $cronStat=new UserStatusCron;
              $cronStat->user_id=$result['user_id'];
              $cronStat->old_status=$result['old_status'];
              $cronStat->new_status=3;
              $cronStat->reason=$result['cancel_reason'];
              $cronStat->created_at=date("Y-m-d H:i:s");
              if($cronStat->save()){
                $connection->createCommand("update ".User::tableName()." set status=3,cancel_start=NULL,cancel_reason=NULL where id=:id",[':id'=>$result['user_id']])->execute();
                $thisUser = User::find()->where(['id'=>$result['user_id']])->one();
                if($thisUser!=null){
                  $activePackage=$thisUser->activePackage;
                  $connection->createCommand("update ".ContractMember::tableName()." set status=3 where contract_id=:contract_id and user_id=:user_id",[':contract_id'=>$activePackage->contract_id,':user_id'=>$result['user_id']])->execute();
                }
                $connection->createCommand("update ".UserStatusReason::tableName()." set is_updated=1 where id=:id",[':id'=>$result['id']])->execute();
              }
            }
          }
          return 1;
        }
      }
