<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;
use app\models\UserProfileInfo;
use app\models\UserNote;
use app\models\Newsletter;
use app\models\NewsletterEmailSent;
use app\models\BlackListedEmail;
use app\models\DroppedEmail;
use app\models\Contract;
use app\models\ContractMember;
use app\models\Package;
use app\models\NewsletterCity;
use app\models\NewsletterPackage;
use app\models\NewsletterAttachment;
use app\models\City;
use Mailgun\Mailgun;

class NewsletterController extends Controller
{
    public function actionProcess()
    {
		$result = Newsletter::find()->where(['sent'=>0])->asArray()->one();
		if($result!=null){
      $connection = Yii::$app->db;

      $attachmentsArr=[];
      $attachments=NewsletterAttachment::find()->where(['newsletter_id'=>$result['id']])->asArray()->all();
      if($attachments!=null){
        foreach($attachments as $attachment){
          if($attachment['attachment_file']!=null && file_exists(Yii::$app->params['newsletter_uploads_abs_path'].$attachment['attachment_file'])){
            $fpInfo=pathinfo(Yii::$app->params['newsletter_uploads_abs_path'].$attachment['attachment_file']);
            $readableFileName=$attachment['file_title'].'.'.$fpInfo['extension'];
            $copyToFile=Yii::$app->params['newsletter_uploads_abs_path'].$readableFileName;
            if(!file_exists($copyToFile)){
              copy(Yii::$app->params['newsletter_uploads_abs_path'].$attachment['attachment_file'],$copyToFile);
            }
            if(!in_array($copyToFile,$attachmentsArr)){
              $attachmentsArr[]=$copyToFile;
            }
          }
        }
      }
			$subQuerySent = NewsletterEmailSent::find()->where(['newsletter_id'=>$result['id']])->select('user_id');
			$subQueryBlackList = BlackListedEmail::find()->select('email')->where(['trashed'=>0]);
      $subQueryDroppedList = DroppedEmail::find()->select('email')->where(['trashed'=>0]);

      $subQueryNewsletterCities=null;
      $checkIfCityExists=NewsletterCity::find()->where(['newsletter_id'=>$result['id']]);
      if($checkIfCityExists->exists()){
        $subQueryNewsletterCities=NewsletterCity::find()->select(['city_id'])->where(['newsletter_id'=>$result['id']]);
      }
      $subQueryNewsletterPackages=null;
      $checkIfPackageExists=NewsletterPackage::find()->where(['newsletter_id'=>$result['id']]);
      if($checkIfPackageExists->exists()){
        $subQueryNewsletterPackages=NewsletterPackage::find()->select(['package_id'])->where(['newsletter_id'=>$result['id']]);
      }

      $crBalPlus=null;
      $crBalMin=null;
      if($result['credit_balance']!=null){
        if($result['credit_balance']=='+'){
          $crBalPlus=0;
        }
        if($result['credit_balance']=='-'){
          $crBalMin=0;
        }
      }
      $subQueryTasksOne=null;
      $subQueryTasksTwo=null;
      $subQueryTasksThree=null;
      $expiryMonth=null;
      $expiryMonthContractCond=null;


      if($result['expiry_month']!=null){
        $expiryMonth=$result['expiry_month'];
        $expiryMonthContractCond=0;
      }

      if($result['tasks']!=''){
        if($result['tasks']==1){
          //No Task
          $subQueryTasksOne = UserNote::find()
            ->select([UserNote::tableName().".user_id"])
            ->groupBy(UserNote::tableName().".user_id");
        }
        if($result['tasks']==2){
          //No Pending Task
          $subQueryTasksTwo = UserNote::find()
            ->select([UserNote::tableName().".user_id"])
            ->where(['and',['<','DATE(reminder)',date("Y-m-d")],['or',['remarks'=>NULL],['=','remarks','']]])
            ->groupBy(UserNote::tableName().".user_id");
        }
        if($result['tasks']==3){
          //No Future Task
          $subQueryTasksThree = UserNote::find()
            ->select([UserNote::tableName().".user_id"])
            ->where(['>=','DATE(reminder)',date("Y-m-d")])
            ->groupBy(UserNote::tableName().".user_id");
        }
      }

      $users = User::find()
      ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
      ->leftJoin(Contract::tableName(),Contract::tableName().'.id='.User::tableName().'.active_contract_id')
      ->leftJoin(Package::tableName(),Package::tableName().'.id='.Contract::tableName().'.package_id')
      ->leftJoin(City::tableName(),City::tableName().'.id='.UserProfileInfo::tableName().'.city_id')
      ->where([
        'and',
        ['user_type' => 0,User::tableName().'.trashed' => 0,],
        ['not in', User::tableName().'.id', $subQuerySent],
        ['not in', User::tableName().'.email', $subQueryBlackList],
        ['not in', User::tableName().'.email', $subQueryDroppedList],
      ])
      ->andFilterWhere([
        UserProfileInfo::tableName().'.city_id' => $subQueryNewsletterCities,
        Contract::tableName().'.package_id' => $subQueryNewsletterPackages,
        UserProfileInfo::tableName().'.security_deposit' => $result['security_deposit'],
        UserProfileInfo::tableName().'.is_licensed' => $result['is_licensed'],
        User::tableName().'.status' => $result['status'],
        Contract::tableName().'.renewed_contract_id' => $expiryMonthContractCond,
      ])
      ->andFilterWhere([
        'or',
        ['like','username',$result['keyword']],
        ['like','firstname',$result['keyword']],
        ['like','lastname',$result['keyword']],
        ['like','email',$result['keyword']],
        ['like',UserProfileInfo::tableName().'.mobile',$result['keyword']],
      ])
      ->andFilterWhere(['like',Contract::tableName().'.end_date',$expiryMonth])
      ->andFilterWhere(['>',UserProfileInfo::tableName().'.credit_balance',$crBalPlus])
      ->andFilterWhere(['<',UserProfileInfo::tableName().'.credit_balance',$crBalMin])
      ->andFilterWhere(['not in',User::tableName().'.id',$subQueryTasksOne])
      ->andFilterWhere(['in',User::tableName().'.id',$subQueryTasksTwo])
      ->andFilterWhere(['in',User::tableName().'.id',$subQueryTasksThree])
      ->limit(15)->all();

      if($users==null && $result['cc_admin_idz']!=null && $result['cc_admin_idz']!=''){
        $ccIdz=explode(",",$result['cc_admin_idz']);
        $users=User::find()
  			->where([
          'and',
          ['in','id',$ccIdz],
          ['not in', User::tableName().'.id', $subQuerySent],
          ['not in', User::tableName().'.email', $subQueryBlackList],
          ['not in', User::tableName().'.email', $subQueryDroppedList],
        ])
        ->limit(15)->all();
      }
			if($users!=null){
        foreach($users as $user){
          //Save to Sent Emails
					$sent=new NewsletterEmailSent;
					$sent->newsletter_id=$result['id'];
					$sent->user_id=$user['id'];
					$sent->email=$user['email'];
					$sent->email_status=0;
					$sent->save();

					$unSubscribeLink=Yii::$app->urlManager->createAbsoluteUrl(['site/un-subscribe', 'email'=>$result['email'],'ref'=>'tccnl'.$result['id']]);
					\Yii::$app->params['unSubscribeLink'] = $unSubscribeLink;

					$message = \Yii::$app->mailer
					->compose(['html' => 'newsletterGeneral-html','text' => 'newsletterGeneral-text'],['model'=>$result])
					->setFrom([$result['email_from'] => Yii::$app->params['siteName'] . ''])
					->setTo([$user['email']=>$user->fullname])
					->setSubject($result['subject']);

          if($attachmentsArr!=null){
            foreach($attachmentsArr as $key=>$val){
              $message->attach($val);
            }
          }

					$headers = $message->getSwiftMessage()->getHeaders();
					$headers->addTextHeader('X-Mailgun-Variables', json_encode(['mail_type'=>'newsletter','campaign_id'=>$result['id']]));
					$headers->addTextHeader('Precedence', 'bulk');
					$headers->addTextHeader('List-Unsubscribe', $unSubscribeLink);

					$message->send();

          $connection->createCommand()->update(NewsletterEmailSent::tableName(), ['email_status' => 1], 'id=:id', [':id' => $sent->id])->execute();
				}
			}else{

        if($attachmentsArr!=null){
          foreach($attachmentsArr as $key=>$val){
            if(file_exists($val)){
              unlink($val);
            }
          }
        }
				$connection->createCommand()->update(Newsletter::tableName(), ['sent' => 1], 'id=:id', [':id' => $result['id']])->execute();
			}
		}
		return 1;
    }
}
