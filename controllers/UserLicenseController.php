<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserLicense;
use app\models\UserLicenseSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* UserLicenseController implements the CRUD actions for User Note model.
*/
class UserLicenseController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Creates a new UserLicense model.
  * @return mixed
  */
  public function actionCreate($user_id=null)
  {
    $this->checkSuperAdmin();
    if($user_id==null){
      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    $model = new UserLicense;
    $model->user_id=$user_id;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('_form', [
      'model' => $model,
    ]);
  }

  /**
  * Update a UserLicense model.
  * @return mixed
  */
  public function actionUpdate($id)
  {
    $this->checkSuperAdmin();
    $model = $this->findModel($id);
    $model->oldImage=$model->license_image;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('_form', [
      'model' => $model,
    ]);
  }

  /**
   * Deletes an existing model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDelete($id)
  {
    $this->checkLogin();
      $this->findModel($id)->softDelete();
      echo "success";
      exit;
  }

  /**
  * Finds the UserLicense model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return User the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = UserLicense::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
