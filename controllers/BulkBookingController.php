<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\models\BulkBooking;
use app\models\BulkBookingSearch;
use app\models\Booking;
use app\models\Boat;
use app\models\BoatToTimeSlot;
use app\models\BookingSearch;
use app\models\BulkBookingSwap;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* BulkBookingController implements the CRUD actions for BulkBooking model.
*/
class BulkBookingController extends DefController
{

  /**
  * Lists all BulkBooking models.
  * @return mixed
  */
  public function actionIndex($listType='active')
  {
    $this->checkAdmin();
    $searchModel = new BulkBookingSearch();
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new BulkBooking model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionAdd($id=null)
  {
    $this->checkAdmin();
    if($id!=null){
      $model = $this->findModel($id);
    }else{
      $model = new BulkBooking();
    }


    if ($model->load(Yii::$app->request->post())) {
      $model->done=0;
      if($model->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['add','id'=>$model->id]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    $searchModel = new BookingSearch;
    $searchModel->boat_id=$model->boat_id;
    $searchModel->is_bulk=0;
    $searchModel->listType='future';
    $searchModel->booking_date=$model->start_date.' - '.$model->end_date;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


    return $this->render('create', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    $this->checkAdmin();
    $model=$this->findModel($id);

    $searchModel = new BookingSearch;
    $searchModel->boat_id=$model->boat_id;
    $searchModel->is_bulk=0;
    $searchModel->listType='future';
    $searchModel->booking_date=$model->start_date.' - '.$model->end_date;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('view', [
          'model' => $model,
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

  public function actionSwap($id)
  {
    $this->checkLogin();
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->asArray()->one();
    if($booking!=null){
      $model = new BulkBookingSwap;
      $model->booking_id = $booking['id'];
      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          echo 'success';
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  echo $val;
                  die();
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('_bulk_booking_swap', [
        'booking' => $booking,
        'model' => $model,
      ]);
    }else{
      throw new NotFoundHttpException('No request found!');
    }
  }

  /**
  * Book boats for given date specified bulk booking entry
  * @return mixed
  */
  public function actionBookForBulkBooking($bulk_booking_id,$date)
  {
    $this->checkAdmin();
    $bulkBooking=$this->findModel($bulk_booking_id);
    $boatTimings=BoatToTimeSlot::find()->where(['boat_id'=>$bulkBooking->boat_id])->all();
    $boat=Boat::findOne($bulkBooking->boat_id);
    if($boat!=null){

      //Check if date is before or last day
      if($date<=$bulkBooking->end_date){
        $incrementBy=3; //Loop run for
        $remaining=Yii::$app->helperFunctions->getNumberOfDaysBetween($date,$bulkBooking->end_date);//Remaining days now
        if($remaining<$incrementBy){
          $incrementBy=$remaining;//If remaining are less then 7 go for remaining
        }

        //Final condition till when loop should run
        $loopTillDate = date ("Y-m-d", strtotime("+".$incrementBy." day", strtotime($date)));

        while (strtotime($date) <= strtotime($loopTillDate)) {
          //Loop Boat Timings
          if($boatTimings!=null){
            foreach($boatTimings as $boatTiming){
              $booking=Booking::find()->where(['city_id'=>$boat->city_id,'port_id'=>$boat->port_id,'boat_id'=>$boat->id,'booking_date'=>$date,'booking_time_slot'=>$boatTiming->time_slot_id,'trashed'=>0]);
              if(!$booking->exists()){
                $booking=new Booking;
                $booking->booking_source='bulk';
                $booking->user_id=Yii::$app->user->identity->id;
                $booking->city_id=$boat->city_id;
                $booking->port_id=$boat->port_id;
                $booking->boat_id=$boat->id;
                $booking->booking_date=$date;
                $booking->booking_time_slot=$boatTiming->time_slot_id;
                $booking->booking_type=$bulkBooking->booking_type;
                $booking->booking_comments=$bulkBooking->booking_comments;
                $booking->is_bulk=1;
                $booking->save();
              }
            }
          }
          //Incrementing date for next day
          $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
        }


        $total=Yii::$app->helperFunctions->getNumberOfDaysBetween($bulkBooking->start_date,$bulkBooking->end_date);
        $done=Yii::$app->helperFunctions->getNumberOfDaysBetween($bulkBooking->start_date,date ("Y-m-d", strtotime("-1 day", strtotime($date))));

        if($total>0){
          $percentage=round(($done/$total)*100);
        }else{
          $percentage=100;
        }
        $msg['progress']=['url'=>Url::to(['book-for-bulk-booking','bulk_booking_id'=>$bulk_booking_id,'date'=>$date]),'total'=>$total,'done'=>$done,'percentage'=>$percentage];
      }else{
        $connection = \Yii::$app->db;
        $connection->createCommand("update ".$bulkBooking->tableName()." set done='1' where id='".$bulkBooking->id."'")->execute();
        $msg['success']=['heading'=>'Success','msg'=>'Bulk booking completed'];
      }
    }else{
      $msg['error']=['heading'=>'Error','msg'=>'Boat not found'];
    }
    echo json_encode($msg);
    exit;
  }

  /**
   * Deletes an existing model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDelete($id,$start_date=null,$current_date=null)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    if($model!=null && $model->end_date>=date("Y-m-d")){

      if($current_date<=$model->end_date){
        $total=Yii::$app->helperFunctions->daysDifference($start_date,$model->end_date);
        $remainingDays=Yii::$app->helperFunctions->daysDifference($current_date,$model->end_date);
        $done=$total-$remainingDays;

        $bookings=Booking::find()
          ->where(['is_bulk'=>1,'boat_id'=>$model->boat_id,'booking_date'=>$current_date,'trashed'=>0])
          ->all();
        if($bookings!=null){
          foreach($bookings as $booking){
            $booking->ignore_bulk_booking_check='yes';
            $booking->softDelete();
          }
        }

        $nextdate = date ("Y-m-d", strtotime("+1 day", strtotime($current_date)));

        if($total>0){
          $percentage=round(($done/$total)*100);
        }else{
          $percentage=100;
        }
        $msg['progress']=['url'=>Url::to(['delete','id'=>$model->id,'start_date'=>$start_date,'current_date'=>$nextdate]),'total'=>$total,'done'=>$done,'percentage'=>$percentage];

      }else{
        //Mark is delete
        $model->softDelete();
        $msg['success']=['heading'=>'Success','msg'=>'Bulk booking deleted successfully'];
      }
    }else{
      $msg['error']=['heading'=>'Error','msg'=>'bulk booking is already expired'];
    }
    echo json_encode($msg);
    exit;
  }

  public function actionMemberBookings($date,$boat_id)
  {
    $this->checkSuperAdmin();
    $searchModel = new BookingSearch();
    $searchModel->listType = 'future';
    $searchModel->boat_id = $boat_id;
    $searchModel->booking_date = $date;
    $searchModel->is_bulk = 0;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $this->render('booking', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Finds the BulkBooking model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return BulkBooking the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = BulkBooking::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
