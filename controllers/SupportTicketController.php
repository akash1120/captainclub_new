<?php

namespace app\controllers;

use Yii;
use app\models\SupportTicket;
use app\models\SupportTicketSearch;
use app\models\SupportTicketAttachment;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* SupportTicketController implements the CRUD actions for SupportTicket model.
*/
class SupportTicketController extends DefController
{

  /**
  * Lists all SupportTicket models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkSuperAdmin();
    $searchModel = new SupportTicketSearch();
    $searchModel->parent_id=0;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Discount model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new SupportTicket();
    $model->scenario = 'ticket';

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', str_replace('"','\"',$val));
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  public function actionView($id)
  {
    $this->checkSuperAdmin();
    $model = $this->findModel($id);

		$searchModel = new SupportTicketSearch();
		$searchModel->parent_id=$model->ref_id;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination->pageSize=10;

    return $this->render('view', [
      'model' => $model,
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Updates an existing Discount model.
  * If update is successful, the browser will be redirected to the 'view' page.
  * @param integer $id
  * @return mixed
  */
  public function actionUpdate($id)
  {
    $this->checkSuperAdmin();
    $model = $this->findModel($id);
    $model->scenario = 'ticket';

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', str_replace('"','\"',$val));
              }
            }
          }
        }
      }
    }
    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Deletes an existing SupportTicket model.
  * If deletion is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  */
  public function actionChangeStatus($id,$type)
  {
    $this->checkSuperAdmin();
    $model=$this->findModel($id);
    $connection = \Yii::$app->db;
		$connection->createCommand("update ".$model->tableName()." set status=:status,updated_at='".date("Y-m-d H:i:s")."',updated_by='".Yii::$app->user->identity->id."' where id='".$model->id."'",[':status'=>$type])->execute();
    echo "done";
    exit;
  }

  /**
  * Deletes an existing SupportTicket model.
  * If deletion is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  */
  public function actionDeleteFile($tid,$id)
  {
    $this->checkSuperAdmin();
    $model=$this->findModel($tid);
    $attModel = $this->findAttachmentModel($model->ref_id,$id);
    $attModel->softDelete();
    echo "done";
    exit;
  }

  /**
  * Finds the SupportTicket model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return SupportTicket the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = SupportTicket::findOne(['rssid'=>$id])) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }

  /**
  * Finds the SupportTicketAttachment model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return SupportTicketAttachment the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findAttachmentModel($tid,$id)
  {
    if (($model = SupportTicketAttachment::findOne(['id'=>$id,'ticket_ref_id'=>$tid])) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }
}
