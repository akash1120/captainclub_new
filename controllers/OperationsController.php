<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\OperationUserCityMarina;

class OperationsController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','reports'],
        'rules' => [
          [
            'actions' => ['index','reports'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Displays Operations homepage.
  *
  * @return string
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $model=OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
    if($model==null){
      $model = new OperationUserCityMarina();
      $model->user_id=Yii::$app->user->identity->id;
    }

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('index',[
      'model'=>$model,
    ]);
  }

  /**
  * Displays Operations Reports Links.
  *
  * @return string
  */
  public function actionReports()
  {
    $this->checkAdmin();

    return $this->render('reports');
  }
}
