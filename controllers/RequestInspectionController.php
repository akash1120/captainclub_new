<?php

namespace app\controllers;

use Yii;
use app\models\RequestInspection;
use app\models\RequestInspectionSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* RequestInspectionController implements the CRUD actions for RequestInspection model.
*/
class RequestInspectionController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all RequestInspection models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new RequestInspectionSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new RequestInspection model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionNew()
  {
    $this->checkAdmin();
    $model = new RequestInspection();

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          echo 'success';
          exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
                exit;
              }
            }
          }
        }
      }
    }

    return $this->renderAjax('_form', [
      'model' => $model,
    ]);
  }

  /**
  * Finds the RequestInspection model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return RequestInspection the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = RequestInspection::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
