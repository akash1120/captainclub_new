<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\components\helpers\DefController;
use app\models\OldUserRequests;
use app\models\OldContract;
use app\models\OldContractMember;
use app\models\User;
use app\models\UserProfileInfo;
use app\models\import\UserLicense;
use app\models\import\UserFeedback;
use app\models\import\UserOtherRequest;
use app\models\Contract;
use app\models\ContractMember;
use app\models\Boat;
use app\models\BoatToTimeSlot;
use app\models\Booking;
use app\models\import\UserFreezeRequest;
use app\models\import\UserNightdriveTrainingRequest;
use app\models\import\UserNightdriveTrainingRequestRemarks;
use app\models\import\UserRequestsNightdriveRemarks;
use app\models\import\UserCityUpgradeRequest;
use app\models\import\UserBookingRequest;
use app\models\import\UserRequestsDiscussion;
use app\models\import\RequestDiscussion;
use app\models\import\UserRequestsBoatAssign;
use app\models\import\LogUserRequestToBoatAssign;
use app\models\import\UserSecurityDeposit;
use app\models\UserAccounts;
use app\models\WaitingListEarlyDeparture;
use app\models\UserSearch;


class FixRecController extends DefController
{
  public function actionExtendDubai($lastid=null)
  {
    $subQueryDubai=UserProfileInfo::find()->select(['user_id'])->where(['city_id'=>736]);
    $results=User::find()
    ->select(['id','active_contract_id'])
    ->where(['status'=>1,'id'=>$subQueryDubai])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];
        if($result['active_contract_id']!=1093){
          $contract=Contract::find()->where(['id'=>$result['active_contract_id']])->asArray()->one();
          if($contract!=null){
            $extendedEndDate=date("Y-m-d", strtotime("+13 day", strtotime($contract['end_date'])));

            $connection->createCommand(
              "update ".Contract::tableName()." set end_date=:end_date where id=:id",
              [':end_date'=>$extendedEndDate,':id'=>$contract['id']]
            )
            ->execute();
          }
        }
      }
      sleep(1);
      $nextPage=Url::to(['extend-dubai','lastid'=>$lastid]);
      //echo '<a href="'.$nextPage.'">'.$nextPage.'</a>';
      echo '<script>window.location.href="'.$nextPage.'"</script>';
    }else{
      echo "<b>Finished</b>";
    }
    exit;
  }

  public function actionBucketUpload()
  {
    //$uploadObject = Yii::$app->get('s3bucket')->upload($path . $randomName, $file->tempName);
    $s3 = Yii::$app->get('s3');
    $image=dirname(dirname(__DIR__)) . '/thecaptainNewApp/web/images/logo.png';
    $result = $s3->upload('logo.png', $image);
    $exist = $s3->exist('logo.png');
    if($exist){
      $url = $s3->getUrl('logo.png');
      echo $url;
    }
    //echo '<pre>';print_r($result->data->ObjectURL);echo '</pre>';
    echo '<hr />';

    //$result = $s3->put($image, 'body');
  }

  public function actionUserLicense($lastid=null)
  {
    $results=User::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];

      }
      echo '<script>window.location.href="'.Url::to(['user-license','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b>";
    }
  exit;
  }

  public function actionEarlyDepartureRequests()
  {
    $subQueryAdded=WaitingListEarlyDeparture::find()->select(['booking_id']);
    $results=Booking::find()->where(['and',['not in','id',$subQueryAdded],['early_departure'=>1,'trashed'=>0]])->orderBy(['booking_date'=>SORT_ASC])->asArray()->limit(5)->all();
    if($results!=null){
      foreach($results as $result){
        $request=WaitingListEarlyDeparture::find()->where(['booking_id'=>$result['id']])->one();
        if($request==null){
          $request=new WaitingListEarlyDeparture;
        }
        $request->user_id=$result['user_id'];
        $request->booking_id=$result['id'];
        $request->city_id=$result['city_id'];
        $request->marina_id=$result['port_id'];
        $request->date=$result['booking_date'];
        $request->time_id=$result['booking_time_slot'];
        $request->reason='';
        $request->expected_reply='';
        $request->active_show_till=date("Y-m-d", strtotime("+2 day", strtotime($result['booking_date'])));
        $request->status=1;
        $request->save();

      }
      echo '<script>window.location.href="'.Url::to(['early-departure-requests']).'"</script>';
    }else{
      echo "<b>Finished</b>";
    }
    exit;
  }

  public function actionUserProfileInfo($lastid=null)
  {
    //Add Operations Staff to booking
    //Add boats > night cruise, marina stay in, drop off
    //License Info update
    $results=User::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userProfile=UserProfileInfo::find()->where(['user_id'=>$result['id']])->one();
        if($userProfile==null){
          $userProfile=new UserProfileInfo;
          $userProfile->user_id=$result['id'];
        }
        $userProfile->city_id=$result['city_id'];
        $userProfile->mobile=$result['mobile'];
        $userProfile->pref_marina_id=$result['pref_marina_id'];
        $userProfile->is_licensed=$result['is_licensed'];
        $userProfile->package_name=$result['package_name'];
        $userProfile->security_deposit=$result['deposit_amount'];
        $userProfile->deposit_type=$result['deposit_type'];
        $userProfile->credit_balance=$result['credit_balance'];
        $userProfile->save_cc=$result['save_cc'];
        $userProfile->note=$result['note'];
        $userProfile->changed_pass=$result['changed_pass'];
        $userProfile->profile_updated=$result['profile_updated'];
        $userProfile->mail_subscribed=$result['mail_subscribed'];
        $userProfile->un_subscribe_reason=$result['un_subscribe_reason'];
        $userProfile->save();
        if($result['license_expiry']!=null && $result['license_expiry']!=''){
          $userLicense=UserLicense::find()->where(['user_id'=>$result['id'],'city_id'=>$result['city_id'],'license_expiry'=>$result['license_expiry']])->one();
          if($userLicense==null){
            $userLicense=new UserLicense;
            $userLicense->user_id=$result['id'];
            $userLicense->city_id=$result['city_id'];
          }
          $userLicense->license_image=$result['license_image'];
          $userLicense->license_expiry=$result['license_expiry'];
          if(!$userLicense->save()){
            print_r($userLicense->getErrors());
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-profile-info','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> Moving to Security Deposit";
      echo '<script>window.location.href="'.Url::to(['security-deposit']).'"</script>';
    }
    exit;
  }

  public function actionSecurityDeposit($lastid=null)
  {
    $results=User::find()
    ->select(['id'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];
        $userProfile=UserProfileInfo::find()->select(['security_deposit','deposit_type'])->where(['user_id'=>$result['id']])->asArray()->one();
        if($userProfile!=null){
          if($userProfile['security_deposit']>0){
            $depositEntry=UserSecurityDeposit::find()->where(['user_id'=>$result['id']])->one();
            if($depositEntry==null){
              $depositEntry=new UserSecurityDeposit;
              $depositEntry->user_id=$result['id'];
            }
            $depositEntry->trans_type=$result['id'];
            $depositEntry->descp='Security Deposit';
            $depositEntry->amount=$userProfile['security_deposit'];
            $depositEntry->deposit_type=$userProfile['deposit_type'];
            $depositEntry->save();
          }
        }
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['security-deposit','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> Moving to password hash";
      echo '<script>window.location.href="'.Url::to(['user-pass-hash']).'"</script>';
    }
    exit;
  }

  public function actionCreditBalance($lastid=null)
  {
    $results=User::find()
    ->select(['id'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(75)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $amountsToPay = UserAccounts::find()->where(['trans_type'=>'dr','user_id'=>$result['id']])->sum('amount');
        $ammountsPaid = UserAccounts::find()->where(['trans_type'=>'cr','user_id'=>$result['id']])->sum('amount');
        $total = $ammountsPaid-$amountsToPay;

        $connection = \Yii::$app->db;
        $connection->createCommand(
          "update ".UserProfileInfo::tableName()." set credit_balance=:credit_balance where user_id=:user_id",
          [':credit_balance'=>$total,':user_id'=>$result['id']]
        )
        ->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['credit-balance','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b>";
    }
    exit;
  }

  public function actionUserPassHash($lastid=null)
  {
    $results=User::find()
    ->select(['id','password_hash'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];
        $passwordHash=Yii::$app->security->generatePasswordHash($result['password_hash']);

        $connection->createCommand(
          "update ".User::tableName()." set password_hash=:password_hash where id=:id",
          [
            ':password_hash' => $passwordHash,
            ':id' => $result['id'],
          ]
        )
        ->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['user-pass-hash','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to active contracts";
      echo '<script>window.location.href="'.Url::to(['fix-rec/fix-active-contract']).'"</script>';
    }
    exit;
  }

	public function actionFixActiveContract($lastId=0)
	{
		$users=User::find()
			->select(['id'])
			->where(['and',['user_type'=>0,'status'=>1],['>','id',$lastId]])
			->orderBy(['id'=>SORT_ASC])->limit(50)->asArray()->all();
		if($users!=null){
			$connection = \Yii::$app->db;
			foreach($users as $user){
				$lastId=$user['id'];
				$userLatestContractId=0;
				$userLatestStatus=1;
				//Updating start date
				$veryStartDateRow=OldContractMember::find()
					->select([
						OldContract::tableName().'.start_date',
					])
					->innerJoin("contract",OldContract::tableName().".id=".OldContractMember::tableName().".contract_id")
					->where([OldContractMember::tableName().'.user_id'=>$user['id']])
					->orderBy(['end_date'=>SORT_ASC])->asArray()->one();
				if($veryStartDateRow!=null){
					$connection->createCommand(
						"update ".User::tableName()." set start_date=:start_date where id=:id",
						[':start_date'=>$veryStartDateRow['start_date'],':id'=>$user['id']]
					)
					->execute();
				}

				//Updating max end date
				$maxEndDateRow=OldContractMember::find()
					->select([
						OldContract::tableName().'.end_date',
						OldContractMember::tableName().'.contract_id',
						OldContractMember::tableName().'.status'
					])
					->innerJoin("contract",OldContract::tableName().".id=".OldContractMember::tableName().".contract_id")
					->where([OldContractMember::tableName().'.user_id'=>$user['id']])
					->orderBy(['end_date'=>SORT_DESC])->asArray()->one();
				if($maxEndDateRow!=null){
					$userLatestContractId=$maxEndDateRow['contract_id'];
					$userLatestStatus=$maxEndDateRow['status'];
					$connection->createCommand(
						"update ".User::tableName()." set end_date=:end_date,status=:status where id=:id",
						[':end_date'=>$maxEndDateRow['end_date'],':status'=>$maxEndDateRow['status'],':id'=>$user['id']]
					)
					->execute();
				}
				//Update Active contract id
				$activeContractId=0;
				$activeContractRow=OldContractMember::find()
					->select([OldContractMember::tableName().'.contract_id','status'])
					->innerJoin("contract",OldContract::tableName().".id=".OldContractMember::tableName().".contract_id")
					->where([
						'and',
						['<=',OldContract::tableName().'.start_date',date("Y-m-d")],
						['>=',OldContract::tableName().'.end_date',date("Y-m-d")],
						[OldContractMember::tableName().'.user_id'=>$user['id']]
					])
					->asArray()->one();
				if($activeContractRow!=null){
					$activeContractId=$activeContractRow['contract_id'];
					$activeContractStatus=$activeContractRow['status'];
				}

				if($activeContractId==0){
					$activeContractId=$userLatestContractId;
					$activeContractStatus=$userLatestStatus;
				}
				if($activeContractId>0){
					$connection->createCommand(
						"update ".User::tableName()." set active_contract_id=:active_contract_id,status=:status where id=:id",
						[':active_contract_id'=>$activeContractId,':status'=>$activeContractStatus,':id'=>$user['id']]
					)
					->execute();
					$connection->createCommand(
						"update ".OldContract::tableName()." set is_activated=1 where id=:id",
						[':id'=>$activeContractId]
					)
					->execute();
				}
			}
			sleep(5);
			die("<script>window.location.href='".Url::to(['fix-rec/fix-active-contract','lastId'=>$lastId])."'</script>");
		}else{
      echo "<b>Finished</b> moving to active packages";
      echo '<script>window.location.href="'.Url::to(['fix-rec/active-package']).'"</script>';
		}
    exit;
	}

  public function actionActivePackage($lastId=0)
  {
    $users=User::find()
      ->select(['id','active_contract_id'])
      ->where(['and',['user_type'=>0],['>','id',$lastId]])
      ->orderBy(['id'=>SORT_ASC])->limit(50)->asArray()->all();
    if($users!=null){
      $connection = \Yii::$app->db;
      foreach($users as $user){
        $lastId=$user['id'];
        $contract=Contract::find()->select(['package_id'])->where(['id'=>$user['active_contract_id']])->asArray()->one();
        $connection->createCommand(
          "update ".User::tableName()." set active_package_id=:active_package_id where id=:id",
          [':active_package_id'=>$contract['package_id'],':id'=>$user['id']]
        )
        ->execute();
      }
      //sleep(2);
      die("<script>window.location.href='".Url::to(['fix-rec/active-package','lastId'=>$lastId])."'</script>");
    }else{
      echo "<b>Finished</b> moving to expired";
      echo '<script>window.location.href="'.Url::to(['fix-rec/expired']).'"</script>';
    }
    exit;
  }

  public function actionExpired($lastId=0)
  {
  		$users=User::find()
  		->select(['id'])
  		->where(['and',['user_type'=>0,'status'=>1],['>','id',$lastId]])
  		->orderBy(['id'=>SORT_ASC])->limit(50)->asArray()->all();
  		if($users!=null){
  			$connection = \Yii::$app->db;
  			foreach($users as $user){
  				$lastId=$user['id'];
          $subQueryUserContracts=ContractMember::find()->select(['contract_id'])->where(['user_id'=>$user['id']]);
          $contract=Contract::find()->where(['and',['id'=>$subQueryUserContracts],['>=','end_date',date("Y-m-d")]]);
          if(!$contract->exists()){
            $connection->createCommand(
              "update ".User::tableName()." set status=4 where id=:id",
              [':id'=>$user['id']]
            )
            ->execute();
          }
        }
  			sleep(5);
  			die("<script>window.location.href='".Url::to(['fix-rec/expired','lastId'=>$lastId])."'</script>");
      }else{
        echo "<b>Finished</b> moving to captain usage";
        echo '<script>window.location.href="'.Url::to(['fix-rec/fix-captain-usage']).'"</script>';
  		}
      exit;
  }

  public function actionFixCaptainUsage()
  {
    $connection = \Yii::$app->db;
    //
    $connection->createCommand(
      "update booking set captain_type='paid' where captain=1 and trashed=0 and booking_date<='".date("Y-m-d")."' and id in (select booking_id from captain_request)"
    )
    ->execute();
    $connection->createCommand(
      "update booking set captain_type='free' where captain=1 and trashed=0 and booking_date<='".date("Y-m-d")."' and id not in (select booking_id from captain_request)"
    )
    ->execute();
    echo "<b>Finished</b> moving to remaining captains";
    echo '<script>window.location.href="'.Url::to(['fix-rec/contract-remaining-captains']).'"</script>';
    exit;
  }

  public function actionContractRemainingCaptains($lastId=0)
  {
  		$results=Contract::find()
  			->where(['and',['>','id',$lastId],['<=','start_date',date("Y-m-d")]])
  			->orderBy(['id'=>SORT_ASC])->limit(50)->asArray()->all();
  		if($results!=null){
  			$connection = \Yii::$app->db;
  			foreach($results as $result){
  				$lastId=$result['id'];
          $subQueryUsers=ContractMember::find()->select(['user_id'])->where(['contract_id'=>$result['id']]);

          $freeCaptainsUsed=Booking::find()->where([
            'and',
            ['user_id'=>$subQueryUsers,'trashed'=>0],
            ['>=','booking_date',$result['start_date']],
            ['<=','booking_date',$result['end_date']],
          ])->count('id');
          $remainingCaptains=$result['allowed_captains']-$freeCaptainsUsed;
          if($remainingCaptains<0){
            $remainingCaptains=0;
          }
          $connection->createCommand(
            "update ".Contract::tableName()." set remaining_captains=:remaining where id=:id",
            [':remaining'=>$remainingCaptains,':id'=>$result['id']]
          )
          ->execute();
        }
  			sleep(5);
  			die("<script>window.location.href='".Url::to(['fix-rec/contract-remaining-captains','lastId'=>$lastId])."'</script>");
      }else{
      echo "<b>Finished</b> moving to boat timings";
      echo '<script>window.location.href="'.Url::to(['fix-rec/fix-boat-timing']).'"</script>';
  		}
      exit;
  }

  public function actionFixBoatTiming($lastid=0)
  {
    $results=Boat::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(5)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];
        $boatTimings=unserialize($result['timeslots']);
        foreach($boatTimings as $key=>$val){
          $boatTimeSlotRow=BoatToTimeSlot::find()->where(['boat_id'=>$result['id'],'time_slot_id'=>$val])->one();
          if($boatTimeSlotRow==null){
            $boatTimeSlotRow=new BoatToTimeSlot;
            $boatTimeSlotRow->boat_id=$result['id'];
            $boatTimeSlotRow->time_slot_id=$val;
            $boatTimeSlotRow->save();
          }
        }
      }
      sleep(5);
      echo '<script>window.location.href="'.Url::to(['fix-boat-timing','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to freeze requests";
      echo '<script>window.location.href="'.Url::to(['user-freeze']).'"</script>';
    }
    exit;
  }

  //Disable rules & before/after save emails
  //UserFreezeRequest
  //LogUserRequestToBoatAssign
  public function actionUserFreeze($lastid=null)
  {
    $connection = \Yii::$app->db;
    $results=OldUserRequests::find()
    ->where(['item_type'=>'freeze'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(5)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userFeedback=new UserFreezeRequest;
        $userFeedback->user_id=$result['created_by'];
        $userFeedback->start_date=$result['freeze_start'];
        $userFeedback->end_date=$result['freeze_end'];
        $userFeedback->requested_date=$result['requested_date'];
        $userFeedback->active_show_till=$result['active_show_till'];
        $userFeedback->status=$result['status'];
        if($userFeedback->save()){
          $connection->createCommand(
            "update ".UserFreezeRequest::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
            [
              ':created_at'=>$result['created_at'],
              ':created_by'=>$result['created_by'],
              ':updated_at'=>$result['updated_at'],
              ':updated_by'=>$result['updated_by'],
              ':trashed'=>$result['trashed'],
              ':trashed_at'=>$result['trashed_at'],
              ':trashed_by'=>$result['trashed_by'],
              ':id'=>$userFeedback->id
            ]
          )->execute();
          $discussions=UserRequestsDiscussion::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($discussions!=null){
            foreach($discussions as $discussion){
              $newComment=new RequestDiscussion;
              $newComment->request_type='freeze';
              $newComment->request_id=$userFeedback->id;
              $newComment->comments=$discussion['comments'];
              if($newComment->save()){
                $connection->createCommand(
                  "update ".RequestDiscussion::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
                  [
                    ':created_at'=>$discussion['created_at'],
                    ':created_by'=>$discussion['created_by'],
                    ':updated_at'=>$discussion['updated_at'],
                    ':updated_by'=>$discussion['updated_by'],
                    ':trashed'=>$discussion['trashed'],
                    ':trashed_at'=>$discussion['trashed_at'],
                    ':trashed_by'=>$discussion['trashed_by'],
                    ':id'=>$newComment->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newComment->getErrors());echo '</pre>';
                die();
              }
            }
          }
          //Boat Assignment
          $boatAssignments=UserRequestsBoatAssign::find()->where(['request_id'=>$result['id']])->asArray()->all();
          if($boatAssignments!=null){
            foreach($boatAssignments as $boatAssignment){
              $logEntry=new LogUserRequestToBoatAssign;
              $logEntry->request_type='freeze';
              $logEntry->request_id=$userFeedback->id;
              $logEntry->booking_id=$boatAssignment['booking_id'];
              $logEntry->city_id=$boatAssignment['city_id'];
              $logEntry->marina_id=$boatAssignment['marina_id'];
              $logEntry->boat_id=$boatAssignment['boat_id'];
              $logEntry->date=$boatAssignment['date'];
              $logEntry->time_id=$boatAssignment['time_id'];
              if($logEntry->save()){
                $connection->createCommand(
                  "update ".LogUserRequestToBoatAssign::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$boatAssignment['created_at'],
                    ':created_by'=>$boatAssignment['created_by'],
                    ':updated_at'=>$boatAssignment['updated_at'],
                    ':updated_by'=>$boatAssignment['updated_by'],
                    ':id'=>$logEntry->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($logEntry->getErrors());echo '</pre>';
                die();
              }
            }
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-freeze','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to user feedback";
      echo '<script>window.location.href="'.Url::to(['user-feedback']).'"</script>';
    }
    exit;
  }

  //Disable rules & before/after save emails
  //UserFeedback
  //LogUserRequestToBoatAssign
  public function actionUserFeedback($lastid=null)
  {
    //Admin menu ? user/feedback > user/new-feedback
    $connection = \Yii::$app->db;
    $results=OldUserRequests::find()
    ->where(['item_type'=>'feedback'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(5)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userFeedback=new UserFeedback;
        $userFeedback->user_id=$result['created_by'];
        $userFeedback->descp=$result['descp'];
        $userFeedback->rating=$result['rating'];
        if($userFeedback->save()){
          $connection->createCommand(
            "update ".UserFeedback::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
            [
              ':created_at'=>$result['created_at'],
              ':created_by'=>$result['created_by'],
              ':updated_at'=>$result['updated_at'],
              ':updated_by'=>$result['updated_by'],
              ':trashed'=>$result['trashed'],
              ':trashed_at'=>$result['trashed_at'],
              ':trashed_by'=>$result['trashed_by'],
              ':id'=>$userFeedback->id
            ]
          )->execute();
          $discussions=UserRequestsDiscussion::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($discussions!=null){
            foreach($discussions as $discussion){
              $newComment=new RequestDiscussion;
              $newComment->request_type='feedback';
              $newComment->request_id=$userFeedback->id;
              $newComment->comments=$discussion['comments'];
              if($newComment->save()){
                $connection->createCommand(
                  "update ".RequestDiscussion::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
                  [
                    ':created_at'=>$discussion['created_at'],
                    ':created_by'=>$discussion['created_by'],
                    ':updated_at'=>$discussion['updated_at'],
                    ':updated_by'=>$discussion['updated_by'],
                    ':trashed'=>$discussion['trashed'],
                    ':trashed_at'=>$discussion['trashed_at'],
                    ':trashed_by'=>$discussion['trashed_by'],
                    ':id'=>$newComment->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newComment->getErrors());echo '</pre>';
                die();
              }
            }
          }
          //Boat Assignment
          $boatAssignments=UserRequestsBoatAssign::find()->where(['request_id'=>$result['id']])->asArray()->all();
          if($boatAssignments!=null){
            foreach($boatAssignments as $boatAssignment){
              $logEntry=new LogUserRequestToBoatAssign;
              $logEntry->request_type='feedback';
              $logEntry->request_id=$userFeedback->id;
              $logEntry->booking_id=$boatAssignment['booking_id'];
              $logEntry->city_id=$boatAssignment['city_id'];
              $logEntry->marina_id=$boatAssignment['marina_id'];
              $logEntry->boat_id=$boatAssignment['boat_id'];
              $logEntry->date=$boatAssignment['date'];
              $logEntry->time_id=$boatAssignment['time_id'];
              if($logEntry->save()){
                $connection->createCommand(
                  "update ".LogUserRequestToBoatAssign::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$boatAssignment['created_at'],
                    ':created_by'=>$boatAssignment['created_by'],
                    ':updated_at'=>$boatAssignment['updated_at'],
                    ':updated_by'=>$boatAssignment['updated_by'],
                    ':id'=>$logEntry->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($logEntry->getErrors());echo '</pre>';
                die();
              }
            }
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-feedback','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to user suggestion";
      echo '<script>window.location.href="'.Url::to(['user-suggestion']).'"</script>';
    }
    exit;
  }

  //Disable rules & before/after save emails
  //UserFeedback
  //LogUserRequestToBoatAssign
  public function actionUserSuggestion($lastid=null)
  {
    //Disable email from model
    $connection = \Yii::$app->db;
    $results=OldUserRequests::find()
    ->where(['item_type'=>'suggestion'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userFeedback=new UserFeedback;
        $userFeedback->user_id=$result['created_by'];
        $userFeedback->descp=$result['descp'];
        $userFeedback->rating=0;
        if($userFeedback->save()){
          $connection->createCommand(
            "update ".UserFeedback::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
            [
              ':created_at'=>$result['created_at'],
              ':created_by'=>$result['created_by'],
              ':updated_at'=>$result['updated_at'],
              ':updated_by'=>$result['updated_by'],
              ':trashed'=>$result['trashed'],
              ':trashed_at'=>$result['trashed_at'],
              ':trashed_by'=>$result['trashed_by'],
              ':id'=>$userFeedback->id
            ]
          )->execute();
          $discussions=UserRequestsDiscussion::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($discussions!=null){
            foreach($discussions as $discussion){
              $newComment=new RequestDiscussion;
              $newComment->request_type='feedback';
              $newComment->request_id=$userFeedback->id;
              $newComment->comments=$discussion['comments'];
              if($newComment->save()){
                $connection->createCommand(
                  "update ".RequestDiscussion::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
                  [
                    ':created_at'=>$discussion['created_at'],
                    ':created_by'=>$discussion['created_by'],
                    ':updated_at'=>$discussion['updated_at'],
                    ':updated_by'=>$discussion['updated_by'],
                    ':trashed'=>$discussion['trashed'],
                    ':trashed_at'=>$discussion['trashed_at'],
                    ':trashed_by'=>$discussion['trashed_by'],
                    ':id'=>$newComment->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newComment->getErrors());echo '</pre>';
                die();
              }
            }
          }
          //Boat Assignment
          $boatAssignments=UserRequestsBoatAssign::find()->where(['request_id'=>$result['id']])->asArray()->all();
          if($boatAssignments!=null){
            foreach($boatAssignments as $boatAssignment){
              $logEntry=new LogUserRequestToBoatAssign;
              $logEntry->request_type='feedback';
              $logEntry->request_id=$userFeedback->id;
              $logEntry->booking_id=$boatAssignment['booking_id'];
              $logEntry->city_id=$boatAssignment['city_id'];
              $logEntry->marina_id=$boatAssignment['marina_id'];
              $logEntry->boat_id=$boatAssignment['boat_id'];
              $logEntry->date=$boatAssignment['date'];
              $logEntry->time_id=$boatAssignment['time_id'];
              if($logEntry->save()){
                $connection->createCommand(
                  "update ".LogUserRequestToBoatAssign::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$boatAssignment['created_at'],
                    ':created_by'=>$boatAssignment['created_by'],
                    ':updated_at'=>$boatAssignment['updated_at'],
                    ':updated_by'=>$boatAssignment['updated_by'],
                    ':id'=>$logEntry->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($logEntry->getErrors());echo '</pre>';
                die();
              }
            }
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-suggestion','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to other requests";
      echo '<script>window.location.href="'.Url::to(['user-other-requests']).'"</script>';
    }
    exit;
  }

  //Disable rules and before/after save emails
  //UserOtherRequest
  //LogUserRequestToBoatAssign
  public function actionUserOtherRequests($lastid=null)
  {
    //Disable email from model
    $connection = \Yii::$app->db;
    $results=OldUserRequests::find()
    ->where(['item_type'=>'other'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userRequest=new UserOtherRequest;
        $userRequest->user_id=$result['created_by'];
        $userRequest->descp=$result['descp'];
        $userRequest->requested_date=$result['requested_date'];
        $userRequest->status=$result['status'];
        $userRequest->remarks=$result['remarks'];
        $userRequest->email_message=$result['email_message'];
        $userRequest->admin_action_date=$result['admin_action_date'];
        $userRequest->active_show_till=$result['active_show_till'];
        if($userRequest->save()){
          $connection->createCommand(
            "update ".UserOtherRequest::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
            [
              ':created_at'=>$result['created_at'],
              ':created_by'=>$result['created_by'],
              ':updated_at'=>$result['updated_at'],
              ':updated_by'=>$result['updated_by'],
              ':trashed'=>$result['trashed'],
              ':trashed_at'=>$result['trashed_at'],
              ':trashed_by'=>$result['trashed_by'],
              ':id'=>$userRequest->id
            ]
          )->execute();
          $discussions=UserRequestsDiscussion::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($discussions!=null){
            foreach($discussions as $discussion){
              $newComment=new RequestDiscussion;
              $newComment->request_type='other';
              $newComment->request_id=$userRequest->id;
              $newComment->comments=$discussion['comments'];
              if($newComment->save()){
                $connection->createCommand(
                  "update ".RequestDiscussion::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
                  [
                    ':created_at'=>$discussion['created_at'],
                    ':created_by'=>$discussion['created_by'],
                    ':updated_at'=>$discussion['updated_at'],
                    ':updated_by'=>$discussion['updated_by'],
                    ':trashed'=>$discussion['trashed'],
                    ':trashed_at'=>$discussion['trashed_at'],
                    ':trashed_by'=>$discussion['trashed_by'],
                    ':id'=>$newComment->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newComment->getErrors());echo '</pre>';
                die();
              }
            }
          }
          //Boat Assignment
          $boatAssignments=UserRequestsBoatAssign::find()->where(['request_id'=>$result['id']])->asArray()->all();
          if($boatAssignments!=null){
            foreach($boatAssignments as $boatAssignment){
              $logEntry=new LogUserRequestToBoatAssign;
              $logEntry->request_type='other';
              $logEntry->request_id=$userRequest->id;
              $logEntry->booking_id=$boatAssignment['booking_id'];
              $logEntry->city_id=$boatAssignment['city_id'];
              $logEntry->marina_id=$boatAssignment['marina_id'];
              $logEntry->boat_id=$boatAssignment['boat_id'];
              $logEntry->date=$boatAssignment['date'];
              $logEntry->time_id=$boatAssignment['time_id'];
              if($logEntry->save()){
                $connection->createCommand(
                  "update ".LogUserRequestToBoatAssign::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$boatAssignment['created_at'],
                    ':created_by'=>$boatAssignment['created_by'],
                    ':updated_at'=>$boatAssignment['updated_at'],
                    ':updated_by'=>$boatAssignment['updated_by'],
                    ':id'=>$logEntry->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($logEntry->getErrors());echo '</pre>';
                die();
              }
            }
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-other-requests','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to night drive training requests";
      echo '<script>window.location.href="'.Url::to(['user-night-drive-training-requests']).'"</script>';
    }
    exit;
  }

  //Disable rules and before/after save emails
  //UserNightDriveRequestReminder
  //UserNightdriveTrainingRequest
  //UserNightdriveTrainingRequestRemarks
  //LogUserRequestToBoatAssign
  public function actionUserNightDriveTrainingRequests($lastid=null)
  {
    //Disable email from model
    $connection = \Yii::$app->db;
    $results=OldUserRequests::find()
    ->where(['item_type'=>'nightdrivetraining'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userRequest=new UserNightdriveTrainingRequest;
        $userRequest->user_id=$result['created_by'];
        $userRequest->descp=$result['descp'];
        $userRequest->requested_date=$result['created_at'];
        $userRequest->status=$result['status'];
        $userRequest->remarks=$result['remarks'];
        $userRequest->email_message=$result['email_message'];
        $userRequest->admin_action_date=$result['admin_action_date'];
        $userRequest->active_show_till=$result['active_show_till'];
        if($userRequest->save()){
          $connection->createCommand(
            "update ".UserNightdriveTrainingRequest::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
            [
              ':created_at'=>$result['created_at'],
              ':created_by'=>$result['created_by'],
              ':updated_at'=>$result['updated_at'],
              ':updated_by'=>$result['updated_by'],
              ':trashed'=>$result['trashed'],
              ':trashed_at'=>$result['trashed_at'],
              ':trashed_by'=>$result['trashed_by'],
              ':id'=>$userRequest->id
            ]
          )->execute();
          $remarks=UserRequestsNightdriveRemarks::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($remarks!=null){
            foreach($remarks as $remark){
              $newRemark=new UserNightdriveTrainingRequestRemarks;
              $newRemark->user_request_id=$userRequest->id;
              $newRemark->training_date=$remark['training_date'];
              $newRemark->marina_id=$remark['marina_id'];
              $newRemark->remarks=$remark['remarks'];
              if($newRemark->save()){
                $connection->createCommand(
                  "update ".UserNightdriveTrainingRequestRemarks::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$remark['created_at'],
                    ':created_by'=>$remark['created_by'],
                    ':updated_at'=>$remark['updated_at'],
                    ':updated_by'=>$remark['updated_by'],
                    ':id'=>$newRemark->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newRemark->getErrors());echo '</pre>';
                die();
              }
            }
          }
          $discussions=UserRequestsDiscussion::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($discussions!=null){
            foreach($discussions as $discussion){
              $newComment=new RequestDiscussion;
              $newComment->request_type='nightdrivetraining';
              $newComment->request_id=$userRequest->id;
              $newComment->comments=$discussion['comments'];
              if($newComment->save()){
                $connection->createCommand(
                  "update ".RequestDiscussion::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
                  [
                    ':created_at'=>$discussion['created_at'],
                    ':created_by'=>$discussion['created_by'],
                    ':updated_at'=>$discussion['updated_at'],
                    ':updated_by'=>$discussion['updated_by'],
                    ':trashed'=>$discussion['trashed'],
                    ':trashed_at'=>$discussion['trashed_at'],
                    ':trashed_by'=>$discussion['trashed_by'],
                    ':id'=>$newComment->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newComment->getErrors());echo '</pre>';
                die();
              }
            }
          }
          //Boat Assignment
          $boatAssignments=UserRequestsBoatAssign::find()->where(['request_id'=>$result['id']])->asArray()->all();
          if($boatAssignments!=null){
            foreach($boatAssignments as $boatAssignment){
              $logEntry=new LogUserRequestToBoatAssign;
              $logEntry->request_type='nightdrivetraining';
              $logEntry->request_id=$userRequest->id;
              $logEntry->booking_id=$boatAssignment['booking_id'];
              $logEntry->city_id=$boatAssignment['city_id'];
              $logEntry->marina_id=$boatAssignment['marina_id'];
              $logEntry->boat_id=$boatAssignment['boat_id'];
              $logEntry->date=$boatAssignment['date'];
              $logEntry->time_id=$boatAssignment['time_id'];
              if($logEntry->save()){
                $connection->createCommand(
                  "update ".LogUserRequestToBoatAssign::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$boatAssignment['created_at'],
                    ':created_by'=>$boatAssignment['created_by'],
                    ':updated_at'=>$boatAssignment['updated_at'],
                    ':updated_by'=>$boatAssignment['updated_by'],
                    ':id'=>$logEntry->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($logEntry->getErrors());echo '</pre>';
                die();
              }
            }
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-night-drive-training-requests','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to upgrade city requests";
      echo '<script>window.location.href="'.Url::to(['user-upgrade-city-requests']).'"</script>';
    }
    exit;
  }

  //Disable rules and before/after save emails
  //UserCityUpgradeRequest
  //LogUserRequestToBoatAssign
  public function actionUserUpgradeCityRequests($lastid=null)
  {
    //Disable email from model
    $connection = \Yii::$app->db;
    $results=OldUserRequests::find()
    ->where(['item_type'=>'upgrade'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userRequest=new UserCityUpgradeRequest;
        $userRequest->user_id=$result['created_by'];
        $userRequest->descp=$result['descp'];
        $userRequest->requested_date=$result['created_at'];
        $userRequest->status=$result['status'];
        $userRequest->remarks=$result['remarks'];
        $userRequest->email_message=$result['email_message'];
        $userRequest->admin_action_date=$result['admin_action_date'];
        $userRequest->active_show_till=$result['active_show_till'];
        if($userRequest->save()){
          $connection->createCommand(
            "update ".UserCityUpgradeRequest::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
            [
              ':created_at'=>$result['created_at'],
              ':created_by'=>$result['created_by'],
              ':updated_at'=>$result['updated_at'],
              ':updated_by'=>$result['updated_by'],
              ':trashed'=>$result['trashed'],
              ':trashed_at'=>$result['trashed_at'],
              ':trashed_by'=>$result['trashed_by'],
              ':id'=>$userRequest->id
            ]
          )->execute();
          $discussions=UserRequestsDiscussion::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($discussions!=null){
            foreach($discussions as $discussion){
              $newComment=new RequestDiscussion;
              $newComment->request_type='upgradecity';
              $newComment->request_id=$userRequest->id;
              $newComment->comments=$discussion['comments'];
              if($newComment->save()){
                $connection->createCommand(
                  "update ".RequestDiscussion::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
                  [
                    ':created_at'=>$discussion['created_at'],
                    ':created_by'=>$discussion['created_by'],
                    ':updated_at'=>$discussion['updated_at'],
                    ':updated_by'=>$discussion['updated_by'],
                    ':trashed'=>$discussion['trashed'],
                    ':trashed_at'=>$discussion['trashed_at'],
                    ':trashed_by'=>$discussion['trashed_by'],
                    ':id'=>$newComment->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newComment->getErrors());echo '</pre>';
                die();
              }
            }
          }
          //Boat Assignment
          $boatAssignments=UserRequestsBoatAssign::find()->where(['request_id'=>$result['id']])->asArray()->all();
          if($boatAssignments!=null){
            foreach($boatAssignments as $boatAssignment){
              $logEntry=new LogUserRequestToBoatAssign;
              $logEntry->request_type='upgradecity';
              $logEntry->request_id=$userRequest->id;
              $logEntry->booking_id=$boatAssignment['booking_id'];
              $logEntry->city_id=$boatAssignment['city_id'];
              $logEntry->marina_id=$boatAssignment['marina_id'];
              $logEntry->boat_id=$boatAssignment['boat_id'];
              $logEntry->date=$boatAssignment['date'];
              $logEntry->time_id=$boatAssignment['time_id'];
              if($logEntry->save()){
                $connection->createCommand(
                  "update ".LogUserRequestToBoatAssign::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$boatAssignment['created_at'],
                    ':created_by'=>$boatAssignment['created_by'],
                    ':updated_at'=>$boatAssignment['updated_at'],
                    ':updated_by'=>$boatAssignment['updated_by'],
                    ':id'=>$logEntry->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($logEntry->getErrors());echo '</pre>';
                die();
              }
            }
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-upgrade-city-requests','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to boat booking requests";
      echo '<script>window.location.href="'.Url::to(['user-boat-booking-requests']).'"</script>';
    }
    exit;
  }

  //Disable rules and before/after save emails
  //UserBookingRequest
  //LogUserRequestToBoatAssign
  public function actionUserBoatBookingRequests($lastid=null)
  {
    //Disable email from model
    $connection = \Yii::$app->db;
    $results=OldUserRequests::find()
    ->where(['item_type'=>'boat-booking'])
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(20)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result['id'];
        $userRequest=new UserBookingRequest;
        $userRequest->user_id=$result['created_by'];
        $userRequest->city_id=$result['city_id'];
        $userRequest->marina=$result['marina'];
        $userRequest->time_slot=$result['time_slot'];
        $userRequest->descp=$result['descp'];
        $userRequest->requested_date=$result['requested_date'];
        $userRequest->status=$result['status'];
        $userRequest->remarks=$result['remarks'];
        $userRequest->email_message=$result['email_message'];
        $userRequest->admin_action_date=$result['admin_action_date'];
        $userRequest->active_show_till=$result['active_show_till'];
        if($userRequest->save()){
          $connection->createCommand(
            "update ".UserBookingRequest::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
            [
              ':created_at'=>$result['created_at'],
              ':created_by'=>$result['created_by'],
              ':updated_at'=>$result['updated_at'],
              ':updated_by'=>$result['updated_by'],
              ':trashed'=>$result['trashed'],
              ':trashed_at'=>$result['trashed_at'],
              ':trashed_by'=>$result['trashed_by'],
              ':id'=>$userRequest->id
            ]
          )->execute();
          $discussions=UserRequestsDiscussion::find()->where(['user_request_id'=>$result['id']])->asArray()->all();
          if($discussions!=null){
            foreach($discussions as $discussion){
              $newComment=new RequestDiscussion;
              $newComment->request_type='boatbooking';
              $newComment->request_id=$userRequest->id;
              $newComment->comments=$discussion['comments'];
              if($newComment->save()){
                $connection->createCommand(
                  "update ".RequestDiscussion::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by,trashed=:trashed,trashed_at=:trashed_at,trashed_by=:trashed_by where id=:id",
                  [
                    ':created_at'=>$discussion['created_at'],
                    ':created_by'=>$discussion['created_by'],
                    ':updated_at'=>$discussion['updated_at'],
                    ':updated_by'=>$discussion['updated_by'],
                    ':trashed'=>$discussion['trashed'],
                    ':trashed_at'=>$discussion['trashed_at'],
                    ':trashed_by'=>$discussion['trashed_by'],
                    ':id'=>$newComment->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($newComment->getErrors());echo '</pre>';
                die();
              }
            }
          }
          //Boat Assignment
          $boatAssignments=UserRequestsBoatAssign::find()->where(['request_id'=>$result['id']])->asArray()->all();
          if($boatAssignments!=null){
            foreach($boatAssignments as $boatAssignment){
              $logEntry=new LogUserRequestToBoatAssign;
              $logEntry->request_type='boatbooking';
              $logEntry->request_id=$userRequest->id;
              $logEntry->booking_id=$boatAssignment['booking_id'];
              $logEntry->city_id=$boatAssignment['city_id'];
              $logEntry->marina_id=$boatAssignment['marina_id'];
              $logEntry->boat_id=$boatAssignment['boat_id'];
              $logEntry->date=$boatAssignment['date'];
              $logEntry->time_id=$boatAssignment['time_id'];
              if($logEntry->save()){
                $connection->createCommand(
                  "update ".LogUserRequestToBoatAssign::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                  [
                    ':created_at'=>$boatAssignment['created_at'],
                    ':created_by'=>$boatAssignment['created_by'],
                    ':updated_at'=>$boatAssignment['updated_at'],
                    ':updated_by'=>$boatAssignment['updated_by'],
                    ':id'=>$logEntry->id
                  ]
                )->execute();
              }else{
                echo '<pre>';print_r($logEntry->getErrors());echo '</pre>';
                die();
              }
            }
          }
        }
      }
      echo '<script>window.location.href="'.Url::to(['user-boat-booking-requests','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to operations";
      echo '<script>window.location.href="'.Url::to(['operations-fix-rec/create-staff']).'"</script>';

    }
    exit;
  }

  public function actionMembersList()
  {
    $this->checkAdmin();
    $searchModel = new UserSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('users', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionTestEmail()
  {
    Yii::$app->mailer->compose()
      ->setFrom([Yii::$app->params['mdEmail'] => Yii::$app->params['siteName']])
      ->setTo("malick.naeem@gmail.com")
      ->setSubject('Test Email')
      ->setHtmlBody("Hello World")
      ->setTextBody("Hello World")
      ->send();
  }
}
