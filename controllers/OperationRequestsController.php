<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\RequestService;
use app\models\RequestServiceSearch;
use app\models\RequestBreakDown;
use app\models\RequestBreakDownSearch;
use app\models\RequestInspection;
use app\models\RequestInspectionSearch;

class OperationRequestsController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['service','inspection','inspection-detail'],
        'rules' => [
          [
            'actions' => ['service','inspection','inspection-detail'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Lists all Requests models.
  * @return mixed
  */
  public function actionService()
  {
    $this->checkAdmin();

    $searchModel = new RequestServiceSearch();
    if(Yii::$app->user->identity->user_type!=1){
      $searchModel->status=0;
      $searchModel->created_by=Yii::$app->user->identity->id;
    }
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('service', [
      'dataProvider' => $dataProvider,
      'searchModel' => $searchModel,
    ]);
  }

  /**
  * Lists all Requests models.
  * @return mixed
  */
  public function actionBreakDown()
  {
    $this->checkAdmin();
    $searchModel = new RequestBreakDownSearch();
    if(Yii::$app->user->identity->user_type!=1){
      $searchModel->status=0;
      $searchModel->created_by=Yii::$app->user->identity->id;
    }
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('break_down', [
      'dataProvider' => $dataProvider,
      'searchModel' => $searchModel,
    ]);
  }

  /**
  * Lists all Requests models.
  * @return mixed
  */
  public function actionInspection()
  {
    $this->checkAdmin();

    $searchModel = new RequestInspectionSearch();
    if(Yii::$app->user->identity->user_type!=1){
      $searchModel->created_by=Yii::$app->user->identity->id;
    }
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('inspection', [
      'dataProvider' => $dataProvider,
      'searchModel' => $searchModel,
    ]);
  }

  public function actionInspectionDetail($id)
  {
    $this->checkAdmin();
    $this->layout = 'ajax';
    $model=$this->findInspectionModel($id);
    if($model!=null){
      return $this->renderAjax('_inspection_items_detail', [
        'model' => $model,
      ]);
    }
  }

  /**
  * Finds the RequestService model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return RequestService the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findServiceModel($id)
  {
    if (($model = RequestService::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Finds the RequestBreakDown model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return RequestBreakDown the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findBreakDownModel($id)
  {
    if (($model = RequestBreakDown::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Finds the RequestBreakDown model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return RequestBreakDown the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findInspectionModel($id)
  {
    if (($model = RequestInspection::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
