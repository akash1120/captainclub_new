<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\ReportForm;
use app\models\MemberBehaviourReport;
use app\models\ReportSearch;
use app\models\UserPaymentSearch;
use app\models\WithdrawPaytabsForm;
use app\models\BookingSearch;
use app\models\ClaimOrderReport;

class ReportController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['balance','manual-deposits','manual-charges','booking'],
        'rules' => [
          [
            'actions' => ['balance','manual-deposits','manual-charges','booking'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Booking Stats Report
  * @return mixed
  */
  public function actionBookingStats($date=null)
  {
    $this->checkSuperAdmin();
    $model=new ReportForm([],$date);

    return $this->render('booking_stats', [
      'model' => $model,
    ]);
  }

  /**
  * Member behavior report
  * @return mixed
  */
  public function actionMemberBehaviour($status_type='be',$status=2,$date=null)
  {
    $this->checkSuperAdmin();
    $model=new MemberBehaviourReport([],$date,$status_type,$status);
    $dataProvider = $model->search([]);

    return $this->render('member_behaviour', [
      'model' => $model,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Boats models.
  * @return mixed
  */
  public function actionAccounts()
  {
    $this->checkSuperAdmin();
    $searchModel = new ReportSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('accounts', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Boats models.
  * @return mixed
  */
  public function actionBalance()
  {
    $this->checkSuperAdmin();
    $searchModel = new ReportSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('balance', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Boats models.
  * @return mixed
  */
  public function actionManualDeposits()
  {
    $this->checkSuperAdmin();
    $searchModel = new ReportSearch();
    $dataProvider = $searchModel->searchManualDeposits(Yii::$app->request->queryParams);

    return $this->render('manual_deposits', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Manual Charges.
  * @return mixed
  */
  public function actionManualCharges()
  {
    $this->checkSuperAdmin();
    $searchModel = new ReportSearch();
    $dataProvider = $searchModel->searchManualCharges(Yii::$app->request->queryParams);

    return $this->render('manual_charges', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Boats models.
  * @return mixed
  */
  public function actionOldCreditLog()
  {
    $this->checkSuperAdmin();
    $searchModel = new UserPaymentSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('old_credit_log', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Withdraw from Paytabs
  * @param integer $id
  * @return mixed
  */
  public function actionPaytabsWithdraw()
  {
    $this->checkSuperAdmin();
    $model = new WithdrawPaytabsForm;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
              }
            }
          }
        }
      }
      exit;
    }
    return $this->renderAjax('withdraw_form', [
      'model' => $model,
    ]);
  }

  /**
  * Lists all Bookings.
  * @return mixed
  */
  public function actionBooking()
  {
    $this->checkSuperAdmin();
    $searchModel = new BookingSearch();
    $dataProvider = $searchModel->searchReport(Yii::$app->request->queryParams);

    return $this->render('booking', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionClaims($port_id=1254)
  {
		$this->checkSuperAdmin();
    if($port_id==null)$port_id=Yii::$app->params['emp_id'];

    $searchModel = new ClaimOrderReport;
    $searchModel->port_id = $port_id;

    if($port_id==Yii::$app->params['emp_id']){
      $dataProvider = $searchModel->searchForEmp(Yii::$app->request->queryParams);
    }else{
      $dataProvider = $searchModel->searchForOther(Yii::$app->request->queryParams);
    }
    $dataProvider->pagination->pageSize = 100;

    return $this->render('claim', [
      'port_id' => $port_id,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }
}
