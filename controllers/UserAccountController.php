<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\AddUserCredit;
use app\models\ChargeUserCredit;
use app\models\AddSecurityDeposit;
use app\models\DeductSecurityDeposit;
use app\models\UserPaymentSearch;
use app\models\UserAccountsSearch;
use app\models\UserSecurityDepositSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* UserAccountController implements the CRUD actions for User Payments.
*/
class UserAccountController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'add-credit' => ['POST'],
          'charge-credit' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists Fuel Credit Log for User models.
  * @return mixed
  */
  public function actionCreditLog($id)
  {
    $this->checkSuperAdmin();
    $model=$this->findModel($id);
    $searchModel = new UserPaymentSearch();
    $searchModel->user_id=$id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('old_credit_history', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists Fuel Credit Log for User models.
  * @return mixed
  */
  public function actionAccountStatement($id)
  {
    $this->checkSuperAdmin();
    $model=$this->findModel($id);
    $searchModel = new UserAccountsSearch();
    $searchModel->user_id=$id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('account_statement', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

	/**
	* Renew Membership
	* @param integer $id
	* @return mixed
	*/
	public function actionAddCredit($id)
	{
		$this->checkLogin();
		$model = new AddUserCredit;
		$model->user_id = $id;
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'success';
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
							}
						}
					}
				}
			}
			exit;
		}
    return $this->renderAjax('add_credit_form', [
      'model' => $model,
    ]);
	}

	/**
	* Charge Member
	* @param integer $id
	* @return mixed
	*/
	public function actionChargeCredit($id)
	{
		$this->checkLogin();
		$model = new ChargeUserCredit;
		$model->user_id = $id;
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'success';
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
							}
						}
					}
				}
			}
			exit;
		}
    return $this->renderAjax('charge_form', [
      'model' => $model,
    ]);
	}

  /**
  * Lists Security Deposit Log for User models.
  * @return mixed
  */
  public function actionSecurityDepositLog($id)
  {
    $this->checkSuperAdmin();
    $model=$this->findModel($id);
    $searchModel = new UserSecurityDepositSearch();
    $searchModel->user_id=$id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('security_deposits_statement', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

	/**
	* Add Security Deposit to Member
	* @param integer $id
	* @return mixed
	*/
	public function actionAddSecurityDeposit($id)
	{
		$this->checkLogin();
		$model = new AddSecurityDeposit;
		$model->user_id = $id;
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'success';
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
							}
						}
					}
				}
			}
			exit;
		}
    return $this->renderAjax('add_security_deposit_form', [
      'model' => $model,
    ]);
	}

	/**
	* Deduct Security Deposit for Member
	* @param integer $id
	* @return mixed
	*/
	public function actionDeductSecurityDeposit($id)
	{
		$this->checkLogin();
		$model = new DeductSecurityDeposit;
		$model->user_id = $id;
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'success';
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
							}
						}
					}
				}
			}
			exit;
		}
    return $this->renderAjax('deduct_security_deposit_form', [
      'model' => $model,
    ]);
	}

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return User the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
