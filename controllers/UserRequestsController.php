<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\RequestsSearch;
use app\models\RequestDiscussion;
use app\models\RequestDiscussionSearch;
use app\models\UserFreezeRequest;
use app\models\UserBookingRequest;
use app\models\UserNightdriveTrainingRequest;
use app\models\UserNightdriveTrainingRequestSearch;
use app\models\UserNightdriveTrainingRequestRemarks;
use app\models\UserCityUpgradeRequest;
use app\models\UserOtherRequest;
use app\models\LogUserRequestToBoatAssign;
use app\models\EmailTemplate;

use app\models\Booking;
use app\models\WaitingListCaptain;
use app\models\WaitingListSportsEquipment;
use app\models\WaitingListBbq;
use app\models\WaitingListLateArrival;
use app\models\WaitingListOvernightCamping;
use app\models\WaitingListEarlyDeparture;

class UserRequestsController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index'],
        'rules' => [
          [
            'actions' => ['index'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * @return string
  */
  public function actionIndex($listType='active')
  {
    $this->checkSuperAdmin();
    $searchModel = new RequestsSearch();
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Lists all Night drive requests.
   * @return mixed
   */
  public function actionNightDriveTraining($listType='active')
  {
    $this->checkSuperAdmin();
    $searchModel = new RequestsSearch();
    $searchModel->item_type='nightdrivetraining';
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Approves an existing CaptainRequest model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionTrainingDone($item_type,$id)
  {
    $this->checkSuperAdmin();
    $reqModel=$this->findRequestModel($item_type,$id);
    $model = new UserNightdriveTrainingRequestRemarks;
    $model->user_request_id = $reqModel->id;
    $model->training_date = date("Y-m-d");
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $connection = \Yii::$app->db;
        $connection->createCommand("update ".$reqModel->tableName()." set status=:status where id=:id",[':status'=>"-1",':id'=>$reqModel->id])->execute();

        Yii::$app->mailer->compose(['html' => 'adminTrainingDoneNotification-html', 'text' => 'adminTrainingDoneNotification-text'], ['model' => $model])
          ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
          ->setTo(Yii::$app->params['mdEmail'])
          ->setSubject('Pending Request - ' . Yii::$app->params['siteName'])
          ->send();

        echo 'success';
      } else {
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
              }
            }
          }
        }
      }
      exit;
    }

    return $this->renderAjax('_training_done_form', [
      'model' => $model,
    ]);
  }

  /**
  * Lists all Discussion models.
  * @return mixed
  */
  public function actionDiscussion($item_type,$id)
  {
    $this->checkSuperAdmin();
    $model = new RequestDiscussion();
    $model->request_type=$item_type;
    $model->request_id=$id;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Comment saved successfully'));
        return $this->redirect(['discussion','item_type'=>$item_type,'id'=>$id]);
      } else {
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    $searchModel = new RequestDiscussionSearch();
    $searchModel->request_type=$item_type;
    $searchModel->request_id=$id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('discussion', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }


  /**
  * Updates an existing RequestService model.
  * @param integer $id
  * @return mixed
  */
  public function actionRequestAction($s,$item_type,$id)
  {
    $this->checkSuperAdmin();
    $smsMsg='';
    $model = $this->findRequestModel($item_type,$id);
    if($s==1){
      $templateId=Yii::$app->appHelperFunctions->getSetting('metEmail');
      $template=EmailTemplate::findOne($templateId);
      if($template!=null){
        $model->email_message=$template->template_text;
      }
      $active_show_till=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")+3),date("Y")));
    }else if($s==2){
      $templateId=Yii::$app->appHelperFunctions->getSetting('notmetEmail');
      $template=EmailTemplate::findOne($templateId);
      if($template!=null){
        $model->email_message=$template->template_text;
      }
      $active_show_till=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")-1),date("Y")));
    }else if($s==3){
      $templateId=Yii::$app->appHelperFunctions->getSetting('requestCancelledEmail');
      $template=EmailTemplate::findOne($templateId);
      if($template!=null){
        $model->email_message=$template->template_text;
      }
      $active_show_till=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")-1),date("Y")));
    }
    if ($model->load(Yii::$app->request->post())) {
      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".$model->tableName()." set status=:status".($active_show_till!=null ? ",active_show_till='".$active_show_till."'" : '').",remarks=:remarks,email_message=:email_message,admin_action_date=:admin_action_date where id=:id",
        [
          ':status'=>$s,
          ':remarks'=>$model->remarks,
          ':email_message'=>$model->email_message,
          ':admin_action_date'=>date("Y-m-d H:i:s"),
          ':id'=>$model->id
        ]
      )->execute();
      if($model->email_message!=null){
        $requestDetail=Yii::$app->appHelperFunctions->getRequestDetail($item_type,$model['id']);

        $trainingMrina='';
        if($item_type=='nightdrivetraining'){
          $ndReqRemarks=UserNightdriveTrainingRequestRemarks::find()->where(['user_request_id'=>$model->id])->one();
          if($ndReqRemarks!=null){
            $trainingMrina=$ndReqRemarks->marina->name;
          }
        }

        $template=new EmailTemplate;
        $vals = [
          '{adminRemarks}' => '<strong>Remarks:</strong> '.$model->remarks,
          '{requestDetail}' => $requestDetail,
          '{trainingMrina}' => $trainingMrina,
        ];
        $htmlBody=$template->searchReplace($model->email_message,$vals,'html');
        $textBody=$template->searchReplace($model->email_message,$vals,'text');

        $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => nl2br($htmlBody), 'textBody' => $textBody])
        ->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
        ->setSubject('Request Response - ' . Yii::$app->params['siteName'])
        ->setTo($model->member->email)
        ->send();
      }

      if($s==1){
    		if(
    			$item_type=='waitingcaptain'
    			|| $item_type=='waitingequipment'
    			|| $item_type=='waitingbbq'
    			|| $item_type=='waitingovernightcamping'
    			|| $item_type=='waitingearlydeparture'
    			|| $item_type=='waitinglatearrival'
    		){
          $this->updateBookingRequest($item_type,$model->booking_id);
        }

$smsMsg="Dear Captain\n
Request on \"".Yii::$app->formatter->asDate($model->created_at,"php:d/m/Y")."\" was successfully MET.
Please check your Email or booking system for more details.\n
Remarks: ".$model->remarks."\n
M: ".Yii::$app->params['smsSigNumber']."
E: ".Yii::$app->params['smsSigEmail']."";
      }else{
$smsMsg="Dear Captain\n
Request on \"".Yii::$app->formatter->asDate($model->created_at,"php:d/m/Y")."\" was Not MET.
Please check your Email or booking system for more details.
We sincerely apologies.\n
Remarks: ".$model->remarks."\n
M: ".Yii::$app->params['smsSigNumber']."
E: ".Yii::$app->params['smsSigEmail']."";
      }
      if($model->member->profileInfo->profile_updated==1 && $model->member->profileInfo->mobile!=null && $model->member->profileInfo->mobile!=''){
        $mobileNumber=Yii::$app->helperFunctions->fullMobileNumber($model->member->profileInfo->mobile);
        if($mobileNumber!=null && $mobileNumber!=''){
          Yii::$app->helperFunctions->sendSms($mobileNumber,$smsMsg);
        }
      }
      echo 'success';
      exit;
    }
    return $this->renderAjax('admin_action_form', [
      'model' => $model,
    ]);
  }

  public function updateBookingRequest($type,$id)
  {
    $this->checkSuperAdmin();
		if(
			$type=='waitingcaptain'
			|| $type=='waitingequipment'
			|| $type=='waitingbbq'
			|| $type=='waitingovernightcamping'
			|| $type=='waitingearlydeparture'
			|| $type=='waitinglatearrival'
		){
      $booking=Booking::findOne($id);
      if($booking!=null){
        $connection = \Yii::$app->db;
        $colName='';
        if($type=='waitingcaptain')$colName='captain';
        if($type=='waitingequipment')$colName='equipment';

        if($type=='waitingbbq')$colName='bbq';
        if($type=='waitingovernightcamping')$colName='overnight_camping';
        if($type=='waitingearlydeparture')$colName='early_departure';
        if($type=='waitinglatearrival')$colName='late_arrival';
        if($colName!=''){
          if($type=='waitingcaptain'){
            $captain_type=Yii::$app->bookingHelperFunctions->getCaptainStatus($booking->member,$booking);
            $connection->createCommand(
              "update ".Booking::tableName()." set captain=1,captain_type='".$captain_type."' where id=:id",
              [
                ':id'=>$booking->id
              ]
            )->execute();
          }elseif($type=='waitingequipment'){
            $waitingList=WaitingListSportsEquipment::findOne(['booking_id'=>$booking->id]);
            $sport_eqp_id=$waitingList->equipment_id;
            $connection->createCommand(
              "update ".Booking::tableName()." set sport_eqp_id='".$sport_eqp_id."' where id=:id",
              [
                ':id'=>$booking->id
              ]
            )->execute();
          }else{
            $connection->createCommand(
              "update ".Booking::tableName()." set ".$colName."=1 where id=:id",
              [
                ':id'=>$booking->id
              ]
            )->execute();
          }
          //$booking->metRequest($colName);
        }
      }
    }
  }

  /**
  * Assign a boat upon user request
  * @return mixed
  */
  public function actionBoatAssign($item_type,$id)
  {
    $this->checkSuperAdmin();
    $requestModel=$this->findRequestModel($item_type,$id);
    if($requestModel!=null){
      $model = new LogUserRequestToBoatAssign();
      $model->request_type = $item_type;
      $model->request_id = $requestModel['id'];
      $model->city_id = $requestModel['city_id'];
      $model->date = $requestModel['requested_date'];
      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          echo 'success';
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  echo $val;
                  die();
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('request_boat_assign', [
        'requestModel' => $requestModel,
        'item_type' => $item_type,
        'model' => $model,
      ]);
    }else{
      throw new NotFoundHttpException('No request found!');
    }
  }

  /**
  * Finds the Request model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return User the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findRequestModel($item_type,$id)
  {
    if($item_type=='freeze'){
      $model = UserFreezeRequest::findOne($id);
    }
    if($item_type=='boatbooking'){
      $model = UserBookingRequest::findOne($id);
    }
    if($item_type=='nightdrivetraining'){
      $model = UserNightdriveTrainingRequest::findOne($id);
    }
    if($item_type=='upgradecity'){
      $model = UserCityUpgradeRequest::findOne($id);
    }
    if($item_type=='other'){
      $model = UserOtherRequest::findOne($id);
    }
    if($item_type=='waitingcaptain'){
      $model = WaitingListCaptain::findOne(['id'=>$id]);
    }
    if($item_type=='waitingequipment'){
      $model = WaitingListSportsEquipment::findOne(['id'=>$id]);
    }
    if($item_type=='waitingbbq'){
      $model = WaitingListBbq::findOne(['id'=>$id]);
    }
    if($item_type=='waitinglatearrival'){
      $model = WaitingListLateArrival::findOne(['id'=>$id]);
    }
    if($item_type=='waitingovernightcamping'){
      $model = WaitingListOvernightCamping::findOne(['id'=>$id]);
    }
    if($item_type=='waitingearlydeparture'){
      $model = WaitingListEarlyDeparture::findOne(['id'=>$id]);
    }

    if ($model !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
