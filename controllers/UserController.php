<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use app\models\UserNoteSearch;
use app\models\SecurityDepositForm;
use app\models\UserFeedback;
use app\models\BookingSearch;
use app\models\RequestsSearch;
use app\models\UserLicenseSearch;
use app\models\Contract;
use app\models\Newsletter;
use app\models\UserPasswordResetForm;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* UserController implements the CRUD actions for User model.
*/
class UserController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all User models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new UserSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    $mailModel = new Newsletter;
    $mailModel->keyword = $searchModel->keyword;
    $mailModel->credit_balance = $searchModel->credit_balance;
    $mailModel->is_licensed = $searchModel->is_licensed;
    $mailModel->package_idz = $searchModel->package_name!=null ? implode(",",$searchModel->package_name) : null;
    //$mailModel->city_idz = $searchModel->city_name!=null ? implode(",",$searchModel->city_name) : null;
    $mailModel->status = $searchModel->status;
    $mailModel->tasks = $searchModel->tasks;
    $mailModel->expiry_month = $searchModel->expiry_month;
    if ($mailModel->load(Yii::$app->request->post())) {
      if($mailModel->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Mail qeued successfully'));
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
      }else{
        if($mailModel->hasErrors()){
          foreach($mailModel->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'mailModel' => $mailModel,
    ]);
  }

  /**
  * Creates a new User model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new User();
    $model->user_type=0;
    $model->permission_group_id=2;
    $model->status=20;
    $model->scenario = 'new';

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        //$this->updateContract($model);
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }


    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing User model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkSuperAdmin();
    $model = $this->findModel($id);
    $oldEmail=$model->email;
    $model->scenario = 'update';
    $model->oldfile=$model->image;
    //$model->oldlicensefile=$model->license_image;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        if($oldEmail!=$model->email){
          $model->sendEmailChangedAlert();
        }
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    $licenseSearchModel = new UserLicenseSearch();
    $licenseSearchModel->user_id=$id;
    $licenseDataProvider = $licenseSearchModel->search(Yii::$app->request->queryParams);

    $searchModel = new UserNoteSearch();
    $searchModel->user_id=$id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->pagination->pageSize=5;

    return $this->render('update', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,

      'licenseSearchModel' => $licenseSearchModel,
      'licenseDataProvider' => $licenseDataProvider,
      'model' => $model,
    ]);
  }

  public function actionResetUserPassword($id)
  {
    $this->checkSuperAdmin();
    $modelUser=$this->findModel($id);
    $model=new UserPasswordResetForm;
    $model->user_id=$modelUser->id;
    if ($model->load(Yii::$app->request->post())) {
      $msg=[];
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Password reset successfully')];
        echo json_encode($msg);
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                echo json_encode($msg);
                exit;
              }
            }
          }
        }
      }

    }

    return $this->renderAjax('reset_user_password', [
      'model' => $model,
    ]);
  }

  /**
  * Mark Dead Membership
  * @param integer $id
  * @return mixed
  */
  public function actionMarkDead($id)
  {
    $this->checkSuperAdmin();
    $model=$this->findModel($id);

    $connection = \Yii::$app->db;
    $connection->createCommand("update ".User::tableName()." set status=:user_status where id=:user_id",[':user_status'=>5,':user_id'=>$model->id])->execute();
    echo 'dead';
    exit;
  }

  /**
  * Feedback from user
  * @param integer $id
  * @return mixed
  */
  public function actionSecurityDeposit($id)
  {
    $this->checkSuperAdmin();
    $model = new SecurityDepositForm();
    $model->user_id=$id;
    $model->amount=$model->user->profileInfo->security_deposit;
    $model->deposit_type=$model->user->profileInfo->deposit_type;
    if ($model->load(Yii::$app->request->post())) {
      if($model->send()){
        echo 'success';
      }else{
        $n=1;
        foreach($model->getErrors() as $error){
          foreach($error as $key=>$val){
            echo $val.($n>1 ? ', ' : '');
          }
          $n++;
        }
      }
      exit;
    }
    return $this->renderAjax('security_deposit_form', [
      'model' => $model,
    ]);
  }

  /**
  * Lists all Members Booking models.
  * @return mixed
  */
  public function actionBookings($id,$contract_id=null,$listType='future')
  {
    $this->checkSuperAdmin();
    $model = $this->findModel($id);

    if($contract_id==null){
      $contract_id=$model->active_contract_id;
    }

    $contract=Contract::findOne($contract_id);

    $searchModel = new BookingSearch();
    $searchModel->contract_id=$contract_id;
    $searchModel->user_id=$id;
    $searchModel->listType=$listType;
    if($contract!=null){
      $searchModel->booking_date=$contract->start_date.' - '.$contract->end_date;
    }
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('bookings', [
      'model' => $model,
      'contract' => $contract,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Members Booking models.
  * @return mixed
  */
  public function actionRequests($id,$listType='future')
  {
    $this->checkSuperAdmin();
    $model = $this->findModel($id);
    $searchModel = new RequestsSearch();
    $searchModel->user_id=$id;
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('requests', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new User;
    return $model;
  }

  /**
  * Lists all User models.
  * @return mixed
  */
  public function actionSearch($id=null)
  {
    $this->checkSuperAdmin();
    $model = new User;
    if($id!=null){
      $model = $this->findModel($id);
    }

    return $this->render('search', [
      'model' => $model,
    ]);
  }

  /**
  * Lists all User models.
  * @return mixed
  */
  public function actionUpdateInfo($id=null)
  {
    $this->checkSuperAdmin();
    $searchModel = new UserSearch();
    $searchModel->user_type=0;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('update_info', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionUpdateMemberInfo($id)
  {
    $this->checkSuperAdmin();
    $model = $this->findModel($id);
    $model->oldfile=$model->image;
    $model->joining_date=$model->profileInfo->joining_date;
    $model->membership_number=$model->profileInfo->membership_number;
    //$model->oldlicensefile=$model->license_image;
    //$model->license_expiry=$model->profileInfo->license_expiry;
    //$model->license_image=$model->profileInfo->license_image;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }

    $licenseSearchModel = new UserLicenseSearch();
    $licenseSearchModel->user_id=$id;
    $licenseDataProvider = $licenseSearchModel->search(Yii::$app->request->queryParams);

    return $this->renderAjax('_basic_info_form', [
      'model' => $model,
      'licenseSearchModel' => $licenseSearchModel,
      'licenseDataProvider' => $licenseDataProvider,
    ]);
  }

  public function actionRequest($id)
  {
    $this->checkSuperAdmin();

    $model = $this->findModel($id);
    return $this->renderAjax('member_request_selection',['model'=>$model]);
  }

	/**
	* Lists Package History for User models.
	* @return mixed
	*/
	public function actionFreezedMembers()
	{
		$this->checkSuperAdmin();
		$searchModel = new UserSearch();
		$dataProvider = $searchModel->searchFreezed(Yii::$app->request->queryParams);

		return $this->render('freezed_members', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return User the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
