<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Contract;
use app\models\ContractSearch;
use app\models\RenewForm;
use app\models\ContractChangeHistory;
use app\models\ContractChangeHistorySearch;
use app\models\UpdateContractForm;
use app\models\StatusForm;
use app\models\ContractMember;
use app\models\UserStatusReason;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* ContractController implements the CRUD actions for Contract model.
*/
class ContractController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

	/**
	* Renew Membership
	* @param integer $id
	* @return mixed
	*/
	public function actionRenew($id)
	{
		$this->checkAdmin();
		$model = new RenewForm($id);
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'success';
				exit;
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
							}
						}
					}
				}
				exit;
			}
		}
		return $this->renderAjax('_renew_form', [
			'model' => $model,
		]);
	}

	/**
	* Update Contract
	* @param integer $id
	* @return mixed
	*/
	public function actionUpdate($id)
	{
		$this->checkAdmin();
		$contractmodel = $this->findModel($id);
		$model = new UpdateContractForm($id);
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'success';
				exit;
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
							}
						}
					}
				}
				exit;
			}
		}
		return $this->renderAjax('_update_form', [
			'contractmodel' => $contractmodel,
			'model' => $model,
		]);
	}

  /**
  * Lists all Contract models.
  * @return mixed
  */
  public function actionIndex($id=null)
  {
    $this->checkAdmin();
    $model=$this->findUserModel($id);
    $searchModel = new ContractSearch();
    $searchModel->user_id=$id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Lists ContractChangeHistory model.
   */
	public function actionUpdateHistory($user_id,$cid)
	{
		$this->checkSuperAdmin();
		$modelContract = $this->findModel($cid);
		$model = $this->findUserModel($user_id);
		$searchModel = new ContractChangeHistorySearch();
		$searchModel->contract_id=$cid;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('change_history', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'modelContract' => $modelContract,
				'model' => $model,
		]);
	}

	/**
	* Cancel Membership
	* @param integer $id
	* @return mixed
	*/
	public function actionCancel($id)
	{
		$this->checkSuperAdmin();

		$model = new StatusForm($id);
		$model->status = 3;
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'canceled';
				exit;
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
								die();
							}
						}
					}
				}
			}
		}
		return $this->renderAjax('_hold_cancel_form', [
			'model' => $model,
		]);
	}

	/**
	* Hold Membership
	* @param integer $id
	* @return mixed
	*/
	public function actionHold($id)
	{
		$this->checkSuperAdmin();

		$model = new StatusForm($id);
		$model->status = 2;
		if ($model->load(Yii::$app->request->post())) {
			if($model->save()){
				echo 'holded';
				exit;
			}else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								echo $val;
								die();
							}
						}
					}
				}
			}
		}
		return $this->renderAjax('_hold_cancel_form', [
			'model' => $model,
		]);
	}

	/**
	* Resume Membership
	* @param integer $id
	* @return mixed
	*/
	public function actionResume($id)
	{
		$this->checkSuperAdmin();
		$model=$this->findUserModel($id);

		$connection = \Yii::$app->db;
		$connection->createCommand("update ".User::tableName()." set status=:user_status where id=:user_id",[':user_status'=>1,':user_id'=>$model->id])->execute();
		$activeContract=$model->activeContract;
		if($activeContract!=null){
			$connection->createCommand("update ".ContractMember::tableName()." set status=:user_status where contract_id=:contract_id and user_id=:user_id",[':user_status'=>1,':contract_id'=>$activeContract->id,':user_id'=>$model->id])->execute();
		}
    echo 'resumed';
    exit;
	}

	/**
	* Resume Membership
	* @param integer $id
	* @return mixed
	*/
	public function actionDelHold($user_id,$id)
	{
		$this->checkSuperAdmin();
		$model=$this->findUserModel($user_id);
    $request=UserStatusReason::find()->where(['user_id'=>$model->id,'id'=>$id,'status'=>2,'is_updated'=>0])->one();
    if($request!=null){
      $request->delete();
    }
    $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Hold request has been deleted')];
    echo json_encode($msg);
    exit;
	}

	/**
	* Resume Membership
	* @param integer $id
	* @return mixed
	*/
	public function actionDelCancel($user_id,$id)
	{
		$this->checkSuperAdmin();
		$model=$this->findUserModel($user_id);
    $request=UserStatusReason::find()->where(['user_id'=>$model->id,'id'=>$id,'status'=>3,'is_updated'=>0])->one();
    if($request!=null){
      $request->delete();
    }
    $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Cancel request has been deleted')];
    echo json_encode($msg);
    exit;
	}

  /**
  * Finds the Contract model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Contract the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Contract::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return User the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findUserModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
