<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserFeedback;
use app\models\UserFeedbackSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* UserFeedbackController implements the CRUD actions for User model.
*/
class UserFeedbackController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all User models.
  * @return mixed
  */
  public function actionIndex($id=null)
  {
    $this->checkAdmin();
    $model = $this->findUserModel($id);
    $searchModel = new UserFeedbackSearch();
    $searchModel->user_id=$id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new User model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate($id=null)
  {
    $this->checkSuperAdmin();
    $model = new UserFeedback();
    $model->user_id=$id;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
      }else{
        $n=1;
        foreach($model->getErrors() as $error){
          foreach($error as $key=>$val){
            echo $val.($n>1 ? ', ' : '');
          }
          $n++;
        }
      }
      exit;
    }
    return $this->renderAjax('_form', [
      'model' => $model,
    ]);
  }

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return User the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findUserModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
