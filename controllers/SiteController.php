<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\LoginForm;
use app\models\LoginFormOpr;
use app\models\ForgetPasswordForm;
use app\models\ResetPasswordForm;
use app\models\Marina;
use app\models\DiscountSearch;
use app\models\PackageCityAssignForm;
use app\models\BoatOptionsForm;
use app\models\BoatRequired;
use app\models\RequestsSearch;
use app\models\BoatSwap;
use app\models\Booking;
use app\models\OperationUserCityMarina;
use app\models\UserDevice;

class SiteController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','discount','logout'],
        'rules' => [
          [
            'actions' => ['index','discount','logout'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          //'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
  * {@inheritdoc}
  */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'app\components\helpers\MyErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  /**
  * Displays homepage.
  *
  * @return string
  */
  public function actionIndex($tab='dashboard',$city=null,$date=null,$dm=null)
  {
    $this->checkLogin();
    if($dm!=null && $dm==1){
      Yii::$app->getSession()->setFlash('success', Yii::t('app','Booking Deleted. Thank you'));
    }

    if(Yii::$app->user->identity->profileInfo->changed_pass==0 || (Yii::$app->user->identity->user_type==0 && Yii::$app->user->identity->profileInfo->profile_updated==0)){
      return $this->redirect(['account/confirm-profile']);
    }

    $modelOpr=OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
    if($modelOpr==null){
      $modelOpr = new OperationUserCityMarina();
      $modelOpr->user_id=Yii::$app->user->identity->id;
    }

    if ($modelOpr->load(Yii::$app->request->post())) {
      if($modelOpr->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index']);
      }else{
        if($modelOpr->hasErrors()){
          foreach($modelOpr->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }


    return $this->render('index',[
      'tab'=>$tab,
      'city'=>$city,
      'date'=>$date,
      'modelOpr'=>$modelOpr,
    ]);
  }

  public function actionBoatSwap()
  {
    $this->checkSuperAdmin();
    $model = new BoatSwap();
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('boat_swap', [
    'model' => $model,
    ]);
  }

  public function actionDiscount()
  {
    $this->checkLogin();
    $searchModel = new DiscountSearch();
    $searchModel->status=1;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->pagination->pageSize=12;

    return $this->render('discount', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionMyRequests($listType='active')
  {
    $this->checkLogin();
    $searchModel = new RequestsSearch();
    $searchModel->listType=$listType;
    $searchModel->user_id=Yii::$app->user->identity->id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('my_requests',[
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionAssignPackageCity()
  {
    $this->checkSuperAdmin();
    $model = new PackageCityAssignForm;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', 'Information saved successfully');
        return $this->redirect(['assign-package-city']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->setFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('/package/assign_package_city', [
      'model' => $model,
    ]);
  }

  public function actionBoatOptions($marina_id=null)
  {
    $this->checkSuperAdmin();
    if($marina_id==null)$marina_id=789;
    $model = new BoatOptionsForm;
    $model->marina_id = $marina_id;
    $marina=$this->findMarinaModel($marina_id);
    $model->marina_city_id=$marina->city_id;
    $model->marina_name=$marina->name;
    $model->marina_short_name=$marina->short_name;
    $model->marina_fuel_profit=$marina->fuel_profit;
    $model->marina_captains=$marina->captains;
    $model->marina_bbq=$marina->bbq;
    $model->marina_no_of_early_departures=$marina->no_of_early_departures;
    $model->marina_no_of_late_arrivals=$marina->no_of_late_arrivals;
    $model->marina_no_of_overnight_camps=$marina->no_of_overnight_camps;
    $model->marina_night_drive_limit=$marina->night_drive_limit;
    $model->marina_wake_boarding_limit=$marina->wake_boarding_limit;
    $model->marina_wake_surfing_limit=$marina->wake_surfing_limit;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', 'Information saved successfully');
        return $this->redirect(['boat-options']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->setFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('/boat/boat_options', [
      'model' => $model,
    ]);
  }

  /**
  * Login action.
  *
  * @return Response|string
  */
  public function actionLogin()
  {
    $this->layout='guest';
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    $model->password = '';
    return $this->render('login', [
      'model' => $model,
    ]);
  }

  /**
  * Login action.
  *
  * @return Response|string
  */
  public function actionOperationsLogin()
  {
    $this->layout='guest';
    Yii::$app->user->logout();
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginFormOpr();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    $model->password = '';
    return $this->render('operations_login', [
      'model' => $model,
    ]);
  }

  /**
  * Logout action.
  *
  * @return Response
  */
  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  /**
   * Forget Password action.
   *
   * @return Response
   */
  public function actionForgetPassword()
  {
    $this->layout='guest';
    $model = new ForgetPasswordForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->sendEmail()) {
        Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
        return $this->goHome();
      } else {
        Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
      }
    }

    return $this->render('forget_password', [
      'model' => $model,
    ]);
  }

  public function actionSendNotification()
  {
    $devices=UserDevice::find()->where(['user_id'=>668])->asArray()->all();
    if($devices!=null){

        $title = "Booking Reminder";
        $message = "Please confirm your booking of tomorrow";
        $actionUrl = 'https://booking.thecaptainsclub.ae/site/discount';
        $deviceIdz=[];
      foreach($devices as $device){
        $deviceIdz[]=$device['device_id'];
      }
      if($deviceIdz!=null){
        Yii::$app->helperFunctions->sendNewNotification($deviceIdz,$title,$message,$actionUrl,668,0);
      }
    }
  }

  /**
   * Reset Password action.
   *
   * @return Response
   */
  public function actionResetPassword($token)
  {
    $this->layout='guest';
    try {
      $model = new ResetPasswordForm($token);
    } catch (InvalidParamException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }
    if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
      Yii::$app->getSession()->setFlash('success', 'New password was saved.');
      return $this->goHome();
    }
    return $this->render('reset_password', [
      'model' => $model,
    ]);
  }

  public function actionEarlyDeparture($id)
  {
    $model = Booking::findOne(['id'=>$id,'early_departure'=>1]);
    if($model!=null){
      $this->layout='guest_full';
      return $this->render('/booking/early_departure_details',['model'=>$model]);
    }else{
      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }

  public function findMarinaModel($id)
  {
    $model = Marina::findOne(['id'=>$id]);
    if($model!=null){
      return $model;
    }else{
      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }
}
