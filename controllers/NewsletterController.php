<?php

namespace app\controllers;

use Yii;
use app\models\Newsletter;
use app\models\NewsletterSearch;
use app\models\NewsletterEmailSentSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
* NewsletterController implements the CRUD actions for Newsletter model.
*/
class NewsletterController extends DefController
{
  public function beforeAction($action)
  {
    $this->enableCsrfValidation = ($action->id !== "drop-upload"); // <-- here
    return parent::beforeAction($action);
  }
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Lists all Newsletter models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkLogin();
    $searchModel = new NewsletterSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * View an existing Newsletter model.
  * @param integer $id
  * @return mixed
  */
  public function actionView($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    $searchModel = new NewsletterEmailSentSearch();
    $searchModel->newsletter_id=$model->id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('view', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionDropUpload($type=null)
  {
    $allowedFileTypes=['jpg','JPG', 'jpeg','JPEG', 'png','PNG', 'gif','GIF', 'pdf','PDF', 'doc','DOC', 'docx','DOCX', 'xls','XLS', 'xlsx','XLSX'];
    $fileName = 'file';
    $uploadPath = Yii::$app->params['temp_abs_path'];

    if (isset($_FILES[$fileName])) {
      $file = UploadedFile::getInstanceByName($fileName);
      if (!empty($file)) {
        $pInfo=pathinfo($file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$allowedFileTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
          }else{
            if (getimagesize($file->tempName)) {
              // generate a unique file name
              if ($file->saveAs(Yii::$app->params['temp_abs_path'].$file->name)) {
                echo $file->name;
              }
            }else{
              if ($file->saveAs(Yii::$app->params['temp_abs_path'].$file->name)) {
                echo $file->name;
              }
            }
          }
        }
      }
    }
  }

  /**
  * Finds the Newsletter model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Newsletter the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Newsletter::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }
}
