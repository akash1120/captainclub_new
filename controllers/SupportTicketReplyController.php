<?php

namespace app\controllers;

use Yii;
use app\models\SupportTicket;
use app\models\SupportTicketSearch;
use app\models\SupportTicketAttachment;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* SupportTicketController implements the CRUD actions for SupportTicket model.
*/
class SupportTicketReplyController extends DefController
{
  /**
  * Creates a new Discount model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate($parent_id)
  {
    $this->checkSuperAdmin();
    $parentModel = $this->findParentModel($parent_id);
    $model = new SupportTicket();
    $model->scenario = 'reply';
    $model->parent_id=$parentModel->ref_id;
    $model->priority_id=0;
    $msg=[];
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','reply saved successfully')];
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
              }
            }
          }
        }
      }
    }
    echo json_encode($msg);
    exit;
  }

  /**
  * Updates an existing Discount model.
  * If update is successful, the browser will be redirected to the 'view' page.
  * @param integer $id
  * @return mixed
  */
  public function actionUpdate($parent_id=null,$id=null)
  {
    $this->checkSuperAdmin();
    $parentModel = $this->findParentModel($parent_id);
    $model = $this->findModel($id);
    $model->scenario = 'reply';

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['support-ticket/view','id'=>$parent_id]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', str_replace('"','\"',$val));
              }
            }
          }
        }
      }
    }
    return $this->render('update', [
      'model' => $model,
      'parentModel' => $parentModel,
    ]);
  }

  /**
  * Deletes an existing SupportTicket model.
  * If deletion is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  */
  public function actionDelete($id)
  {
    $this->checkSuperAdmin();
    $model=$this->findModel($id);
    $model->softDelete();
    echo "done";
    exit;
  }

  /**
  * Finds the SupportTicket model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return SupportTicket the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = SupportTicket::findOne(['rssid'=>$id])) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }

  /**
  * Finds the SupportTicket model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return SupportTicket the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findParentModel($id)
  {
    if (($model = SupportTicket::findOne(['rssid'=>$id])) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }
}
