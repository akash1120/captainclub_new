<?php

namespace app\controllers;

use Yii;
use app\models\BoatRequired;
use app\models\BoatRequiredSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* BoatRequiredController implements the CRUD actions for BoatRequired model.
*/
class BoatRequiredController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all BoatRequired models.
  * @return mixed
  */
  public function actionIndex($listType='future')
  {
    $this->checkAdmin();
    $searchModel = new BoatRequiredSearch();
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new BoatRequired model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new BoatRequired();
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('_form', [
      'model' => $model,
    ]);
  }

  /**
  * Finds the BoatRequired model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return BoatRequired the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = BoatRequired::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
