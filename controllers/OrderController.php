<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Order;
use app\models\UserAccounts;
use app\models\UserSavedCc;
use app\models\User;
use yii\helpers\Url;
use app\components\helpers\PayTabs;
use app\models\PaymentForm;

class OrderController extends DefController
{
	public function beforeAction($action)
	{
    $this->enableCsrfValidation = ($action->id !== "success" && $action->id !== "response");
    return parent::beforeAction($action);
  }

  /**
  * Payment Page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new PaymentForm();
    $model->credits=Yii::$app->helperFunctions->minimumCredits;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['url'=>Url::to(['pay-tabs','id'=>$model->order_id])];
        echo json_encode($msg);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>$val];
                echo json_encode($msg);
              }
            }
          }
        }
      }
      exit;
    }

    return $this->renderAjax('step1', [
      'model' => $model,
    ]);
  }

  /**
  * Displays Confirmation page.
  * @param integer $id
  * @return mixed
  */
  public function actionPayTabs($id=null)
  {
    $this->checkLogin();
    if($id==null){
      $alreadyOrder=Order::find()->select(['order_id'])->where(['user_id'=>Yii::$app->user->identity->id,'status'=>'pending'])->asArray()->one();
      if($alreadyOrder!=null){
        return $this->redirect(['pay-tabs','id'=>$alreadyOrder['order_id']]);
      }else{
        $model = new PaymentForm();
        $model->credits=1;
        if($model->save()){
          return $this->redirect(['pay-tabs','id'=>$model->order_id]);
        }
      }
    }
    $model=$this->findModel($id);
    return $this->render('paytabs', [
      'model' => $model,
    ]);
  }

  public function actionProcessPayment($id)
  {
    $order=$this->findModel($id);
    $savedInfo=UserSavedCc::find()->where(['user_id'=>Yii::$app->user->identity->id,'id'=>$order->use_saved_cc_id])->asArray()->one();
    if($savedInfo!=null){
      $paytabsOptions = Yii::$app->paytabsHelperFunctions->paytabOptions;
      list($firstName,$lastName)=explode(" ",$savedInfo['customer_name']);

      $customerEmail=$savedInfo['customer_email'];
      if($paytabsOptions['testMode']==1){
        $customerEmail='abc@accept.com';
      }

      $pt = new PayTabs($paytabsOptions['merchentEmail'], $paytabsOptions['secretKey']);
      $result = $pt->create_tokenized_payment(array(
        'merchant_email' => $paytabsOptions['merchentEmail'],
        'secret_key' => $paytabsOptions['secretKey'],
        'title' => $order->member->username,
        'cc_first_name' => $firstName,
        'cc_last_name' => $lastName,
        "order_id" => $order->order_id,
        "product_name" => $order->member->username,
        'customer_email' => $customerEmail,
        'amount' => $order->amount_payable,
        'pt_token' => $savedInfo['pt_token'],
        'pt_customer_email' => $savedInfo['customer_email'],
        'pt_customer_password' => $savedInfo['pt_customer_password'],
        'billing_shipping_details' => 'no',
        'currency' => $order->currency,
        'phone_number' => $savedInfo['customer_phone'],
      ));
      if($result->response_code=='100'){
        $order->response_code = $result->response_code;
        $order->trans_date = date("Y-m-d H:i:s");
        $order->transaction_id = $result->transaction_id;
        $order->amount = $order->amount_payable;
        $order->status = 'done';
        $order->save();

        $crtransaction=new UserAccounts;
        $crtransaction->user_id=$order->user_id;
        $crtransaction->order_id=$order->id;
        $crtransaction->trans_type='cr';
        $crtransaction->descp=$order->detail;
        $crtransaction->account_id=0;
        if($order->amount_payable==$order->amount){
          $crtransaction->amount=$order->qty;
          $crtransaction->profit=$order->profit;
        }
        $crtransaction->booking_id=0;
        $crtransaction->save();
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Payment completed successfully'));
        return $this->redirect(['site/index']);
      }else{
        $order->response_code = $result->response_code;
        $order->trans_date = date("Y-m-d H:i:s");
        $order->transaction_id = $result->transaction_id;
        $order->amount = $order->amount_payable;
        $order->status = 'rejected';
        $order->save();
        Yii::$app->getSession()->setFlash('error', Yii::t('app','Payment was rejected'));
        return $this->redirect(['site/index']);
      }
    }
  }

  /**
  * Displays a single AdminGroup model.
  * @param integer $id
  * @return mixed
  */
  public function actionResponse()
  {
    $this->checkLogin();
    if(Yii::$app->request->post()){
      $response = \Yii::$app->request->post();
      if ($response <> null && count($response) > 0) {
        if ($response['response_code'] == "100") {
          $order = $this->findModel($response['order_id']);
          $order->response_code = (isset($response['response_code'])) ? $response['response_code'] : "";
          $order->trans_date = (isset($response['trans_date'])) ? $response['trans_date'] : (isset($response['datetime']) ? isset($response['datetime']) : "");
          $order->transaction_id = (isset($response['transaction_id'])) ? $response['transaction_id'] : "";
          $order->amount = (isset($response['transaction_amount'])) ? $response['transaction_amount'] : "";
          $order->status = 'done';

          $order->customer_name = (isset($response['customer_name'])) ? $response['customer_name'] : "";
          $order->customer_email = (isset($response['customer_email'])) ? $response['customer_email'] : "";
          $order->customer_phone = (isset($response['customer_phone'])) ? $response['customer_phone'] : "";

          $order->last_4_digits = (isset($response['last_4_digits'])) ? $response['last_4_digits'] : "";
          $order->first_4_digits = (isset($response['first_4_digits'])) ? $response['first_4_digits'] : "";
          $order->card_brand = (isset($response['card_brand'])) ? $response['card_brand'] : "";

          $order->secure_sign = (isset($response['secure_sign'])) ? $response['secure_sign'] : "";
          $order->save();

          $crtransaction=new UserAccounts;
          $crtransaction->user_id=$order->user_id;
          $crtransaction->order_id=$order->id;
          $crtransaction->trans_type='cr';
          $crtransaction->descp=$order->detail;
          $crtransaction->account_id=0;
          if($order->amount_payable==$order->amount){
            $crtransaction->amount=$order->qty;
            $crtransaction->profit=$order->profit;
          }
          $crtransaction->booking_id=0;
          $crtransaction->save();

          $user=User::findOne($order->user_id);
          if($user!=null && $user->profileInfo->save_cc==1){
            $ccEmail=(isset($response['customer_email'])) ? $response['customer_email'] : "";
            $ccLast4Digits=(isset($response['last_4_digits'])) ? $response['last_4_digits'] : "";
            $ccFirst4Digits=(isset($response['first_4_digits'])) ? $response['first_4_digits'] : "";
            $ccCardBrand=(isset($response['card_brand'])) ? $response['card_brand'] : "";
            $ccPtToken=(isset($response['pt_token'])) ? $response['pt_token'] : "";
            $ccPtCustomerPassword=(isset($response['pt_customer_password'])) ? $response['pt_customer_password'] : "";
            if($ccPtToken!="" && $ccPtCustomerPassword!=""){
              $saveInfo=UserSavedCc::find()->where([
                'user_id'=>$user->id,
                'customer_email'=>$ccEmail,
                'last_4_digits'=>$ccLast4Digits,
                'first_4_digits'=>$ccFirst4Digits,
                'card_brand'=>$ccCardBrand,
                'pt_token'=>$ccPtToken,
                'pt_customer_password'=>$ccPtCustomerPassword,
              ]);
              if(!$saveInfo->exists()){
                $saveInfo=new UserSavedCc;
                $saveInfo->user_id=$user->id;
                $saveInfo->customer_name=(isset($response['customer_name'])) ? $response['customer_name'] : "";
                $saveInfo->customer_email=$ccEmail;
                $saveInfo->customer_phone=(isset($response['customer_phone'])) ? $response['customer_phone'] : "";
                $saveInfo->last_4_digits=$ccLast4Digits;
                $saveInfo->first_4_digits=$ccFirst4Digits;
                $saveInfo->card_brand=$ccCardBrand;
                $saveInfo->pt_token=$ccPtToken;
                $saveInfo->pt_customer_password=$ccPtCustomerPassword;
                $saveInfo->save();
              }
            }
          }

          Yii::$app->getSession()->setFlash('success', Yii::t('app','Payment completed successfully'));
          return $this->redirect(['site/index']);
        }else{
          $order = $this->findModel($response['order_id']);
          $order->response_code = (isset($response['response_code'])) ? $response['response_code'] : "";
          $order->trans_date = (isset($response['trans_date'])) ? $response['trans_date'] : "";
          $order->transaction_id = (isset($response['transaction_id'])) ? $response['transaction_id'] : "";
          $order->amount = (isset($response['transaction_amount'])) ? $response['transaction_amount'] : "";
          $order->status = 'cancelled';

          $order->last_4_digits = (isset($response['last_4_digits'])) ? $response['last_4_digits'] : "";
          $order->first_4_digits = (isset($response['first_4_digits'])) ? $response['first_4_digits'] : "";
          $order->card_brand = (isset($response['card_brand'])) ? $response['card_brand'] : "";

          $order->secure_sign = (isset($response['secure_sign'])) ? $response['secure_sign'] : "";
          $order->save();
          return $this->redirect(['rejected']);
        }
      }
    }
  }

  /**
  * Displays a single AdminGroup model.
  * @param integer $id
  * @return mixed
  */
  public function actionAdviceListener()
  {
    $this->checkLogin();
    if(Yii::$app->request->post()){
      $response = \Yii::$app->request->post();
      if ($response <> null && count($response) > 0) {
        if ($response['response_code'] == "100") {
          $order = $this->findModel($response['order_id']);

          $order->response_code = (isset($response['response_code'])) ? $response['response_code'] : "";
          $order->trans_date = (isset($response['trans_date'])) ? $response['trans_date'] : (isset($response['datetime']) ? isset($response['datetime']) : "");
          $order->transaction_id = (isset($response['transaction_id'])) ? $response['transaction_id'] : "";
          $order->amount = (isset($response['transaction_amount'])) ? $response['transaction_amount'] : "";
          $order->status = 'done';

          $order->customer_name = (isset($response['customer_name'])) ? $response['customer_name'] : "";
          $order->customer_email = (isset($response['customer_email'])) ? $response['customer_email'] : "";
          $order->customer_phone = (isset($response['customer_phone'])) ? $response['customer_phone'] : "";

          $order->last_4_digits = (isset($response['last_4_digits'])) ? $response['last_4_digits'] : "";
          $order->first_4_digits = (isset($response['first_4_digits'])) ? $response['first_4_digits'] : "";
          $order->card_brand = (isset($response['card_brand'])) ? $response['card_brand'] : "";

          $order->secure_sign = (isset($response['secure_sign'])) ? $response['secure_sign'] : "";
          $order->save();

          $crtransaction=new UserAccounts;
          $crtransaction->user_id=$order->user_id;
          $crtransaction->order_id=$order->id;
          $crtransaction->trans_type='cr';
          $crtransaction->descp=$order->detail;
          $crtransaction->account_id=0;
          if($order->amount_payable==$order->amount){
            $crtransaction->amount=$order->qty;
            $crtransaction->profit=$order->profit;
          }
          $crtransaction->booking_id=0;
          $crtransaction->save();

          $user=User::findOne($order->user_id);
          if($user!=null && $user->profileInfo->save_cc==1){
            $ccEmail=(isset($response['customer_email'])) ? $response['customer_email'] : "";
            $ccLast4Digits=(isset($response['last_4_digits'])) ? $response['last_4_digits'] : "";
            $ccFirst4Digits=(isset($response['first_4_digits'])) ? $response['first_4_digits'] : "";
            $ccCardBrand=(isset($response['card_brand'])) ? $response['card_brand'] : "";
            $ccPtToken=(isset($response['pt_token'])) ? $response['pt_token'] : "";
            $ccPtCustomerPassword=(isset($response['pt_customer_password'])) ? $response['pt_customer_password'] : "";
            if($ccPtToken!="" && $ccPtCustomerPassword!=""){
              $saveInfo=UserSavedCc::find()->where([
                'user_id'=>$user->id,
                'customer_email'=>$ccEmail,
                'last_4_digits'=>$ccLast4Digits,
                'first_4_digits'=>$ccFirst4Digits,
                'card_brand'=>$ccCardBrand,
                'pt_token'=>$ccPtToken,
                'pt_customer_password'=>$ccPtCustomerPassword,
              ]);
              if(!$saveInfo->exists()){
                $saveInfo=new UserSavedCc;
                $saveInfo->user_id=$user->id;
                $saveInfo->customer_name=(isset($response['customer_name'])) ? $response['customer_name'] : "";
                $saveInfo->customer_email=$ccEmail;
                $saveInfo->customer_phone=(isset($response['customer_phone'])) ? $response['customer_phone'] : "";
                $saveInfo->last_4_digits=$ccLast4Digits;
                $saveInfo->first_4_digits=$ccFirst4Digits;
                $saveInfo->card_brand=$ccCardBrand;
                $saveInfo->pt_token=$ccPtToken;
                $saveInfo->pt_customer_password=$ccPtCustomerPassword;
                $saveInfo->save();
              }
            }
          }

          Yii::$app->getSession()->setFlash('success', Yii::t('app','Payment completed successfully'));
          return $this->redirect(['site/index']);
        }else{
          $order = $this->findModel($response['order_id']);
          $order->response_code = (isset($response['response_code'])) ? $response['response_code'] : "";
          $order->trans_date = (isset($response['trans_date'])) ? $response['trans_date'] : "";
          $order->transaction_id = (isset($response['transaction_id'])) ? $response['transaction_id'] : "";
          $order->amount = (isset($response['transaction_amount'])) ? $response['transaction_amount'] : "";
          $order->status = 'cancelled';

          $order->last_4_digits = (isset($response['last_4_digits'])) ? $response['last_4_digits'] : "";
          $order->first_4_digits = (isset($response['first_4_digits'])) ? $response['first_4_digits'] : "";
          $order->card_brand = (isset($response['card_brand'])) ? $response['card_brand'] : "";

          $order->secure_sign = (isset($response['secure_sign'])) ? $response['secure_sign'] : "";
          $order->save();
          return $this->redirect(['rejected']);
        }
      }
    }
  }

  /**
  * Displays a single AdminGroup model.
  * @param integer $id
  * @return mixed
  */
  public function actionSuccess()
  {
    $this->checkLogin();
    return $this->render('success');
  }

  /**
  * Displays a single AdminGroup model.
  * @param integer $id
  * @return mixed
  */
  public function actionRejected()
  {
    $this->checkLogin();
    return $this->render('error');
  }

  /**
  * Finds the AdminGroup model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return AdminGroup the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Order::findOne(['order_id'=>$id,'status'=>'pending'])) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
