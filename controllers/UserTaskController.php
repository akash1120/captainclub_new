<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserNote;
use app\models\UserNoteSearch;
use app\models\UserNoteUser;
use app\models\UserNoteUpdateForm;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* UserTaskController implements the CRUD actions for User Note model.
*/
class UserTaskController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all User models.
  * @return mixed
  */
  public function actionIndex($id=null,$listType='all')
  {
    $this->checkAdmin();
    $model = $this->findUserModel($id);
    $searchModel = new UserNoteSearch();
    $searchModel->user_id=$id;
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('/user-note/index', [
      'model' => $model,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }
  /**
  * Creates a new Cities model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate($id=null)
  {
    $this->checkSuperAdmin();
    $model = new UserNote;
    $model->user_id=$id;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('/user-note/new_task', [
      'model' => $model,
    ]);
  }

  /**
  * Update a User Note model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionUpdate($id)
  {
    $this->checkSuperAdmin();
    $parentModel = UserNote::find()->where(['id'=>$id])->one();
    if($parentModel!=null){

      $model = new UserNoteUpdateForm;
      $model->parent=$id;

      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          echo 'success';
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  echo $val;
                  die();
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('/user-note/update', [
        'model' => $model,
      ]);
    }else{
      echo "No Note found!";
    }
  }

  /**
  * Tasks in Calendar Json.
  * @return mixed
  */
  public function actionCalendarJson($start, $end)
  {
    $output_arrays = [];
    $startDate=strtotime($start);
    $endDate=strtotime($end);

    $tasks = UserNote::find()
    ->select([
      UserNote::tableName().'.id',
      'user_id',
      User::tableName().'.username',
      User::tableName().'.firstname',
      User::tableName().'.lastname',
      'reminder','comments',
      'color',
      'reminder_type',
      'is_feedback'
    ])
    ->innerJoin("user",UserNote::tableName().".user_id=".User::tableName().".id")
    ->innerJoin("user_note_user",UserNoteUser::tableName().".note_id=".UserNote::tableName().".id")
    ->where([
      'and',
      [UserNoteUser::tableName().'.staff_id'=>Yii::$app->user->identity->id,UserNote::tableName().'.trashed'=>0],
      ['>=', 'DATE('.UserNote::tableName().'.reminder)', date("Y-m-d", $startDate)],
      ['<=', 'DATE('.UserNote::tableName().'.reminder)', date("Y-m-d", $endDate)],
      ['is', 'remarks', NULL]
    ])
    ->asArray()->all();

    if($tasks!=null){
      foreach($tasks as $task){
        $clrCode="#00aa4f";
        $txtColor="#fff";
        if($task['reminder_type']=='1'){
          $clrCode="#00aa4f";
          $txtColor="#fff";
        }elseif($task['reminder_type']=='2'){
          $clrCode="#f00";
          $txtColor="#fff";
        }elseif($task['reminder_type']=='3'){
          $clrCode="#0088cc";
          $txtColor="#fff";
        }elseif($task['reminder_type']=='4'){
          $clrCode="#ed9c28";
          $txtColor="#fff";
        }

        $memberName=Yii::$app->helperFunctions->getMemberFullName($task['username'],$task['firstname'],$task['lastname']);
        $title = Yii::$app->helperFunctions->reminderTypes[$task['reminder_type']].' '.$memberName;
        $output_arrays[] = [
          'id' => $task['id'],
          'user_id' => $task['user_id'],
          'username' => $task['username'],
          'memberName' => $memberName,
          'isfeedback' => $task['is_feedback'],
          'title'=> $title,
          'description'=>$title."<hr>".$task['comments'],
          'start'=>str_replace(" ","T",$task['reminder']),
          'end'=>str_replace(" ","T",$task['reminder']),
          'color'=>$clrCode,
          'borderColor'=>$txtColor,
          'textColor'=>$txtColor,
          'textEscape'=>false,
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode($output_arrays);
    exit;
  }

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return User the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findUserModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
