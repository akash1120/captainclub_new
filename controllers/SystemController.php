<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Setting;
use app\models\SettingForm;

class SystemController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['setting'],
        'rules' => [
          [
            'actions' => ['setting'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Displays Setting Page.
  *
  * @return string
  */
  public function actionSetting()
  {
    $this->checkSuperAdmin();
    $model=new SettingForm;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      Yii::$app->getSession()->setFlash('success', 'Information saved successfully');
      return $this->redirect(['setting']);
    }

    $sValues['SettingForm']=[];
    $settings=Setting::find()->all();
    foreach($settings as $record){
      $sValues['SettingForm'][$record['config_name']]=$record['config_value'];
    }
    $model->load($sValues);


    return $this->render('setting',['model'=>$model]);
  }
}
