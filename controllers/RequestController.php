<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\UserFreezeRequest;
use app\models\UserFeedback;
use app\models\UserOtherRequest;
use app\models\ActivityLog;
use app\models\Booking;
use app\models\BookingSearch;
use app\models\WaterSportEquipment;
use app\models\CaptainRequest;
use app\models\UserNightdriveTrainingRequest;
use app\models\UserNightDriveRequestReminder;
use app\models\UserCityUpgradeRequest;
use app\models\UserBookingRequest;
use app\models\WaitingListCaptain;
use app\models\WaitingListSportsEquipment;
use app\models\WaitingListBbq;
use app\models\WaitingListWakeBoarding;
use app\models\WaitingListWakeSurfing;
use app\models\RequestKidsLifeJacketForm;
use app\models\WaitingListEarlyDeparture;
use app\models\WaitingListOvernightCamping;
use app\models\WaitingListLateArrival;
use app\models\BoatToTimeSlot;

class RequestController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['setting'],
        'rules' => [
          [
            'actions' => ['setting'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Freeze Account
  * @return mixed
  */
  public function actionBookingList($user_id=null)
  {
    $this->checkLogin();
    if($user_id==null)$user_id=Yii::$app->user->identity->id;
    $member=User::findOne($user_id);
    $searchModel = new BookingSearch();
    $searchModel->user_id=$member->id;
    $searchModel->listType='future';
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->renderAjax('member_bookings_list', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Freeze Account
  * @return mixed
  */
  public function actionFreeze($user_id=null)
  {
    $this->checkLogin();
    if($user_id==null)$user_id=Yii::$app->user->identity->id;
    $member=User::findOne($user_id);
    $model = new UserFreezeRequest();
    $model->user_id=$user_id;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
      }else{
        foreach($model->getErrors() as $error){
          foreach($error as $key=>$val){
            echo $val;
          }
        }
      }
      exit;
    }

    return $this->renderAjax('freeze_form', [
      'model' => $model,
      'member' => $member,
    ]);
  }

  /**
  * Unfreeze Account.
  * @return mixed
  */
  public function actionUnFreeze()
  {
    $this->checkLogin();
    $msg=[];
    $user=Yii::$app->user->identity;
    if(date("Y-m-d")<$user->freezeEndDate){
      $activePackage=$user->activePackage;
      if($activePackage!=null){
        $userIdz=$user->memberIdz;

        $remainingDays=ceil((strtotime(Yii::$app->user->identity->freezeEndDate) - time())/60/60/24);
        if($remainingDays>0){
          $oldPackageEndDate=Yii::$app->user->identity->packageEndDate;
          list($y,$m,$d)=explode("-",$oldPackageEndDate);
          $newPackageEndDate = date("Y-m-d",mktime(0,0,0,$m,($d-$remainingDays),$y));

          $newFreeEndDate=date("Y-m-d",mktime(0,0,0,date("m"),(date("d")-1),date("Y")));

          $connection = \Yii::$app->db;

          $connection->createCommand("update ".Contract::tableName()." set end_date='".$newPackageEndDate."',remaining_freeze_days=(remaining_freeze_days+$remainingDays) where id='".$activePackage->contract->id."'")->execute();


          $freezeRequest=UserRequests::find()->where(['and',['item_type'=>'freeze','status'=>1,'trashed'=>0],['in','created_by',$userIdz],['<=','freeze_start',date("Y-m-d")],['>','freeze_end',date("Y-m-d")]])->one();
          if($freezeRequest!=null){
            $connection->createCommand("update ".UserRequests::tableName()." set freeze_end='".$newFreeEndDate."' where id='".$freezeRequest->id."'")->execute();
          }

          $msg['success']=['heading'=>Yii::t('app','Unfreezed'),'msg'=>Yii::t('app','You account is unfreezed successfully!')];
        }else{
          $msg['error']=['heading'=>Yii::t('app','Sorry'),'msg'=>Yii::t('app','You can not unfreeze on last day')];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Sorry'),'msg'=>Yii::t('app','You can not unfreeze now')];
      }
    }else{
      $msg['error']=['heading'=>Yii::t('app','Sorry'),'msg'=>Yii::t('app','You can not unfreeze on last day')];
    }
    echo json_encode($msg);
  }

  /**
  * Send Request type Suggestion
  * @return mixed
  */
  public function actionSuggestion()
  {
    $this->checkLogin();
    $model = new UserFeedback();
    $model->user_id=Yii::$app->user->identity->id;
    $model->rating=0;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
      }else{
        $n=1;
        foreach($model->getErrors() as $error){
          foreach($error as $key=>$val){
            echo $val.($n>1 ? ', ' : '');
          }
          $n++;
        }
      }
      exit;
    }
    return $this->renderAjax('suggestion_form',[
      'model'=>$model
    ]);
  }

  /**
  * Send Request type Other
  * @return mixed
  */
  public function actionOther($user_id=null,$booking_id=0)
  {
    $this->checkLogin();
    if($user_id==null)$user_id=Yii::$app->user->identity->id;
    $member=User::findOne($user_id);
    $model = new UserOtherRequest();
    $model->user_id=$user_id;
    $model->booking_id=$booking_id;
    if($booking_id!=null){
      $booking=Booking::findOne($booking_id);
      $model->requested_date=$booking->booking_date;
    }
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
      }else{
        $n=1;
        foreach($model->getErrors() as $error){
          foreach($error as $key=>$val){
            echo $val.($n>1 ? ', ' : '');
          }
          $n++;
        }
      }
      exit;
    }
    return $this->renderAjax('other_form',[
      'model'=>$model,
      'member'=>$member,
      'user_id'=>$user_id,
      'booking_id'=>$booking_id,
    ]);
  }

  /**
  * Request Captain with the booking
  * @return json
  */
  public function actionBookCaptain($id)
  {
    $this->checkLogin();
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->booking_time_slot==Yii::$app->params['nightDriveTimeSlot']){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','if you want an In-house Captain for this trip, kindly delete your booking and book a night cruise. Best Regards')];
        echo json_encode($msg);
        exit;
      }
      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeCaptainMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->captain==0){
        $totalCaptainsInMarina=$booking->marina->captains;
        $usedOnDate=Booking::find()->where(['booking_date'=>$booking->booking_date,'port_id'=>$booking->port_id,'booking_time_slot'=>$booking->booking_time_slot,'captain'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalCaptainsInMarina){
          $captainType=Yii::$app->bookingHelperFunctions->getCaptainStatus($booking->member,$booking);
          if($captainType=='paid'){
            $msg['selection']=['heading'=>Yii::t('app','Sorry Captain'),'msg'=>Yii::t('app','you have used all of your in-house captains. would you like to add a captain for '.Yii::$app->appHelperFunctions->captainPrice.' AED / Trip. If yes click CONFIRM.')];
          }else{
            $connection = \Yii::$app->db;
            $connection->createCommand("update ".Booking::tableName()." set captain=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
            $booking->metRequest('captain');
            ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Captain Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
            $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request is confirmed')];
          }
        }else{
          $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->captainLimitReachedReason];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed captain for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Buy Captain with the booking
  * @return json
  */
  public function actionBuyCaptain($id)
  {
    $this->checkLogin();
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->booking_time_slot==Yii::$app->params['nightDriveTimeSlot']){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','if you want an In-house Captain for this trip, kindly delete your booking and book a night cruise. Best Regards')];
        echo json_encode($msg);
        exit;
      }

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeCaptainMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->captain==0){
        $totalCaptainsInMarina=$booking->marina->captains;
        $usedOnDate=Booking::find()->where(['booking_date'=>$booking->booking_date,'port_id'=>$booking->port_id,'booking_time_slot'=>$booking->booking_time_slot,'captain'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalCaptainsInMarina){
          $request=CaptainRequest::find()->where(['booking_id'=>$booking->id])->one();
          if($request==null){
            $request=new CaptainRequest;
            $request->user_id=$booking->user_id;
            $request->booking_id=$booking->id;
            $request->status=0;
            if($request->save()){
              $connection = \Yii::$app->db;
              $connection->createCommand("update ".Booking::tableName()." set captain=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
              ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Captain Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
              $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request is confirmed')];
              $booking->metRequest('captain');
            }
          }
        }else{
          $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->captainLimitReachedReason];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed captain for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Add an entry for waiting list
  * @return json
  */
  public function actionWaitingListCaptain($id,$reason=null)
  {
    $this->checkLogin();
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->booking_time_slot==Yii::$app->params['nightDriveTimeSlot']){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','if you want an In-house Captain for this trip, kindly delete your booking and book a night cruise. Best Regards')];
        echo json_encode($msg);
        exit;
      }
      if($booking->captain==0){
        $totalCaptainsInMarina=$booking->marina->captains;
        $usedOnDate=Booking::find()->where(['booking_date'=>$booking->booking_date,'port_id'=>$booking->port_id,'booking_time_slot'=>$booking->booking_time_slot,'captain'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalCaptainsInMarina){

        }else{
          $request=WaitingListCaptain::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
          if($request==null){
            $request=new WaitingListCaptain;
            $request->user_id=$booking->user_id;
            $request->booking_id=$booking->id;
            $request->city_id=$booking->city_id;
            $request->marina_id=$booking->port_id;
            $request->date=$booking->booking_date;
            $request->time_id=$booking->booking_time_slot;
            $request->reason=$reason!=null ? $reason : Yii::$app->bookingHelperFunctions->captainLimitReachedReason;
            $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
            $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
            $request->status=0;
            if($request->save()){
              ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Captain Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
              $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage('captain')];
            }
          }
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed captain for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request Sports Equipmenet with the booking
  * @return json
  */
  public function actionSportsEquipment($id,$equip_id)
  {
    $this->checkLogin();
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->sport_eqp_id==0){
        $msg=[];
        if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
          $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeEquipmentMsg];
          echo json_encode($msg);
          exit;
        }
        $equipmentRow=WaterSportEquipment::find()->where(['id'=>$equip_id,'city_id'=>$booking->city_id,'port_id'=>$booking->port_id])->one();
        if($equipmentRow!=null){

          $totalEquipmentsInMarina=$equipmentRow->qty;
          $usedOnDate=Booking::find()->where(['booking_date'=>$booking->booking_date,'port_id'=>$booking->port_id,'booking_time_slot'=>$booking->booking_time_slot,'sport_eqp_id'=>$equipmentRow->id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
          if($usedOnDate<$totalEquipmentsInMarina){
            $connection = \Yii::$app->db;
            $connection->createCommand("update ".Booking::tableName()." set sport_eqp_id=:eqp_id where id=:booking_id",[':eqp_id'=>$equipmentRow->id,':booking_id'=>$booking->id])->execute();
            ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Sports Equipment ('.$equipmentRow->title.') Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
            $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request is confirmed')];
            $booking->metRequest('equipment',$equip_id);
          }else{
            $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->equipmentLimitReachedReason];
          }
        }else{
          $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','No Equipment found!')];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed water sports equipment for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request Sports Equipmenet with the booking
  * @return json
  */
  public function actionWaitingListSportsEquipment($id,$equip_id,$reason=null)
  {
    $this->checkLogin();
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->sport_eqp_id==0){
        $msg=[];
        if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
          $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeEquipmentMsg];
          echo json_encode($msg);
          exit;
        }
        $equipmentRow=WaterSportEquipment::find()->where(['id'=>$equip_id,'city_id'=>$booking->city_id,'port_id'=>$booking->port_id])->one();
        if($equipmentRow!=null){

          $totalEquipmentsInMarina=$equipmentRow->qty;
          $usedOnDate=Booking::find()->where(['booking_date'=>$booking->booking_date,'port_id'=>$booking->port_id,'booking_time_slot'=>$booking->booking_time_slot,'sport_eqp_id'=>$equipmentRow->id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
          if($usedOnDate<$totalEquipmentsInMarina){
            $connection = \Yii::$app->db;
            $connection->createCommand("update ".Booking::tableName()." set sport_eqp_id=:eqp_id where id=:booking_id",[':eqp_id'=>$equipmentRow->id,':booking_id'=>$booking->id])->execute();
            ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Sports Equipment ('.$equipmentRow->title.') Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
            $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request is confirmed')];
            $booking->metRequest('equipment');
          }else{
            $request=WaitingListSportsEquipment::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
            if($request==null){
              $request=new WaitingListSportsEquipment;
              $request->user_id=$booking->user_id;
              $request->booking_id=$booking->id;
              $request->city_id=$booking->city_id;
              $request->marina_id=$booking->port_id;
              $request->date=$booking->booking_date;
              $request->time_id=$booking->booking_time_slot;
              $request->equipment_id=$equip_id;
              $request->reason=$reason!=null ? $reason :Yii::$app->bookingHelperFunctions->equipmentLimitReachedReason;
              $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
              $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
              $request->status=0;
              if($request->save()){
                ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Sports Equipment Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
                $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage($equipmentRow->title)];
              }
            }
          }
        }else{
          $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','No Equipment found!')];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed water sports equipment for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request BBQ with the booking
  * @return json
  */
  public function actionBookBbq($id)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeBBQMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->bbq==0){
        $totalEquipmentsInMarina=$booking->marina->bbq;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'bbq'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalEquipmentsInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set bbq=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'BBQ Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->appHelperFunctions->bBQMsg];
          $booking->metRequest('bbq');
        }else{
          $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->bbqSetLimitReachedReason];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed BBQ Set for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Add an entry for waiting list
  * @return json
  */
  public function actionWaitingListBbq($id,$reason=null)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->bbq==0){
        $totalEquipmentsInMarina=$booking->marina->bbq;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'bbq'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalEquipmentsInMarina){

        }else{
          $request=WaitingListBbq::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
          if($request==null){
            $request=new WaitingListBbq;
            $request->user_id=$booking->user_id;
            $request->booking_id=$booking->id;
            $request->city_id=$booking->city_id;
            $request->marina_id=$booking->port_id;
            $request->date=$booking->booking_date;
            $request->time_id=$booking->booking_time_slot;
            $request->reason=$reason!=null ? $reason : Yii::$app->bookingHelperFunctions->bbqSetLimitReachedReason;
            $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
            $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
            $request->status=0;
            if($request->save()){
              ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'BBQ Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
              $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage('bbqset')];
            }
          }
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed BBQ Set for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request Wake Boarding with the booking
  * @return json
  */
  public function actionBookWakeBoarding($id)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeWakeBoardingMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->wake_boarding==0){
        $totalEquipmentsInMarina=$booking->marina->wake_boarding_limit;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'wake_boarding'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalEquipmentsInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set wake_boarding=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Wake Boarding Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Your request for wake boarding is confirmed'];
          $booking->metRequest('wakeboarding');
        }else{
          $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->wakeBoardingLimitReachedReason];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Wake Boarding for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Add an entry for waiting list
  * @return json
  */
  public function actionWaitingListWakeBoarding($id,$reason=null)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->wake_boarding==0){
        $totalEquipmentsInMarina=$booking->marina->wake_boarding_limit;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'wake_boarding'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalEquipmentsInMarina){

        }else{
          $request=WaitingListWakeBoarding::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
          if($request==null){
            $request=new WaitingListWakeBoarding;
            $request->user_id=$booking->user_id;
            $request->booking_id=$booking->id;
            $request->city_id=$booking->city_id;
            $request->marina_id=$booking->port_id;
            $request->date=$booking->booking_date;
            $request->time_id=$booking->booking_time_slot;
            $request->reason=$reason!=null ? $reason : Yii::$app->bookingHelperFunctions->wakeBoardingLimitReachedReason;
            $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
            $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
            $request->status=0;
            if($request->save()){
              ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Wake Boarding Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
              $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage('wakeboarding')];
            }
          }
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Wake Boarding for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request Wake Surfing with the booking
  * @return json
  */
  public function actionBookWakeSurfing($id)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeWakeSurfingMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->wake_surfing==0){
        $totalEquipmentsInMarina=$booking->marina->wake_surfing_limit;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'wake_surfing'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalEquipmentsInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set wake_surfing=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Wake Surfing Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Your request for wake surfing is confirmed'];
          $booking->metRequest('wakesurfing');
        }else{
          $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->wakeSurfingLimitReachedReason];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Wake Surfing for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Add an entry for waiting list
  * @return json
  */
  public function actionWaitingListWakeSurfing($id,$reason=null)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->wake_surfing==0){
        $totalEquipmentsInMarina=$booking->marina->wake_surfing_limit;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'wake_surfing'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalEquipmentsInMarina){

        }else{
          $request=WaitingListWakeSurfing::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
          if($request==null){
            $request=new WaitingListWakeSurfing;
            $request->user_id=$booking->user_id;
            $request->booking_id=$booking->id;
            $request->city_id=$booking->city_id;
            $request->marina_id=$booking->port_id;
            $request->date=$booking->booking_date;
            $request->time_id=$booking->booking_time_slot;
            $request->reason=$reason!=null ? $reason : Yii::$app->bookingHelperFunctions->wakeSurfingLimitReachedReason;
            $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
            $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
            $request->status=0;
            if($request->save()){
              ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Wake Surfing Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
              $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage('wakesurfing')];
            }
          }
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Wake Surfing for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request Ice with the booking
  * @return json
  */
  public function actionBookIce($id)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeBBQMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->ice==0){
        $connection = \Yii::$app->db;
        $connection->createCommand("update ".Booking::tableName()." set ice=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Ice Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
        $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Ice is confirmed'];
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Ice Set for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request Kids Life Jacket with the booking
  * @return json
  */
  public function actionBookKidsLifeJacket($id)
  {
    $this->checkLogin();
    $model = new RequestKidsLifeJacketForm;
    $model->booking_id=$id;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Kids Life Jackets are confirmed with your trip'];
        echo json_encode($msg);
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>$val];
                echo json_encode($msg);
                exit;
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('request_kids_life_jacket',['model'=>$model]);
  }

  /**
  * Cancels Kids Life Jackets with the booking
  * @return json
  */
  public function actionCancelKidsLifeJacket($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set kids_life_jacket=0,no_of_one_to_three_jackets=0,no_of_four_to_twelve_jackets=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Kids Life Jackets ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Kids Life Jackets for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function processEarlyDeparture($id,$type)
  {
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $msg=[];
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeEarlyDepartureMsg];
        return $msg;
      }
      if($booking->early_departure==0){
        $totalEarlyDeparturesAllowedInMarina=$booking->marina->no_of_early_departures;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalEarlyDeparturesAllowedInMarina){
          //Check this boat is booked day before as overnight camping
          $previousDate=Yii::$app->helperFunctions->getDayBefore($booking->booking_date);
          $previousDayOvernightCampingBookingForThisBoat=Booking::find()->where(['city_id'=>$booking->city_id,'boat_id'=>$booking->boat_id,'port_id'=>$booking->port_id,'booking_date'=>$previousDate,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
          if($previousDayOvernightCampingBookingForThisBoat->exists()){
            if($type=='waiting'){
              $msg = $this->addEarlyDepartureToWaiting($booking,Yii::$app->bookingHelperFunctions->earlyDeparturePreviousDayOvernightCampReason);
            }else{
              $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->earlyDeparturePreviousDayOvernightCampReason];
              return $msg;
            }
          }

          if($type=='waiting'){

          }else{
            $connection = \Yii::$app->db;
            $connection->createCommand("update ".Booking::tableName()." set early_departure=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
            ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Early Departure Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
            $booking->sendEarlyDepartureConfirmationEmail();
            $booking->metRequest('early_departure');
            $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Early Departure is confirmed'];
          }
        }else{
          if($type=='waiting'){
            $msg = $this->addEarlyDepartureToWaiting($booking,Yii::$app->bookingHelperFunctions->earlyDepartureLimitReachedReason);
          }else{
            $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->earlyDepartureLimitReachedReason];
            return $msg;
          }
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Overnight Camping for this booking')];
      }
      return $msg;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function addEarlyDepartureToWaiting($booking,$reason)
  {
    $request=WaitingListEarlyDeparture::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
    if($request==null){
      $request=new WaitingListEarlyDeparture;
      $request->user_id=$booking->user_id;
      $request->booking_id=$booking->id;
      $request->city_id=$booking->city_id;
      $request->marina_id=$booking->port_id;
      $request->date=$booking->booking_date;
      $request->time_id=$booking->booking_time_slot;
      $request->reason=$reason;
      $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
      $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
      $request->status=0;
      if($request->save()){
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Early Departure Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
        $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage('earlydeparture')];
        return $msg;
      }
    }
  }

  /**
  * Request Late Arrival with the booking
  * @return json
  */
  public function actionBookEarlyDeparture($id)
  {
    $this->checkLogin();
    $msg=[];
    $msg = $this->processEarlyDeparture($id,'new');

    echo json_encode($msg);
    exit;
  }

  /**
  * Add an entry for waiting list
  * @return json
  */
  public function actionWaitingListEarlyDeparture($id)
  {
    $this->checkLogin();
    $msg=[];
    $msg = $this->processEarlyDeparture($id,'waiting');

    echo json_encode($msg);
    exit;
  }

  /**
  * Request Late Arrival with the booking
  * @return json
  */
  public function actionBookOvernightCamping($id)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeOvernightCampingMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->overnight_camping==0){
        $totalOvernightCampsAllowedInMarina=$booking->marina->no_of_overnight_camps;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalOvernightCampsAllowedInMarina){
          //Check this boat is booked for early departure next day
          $nextDate=Yii::$app->helperFunctions->getNextDay($booking->booking_date);
          $nextDayEarlyDepartureBookingForThisBoat=Booking::find()->where(['city_id'=>$booking->city_id,'boat_id'=>$booking->boat_id,'port_id'=>$booking->port_id,'booking_date'=>$nextDate,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
          if($nextDayEarlyDepartureBookingForThisBoat->exists()){
            $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->overnightCampingBoatIsEarlyDepartureNextDayReason];
            echo json_encode($msg);
            exit;
          }

          if($booking->booking_time_slot==2){
            //If this boat has night sessions available
            $nightDriveSlot=BoatToTimeSlot::find()->where(['boat_id'=>$booking->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
            if($nightDriveSlot->exists()){
              //it has night drive session, so Not Available
              $msg['requestIt']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "Request" button, a request will be submitted.'),'reason'=>Yii::$app->bookingHelperFunctions->overnightCampingBoatIsOneOfNightDrivesReason];
              echo json_encode($msg);
              exit;
            }
          }

          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set overnight_camping=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Overnight Camping Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Overnight Camping is confirmed'];
          $booking->metRequest('overnight_camping');
        }else{
          $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->overnightCampingLimitReachedReason];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Overnight Camping for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function addWaitingListOvernightCamping($booking,$reason)
  {
    $request=WaitingListOvernightCamping::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
    if($request==null){
      $request=new WaitingListOvernightCamping;
      $request->user_id=$booking->user_id;
      $request->booking_id=$booking->id;
      $request->city_id=$booking->city_id;
      $request->marina_id=$booking->port_id;
      $request->date=$booking->booking_date;
      $request->time_id=$booking->booking_time_slot;
      $request->reason=$reason;
      $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
      $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
      $request->status=0;
      if($request->save()){
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Overnight Camping Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
        $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage('overnightcamp')];
        $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.')];
        echo json_encode($msg);
        exit;
      }
    }
  }
  /**
  * Add an entry for waiting list
  * @return json
  */
  public function actionWaitingListOvernightCamping($id,$reason)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->overnight_camping==0){
        $totalOvernightCampsAllowedInMarina=$booking->marina->no_of_overnight_camps;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalOvernightCampsAllowedInMarina){
          //Check this boat is booked for early departure next day
          $nextDate=Yii::$app->helperFunctions->getNextDay($booking->booking_date);
          $nextDayEarlyDepartureBookingForThisBoat=Booking::find()->where(['city_id'=>$booking->city_id,'boat_id'=>$booking->boat_id,'port_id'=>$booking->port_id,'booking_date'=>$nextDate,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
          if($nextDayEarlyDepartureBookingForThisBoat->exists()){
            $this->addWaitingListOvernightCamping($booking,Yii::$app->bookingHelperFunctions->overnightCampingBoatIsEarlyDepartureNextDayReason);
            //$msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.')];
            //echo json_encode($msg);
            exit;
          }

          //If this boat has night sessions available
          $nightDriveSlot=BoatToTimeSlot::find()->where(['boat_id'=>$booking->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
          if($nightDriveSlot->exists()){
            //it has night drive session, so Not Available
            $this->addWaitingListOvernightCamping($booking,Yii::$app->bookingHelperFunctions->overnightCampingBoatIsOneOfNightDrivesReason);
            //$msg['requestIt']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "Request" button, a request will be submitted.')];
            //echo json_encode($msg);
            exit;
          }

          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set overnight_camping=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Overnight Camping Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Overnight Camping is confirmed'];
        }else{
          $this->addWaitingListOvernightCamping($booking,Yii::$app->bookingHelperFunctions->overnightCampingLimitReachedReason);
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Late Arrival for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Request Late Arrival with the booking
  * @return json
  */
  public function actionBookLateArrival($id)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){

      if($booking->booking_date==date("Y-m-d") || (date("Y-m-d")==Yii::$app->helperFunctions->getDayBefore($booking->booking_date) && date("H")>=Yii::$app->helperFunctions->shortNoticeAfter)){
        $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::$app->helperFunctions->shortNoticeLateArrivalMsg];
        echo json_encode($msg);
        exit;
      }
      if($booking->late_arrival==0){
        $totalLateArrivalsAllowedInMarina=$booking->marina->no_of_late_arrivals;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'late_arrival'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalLateArrivalsAllowedInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set late_arrival=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
          $booking->metRequest('late_arrival');

          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Late Arrival Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Late Arrival is confirmed'];
        }else{
          $msg['waitinglist']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','At the moment this service is booked. By clicking on the "wait list" button, it will automatically book it for you if it becomes available.'),'reason'=>Yii::$app->bookingHelperFunctions->lateArrivalLimitReachedReason];
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Late Arrival for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Add an entry for waiting list
  * @return json
  */
  public function actionWaitingListLateArrival($id,$reason=null)
  {
    $this->checkLogin();
    $msg=[];

    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->late_arrival==0){
        $totalLateArrivalsAllowedInMarina=$booking->marina->no_of_late_arrivals;
        $usedOnDate=Booking::find()->where(['city_id'=>$booking->city_id,'port_id'=>$booking->port_id,'booking_date'=>$booking->booking_date,'booking_time_slot'=>$booking->booking_time_slot,'late_arrival'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalLateArrivalsAllowedInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set late_arrival=1 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
          $booking->metRequest('late_arrival');
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Late Arrival Confirmed ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
          $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>'Late Arrival is confirmed'];
        }else{
          $request=WaitingListLateArrival::find()->where(['booking_id'=>$booking->id,'status'=>0])->one();
          if($request==null){
            $request=new WaitingListLateArrival;
            $request->user_id=$booking->user_id;
            $request->booking_id=$booking->id;
            $request->city_id=$booking->city_id;
            $request->marina_id=$booking->port_id;
            $request->date=$booking->booking_date;
            $request->time_id=$booking->booking_time_slot;
            $request->reason=$reason!=null ? $reason : Yii::$app->bookingHelperFunctions->lateArrivalLimitReachedReason;
            $request->active_show_till=date("Y-m-d",strtotime("+2 day",strtotime($booking->booking_date)));
            $request->expected_reply=date ("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date)));
            $request->status=0;
            if($request->save()){
              ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Late Arrival Waiting List ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
              $msg['success']=['heading'=>Yii::$app->waitingListHelper->waitlistSuccessHeading,'msg'=>Yii::$app->waitingListHelper->getWaitlistSuccessMessage('latearrival')];
            }
          }
        }
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You have already confirmed Late Arrival for this booking')];
      }
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Captain with the booking
  * @return json
  */
  public function actionCancelCaptain($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      if($booking->captain==1){
        $connection = \Yii::$app->db;
        $connection->createCommand("update ".Booking::tableName()." set captain=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Captain ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
        $request=CaptainRequest::find()->where(['booking_id'=>$booking->id])->one();
        if($request!=null)$request->delete();
        $waitngListentry=WaitingListCaptain::findOne(['booking_id'=>$booking->id]);
        if($waitngListentry!=null)$waitngListentry->delete();

        //Check if there is any waiting list
        Yii::$app->waitingListHelper->checkAndAssignCaptain($booking->city_id,$booking->port_id,$booking->booking_date,$booking->booking_time_slot);
      }
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of captain for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Captain waiting entry
  * @return json
  */
  public function actionCancelCaptainWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListCaptain::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Captain Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of captain for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Equipment with the booking
  * @return json
  */
  public function actionCancelEquipment($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set sport_eqp_id=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Sports Equipment ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of equipment for this booking has been cancelled.')];
      $waitngListentry=WaitingListSportsEquipment::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();


      //Check if there is any waiting list
      Yii::$app->waitingListHelper->checkAndAssignSportsEquipment($booking->city_id,$booking->port_id,$booking->booking_date,$booking->booking_time_slot);

      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Sports Equipment Waiting entry
  * @return json
  */
  public function actionCancelSportsEquipmentWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListSportsEquipment::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Sports Equipment Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Sports Equipment for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels BBQ with the booking
  * @return json
  */
  public function actionCancelBbq($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set bbq=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled BBQ ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of BBQ Set for this booking has been cancelled.')];
      $waitngListentry=WaitingListBbq::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      //Check if there is any waiting list
      Yii::$app->waitingListHelper->checkAndAssignBBQSet($booking->city_id,$booking->port_id,$booking->booking_date,$booking->booking_time_slot);

      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels BBQ waiting entry
  * @return json
  */
  public function actionCancelBbqWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListBbq::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled BBQ Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of BBQ for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Wake Boarding with the booking
  * @return json
  */
  public function actionCancelWakeBoarding($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set wake_boarding=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Wake Boarding ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Wake Boarding for this booking has been cancelled.')];
      $waitngListentry=WaitingListWakeBoarding::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      //Check if there is any waiting list
      Yii::$app->waitingListHelper->checkAndAssignWakeBoarding($booking->city_id,$booking->port_id,$booking->booking_date,$booking->booking_time_slot);

      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Wake Boarding waiting entry
  * @return json
  */
  public function actionCancelWakeBoardingWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListWakeBoarding::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Wake Boarding Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Wake Boarding for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Wake Surfing with the booking
  * @return json
  */
  public function actionCancelWakeSurfing($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set wake_surfing=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Wake Surfing ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Wake Surfing for this booking has been cancelled.')];
      $waitngListentry=WaitingListWakeSurfing::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      //Check if there is any waiting list
      Yii::$app->waitingListHelper->checkAndAssignWakeSurfing($booking->city_id,$booking->port_id,$booking->booking_date,$booking->booking_time_slot);

      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Wake Surfing waiting entry
  * @return json
  */
  public function actionCancelWakeSurfingWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListWakeSurfing::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Wake Surfing Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Wake Surfing for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Overnight Camping with the booking
  * @return json
  */
  public function actionCancelEarlyDeparture($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListEarlyDeparture::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set early_departure=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Early Departure ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Early Departure for this booking has been cancelled.')];

      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Overnight Camping waiting entry
  * @return json
  */
  public function actionCancelEarlyDepartureWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListEarlyDeparture::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Early Departure Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Early Departure for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Overnight Camping with the booking
  * @return json
  */
  public function actionCancelOvernightCamping($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListOvernightCamping::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set overnight_camping=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Overnight Camping ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Overnight Camping for this booking has been cancelled.')];

      //Check if there is any waiting list
      /*$waitingList=WaitingListOvernightCamping::find()->where(['city_id'=>$booking->city_id,'marina_id'=>$booking->port_id,'date'=>$booking->booking_date,'status'=>0])->asArray()->orderBy(['created_at'=>SORT_ASC])->one();
      if($waitingList!=null){
        $nextBooking=Booking::find()->where(['id'=>$waitingList['booking_id']])->one();
        if($nextBooking!=null){
          $totalOvernightCampsAllowedInMarina=$nextBooking->marina->no_of_overnight_camps;
          $usedOnDate=Booking::find()->where(['city_id'=>$nextBooking->city_id,'port_id'=>$nextBooking->port_id,'booking_date'=>$nextBooking->booking_date,'overnight_camping'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
          if($usedOnDate<$totalOvernightCampsAllowedInMarina){
            $assign=true;
            //Check this boat is booked for early departure next day
            $nextDate=Yii::$app->helperFunctions->getNextDay($nextBooking->booking_date);
            $nextDayEarlyDepartureBookingForThisBoat=Booking::find()->where(['city_id'=>$nextBooking->city_id,'boat_id'=>$nextBooking->boat_id,'port_id'=>$nextBooking->port_id,'booking_date'=>$nextDate,'early_departure'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0]);
            if($nextDayEarlyDepartureBookingForThisBoat->exists()){
              $assign=false;
            }

            //If this boat has night sessions available
            $nightDriveSlot=BoatToTimeSlot::find()->where(['boat_id'=>$nextBooking->boat_id,'time_slot_id'=>Yii::$app->params['nightDriveTimeSlot']]);
            if($nightDriveSlot->exists()){
              //it has night drive session, so Not Available
              $assign=false;
            }

            if($assign==true){
              $connection = \Yii::$app->db;
              $connection->createCommand("update ".Booking::tableName()." set overnight_camping=1 where id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
              ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Overnight Camping Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
              $connection->createCommand("update ".WaitingListOvernightCamping::tableName()." set status=1 where booking_id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
            }
          }
        }
      }*/

      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Overnight Camping waiting entry
  * @return json
  */
  public function actionCancelOvernightCampingWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListOvernightCamping::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Overnight Camping Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Overnight Camping for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Late Arrival with the booking
  * @return json
  */
  public function actionCancelLateArrival($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListLateArrival::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      $connection = \Yii::$app->db;
      $connection->createCommand("update ".Booking::tableName()." set late_arrival=0 where id=:booking_id",[':booking_id'=>$booking->id])->execute();
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Late Arrival ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Late Arrival for this booking has been cancelled.')];

      //Check if there is any waiting list
      Yii::$app->waitingListHelper->checkAndAssignLateArrival($booking->city_id,$booking->port_id,$booking->booking_date,$booking->booking_time_slot);

      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Cancels Late Arrival waiting entry
  * @return json
  */
  public function actionCancelLateArrivalWaiting($id)
  {
    $this->checkLogin();
    $this->layout="ajax";
    $msg=[];
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }else{
      $user_id='';
    }
    $booking=Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one();
    if($booking!=null){
      $waitngListentry=WaitingListLateArrival::findOne(['booking_id'=>$booking->id]);
      if($waitngListentry!=null)$waitngListentry->delete();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Cancelled Late Arrival Waiting ('.$booking->booking_date.' / '.$booking->boat->name.' from '.$booking->marina->name.' {Ref:'.$booking->id.'}) by ('.Yii::$app->user->identity->fullnameWithUsername.') successfully');
      $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request of Late Arrival for this booking has been cancelled.')];
      echo json_encode($msg);
      exit;
    }else{
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
  * Sends night drive training request
  * @return html
  */
  public function actionNightDriveTraining($user_id=null)
  {
    $this->checkLogin();
    if($user_id==null)$user_id=Yii::$app->user->identity->id;
    $request=UserNightdriveTrainingRequest::find()->where(['user_id'=>$user_id,'status'=>0,'trashed'=>0]);
    if($request->exists()){
      $model = new UserNightDriveRequestReminder;
      $model->user_id=$user_id;
      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          echo 'success';
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  echo $val;
                  die();
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('night_drive_training_reminder_form', [
        'model' => $model,
      ]);
    }else{
      $model = new UserNightdriveTrainingRequest;
      $model->requested_date=date("Y-m-d");
      $model->user_id=$user_id;
      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          echo 'success';
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  echo $val;
                  die();
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('night_drive_training_request_form', [
        'model' => $model,
      ]);
    }
  }

  /**
  *Upgrade City
  */
  public function actionUpgradeCity()
  {
    $this->checkLogin();
    $msg=[];
    $model=UserCityUpgradeRequest::find()->where(['user_id'=>Yii::$app->user->identity->id,'status'=>0,'trashed'=>0])->one();
    if($model==null){
      $model=new UserCityUpgradeRequest;
      $model->requested_date=date("Y-m-d");
      $model->user_id=Yii::$app->user->identity->id;
      $model->descp="Upgrade City Access";
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request has been sent to the concerned department and one of our representative will contact you.')];
      }else{
        foreach($model->getErrors() as $error){
          foreach($error as $key=>$val){
            $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
          }
        }
      }
    }else{
      $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your already have a pending upgrade request, You will be notified when it is approved.')];
    }
    echo json_encode($msg);
    exit;
  }

  /**
  * Send Request for Boat Booking
  * @return mixed
  */
  public function actionBoatBooking($user_id=null)
  {
    $this->checkLogin();
    $model = new UserBookingRequest();
    if($user_id==null){
      $user_id=Yii::$app->user->identity->id;
    }
    $model->user_id=$user_id;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
      }else{
        $n=1;
        foreach($model->getErrors() as $error){
          foreach($error as $key=>$val){
            echo $val.($n>1 ? ', ' : '');
          }
          $n++;
        }
      }
      exit;
    }
    return $this->renderAjax('boat_booking_form', [
      'model' => $model,
    ]);
  }

  public function actionDelete($id,$item_type=null)
  {
    $this->checkLogin();
    $userId='';
    if(Yii::$app->user->identity->user_type==0){
      $userId=Yii::$app->user->identity->id;
    }
    if($item_type=='freeze'){
      $model=UserFreezeRequest::find()->where(['id'=>$id])->andFilterWhere(['user_id'=>$userId])->one();
    }
    if($item_type=='nightdrivetraining'){
      $model=UserNightdriveTrainingRequest::find()->where(['id'=>$id])->andFilterWhere(['user_id'=>$userId])->one();
    }
    if($item_type=='boatbooking'){
      $model=UserBookingRequest::find()->where(['id'=>$id])->andFilterWhere(['user_id'=>$userId])->one();
    }
    if($item_type=='upgradecity'){
      $model=UserCityUpgradeRequest::find()->where(['id'=>$id])->andFilterWhere(['user_id'=>$userId])->one();
    }
    if($item_type=='other'){
      $model=UserOtherRequest::find()->where(['id'=>$id])->andFilterWhere(['user_id'=>$userId])->one();
    }
    $model->softDelete();
    echo 'success';
    exit;
  }
}
