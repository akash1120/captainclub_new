<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ChangePasswordForm;
use app\models\User;
use app\models\ProfileForm;

class AccountController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['update','change-password'],
        'rules' => [
          [
            'actions' => ['update','change-password'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
   * Change Password action.
   *
   * @return Response|string
   */
  public function actionChangePassword()
  {
    $this->checkLogin();
    $model = new ChangePasswordForm();
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      Yii::$app->getSession()->addFlash('success', Yii::t('app','new password saved successfully'));
      return $this->redirect(['site/index']);
    }

    return $this->render('change_password', [
      'model' => $model,
    ]);
  }

  /**
  * Displays homepage.
  *
  * @return string
  */
  public function actionUpdateProfile()
  {
    $this->checkLogin();
    $model = new ProfileForm;
    $oldEmail=Yii::$app->user->identity->email;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        if($oldEmail!=$model->email){
          $user=User::findOne(Yii::$app->user->identity->id);
          $user->sendEmailChangedAlert();
        }
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['site/index']);
      }else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
      }
    }
    return $this->render('profile',['model'=>$model]);
  }

  /**
  * Displays Confirm Profile.
  *
  * @return string
  */
  public function actionConfirmProfile()
  {
    $this->checkLogin();
    if(Yii::$app->user->identity->profileInfo->profile_updated==1 && Yii::$app->user->identity->profileInfo->changed_pass==1){
      return $this->redirect(['site/index']);
    }
    $modelProfile = new ProfileForm;
    $oldEmail=Yii::$app->user->identity->email;
    if ($modelProfile->load(Yii::$app->request->post())) {
      if($modelProfile->save()){
        if($oldEmail!=$modelProfile->email){
          $user=User::findOne(Yii::$app->user->identity->id);
          $user->sendEmailChangedAlert();
        }
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        if(Yii::$app->user->identity->profileInfo->profile_updated==0 || Yii::$app->user->identity->profileInfo->changed_pass==0){
          return $this->redirect(['account/confirm-profile']);
        }else{
          return $this->redirect(['site/index']);
        }
      }else{
				if($modelProfile->hasErrors()){
					foreach($modelProfile->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
      }
    }

    $modelPassword = new ChangePasswordForm();
    if ($modelPassword->load(Yii::$app->request->post()) && $modelPassword->save()) {
      Yii::$app->getSession()->addFlash('success', Yii::t('app','new password saved successfully'));
      if(Yii::$app->user->identity->profileInfo->profile_updated==0 || Yii::$app->user->identity->profileInfo->changed_pass==0){
        return $this->redirect(['account/confirm-profile']);
      }else{
        return $this->redirect(['site/index']);
      }
    }


    return $this->render('confirm_profile',[
      'modelProfile'=>$modelProfile,
      'modelPassword'=>$modelPassword,
    ]);
  }
}
