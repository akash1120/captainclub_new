<?php

namespace app\controllers;

use Yii;
use app\models\Booking;
use app\models\BookingSearch;
use app\models\OldBookingForm;
use app\models\BookingForm;
use app\models\BookingEarlyDepartureAssets;
use app\models\BookingUpdateHistory;
use app\models\User;
use app\models\AdminBookingAssign;
use app\models\NewUserBooking;
use app\models\BookingSuggestionForm;
use app\models\BookingActivity;
use app\models\BookingExpectedArrivalTime;
use app\models\OperationUserCityMarina;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* BookingController implements the CRUD actions for Booking model.
*/
class BookingController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Booking models.
  * @return mixed
  */
  public function actionAll($listType='future')
  {
    $this->checkAdmin();
    $searchModel = new BookingSearch();
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('all', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Booking models.
  * @return mixed
  */
  public function actionIndex($listType='future')
  {
    $this->checkLogin();
    $searchModel = new BookingSearch();
    $searchModel->user_id=Yii::$app->user->identity->id;
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all history Booking models.
  * @return mixed
  */
  public function actionHistory()
  {
    $this->checkLogin();
    return $this->redirect(['index','listType'=>'history']);
  }

  public function actionActivityDetail($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    return $this->renderAjax('booking_full_detail', [
      'model' => $model,
    ]);
  }

  public function actionNotificationResponse($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $modelArriving = new BookingExpectedArrivalTime;
    $modelArriving->booking_id = $model->id;
    $modelArriving->user_id = Yii::$app->user->identity->id;
    if ($modelArriving->load(Yii::$app->request->post())) {
      if($modelArriving->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Thankyou for confirmation'));
        return $this->redirect(['site/index']);
      }else{
        if($modelArriving->hasErrors()){
          foreach($modelArriving->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->setFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('notification', [
      'model' => $model,
      'modelArriving' => $modelArriving,
    ]);
  }

  /**
  * Lists all Booking models for activity.
  * @return mixed
  */
  public function actionUnclosedActivity()
  {
    $this->checkAdmin();
    $modelOpr=OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
    if($modelOpr==null){
      return $this->redirect(['site/index','tab'=>'operations']);
    }
    $searchModel = new BookingSearch();
    $searchModel->city_id=$modelOpr->city_id;
    $searchModel->port_id=$modelOpr->marina_id;
    $dataProvider = $searchModel->searchUnClosed(Yii::$app->request->queryParams);

    return $this->render('unclosed_activity', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Booking models for activity.
  * @return mixed
  */
  public function actionActivity()
  {
    $this->checkAdmin();
    $modelOpr=OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
    if($modelOpr==null){
      return $this->redirect(['site/index','tab'=>'operations']);
    }

    $searchModel = new BookingSearch();
    $searchModel->booking_date=date("Y-m-d");
    $searchModel->city_id=$modelOpr->city_id;
    $searchModel->port_id=$modelOpr->marina_id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    $searchModelEarlyDepartures=new BookingSearch();
    $searchModelEarlyDepartures->booking_date=date ("Y-m-d", strtotime("+1 day", strtotime($searchModel->booking_date)));
    $searchModelEarlyDepartures->city_id=$searchModel->city_id;
    $searchModelEarlyDepartures->port_id=$searchModel->port_id;
    $searchModelEarlyDepartures->early_departure=1;
    $dataProviderEarlyDepartures = $searchModelEarlyDepartures->search([]);

    return $this->render('activity', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'searchModelEarlyDepartures' => $searchModelEarlyDepartures,
      'dataProviderEarlyDepartures' => $dataProviderEarlyDepartures,
    ]);
  }

  /**
  * Early Departure Assets
  * @return mixed
  */
  public function actionEarlyDepartureAssets($id)
  {
    $this->checkAdmin();
    $modelBooking = $this->findModel($id);

    $model = BookingEarlyDepartureAssets::find()->where(['booking_id'=>$modelBooking->id])->one();
    if($model==null){
      $model=new BookingEarlyDepartureAssets;
      $model->booking_id=$modelBooking->id;
    }else{
      $model->oldPhoto=$model->photo;
      $model->oldBatterySwitch=$model->battery_switch;
      $model->oldRegCardLocation=$model->registration_card_location;
      $model->oldKeysOfBoat=$model->keys_of_the_boat;
      $model->oldPetroGuage=$model->petro_guage;
    }

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>'Information Saved'];
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Attention'),'msg'=>$val];
              }
            }
          }
        }
      }
			echo json_encode($msg);
      exit;
    }

    return $this->renderAjax('activity_early_departure_assets', [
      'modelBooking' => $modelBooking,
      'model' => $model,
    ]);
  }

  /**
  * Update status of the booking
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionActivityStatus($id,$status)
  {
    $this->checkAdmin();
    $model = $this->findModel($id);
    $model->updateStatus($status);
    echo "done";
    exit;
  }

  /**
  * Creates a new booking for user from dashboard.
  * @return mixed
  */
  public function actionInOut($id,$rb=null)
  {
    $this->checkAdmin();
    $modelBooking = $this->findModel($id);
    if($modelBooking->bookingActivity!=null){
      $model=$modelBooking->bookingActivity;
      $model->oldBillFile=$model->bill_image;
      if($model->hide_other==1){
        return $this->redirect(['booking/view-in-out','id'=>$modelBooking->id]);
        exit;
      }
    }else{
      $model = new BookingActivity;
      $model->booking_id=$modelBooking->id;
      $model->boat_provided=$modelBooking->boat_id;
      $model->fuel_chargeable=1;
      if($modelBooking->bbq==1)$model->charge_bbq=1;
      if($modelBooking->captain==1)$model->charge_captain=1;
    }

    $model->booking_exp=$modelBooking->booking_exp;

    if($modelBooking->status!=1){
      return $this->redirect(['booking/activity']);
      exit;
    }
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Activity saved successfully'));
        if($rb=='uca'){
          return $this->redirect(['booking/unclosed-activity','BookingSearch[booking_date]'=>$modelBooking->booking_date,'BookingSearch[city_id]'=>$modelBooking->city_id,'BookingSearch[port_id]'=>$modelBooking->port_id]);
        }else{
          if($modelBooking->booking_date<date("Y-m-d")){
            return $this->redirect(['booking/activity','BookingSearch[booking_date]'=>$modelBooking->booking_date,'BookingSearch[city_id]'=>$modelBooking->city_id,'BookingSearch[port_id]'=>$modelBooking->port_id]);
          }else{
            return $this->redirect(['booking/activity','BookingSearch[city_id]'=>$modelBooking->city_id,'BookingSearch[port_id]'=>$modelBooking->port_id]);
          }
        }
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('in_out_form',[
      'modelBooking'=>$modelBooking,
      'model'=>$model,
    ]);
  }

  /**
  * Creates a new booking for user from dashboard.
  * @return mixed
  */
  public function actionViewInOut($id)
  {
    $this->checkAdmin();
    $modelBooking = $this->findModel($id);
    $model=$modelBooking->bookingActivity;
    $model->booking_exp=$modelBooking->booking_exp;

    if($modelBooking->status!=1){
      return $this->redirect(['booking/activity']);
      exit;
    }

    return $this->render('in_out_view',[
      'modelBooking'=>$modelBooking,
      'model'=>$model,
    ]);
  }

  /**
  * Delete Fuel Bill from In/Our Form
  * @return mixed
  */
	public function actionDeleteBill($id)
	{
		$this->checkLogin();
		$model=$this->findActivityModel($id);
		if($model!=null){
			$msg=[];
      if($model->bill_image!=null){// && file_exists(Yii::$app->params['fuelbill_abs_path'].$model->bill_image)
        //unlink(Yii::$app->params['fuelbill_abs_path'].$model->bill_image);
        $connection = \Yii::$app->db;
				$connection->createCommand("update ".$model->tableName()." set bill_image='' where id=:id",[':id'=>$model->id])->execute();
				$msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Photo deleted successfully')];
      }else{
        $msg['error']=['heading'=>Yii::t('app','Attention'),'msg'=>Yii::t('app','No photo found!')];
      }
			echo json_encode($msg);
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
    exit;
	}

  /**
  * Creates a new model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionOldCreate($user_id=null)
  {
    $this->checkLogin();
    $searchModel = new OldBookingForm;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $dataProvider->pagination=false;

    if($user_id==null){
      $user_id=Yii::$app->user->identity->id;
    }
    $model = new Booking;
    $model->booking_source='member';
    if(Yii::$app->user->identity->user_type==1){
      $model->scenario='admbook';
    }else{
      $model->booking_type=0;
    }
    $model->user_id=$user_id;
    $model->status=1;

    if ($model->load(Yii::$app->request->post())) {
      $msg=[];
      if($model->save()){
        $model->sendUserEmail();
        $model->sendAdminEmail();
        $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Booking saved successfully')];
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Booking saved successfully'));
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>$val];
              }
            }
          }
        }
      }
      echo json_encode($msg);
      exit;
    }

    return $this->render('old_create', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate($user_id=null)
  {
    $this->checkLogin();
    $searchModel = new BookingForm;
    $searchModel->city_id=Yii::$app->user->identity->profileInfo->city_id;
    $searchModel->load(Yii::$app->request->queryParams);
    if($searchModel->port_id==null){
      $searchModel->port_id=Yii::$app->appHelperFunctions->getFirstActiveCityMarina($searchModel->city_id)['id'];
    }

    if($user_id==null){
      $user_id=Yii::$app->user->identity->id;
    }
    $model = new Booking;
    $model->booking_source='member';
    if(Yii::$app->user->identity->user_type==1){
      $model->scenario='admbook';
    }else{
      $model->booking_type=0;
    }
    $model->user_id=$user_id;
    $model->status=1;

    if ($model->load(Yii::$app->request->post())) {
      $msg=[];
      if($model->save()){
        $model->sendUserEmail();
        $model->sendAdminEmail();
        $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Booking saved successfully')];
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Booking saved successfully'));
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>$val];
              }
            }
          }
        }
      }
      echo json_encode($msg);
      exit;
    }

    return $this->render('create', [
      'searchModel' => $searchModel,
      'model' => $model,
    ]);
  }

  /**
  * Loads boat selection for special boat
  * @return mixed
  */
  public function actionNightDriveBoatSelection($source,$marina_id,$boat_id,$date)
  {
    $this->checkLogin();
    return $this->renderAjax('special_boat_selection',[
      'marina_id'=>$marina_id,
      'boat_id'=>$boat_id,
      'date'=>$date,
      'source'=>$source,
    ]);
  }

  /**
  * Creates a new booking for user from dashboard.
  * @return mixed
  */
  public function actionNewUserBooking($user_id=null)
  {
    $this->checkLogin();
    $model = new NewUserBooking();
    if($user_id!=null){
      $user=User::find()->where(['id'=>$user_id,'trashed'=>0])->one();
      $model->user_id=$user_id;
      $model->username=$user->FirstLastName;
    }
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('user_booking_form',[
      'model'=>$model,
    ]);
  }

  /**
  * Creates a new booking for user from dashboard.
  * @return mixed
  */
  public function actionOperationCreate()
  {
    $this->checkLogin();
    $model = new NewUserBooking();
    $model->date=date("Y-m-d");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('operation_booking_form',[
      'model'=>$model,
    ]);
  }

  public function actionAssign($id)
  {
    $this->checkAdmin();
    $subQueryAdmin = User::find()->select('id')->where(['!=','user_type',0]);
    $bookingModel = Booking::find()->where(['id'=>$id,'user_id'=>$subQueryAdmin,'trashed'=>0])->one();
    if($bookingModel!=null){
      $model = new AdminBookingAssign();
      $model->booking_id = $bookingModel['id'];
      $model->booking_type = $bookingModel['booking_type'];
      $model->booking_comments = $bookingModel['booking_comments'];
      $model->from_user_id = $bookingModel['user_id'];
      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          echo 'success';
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  echo $val;
                  die();
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('_assign_admin_booking', [
        'bookingModel' => $bookingModel,
        'model' => $model,
      ]);
    }else{
      throw new NotFoundHttpException('No request found!');
    }
  }

  public function actionSuggestionSummary($user_id=null,$city_id,$date)
  {
    $this->checkLogin();
    if($user_id==null)$user_id=Yii::$app->user->identity->id;
    $allowedCities=Yii::$app->appHelperFunctions->getUserAllowedCities($user_id);
    if(Yii::$app->user->identity->user_type==0 && in_array($city_id,$allowedCities)){
      return $this->renderAjax('suggestion_summary',['user_id'=>$user_id,'city_id'=>$city_id,'date'=>$date]);
    }elseif(Yii::$app->user->identity->user_type!=0){
      return $this->renderAjax('suggestion_summary',['user_id'=>$user_id,'city_id'=>$city_id,'date'=>$date]);
    }else{
      return '';
    }
  }

  public function actionSuggestionTimeBoats($user_id=null,$city_id,$marina_id,$date,$slotTitle,$time_id,$marinaName)
  {
    $this->checkLogin();
    if($user_id==null)$user_id=Yii::$app->user->identity->id;

    $user=User::findOne($user_id);
    if($user->canBookBoat==false){
      $fuelCredit = $user->profileInfo->credit_balance;
      Yii::$app->getSession()->setFlash('warning', Yii::t('app','Dear Captain, Your current balance is '.$fuelCredit.' AED. Please deposit funds by clicking deposit funds. Enjoy Boating With Relief'));
      return $this->redirect(['site/index']);
    }

    return $this->renderAjax('suggestion_summary_time_slot_boats',['user'=>$user,'city_id'=>$city_id,'marina_id'=>$marina_id,'date'=>$date,'slotTitle'=>$slotTitle,'time_id'=>$time_id,'marinaName'=>$marinaName]);
  }

  /**
  * Creates a new Booking model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreateFromSuggestion($user_id=null)
  {
    $this->checkLogin();
    if($user_id==null)$user_id=Yii::$app->user->identity->id;
    $user=User::findOne($user_id);
    if($user->canBookBoat==false){
      $fuelCredit = $user->profileInfo->credit_balance;
      Yii::$app->getSession()->setFlash('warning', Yii::t('app','Dear Captain, Your current balance is '.$fuelCredit.' AED. Please deposit funds by clicking deposit funds. Enjoy Boating With Relief'));
      return $this->redirect(['site/index']);
    }
    $msg=[];
    $model = new BookingSuggestionForm();
    $model->user_id=$user_id;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Booking Confirmed. Thank you')];
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>$val];
              }
            }
          }
        }
      }
    }
    echo json_encode($msg);
    exit;
  }

  /**
  * Update Admin Comments
  * @param integer $id
  * @return mixed
  */
  public function actionUpdateComments($id)
  {
    $this->checkAdmin();

    $modelBooking = $this->findModel($id);
    $model = new BookingUpdateHistory;
    $model->booking_id=$modelBooking->id;
    $model->old_booking_type=$modelBooking->booking_type;
    $model->old_booking_comments=$modelBooking->booking_comments;
    $model->new_booking_type=$modelBooking->booking_type;
    $model->new_booking_comments=$modelBooking->booking_comments;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('_comments_update_form', [
      'model' => $model,
    ]);
  }

  public function actionDelete($id,$p='a')
  {
    $this->checkLogin();
    $model=$this->findModel($id);
    if(date("Y-m-d",strtotime($model->booking_date))>=date("Y-m-d") && $model->status==1){
      if($model->softDelete()){
        if(Yii::$app->request->isAjax){
          $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Booking Deleted. Thank you')];
          echo json_encode($msg);
          exit;
        }else{
          Yii::$app->getSession()->setFlash('success', Yii::t('app','Booking Deleted. Thank you'));
        }
      }else{
        if(Yii::$app->request->isAjax){
          $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','Booking is not deleted')];
          echo json_encode($msg);
          exit;
        }else{
          Yii::$app->getSession()->setFlash('error', Yii::t('app','Booking is not deleted'));
        }
      }
    }else{
      if(Yii::$app->request->isAjax){
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','You can not delete a booking on same day')];
        echo json_encode($msg);
        exit;
      }else{
        Yii::$app->getSession()->setFlash('error', Yii::t('app','You can not delete a booking on same day.'));
      }
    }
    return $this->redirect($p=='d' ? ['site/index'] : ($p=='a' ? ['booking/all'] : ['booking/index']));
  }

  /**
  * Finds the Booking model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Booking the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    $user_id='';
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }
    if (($model = Booking::find()->where(['id'=>$id,'trashed'=>0])->andFilterWhere(['user_id'=>$user_id])->one()) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

  /**
  * Finds the Booking model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Booking the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findActivityModel($id)
  {
    $user_id='';
    if(Yii::$app->user->identity->user_type==0){
      $user_id=Yii::$app->user->identity->id;
    }
    if (($model = BookingActivity::find()->where(['id'=>$id])->andFilterWhere(['user_id'=>$user_id])->one()) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
