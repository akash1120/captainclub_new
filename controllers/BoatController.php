<?php

namespace app\controllers;

use Yii;
use app\models\Boat;
use app\models\BoatSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* BoatController implements the CRUD actions for Boat model.
*/
class BoatController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Boat models.
  * @return mixed
  */
  public function actionIndex($listType=0)
  {
    $this->checkAdmin();
    $searchModel = new BoatSearch();
    $searchModel->listType=$listType;
    $searchModel->special_boat=$listType==0 ? [0,2] : $listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'listType' => $listType,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Boat model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkAdmin();
    $model = new Boat();
    $model->status=1;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index','listType'=>$model->special_boat]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing Boat model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->oldImage=$model->image;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index','listType'=>$model->special_boat]);
      }else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
      }
    }

    return $this->render('update', [
        'model' => $model,
    ]);
  }

  /**
  * Finds the Boat model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Boat the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Boat::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
