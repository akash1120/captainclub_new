<?php
namespace app\controllers;

use Yii;
use Mailgun\Mailgun;
use app\models\BlackListedEmail;
use app\models\DroppedEmail;
use app\models\Newsletter;
use app\models\NewsletterEmailSent;
use app\models\User;

/**
 * MailGunController implements the mailgun web hooks.
 */
class MailGunController extends DefController
{
	public function beforeAction($action)
	{
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
	
	public function actionStats()
	{
		$responseMsg='';
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			if(isset($_POST['timestamp']) && isset($_POST['token']) && isset($_POST['signature']) && $this->verify(Yii::$app->params['mailGunApi'], $_POST['token'], $_POST['timestamp'], $_POST['signature']))
			{
				if($_POST['event'] == 'complained' || $_POST['event'] == 'bounced' || $_POST['event'] == 'dropped' || $_POST['event'] == 'unsubscribed'){
					
					switch ($_POST['event']) {
						case "complained":
							$reason = "[Mailgun][".$_POST['event']."] Spam Complaint";
							break;
						case "bounced":
							$reason = "[Mailgun][".$_POST['event']."] Bounced Email";
							break;
						case "dropped":
							$reason = "[Mailgun][".$_POST['event']."] Dropped Email";
							break;
						default:
							$reason = "[Mailgun][".$_POST['event']."] Unsubscribed";
					}
					$description = $_POST['description'];
					$reason.=' ('.$description.')';
					$connection = \Yii::$app->db;

					//Saving Bounce
					if($_POST['event'] == 'bounced'){
					
						$blackListed=BlackListedEmail::find()->where(['email' => $_POST['recipient']])->one();
						if($blackListed==null){
							$blackListed=new BlackListedEmail;
							$blackListed->email = $_POST['recipient'];
							$blackListed->reason = $reason;
							$blackListed->save();
						}else{
							if($blackListed->trashed=1){
								$connection->createCommand()->update(BlackListedEmail::tableName(), ['trashed' => 0], 'id=:id', [':id' => $blackListed->id])->execute();
							}
						}
						$this->updateTracking($_POST,'bounced');
					}
					
					//Saving Dropped
					if($_POST['event'] == 'dropped'){

						$softBounced=DroppedEmail::find()->where(['email' => $_POST['recipient']])->one();
						if($softBounced==null){
							$softBounced=new DroppedEmail;
							$softBounced->email = $_POST['recipient'];
							$softBounced->reason = $reason;
							$softBounced->save();
						}else{
							if($softBounced->trashed=1){
								$connection->createCommand()->update(DroppedEmail::tableName(), ['trashed' => 0], 'id=:id', [':id' => $softBounced->id])->execute();
							}
						}
						$this->updateTracking($_POST,'dropped');
					}
					$responseMsg = 'Remove Received.';
				}
				
				//Updating delivery
				if($_POST['event'] == 'delivered') {
					$responseMsg = 'Delivery Received.';
					$this->updateTracking($_POST,'delivered');
				}
				
				//Updating open
				if($_POST['event'] == 'opened') {
					$responseMsg = 'Open Received.';
					$this->updateTracking($_POST,'opened');
				}
				
				//Updating click
				if($_POST['event'] == 'clicked') {
					$responseMsg = 'Click Received.';
					$this->updateTracking($_POST,'clicked');
				}
			}
		}
		Yii::$app->response->statusCode = 200;
		Yii::$app->response->statusText = 'Success';
		Yii::$app->response->content = $responseMsg;
		Yii::$app->response->send();
	}
	
	private function verify($apiKey, $token, $timestamp, $signature)
	{
		//check if the timestamp is fresh
		if (abs(time() - $timestamp) > 15) {
			return false;
		}
	
		//returns true if signature is valid
		return hash_hmac('sha256', $timestamp.$token, $apiKey) === $signature;
	}
	
	public function updateTracking($post,$type)
	{
		if(isset($post['tag']) && $post['tag']!=null){
			$tag=$post['tag'];
			//Newsletter Stats
			if(strpos($tag,'newsletter-')){
				$newsletterId = str_replace("newsletter-","",$tag);
				
				//Updating click
				if($_POST['event'] == 'clicked') {
					$connection = \Yii::$app->db;
					$connection->createCommand("update ".NewsletterEmailSent::tableName()." set clicks=(clicks+1) where email=:email and newsletter_id=:newsletter_id",[':email'=>$_POST['recipient'],':newsletter_id'=>$newsletterId])->execute();
					
					$connection->createCommand("update ".Newsletter::tableName()." set clicks=(clicks+1) where id=:id",[':id'=>$newsletterId])->execute();
				}
				//Updating open
				if($_POST['event'] == 'opened') {
					$connection = \Yii::$app->db;
					$connection->createCommand("update ".NewsletterEmailSent::tableName()." set opens=(opens+1) where email=:email and newsletter_id=:newsletter_id",[':email'=>$_POST['recipient'],':newsletter_id'=>$newsletterId])->execute();
					
					$connection->createCommand("update ".Newsletter::tableName()." set opens=(opens+1) where id=:id",[':id'=>$newsletterId])->execute();
				}
				//Updating delivery
				if($_POST['event'] == 'delivered') {
					$connection = \Yii::$app->db;
					$connection->createCommand("update ".NewsletterEmailSent::tableName()." set deliveries=(deliveries+1) where email=:email and newsletter_id=:newsletter_id",[':email'=>$_POST['recipient'],':newsletter_id'=>$newsletterId])->execute();
					
					$connection->createCommand("update ".Newsletter::tableName()." set deliveries=(deliveries+1) where id=:id",[':id'=>$newsletterId])->execute();
					$responseMsg = 'Delivery Received.';
				}
			}
		}
		
		if(isset($post['campaign_id']) && $post['campaign_id']!=null){
			$connection = \Yii::$app->db;
			$newsletterId=$post['campaign_id'];
			//Updating click
			if($post['event'] == 'clicked') {
				$connection->createCommand("update ".NewsletterEmailSent::tableName()." set clicks=(clicks+1) where email=:email and newsletter_id=:newsletter_id",[':email'=>$post['recipient'], ':newsletter_id' => $newsletterId])->execute();
				$connection->createCommand("update ".Newsletter::tableName()." set clicks=(clicks+1) where id=:id",[':id'=>$newsletterId])->execute();
			}
			//Updating open
			if($post['event'] == 'opened') {
				$connection->createCommand("update ".NewsletterEmailSent::tableName()." set opens=(opens+1) where email=:email and newsletter_id=:newsletter_id",[':email'=>$post['recipient'], ':newsletter_id' => $newsletterId])->execute();
				$connection->createCommand("update ".Newsletter::tableName()." set opens=(opens+1) where id=:id",[':id'=>$newsletterId])->execute();
			}
			//Updating delivery
			if($post['event'] == 'delivered') {
				$connection->createCommand("update ".NewsletterEmailSent::tableName()." set deliveries=1 where email=:email and newsletter_id=:newsletter_id",[':email'=>$post['recipient'], ':newsletter_id' => $newsletterId])->execute();
				$responseMsg = 'Delivery Received.';
			}
			//Updating dropped
			if($post['event'] == 'dropped') {
				$connection->createCommand("update ".NewsletterEmailSent::tableName()." set soft_bounced=1 where email=:email and newsletter_id=:newsletter_id",[':email'=>$post['recipient'], ':newsletter_id' => $newsletterId])->execute();
				$responseMsg = 'Drop Received.';
			}
			//Updating bounced
			if($post['event'] == 'bounced') {
				$connection->createCommand("update ".NewsletterEmailSent::tableName()." set hard_bounced=1 where email=:email and newsletter_id=:newsletter_id",[':email'=>$post['recipient'], ':newsletter_id' => $newsletterId])->execute();
				$responseMsg = 'Bounce Received.';
			}
		}
	}
}
