<?php

namespace app\controllers;

use Yii;
use app\models\DamageCheckItems;
use app\models\DamageCheckItemsSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* DamageCheckItemsController implements the CRUD actions for DamageCheckItems model.
*/
class DamageCheckItemController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all DamageCheckItems models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new DamageCheckItemsSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new DamageCheckItems;
    return $model;
  }

  /**
  * Finds the DamageCheckItems model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return DamageCheckItems the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = DamageCheckItems::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
