<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Booking;
use app\models\BookingSearch;
use app\models\BookingOperationComments;
use app\models\ClaimOrder;
use app\models\ClaimAmountForm;
use app\models\ClaimOrderSearch;
use app\models\ClaimOrderItemSearch;
use app\models\ClaimOrderComments;

class ClaimController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index','add-comments','claim-bulk','claim-amount'],
        'rules' => [
          [
            'actions' => ['index','add-comments','claim-bulk','claim-amount'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Lists all Booking models for claiming.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();

    return $this->render('claim', [
    ]);
  }

  /**
  * Update Admin Comments
  * @param integer $id
  * @return mixed
  */
  public function actionAddComments($id,$process)
  {
    $this->checkAdmin();

    $modelBooking = $this->findBookingModel($id);
    $model = new BookingOperationComments;
    $model->booking_id=$modelBooking->id;
    $model->user_type=1;
    $model->process_type=$process;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        echo 'success';
        exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('_comments_update_form', [
      'model' => $model,
    ]);
  }

  public function actionClaimBulk()
  {
    $this->checkAdmin();
    $selection=Yii::$app->request->post('selection');
    $city_id=Yii::$app->request->post('city_id');
    $port_id=Yii::$app->request->post('port_id');
    $comments=Yii::$app->request->post('comments');
    if($selection!=null){
      $order=new ClaimOrder;
      $order->user_id=Yii::$app->user->identity->id;
      $order->city_id=$city_id;
      $order->marina_id=$port_id;
      $order->items=$selection;
      $order->process_type=0;
      $order->user_type=1;
      $order->comments=$comments;
      $order->save();
      $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Booking claimed successfully')];
    }else{
      $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','Please select bookings to claim')];
    }
    echo json_encode($msg);
    exit;
  }

  public function actionClaimAmount()
  {
    $this->checkAdmin();
    $msg=[];
    $model = new ClaimAmountForm;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Claimed successfully')];
      }else{
        foreach($model->getErrors() as $error){
          if(count($error)>0){
            foreach($error as $key=>$val){
              $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
            }
          }
        }
      }
    }
    echo json_encode($msg);
    exit;
  }

  /**
  * Lists all Booking models.
  * @return mixed
  */
  public function actionClaimed($list='pending')
  {
    $this->checkAdmin();
    $searchModel = new ClaimOrderSearch();
    $searchModel->list_type=$list;
    if($list=='approved'){
      $searchModel->status = 1;
    }else{
      $searchModel->status = 0;
    }

    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('claimed', [
      'list' => $list,
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionApproveBulk()
  {
    $this->checkAdmin();
    $selection=Yii::$app->request->post('selection');
    if($selection!=null){
      $connection = \Yii::$app->db;
      $connection->createCommand()
      ->update(
        ClaimOrder::tableName(),
        [
          'status'=>1,
        ],
        ['id'=>$selection,'status'=>0]
        )->execute();
        $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Claims approved successfully')];

      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','Please select claims to approve')];
      }
      echo json_encode($msg);
      exit;
    }

    /**
    * Lists all Booking models.
    * @return mixed
    */
    public function actionViewClaim($id)
    {
      $this->checkAdmin();
      $model=$this->findClaimOrderModel($id);
      $searchModel = new ClaimOrderItemSearch();
      $searchModel->order_id = $model->id;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('claim_order_detail', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
      ]);
    }

    /**
    * Update Admin Comments
    * @param integer $id
    * @return mixed
    */
    public function actionAddOrderComments($id)
    {
      $this->checkAdmin();

      $modelBooking = $this->findClaimOrderModel($id);
      $model = new ClaimOrderComments;
      $model->claim_order_id=$modelBooking->id;
      $model->user_type=1;
      $model->process_type=1;
      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          echo 'success';
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  echo $val;
                  die();
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('_order_comments_update_form', [
        'model' => $model,
      ]);
    }

    /**
    * Finds the BookingInOut model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return BookingInOut the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findClaimOrderModel($id)
    {
      if (($model = ClaimOrder::findOne($id)) !== null) {
        return $model;
      } else {
        throw new NotFoundHttpException('The requested page does not exist.');
      }
    }

    /**
    * Finds the Booking model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return Booking the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findBookingModel($id)
    {
      if (($model = Booking::find()->where(['id'=>$id])->one()) !== null) {
        return $model;
      }

      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }
