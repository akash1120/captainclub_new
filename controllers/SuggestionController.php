<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\City;
use app\models\Marina;
use app\models\TimeSlot;
use app\models\Boat;
use app\models\User;
use app\models\Contract;
use app\models\ContractMember;
use app\models\Booking;
use app\models\BookingSearch;
use app\models\BulkBooking;
use app\models\BoatToTimeSlot;
use app\models\BoatAvailableDays;

class SuggestionController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['member-search'],
        'rules' => [
          [
            'actions' => ['member-search'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  public function actionUserBookingHistory($user_id,$status_type,$status)
  {
    $this->checkAdmin();
    $searchModel = new BookingSearch();
    $searchModel->user_id=$user_id;
    $searchModel->listType='history';
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->renderAjax('member_booking_history', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * search all the members
   * @return mixed
   */
  public function actionMemberSearch($query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    $results=User::find()
      ->select([User::tableName().'.id','username','firstname','lastname','email'])
      ->where([
        'and',
        [
          'or',
          ['like','username',$query],
          ['like','firstname',$query],
          ['like','lastname',$query],
          ['like','email',$query],
        ],
        [User::tableName().'.status'=>1],
        ['>=',Contract::tableName().'.end_date',date("Y-m-d")]
      ])
			->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
      ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $usersName=$result['firstname'].($result['lastname']!='' ? ' '.$result['lastname'] : '');
        if($usersName==''){
          $usersName=$result['username'];
        }
        if($usersName==''){
          $usersName=$result['email'];
        }
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($usersName),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  public function actionBoatsList($city_id=null,$marina_id=null)
  {
    $results=Boat::find()
			->select([
				Boat::tableName().'.id',
				'name',
			])
			->where([Boat::tableName().'.status'=>1,Boat::tableName().'.trashed'=>0])
			->andFilterWhere(['city_id'=>$city_id,'port_id'=>$marina_id])
			->orderBy([Boat::tableName().'.name'=>SORT_ASC])
			->asArray()
			->all();
    if($results!=null){
      foreach($results as $result){
        echo '<option value="'.$result['id'].'">'.$result['name'].'</option>';
      }
    }
    exit;
  }

  public function actionBoatInfo($id)
  {
    $this->checkLogin();
    $model = $this->findBoatModel($id);
    return $this->renderAjax('/boat/info', [
      'model' => $model,
    ]);
  }

  public function actionFreeBoats($marina,$date,$time,$showbulk="")
  {
    $this->checkLogin();
    if($marina!=null && $date!=null && $time!=null){

      $boatsArray=[];

      $dayOfWeek=Yii::$app->helperFunctions->WeekendDay($date);
      $subQueryAdminStaff=User::find()->select(['id'])->where(['!=','user_type',0]);
      $subQueryTiming=BoatToTimeSlot::find()->select(['boat_id'])->where(['time_slot_id'=>$time]);
      $subQueryAvailability=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);
      $boats=Boat::find()
        ->where([
          'status'=>1,
          'port_id'=>$marina,
        ])
        ->andWhere(['id'=>$subQueryTiming])
        ->andWhere(['id'=>$subQueryAvailability])
        ->asArray()->all();
      if($boats!=null){
        foreach($boats as $boat){
          $existingBooking=Booking::find()
          ->where([
            'and',
            [
              'booking_date'=>$date,
              'boat_id'=>$boat['id'],
              'port_id'=>$boat['port_id'],
              'booking_time_slot'=>$time,
              'status'=>1,
              'trashed'=>0,
            ],
          ])->asArray()->one();
          if($existingBooking!=null){
            if($existingBooking['is_bulk']==1){
              if($showbulk!='no'){
                $reason=Yii::$app->helperFunctions->adminBookingTypes[$existingBooking['booking_type']].':'.$existingBooking['booking_comments'];
                $boatsArray[]=['id'=>$boat['id'],'name'=>$boat['name'],'status'=>' (Bulk Booking - '.$reason.')','order'=>3];
              }
            }elseif(in_array($existingBooking['user_id'],Yii::$app->appHelperFunctions->adminIdzArr)){
              $reason=($existingBooking['booking_type']>0 ? Yii::$app->helperFunctions->adminBookingTypes[$existingBooking['booking_type']] : '').':'.$existingBooking['booking_comments'];
              $boatsArray[]=['id'=>$boat['id'],'name'=>$boat['name'],'status'=>' (Admin - '.$reason.')','order'=>2];
            }
          }else{
            $bulkBooked=BulkBooking::find()->where(['and',['boat_id'=>$boat['id'],'trashed'=>0],['<=','start_date',$date],['>=','end_date',$date]])->one();
            if($bulkBooked!=null){
              $reason=Yii::$app->helperFunctions->adminBookingTypes[$bulkBooked['booking_type']].':'.$bulkBooked['booking_comments'];
              $boatsArray[]=['id'=>$boat['id'],'name'=>$boat['name'],'status'=>' (Bulk Booking - '.$reason.')','order'=>3];
            }else{
              $boatsArray[]=['id'=>$boat['id'],'name'=>$boat['name'],'status'=>' (Free)','order'=>1];
            }
          }
        }
      }
    }
    if($boatsArray!=null){
      usort($boatsArray, function($a, $b) {
          return $a['order'] - $b['order'];
      });
      echo '<option value="">Select</option>';
      foreach($boatsArray as $boatArray){
        echo '<option value="'.$boatArray['id'].'">'.$boatArray['name'].' '.$boatArray['status'].'</option>';
      }

    }
    exit;
  }

  public function actionMemberBookings($id)
  {
    $this->checkLogin();
    $bookings=Booking::find()
    ->select([
      Booking::tableName().'.id',
      Booking::tableName().'.user_id',
      Booking::tableName().'.city_id',
      'city_name'=>City::tableName().'.name',
      Booking::tableName().'.port_id',
      'marina_name'=>Marina::tableName().'.name',
      Booking::tableName().'.boat_id',
      'boat_name'=>Boat::tableName().'.name',
      Booking::tableName().'.booking_date',
      Booking::tableName().'.booking_time_slot',
      'time_name'=>TimeSlot::tableName().'.name',
    ])
    ->innerJoin(City::tableName(),City::tableName().".id=".Booking::tableName().".city_id")
    ->innerJoin(Marina::tableName(),Marina::tableName().".id=".Booking::tableName().".port_id")
    ->innerJoin(TimeSlot::tableName(),TimeSlot::tableName().".id=".Booking::tableName().".booking_time_slot")
    ->innerJoin(Boat::tableName(),Boat::tableName().".id=".Booking::tableName().".boat_id")
    ->where([
      'and',
      [
        Booking::tableName().'.user_id'=>$id,
        Booking::tableName().'.status'=>1,
        Booking::tableName().'.trashed'=>0
      ],
      ['>=','booking_date',date("Y-m-d")]
    ])
    ->asArray()->all();
    if($bookings!=null){
      $n=1;
      foreach($bookings as $booking){
        $bookingDetail =$booking['city_name'].' / '.$booking['marina_name'].' / '.$booking['boat_name'];
        $bookingDetail.=' <small>'.Yii::$app->formatter->asDate($booking['booking_date']).' - '.$booking['time_name'].'</small>';
        echo ''.($n>1 ? '<hr style="height:1px; margin:2px 0;" />' : '').'
        <div class="row">
          <div class="col-sm-12">
            <label>
              <input class="b_rb" type="radio" name="BoatSwap[booking_id]" data-booking_id="'.$booking['id'].'" data-city_id="'.$booking['city_id'].'" data-marina_id="'.$booking['port_id'].'" data-booking_date="'.$booking['booking_date'].'" data-time_id="'.$booking['booking_time_slot'].'" data-booking="'.$bookingDetail.'" value="'.$booking['id'].'">&nbsp;
              '.$bookingDetail.'
            </label>
          </div>
        </div>';
        $n++;
      }
    }else{
      echo '<div class="alert alert-danger">No future bookings found!</div>';
    }
    exit;
  }

  /**
  * Loads boat selection for special boat
  * @return mixed
  */
  public function actionSubBoatSelection($marina_id,$date,$fld)
  {
    $this->checkLogin();
    return $this->renderAjax('sub_boat_selection',[
      'marina_id'=>$marina_id,
      'date'=>$date,
      'fld'=>$fld,
    ]);
  }

  /**
  * Finds the Boat model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Boat the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findBoatModel($id)
  {
    if (($model = Boat::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
