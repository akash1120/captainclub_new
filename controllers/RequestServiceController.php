<?php

namespace app\controllers;

use Yii;
use app\models\RequestService;
use app\models\RequestServiceSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* RequestServiceController implements the CRUD actions for RequestService model.
*/
class RequestServiceController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all RequestService models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new RequestServiceSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new RequestService model.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkAdmin();
    $model = new RequestService();
    $model->req_date = date("Y-m-d");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          echo 'success';
          exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
                exit;
              }
            }
          }
        }
      }
    }

    return $this->renderAjax('_form', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing RequestService model.
  * @return mixed
  */
  public function actionUpdate($id)
  {
    $this->checkAdmin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          echo 'success';
          exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
                exit;
              }
            }
          }
        }
      }
    }

    return $this->renderAjax('_form', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing RequestService model.
  * @return mixed
  */
  public function actionAdminRemarks($id)
  {
    $this->checkAdmin();
    $model = $this->findModel($id);
    $model->admin_action_date=date("Y-m-d");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          echo 'success';
          exit;
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                echo $val;
                die();
                exit;
              }
            }
          }
        }
      }
    }

    return $this->renderAjax('_action_form', [
      'model' => $model,
    ]);
  }

  /**
  * Enable/Disable an existing model.
  * If action is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionChangeStatus($id,$status)
  {
    $this->checkAdmin();
    $model = $this->findModel($id);
    $model->updateStatus($status);
    $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Request marked as '.Yii::$app->operationHelperFunctions->requestStatus[$status])];
    echo json_encode($msg);
    exit;
  }

  /**
  * Finds the RequestService model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return RequestService the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = RequestService::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
