<?php

namespace app\controllers;

use Yii;
use app\models\BookingAlert;
use app\models\BookingAlertSearch;
use app\models\BookingAlertTiming;
use app\models\BookingAlertEmails;
use app\models\Booking;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* BookingAlertController implements the CRUD actions for BookingAlert model.
*/
class BookingAlertController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all BookingAlert models of type Warning.
  * @return mixed
  */
  public function actionWarning($listType='active')
  {
    $this->checkAdmin();
    $searchModel = new BookingAlertSearch();
    $searchModel->alert_type=1;
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new BookingAlert model of type Warning.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreateWarning()
  {
    $this->checkAdmin();
    $model = new BookingAlert();
    $model->alert_type=1;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        return $this->redirect(['send-email','id'=>$model->id]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Lists all BookingAlert models of type Hold.
  * @return mixed
  */
  public function actionHold($listType='active')
  {
    $this->checkAdmin();
    $searchModel = new BookingAlertSearch();
    $searchModel->alert_type=2;
    $searchModel->listType=$listType;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new BookingAlert model of type Hold.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreateHold()
  {
    $this->checkAdmin();
    $model = new BookingAlert();
    $model->alert_type=2;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        return $this->redirect(['send-email','id'=>$model->id]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Sends Email
  * @return mixed
  */
  public function actionSendEmail($id)
  {
    $this->checkAdmin();
    $alert=$this->findModel($id);
    if($alert!=null){
      $alertTimings=[];
      $sentEmails=BookingAlertEmails::find()->select(['booking_id'])->where(['alert_id'=>$alert->id]);
      $alertTimings=BookingAlertTiming::find()->select(['time_slot_id'])->where(['alert_id'=>$alert->id]);
      $bookings=Booking::find()
      ->where([
        'and',
        ['booking_date'=>$alert->alert_date,'port_id'=>$alert->port_id,'trashed'=>0,'status'=>1],
        ['in','booking_time_slot',$alertTimings],
        ['not in','id',$sentEmails]
        ])->limit(1)->all();
        if($bookings!=null){
          foreach($bookings as $booking){
            $bookingMember=$booking->member;

            $htmlBody=nl2br($alert->email_message);
            $textBody=$alert->email_message;
            $message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
            ->setSubject('Attention Captain')
            ->setTo($bookingMember->email)
            ->send();

            //Bulk Booking - SMS
            if($bookingMember->profileInfo->profile_updated==1 && $bookingMember->profileInfo->mobile!=null && $bookingMember->profileInfo->mobile!=''){
              $mobileNumber=Yii::$app->helperFunctions->fullMobileNumber($bookingMember->profileInfo->mobile);
              if($mobileNumber!=null && $mobileNumber!=''){
                $smsMsg ="Dear Captain\n";
                $smsMsg.="A ".($alert->alert_type==1 ? 'Warning' : 'Hold')." alert was placed for your booking on \"".Yii::$app->formatter->asDate($alert->alert_date)."\", please check your email for more details.\n";
                $smsMsg.="Best Regards\n";
                $smsMsg.="M: ".Yii::$app->params['smsSigNumber']."\n";
                $smsMsg.="E: ".Yii::$app->params['smsSigEmail']."";
                Yii::$app->helperFunctions->sendSms($mobileNumber,$smsMsg);
              }
            }
            $emailSent=BookingAlertEmails::find()->where(['alert_id'=>$alert->id,'booking_id'=>$booking->id])->one();
            if($emailSent==null){
              $emailSent=new BookingAlertEmails;
              $emailSent->alert_id=$alert->id;
              $emailSent->booking_id=$booking->id;
              $emailSent->save();
            }
          }
          die('<script>window.location.reload();</script>');
        }else{
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".BookingAlert::tableName()." set is_sent='1' where id=:id",[':id'=>$alert->id])->execute();

          Yii::$app->getSession()->setFlash('success', Yii::t('app','Email sent successfully'));
          if($alert->alert_type==1){
            return $this->redirect(['warning']);
          }else{
            return $this->redirect(['hold']);
          }
        }
      }else{
        return $this->redirect(['site/index']);
      }
    }

    /**
    * Updates an existing BookingAlert model.
    * If update is successful, the browser will be redirected to the respective page.
    * @param integer $id
    * @return mixed
    */
    public function actionUpdate($id)
    {
      $this->checkSuperAdmin();
      $model = $this->findModel($id);

      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
          if($model->alert_type==1){
            return $this->redirect(['warning']);
          }else{
            return $this->redirect(['hold']);
          }
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  Yii::$app->getSession()->addFlash('error', $val);
                }
              }
            }
          }
        }
      }
      return $this->render('update', [
        'model' => $model,
      ]);
    }

    /**
    * Deletes an existing BookingAlert model.
    * If deletion is successful, the browser will be redirected to the respective page.
    * @param integer $id
    * @return mixed
    */
    public function actionDelete($id)
    {
      $this->checkSuperAdmin();
      $model=$this->findModel($id);
      $type=1;
      if($model!=null){
        $type=$model->alert_type;
        $model->softDelete();
      }
      Yii::$app->getSession()->setFlash('success', Yii::t('app','Information deleted successfully'));
      if($type==1){
        return $this->redirect(['warning']);
      }else{
        return $this->redirect(['hold']);
      }
    }

    /**
    * Finds the BookingAlert model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return BookingAlert the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id)
    {
      if (($model = BookingAlert::findOne($id)) !== null) {
        return $model;
      }

      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }
