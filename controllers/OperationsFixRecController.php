<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\components\helpers\DefController;

use app\models\OperationRequests;
use app\models\RequestBreakDown;
use app\models\RequestService;
use app\models\RequestInspection;
use app\models\OperationsUser;

use app\models\Booking;
use app\models\import\BookingActivity;
use app\models\OperationBooking;
use app\models\OperationBookingInOut;
use app\models\BookingInOutBookingExp;
use app\models\BookingInOutTripExp;
use app\models\BookingInOutMemberExp;
use app\models\BookingNoShow;
use app\models\BookingCancelled;
use app\models\BookingInOutChangedBoat;
use app\models\BookingInOutDamages;
use app\models\BookingActivityDamages;
use app\models\BookingInOutExtras;
use app\models\BookingActivityAddons;
use app\models\ClaimOrderItem;
use app\models\BookingOperationComments;
use app\models\ClaimOrderComments;
use app\models\ClaimOrder;

use app\models\import\User;
use app\models\import\UserProfileInfo;

class OperationsFixRecController extends DefController
{
  public function actionCreateStaff()
  {
    $results=OperationsUser::find()->where(['booking_staff_id'=>0])->asArray()->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $user=new User;
        $user->permission_group_id=0;
        $user->group_id=0;
        $user->changed_pass=1;
        $user->profile_updated=1;
        $user->username=trim($result['email']);
        $user->email=trim($result['email']).'@thecaptainsclub.ae';
        $user->mobile=trim($result['mobile']);
        $user->firstname=$result['name'];
        $user->user_type=$result['super_admin']==1 ? 1 : 2;
        $user->superadmin=$result['super_admin']==1 ? 1 : 2;
        $user->old_password=$result['password_hash'];
        $user->password_hash=Yii::$app->security->generatePasswordHash($result['password_hash']);
        $user->status=$result['status'];
        $user->start_date=$result['created_at'];
        $user->end_date='2022-07-26';
        if($user->save()){
          $userProfile=new UserProfileInfo;
          $userProfile->user_id=$user->id;
          $userProfile->mobile=trim($result['mobile']);
          $userProfile->changed_pass=1;
          $userProfile->profile_updated=1;
          $userProfile->save();

          $connection->createCommand(
            "update ".OperationsUser::tableName()." set booking_staff_id=:booking_staff_id where id=:id",
            [
              ':booking_staff_id'=>$user->id,
              ':id'=>$result['id']
            ]
          )->execute();

        }else{
          echo $result['mobile'].'<br />';
          print_r($user->getErrors());
          die();
        }
      }
    }
    echo "<b>Finished</b>";
    echo '<script>window.location.href="'.Url::to(['break-downs']).'"</script>';
    exit;
  }

  public function actionBreakDowns($lastid=null)
  {
    $results=RequestBreakDown::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(15)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $requestRow=OperationRequests::find()->where(['req_type'=>'BD','req_id'=>$result['id']])->asArray()->one();
        if($requestRow!=null){
          $connection->createCommand(
            "update ".RequestBreakDown::tableName()." set boat_id=:boat_id,completed=:completed,completed_date=:completed_date where id=:id",
            [
              ':boat_id'=>$requestRow['boat_id'],
              ':completed'=>$requestRow['completed'],
              ':completed_date'=>$requestRow['completed_date'],
              ':id'=>$result['id']
            ]
          )->execute();
        }

        $createdById=0;
        $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['created_by']])->asArray()->one();
        if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

        $updatedById=0;
        $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['updated_by']])->asArray()->one();
        if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

        $connection->createCommand(
          "update ".RequestBreakDown::tableName()." set created_by=:created_by,updated_by=:updated_by where id=:id",
          [
            ':created_by'=>$createdById,
            ':updated_by'=>$updatedById,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['break-downs','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> Moving to boat service";
      echo '<script>window.location.href="'.Url::to(['boat-service']).'"</script>';
    }
    exit;
  }

  public function actionBoatService($lastid=null)
  {
    $results=RequestService::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(15)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];
        $requestRow=OperationRequests::find()->where(['req_type'=>'S','req_id'=>$result['id']])->asArray()->one();
        if($requestRow!=null){
          $connection->createCommand(
            "update ".RequestService::tableName()." set boat_id=:boat_id,completed=:completed,completed_date=:completed_date where id=:id",
            [
              ':boat_id'=>$requestRow['boat_id'],
              ':completed'=>$requestRow['completed'],
              ':completed_date'=>$requestRow['completed_date'],
              ':id'=>$result['id']
            ]
          )->execute();
        }

        $createdById=0;
        $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['created_by']])->asArray()->one();
        if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

        $updatedById=0;
        $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['updated_by']])->asArray()->one();
        if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

        $connection->createCommand(
          "update ".RequestService::tableName()." set created_by=:created_by,updated_by=:updated_by where id=:id",
          [
            ':created_by'=>$createdById,
            ':updated_by'=>$updatedById,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['boat-service','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to boat inspection";
      echo '<script>window.location.href="'.Url::to(['boat-inspection']).'"</script>';
    }
    exit;
  }

  public function actionBoatInspection($lastid=null)
  {
    $results=RequestInspection::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(15)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];
        $requestRow=OperationRequests::find()->where(['req_type'=>'I','req_id'=>$result['id']])->asArray()->one();
        if($requestRow!=null){
          $connection->createCommand(
            "update ".RequestInspection::tableName()." set boat_id=:boat_id,completed=:completed,completed_date=:completed_date where id=:id",
            [
              ':boat_id'=>$requestRow['boat_id'],
              ':completed'=>$requestRow['completed'],
              ':completed_date'=>$requestRow['completed_date'],
              ':id'=>$result['id']
            ]
          )->execute();
        }

        $createdById=0;
        $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['created_by']])->asArray()->one();
        if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

        $updatedById=0;
        $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['updated_by']])->asArray()->one();
        if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

        $connection->createCommand(
          "update ".RequestInspection::tableName()." set created_by=:created_by,updated_by=:updated_by where id=:id",
          [
            ':created_by'=>$createdById,
            ':updated_by'=>$updatedById,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['boat-inspection','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to booking activity";
      echo '<script>window.location.href="'.Url::to(['claim-booking-items']).'"</script>';
    }
    exit;
  }

  public function actionBookingActivity($lastid=null)
  {
    $subQueryAddedActivity=BookingActivity::find()->select(['booking_id']);
    $inOutAdded=OperationBookingInOut::find()->select(['booking_id']);
    $subQueryOperations=OperationBooking::find()->select(['booking_id'])->where([
      'imported'=>0,
      //'and',
      //['id'=>$inOutAdded],
      //['not in','booking_id',$subQueryAddedActivity],
    ]);

    $results=Booking::find()
    ->where(['id'=>$subQueryOperations,'trashed'=>0])
    //->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(500)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $operationBooking=OperationBooking::find()->where(['booking_id'=>$result['id']])->asArray()->one();
        if($operationBooking!=null){
          //In Out
          $operationBookingInOut=OperationBookingInOut::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
          if($operationBookingInOut!=null){

            $bookingActivity=BookingActivity::find()->where(['booking_id'=>$result['id']])->one();
            if($bookingActivity==null){
              $bookingActivity=new BookingActivity;
              $bookingActivity->booking_id=$result['id'];
            }
            $bookingActivity->dep_time=$operationBookingInOut['dep_time'];
            $bookingActivity->arr_time=$operationBookingInOut['arr_time'];
            $bookingActivity->dep_engine_hours=$operationBookingInOut['dep_engine_hours'];
            $bookingActivity->arr_engine_hours=$operationBookingInOut['arr_engine_hours'];
            $bookingActivity->dep_ppl=$operationBookingInOut['dep_ppl'];
            $bookingActivity->arr_ppl=$operationBookingInOut['arr_ppl'];
            $bookingActivity->dep_fuel_level=$operationBookingInOut['dep_fuel_level'];
            $bookingActivity->arr_fuel_level=$operationBookingInOut['arr_fuel_level'];
            $bookingActivity->fuel_chargeable=$operationBookingInOut['fuel_chargeable'];
            $bookingActivity->no_charge_comment=$operationBookingInOut['no_charge_comment'];
            $bookingActivity->fuel_cost=$operationBookingInOut['fuel_cost']!='' ? $operationBookingInOut['fuel_cost'] : 0;
            $bookingActivity->fuel_cost_paid=$operationBookingInOut['fuel_cost_paid'];
            $bookingActivity->charge_bbq=$operationBookingInOut['bbq'];
            $bookingActivity->charge_captain=$operationBookingInOut['captain'];
            //$bookingActivity->charge_wake_boarding=$operationBookingInOut['charge_wake_boarding'];
            //$bookingActivity->charge_wake_surfing=$operationBookingInOut['charge_wake_surfing'];
            $bookingActivity->bill_image=$operationBookingInOut['bill_image'];
            $bookingActivity->damage_comments=$operationBookingInOut['damage_comments'];
            $bookingActivity->hide_other=$operationBookingInOut['hide_other'];


            $bookingStatus=1;
            //Check Stay In
            if($operationBooking['stayin']==1)$bookingStatus=2;

            //Check No show row
            $noShowStatus=BookingNoShow::find()->where(['booking_id'=>$operationBooking['id']]);
            if($noShowStatus->exists())$bookingStatus=3;

            //Check TCC Cancelled
            $tccCancelledStatus=BookingCancelled::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            //Weather
            if($tccCancelledStatus!=null && $tccCancelledStatus['remarks']==1)$bookingStatus=4;
            //Boat Not Available
            if($tccCancelledStatus!=null && $tccCancelledStatus['remarks']==2)$bookingStatus=5;

            //Check Admin
            if($operationBooking['is_admin']==1)$bookingStatus=6;

            //Check Same Day Cancel
            if($operationBooking['same_day_cancel']==1)$bookingStatus=7;

            //Check Weather Warning Cancel
            if($operationBooking['weather_warning_cancel']==1)$bookingStatus=8;

            //Check Training
            if($operationBooking['is_training']==1)$bookingStatus=9;

            //Check Emergency
            if($operationBooking['is_emergency']==1)$bookingStatus=10;

            //Check Out Of Service
            if($operationBooking['is_outofservice']==1)$bookingStatus=11;
            //////////////////Update Booking Status//////////////////
            $connection->createCommand(
              "update ".Booking::tableName()." set status=:status where id=:id",
              [
                ':status'=>$bookingStatus,
                ':id'=>$result['id'],
              ]
            )->execute();

            $bookingExperienceTblRow=BookingInOutBookingExp::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            //Smooth
            $bookingExp=1;
            if($bookingExperienceTblRow!=null && $bookingExperienceTblRow['exp_id']==1)$bookingExp=1;
            //Smooth
            if($bookingExperienceTblRow!=null && $bookingExperienceTblRow['exp_id']==2)$bookingExp=2;
            //Smooth
            if($bookingExperienceTblRow!=null && $bookingExperienceTblRow['exp_id']==3)$bookingExp=3;
            //////////////////Update Booking Experience//////////////////
            $connection->createCommand(
              "update ".Booking::tableName()." set booking_exp=:booking_exp where id=:id",
              [
                ':booking_exp'=>$bookingExp,
                ':id'=>$result['id'],
              ]
            )->execute();
            ////////////////////////////////////

            //Boat Provided
            $boatProvidedId=$operationBooking['boat_id'];
            $boatProvidedComments='';
            $boatProvidedRow=BookingInOutChangedBoat::find()->select(['boat_id','remarks'])->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            if($boatProvidedRow!=null){
              $boatProvidedId=$boatProvidedRow['boat_id'];
              $boatProvidedComments=$boatProvidedRow['remarks'];
            }
            $bookingActivity->boat_provided=$boatProvidedId;

            //Trip Experience
            $tripExperienceStatus=1;
            $tripExperienceComments='';
            $tripExperienceTblRow=BookingInOutTripExp::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            if($tripExperienceTblRow!=null){
              $tripExperienceStatus=$tripExperienceTblRow['exp_id'];
              $tripExperienceComments=$tripExperienceTblRow['remarks'];
              $tripExperienceComments.=($tripExperienceComments!='' && $tripExperienceComments!=null && $tripExperienceComments!=NULL) ? "\n\r" : '';
              $tripExperienceComments.=$tripExperienceTblRow['trip_comment'];
            }
            //////////////////Update Trip Status//////////////////
            $bookingActivity->trip_exp=$tripExperienceStatus;
            $bookingActivity->trip_exp_comments=$tripExperienceComments;
            ////////////////////////////////////

            //Member Experience
            $memberExperienceStatus=0;
            $member_exp_late_hr=0;
            $member_exp_late_min=0;
            $memberExperienceComments='';
            $memberExperienceTblRow=BookingInOutMemberExp::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            if($memberExperienceTblRow!=null){
              $memberExperienceStatus=$memberExperienceTblRow['exp_id'];
              $member_exp_late_hr=$memberExperienceTblRow['late_hr'];
              $member_exp_late_min=$memberExperienceTblRow['late_min'];
              $memberExperienceComments=$memberExperienceTblRow['late_time'];
              $memberExperienceComments.=($memberExperienceComments!='' && $memberExperienceComments!=null && $memberExperienceComments!=NULL) ? "\n\r" : '';
              $memberExperienceComments.=$memberExperienceTblRow['late_comments'];
            }
            //////////////////Update Member Status//////////////////
            $bookingActivity->member_exp=$memberExperienceStatus;
            $bookingActivity->member_exp_late_hr=$member_exp_late_hr;
            $bookingActivity->member_exp_late_min=$member_exp_late_min;
            $bookingActivity->member_exp_comments=$memberExperienceComments;
            ////////////////////////////////////

            if($operationBookingInOut['dep_crew_sign']!=''){
              $depCrewSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['dep_crew_sign']);
              $bookingActivity->dep_crew_sign=$depCrewSignFile;
            }

            if($operationBookingInOut['dep_mem_sign']!=''){
              $depMemSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['dep_mem_sign']);
              $bookingActivity->dep_mem_sign=$depMemSignFile;
            }

            if($operationBookingInOut['arr_crew_sign']!=''){
              $arrCrewSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['arr_crew_sign']);
              $bookingActivity->arr_crew_sign=$arrCrewSignFile;
            }

            if($operationBookingInOut['arr_mem_sign']!=''){
              $arrMemSignFile=Yii::$app->fileHelperFunctions->generateImageFromSignature($operationBookingInOut['arr_mem_sign']);
              $bookingActivity->arr_mem_sign=$arrMemSignFile;
            }

            $bookingActivity->overall_remarks=$boatProvidedComments;

            //Extra
            $extraAddons=BookingInOutExtras::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->all();
            if($extraAddons!=null){
              foreach($extraAddons as $extraAddon){
                $baAddon=BookingActivityAddons::find()->where(['booking_id'=>$result['id'],'addon_id'=>$extraAddon['extra_id']])->one();
                if($baAddon==null){
                  $baAddon=new BookingActivityAddons;
                  $baAddon->booking_id=$result['id'];
                  $baAddon->addon_id=$extraAddon['extra_id'];
                  $baAddon->save();
                }
              }
            }

            //Damages
            $damgesRows=BookingInOutDamages::find()->where(['booking_inout_id'=>$operationBookingInOut['id']])->asArray()->all();
            if($damgesRows!=null){
              foreach($damgesRows as $damgesRow){
                $badamage=BookingActivityDamages::find()->where(['booking_id'=>$result['id'],'item_id'=>$damgesRow['item_id']])->one();
                if($badamage==null){
                  $badamage=new BookingActivityDamages;
                  $badamage->booking_id=$result['id'];
                  $badamage->item_id=$damgesRow['item_id'];
                }
                $badamage->item_condition=$damgesRow['item_condition'];
                $badamage->item_comments=$damgesRow['item_comments'];
                $badamage->save();
              }
            }


            $claimedById=0;
            $claimedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBooking['claimed_by']])->asArray()->one();
            if($claimedByUser!=null)$claimedById=$claimedByUser['booking_staff_id'];

            $approvedById=0;
            $approvedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBooking['approved_by']])->asArray()->one();
            if($approvedByUser!=null)$approvedById=$approvedByUser['booking_staff_id'];

            $bookingActivity->claimed=$operationBooking['claimed'];
            $bookingActivity->claimed_at=$operationBooking['claimed_at'];
            $bookingActivity->claimed_by=$claimedById;
            $bookingActivity->approved=$operationBooking['approved'];
            $bookingActivity->approved_at=$operationBooking['approved_at'];
            $bookingActivity->approved_by=$approvedById;

            if($bookingActivity->save()){

              //Update Staff Ids
              $createdById=0;
              $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBookingInOut['created_by']])->asArray()->one();
              if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

              $updatedById=0;
              $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$operationBookingInOut['updated_by']])->asArray()->one();
              if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

              $connection->createCommand(
                "update ".BookingActivity::tableName()." set created_at=:created_at,created_by=:created_by,updated_at=:updated_at,updated_by=:updated_by where id=:id",
                [
                  ':created_at'=>$operationBookingInOut['created_at'],
                  ':created_by'=>$createdById,
                  ':updated_at'=>$operationBookingInOut['updated_at'],
                  ':updated_by'=>$updatedById,
                  ':id'=>$bookingActivity['id']
                ]
              )->execute();
              //Generate Signature Images
            }else{
              echo 'In/Out ID = '.$operationBooking['id'].'<br />';
              print_r($bookingActivity->getErrors());
              die();
            }
          }else{
            $bookingStatus=1;
            //Check Stay In
            if($operationBooking['stayin']==1)$bookingStatus=2;

            //Check No show row
            $noShowStatus=BookingNoShow::find()->where(['booking_id'=>$operationBooking['id']]);
            if($noShowStatus->exists())$bookingStatus=3;

            //Check TCC Cancelled
            $tccCancelledStatus=BookingCancelled::find()->where(['booking_id'=>$operationBooking['id']])->asArray()->one();
            //Weather
            if($tccCancelledStatus!=null && $tccCancelledStatus['remarks']==1)$bookingStatus=4;
            //Boat Not Available
            if($tccCancelledStatus!=null && $tccCancelledStatus['remarks']==2)$bookingStatus=5;

            //Check Admin
            if($operationBooking['is_admin']==1)$bookingStatus=6;

            //Check Same Day Cancel
            if($operationBooking['same_day_cancel']==1)$bookingStatus=7;

            //Check Weather Warning Cancel
            if($operationBooking['weather_warning_cancel']==1)$bookingStatus=8;

            //Check Training
            if($operationBooking['is_training']==1)$bookingStatus=9;

            //Check Emergency
            if($operationBooking['is_emergency']==1)$bookingStatus=10;

            //Check Out Of Service
            if($operationBooking['is_outofservice']==1)$bookingStatus=11;
            //////////////////Update Booking Status//////////////////
            $connection->createCommand(
              "update ".Booking::tableName()." set status=:status where id=:id",
              [
                ':status'=>$bookingStatus,
                ':id'=>$result['id'],
              ]
            )->execute();
          }
          $connection->createCommand(
            "update ".OperationBooking::tableName()." set imported=:imported where id=:id",
            [
              ':imported'=>1,
              ':id'=>$operationBooking['id'],
            ]
          )->execute();
        }
      }
      //echo "Imported";
      sleep(2);
      echo '<script>window.location.href="'.Url::to(['booking-activity','lastid'=>$lastid]).'"</script>';
      //echo ''.Url::to(['booking-activity','lastid'=>$lastid]).'';
    }else{
      echo "<b>Finished</b>";
      //echo '<script>window.location.href="'.Url::to(['claim-booking-items']).'"</script>';
    }
    exit;
  }

  public function actionClaimBookingItems($lastid=null)
  {
    $results=ClaimOrderItem::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(25)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $main_booking_id=0;
        $operationBooking=OperationBooking::find()->select(['booking_id'])->where(['id'=>$result['booking_id']])->asArray()->one();
        if($operationBooking!=null){
          $main_booking_id=$operationBooking['booking_id'];
        }

        $connection->createCommand(
          "update ".ClaimOrderItem::tableName()." set main_booking_id=:main_booking_id where id=:id",
          [
            ':main_booking_id'=>$main_booking_id,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['claim-booking-items','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to operations booking comments";
      echo '<script>window.location.href="'.Url::to(['booking-operation-comments']).'"</script>';
    }
    exit;
  }

  public function actionBookingOperationComments($lastid=null)
  {
    $results=BookingOperationComments::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(25)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $createdById=0;
        $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['created_by']])->asArray()->one();
        if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

        $updatedById=0;
        $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['updated_by']])->asArray()->one();
        if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

        $connection->createCommand(
          "update ".BookingOperationComments::tableName()." set created_by=:created_by,updated_by=:updated_by where id=:id",
          [
            ':created_by'=>$createdById,
            ':updated_by'=>$updatedById,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['booking-operation-comments','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to claim order comments";
      echo '<script>window.location.href="'.Url::to(['claim-order-comments']).'"</script>';
    }
    exit;
  }

  public function actionClaimOrderComments($lastid=null)
  {
    $results=ClaimOrderComments::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(25)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $createdById=0;
        $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['created_by']])->asArray()->one();
        if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

        $updatedById=0;
        $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['updated_by']])->asArray()->one();
        if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

        $connection->createCommand(
          "update ".ClaimOrderComments::tableName()." set created_by=:created_by,updated_by=:updated_by where id=:id",
          [
            ':created_by'=>$createdById,
            ':updated_by'=>$updatedById,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['claim-order-comments','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to claim order";
      echo '<script>window.location.href="'.Url::to(['claim-order']).'"</script>';
    }
    exit;
  }

  public function actionClaimOrder($lastid=null)
  {
    $results=ClaimOrder::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(25)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $createdById=0;
        $createdByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['created_by']])->asArray()->one();
        if($createdByUser!=null)$createdById=$createdByUser['booking_staff_id'];

        $updatedById=0;
        $updatedByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['updated_by']])->asArray()->one();
        if($updatedByUser!=null)$updatedById=$updatedByUser['booking_staff_id'];

        $connection->createCommand(
          "update ".ClaimOrder::tableName()." set created_by=:created_by,updated_by=:updated_by where id=:id",
          [
            ':created_by'=>$createdById,
            ':updated_by'=>$updatedById,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['claim-order','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b> moving to claim order staff";
      echo '<script>window.location.href="'.Url::to(['claim-order-staff']).'"</script>';
    }
    exit;
  }

  public function actionClaimOrderStaff($lastid=null)
  {
    $results=ClaimOrder::find()
    ->andFilterWhere(['>','id',$lastid])
    ->asArray()->orderBy(['id'=>SORT_ASC])->limit(25)->all();
    if($results!=null){
      $connection = \Yii::$app->db;
      foreach($results as $result){
        $lastid=$result['id'];

        $userById=0;
        $userByUser=OperationsUser::find()->select(['booking_staff_id'])->where(['id'=>$result['user_id']])->asArray()->one();
        if($userByUser!=null)$userById=$userByUser['booking_staff_id'];

        $connection->createCommand(
          "update ".ClaimOrder::tableName()." set user_id=:user_id where id=:id",
          [
            ':user_id'=>$userById,
            ':id'=>$result['id']
          ]
        )->execute();
      }
      sleep(1);
      echo '<script>window.location.href="'.Url::to(['claim-order-staff','lastid'=>$lastid]).'"</script>';
    }else{
      echo "<b>Finished</b>";
      //echo '<script>window.location.href="'.Url::to(['booking-activity']).'"</script>';
    }
    exit;
  }
}
