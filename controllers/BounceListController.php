<?php

namespace app\controllers;

use Yii;
use app\models\BlackListedEmail;
use app\models\BlackListedEmailSearch;
use app\models\BlackListedEmailImport;
use app\models\BlackListedEmailImportByLines;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* BounceListController.
*/
class BounceListController extends DefController
{
  /**
  * Lists all BlackListedEmail models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new BlackListedEmailSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Import.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionImport()
  {
    $this->checkSuperAdmin();
    $model = new BlackListedEmailImport;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        return $this->redirect(['import']);
      } else {
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    $model2 = new BlackListedEmailImportByLines;

    if ($model2->load(Yii::$app->request->post())) {
      if($model2->save()){
        return $this->redirect(['import']);
      } else {
        if($model2->hasErrors()){
          foreach($model2->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('import', [
      'model' => $model,
      'model2' => $model2,
    ]);
  }

  /**
  * Finds the BlackListedEmail model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return BlackListedEmail the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = BlackListedEmail::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
