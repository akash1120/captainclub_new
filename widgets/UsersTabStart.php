<?php
namespace app\widgets;

use Yii;

class UsersTabStart extends \yii\bootstrap\Widget
{
  public static function header($model,$searchModel)
  {
    return '
    <div class="tabs">
      <ul class="nav nav-tabs nav-justified" style="position:relative;">
      '.Yii::$app->helperFunctions->getUserTabs($model->id,$searchModel).'
      </ul>
      <div class="tab-content">
        <div class="tab-pane active">';
  }
  public static function footer()
  {
    return '
        </div>
      </div>
    </div>';
  }
}
