<?php
namespace app\widgets;

use Yii;

class UserBookingTabs extends \yii\bootstrap\Widget
{
  public static function header($searchModel)
  {
    return '
    <div class="tabs">
      <ul class="nav nav-tabs nav-justified" style="position:relative;">
      '.Yii::$app->helperFunctions->getUserBookingTabs($searchModel).'
      </ul>
      <div class="tab-content">
        <div class="tab-pane active">';
  }
  public static function footer()
  {
    return '
        </div>
      </div>
    </div>';
  }
}
