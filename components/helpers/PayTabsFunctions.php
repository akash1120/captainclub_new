<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;

class PayTabsFunctions extends Component
{
  public function getPaytabOptions()
  {
    return [
      'currency'=>'AED',
      'merchentEmail'=>'malick.naeem@gmail.com',
      'returnUrl'=>'http://test.thecaptainsclub.ae/web/order/response',
      'testMode'=>1,
      //Test
      'merchentId'=>'10012247',
      'secretKey'=>'jDjOfDX9tWLZ1h8cFl1yyKRrEs381YKkB49QH70b3Lb52khIFjhe48FJAIRBHCWjH6apdaAhVMnY6FGLYX035GMGoEi6hepYlzny',
    ];
  }

	/**
	 * Calculate Paytabs Amount
	 */
	public function calculateFinalAmount($amt)
	{
    $finalAmout=0;
    $scToUsers=Yii::$app->appHelperFunctions->getSetting('paytab_servicefees_u');
    $serviceChargesToUser=$amt*$scToUsers/100;
    $scAdditional=Yii::$app->appHelperFunctions->getSetting('paytab_addiotional_pt');
    $finalAmout=$amt+$serviceChargesToUser+$scAdditional;
    return $finalAmout;
	}

	/**
	 * Calculate Paytabs Amount
	 */
	public function calculateProfitAmount($amt)
	{
    $finalAmout=0;
    $scToUsers=Yii::$app->appHelperFunctions->getSetting('paytab_servicefees_u');
    $scToPt=Yii::$app->appHelperFunctions->getSetting('paytab_servicefees_pt');
    $scAdditional=Yii::$app->appHelperFunctions->getSetting('paytab_addiotional_pt');
    $scAdditionalOrg=Yii::$app->appHelperFunctions->getSetting('paytab_addiotional_pt_org');

    $serviceChargesToUser=$amt*($scToUsers)/100+$scAdditional;

    $serviceChargesToPt=(($amt+$serviceChargesToUser)*$scToPt/100)+$scAdditionalOrg;

    $finalAmout=$serviceChargesToUser-$serviceChargesToPt;
    /*echo 'Amount: '.$amt.'<br />';
    echo 'From User: '.$serviceChargesToUser.'<br />';
    echo 'To Paytabs: '.$serviceChargesToPt.'<br />';
    echo 'Profit: '.$finalAmout.'<hr />';
    die();*/
    return $finalAmout;
    //4111111111111111
	}
}
