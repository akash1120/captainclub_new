<?php
namespace app\components\helpers;

use Yii;
use yii\swiftmailer\Mailer;

class MyMailer extends Mailer
{
  public $testmode = false;
  public $testemail = 'na33m.awan@gmail.com';

  public function beforeSend($message)
  {
    if (parent::beforeSend($message)) {
      //setting bounced email
      $headers = $message->getSwiftMessage()->getHeaders();
      $headers->addPathHeader('Return-Path', Yii::$app->params['bouncedEmail']);

      if ($this->testmode) {
        $message->setTo($this->testemail);
      }
      return true;
    }
    return false;
  }
}
