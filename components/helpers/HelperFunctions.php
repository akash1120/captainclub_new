<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Url;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Contract;
use app\models\SupportTicket;
use app\models\UserStatusReason;
use app\models\EmailTemplate;
use app\models\WaitingListSportsEquipment;
use app\models\UserDeviceNotification;

class HelperFunctions extends Component
{
	public function daysDifference($s,$e){
		$start = strtotime($s);
		$end = strtotime($e);

		$days_between = ceil(abs($end - $start) / 86400);
		return $days_between;
	}

	public function getShortAmPmTime($long_time)
	{
		return date("g:i a",strtotime($long_time));
	}

	public function convertTime($time_ago2)
	{
		$time_ago = strtotime($time_ago2);
		$cur_time   = time();
		$time_elapsed   = $cur_time - $time_ago;
		$seconds    = $time_elapsed ;
		$minutes    = round($time_elapsed / 60 );
		$hours      = round($time_elapsed / 3600);
		$days       = round($time_elapsed / 86400 );
		$weeks      = round($time_elapsed / 604800);
		$months     = round($time_elapsed / 2600640 );
		$years      = round($time_elapsed / 31207680 );

		if($seconds <= 60){// Seconds
			return Yii::t('app', 'just now');
		}else if($minutes <=60){//Minutes
			if($minutes==1){
				return Yii::t('app', 'one minute ago');
			}else{
				return Yii::t('app', '{mn} minutes',['mn'=>$minutes]).' '.Yii::t('app', 'ago');
			}
		}else if($hours <=24){//Hours
			if($hours==1){
				return Yii::t('app', 'an hour ago');
			}else{
				return Yii::t('app', '{hr} hrs ago',['hr'=>$hours]);
			}
		}else{
			return Yii::$app->formatter->asdatetime($time_ago2);
		}
	}

	public function getExpiryFilters()
	{

    $expiry_months = array(
      date('Y-m', strtotime(date('Y-m')." -6 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." -6 month")))),
      date('Y-m', strtotime(date('Y-m')." -5 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." -5 month")))),
      date('Y-m', strtotime(date('Y-m')." -4 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." -4 month")))),
      date('Y-m', strtotime(date('Y-m')." -3 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." -3 month")))),
      date('Y-m', strtotime(date('Y-m')." -2 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." -2 month")))),
      date('Y-m', strtotime(date('Y-m')." -1 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." -1 month")))),
      date('Y-m') =>  date("F Y", strtotime(date('Y-m'))),
      date('Y-m', strtotime(date('Y-m')." +1 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." +1 month")))),
      date('Y-m', strtotime(date('Y-m')." +2 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." +2 month")))),
      date('Y-m', strtotime(date('Y-m')." +3 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." +3 month")))),
      date('Y-m', strtotime(date('Y-m')." +4 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." +4 month")))),
      date('Y-m', strtotime(date('Y-m')." +5 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." +5 month")))),
      date('Y-m', strtotime(date('Y-m')." +6 month")) =>   date("F Y", strtotime(date('Y-m', strtotime(date('Y-m')." +6 month")))),
    );

    return $expiry_months;
	}

  public function sendNotification($to,$messageArr,$dataArr,$user_id,$booking_id=0)
  {
		return '';
    $apiKey='ZTM2OWViMDQtNzk0MS00NTA1LWFmZmItYjU0ZmJjNmM2ODhj';

    $data = [
      "to" => $to,
      "notification" => $messageArr,
      "data" => $dataArr,
    ];
    $data_string = json_encode($data);
    $sendIt=true;
    if($booking_id>0){
      $notificationCheck=UserDeviceNotification::find()->where(['user_id'=>$user_id,'booking_id'=>$booking_id,'user_device_id'=>$to]);
      if($notificationCheck->exists()){
        $sendIt=false;
      }
    }
    if($sendIt==true){
      $notificationRow=new UserDeviceNotification;
      $notificationRow->user_id=$user_id;
      $notificationRow->booking_id=$booking_id;
      $notificationRow->user_device_id=$to;
      $notificationRow->json_data=$data_string;
      $notificationRow->save();

      $headers = array ( 'Authorization: key=' . $apiKey, 'Content-Type: application/json' );
      $ch = curl_init(); curl_setopt( $ch,CURLOPT_URL, 'https://onesignal.com/api/v1/notifications' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);
      $result = curl_exec($ch);
      curl_close ($ch);
      $notificationRow->notification_response=$result;
      $notificationRow->save();
			echo '<pre>';print_r($result);echo '</pre>';
			die();
    }
  }

  public function sendNewNotification($to,$title,$message,$url,$user_id,$booking_id=0)
  {
		$appID='efdc4d43-d178-40bb-a4e9-7a9f714a3068';
		$apiKey='ZTM2OWViMDQtNzk0MS00NTA1LWFmZmItYjU0ZmJjNmM2ODhj';

		$title = ["en" => $title];
		$content = ["en" => $message];

    $hashes_array = array();
    $fields = array(
        'app_id' => $appID,
				'include_player_ids' => $to,
				'headings' => $title,
        'contents' => $content,
        'url' => $url,
        //'web_buttons' => $hashes_array
    );

    $fields = json_encode($fields);
    //print("\nJSON sent:\n");
    print($fields);

		$sendIt=true;
		if($booking_id>0){
			$notificationCheck=UserDeviceNotification::find()->where(['user_id'=>$user_id,'booking_id'=>$booking_id,'user_device_id'=>$to]);
			if($notificationCheck->exists()){
				$sendIt=false;
			}
		}

    if($sendIt==true){
      $notificationRow=new UserDeviceNotification;
      $notificationRow->user_id=$user_id;
      $notificationRow->booking_id=$booking_id;
      $notificationRow->user_device_id=$to;
      $notificationRow->json_data=$fields;
      $notificationRow->save();

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	        'Content-Type: application/json; charset=utf-8',
	        'Authorization: Basic '.$apiKey.''
	    ));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, FALSE);
	    curl_setopt($ch, CURLOPT_POST, TRUE);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

	    $response = curl_exec($ch);
	    curl_close($ch);
			$notificationRow->notification_response=$response;
			$notificationRow->save();
			echo '<pre>';print_r($response);echo '</pre>';
	    //return $response;
		}
		return ;
  }

	public function getDubaiLicenseFee()
	{
		return '620 AED';
	}

	public function getLicenseReminderSubject($city_id)
	{
		//Abu Dhabi
		if($city_id==735){
			return 'FTA Local License Renewal Reminder';
		}
		//Dubai
		elseif($city_id==736){
			return 'DMCA License Renewal';
		}
	}

	public function getAmPmTimeSlots()
	{
		return [1,2];
	}

	public function getDayBefore($date)
	{
		return date ("Y-m-d", strtotime("-1 day", strtotime($date)));
	}

	public function getNextDay($date)
	{
		return date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	}

	public function getGeneralShortNoticeMessage()
	{
		return 'Although it&apos;s a short notice, please feel free to ask the team in the marina once you reach. &quot;Subject to Availability&quot;';
	}

	public function getShortNoticeCaptainMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an in house captain, please call 052 641 4172, and we will try to arrange subject to availability of the team';
	}

	public function getShortNoticeEquipmentMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an equipment, please call 052 641 4172, and we will try to arrange subject to availability of the team';
	}

	public function getShortNoticeBBQMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an equipment, please call 052 641 4172, and we will try to arrange subject to the availability';
	}

	public function getShortNoticeWakeBoardingMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an equipment, please call 052 641 4172, and we will try to arrange subject to the availability';
	}

	public function getShortNoticeWakeSurfingMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an equipment, please call 052 641 4172, and we will try to arrange subject to the availability';
	}

	public function getShortNoticeIceMsg()
	{
		return 'Although it\'s a short notice, please feel free to ask the team in the marina once you reach';
	}

	public function getShortNoticeKidsLifeJacketMsg()
	{
		return 'Although it\'s a short notice, please feel free to ask the team in the marina once you reach';
	}

	public function getShortNoticeEarlyDepartureMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an Early Departure, please call 052 641 4172, and we will try to arrange subject to the availability';
	}

	public function getShortNoticeOvernightCampingMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an Overnight Camping, please call 052 641 4172, and we will try to arrange subject to the availability';
	}

	public function getShortNoticeLateArrivalMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request an Late Arrival, please call 052 641 4172, and we will try to arrange subject to the availability';
	}

	public function getShortNoticeOtherMsg()
	{
		return 'Sorry for the inconvenience. Its a short notice to request, please call 052 641 4172, and we will try to arrange subject to the availability';
	}

	public function getShortNoticeAfter()
	{
		return 18;
	}

	public function getLateMroningCheckTill()
	{
		return 20;
	}

  public function fullMobileNumber($mobile)
  {
    $mobile=$this->fixMobile($mobile);
    if($mobile!=null && $mobile!=''){
      return "971".$mobile;
    }else{
      return '';
    }
  }

  public function checkSmsBalance()
  {

    $url = "http://mshastra.com/balance.asp?user=20080739&pwd=Client987";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $curl_scraped_page = curl_exec($ch);
    curl_close($ch);
    return $curl_scraped_page;
  }

  public function sendSms($mobileNumbers,$smsMsg)
  {
		return ;
    /*$smsMsg=$smsMsg."\n\nOptout 3001";
    $smsMsg=urlencode($smsMsg);
    $url = "http://mshastra.com/sendurlcomma.aspx?user=20080739&pwd=Client987&senderid=CaptainClub&mobileno=".$mobileNumbers."&msgtext=".$smsMsg."&CountryCode=ALL";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); $curl_scraped_page = curl_exec($ch);
    curl_close($ch);

    return $curl_scraped_page;*/
  }

  public function fixMobile($mobile)
  {
    $length=strlen($mobile);
    if($length==10){
      $firstDigit=substr($mobile,0,1);
      if($firstDigit=="0"){
        $mobile=substr($mobile,1,$length);
      }
    }
    if($length==10){
      $firstDigit=substr($mobile,0,1);
      if($firstDigit=="0"){
        $mobile=substr($mobile,1,$length);
      }
    }
    $firstThreeDigit=substr($mobile,0,3);
    if($firstThreeDigit=="971"){
      $mobile=substr($mobile,3,$length);
    }
    $firstFourDigit=substr($mobile,0,4);
    if($firstFourDigit=="+971"){
      $mobile=substr($mobile,4,$length);
    }
    $firstFiveDigit=substr($mobile,0,5);
    if($firstFiveDigit=="00971"){
      $mobile=substr($mobile,5,$length);
    }
    return $mobile;
  }

	public function getExpiryAlertDays()
	{
		return 45;
	}

	public function getBoatRequiredOptions()
	{
		return [
			'all' => 'All',
			'byboat' => 'Specific Boats',
			'bytag' => 'Specific Tags',
		];
	}

	public function getSpecialBoatTypes()
	{
		return [
			'1' => Yii::t('app','Night Cruise'),
			'2' => Yii::t('app','Marina StayIn'),
			'3' => Yii::t('app','DropOff'),
		];
	}

	public function getSpecialBoatTypeMessage()
	{
		return [
			'1' => 'You booked a Night Cruise.<br />A complimentary captain will be operating the boat in order for you to Enjoy your dinner in front of the City Lights.',
			'2' => 'You booked a Marina Stay in.<br />Where the boat will sit idle in the Marina, and you can enjoy your dinner in the Marina facing the amazing view.',
			'3' => '<div align="left">• Please don\'t be late for the Drop off so it doesn\'t affect the afternoon bookings.<br />• The pick up time will be maximum 7:15 PM, the captains will text you as soon as a boat is available, if you wanted early pick up.<br />• Fuel will be charged on the round trip.</div>',
		];
	}

	public function getAdminBookingTypes()
	{
		return [
			'1' => 'Request',
			'2' => 'Service and Repairs',
			'3' => 'Training',
			'4' => 'Emergency',
			'6' => 'Replacement',
			'5' => 'Others',
		];
	}

	public function getBoatSwapReason()
	{
		return [
			'3' => 'Request',
			'2' => 'Original Boat NA',
		];
	}

	public function getBoatSwapReasonDetail()
	{
		return [
			'2' => 'Enjoy Boating With Relief',
			'3' => 'Sorry for the inconvenience',
		];
	}

	public function getBookingStatus()
	{
		return [
			1=>'Active',
			2=>'Stay In',
			3=>'No Show',
			4=>'TCC Cancelled - Weather',
			5=>'TCC Cancelled - Boat not available',
			6=>'Admin',
			7=>'Same Day Cancel',
			8=>'Weather Warning Cancel',
			9=>'Training',
			10=>'Emergency',
			11=>'Out Of Service',
		];
	}

	public function getReportBookingStatus()
	{
		return [
			1=>'Active',
			2=>'Stay In',
			6=>'Admin',
			9=>'Training',
			10=>'Emergency',
			11=>'Out Of Service',
		];
	}

	public function getBookingActiveStatus()
	{
		return [
			1,//Active/Future
			2,//Stay In
			3,//No Show
			//6,//Admin
			7,//Same Day Cancel
			8,//Weather Warning Cancel
		];
	}

	public function getBookingSysActiveStatus()
	{
		return [
			1,//Active/Future
			2,//Stay In
		];
	}

	public function getBookingActiveStatusForAvailability()
	{
		return [
			1,//'Active'
			2,//'Stay In'
			4,//'TCC Cancelled - Weather'
			5,//'TCC Cancelled - Boat not available'
			6,//'Admin'
			9,//'Training'
			10,//'Emergency'
			11,//'Out Of Service'
		];
	}

	public function getSameDayCancelID()
	{
		return 7;
	}

	public function getUserTypes()
	{
		return [
			'0' => Yii::t('app','Member'),
			'1' => Yii::t('app','Admin'),
			'10' => Yii::t('app','Super Admin'),
		];
	}

	public function getArrYesNo()
	{
		return [
			'1' => Yii::t('app','Yes'),
			'0' => Yii::t('app','No'),
		];
	}

	public function getArrYesNoIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i></span>',
			'0' => '<span class="badge grid-badge badge-danger"><i class="fa fa-times"></i></span>',
		];
	}

	public function getArrYesNoSpan()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Yes').'</span>',
			'0' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','No').'</span>',
		];
	}

	public function getArrPublishing()
	{
		return [
			'1' => Yii::t('app','Publish'),
			'0' => Yii::t('app','Pending'),
			'2' => Yii::t('app','Draft'),
		];
	}

	public function getArrPublishingIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Published').'</span>',
			'0' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Pending').'</span>',
			'2' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
		];
	}

	public function getRequestStatus()
	{
		return [
			'-1' => Yii::t('app','Training Completed'),
			'0' => Yii::t('app','Pending'),
			'1' => Yii::t('app','Met'),
			'2' => Yii::t('app','Not Met'),
			'3' => Yii::t('app','Cancelled'),
		];
	}

	public function getRequestStatusIcon()
	{
		return [
			'-1' => '<span class="badge grid-badge badge-primary"><i class="fa fa-thumbs-up"></i> '.Yii::t('app','Training Completed').'</span>',
			'0' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Pending').'</span>',
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Met').'</span>',
			'2' => '<span class="badge grid-badge badge-not-met"><i class="fa fa-times"></i> '.Yii::t('app','Not Met').'</span>',
			'3' => '<span class="badge grid-badge badge-danger"><i class="fa fa-exclamation"></i> '.Yii::t('app','Cancelled').'</span>',
		];
	}

	public function getUserRequestStatus()
	{
		return [
			'-1' => Yii::t('app','Training Completed'),
			'0' => Yii::t('app','Under Process'),
			'1' => Yii::t('app','Met'),
			'2' => Yii::t('app','Not Met'),
			'3' => Yii::t('app','Cancelled'),
		];
	}

	public function getUserRequestStatusIcon()
	{
		return [
			'-1' => '<span class="badge grid-badge badge-primary"><i class="fa fa-thumbs-up"></i> '.Yii::t('app','Training Completed').'</span>',
			'0' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Under Process').'</span>',
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Met').'</span>',
			'2' => '<span class="badge grid-badge badge-info"><i class="fa fa-times"></i> '.Yii::t('app','Not Met').'</span>',
			'3' => '<span class="badge grid-badge badge-danger"><i class="fa fa-exclamation"></i> '.Yii::t('app','Cancelled').'</span>',
		];
	}

	public function getRequestTypes()
	{
		return [
			'freeze'=>Yii::t('app','Freeze'),
			'boatbooking'=>Yii::t('app','Boat Booking'),
			'nightdrivetraining'=>Yii::t('app','Night Drive Training'),
			'upgradecity'=>Yii::t('app','Upgrade City'),
			'waitingearlydeparture'=>Yii::t('app','Early Departure'),
			'waitingcaptain'=>Yii::t('app','Captain'),
			'waitingequipment'=>Yii::t('app','Sports Equipment (Donuts)'),
			'waitingbbq'=>Yii::t('app','BBQ Set'),
			'bookingice'=>Yii::t('app','Ice'),
			'bookingkidsjackets'=>Yii::t('app','Kids Jackets'),
			'waitingwakeboarding'=>Yii::t('app','Wake Boarding'),
			'waitinglatearrival'=>Yii::t('app','Late Arrival'),
			'waitingovernightcamping'=>Yii::t('app','Overnight Camping'),
			'other'=>Yii::t('app','Other'),
		];
	}

	public function getArrUserStatus()
	{
		return [
			'1' => Yii::t('app','Active'),
			'2' => Yii::t('app','Hold'),
			'3' => Yii::t('app','Canceled'),
			'4' => Yii::t('app','Expired'),
			'5' => Yii::t('app','Dead'),
			'20' => Yii::t('app','Pending'),
			'0' => Yii::t('app','Blocked'),
		];
	}

	public function getArrUserStatusIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Active').'</span>',
			'2' => '<span class="badge grid-badge badge-primary">'.Yii::t('app','Hold').'</span>',
			'3' => '<span class="badge grid-badge badge-info">'.Yii::t('app','Canceled').'</span>',
			'4' => '<span class="badge grid-badge badge-warning">'.Yii::t('app','Expired').'</span>',
			'5' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Dead').'</span>',
			'20' => '<span class="badge grid-badge badge-purple">'.Yii::t('app','Pending').'</span>',
			'0' => '<span class="badge grid-badge badge-blocked">'.Yii::t('app','Blocked').'</span>',
		];
	}

	public function getUserStatus($user_id,$status,$model)
	{
		$html='';
		if(isset(Yii::$app->helperFunctions->arrUserStatusIcon[$status])){
			$html=Yii::$app->helperFunctions->arrUserStatusIcon[$status];
		}
		$fullName=$model['firstname'].($model['lastname']!='' ? $model['lastname'] : '');
		if($fullName==''){
			$fullName.=$model['username'];
		}
		if($fullName==''){
			$fullName.=$model['email'];
		}
		if($status==1){
			$comingReasonRow = UserStatusReason::find()->where(['and',['user_id'=>$user_id],['>','date',date("Y-m-d")]])->asArray()->one();
			if($comingReasonRow!=null){
				$heading=Yii::$app->formatter->asDate($comingReasonRow['date']);
				$reason=$comingReasonRow['reason'];

				if($comingReasonRow['status']==2){
					$html.=' <span class="badge grid-badge badge-primary" data-toggle="popover" data-placement="top" data-trigger="hover" data-title="Scheduled Hold: '.$heading.'" data-content="'.$reason.'">'.Yii::t('app','Scheduled Hold').'</span>';
					if(Yii::$app->menuHelperFunction->checkActionAllowed('del-hold','contract')){
						$html.=' <a href="javascript:;" class="btn btn-xs btn-danger btn-del-hold" data-user_id="'.$user_id.'" data-id="'.$comingReasonRow['id'].'"><i class="fa fa-times"></i></a>';
					}
				}
				if($comingReasonRow['status']==3){
					$html.=' <span class="badge grid-badge badge-info" data-toggle="popover" data-placement="top" data-trigger="hover" data-title="Scheduled Cancel: '.$heading.'" data-content="'.$reason.'">'.Yii::t('app','Scheduled Canceled').'</span>';
					if(Yii::$app->menuHelperFunction->checkActionAllowed('del-cancel','contract')){
						$html.=' <a href="javascript:;" class="btn btn-xs btn-danger btn-del-cancel" data-user_id="'.$user_id.'" data-id="'.$comingReasonRow['id'].'"><i class="fa fa-times"></i></a>';
					}
				}
			}
		}
		if($status==4 && Yii::$app->menuHelperFunction->checkActionAllowed('mark-dead','user')){
			$html.='<br /><a href="javascript::" class="mark-dead" data-key="'.$model['id'].'" data-username="'.$fullName.'">Mark dead?</a>';
		}
		if($status==2 || $status==3){
			$heading='';
			$reason='';
			$reasonRow = UserStatusReason::find()->where(['user_id'=>$user_id,'status'=>$status])->asArray()->orderBy(['id'=>SORT_DESC])->one();
			if($reasonRow!=null){
				$heading=' on '.Yii::$app->formatter->asDate($reasonRow['date']);
				$reason='<br />'.$reasonRow['reason'];
			}
			if($status==2){
				$html='<span class="badge text-left status-badge grid-badge badge-primary">'.Yii::t('app','Hold').' '.$heading.' '.$reason.'</span>';
			}
			if($status==3){
				$html='<span class="badge text-left status-badge grid-badge badge-info">'.Yii::t('app','Canceled').' '.$heading.' '.$reason.'</span>';
			}

		}
		return $html;
	}

	/**
	* Generate Time Token
	*/
	public function generateTimeId()
	{
		return str_replace(array(" ","."),"",microtime());
	}

	/**
	* Generate Number Token
	*/
	public function generateNumberToken($length)
	{
		$key = '';
		$keys = range(1, 9);

		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}

		return $key;
	}

	/**
	* Generate Token
	*/
	public function generateToken($length)
	{
		$key = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));

		for ($i = 0; $i < $length; $i++) {
			$key .= $keys[array_rand($keys)];
		}

		return $key;
	}

	public function getNumberOfDaysBetween($start,$end)
	{
		$datediff=strtotime($end)-strtotime($start);
		return round($datediff / (60 * 60 * 24));
	}

	public function getTemplateShortCodes()
	{
		return [
			'{loginlink}' => '{loginlink}',
			'{resetlink}' => '{resetlink}',
			'{username}' => '{username}',
			'{password}' => '{password}',
			'{email}' => '{email}',
			'{bookingCity}' => '{bookingCity}',
			'{bookingMarina}' => '{bookingMarina}',
			'{bookingBoat}' => '{bookingBoat}',
			'{bookingDate}' => '{bookingDate}',
			'{bookingTime}' => '{bookingTime}',
			'{freezeStart}' => '{freezeStart}',
			'{freezeEnd}' => '{freezeEnd}',
			'{packageName}' => '{packageName}',
			'{tomorrowDate}' => '{tomorrowDate}',
			'{bookingList}' => '{bookingList}',
			'{bookingDuration}' => '{bookingDuration}',
			'{packageStart}' => '{packageStart}',
			'{packageEnd}' => '{packageEnd}',
			'{adminRemarks}' => '{adminRemarks}',
			'{requestDetail}' => '{requestDetail}',
			'{logo}' => '{logo}',
		];
	}

	public function getSameDayTimeCheck()
	{
		return [
			'1' => '12:00',
			'2' => '9:00',
			'5' => '23:00',
		];
	}

	public function isTimeAlreadyPassed($TimeLimit,$toCheck)
	{
		list($tlHr,$tlMin)=explode(":",$TimeLimit);
		list($tcHr,$tcMin)=explode(":",$toCheck);
		if($tlHr){

		}
	}

	public function getTicketStatus()
	{
		return [
			'1'=>'Open',
			'2'=>'Closed',
			//'3'=>'Answered',
		];
	}

	public function getTicketRefId()
	{
		$refId=1001;
		$result=SupportTicket::find()->select(['ref_id'])->where(['is not','ref_id',NULL])->orderBy(['ref_id'=>SORT_DESC])->one();
		if($result!=null){
			$refId=$result['ref_id']+1;
		}
		return $refId;
	}

	public function getTicketSuperAdminIdz()
	{
		return [1];
	}

	public function getWeekDayNames()
	{
		return [
			'0' => 'Sunday',
			'1' => 'Monday',
			'2' => 'Tuesday',
			'3' => 'Wednesday',
			'4' => 'Thursday',
			'5' => 'Friday',
			'6' => 'Saturday',
		];
	}

	public function getReminderTypes()
	{
		return [
			'1'=>'Call to',
			'2'=>'Meeting with',
			'3'=>'Task',
			'4'=>'Email',
		];
	}

	public function getUserTabs($user_id,$searchModel)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		return '
		<li class="nav-item">
		<a class="nav-link" href="javascript::" data-pjax="0">Profile</a>
		</li>
		<li class="nav-item'.($controllerID=='user-account' && ($actionID=='account-statement' ||  $actionID=='credit-log') ? ' active' : '').'">
		<a class="nav-link" href="'.Url::to(['user-account/account-statement','id'=>$user_id]).'" data-pjax="0">Payments</a>
		</li>
		<li class="nav-item'.($controllerID=='contract' && ($actionID=='index' || $actionID=='update-history') ? ' active' : '').'">
		<a class="nav-link" href="'.Url::to(['contract/index','id'=>$user_id]).'" data-pjax="0">Contracts</a>
		</li>
		<li class="nav-item'.($controllerID=='user-task' && $actionID=='index' ? ' active' : '').'">
		<a class="nav-link" href="'.Url::to(['user-task/index','id'=>$user_id]).'" data-pjax="0">Tasks</a>
		</li>
		<li class="nav-item'.($controllerID=='user' && $actionID=='bookings' ? ' active' : '').'">
		<a class="nav-link" href="'.Url::to(['user/bookings','id'=>$user_id]).'" data-pjax="0">Bookings</a>
		</li>
		<li class="nav-item'.($controllerID=='user' && $actionID=='requests' ? ' active' : '').'">
		<a class="nav-link" href="'.Url::to(['user/requests','id'=>$user_id]).'" data-pjax="0">Requests</a>
		</li>
		<li class="nav-item'.($controllerID=='user-feedback' && $actionID=='index' ? ' active' : '').'">
		<a class="nav-link" href="'.Url::to(['user-feedback/index','id'=>$user_id]).'" data-pjax="0">Feedbacks</a>
		</li>';
	}

	public function getRequestTabs($searchModel)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		return '
		<li class="nav-item'.($searchModel->listType=='history' ? '' : ' active').'">
			<a class="nav-link" href="'.Url::to(['user-requests/'.$actionID,'listType'=>'active']).'" data-pjax="0">Active Requests</a>
		</li>
		<li class="nav-item'.($searchModel->listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user-requests/'.$actionID,'listType'=>'history']).'" data-pjax="0">Request History</a>
		</li>';
	}

	public function getMemberRequestTabs($user_id,$searchModel)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		return '
		<li class="nav-item'.($searchModel->listType=='history' ? '' : ' active').'">
			<a class="nav-link" href="'.Url::to(['user/requests','id'=>$user_id,'listType'=>'active']).'" data-pjax="0">Active Requests</a>
		</li>
		<li class="nav-item'.($searchModel->listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user/requests','id'=>$user_id,'listType'=>'history']).'" data-pjax="0">Request History</a>
		</li>
		<li class="nav-item" style="position:absolute;right:5px;">
			<div class="clearfix">
				<div class="pull-right" style="margin-left:5px;">
					<a class="btn btn-xs btn-primary load-modal" href="javascript:;" data-pjax="0" data-toggle="tooltip" data-title="Add New" data-url="'.Url::to(['request/boat-booking','user_id'=>$user_id]).'" data-heading="New Boat Booking"><i class="fas fa-plus"></i></a>
				</div>
			</div>
		</li>';
	}

	public function getUserPaymentsTabs($user_id)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		$user=User::findOne($user_id);
		$html='
		<li class="nav-item'.($controllerID=='user-account' && $actionID=='account-statement' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user-account/account-statement','id'=>$user_id]).'" data-pjax="0">Account Statment</a>
		</li>
		<li class="nav-item'.($controllerID=='user-account' && $actionID=='security-deposit-log' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user-account/security-deposit-log','id'=>$user_id]).'" data-pjax="0">Security Deposits Log</a>
		</li>
		<li class="nav-item'.($controllerID=='user-account' && $actionID=='credit-log' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user-account/credit-log','id'=>$user_id]).'" data-pjax="0">Old Credit History</a>
		</li>';
		//Add/Charge Normal Credit
		if($controllerID=='user-account' && $actionID=='account-statement'){
			$html.='<li class="clearfix action-btns" style="position:absolute;right:0px;">';
			if(Yii::$app->user->identity->user_type!=0){
				if(Yii::$app->menuHelperFunction->checkActionAllowed('charge-credit','user-account')){
					$html.='<a class="btn-danger btn-sm pull-right btn-charge-credit" href="javascript:;" data-userid="'.$user_id.'" data-username="'.$user->fullname.'"><i class="fa fa-minus"></i> Charge</a>';
				}
				if(Yii::$app->menuHelperFunction->checkActionAllowed('add-credit','user-account')){
					$html.='<a class="btn-success btn-sm pull-right btn-add-credit" href="javascript:;" data-userid="'.$user_id.'" data-username="'.$user->fullname.'"><i class="fa fa-plus"></i> Add Credits</a>&nbsp;';
				}
			}
			$html.='</li>';
		}
		//Add/Deduct Deposits
		if($controllerID=='user-account' && $actionID=='security-deposit-log'){
			$html.='<li class="clearfix action-btns" style="position:absolute;right:0px;">';
			if(Yii::$app->user->identity->user_type!=0){
				if(Yii::$app->menuHelperFunction->checkActionAllowed('deduct-security-deposit','user-account')){
					$html.='<a class="btn-danger btn-sm pull-right load-modal" href="javascript:;" data-url="'.Url::to(['user-account/deduct-security-deposit','id'=>$user_id]).'" data-heading="Deduct Security Deposit for '.$user->fullname.'"><i class="fa fa-minus"></i> Deduct Security Deposit</a>';
				}
				if(Yii::$app->menuHelperFunction->checkActionAllowed('add-security-deposit','user-account')){
					$html.='<a class="btn-success btn-sm pull-right load-modal" href="javascript:;" data-url="'.Url::to(['user-account/add-security-deposit','id'=>$user_id]).'" data-heading="Add Security Deposit for '.$user->fullname.'"><i class="fa fa-plus"></i> Add Security Deposit</a>&nbsp;';
				}
			}
			$html.='</li>';
		}
		return $html;
	}

	public function getBoatRequiredTabs($searchModel)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		$listType=$searchModel->listType;
		$html='
		<li class="nav-item'.($listType=='future' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['boat-required/index','listType'=>'future']).'" data-pjax="0">Active Requests ('.Yii::$app->statsFunctions->activeCount.')</a>
		</li>
		<li class="nav-item'.($listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['boat-required/index','listType'=>'history']).'" data-pjax="0">Requests History ('.Yii::$app->statsFunctions->historyCount.')</a>
		</li>
		<li class="nav-item'.($listType=='deleted' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['boat-required/index','listType'=>'deleted']).'" data-pjax="0">Deleted Requests ('.Yii::$app->statsFunctions->trashedCount.')</a>
		</li>';
		return $html;
	}

	public function getUserBookingTabs($searchModel)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		$listType=$searchModel->listType;
		$user=User::findOne($searchModel->user_id);
		$html='
		<li class="nav-item'.($controllerID=='booking' && $actionID=='index' && $listType=='future' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/index','listType'=>'future']).'" data-pjax="0">Active Bookings</a>
		</li>
		<li class="nav-item'.($controllerID=='booking' && $actionID=='index' && $listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/index','listType'=>'history']).'" data-pjax="0">Booking History</a>
		</li>
		<li class="nav-item'.($controllerID=='booking' && $actionID=='index' && $listType=='deleted' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/index','listType'=>'deleted']).'" data-pjax="0">Deleted Bookings</a>
		</li>
		<li class="nav-item'.($controllerID=='site' && $actionID=='my-requests' && $listType=='active' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['site/my-requests','listType'=>'active']).'" data-pjax="0">Active Requests</a>
		</li>
		<li class="nav-item'.($controllerID=='site' && $actionID=='my-requests' && $listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['site/my-requests','listType'=>'history']).'" data-pjax="0">Request History</a>
		</li>';
		return $html;
	}

	public function getUserProfileBookingTabs($searchModel)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		$listType=$searchModel->listType;
		$user=User::findOne($searchModel->user_id);

		$contract_start_date='';
		$contract_end_date='';
		if($searchModel->contract_id!=null){
			$contract=Contract::find()->where(['id'=>$searchModel->contract_id])->asArray()->one();
			if($contract!=null){
				$contract_start_date=$contract['start_date'];
				$contract_end_date=$contract['end_date'];
			}
		}
		$html='
		<li class="nav-item'.($controllerID=='user' && $actionID=='bookings' && $listType=='future' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user/bookings','id'=>$searchModel->user_id,'contract_id'=>$searchModel->contract_id,'listType'=>'future']).'" data-pjax="0">Active Bookings ('.Yii::$app->statsFunctions->getUserActiveBookingCount($searchModel->user_id,$contract_start_date,$contract_end_date).')</a>
		</li>
		<li class="nav-item'.($controllerID=='user' && $actionID=='bookings' && $listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user/bookings','id'=>$searchModel->user_id,'contract_id'=>$searchModel->contract_id,'listType'=>'history']).'" data-pjax="0">Booking History ('.Yii::$app->statsFunctions->getUserOldBookingCount($searchModel->user_id,$contract_start_date,$contract_end_date).')</a>
		</li>
		<li class="nav-item'.($controllerID=='user' && $actionID=='bookings' && $listType=='deleted' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user/bookings','id'=>$searchModel->user_id,'contract_id'=>$searchModel->contract_id,'listType'=>'deleted']).'" data-pjax="0">Deleted Bookings ('.Yii::$app->statsFunctions->getUserDeletedBookingCount($searchModel->user_id,$contract_start_date,$contract_end_date).')</a>
		</li>
		<li class="nav-item'.($controllerID=='user' && $actionID=='bookings' && $listType=='bookingstats' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['user/bookings','id'=>$searchModel->user_id,'contract_id'=>$searchModel->contract_id,'listType'=>'bookingstats']).'" data-pjax="0">Booking Stats</a>
		</li>';
		return $html;
	}

	public function getAllBookingTabs($searchModel)
	{
		$controllerID=Yii::$app->controller->id;
		$actionID=Yii::$app->controller->action->id;
		$listType=$searchModel->listType;
		$html='
		<li class="nav-item'.($controllerID=='booking' && $actionID=='all' && $listType=='future' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/all','listType'=>'future']).'" data-pjax="0">Active Bookings ('.Yii::$app->statsFunctions->activeBookingsCount.')</a>
		</li>
		<li class="nav-item'.($controllerID=='booking' && $actionID=='all' && $listType=='archived' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/all','listType'=>'archived']).'" data-pjax="0">Archived Bookings ('.Yii::$app->statsFunctions->archivedBookingsCount.')</a>
		</li>
		<li class="nav-item'.($controllerID=='booking' && $actionID=='all' && $listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/all','listType'=>'history']).'" data-pjax="0">Booking History ('.Yii::$app->statsFunctions->oldBookingsCount.')</a>
		</li>
		<li class="nav-item'.($controllerID=='booking' && $actionID=='all' && $listType=='all' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/all','listType'=>'all']).'" data-pjax="0">All Booking ('.Yii::$app->statsFunctions->allBookingsCount.')</a>
		</li>
		<li class="nav-item'.($controllerID=='booking' && $actionID=='all' && $listType=='deleted' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['booking/all','listType'=>'deleted']).'" data-pjax="0">Deleted Bookings ('.Yii::$app->statsFunctions->deletedBookingsCount.')</a>
		</li>';
		return $html;
	}

	public function getBulkBookingTabs($searchModel)
	{
		$listType=$searchModel->listType;
		$html='
		<li class="nav-item'.($listType=='active' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['bulk-booking/index','listType'=>'active']).'" data-pjax="0">Active ('.Yii::$app->statsFunctions->activeBulkBookingCount.')</a>
		</li>
		<li class="nav-item'.($listType=='history' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['bulk-booking/index','listType'=>'history']).'" data-pjax="0">History ('.Yii::$app->statsFunctions->historyBulkBookingCount.')</a>
		</li>
		<li class="nav-item'.($listType=='deleted' ? ' active' : '').'">
			<a class="nav-link" href="'.Url::to(['bulk-booking/index','listType'=>'deleted']).'" data-pjax="0">Deleted ('.Yii::$app->statsFunctions->deletedBulkBookingCount.')</a>
		</li>';
		return $html;
	}

  public function getMinimumCredits()
  {
    return 100;
  }


  public function getCanBook()
  {
    $can=true;
    if(Yii::$app->user->identity->user_type==0){
      $fuelCredit = Yii::$app->user->identity->fuelCredit;
      $minBalReq=Yii::$app->controller->getSetting('booking_min_balance');
      $daysRemaining=Yii::$app->user->identity->remainingPackageDays;
      if($daysRemaining>Yii::$app->params['ExpiryDaysToBookInNegative']){
        $minBalReq=Yii::$app->params['ExpiryDaysToBookInNegativeBalance'];
      }
      if($fuelCredit<$minBalReq){
        $can=false;
      }
    }
    return $can;
  }

	public function isWeekend($date)
	{
		$weekDay = date('w', strtotime($date));
		return ($weekDay == 5 || $weekDay == 6);
	}

	public function WeekendDay($date)
	{
		$weekDay = date('w', strtotime($date));
		return $weekDay;
	}

	public function getBookingExperience()
	{
		return [
			'1' => 'Smooth',
			'2' => 'Original Not Available',
			'3' => 'Request',
		];
	}

	public function getTripExperience()
	{
		return [
			'1' => 'Smooth',
			'2' => 'Technical',
			'3' => 'Stuck',
			'4' => 'Accident',
			'6' => 'Incident',
			'5' => 'Other',
		];
	}

	public function getReportTripExperience()
	{
		return [
			'trip1' => 'Smooth',
			'booking3' => 'No Show',
			'booking4' => 'TCC Cancelled - Weather',
			'booking5' => 'TCC Cancelled - Boat not available',
			'booking7' => 'Same Day Cancel',
			'booking8' => 'Weather Warning Cancel',
			'trip2' => 'Technical',
			'trip3' => 'Stuck',
			'trip4' => 'Accident',
			'trip6' => 'Incident',
			'trip5' => 'Other',
		];
	}

	public function getMemberExperience()
	{
		return [
			'1' => 'Late Arrival',
			'2' => 'DUI',
		];
	}

	public function getBookingExtras()
	{
		return [
			'1' => 'Overnight Camp',
			'2' => 'Captain',
		];
	}

	public function getCreditHtml($credits)
	{
		return '<span class="badge grid-badge badge-'.($credits<0 ? 'danger' : 'success').'">Credit: AED '.$credits.'</span>';
	}

	public function getMemberFullName($username,$firstname,$lastname)
	{
		$fullname='';
		if($firstname!=''){
			$fullname.=$firstname;
		}
		if($lastname!=''){
			$fullname.=($fullname!='' ? ' ' : '').$lastname;
		}
		if($fullname==''){
			$fullname=$username;
		}
		return $fullname;
	}

	public function getMemberFullNameWithUsername($username,$firstname,$lastname)
	{
		$fullname='';
		if($firstname!=''){
			$fullname.=$firstname;
		}
		if($lastname!=''){
			$fullname.=($fullname!='' ? ' ' : '').$firstname;
		}
		if($fullname!=''){
			$fullname.=' ('.$username.')';
		}else{
			$fullname=$username;
		}
		return $fullname;
	}

	//Send Admin email in case addons waiting list requested
	public function sendAminAlert($type,$booking)
	{
		$expectedTimeOfReply='';
		if(
			$type=='In House Captain'
			|| $type=='BBQ Set'
			|| $type=='Wake Boarding'
			|| $type=='Wake Surfing'
			|| $type=='Sports Equipment'
			|| $type=='Early Departure'
			|| $type=='Late Arrival'
			|| $type=='Overnight Camping'
		){
			$expectedTimeOfReply=Yii::$app->formatter->asDate(date("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date))));
		}

    $addonsText='';
    if($type=='In House Captain')$addonsText='a captain';
    if($type=='Sports Equipment')$addonsText='a donut';
    if($type=='BBQ Set')$addonsText='a BBQ set';
    if($type=='Early Departure')$addonsText='early departure';
    if($type=='Overnight Camping')$addonsText='overnight camping';
    if($type=='Late Arrival')$addonsText='late arrival';
    if($type=='Wake Surfing')$addonsText='wake surfing';
    if($type=='Wake Boarding')$addonsText='wake boarding';

		$additionalText='';
		if($type=='Sports Equipment'){
			$waitingList=WaitingListSportsEquipment::findOne(['booking_id'=>$booking->id]);
			if($waitingList!=null){
				$additionalText.=' ('.$waitingList->waterSportsEquipment->title.')';
			}

		}

		$textBody = "Hello Admin,\n\r\n\r";
		$textBody.= "A new ".$type.$additionalText." request as been posted, following is the information:\n\r\n\r";
		$textBody.= "Posted Date: ".Yii::$app->formatter->asDate(date("Y-m-d"))."\n\r\n\r";
		$textBody.= "Name: ".$booking->member->fullname."\n\r";
		$textBody.= "Email: ".$booking->member->email."\n\r\n\r";
		$textBody.= "Booking Details:\n\r";
		$textBody.= "City: ".$booking->city->name."\n\r";
		$textBody.= "Marina: ".$booking->marina->name."\n\r";
		$textBody.= "Boat: ".$booking->boat->name."\n\r";
		$textBody.= "Date: ".Yii::$app->formatter->asDate($booking->booking_date)."\n\r";
		$textBody.= "Time: ".$booking->timeZone->name."\n\r";
		if($expectedTimeOfReply!=''){
			$textBody.= "Expected Time of Replay: System will automatically assign ".$addonsText." once it becomes available.\n\r";
			//$textBody.= "Expected Time of Replay: ".$expectedTimeOfReply."\n\r";
		}
		$textBody.="\n\r";

		$htmlBody = "Hello Admin,<br /><br />";
		$htmlBody.= "A new ".$type.$additionalText." request as been posted, following is the information:<br /><br />";
		$htmlBody.= "<strong>Posted Date:</strong> ".Yii::$app->formatter->asDate(date("Y-m-d"))."<br />";
		$htmlBody.= "<strong>Name:</strong> ".$booking->member->fullname."<br />";
		$htmlBody.= "<strong>Email:</strong> ".$booking->member->email."<br /><br />";
		$htmlBody.= "<strong>Booking Details:</strong><br />";
		$htmlBody.= "<strong>City:</strong> ".$booking->city->name."<br />";
		$htmlBody.= "<strong>Marina:</strong> ".$booking->marina->name."<br />";
		$htmlBody.= "<strong>Boat:</strong> ".$booking->boat->name."<br />";
		$htmlBody.= "<strong>Date:</strong> ".Yii::$app->formatter->asDate($booking->booking_date)."<br />";
		$htmlBody.= "<strong>Time:</strong> ".$booking->timeZone->name."<br />";
		if($expectedTimeOfReply!=''){
			$htmlBody.= "<strong>Expected Time of Replay:</strong> System will automatically assign ".$addonsText." once it becomes available.<br />";
			//$htmlBody.= "<strong>Expected Time of Replay:</strong> ".$expectedTimeOfReply."<br />";
		}
		$htmlBody.="<br />";

		$message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
		->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
		->setSubject('New Request Type '.$type.' - ' . Yii::$app->params['siteName'])
		->setTo(Yii::$app->appHelperFunctions->getSetting('requestEmail'))
		->send();
	}

	//Send Member email in case addons waiting list requested
	public function sendWaitingListEmail($type,$booking)
	{
		$expectedTimeOfReply='';
		if(
			$type=='In House Captain'
			|| $type=='BBQ Set'
			|| $type=='Wake Boarding'
			|| $type=='Wake Surfing'
			|| $type=='Sports Equipment'
			|| $type=='Early Departure'
			|| $type=='Late Arrival'
			|| $type=='Overnight Camping'
		){
			$estimatedTimeOfReply=Yii::$app->formatter->asDate(date("Y-m-d", strtotime("-1 day", strtotime($booking->booking_date))));
		}

    $addonsText='';
    if($type=='In House Captain')$addonsText='a captain';
    if($type=='Sports Equipment')$addonsText='a donut';
    if($type=='BBQ Set')$addonsText='a BBQ set';
    if($type=='Early Departure')$addonsText='early departure';
    if($type=='Overnight Camping')$addonsText='overnight camping';
    if($type=='Late Arrival')$addonsText='late arrival';
    if($type=='Wake Surfing')$addonsText='wake surfing';
    if($type=='Wake Boarding')$addonsText='wake boarding';

		$additionalText='';
		if($type=='Sports Equipment'){
			$waitingList=WaitingListSportsEquipment::findOne(['booking_id'=>$booking->id]);
			if($waitingList!=null){
				$additionalText.=' ('.$waitingList->waterSportsEquipment->title.')';
			}
		}

		$templateId=Yii::$app->appHelperFunctions->getSetting('general_response');
		$template=EmailTemplate::findOne($templateId);
		if($template!=null){

			$emailMessage ="Your ".$type.$additionalText." request is under process.\n\r\n\r";
			$emailMessage.="<strong>City:</strong> ".$booking->city->name."\n\r";
			$emailMessage.="<strong>Marina:</strong> ".$booking->marina->name."\n\r";
			$emailMessage.="<strong>Boat:</strong> ".$booking->boat->name."\n\r";
			$emailMessage.="<strong>Date:</strong> ".Yii::$app->formatter->asDate($booking->booking_date)."\n\r";
			$emailMessage.="<strong>Time:</strong> ".$booking->timeZone->name."\n\r";
			if($estimatedTimeOfReply!=''){
				$emailMessage.="<strong>Estimated Time of Reply:</strong> System will automatically assign ".$addonsText." once it becomes available.\n\r\n\r";
				//$emailMessage.="<strong>Estimated Time of Reply:</strong> ".Yii::$app->formatter->asDate($estimatedTimeOfReply)."\n\r\n\r";
			}

			$vals = [
				'{captainName}' => $booking->member->fullname,
				'{emailMessage}' => $emailMessage,
			];
			$htmlBody=nl2br($template->searchReplace($template->template_html,$vals));
			$textBody=$template->searchReplace($template->template_text,$vals);
			$message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
			->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName']])
			->setSubject('Request Sent - ' . Yii::$app->params['siteName'])
			->setTo($booking->member->email)
			->send();
		}
	}

	//Send Member email in case addons waiting list is auto assigned
	public function sendMetEmail($type,$booking)
	{
		$typeText='';
		$additionalText='';
		if($type=='captain')$typeText='In House Captain';
		if($type=='bbq')$typeText='BBQ Set';
		if($type=='wakeboarding')$typeText='Wake Boarding';
		if($type=='wakesurfing')$typeText='Wake Surfing';
		if($type=='equipment'){
			$typeText='Sports Equipment';

			$waitingList=WaitingListSportsEquipment::findOne(['booking_id'=>$booking->id]);
			if($waitingList!=null){
				$additionalText.=' ('.$waitingList->waterSportsEquipment->title.')';
			}
		}
		if($type=='late_arrival')$typeText='Late Arrival';
		if($type=='overnight_camping')$typeText='Overnight Camping';

		$textBody = "Dear captain ".$booking->member->fullname.",\n\r\n\r";
		$textBody.= "Thank you for using the request panel.\n\r";
		$textBody.= "We are glad to inform you that your request for ".$typeText.$additionalText." was successfully MET.\n\r\n\r";
		$textBody.= "Booking Details:\n\r";
		$textBody.= "City: ".$booking->city->name."\n\r";
		$textBody.= "Marina: ".$booking->marina->name."\n\r";
		$textBody.= "Boat: ".$booking->boat->name."\n\r";
		$textBody.= "Date: ".Yii::$app->formatter->asDate($booking->booking_date)."\n\r";
		$textBody.= "Time: ".$booking->timeZone->name."\n\r\n\r";

		$textBody.= "Enjoy Boating With Relief\n\r";
		$textBody.= "Best Regards\n\r\n\r";

		$htmlBody = "Dear captain ".$booking->member->fullname.",<br /><br />";
		$htmlBody.= "Thank you for using the request panel.<br />";
		$htmlBody.= "We are glad to inform you that your request for ".$typeText.$additionalText." was successfully MET.<br /><br />";
		$htmlBody.= "<strong>Booking Details:</strong><br />";
		$htmlBody.= "<strong>City:</strong> ".$booking->city->name."<br />";
		$htmlBody.= "<strong>Marina:</strong> ".$booking->marina->name."<br />";
		$htmlBody.= "<strong>Boat:</strong> ".$booking->boat->name."<br />";
		$htmlBody.= "<strong>Date:</strong> ".Yii::$app->formatter->asDate($booking->booking_date)."<br />";
		$htmlBody.= "<strong>Time:</strong> ".$booking->timeZone->name."<br /><br />";

		$htmlBody.= "Enjoy Boating With Relief<br />";
		$htmlBody.= "Best Regards<br /><br />";

		$message=Yii::$app->mailer->compose(['html' => 'templateMsg-html', 'text' => 'templateMsg-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
		->setFrom([Yii::$app->params['icareEmail'] => Yii::$app->params['siteName']])
		->setSubject('Request Response - ' . Yii::$app->params['siteName'])
		->setTo($booking->member->email)
		->send();

		$smsMsg="Dear Captain\n
		Your Request for ".$typeText.$additionalText." was successfully MET.
		Date: ".Yii::$app->formatter->asDate($booking->booking_date)."\n
		Marina: ".$booking->marina->name."\n
		Time: ".$booking->timeZone->name."\n\n
		Regards.\n\n
		M: ".Yii::$app->params['smsSigNumber']."
		E: ".Yii::$app->params['smsSigEmail']."";

		if($booking->member->profileInfo->profile_updated==1 && $booking->member->profileInfo->mobile!=null && $booking->member->profileInfo->mobile!=''){
			$mobileNumber=Yii::$app->helperFunctions->fullMobileNumber($booking->member->profileInfo->mobile);
			if($mobileNumber!=null && $mobileNumber!=''){
				Yii::$app->helperFunctions->sendSms($mobileNumber,$smsMsg);
			}
		}
	}
}
?>
