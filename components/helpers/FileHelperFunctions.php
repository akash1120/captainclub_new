<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;

class FileHelperFunctions extends Component
{
	public function getDefaultPhoto($type)
	{
		if($type=='user'){
			return 'images/default/default_avatar.png';
		}else{
			return 'images/default/default.png';
		}
	}
	public function getImagePath($source,$image,$size)
	{
		$photo='';
		if($image!='' && $image!=null){
			$ext = pathinfo($image, PATHINFO_EXTENSION);
			if($source=='user'){
				$absPath=Yii::$app->params['member_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='topbar'){
  					$photo=$this->resizeAbsolute($absPath, $image, 25, 25);
  				}elseif($size=='tiny'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}elseif($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}elseif($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}
				}
			}
			if($source=='user_license'){
				$absPath=Yii::$app->params['member_license_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='tiny'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}elseif($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}elseif($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}
				}
			}
			if($source=='discount'){
				$absPath=Yii::$app->params['discount_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}
					if($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 150, 150);
  				}
				}
			}
			if($source=='signature'){
				$absPath=Yii::$app->params['signatures_abs_path'];
				if(file_exists($absPath.$image)){
					$cached=Yii::$app->params['cache_abs_path'].$image;
					if(!file_exists($cached)){
						copy(Yii::$app->params['signatures_abs_path'].$image,Yii::$app->params['cache_abs_path'].$image);
					}
					$photo=Yii::$app->params['cache_rel_path'].$image;
				}
			}
			if($source=='fuelbill'){
				$absPath=Yii::$app->params['fuelbill_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='full'){
  					$photo=$this->resizeAbsolute($absPath, $image, 500, 500);
  				}
				}
			}
			if($source=='claimfuelbill'){
				$absPath=Yii::$app->params['claimbill_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='full'){
  					$photo=$this->resizeAbsolute($absPath, $image, 500, 500);
  				}
				}
			}
			if($source=='watersportequipment'){
				$absPath=Yii::$app->params['wse_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}
					if($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 150, 150);
  				}
				}
			}
			if($source=='ticket'){
				$absPath=Yii::$app->params['ticket_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}
					if($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 150, 150);
  				}
				}
			}
			if($source=='boat'){
				$absPath=Yii::$app->params['boat_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}
					if($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 150, 150);
  				}
					if($size=='large'){
  					$photo=$this->resizeAbsolute($absPath, $image, 300, 200);
  				}
				}
			}
			if($source=='early-departure'){
				$absPath=Yii::$app->params['early_depart_asset_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='tiny'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}
					if($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 150, 150);
  				}
					if($size=='large'){
  					$photo=$this->resizeByHeight($absPath, $image, 200, 200);
  				}
					if($size=='xlarge'){
  					$photo=$this->resizeByHeight($absPath, $image, 800, 800);
  				}
				}
			}
		}
		if($photo==''){
			$photo=$this->getDefaultPhoto($source);
		}
		return $photo;
	}

	public function resizeAbsolute($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}else{

				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->crop($width, $height);
					$image->save(Yii::$app->params['cache_abs_path'] . $new_image);

					//Crop
					//$image = new ImageResize(Yii::$app->params['cache_abs_path'] . $new_image);
					//$image->crop($width, $height);
					//$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
				} else {
					copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
				}
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function resizeByWidth($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}else{
				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->resizeToWidth($width);
					$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
				} else {
					copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
				}
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function resizeByHeight($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}else{
				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->resizeToHeight($width);
					$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
				} else {
					copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
				}
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function resizeBestFit($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

		if (!is_file(Yii::$app->params['cache_abs_path'] . $new_image) || (filectime($folder . $old_image) > filectime(Yii::$app->params['cache_abs_path'] . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
				$image = new ImageResize($folder . $old_image);
				$image->resizeToBestFit($width, $height);
				$image->save(Yii::$app->params['cache_abs_path'] . $new_image);
			} else {
				copy($folder . $old_image, Yii::$app->params['cache_abs_path'] . $new_image);
			}
		}
		return Yii::$app->params['cache_rel_path'] . $new_image;
	}

	public function generateName()
	{
		return str_replace(array(" ","."),"",microtime());
	}

	function generateImageFromSignature($img)
	{
		if($img!=''){
			$folderPath=Yii::$app->params['signatures_abs_path'];
			$image_parts = explode(";base64,", $img);
			if($image_parts!=null){
				$image_type_aux = explode("image/", $image_parts[0]);
				if($image_type_aux!=null){
					$image_type = $image_type_aux[1];
					$image_base64 = base64_decode($image_parts[1]);
					$fileName=$this->generateName() . '.svg';
					$file = $folderPath . $fileName;
					$s3 = Yii::$app->get('s3');
					$result = $s3->commands()->put($fileName, $image_base64)->withContentType('image/svg+xml')->execute();
					$url = $s3->commands()->getUrl($fileName)->execute();
					//file_put_contents($file, $image_base64);
					return $url;
				}
			}
		}
		return '';
	}

	public function generateImageFromUploadedFile($file,$fileName)
	{
		$dataFromFile = file_get_contents($file->tempName);
		$s3 = Yii::$app->get('s3');
		$result = $s3->commands()->put($fileName, $dataFromFile)->withContentType(mime_content_type($file->tempName))->execute();
		$url = $s3->commands()->getUrl($fileName)->execute();
		return $url;
	}

	public function copyToAwsAndRemoveLocal($folder,$fileName)
	{
		ini_set('memory_limit', '-1');
		list($width_orig, $height_orig) = getimagesize($folder . $fileName);

		$image = new ImageResize($folder . $fileName);
		$image->resizeToBestFit($width_orig,$height_orig);
		$image->save($folder . $fileName);

		$dataFromFile = file_get_contents($folder . $fileName);
		$s3 = Yii::$app->get('s3');
		$result = $s3->commands()->put($fileName, $dataFromFile)->withContentType(mime_content_type($folder . $fileName))->execute();
		$url = $s3->commands()->getUrl($fileName)->execute();
		if($url!=''){
			unlink($folder . $fileName);
		}
		return $url;
	}
}
?>
