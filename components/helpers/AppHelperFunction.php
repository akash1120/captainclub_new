<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use app\models\Setting;
use app\models\City;
use app\models\Marina;
use app\models\Boat;
use app\models\TimeSlot;
use app\models\AppColor;
use app\models\AppPriority;
use app\models\PackageOption;
use app\models\Package;
use app\models\PackageGroupToPackage;
use app\models\User;
use app\models\BulkBooking;
use app\models\BookingAlert;
use app\models\UserFreezeRequest;
use app\models\UserNightdriveTrainingRequest;
use app\models\UserNightdriveTrainingRequestRemarks;
use app\models\UserOtherRequest;
use app\models\UserCityUpgradeRequest;
use app\models\UserBookingRequest;
use app\models\BoatInfoField;
use app\models\BoatInfoValue;
use app\models\BoatPurpose;
use app\models\FuelConsumptionType;
use app\models\BoatFuelConsumptionRate;
use app\models\BoatTag;
use app\models\Booking;
use app\models\Contract;
use app\models\ContractMember;
use app\models\PackageCity;
use app\models\RequestDiscussion;
use app\models\BoatAvailableDays;
use app\models\DamageCheckItems;
use app\models\InspectionItems;
use app\models\OperationUserCityMarina;
use app\models\BookingActivity;
use app\models\ClaimOrder;
use app\models\WaitingListCaptain;
use app\models\WaitingListSportsEquipment;
use app\models\WaitingListBbq;
use app\models\WaitingListWakeBoarding;
use app\models\WaitingListWakeSurfing;
use app\models\WaitingListLateArrival;
use app\models\WaitingListOvernightCamping;
use app\models\WaitingListEarlyDeparture;


class AppHelperFunction extends Component
{
	/*
	* Returns Timing Group for suggestions
	*/
	public function getTimingGroups()
	{
		return [
			['title'=>'Morning','is_special'=>0,'available_till'=>'12:00','expiry'=>'12:00','permit_required'=>0,'timeIdz'=>[1,9,10],'primary_id'=>1],
			['title'=>'Afternoon','is_special'=>0,'available_till'=>'19:00','expiry'=>'19:00','permit_required'=>0,'timeIdz'=>[2,11,12],'primary_id'=>2],
			['title'=>'Night Drive','is_special'=>0,'available_till'=>'23:00','expiry'=>'24:00','permit_required'=>1,'timeIdz'=>[5],'primary_id'=>5],
			['title'=>'Night Cruise','is_special'=>1,'available_till'=>'00:00','expiry'=>'24:00','permit_required'=>0,'timeIdz'=>[7],'primary_id'=>7],
			['title'=>'Marina Stay in','is_special'=>1,'available_till'=>'03:00','expiry'=>'24:00','permit_required'=>0,'timeIdz'=>[8],'primary_id'=>8],
			['title'=>'Drop Off','is_special'=>1,'available_till'=>'00:00','expiry'=>'24:00','permit_required'=>0,'timeIdz'=>[6],'primary_id'=>6],
		];
	}

	public static function getSetting($key)
	{
		$value="";
		$model=Setting::find()->where("config_name='$key'")->asArray()->one();
		if($model!=null){
			$value=$model['config_value'];
		}
		return $value;
	}

	public function getBoatTagsList()
	{
		return BoatTag::find()
			->select([
				'id',
				'title',
			])
			->where(['trashed'=>0])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getBoatTagsListArr()
	{
		return ArrayHelper::map($this->boatTagsList,"id","title");
	}

	public function getBoatInfoList()
	{
		return BoatInfoField::find()
			->select([
				'id',
				'title',
			])
			->where(['trashed'=>0])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getBoatInfoListArr()
	{
		return ArrayHelper::map($this->boatInfoList,"id","title");
	}

	public function getBoatPurposeList()
	{
		return BoatPurpose::find()
			->select([
				'id',
				'title',
			])
			->where(['trashed'=>0])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getBoatPurposeListArr()
	{
		return ArrayHelper::map($this->boatPurposeList,"id","title");
	}

	public function getCityList()
	{
		return City::find()
			->select([
				'id',
				'name',
			])
			->where(['trashed'=>0])
			->orderBy(['name'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getCityListArr()
	{
		return ArrayHelper::map($this->cityList,"id","name");
	}

	public function getFirstActiveCity()
	{
		return City::find()
			->select([
				'id',
				'name',
			])
			->where(['status'=>1,'trashed'=>0])
			->orderBy(['name'=>SORT_ASC])
			->asArray()
			->one();
	}

	public function getActiveCityList()
	{
		return City::find()
			->select([
				'id',
				'name',
			])
			->where(['status'=>1,'trashed'=>0])
			->orderBy(['name'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getActiveCityListArr()
	{
		return ArrayHelper::map($this->activeCityList,"id","name");
	}

	public function getMarinaList()
	{
		return Marina::find()
			->select([
				'id',
				'city_id',
				'name',
				'short_name',
			])
			->where(['trashed'=>0])
			->orderBy(['id'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getMarinaListArr()
	{
		return ArrayHelper::map($this->marinaList,"id","name");
	}

	public function getCityMarinaList($cityId)
	{
		return Marina::find()
			->select([
				'id',
				'city_id',
				'name',
			])
			->where(['city_id'=>$cityId,'trashed'=>0])
			->orderBy(['name'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getCityMarinaListArr($cityId)
	{
		return ArrayHelper::map($this->getCityMarinaList($cityId),"id","name");
	}

	public function getActiveMarinaList()
	{
		return Marina::find()
			->select([
				'id',
				'city_id',
				'name',
			])
			->where(['status'=>1,'trashed'=>0])
			->orderBy(['id'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getActiveMarinaListArr()
	{
		return ArrayHelper::map($this->activeMarinaList,"id","name");
	}

	public function getFirstActiveCityMarina($cityId)
	{
		return Marina::find()
			->select([
				'id',
				'city_id',
				'name',
			])
			->where(['city_id'=>$cityId,'status'=>1,'trashed'=>0])
			->orderBy(['rank'=>SORT_ASC])
			->asArray()
			->one();
	}

	public function getActiveCityMarinaList($cityId)
	{
		return Marina::find()
			->select([
				'id',
				'city_id',
				'name',
				'short_name',
				'night_drive_limit',
			])
			->where(['city_id'=>$cityId,'status'=>1,'trashed'=>0])
			->orderBy(['rank'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getActiveCityMarinaListArr($cityId)
	{
		return ArrayHelper::map($this->getActiveCityMarinaList($cityId),"id","name");
	}

	public function getMarinaById($marina_id)
	{
		return Marina::find()
		->select([
			'id',
			'city_id',
			'name',
			'short_name',
		])
		->where(['id'=>$marina_id])
		->asArray()->one();
	}

	public function getBoatsList($city_id=null,$marina_id=null)
	{
		return Boat::find()
			->select([
				Boat::tableName().'.id',
				'name'=>'CONCAT('.City::tableName().'.name," > ", '.Marina::tableName().'.short_name, " > ",'.Boat::tableName().'.name)',
			])
			->innerJoin(City::tableName(),City::tableName().".id=".Boat::tableName().".city_id")
			->innerJoin(Marina::tableName(),Marina::tableName().".id=".Boat::tableName().".port_id")
			->where([Boat::tableName().'.trashed'=>0])
			->andFilterWhere([Boat::tableName().'.city_id'=>$city_id,Boat::tableName().'.port_id'=>$marina_id])
			->orderBy([City::tableName().'.name'=>SORT_ASC,Marina::tableName().'.name'=>SORT_ASC,Boat::tableName().'.name'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getBoatsListArr($city_id=null,$marina_id=null)
	{
		return ArrayHelper::map($this->getBoatsList($city_id,$marina_id),"id","name");
	}

	public function getActiveBoatsList($city_id=null,$marina_id=null)
	{
		return Boat::find()
			->select([
				Boat::tableName().'.id',
				'name'=>'CONCAT('.City::tableName().'.name," > ", '.Marina::tableName().'.short_name, " > ",'.Boat::tableName().'.name)',
			])
			->innerJoin(City::tableName(),City::tableName().".id=".Boat::tableName().".city_id")
			->innerJoin(Marina::tableName(),Marina::tableName().".id=".Boat::tableName().".port_id")
			->where([Boat::tableName().'.status'=>1,Boat::tableName().'.trashed'=>0])
			->andFilterWhere([Boat::tableName().'.city_id'=>$city_id,Boat::tableName().'.port_id'=>$marina_id])
			->orderBy([City::tableName().'.name'=>SORT_ASC,Marina::tableName().'.name'=>SORT_ASC,Boat::tableName().'.name'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getActiveBoatsListArr($city_id=null,$marina_id=null)
	{
		return ArrayHelper::map($this->getActiveBoatsList($city_id,$marina_id),"id","name");
	}

	public function getColorList()
	{
		return AppColor::find()
			->select([
				'id',
				'title',
			])
			->where(['trashed'=>0])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getColorListArr()
	{
		return ArrayHelper::map($this->colorList,"id","title");
	}

	public function getTimeSlotListDashboard()
	{
		return TimeSlot::find()
			->select([
				'id',
				'name',
				'short_name',
				'dashboard_name',
				'is_special',
				'permit_required',
				'available_till',
				'expiry',
			])
			->where(['status'=>1,'trashed'=>0])
			->orderBy(['dashboard_rank'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getTimeSlotListDashboardArr()
	{
		return ArrayHelper::map($this->timeSlotListDashboard,"id","name");
	}

	public function getBookingTimeSlotList()
	{
		return TimeSlot::find()
			->select([
				'id',
				'name',
				'dashboard_name',
				'start_time',
				'end_time',
				'is_special',
				'available_till',
				'expiry',
				'permit_required',
			])
			->where(['parent'=>0,'status'=>1,'trashed'=>0])
			->orderBy(['booking_rank'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getTimeSlotList()
	{
		return TimeSlot::find()
			->select([
				'id',
				'name',
			])
			->where(['status'=>1,'trashed'=>0])
			->orderBy(['id'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getTimeSlotListArr()
	{
		return ArrayHelper::map($this->timeSlotList,"id","name");
	}

	public function getChildTimeSlots($parent)
	{
		$timeIdz[]=$parent;
		$childTimeSlots=TimeSlot::find()->select(['id'])->where(['parent'=>$parent,'trashed'=>0])->asArray()->all();
		if($childTimeSlots!=null){
			foreach($childTimeSlots as $childTimeSlot){
				$timeIdz[]=$childTimeSlot['id'];
			}
		}
		return $timeIdz;
	}

	public function getTimeSlotById($id)
	{
		return TimeSlot::find()
			->select([
				'id',
				'name',
				'short_name',
				'dashboard_name',
				'is_special',
				'permit_required',
				'available_till',
				'expiry',
			])
			->where(['id'=>$id,'trashed'=>0])
			->asArray()
			->one();
	}

	public function getActiveColorList()
	{
		return AppColor::find()
			->select([
				'id',
				'title',
			])
			->where(['status'=>1,'trashed'=>0])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getActiveColorListArr()
	{
		return ArrayHelper::map($this->activeColorList,"id","title");
	}

	public function getAppColorBlock($color_id)
	{
		$html='';
		$result=AppColor::find()->select(['title','code','text_color'])->where(['id'=>$color_id])->asArray()->one();
		if($result!=null){
			$html = '<span class="label" style="background-color:'.$result['code'].';color:'.$result['text_color'].';">'.$result['title'].'</span>';
		}
		return $html;
	}

	public function getPrioritiesList()
	{
		return AppPriority::find()
			->select([
				'id',
				'title',
			])
			->where(['trashed'=>0])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getPrioritiesListArr()
	{
		return ArrayHelper::map($this->prioritiesList,"id","title");
	}

	public function getPackageOptionsList()
	{
		return PackageOption::find()
			->select([
				'id',
				'name',
			])
			->orderBy(['id'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getPackageOptionByKeyword($keyword)
	{
		return PackageOption::find()
			->select([
				'id',
				'name',
			])
			->where(['keyword'=>$keyword])
			->asArray()
			->one();
	}

	public function getPackageOptionsListArr()
	{
		return ArrayHelper::map($this->packageOptionsList,"id","name");
	}

	public function getPackageList()
	{
		return Package::find()
			->select([
				'id',
				'name',
			])
			->orderBy(['name'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getPackageListArr()
	{
		return ArrayHelper::map($this->packageList,"id","name");
	}

	public function getDamageCheckItems()
	{
		return DamageCheckItems::find()
			->select([
				'id',
				'title',
			])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getDamageCheckItemsArr()
	{
		return ArrayHelper::map($this->damageCheckItems,"id","title");
	}

	public function getInspectionItems()
	{
		return InspectionItems::find()
			->select([
				'id',
				'title',
			])
			->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
	}

	public function getInspectionItemsArr()
	{
		return ArrayHelper::map($this->inspectionItems,"id","title");
	}

	public function getGroupPackages($group_id)
	{
		$subQueryGroupPackages=PackageGroupToPackage::find()->select(['package_id'])->where(['package_group_id'=>$group_id]);
		return Package::find()->select(['id','name'])->where(['id'=>$subQueryGroupPackages])->asArray()->all();
	}

  public function getStaffMembersList()
  {
    return User::find()->select(['id','username'])->where(['user_type'=>1])->asArray()->all();
  }

  public function getAdminIdz()
  {
    return User::find()->select(['id'])->where(['user_type'=>1]);
  }

  public function getAdminIdzList()
  {
    return User::find()->select(['id'])->where(['user_type'=>1])->asArray()->all();
  }

  public function getAdminIdzArr()
  {
		$idz=[];
		$adminIdz=$this->adminIdzList;
		if($adminIdz!=null){
			foreach($adminIdz as $key=>$val){
				$idz[]=$val['id'];
			}
		}
		return $idz;
  }

  public function getStaffMembersArr()
  {
    return ArrayHelper::map($this->staffMembersList,"id","username");;
  }

	public function getBookingAlerts($marina_id,$date)
	{
		return BookingAlert::find()->where(['port_id'=>$marina_id,'alert_date'=>$date,'trashed'=>0])->orderBy(['id'=>SORT_DESC,'alert_type'=>SORT_DESC])->one();
	}

	public function getRequestDetail_old($type,$id)
	{
		if($type=='freeze'){
			$result=UserFreezeRequest::findOne($id);
			if($result!=null){
				$html = '<strong>Freeze Confirmed</strong>';
				$html.= '<br />';
				$html.= '<strong>From: </strong>'.Yii::$app->formatter->asDate($result->start_date).' <strong>To: </strong>'.Yii::$app->formatter->asDate($result->end_date);
				return $html;
			}
		}
		if($type=='nightdrivetraining'){
			$result=UserNightdriveTrainingRequest::findOne($id);
			if($result!=null){
				$html = '<strong>Night Drive Training</strong>';
				$html.= '<br />';
				$html.= $result->descp;
				return $html;
			}
		}
		if($type=='upgradecity'){
			$result=UserCityUpgradeRequest::findOne($id);
			if($result!=null){
				$html= $result->descp;
				return $html;
			}
		}
		if($type=='other'){
			$result=UserOtherRequest::findOne($id);
			if($result!=null){
				$html = '<strong>Other</strong>';
				$html.= '<br />';
				$html.= ($result->requested_date!=null ? '<strong>Requested Date:</strong> '.Yii::$app->formatter->asDate($result->requested_date).'<br />' : '');
				$html.= $result->descp;
				if($result->booking_id>0){
					$resultB=Booking::findOne($result->booking_id);
					if($resultB!=null){
						$html.= '<hr /><strong>Booking Info</strong>';
						$html.= '<br />';
						$html.= '<strong>City:</strong> '.($resultB->city!=null ? $resultB->city->name : '').'<br />';
						$html.= '<strong>Marina:</strong> '.$resultB->marina->name.'<br />';
						$html.= '<strong>Requested Date:</strong> '.Yii::$app->formatter->asDate($resultB->booking_date).'<br />';
						$html.= '<strong>Time:</strong> '.$resultB->timeZone->name.'<br />';
					}
				}
				return $html;
			}
		}
		if($type=='boatbooking'){
			$result=UserBookingRequest::findOne($id);
			if($result!=null){
				$html = '<strong>Boat Booking</strong>';
				$html.= '<br />';
				$html.= '<strong>City:</strong> '.($result->city!=null ? $result->city->name : '').'<br />';
				$html.= '<strong>Marina:</strong> '.$result->marina.'<br />';
				$html.= '<strong>Requested Date:</strong> '.Yii::$app->formatter->asDate($result->requested_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->time_slot.'<br />';
				$html.= $result->descp;
				return $html;
			}
		}
		if($type=='waitingcaptain'){
			$result=WaitingListCaptain::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>In house Captain</strong><br />';
				}else{
					$html = '<strong>Waiting List - Captain</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='waitingequipment'){
			$result=WaitingListSportsEquipment::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>Sports Equipment ('.$result->waterSportsEquipment->title.')</strong><br />';
				}else{
					$html = '<strong>Waiting List - Sports Equipment ('.$result->waterSportsEquipment->title.')</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='waitingbbq'){
			$result=WaitingListBbq::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>BBQ Set</strong><br />';
				}else{
					$html = '<strong>Waiting List - BBQ Set</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='waitingwakeboarding'){
			$result=WaitingListWakeBoarding::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>Wake Boarding</strong><br />';
				}else{
					$html = '<strong>Waiting List - Wake Boarding</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='waitingwakesurfing'){
			$result=WaitingListWakeSurfing::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>Wake Surfing</strong><br />';
				}else{
					$html = '<strong>Waiting List - Wake Surfing</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='bookingice'){
			$result=Booking::findOne(['id'=>$id]);
			if($result!=null){
				$html = '<strong>Ice</strong><br />';
				$html.= '<strong>City:</strong> '.$result->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='bookingkidsjackets'){
			$result=Booking::findOne(['id'=>$id]);
			if($result!=null){
				$html = '<strong>Kids Life Jackets</strong><br />';
				$html.= '<strong>City:</strong> '.$result->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='waitinglatearrival'){
			$result=WaitingListLateArrival::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>Late Arrival</strong><br />';
				}else{
					$html = '<strong>Waiting List - Late Arrival</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='waitingovernightcamping'){
			$result=WaitingListOvernightCamping::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>Overnight Camping</strong><br />';
				}else{
					$html = '<strong>Waiting List - Overnight Camping</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
		if($type=='waitingearlydeparture'){
			$result=WaitingListEarlyDeparture::findOne(['id'=>$id]);
			if($result!=null){
				if($result->status==1){
					$html = '<strong>Early Departure</strong><br />';
				}else{
					$html = '<strong>Waiting List - Early Departure</strong><br />';
				}
				$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
				$html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
				$html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
				$html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
				$html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';
				return $html;
			}
		}
	}

    public function getRequestDetail($type,$id)
    {
        if($type=='freeze'){
            $result=UserFreezeRequest::findOne($id);
            if($result!=null){
                $html = '<strong>Freeze Confirmed</strong>';
                $html.= '<br />';
                $html.= '<strong>From: </strong>'.Yii::$app->formatter->asDate($result->start_date).' <strong>To: </strong>'.Yii::$app->formatter->asDate($result->end_date);
                return $html;
            }
        }
        if($type=='nightdrivetraining'){
            $result=UserNightdriveTrainingRequest::findOne($id);
            if($result!=null){
                $html = '<strong>Night Drive Training</strong>';
                $html.= '<br />';
                $html.= $result->descp;
                return $html;
            }
        }
        if($type=='upgradecity'){
            $result=UserCityUpgradeRequest::findOne($id);
            if($result!=null){
                $html= $result->descp;
                return $html;
            }
        }
        if($type=='other'){
            $result=UserOtherRequest::findOne($id);
            if($result!=null){
                $html = '<strong>Other</strong>';
                $html.= '<br />';
                $html.= ($result->requested_date!=null ? '<strong>Requested Date:</strong> '.Yii::$app->formatter->asDate($result->requested_date).'<br />' : '');
                $html.= $result->descp;
                if($result->booking_id>0){
                    $resultB=Booking::findOne($result->booking_id);
                    if($resultB!=null){
                        $html.= '<hr /><strong>Booking Info</strong>';
                        $html.= '<br />';
                        $html.= '<table >';
                        $html.= '<tr>';
                        $html.= '<th>City:</th>';
                        $html.= '<th>Marina:</th>';
                        $html.= '<th>Requested Date:</th>';
                        $html.= '<th>Time:</th>';
                        $html.= '</tr>';
                        $html.= '<tr>';
                        $html.= '<td>'.($resultB->city!=null ? $resultB->city->name : "").'</td>';
                        $html.= '<td>'.$resultB->marina->name.'</td>';
                        $html.= '<td>'.Yii::$app->formatter->asDate($resultB->booking_date).'</td>';
                        $html.= '<td>'.$resultB->timeZone->name.'</td>';
                        $html.= '</tr>';
                        $html.= '</table>';
                        /*						$html.= '<strong>City:</strong> '.($resultB->city!=null ? $resultB->city->name : '').'<br />';
                                                $html.= '<strong>Marina:</strong> '.$resultB->marina->name.'<br />';
                                                $html.= '<strong>Requested Date:</strong> '.Yii::$app->formatter->asDate($resultB->booking_date).'<br />';
                                                $html.= '<strong>Time:</strong> '.$resultB->timeZone->name.'<br />';*/
                    }
                }
                return $html;
            }
        }
        if($type=='boatbooking'){
            $result=UserBookingRequest::findOne($id);
            if($result!=null){
                $html = '<strong>Boat Booking</strong>';
                $html.= '<br />';

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Requested Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.($result->city!=null ? $result->city->name : '').'</td>';
                $html.= '<td>'.$result->marina.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->requested_date).'</td>';
                $html.= '<td>'.$result->time_slot.'</td>';
                $html.= '</tr>';
                $html.= '</table>';


                /*$html.= '<strong>City:</strong> '.($result->city!=null ? $result->city->name : '').'<br />';
                $html.= '<strong>Marina:</strong> '.$result->marina.'<br />';
                $html.= '<strong>Requested Date:</strong> '.Yii::$app->formatter->asDate($result->requested_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->time_slot.'<br />';*/
                $html.= $result->descp;
                return $html;
            }
        }
        if($type=='waitingcaptain'){
            $result=WaitingListCaptain::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>In house Captain</strong><br />';
                }else{
                    $html = '<strong>Waiting List - Captain</strong><br />';
                }

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';




                /*$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='waitingequipment'){
            $result=WaitingListSportsEquipment::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>Sports Equipment ('.$result->waterSportsEquipment->title.')</strong><br />';
                }else{
                    $html = '<strong>Waiting List - Sports Equipment ('.$result->waterSportsEquipment->title.')</strong><br />';
                }

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';


                /*$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='waitingbbq'){
            $result=WaitingListBbq::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>BBQ Set</strong><br />';
                }else{
                    $html = '<strong>Waiting List - BBQ Set</strong><br />';
                }

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';

                /*	$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                    $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                    $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                    $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                    $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='waitingwakeboarding'){
            $result=WaitingListWakeBoarding::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>Wake Boarding</strong><br />';
                }else{
                    $html = '<strong>Waiting List - Wake Boarding</strong><br />';
                }

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';

                /*$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='waitingwakesurfing'){
            $result=WaitingListWakeSurfing::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>Wake Surfing</strong><br />';
                }else{
                    $html = '<strong>Waiting List - Wake Surfing</strong><br />';
                }

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';

                /*$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='bookingice'){
            $result=Booking::findOne(['id'=>$id]);
            if($result!=null){
                $html = '<strong>Ice</strong><br />';
                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->city->name.'</td>';
                $html.= '<td>'.$result->marina->name.'</td>';
                $html.= '<td>'.$result->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking_date).'</td>';
                $html.= '<td>'.$result->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';




                /*$html = '<strong>Ice</strong><br />';
                $html.= '<strong>City:</strong> '.$result->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='bookingkidsjackets'){
            $result=Booking::findOne(['id'=>$id]);
            if($result!=null){
                $html = '<strong>Kids Life Jackets</strong><br />';

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->city->name.'</td>';
                $html.= '<td>'.$result->marina->name.'</td>';
                $html.= '<td>'.$result->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking_date).'</td>';
                $html.= '<td>'.$result->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';

                /*$html.= '<strong>City:</strong> '.$result->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='waitinglatearrival'){
            $result=WaitingListLateArrival::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>Late Arrival</strong><br />';
                }else{
                    $html = '<strong>Waiting List - Late Arrival</strong><br />';
                }

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';


                /*$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='waitingovernightcamping'){
            $result=WaitingListOvernightCamping::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>Overnight Camping</strong><br />';
                }else{
                    $html = '<strong>Waiting List - Overnight Camping</strong><br />';
                }
                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';
                /*$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
        if($type=='waitingearlydeparture'){
            $result=WaitingListEarlyDeparture::findOne(['id'=>$id]);
            if($result!=null){
                if($result->status==1){
                    $html = '<strong>Early Departure</strong><br />';
                }else{
                    $html = '<strong>Waiting List - Early Departure</strong><br />';
                }

                $html.= '<table class="table table-responsive-lg table-sm table-striped table-bordered table-hover mb-0" >';
                $html.= '<tr>';
                $html.= '<th>City</th>';
                $html.= '<th>Marina</th>';
                $html.= '<th>Boat</th>';
                $html.= '<th>Date</th>';
                $html.= '<th>Time</th>';
                $html.= '</tr>';
                $html.= '<tr>';
                $html.= '<td>'.$result->booking->city->name.'</td>';
                $html.= '<td>'.$result->booking->marina->name.'</td>';
                $html.= '<td>'.$result->booking->boatName.'</td>';
                $html.= '<td>'.Yii::$app->formatter->asDate($result->booking->booking_date).'</td>';
                $html.= '<td>'.$result->booking->timeZone->name.'</td>';
                $html.= '</tr>';
                $html.= '</table>';


                /*$html.= '<strong>City:</strong> '.$result->booking->city->name.'<br />';
                $html.= '<strong>Marina:</strong> '.$result->booking->marina->name.'<br />';
                $html.= '<strong>Boat:</strong> '.$result->booking->boatName.'<br />';
                $html.= '<strong>Date:</strong> '.Yii::$app->formatter->asDate($result->booking->booking_date).'<br />';
                $html.= '<strong>Time:</strong> '.$result->booking->timeZone->name.'<br />';*/
                return $html;
            }
        }
    }

	public function getRequestEstimatedTimeOfReply($type,$id)
	{
		$dayCount=-1;
		if(
			$type=='waitingcaptain'
			|| $type=='waitingequipment'
			|| $type=='waitingbbq'
			|| $type=='waitingwakeboarding'
			|| $type=='waitingwakesurfing'
			|| $type=='waitingovernightcamping'
			|| $type=='waitingearlydeparture'
		){
			if($type=='waitingcaptain'){
				$result=WaitingListCaptain::findOne(['id'=>$id]);
			}elseif($type=='waitingequipment'){
				$result=WaitingListSportsEquipment::findOne(['id'=>$id]);
			}elseif($type=='waitingbbq'){
				$result=WaitingListBbq::findOne(['id'=>$id]);
			}elseif($type=='waitingwakeboarding'){
				$result=WaitingListWakeBoarding::findOne(['id'=>$id]);
			}elseif($type=='waitingwakesurfing'){
				$result=WaitingListWakeSurfing::findOne(['id'=>$id]);
			}elseif($type=='waitingovernightcamping'){
				$result=WaitingListOvernightCamping::findOne(['id'=>$id]);
			}elseif($type=='waitingearlydeparture'){
				$result=WaitingListEarlyDeparture::findOne(['id'=>$id]);
			}
			if($result!=null){
				if($result->status==1){
					return '';
				}else{
					$date=$result->booking->booking_date;
					$estimatedDate=date("Y-m-d", strtotime($dayCount." day", strtotime($date)));
					$estRawDate=$estimatedDate;
					$estimatedDate=Yii::$app->formatter->asDate($estimatedDate);
					if($estRawDate==date("Y-m-d")){
						$estimatedDate='24 Hrs';
					}
					if($estRawDate<date("Y-m-d")){
						$estimatedDate='';
					}
					return $estimatedDate;
				}
			}
		}
		if($type=='other'){
			$result=UserOtherRequest::findOne($id);
			if($result!=null){
				$date=$result->requested_date;
				$estimatedDate=date("Y-m-d", strtotime($dayCount." day", strtotime($date)));
				$estRawDate=$estimatedDate;
				$estimatedDate=Yii::$app->formatter->asDate($estimatedDate);
				if($estRawDate==date("Y-m-d")){
					$estimatedDate='24 Hrs';
				}
				if($estRawDate<date("Y-m-d")){
					$estimatedDate='';
				}
				return $estimatedDate;
			}
		}
		return '';
	}

	public function getNDTrainingRemarks($id)
	{
		$html='';
		$trainingRemarks=UserNightdriveTrainingRequestRemarks::find()->where(['user_request_id'=>$id])->orderBy(['id'=>SORT_DESC])->one();
    if($trainingRemarks!=null){
      $html.='<hr /><strong>Training Date: </strong>'.Yii::$app->formatter->asDate($trainingRemarks->training_date);
      $html.='<br /><strong>Training Marina: </strong>'.$trainingRemarks->marina->name;
      $html.='<br /><strong>Training Remarks: </strong>'.$trainingRemarks->remarks;
    }
		return $html;
	}


  public function getBoatInfoHtml($id)
  {
    $html='';
    $infoFields=BoatInfoValue::find()
      ->select([
        BoatInfoField::tableName().'.title',
        BoatInfoValue::tableName().'.field_value',
      ])
      ->innerJoin("boat_info_field",BoatInfoField::tableName().".id=".BoatInfoValue::tableName().".info_field_id")
      ->where([
        BoatInfoValue::tableName().'.boat_id'=>$id,
        BoatInfoField::tableName().'.status'=>1,
        BoatInfoField::tableName().'.trashed'=>0
      ])
      ->orderBy([BoatInfoField::tableName().'.sort_order'=>SORT_ASC])
      ->asArray()->all();
    if($infoFields!=null){
      $html.='
      <strong>Boat Info</strong>
      <table width="100%" border="1" cellspacing="0" cellpadding="3" style="margin-top:10px;font-size:12px;">
        <tr>';
        foreach($infoFields as $infoField){
          $html.='<th>'.$infoField['title'].'</th>';
        }
        $html.='</tr>
        <tr>';
        foreach($infoFields as $infoField){
          $html.='<td align="center">'.$infoField['field_value'].'</td>';
        }
        $html.='</tr>
      </table>';
    }
    return $html;
  }

  public function getBoatInfoText($id)
  {
    $text='';
    $infoFields=BoatInfoValue::find()
      ->select([
        BoatInfoField::tableName().'.title',
        BoatInfoValue::tableName().'.field_value',
      ])
      ->innerJoin("boat_info_field",BoatInfoField::tableName().".id=".BoatInfoValue::tableName().".info_field_id")
      ->where([
        BoatInfoValue::tableName().'.boat_id'=>$id,
        BoatInfoField::tableName().'.status'=>1,
        BoatInfoField::tableName().'.trashed'=>0
      ])
      ->orderBy([BoatInfoField::tableName().'.sort_order'=>SORT_ASC])
      ->asArray()->all();
    if($infoFields!=null){
      $text.="Boat Info:\n";
      foreach($infoFields as $infoField){
        $text.=$infoField['title'].": ".$infoField['field_value']."\n";
      }
    }
    return $text;
  }

  public function getBoatFuelConsumptionChartHtml($id)
  {
    $html='';
  	$fuelConsumptionTypes=FuelConsumptionType::find()->asArray()->all();
  	if($fuelConsumptionTypes!=null){
  	$html='
    <strong>Fuel Consumption Chart</strong>
		<table width="100%" border="1" cellspacing="0" cellpadding="3" style="margin-top:10px;font-size:12px;">
			<tr>';
  	foreach($fuelConsumptionTypes as $fuelConsumptionType){
  	$html.='			<th>'.$fuelConsumptionType['title'].'</th>';
  	}
  	$html.='		</tr>
  			<tr>';
  				foreach($fuelConsumptionTypes as $fuelConsumptionType){
  					$valRow=BoatFuelConsumptionRate::find()->where(['boat_id'=>$id,'type_id'=>$fuelConsumptionType['id']])->asArray()->one();
  					if($valRow!=null){
  	$html.='			<th>'.$valRow['rate_value'].'</th>';
  					}
  				}
  	$html.='		</tr>
  		</table>';
  	}
    return $html;
  }

  public function getBoatFuelConsumptionChartText($id)
  {
    $text='';
  	$fuelConsumptionTypes=FuelConsumptionType::find()->asArray()->all();
  	if($fuelConsumptionTypes!=null){
    	$text="Fuel Consumption Chart\n";
    	foreach($fuelConsumptionTypes as $fuelConsumptionType){
        $valRow=BoatFuelConsumptionRate::find()->where(['boat_id'=>$id,'type_id'=>$fuelConsumptionType['id']])->asArray()->one();
        if($valRow!=null){
    	     $text.=$fuelConsumptionType['title'].": ".$valRow['rate_value']."\n";
        }
    	}
  	}
    return $text;
  }

	public function getSubBoatSelectionBoatIds()
	{
		$idz=[];
		$results=Boat::find()->select(['id','name'])->where(['special_boat'=>1,'boat_selection'=>1,'trashed'=>0])->asArray()->all();
		if($results!=null){
			foreach($results as $result){
				$idz[]=$result['id'];
			}
		}
		return $idz;
	}

	public function getNightCruiseIds()
	{
		$idz=[];
		$results=Boat::find()->select(['id','name'])->where(['special_boat'=>1,'special_boat_type'=>1,'trashed'=>0])->asArray()->all();
		if($results!=null){
			foreach($results as $result){
				$idz[]=$result['id'];
			}
		}
		return $idz;
	}

	public function getMarinaStayInIds()
	{
		$idz=[];
		$results=Boat::find()->select(['id','name'])->where(['special_boat'=>1,'special_boat_type'=>2,'trashed'=>0])->asArray()->all();
		if($results!=null){
			foreach($results as $result){
				$idz[]=$result['id'];
			}
		}
		return $idz;
	}

	public function getDropOffIds()
	{
		$idz=[];
		$results=Boat::find()->select(['id','name'])->where(['special_boat'=>1,'special_boat_type'=>3,'trashed'=>0])->asArray()->all();
		if($results!=null){
			foreach($results as $result){
				$idz[]=$result['id'];
			}
		}
		return $idz;
	}

	public function getRestrictedTodayBookingBoats()
	{
		$boatIdz=[];
		$nightCruiseIds=$this->nightCruiseIds;
		if($nightCruiseIds!=null){
			foreach($nightCruiseIds as $key=>$val){
				$boatIdz[]=$val;
			}
		}
		$dropOffIds=$this->dropOffIds;
		if($dropOffIds!=null){
			foreach($dropOffIds as $key=>$val){
				$boatIdz[]=$val;
			}
		}
		return $boatIdz;
	}

	public function getCityIdz()
	{
		return [
			'onlyabudhabi' => 735,
			'onlydubai' => 736,
		];
	}

	public function getCaptainPrice()
	{
		return 250;
	}

	public function getBbqPrice()
	{
		return 150;
	}

	public function getWakeBoardingPrice()
	{
		return 100;
	}

	public function getWakeSurfingPrice()
	{
		return 100;
	}

  public function getBBQMsg()
  {
    return '<div style=\"text-align:left;\">Your BBQ SET consists of the following:<br />6 chairs<br />1 table<br />1 Huge beach rug<br />1 Grill<br />3 KG of Coal<br />Flammable Fuel,<br />Waste bags<br />Cooler and Ice<br />Tissue Box<br />Kindly Note that a charge of <span style=\"color:#F00;\">'.$this->bbqPrice.' AED</span> is applicable. Please in cash in the marina</div>';
  }

	public function getContractByBooking($booking)
	{
		$subQueryContractMember=ContractMember::find()->select(['contract_id'])->where(['user_id'=>$booking->user_id,'status'=>1]);
		return Contract::find()->where(['and',['id'=>$subQueryContractMember],['<=','start_date',$booking->booking_date],['>=','end_date',$booking->booking_date]])->asArray()->one();
	}

  public function getAllowedCities()
  {
    $cities=[];
    $activeContract=Yii::$app->user->identity->activeContract;
    $results=PackageCity::find()->select(['city_id'])->where(['package_id'=>$activeContract->package_id])->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $cities[]=$result['city_id'];
      }
    }
    return $cities;
  }

  public function getUserAllowedCities($user_id)
  {
    $cities=[];
		$user=User::findOne($user_id);
		if($user!=null){
	    $activeContract=$user->activeContract;
	    $results=PackageCity::find()->select(['city_id'])->where(['package_id'=>$activeContract->package_id])->asArray()->all();
	    if($results!=null){
	      foreach($results as $result){
	        $cities[]=$result['city_id'];
	      }
	    }
		}
    return $cities;
  }

	public function getRequestDiscussionLastComment($item_type,$id)
	{
		$txtMsg='';
		$row=RequestDiscussion::find()->where(['request_type'=>$item_type,'request_id'=>$id])->orderBy(['id'=>SORT_DESC])->one();
		if($row!=null){
			$txtMsg=$row->createdBy.' -  <small><i class="fas fa-clock"></i> '.Yii::$app->formatter->asDate($row->created_at).'</small><hr style="margin:5px 0;" />'.$row->comments.'<br />';
		}
		return $txtMsg;
	}

	public function checkBoatAvailability($boat_id,$date)
	{
		$dayOfWeek=Yii::$app->helperFunctions->WeekendDay($date);
		$available=true;
		$boatAvailability=BoatAvailableDays::find()->where(['boat_id'=>$boat_id,'available_day'=>$dayOfWeek]);
		if(!$boatAvailability->exists()){
			$available=false;
		}
		return $available;
	}

	public function checkBoatAvailabilityByBulkBooking($boat_id,$date)
	{
		$status=false;
		$bulkBooked = BulkBooking::find()->where(['and',['boat_id'=>$boat_id,'trashed'=>0],['<=','start_date',$date],['>=','end_date',$date]])->one();
		if($bulkBooked!=null){
			$status=true;
		}
		return $status;
	}

	public function getMinPetrolConsumption($marina_id)
	{
		return 15;
	}

	public function getOperationUserCityMarina()
	{
		return OperationUserCityMarina::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->one();
	}

	public function getUnClaimedMarinaAmount($marina_id)
	{

			$profit=0;
			$marinaProfit=Marina::find()->select(['fuel_profit'])->where(['id'=>$marina_id])->asArray()->one();
			if($marinaProfit!=null){
				$profit=$marinaProfit['fuel_profit'];
			}

			$marinaAmount=BookingActivity::find()
			->innerJoin(Booking::tableName(),Booking::tableName().".id=".BookingActivity::tableName().".booking_id")
			->where(['port_id'=>$marina_id,'hide_other'=>1,'claimed'=>'0','fuel_chargeable'=>1])
			->sum('(fuel_cost-'.$profit.')');

			$alreadyClaimed = ClaimOrder::find()->where(['marina_id'=>$marina_id,'amt_claimed'=>0])->sum('total_amount');

			return $marinaAmount-$alreadyClaimed;
	}

	public function getBoatPurposeById($id)
	{
		return BoatPurpose::find()->select(['title'])->where(['id'=>$id])->one();
	}
}
?>
