<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use app\models\BookingAlert;
use app\models\UserNote;
use app\models\ContractMember;
use app\models\Contract;
use app\models\Booking;
use app\models\User;
use app\models\UserAccounts;
use app\models\Order;
use app\models\BoatRequired;
use app\models\BulkBooking;
use app\models\BoatAvailableDays;
use app\models\Boat;
use app\models\BoatToTimeSlot;
use app\models\TimeSlot;
use app\models\BookingSpecialBoatSelection;
use app\models\Marina;

use app\models\BookingActivity;

class StatsFunctions extends Component
{
  public function getActiveBookingsCount()
  {
    return Booking::find()->where(['and',['is_archive'=>0,'trashed'=>0],['>=','booking_date',date("Y-m-d")]])->count('id');
  }

  public function getArchivedBookingsCount()
  {
    return Booking::find()->where(['is_archive'=>1])->count('id');
  }

  public function getOldBookingsCount()
  {
    return Booking::find()->where(['and',['trashed'=>0],['<','booking_date',date("Y-m-d")]])->count('id');
  }

  public function getAllBookingsCount()
  {
    return Booking::find()->where(['trashed'=>0])->count('id');
  }

  public function getDeletedBookingsCount()
  {
    return Booking::find()->where(['trashed'=>1])->count('id');
  }

  public function getUserActiveBookingCount($user_id,$start,$end)
  {
    return Booking::find()->where(['and',['user_id'=>$user_id,'trashed'=>0],['>=','booking_date',$start],['<=','booking_date',$end],['>=','booking_date',date("Y-m-d")]])->count('id');
  }

  public function getUserOldBookingCount($user_id,$start,$end)
  {
    return Booking::find()->where(['and',['user_id'=>$user_id,'trashed'=>0],['>=','booking_date',$start],['<=','booking_date',$end],['<','booking_date',date("Y-m-d")]])->count('id');
  }

  public function getUserDeletedBookingCount($user_id,$start,$end)
  {
    return Booking::find()->where(['and',['user_id'=>$user_id,'trashed'=>1],['>=','booking_date',$start],['<=','booking_date',$end]])->count('id');
  }

  public function getUserAllOldBookingCount($user_id)
  {
    return Booking::find()->where(['user_id'=>$user_id,'trashed'=>0])->count('id');
  }

  public function getActiveAlertCount($alert_type)
  {
    return BookingAlert::find()->where(['and',['alert_type'=>$alert_type],['>=','DATE(alert_date)',date("Y-m-d")]])->count('id');
  }

  public function getHistoryAlertCount($alert_type)
  {
    return BookingAlert::find()->where(['and',['alert_type'=>$alert_type],['<','DATE(alert_date)',date("Y-m-d")]])->count('id');
  }

  public function getUserTaskCount($user_id)
  {
    return UserNote::find()->where(['user_id'=>$user_id,'trashed'=>0])->count('id');
  }

  public function getUserFutureTaskCount($user_id)
  {
    return UserNote::find()->where(['and',['user_id'=>$user_id,'trashed'=>0],['>=','DATE(reminder)',date("Y-m-d")]])->count('id');
  }

  public function getUserPendingTaskCount($user_id)
  {
    return UserNote::find()->where(['and',['user_id'=>$user_id,'trashed'=>0],['<','DATE(reminder)',date("Y-m-d")],['or',['remarks'=>NULL],['=','remarks','']]])->count('id');
  }

  public function getUserContractCount($user_id)
  {
    $subQueryUser=ContractMember::find()->select(['contract_id'])->where(['user_id'=>$user_id]);
    return Contract::find()->where(['id'=>$subQueryUser])->count('id');
  }

  public function getCaptainsByContract($contract_id,$user=null)
  {
    $allowed=0;
    $remaining=0;
    $freeused=0;
    $paidused=0;
    $contract=Contract::find()->where(['id'=>$contract_id])->asArray()->one();
    if($contract!=null){
      $subQueryContractMembers=ContractMember::find()->select(['user_id'])->where(['contract_id'=>$contract['id']]);
      $allowed=$contract['allowed_captains'];

      $startDate=$contract['start_date'];
      $endDate=$contract['end_date'];

      $freeUsed=Booking::find()
      ->select('id')
      ->where([
        'and',
        ['>=','booking_date',$startDate],
        ['<=','booking_date',$endDate],
        ['in','user_id',$subQueryContractMembers],
        ['captain'=>1,'status'=>1,'trashed'=>0],
        [
          'or',
          ['captain_type'=>'free'],
          ['captain_type'=>NULL],
        ]
      ])
      ->count();
      $paidused=Booking::find()
      ->select('id')
      ->where([
        'and',
        ['>=','booking_date',$startDate],
        ['<=','booking_date',$endDate],
        ['in','user_id',$subQueryContractMembers],
        ['captain'=>1,'captain_type'=>'paid','status'=>1,'trashed'=>0],
      ])
      ->count();

      $remaining=$allowed-$freeUsed;
      $freeused=$freeUsed;
    }
    return ['allowed'=>$allowed,'remaining'=>$remaining,'freeused'=>$freeused,'paidused'=>$paidused];
  }

  public function getFreezeDaysByContract($contract_id)
  {
    $allowed=0;
    $remaining=0;
    $used=0;
    $contract=Contract::find()->where(['id'=>$contract_id])->asArray()->one();
    if($contract!=null){
      $allowed=$contract['allowed_freeze_days'];
      $remaining=$contract['remaining_freeze_days'];
      $used=$allowed-$remaining;
    }
    return ['allowed'=>$allowed,'remaining'=>$remaining,'used'=>$used];
  }

  public function getRemainingFreezeDays($user_id)
  {
    //Not used
    $remaining=0;
    $userContract=User::find()
    ->select(['remaining_freeze_days'])
    ->innerJoin(Contract::tableName(),Contract::tableName().".id=".User::tableName().".active_contract_id")
    ->where([User::tableName().'.id'=>$user_id])
    ->asArray()->one();
    if($userContract!=null){
      $remaining=$userContract['remaining_freeze_days'];
    }
    return $remaining;
  }

  public function getBoatFutureBookingCount($boat_id)
  {
    return Booking::find()->where(['and',['>=','booking_date',date("Y-m-d")],['boat_id'=>$boat_id,'status'=>1,'trashed'=>0]])->count('id');
  }

  //Get Total Active Morning/Noon bookings
  public function getUserAMPMBookingsCount($user_id)
  {
    return Booking::find()->where(['and',['<=','booking_date',date("Y-m-d")],['user_id'=>$user_id,'booking_time_slot'=>Yii::$app->helperFunctions->amPmTimeSlots,'status'=>Yii::$app->helperFunctions->bookingActiveStatus,'trashed'=>0]])->count('id');
  }
    /*
    *return total un consumed
    */
    public function getTotalUnConsumed()
    {
      $crQuery=UserAccounts::find()
    	->where(['and',['trans_type'=>'cr'],['!=','account_id',11]]);
    	$sumOfCr=$crQuery->sum('amount');

    	$drQuery=UserAccounts::find()
    	->where(['and',['trans_type'=>'dr'],['!=','account_id',11]]);
    	$sumOfDr=$drQuery->sum('amount');
    	return $sumOfCr-$sumOfDr;
    }

    /*
    *return total deposits
    */
    public function getTotalDeposits()
    {
      return UserAccounts::find()
        ->where(['account_id'=>0])
        ->sum("amount");
    }

    /*
    *return total Charges
    */
    public function getTotalCharges()
    {
      return UserAccounts::find()
        ->innerJoin("order",Order::tableName().".id=".UserAccounts::tableName().".order_id")
        ->where(['account_id'=>0])
        ->sum(Order::tableName().".amount_payable-".UserAccounts::tableName().".amount-".UserAccounts::tableName().".profit");
    }

    /*
    *return total Income
    */
    public function getTotalIncome()
    {
      return UserAccounts::find()
        ->where(['and',['account_id'=>0],['>','order_id',0]])
        ->sum("profit");
    }

    /*
    *return total Income
    */
    public function marinaFuel($port_id)
    {
      return UserAccounts::find()
        ->where(['account_id'=>1,'port_id'=>$port_id])
        ->innerJoin(Booking::tableName(),Booking::tableName().".id=".UserAccounts::tableName().".booking_id")
        ->sum("amount-profit");
    }

    /*
    *return total Fuel Profit
    */
    public function getTotalFuelProfit()
    {
      return UserAccounts::find()
        ->where(['and',['account_id'=>1],['>','booking_id',0]])
        ->sum("profit");
    }

    /*
    *return total Captain
    */
    public function getTotalCaptin()
    {
      return UserAccounts::find()
        ->where(['and',['account_id'=>2],['>','booking_id',0]])
        ->sum("amount");
    }

    /*
    *return total BBQ
    */
    public function getTotalBBQ()
    {
      return UserAccounts::find()
        ->where(['and',['account_id'=>3],['>','booking_id',0]])
        ->sum("amount");
    }

    /*
    *return total Other
    */
    public function getTotalOther()
    {
      return UserAccounts::find()
        ->where(['account_id'=>10])
        ->sum("amount");
    }

    /*
    *return total Paytabs
    */
    public function getTotalPaytabs()
    {
      $deposits=UserAccounts::find()
        ->where(['and',['account_id'=>0],['>','order_id',0]])
        ->sum("amount+profit");
      $withdrawals=UserAccounts::find()
        ->where(['account_id'=>11])
        ->sum("amount");
      return $deposits-$withdrawals;
    }

    public function getActiveCount()
    {
      return BoatRequired::find()->where(['and',['trashed'=>0],['>=','DATE(date)',date("Y-m-d")]])->count('id');
    }

    public function getHistoryCount()
    {
      return BoatRequired::find()->where(['and',['trashed'=>0],['<','DATE(date)',date("Y-m-d")]])->count('id');
    }

    public function getTrashedCount()
    {
      return BoatRequired::find()->where(['trashed'=>1])->count('id');
    }

    public function getActiveBulkBookingCount()
    {
      return BulkBooking::find()->where(['and',['trashed'=>0],['>=','DATE(end_date)',date("Y-m-d")]])->count('id');
    }

    public function getHistoryBulkBookingCount()
    {
      return BulkBooking::find()->where(['and',['trashed'=>0],['<','DATE(end_date)',date("Y-m-d")]])->count('id');
    }

    public function getDeletedBulkBookingCount()
    {
      return BulkBooking::find()->where(['trashed'=>1])->count('id');
    }

    public function getMarinaDateTimeFreeBoats($marina_id,$date,$time,$dayOfWeek)
    {
      $subQueryAvailable=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);
      $subQueryBulkBooked=BulkBooking::find()->select(['boat_id'])->where(['and',['trashed'=>0],['<=','start_date',$date],['>=','end_date',$date]]);
      $subQueryBoats=Boat::find()->select(['id'])->where(['and',['id'=>$subQueryAvailable,'port_id'=>$marina_id,'status'=>1,'trashed'=>0],['not in','id',$subQueryBulkBooked]]);
      $totalAvailable=BoatToTimeSlot::find()->where(['boat_id'=>$subQueryBoats,'time_slot_id'=>$time])->count(BoatToTimeSlot::tableName().'.id');
      $totalBooked=Booking::find()
      ->where([
        'port_id'=>$marina_id,
        'boat_id'=>$subQueryBoats,
        'booking_date'=>$date,
        'booking_time_slot'=>$time,
        'status'=>1,
        'trashed'=>0
      ])
      ->count(Booking::tableName().'.id');
      return ($totalAvailable-$totalBooked);
    }

    public function getMarinaDateTimeTotalBoats($marina_id,$date,$time,$dayOfWeek)
    {
      $subQueryAvailable=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);
      $subQueryBulkBooked=BulkBooking::find()->select(['boat_id'])->where(['and',['trashed'=>0],['<=','start_date',$date],['>=','end_date',$date]]);
      $subQueryBoats=Boat::find()->select(['id'])->where(['and',['id'=>$subQueryAvailable,'port_id'=>$marina_id,'status'=>1,'trashed'=>0],['not in','id',$subQueryBulkBooked]]);
      $totalAvailable=BoatToTimeSlot::find()->where(['boat_id'=>$subQueryBoats,'time_slot_id'=>$time])->count(BoatToTimeSlot::tableName().'.id');
      return $totalAvailable;
    }

    public function getMarinaTotalSessions($marina_id,$date)
    {
      $subQueryBoats=Boat::find()->select(['id'])->where(['port_id'=>$marina_id,'status'=>1,'trashed'=>0]);
      return BoatToTimeSlot::find()->where(['boat_id'=>$subQueryBoats])->count(BoatToTimeSlot::tableName().'.id');
    }

    public function getMarinaUnavailableSessions($marina_id,$date)
    {
      $dayOfWeek=Yii::$app->helperFunctions->WeekendDay($date);
      $totalBooked=Booking::find()
      ->where([
        'port_id'=>$marina_id,
        'booking_date'=>$date,
        'status'=>1,
        'trashed'=>0
      ])
      ->count(Booking::tableName().'.id');

      $subQueryAvailable=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);
      $subQueryUnAvailable=BoatAvailableDays::find()->select(['boat_id'])->where(['not in','boat_id',$subQueryAvailable]);
      $subQueryBoats=Boat::find()->select(['id'])->where(['id'=>$subQueryUnAvailable,'port_id'=>$marina_id,'status'=>1,'trashed'=>0]);
      $totalUnAvailable=BoatToTimeSlot::find()->where(['boat_id'=>$subQueryBoats])->count(BoatToTimeSlot::tableName().'.id');

      return ($totalUnAvailable+$totalBooked);
    }

    public function getMembersSubQuery()
    {
      return User::find()->select(['id'])->where(['user_type'=>0]);
    }

    /*
    * return Available session for booking tab
    */
    public function getMarinaAvailableCount($cityId,$marinaId,$date)
    {
      $availableCount=0;
      $timeSlots=Yii::$app->appHelperFunctions->bookingTimeSlotList;
      if($timeSlots!=null){
        list($sy,$sm,$sd)=explode("-",$date);
        $dayOfWeek=date("w",mktime(0,0,0,$sm,$sd,$sy));
        foreach($timeSlots as $timeSlot){
          $timeIdz=Yii::$app->appHelperFunctions->getChildTimeSlots($timeSlot['id']);
          $subQueryAvailbleDays=BoatAvailableDays::find()->select(['boat_id'])->where(['available_day'=>$dayOfWeek]);

          //Package Allowed
          $subQueryPackageAllowedPurposes=Yii::$app->user->identity->activePackageAllowedPurposes;

          $subQueryBulkBooked=BulkBooking::find()->select(['boat_id'])->where(['and',['trashed'=>0],['<=','start_date',$date],['>=','end_date',$date]]);
          $subQueryBookings=Booking::find()->select(['boat_id'])
          ->where([
            'city_id'=>$cityId,
            'port_id'=>$marinaId,
            'booking_date'=>$date,
            'booking_time_slot'=>$timeIdz,
            'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,
            'trashed'=>0,
          ]);


          $subQueryBookingSpecialBoatSelection=null;
          $subQueryNightDriveLimit=null;
          if($timeSlot['id']==Yii::$app->params['nightDriveTimeSlot']){
            $subQueryDateBookings=Booking::find()->select(['id'])
            ->where([
              'city_id'=>$cityId,
              'port_id'=>$marinaId,
              'booking_date'=>$date,
              'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,
              'trashed'=>0,
            ]);
            $subQueryBookingSpecialBoatSelection=BookingSpecialBoatSelection::find()->select(['boat_id'])
            ->where(['booking_id'=>$subQueryDateBookings]);

            //Check Limit & hide all
            $thisMarina=Marina::findOne($marinaId);
            $ndAllowedLimitInMarina=$thisMarina->night_drive_limit;
            $bookedCount=Booking::find()->where(['port_id'=>$marinaId,'booking_date'=>$date,'booking_time_slot'=>$timeIdz,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'is_bulk'=>0,'trashed'=>0])->count('id');
            if($bookedCount>=$ndAllowedLimitInMarina){
              $subQueryNightDriveLimit=Boat::find()->select(['id']);
            }
          }

          $thisTimeBoatsCount=BoatToTimeSlot::find()
          ->select([
            BoatToTimeSlot::tableName().'.time_slot_id',
            'boat_id'=>Boat::tableName().'.id',
            Boat::tableName().'.name',
            TimeSlot::tableName().'.dashboard_name',
            TimeSlot::tableName().'.start_time',
            TimeSlot::tableName().'.end_time',
          ])
          ->innerJoin(Boat::tableName(),Boat::tableName().".id=".BoatToTimeSlot::tableName().".boat_id")
          ->innerJoin(TimeSlot::tableName(),TimeSlot::tableName().".id=".BoatToTimeSlot::tableName().".time_slot_id")
          ->where(['city_id'=>$cityId,'port_id'=>$marinaId,'time_slot_id'=>$timeIdz,Boat::tableName().'.status'=>1,Boat::tableName().'.trashed'=>0])
          ->andWhere([Boat::tableName().'.id'=>$subQueryAvailbleDays])
          ->andWhere(['or',[Boat::tableName().'.boat_purpose_id'=>$subQueryPackageAllowedPurposes],['special_boat'=>[1,2]]])
          ->andWhere(['not in',Boat::tableName().'.id',$subQueryBulkBooked])
          ->andWhere(['not in',Boat::tableName().'.id',$subQueryBookings])
          ->andFilterWhere(['not in',Boat::tableName().'.id',$subQueryBookingSpecialBoatSelection])
          ->andFilterWhere(['not in',Boat::tableName().'.id',$subQueryNightDriveLimit])
          ->count(BoatToTimeSlot::tableName().'.time_slot_id');

          $availableCount+=$thisTimeBoatsCount;
        }
      }
      return $availableCount;
    }

    public function getMarinaTotalBookings($marinaId,$date)
    {
      $startDate=null;
      $endDate=null;
      if($date!=null){
        list($startDate,$endDate)=explode(" - ",$date);
      }
      return Booking::find()
      ->where(['port_id'=>$marinaId,'user_id'=>$this->membersSubQuery,'trashed'=>0])
      ->andWhere(['<=','booking_date',date("Y-m-d")])
      ->andFilterWhere(['and',['>=','booking_date',$startDate],['<=','booking_date',$endDate]])
      ->count('id');
    }

    public function getMarinaTotalBookingsByBookingExp($marinaId,$exp,$date)
    {
      $startDate=null;
      $endDate=null;
      if($date!=null){
        list($startDate,$endDate)=explode(" - ",$date);
      }
      return Booking::find()
      ->where(['port_id'=>$marinaId,'user_id'=>$this->membersSubQuery,'booking_exp'=>$exp,'trashed'=>0])
      ->andWhere(['<=','booking_date',date("Y-m-d")])
      ->andFilterWhere(['and',['>=','booking_date',$startDate],['<=','booking_date',$endDate]])
      ->count('id');
    }

    public function getMarinaUnClosedBookings($marinaId,$date)
    {
      $startDate=null;
      $endDate=null;
      if($date!=null){
        list($startDate,$endDate)=explode(" - ",$date);
      }
      $subQueryNonClosed=BookingActivity::find()->select(['booking_id'])->where(['hide_other'=>0]);

      $subQueryClosed=BookingActivity::find()->select(['booking_id'])->where(['hide_other'=>1]);
      $subQueryNotUpdated=Booking::find()->select(['id'])->where(['status'=>1])->andWhere(['not in','id',$subQueryClosed]);

      return Booking::find()
      ->where(['port_id'=>$marinaId,'user_id'=>$this->membersSubQuery,'status'=>1,'trashed'=>0])
      ->andWhere(['<=','booking_date',date("Y-m-d")])
      ->andWhere([
        'or',
        [Booking::tableName().'.id'=>$subQueryNonClosed],
        [Booking::tableName().'.id'=>$subQueryNotUpdated],
      ])
      ->andFilterWhere(['and',['>=','booking_date',$startDate],['<=','booking_date',$endDate]])
      ->count('id');
    }

    public function getMarinaBookingTripStats($marinaId,$exp,$date)
    {
      $startDate=null;
      $endDate=null;
      if($date!=null){
        list($startDate,$endDate)=explode(" - ",$date);
      }
      return Booking::find()
      ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
      ->where(['port_id'=>$marinaId,'user_id'=>$this->membersSubQuery,'trip_exp'=>$exp,'status'=>1,'hide_other'=>1,'trashed'=>0])
      ->andWhere(['<=','booking_date',date("Y-m-d")])
      ->andFilterWhere(['and',['>=','booking_date',$startDate],['<=','booking_date',$endDate]])
      ->count(Booking::tableName().'.id');
    }

    public function getMarinaBookingStatusStats($marinaId,$status,$date)
    {
      $startDate=null;
      $endDate=null;
      if($date!=null){
        list($startDate,$endDate)=explode(" - ",$date);
      }
      return Booking::find()
      ->where(['port_id'=>$marinaId,'user_id'=>$this->membersSubQuery,'status'=>$status,'trashed'=>0])
      ->andWhere(['<=','booking_date',date("Y-m-d")])
      ->andFilterWhere(['and',['>=','booking_date',$startDate],['<=','booking_date',$endDate]])
      ->count(Booking::tableName().'.id');
    }

    public function memberHistoryTotal($singleModel,$searchModel)
    {
      if($searchModel->status_type=='be'){
       return Booking::find()
       ->where([
         'and',
         ['user_id'=>$singleModel->user_id,'status'=>[1,2],'booking_exp'=>$searchModel->status,'trashed'=>0],
         ['<=','booking_date',date("Y-m-d")]
       ])
       ->count(Booking::tableName().'.id');
      }
      if($searchModel->status_type=='te'){
       return Booking::find()
       ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
       ->where([
         'and',
         ['user_id'=>$singleModel->user_id,'status'=>[1,2],'trip_exp'=>$searchModel->status,'trashed'=>0],
         ['<=','booking_date',date("Y-m-d")]
       ])
       ->count(Booking::tableName().'.id');
      }
      if($searchModel->status_type=='bs'){
       return Booking::find()
       ->where([
         'and',
         ['user_id'=>$singleModel->user_id,'status'=>$searchModel->status,'trashed'=>0],
         ['<=','booking_date',date("Y-m-d")]
       ])
       ->count(Booking::tableName().'.id');
      }
      if($searchModel->status_type=='mb'){
        $col='member_exp';
        if($searchModel->status==2)$col='dui';
       return Booking::find()
       ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
       ->where([
         'and',
         ['user_id'=>$singleModel->user_id,'status'=>[1,2],$col=>1,'trashed'=>0],
         ['<=','booking_date',date("Y-m-d")]
       ])
       ->count(Booking::tableName().'.id');
      }
    }
}
