<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use app\models\WaterSportEquipment;

class JavaScriptFunctions extends Component
{
  public function getCityMarinaArr($cities)
  {
    $txtJScript="var cities=new Array();\n";
    if($cities!=null){
    	foreach($cities as $cityId=>$cityName){
    		$jsCityMarinasArr=[];
    		$cityMarinas=Yii::$app->appHelperFunctions->getCityMarinaListArr($cityId);
    		if($cityMarinas!=null){
    			foreach($cityMarinas as $marinaId=>$marinaName){
    				$jsCityMarinasArr[]=['display'=>$marinaName,'value'=>$marinaId,'sel'=>''];
    			}
    		}
    		$txtJScript.="cities[".$cityId."]=".json_encode($jsCityMarinasArr).";\n";
    	}
    }
    return $txtJScript;
  }

  public function getCityMarinaNameArr($cities)
  {
    $txtJScript="var cities=new Array();\n";
    if($cities!=null){
    	foreach($cities as $cityId=>$cityName){
    		$jsCityMarinasArr=[];
    		$cityMarinas=Yii::$app->appHelperFunctions->getCityMarinaListArr($cityId);
    		if($cityMarinas!=null){
    			foreach($cityMarinas as $marinaId=>$marinaName){
    				$jsCityMarinasArr[]=['display'=>$marinaName,'value'=>$marinaName,'sel'=>''];
    			}
    			$jsCityMarinasArr[]=['display'=>'Any Marina','value'=>'Any Marina','sel'=>''];
    		}
    		$txtJScript.="cities[".$cityId."]=".json_encode($jsCityMarinasArr).";\n";
    	}
    }
    return $txtJScript;
  }

  public function getMarinaEquipments()
  {
    $txtJScript='var equips_ = []';
    $cities=Yii::$app->appHelperFunctions->cityList;
    if($cities!=null){
    	foreach($cities as $city){
    		$marinas=Yii::$app->appHelperFunctions->getCityMarinaList($city['id']);
    		if($marinas!=null){
    			foreach($marinas as $marina){
    				$equipments=WaterSportEquipment::find()->where(['city_id'=>$city['id'],'port_id'=>$marina['id'],'status'=>1,'trashed'=>0])->asArray()->all();
    				$txtJScript.='
    				var equips_'.$city['id'].'_'.$marina['id'].' = {
    				';
    				if($equipments!=null){
    					$n=1;
    					foreach($equipments as $equipment){
    						if($n>1)$txtJScript.=",";
    						$txtJScript.='"'.$equipment['id'].'": "'.$equipment['title'].'"';
    						$n++;
    					}
    				}
    				$txtJScript.='}';
    			}
    		}
    	}
    }
    return $txtJScript;
  }
}
