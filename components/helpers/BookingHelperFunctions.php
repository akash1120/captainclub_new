<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use app\models\BulkBooking;
use app\models\BoatRequired;
use app\models\BoatRequiredBooking;
use app\models\Booking;
use app\models\BookingActivity;
use app\models\BookingActivityAddons;
use app\models\ContractMember;
use app\models\Contract;

class BookingHelperFunctions extends Component
{
  /*
  *check & book free boats
  */
  public function checkBulkOrBoatRequired($city_id,$port_id,$boat_id,$date,$time_slot,$ignore_bulk_booking_check)
  {
    $connection = \Yii::$app->db;
    //Check if boat is bulk booked.
    $checkIfBulkBooked = BulkBooking::find()->where(['and',['boat_id'=>$boat_id,'trashed'=>0],['<=','start_date',$date],['>=','end_date',$date]])->one();
		if($checkIfBulkBooked!=null && $ignore_bulk_booking_check=='no'){
			$booking=new Booking;
			$booking->booking_source='bulk';
			$booking->user_id=$checkIfBulkBooked->created_by;
			$booking->city_id=$city_id;
			$booking->port_id=$port_id;
			$booking->boat_id=$boat_id;
			$booking->booking_date=$date;
			$booking->booking_time_slot=$time_slot;
			$booking->booking_type=$checkIfBulkBooked->booking_type;
			$booking->booking_comments=$checkIfBulkBooked->booking_comments;
			$booking->is_bulk=1;
			$booking->save();
			$connection->createCommand("update ".Booking::tableName()." set created_by='".$checkIfBulkBooked->created_by."',updated_by='".$checkIfBulkBooked->created_by."' where id='".$booking->id."'")->execute();
		}else{
			//Check If boat is required
			$areBoatsRequired=BoatRequired::find()->where(['and','boats_booked<no_of_boats',['city_id'=>$city_id,'marina_id'=>$port_id,'date'=>$date,'time_id'=>$time_slot,'trashed'=>0]])->all();
			if($areBoatsRequired!=null){
        foreach($areBoatsRequired as $isboatRequired){
          $isboatRequired->searchAndBookBoats();
        }
			}
		}
  }

  public function getCaptainStatus($member,$booking)
  {
    $status='free';
    if($member!=null && $member->user_type==0){
      $subQueryContractMember=ContractMember::find()->select(['contract_id'])->where(['user_id'=>$member->id,'status'=>1]);
      $activeContract=Contract::find()->where(['and',['<=','start_date',$booking->booking_date],['>=','end_date',$booking->booking_date],['id'=>$subQueryContractMember]])->one();
      //$activeContract=$member->activeContract;
      $captainsAllowed=0;
      if($activeContract!=null){
        $captainsAllowed=$activeContract->allowed_captains;
      }
      $startDate=$activeContract->start_date;
      $endDate=$activeContract->end_date;

      $freeUsed=Booking::find()
      ->select('id')
      ->where([
        'and',
        ['>=','booking_date',$startDate],
        ['<=','booking_date',$endDate],
        ['in','user_id',$member->memberIdz],
        ['captain'=>1,'status'=>1,'trashed'=>0],
        ['<','id',$booking->id],
        [
          'or',
          ['captain_type'=>'free'],
          ['captain_type'=>NULL],
        ]
      ])
      ->count();
      if($freeUsed>=$captainsAllowed){
        $status='paid';
      }
    }

    return $status;
  }

  public function getBookingExperienceStat($user_id,$marina_id,$startDate,$endDate,$type)
  {
    return Booking::find()
      ->where([
        'and',
        [
          Booking::tableName().'.booking_exp'=>$type,
          //Booking::tableName().'.status'=>Yii::$app->operationHelperFunctions->bookingTripStatus,
          Booking::tableName().'.port_id'=>$marina_id,
          Booking::tableName().'.trashed'=>0,
        ],
        ['>=',Booking::tableName().'.booking_date',$startDate],
        ['<=',Booking::tableName().'.booking_date',$endDate],
      ])
      ->andFilterWhere([Booking::tableName().'.user_id'=>$user_id])
      ->count(Booking::tableName().'.id');
  }

  public function getTripExperienceStat($user_id,$marina_id,$startDate,$endDate,$type,$col='te')
  {
    if($col=='bs'){
      return Booking::find()
        ->where([
          'and',
          [
            Booking::tableName().'.booking_exp'=>[1,2,3],
            Booking::tableName().'.status'=>$type,
            Booking::tableName().'.port_id'=>$marina_id,
            Booking::tableName().'.trashed'=>0,
          ],
          ['>=',Booking::tableName().'.booking_date',$startDate],
          ['<=',Booking::tableName().'.booking_date',$endDate],
        ])
        ->andFilterWhere([Booking::tableName().'.user_id'=>$user_id])
        ->leftJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
        ->count(Booking::tableName().'.id');
    }else{
      return Booking::find()
        ->where([
          'and',
          [
            Booking::tableName().'.booking_exp'=>[1,2,3],
            BookingActivity::tableName().'.trip_exp'=>$type,
            //Booking::tableName().'.status'=>1,
            Booking::tableName().'.port_id'=>$marina_id,
            Booking::tableName().'.trashed'=>0,
          ],
          ['>=',Booking::tableName().'.booking_date',$startDate],
          ['<=',Booking::tableName().'.booking_date',$endDate],
        ])
        ->andFilterWhere([Booking::tableName().'.user_id'=>$user_id])
        ->leftJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
        ->count(Booking::tableName().'.id');
    }
  }

  public function getBookingBehaviorStat($user_id,$marina_id,$startDate,$endDate,$type)
  {
    return Booking::find()
      ->where([
        'and',
        [
          BookingActivity::tableName().'.member_exp'=>$type,
          Booking::tableName().'.port_id'=>$marina_id,
          Booking::tableName().'.trashed'=>0,
        ],
        ['>=',Booking::tableName().'.booking_date',$startDate],
        ['<=',Booking::tableName().'.booking_date',$endDate],
      ])
      ->andFilterWhere([Booking::tableName().'.user_id'=>$user_id])
      ->innerJoin(BookingActivity::tableName(),BookingActivity::tableName().".booking_id=".Booking::tableName().".id")
      ->count(Booking::tableName().'.id');
  }

  public function getBookingStatusStat($user_id,$marina_id,$startDate,$endDate,$type)
  {
    return Booking::find()
      ->where([
        'and',
        [
          Booking::tableName().'.status'=>$type,
          Booking::tableName().'.port_id'=>$marina_id,
          Booking::tableName().'.trashed'=>0,
        ],
        ['>=',Booking::tableName().'.booking_date',$startDate],
        ['<=',Booking::tableName().'.booking_date',$endDate],
      ])
      ->andFilterWhere([Booking::tableName().'.user_id'=>$user_id])
      ->count(Booking::tableName().'.id');
  }

  public function getBookingExtraStat($user_id,$marina_id,$startDate,$endDate,$type)
  {
    $subQueryAdons=BookingActivityAddons::find()->select(['booking_id'])->where(['addon_id'=>$type]);
    return Booking::find()
      ->where([
        'and',
        [
          Booking::tableName().'.id'=>$subQueryAdons,
          Booking::tableName().'.status'=>1,
          Booking::tableName().'.port_id'=>$marina_id,
          Booking::tableName().'.trashed'=>0,
        ],
        ['>=',Booking::tableName().'.booking_date',$startDate],
        ['<=',Booking::tableName().'.booking_date',$endDate],
      ])
      ->andFilterWhere([Booking::tableName().'.user_id'=>$user_id])
      ->count(Booking::tableName().'.id');
  }

  public function getCaptainLimitReachedReason()
  {
    return 'Marina Captains Limit reached';
  }

  public function getEquipmentLimitReachedReason()
  {
    return 'Marina Equipment Limit Reached';
  }

  public function getBbqSetLimitReachedReason()
  {
    return 'Marina BBQ Set Limit Reached';
  }

  public function getWakeBoardingLimitReachedReason()
  {
    return 'Marina Wake Boarding Limit Reached';
  }

  public function getWakeSurfingLimitReachedReason()
  {
    return 'Marina Wake Surfing Limit Reached';
  }

  public function getEarlyDepartureLimitReachedReason()
  {
    return 'Marina Early Departure Limit reached';
  }

  public function getEarlyDeparturePreviousDayOvernightCampReason()
  {
    return 'Boat is booked for overnight camping day before';
  }

  public function getOvernightCampingLimitReachedReason()
  {
    return 'Marina Overnight Camping Limit reached';
  }

  public function getOvernightCampingBoatIsOneOfNightDrivesReason()
  {
    return 'Boat is one of night drive session';
  }

  public function getOvernightCampingBoatIsEarlyDepartureNextDayReason()
  {
    return 'Boat is booked for early departure next day';
  }

  public function getLateArrivalLimitReachedReason()
  {
    return 'Marina Late arrival Limit reached';
  }
}
