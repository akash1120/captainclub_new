<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\ActivityLog;
use app\models\Booking;
use app\models\CaptainRequest;
use app\models\WaterSportEquipment;

use app\models\WaitingListCaptain;
use app\models\WaitingListSportsEquipment;
use app\models\WaitingListBbq;
use app\models\WaitingListWakeBoarding;
use app\models\WaitingListWakeSurfing;
use app\models\WaitingListLateArrival;

class WaitingListHelper extends Component
{

  //Check if there is any captain waiting list
  public function checkAndAssignCaptain($city_id,$marina_id,$date,$time_id)
  {
    $waitingList=WaitingListCaptain::find()->where(['city_id'=>$city_id,'marina_id'=>$marina_id,'date'=>$date,'time_id'=>$time_id,'status'=>0])->asArray()->orderBy(['created_at'=>SORT_ASC])->one();
    if($waitingList!=null){
      $nextBooking=Booking::find()->where(['id'=>$waitingList['booking_id']])->one();
      if($nextBooking!=null){
        $totalCaptainsInMarina=$nextBooking->marina->captains;
        $usedOnDate=Booking::find()->where(['booking_date'=>$nextBooking->booking_date,'port_id'=>$nextBooking->port_id,'booking_time_slot'=>$nextBooking->booking_time_slot,'captain'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalCaptainsInMarina){
          $connection = \Yii::$app->db;
          $captainType=Yii::$app->bookingHelperFunctions->getCaptainStatus($nextBooking->member,$nextBooking);
          if($captainType=='paid'){
            $request=CaptainRequest::find()->where(['booking_id'=>$nextBooking->id])->one();
            if($request==null){
              $request=new CaptainRequest;
              $request->user_id=$nextBooking->user_id;
              $request->booking_id=$nextBooking->id;
              $request->status=0;
              if($request->save()){
                $connection->createCommand("update ".Booking::tableName()." set captain=1 where id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
                ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Captain Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
                $nextBooking->metRequest('captain');
              }
            }
          }else{
            $connection->createCommand("update ".Booking::tableName()." set captain=1 where id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
            ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Captain Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
            $nextBooking->metRequest('captain');
          }
          $connection->createCommand("update ".WaitingListCaptain::tableName()." set status=1 where booking_id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
        }
      }
    }
  }

  //Check if there is any sports equipments waiting list
  public function checkAndAssignSportsEquipment($city_id,$marina_id,$date,$time_id)
  {
    $waitingList=WaitingListSportsEquipment::find()->where(['city_id'=>$city_id,'marina_id'=>$marina_id,'date'=>$date,'time_id'=>$time_id,'status'=>0])->asArray()->orderBy(['created_at'=>SORT_ASC])->one();
    if($waitingList!=null){
      $nextBooking=Booking::find()->where(['id'=>$waitingList['booking_id']])->one();
      if($nextBooking!=null){

        $equipmentRow=WaterSportEquipment::find()->where(['id'=>$waitingList['equipment_id'],'city_id'=>$nextBooking->city_id,'port_id'=>$nextBooking->port_id])->one();
        if($equipmentRow!=null){

          $totalEquipmentsInMarina=$equipmentRow->qty;
          $usedOnDate=Booking::find()->where(['booking_date'=>$nextBooking->booking_date,'port_id'=>$nextBooking->port_id,'booking_time_slot'=>$nextBooking->booking_time_slot,'sport_eqp_id'=>$equipmentRow->id,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
          if($usedOnDate<$totalEquipmentsInMarina){
            $connection = \Yii::$app->db;
            $connection->createCommand("update ".Booking::tableName()." set sport_eqp_id=:eqp_id where id=:booking_id",[':eqp_id'=>$equipmentRow->id,':booking_id'=>$nextBooking->id])->execute();
            ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Sports Equipment ('.$equipmentRow->title.') Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
            $msg['success']=['heading'=>Yii::t('app','Dear Captain'),'msg'=>Yii::t('app','Your request is confirmed')];
            $nextBooking->metRequest('equipment',$waitingList['equipment_id']);
            $connection->createCommand("update ".WaitingListSportsEquipment::tableName()." set status=1 where booking_id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          }
        }
      }
    }
  }

  //Check if there is any BBQ Set waiting list
  public function checkAndAssignBBQSet($city_id,$marina_id,$date,$time_id)
  {
    $waitingList=WaitingListBbq::find()->where(['city_id'=>$city_id,'marina_id'=>$marina_id,'date'=>$date,'time_id'=>$time_id,'status'=>0])->asArray()->orderBy(['created_at'=>SORT_ASC])->one();
    if($waitingList!=null){
      $nextBooking=Booking::find()->where(['id'=>$waitingList['booking_id']])->one();
      if($nextBooking!=null){
        $totalBBQInMarina=$nextBooking->marina->bbq;
        $usedOnDate=Booking::find()->where(['booking_date'=>$nextBooking->booking_date,'port_id'=>$nextBooking->port_id,'booking_time_slot'=>$nextBooking->booking_time_slot,'bbq'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalBBQInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set bbq=1 where id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'BBQ Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
          $connection->createCommand("update ".WaitingListBbq::tableName()." set status=1 where booking_id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          $nextBooking->metRequest('bbq');
        }
      }
    }
  }

  //Check if there is any Wake Boarding waiting list
  public function checkAndAssignWakeBoarding($city_id,$marina_id,$date,$time_id)
  {
    $waitingList=WaitingListWakeBoarding::find()->where(['city_id'=>$city_id,'marina_id'=>$marina_id,'date'=>$date,'time_id'=>$time_id,'status'=>0])->asArray()->orderBy(['created_at'=>SORT_ASC])->one();
    if($waitingList!=null){
      $nextBooking=Booking::find()->where(['id'=>$waitingList['booking_id']])->one();
      if($nextBooking!=null){
        $totalWBInMarina=$nextBooking->marina->wake_boarding_limit;
        $usedOnDate=Booking::find()->where(['booking_date'=>$nextBooking->booking_date,'port_id'=>$nextBooking->port_id,'booking_time_slot'=>$nextBooking->booking_time_slot,'wake_boarding'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalWBInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set wake_boarding=1 where id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Wake Boarding Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
          $connection->createCommand("update ".WaitingListWakeBoarding::tableName()." set status=1 where booking_id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          $nextBooking->metRequest('wakeboarding');
        }
      }
    }
  }

  //Check if there is any Wake Surfing waiting list
  public function checkAndAssignWakeSurfing($city_id,$marina_id,$date,$time_id)
  {
    $waitingList=WaitingListWakeSurfing::find()->where(['city_id'=>$city_id,'marina_id'=>$marina_id,'date'=>$date,'time_id'=>$time_id,'status'=>0])->asArray()->orderBy(['created_at'=>SORT_ASC])->one();
    if($waitingList!=null){
      $nextBooking=Booking::find()->where(['id'=>$waitingList['booking_id']])->one();
      if($nextBooking!=null){
        $totalWSInMarina=$nextBooking->marina->wake_surfing_limit;
        $usedOnDate=Booking::find()->where(['booking_date'=>$nextBooking->booking_date,'port_id'=>$nextBooking->port_id,'booking_time_slot'=>$nextBooking->booking_time_slot,'wake_surfing'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalWSInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set wake_surfing=1 where id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Wake Surfing Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
          $connection->createCommand("update ".WaitingListWakeSurfing::tableName()." set status=1 where booking_id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          $nextBooking->metRequest('wakesurfing');
        }
      }
    }
  }

  //Check if there is any Wake Surfing waiting list
  public function checkAndAssignLateArrival($city_id,$marina_id,$date,$time_id)
  {
    $waitingList=WaitingListLateArrival::find()->where(['city_id'=>$city_id,'marina_id'=>$marina_id,'date'=>$date,'time_id'=>$time_id,'status'=>0])->asArray()->orderBy(['created_at'=>SORT_ASC])->one();
    if($waitingList!=null){
      $nextBooking=Booking::find()->where(['id'=>$waitingList['booking_id']])->one();
      if($nextBooking!=null){
        $totalLateArrivalsAllowedInMarina=$nextBooking->marina->no_of_late_arrivals;
        $usedOnDate=Booking::find()->where(['booking_date'=>$nextBooking->booking_date,'port_id'=>$nextBooking->port_id,'booking_time_slot'=>$nextBooking->booking_time_slot,'late_arrival'=>1,'status'=>Yii::$app->helperFunctions->bookingSysActiveStatus,'trashed'=>0])->count();
        if($usedOnDate<$totalLateArrivalsAllowedInMarina){
          $connection = \Yii::$app->db;
          $connection->createCommand("update ".Booking::tableName()." set late_arrival=1 where id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, 'Late Arrival Confirmed ('.$nextBooking->booking_date.' / '.$nextBooking->boat->name.' from '.$nextBooking->marina->name.' {Ref:'.$nextBooking->id.'}) by (Waiting List Action) successfully');
          $connection->createCommand("update ".WaitingListLateArrival::tableName()." set status=1 where booking_id=:booking_id",[':booking_id'=>$nextBooking->id])->execute();
          $nextBooking->metRequest('late_arrival');
        }
      }
    }
  }

  public function getWaitlistSuccessHeading()
  {
    return Yii::t('app','Added to waiting list');
  }

  public function getWaitlistSuccessMessage($type)
  {
    $addonsText='';
    if($type=='captain')$addonsText='a captain';
    if(strtolower($type)=='donuts')$addonsText='a donut';
    if($type=='bbqset')$addonsText='a BBQ set';
    if($type=='earlydeparture')$addonsText='early departure';
    if($type=='overnightcamp')$addonsText='overnight camping';
    if($type=='latearrival')$addonsText='late arrival';
    if($type=='wakesurfing')$addonsText='wake surfing';
    if($type=='wakeboarding')$addonsText='wake boarding';
    return Yii::t('app','System will automatically assign '.$addonsText.' once it becomes available.');
    //return Yii::t('app','Thankyou for requesting an extra  in house captain. If your request becomes available the system will automatically assign a captain for your trip.');
  }
}
