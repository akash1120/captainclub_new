<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MemberActivity;

class ActivityHelper extends Component
{
  public function log($type,$model,$parent_id=0)
  {
    $icon='';
    $descp='';
    $link='javascript:;';
    if($type=='sent-inquiry'){
      $icon='fa-envelope';
      $descp='Sent Inquiry to '.$model->mediaOutlet->company->title;
      $link=Url::to(['inquiries/view','id'=>$model->rssid]);
    }
    if($type=='replied-inquiry'){
      $icon='fa-envelope';
      $descp='Replied to Inquiry';
      $link=Url::to(['inquiries/view','id'=>$model->inquiry->rssid]);
    }
    $activity=new MemberActivity;
    $activity->user_id=Yii::$app->user->identity->id;
    $activity->type=$type;
    $activity->icon=$icon;
    $activity->descp=$descp;
    $activity->parent_id=$parent_id;
    $activity->ref_id=$model->id;
    $activity->link=$link;
    $activity->save();
  }
}
?>
