<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Contract;
use app\models\User;
use app\models\UserProfileInfo;
use app\models\City;
use app\models\UserDevice;
use yii\web\UploadedFile;

/**
 * Default controller
 */
class DefController extends Controller
{
  public function beforeAction($action)
  {
      $session = Yii::$app->session;
    if(isset($_GET['onesignal_push_id']) && $_GET['onesignal_push_id']!=''){
      $session->set('onesignal_push_id', $_GET['onesignal_push_id']);
    }

    if($session->get('onesignal_push_id')!=null && $session->get('onesignal_push_id')!=''){
        $device_id=$session->get('onesignal_push_id');
        if(!\Yii::$app->user->isGuest){
            $userDevice=UserDevice::find()->where(['device_id'=>$device_id])->one();
            if($userDevice==null){
                $userDevice=new UserDevice;
                $userDevice->device_id=$device_id;
            }
            $userDevice->user_id=Yii::$app->user->identity->id;
            $userDevice->save();
        }else{
            $getUserByDevice=UserDevice::find()->select(['user_id'])->where(['device_id'=>$device_id])->asArray()->one();
            if($getUserByDevice!=null){
                $user=User::findOne(['id'=>$getUserByDevice['user_id'],'trashed'=>'0','status'=>1]);
                if($user!=null){
                    Yii::$app->user->login($user, 3600*24*30);
                }
            }
        }
    }
    if($action->id == "drop-upload" || $action->id=="drop-upload-all" || ($this->id=='order' && $action->id=='response')){
      $this->enableCsrfValidation = false;
    }
    return parent::beforeAction($action);
  }

  /**
   * @inheritdoc
   */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'app\components\helpers\MyErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex()
  {
    return $this->render('index');
  }

	public function goHome()
	{
		return $this->redirect(['index']);
	}

	public function checkLogin()
	{
		if (\Yii::$app->user->isGuest) {
      header("location:".Url::to(['site/login']));
      die();
      exit;
    }
	}

	public function checkAdmin()
	{
		if (\Yii::$app->user->isGuest) {
      return $this->redirect(['site/login']);
    }
		if(Yii::$app->user->identity->user_type==0){
      return $this->redirect(['site/index']);
		}
	}

	public function checkSuperAdmin()
	{
		if (\Yii::$app->user->isGuest) {
      return $this->redirect(['site/login']);
    }
		if(Yii::$app->user->identity->user_type==0){
      return $this->redirect(['site/index']);
		}
	}

  /**
  * Creates a new model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkAdmin();
    $model = $this->newModel();
    $model->status=1;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Displays a single model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    $this->checkLogin();
      return $this->render('view', [
          'model' => $this->findModel($id),
      ]);
  }

  /**
   * Updates an existing model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
      }
    }

    return $this->render('update', [
        'model' => $model,
    ]);
  }

  /**
  * Enable/Disable an existing model.
  * If action is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionStatus($id)
  {
    $this->checkAdmin();
    $model = $this->findModel($id);
    $model->updateStatus();
    Yii::$app->getSession()->addFlash('success', Yii::t('app','Record updated successfully'));
    return $this->redirect(['index']);
  }

  /**
   * Deletes an existing model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDelete($id)
  {
    $this->checkLogin();
      $this->findModel($id)->softDelete();
      Yii::$app->getSession()->addFlash('success', Yii::t('app','Record trashed successfully'));
      return $this->redirect(['index']);
  }

  /**
   * Deletes an existing model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDeleteLogo($id)
  {
    $this->checkLogin();
    $this->findModel($id)->deleteLogo();
    echo 'success';
    exit;
  }

  /**
   * Loads Cities for a country and zone.
   *
   * @return string
   */
  /*public function actionCityOptions($country_id,$zone_id=null)
  {
    $html = '';
    $results=City::find()
			->select([
				'id',
				'title',
			])
			->where(['status'=>1,'trashed'=>0])
      ->andFilterWhere(['country_id'=>$country_id,'zone_id'=>$zone_id,])
      ->orderBy(['title'=>SORT_ASC])
			->asArray()
			->all();
    if($results!=null){
      $html.='<option value="">'.Yii::t('app','Select City').'</option>';
      foreach($results as $result){
        $html.='<option value="'.$result['id'].'">'.$result['title'].'</option>';
      }
    }
    echo $html;
    exit;
  }*/

  public function actionDropUpload($type=null)
  {
    $allowedFileTypes=['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF'];
    $fileName = 'file';
    $uploadPath = Yii::$app->params['temp_abs_path'];

    if (isset($_FILES[$fileName])) {
      $file = UploadedFile::getInstanceByName($fileName);
      if (!empty($file)) {
        $pInfo=pathinfo($file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$allowedFileTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
          }else{
            if (getimagesize($file->tempName)) {
              // generate a unique file name
              if ($file->saveAs(Yii::$app->params['temp_abs_path'].$file->name)) {
                echo $file->name;
              }
            }else{
              if ($file->saveAs(Yii::$app->params['temp_abs_path'].$file->name)) {
                echo $file->name;
              }
            }
          }
        }
      }
    }
    exit;
  }

  public function actionDropUploadAll($type=null)
  {
    $allowedFileTypes=['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF','doc','docx','DOC','DOCX','xls','xlsx','XLS','XLSX','pdf','PDF'];
    $fileName = 'file';
    $uploadPath = Yii::$app->params['temp_abs_path'];

    if (isset($_FILES[$fileName])) {
      $file = UploadedFile::getInstanceByName($fileName);
      if (!empty($file)) {
        $pInfo=pathinfo($file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$allowedFileTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
          }else{
            if (getimagesize($file->tempName)) {
              // generate a unique file name
              if ($file->saveAs(Yii::$app->params['temp_abs_path'].$file->name)) {
                echo $file->name;
              }
            }else{
              if ($file->saveAs(Yii::$app->params['temp_abs_path'].$file->name)) {
                echo $file->name;
              }
            }
          }
        }
      }
    }
    exit;
  }

  /**
   * Displays Not allowed page.
   *
   * @return string
   */
  public function actionNotAllowed()
  {
    return $this->render('/site/not_allowed');
  }

  /**
   * Flushes the cache
   */
  public function actionFlushCache()
  {
    if(!Yii::$app->user->isGuest){
      Yii::$app->cache->flush();
      echo 'Done';
      exit;
    }
  }

  /**
   * Loads Staff Members
   * @return array
   */
  /*public function actionStaffSearch($query=null)
  {
    header('Content-type: application/json');
    $output_arrays=[];
    $results=User::find()
      ->select([
        User::tableName().'.id',
        User::tableName().'.username',
        User::tableName().'.image',
        User::tableName().'.fullname',
        User::tableName().'.family_name',
        UserProfileInfo::tableName().'.email',
        UserProfileInfo::tableName().'.mobile',
      ])
      ->innerJoin("user_profile_info",UserProfileInfo::tableName().".user_id=".User::tableName().".id")
      ->where([
        'and',
        ['status'=>1,'verified'=>1,'trashed'=>0,'active_company_id'=>Yii::$app->user->identity->active_company_id],
      	[
      		'or',
      		['like',User::tableName().'.username',$query],
      		['like',User::tableName().'.fullname',$query],
      		['like',User::tableName().'.family_name',$query],
      		['like',UserProfileInfo::tableName().'.email',$query],
      	],
      ])
      ->asArray()->limit(20)->all();
    $adminz=Yii::$app->helperFunction->getCompanyAccountAdminIdzArray(Yii::$app->user->identity->active_company_id);
    if($results!=null){
      foreach($results as $result){

        $photo = Yii::$app->helperFunction->getImagePath('user',$result['image'],'tiny');
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>$result['fullname'].($result['family_name']!='' ? ' '.$result['family_name'] : '').' ('.$result['username'].')',
          'name'=>$result['fullname'].($result['family_name']!='' ? ' '.$result['family_name'] : ''),
          'username'=>$result['username'],
          'email'=>$result['email'],
          'photo'=>$photo,
          'is_admin'=>in_array($result['id'],$adminz) ? 1 : 0,
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }*/

  /**
   * search all the members
   * @return mixed
   */
  /*public function actionMemberSearch($query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    $results=User::find()
      ->select([User::tableName().'.id','username'])
      ->where([
        'and',
        ['like','username',$query],
        [User::tableName().'.status'=>1],
        ['>=',Contract::tableName().'.end_date',date("Y-m-d")]
      ])
			->leftJoin("contract",Contract::tableName().".id=".User::tableName().".active_contract_id")
      ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['username']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
  }*/
}
