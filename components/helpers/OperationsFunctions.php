<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;

class OperationsFunctions extends Component
{
  public function getBreakDownLevel()
  {
    return [
      'maj' => Yii::t('app','Major'),
      'min' => Yii::t('app','Minor'),
    ];
  }

  public function getBreakDownReason()
  {
    return [
      'mec' => Yii::t('app','Mechanical'),
      'mem' => Yii::t('app','Member'),
    ];
  }

  public function getRequestStatus()
  {
    return [
      '0' => Yii::t('app','Pending'),
      '1' => Yii::t('app','Cancelled'),
      '2' => Yii::t('app','Done'),
    ];
  }

  public function getRequestStatusIcon()
  {
    return [
      '0' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Pending').'</span>',
      '1' => '<span class="badge grid-badge badge-info"><i class="fa fa-times"></i> '.Yii::t('app','Cancelled').'</span>',
      '2' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Done').'</span>',
    ];
  }

  public function getBookingTripStatus()
  {
    return [
      1,2,3,4,5,7,8
    ];
  }
}
