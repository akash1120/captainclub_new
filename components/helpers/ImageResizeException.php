<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
/**
 * PHP class to resize and scale images
 */
 class ImageResizeException extends \Exception
 {
 }
