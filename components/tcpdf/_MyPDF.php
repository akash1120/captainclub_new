<?php
require_once( __DIR__ . '/../../../vendor/tcpdf/tcpdf.php');

class MyPDF extends TCPDF
{
  //Page header
  public function Header() {
    // Logo
    $logoWidth = '200%';
    $logoHeight = '';
    if($this->CurOrientation=='L'){
      $logoWidth = '287%';
    }
    $image_file = K_PATH_IMAGES.'pdf_header.jpg';
    $this->Image($image_file, 10, 10, $logoWidth, $logoHeight, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
  }

  // Page footer
  public function Footer() {
    $logoWidth = 100; // 15mm
    $logoX = $this->w - $this->documentRightMargin - $logoWidth - 10; // 186mm. The logo will be displayed on the right side close to the border of the page
    $logoFileName = K_PATH_IMAGES.'pdf_footer.jpg';
    $logo = $this->PageNo() . ' | '. $this->Image($logoFileName, $logoX, $this->GetY()-2, $logoWidth);

    $this->SetY(-15);
    $this->Cell(1,10, $logo, 0, 0, 'R');
  }
}

?>
