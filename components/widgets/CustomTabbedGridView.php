<?php

namespace app\components\widgets;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

class CustomTabbedGridView extends GridView
{
	public $showPerPage=false;
	public $createBtn=false;
  public $createBtnLink = ['create'];
  public $createInPopUp = false;
  public $popUpHeading = '';
	public $tableOptions = ['class'=>'table'];
	public $additionalHtml = '';

	public $export=false;
	public $import=false;

  /**
   * Initializes the grid view.
   * This method will initialize required property values and instantiate [[columns]] objects.
   */
  public function init()
  {
		$this->tableOptions=['class' => 'table table-striped table-responsive-md mb-0'];
		$this->filterSelector = '#' . Html::getInputId($this->filterModel, 'pageSize');
		$this->filterSelector = '#filter-pageSize';
		$Btns='';
		if($this->createBtn==true){
			if($this->createInPopUp==true){
				$Btns.=Html::a('<i class="fas fa-plus"></i>','javascript:;',['class'=>'btn btn-xs btn-primary load-modal', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Add New','data-url'=>Url::to($this->createBtnLink),'data-heading'=>$this->popUpHeading]);
			}else{
				$Btns.=Html::a('<i class="fas fa-plus"></i>',$this->createBtnLink,['class'=>'btn btn-xs btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Add New']);
			}
		}
		if($this->import==true && Yii::$app->menuHelperFunction->checkActionAllowed('import')){
			$Btns.=Html::a('<i class="fas fa-file-import"></i>',['import'],['class'=>'btn btn-xs btn-success', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Import']);
		}
		if($this->export==true && Yii::$app->menuHelperFunction->checkActionAllowed('export')){
			$Btns.=Html::a('<i class="fas fa-download"></i>',['import'],['class'=>'btn btn-xs btn-info', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Export']);
		}
		/*
		*/
		$this->layout='
		<div class="tabs">
	    <ul class="nav nav-tabs" style="position:relative;">
	      '.$this->filterModel->tabItems.'
				<li style="position:absolute;right:5px;">
					<div class="clearfix">
						<div class="pull-right" style="margin-left:5px;">
							'.$Btns.'
						</div>
						<div class="pull-right" style="margin-left:5px;">
							'.($this->showPerPage==true ? Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id'=>'filter-pageSize', 'class'=>'form-control input-xs']) : '').'
						</div>
					</div>
				</li>
	    </ul>
	    <div class="tab-content">
	      <div class="tab-pane active">
					'.($this->additionalHtml!='' ? '<div class="grid-additional-block">'.$this->additionalHtml.'</div>' : '').'
					<section class="card">
						<div class="card-body">
							{items}
						</div>
						<div class="card-footer">
							<div class="row">
								<div class="col-sm-3">{summary}</div>
								<div class="col-sm-9 pager-container">{pager}</div>
							</div>
						</div>
					</section>
	      </div>
	    </div>
	  </div>
		';
		$this->pager=[
      'maxButtonCount' => 5,
			'linkContainerOptions' => ['class'=>'paginate_button page-item'],
			'linkOptions' => ['class'=>'page-link'],
			'prevPageCssClass' => 'previous',
			'disabledPageCssClass' => 'disabled',
			'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
    ];
    parent::init();
  }
}
