<?php

namespace app\components\widgets;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

class CustomGridView extends GridView
{
	public $cardHeader=true;
	public $showPerPage=false;
	public $createBtn=false;
	public $mailBtn=false;
  public $createBtnLink = ['create'];
	public $tableOptions = ['class'=>'table'];

	public $export=false;
	public $import=false;

  /**
   * Initializes the grid view.
   * This method will initialize required property values and instantiate [[columns]] objects.
   */
  public function init()
  {
		$this->tableOptions=['class' => 'table table-responsive-lg table-sm table-striped table-hover mb-0'];
		$this->filterSelector = '#' . Html::getInputId($this->filterModel, 'pageSize');
		$this->filterSelector = '#filter-pageSize';
		$Btns='';
		if($this->createBtn==true){
			$Btns.=Html::a('<i class="fas fa-plus"></i>',$this->createBtnLink,['class'=>'btn btn-sm btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Add New']);
		}
		if($this->mailBtn==true){
			$Btns.=Html::a('<i class="fas fa-envelope"></i>','javascript:;',['id'=>'btn-mailing', 'class'=>'btn btn-sm btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Add New']);
		}
		if($this->import==true && Yii::$app->menuHelperFunction->checkActionAllowed('import')){
			$Btns.=Html::a('<i class="fas fa-file-import"></i>',['import'],['class'=>'btn btn-sm btn-success', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Import']);
		}
		if($this->export==true && Yii::$app->menuHelperFunction->checkActionAllowed('export')){
			$Btns.=Html::a('<i class="fas fa-download"></i>',['export'],['class'=>'btn btn-sm btn-info', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>'Export']);
		}
		$carHeaderHtml='';
		if($this->cardHeader==true){
			$carHeaderHtml='
			<header class="card-header">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<h2 class="card-title">'.$this->view->title.'</h2>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="clearfix">
							<div class="pull-right">
								'.$Btns.'
							</div>
							<div class="pull-right" style="margin-right:10px;">
								'.($this->showPerPage==true ? Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id'=>'filter-pageSize', 'class'=>'form-control input-xs']) : '').'
							</div>
						</div>
					</div>
				</div>
			</header>';
		}
		$this->layout='
		<section class="card'.($this->cardHeader==true ? ' card-featured card-featured-warning' : '').'">
			'.$carHeaderHtml.'
			<div class="card-body tbl-container table-responsive">
				{items}
			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-3">{summary}</div>
					<div class="col-sm-9 pager-container">{pager}</div>
				</div>
			</div>
		</section>
		';
		$this->pager=[
      'maxButtonCount' => 5,
			'linkContainerOptions' => ['class'=>'paginate_button page-item'],
			'linkOptions' => ['class'=>'page-link'],
			'prevPageCssClass' => 'previous',
			'disabledPageCssClass' => 'disabled',
			'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
    ];
    parent::init();
  }
}
